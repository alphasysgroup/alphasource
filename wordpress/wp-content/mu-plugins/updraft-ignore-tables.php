<?php

add_filter('updraftplus_backup_table', 'my_updraftplus_backup_table', 11, 5);

function my_updraftplus_backup_table($go_ahead, $table, $table_prefix, $whichdb, $dbinfo) {
    $tables_to_not_back_up = array('wp_stream', 'wp_stream_meta');
    if (in_array($table, $tables_to_not_back_up)) return false;
    return $go_ahead;
}