<?php
/**
 * Template for displaying search forms in Custom Theme
 *
 * @package WordPress
 * @subpackage Cusotm Theme
 * @since 1.0
 * @version 1.0
 */

?>

<div class="widget-search-section">
	 <form class="search-form" method="get" action="<?php echo site_url(); ?>">
		<div class="input-group">
			<input class="form-control border-secondary py-2" type="search" name="s" placeholder="Search">
			<div class="input-group-append btn-content">
				<button class="btn footer-search-btn" type="submit">
					<i class="fa fa-search"></i>
				</button>
			</div>
		</div>
	</form>
</div>