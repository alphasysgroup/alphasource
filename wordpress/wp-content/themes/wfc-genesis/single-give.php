<?php
/** 
* Single Campaign
*
*/

/**
 *  Enqueue event-archive-specific scripts
 *
 */
add_action( 'wp_enqueue_scripts', function(){

	wp_enqueue_script(
		'events-js',
		get_stylesheet_directory_uri() . '/assets/js/single-campaign.js',
		array( 'jquery' )
	);
});

//add_filter('breadcrumb_container_class', function(){
//	return 'container';
//});
/**
 *  Force full-width-content layout setting
 */
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

/**
 *  Remove genesis inner padding and margin
 */
remove_filter( 'genesis_attr_content', 'su_set_content_class' );

/**
 *  Set container wrapper as bootstrap class
 * @return string
 */
add_filter( 'breadcrumb_container_class', function() {
	return 'container pt-3';
} );

remove_action( 'genesis_sidebar', 'genesis_do_sidebar' );
remove_action( 'genesis_loop', 'genesis_do_loop' );


/*remove_action( 'genesis_header', 'wfc_render_topbar_section' ); 
remove_action( 'genesis_header', 'wfc_render_header_section' );
add_action('genesis_after_header', function() { ?>
	<section class="container-fluid outer">
		<div class="wrapper inner">
			<div class="row">
				<div class="col-sm col-md col-lg">
					<div class="logo-container">
						<a href="<? echo esc_url( get_site_url() ); ?>"> 
							<img src="<?php echo esc_url( wfc_get_main_logo() ); ?>" alt="<?php echo __( 'Logo', 'wfc-genesis' ); ?>"> 
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
});*/
/*remove_action( 'genesis_before_header', 'wfc_render_offcanvas_section', 8 ); 
remove_action( 'genesis_header', 'wfc_render_side_slide_callout', 8 );
remove_action( 'genesis_after_header', 'wfc_do_breadcrumbs' );
remove_action( 'genesis_after_header', 'wfc_do_extended_hero', 11 );
remove_action( 'genesis_after_header', 'render_submenu_section', 12 );
remove_action( 'genesis_after_header', 'render_submenu_as_tiles', 12 );
remove_action( 'genesis_after_header', 'render_blog_post_hero_section', 13 );*/
remove_action( 'genesis_after_header', 'render_single_posts_action_buttons', 14 );
remove_action( 'genesis_footer', 'wfc_do_footer' );

add_action( 'genesis_footer', function() { ?>
	<section class="site-footer container-fluid outer ">
		<div class="wrapper inner" style="color: #e8dddd;">
			<div class="row d-flex justify-content-center mb-md-0 mb-4">
				<div class="col-12 col-md-6 give-footerDonation-ctrl d-flex justify-content-center" >
					<?php
						genesis_widget_area ('page-widget-footer5', array(
							'before' => '<div class="page-widget-last-row"><div class="widget-last-row-wrap">',
							'after' => '</div></div>',
						) );
					?>
				</div>
				<div class="col-10" style="text-align: center">
					<div class="description">
						<p style="line-height: 1; line-height: 1.5; font-size: 12px; text-align: center;"><?php echo genesis_get_option( 'footer_text' );?></p>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php
} );


remove_action('genesis_footer', 'genesis_footer_markup_open', 5);
remove_action('genesis_footer', 'genesis_footer_markup_close', 15);


add_action('genesis_loop', 'su_campaign_content');

function su_campaign_content() {
	
	global $post;
	
	$sf_campaign_id 			= get_post_meta( $post->ID, 'pwp-syncer_sf_campaign_id', true );
	$sf_gau_code			 	= get_post_meta( $post->ID, 'pwp-syncer_sf_gau_code', true );
	$campaign_leading_paragraph = get_post_meta( $post->ID, 'pwp-syncer_campaign_leading_paragraph', true );	
	$campaign_ask 				= get_post_meta( $post->ID, 'pwp-syncer_campaign_ask', true );	
	$gift_table 				= get_post_meta( $post->ID, 'pwp-syncer_gift_table', true );	
	$best_gift_label 			= get_post_meta( $post->ID, 'pwp-syncer_best_gift_label', true );	
	$payment_methods 			= get_post_meta( $post->ID, 'pwp-syncer_payment_methods', true );	
	$campaign_video_embed 		= get_post_meta( $post->ID, 'pwp-syncer_campaign_video_embed', true );	
	$other_ways_to_give 		= get_post_meta( $post->ID, 'pwp-syncer_other_ways_to_give', true );		
	$column_order 				= get_post_meta( $post->ID, 'pwp-syncer_column_order', true );		
	$checkout_url 				= get_post_meta( $post->ID, 'pwp-syncer_checkout_url', true );
	$ssl_seal 					= get_post_meta( $post->ID, 'pwp-syncer_ssl_seal', true );
	$gateway_seal 				= get_post_meta( $post->ID, 'pwp-syncer_gateway_seal', true );
	$campaign_leading_color     = get_post_meta( $post->ID, 'pwp-syncer_campaign_leading_text_color', true );
	$tribute					= $_GET['tribute'] == 'true' ? true : false;
	$checked					= $tribute ? 'checked="checked"' : '';
	
	if( have_rows('pwp-syncer_gift_table') ):
	
		$amount_tbl = get_field('pwp-syncer_gift_table');
		$default_amount = $amount_tbl[0]['amount'];
	
	endif;
	
	$gaus = get_field('pwp-syncer_gau'); ?>
	<section id="su-campaign-donation-container" class="mb-3">
		<div class="container">
			<div class="row single-campaign-header <?php echo $column_order; ?>">
				<div class="col-12">
					<p class="h3 pb-3 mb-3 d-inline-block"><?php echo __( $post->post_title , 'text_domain')  ?> </p>
				</div>
			</div>
			<div class="row <?php echo $column_order; ?>">
				<div class="camp-donation-section col-md-12 col-lg-6 mb-3">
					<div class="camp-border p-3 pb-5">
						<form id="camp-donation-details" action="<?php echo esc_url($checkout_url); ?>">	
						
							<input type="hidden" id="sf_campaign_id" name="sf_campaign_id" value="<?php echo $sf_campaign_id; ?>">
							<!--<input type="hidden" id="camp_gau" name="gau" value="<?php //echo $sf_gau_code; ?>"> -->
							<input type="hidden" id="camp_name" name="campaign" value="<?php echo esc_html($post->post_title); ?>">
		
							<!--<p class="mb-3">Donation Frequency: <span class="field-important">*</span></p>-->
							<div class="camp-frequency-container mb-4">
							
								<label id="camp-freq-type" class="camp-frequency-radio-container text2 camp-frequency-active" for="one-off"> <?php echo __( 'One Off Donation','text_domain' ) ?> 
									<input type="radio" id="one-off" name="type" value="one-off" donation-type-label="One-off" checked="">
									<!--<span class="camp-frequency-radio-checkmark bg2"></span>-->
								</label>
							
								
								<label id="camp-freq-type" class="camp-frequency-radio-container text2" for="monthly"><?php echo __( 'Monthly Donation','text_domain' )	?>
									<input type="radio" id="monthly" name="type" value="monthly" donation-type-label="monthly">
									<!--<span class="camp-frequency-radio-checkmark bg2"></span>-->
								</label>
							
							</div>
							
							<?php if( count( $gaus ) > 1 ): ?>
								<select class="form-control" name="gau">
									
									<?php while ( have_rows('pwp-syncer_gau') ) : the_row(); ?>
										<option value="<?php echo esc_html( get_sub_field( 'gau_code' ) ); ?>"> <?php echo esc_html( get_sub_field( 'dropdown_label' ) ); ?> </option>
									<?php endwhile; ?>
									
								</select>
								
							<?php elseif( count( $gaus ) == 1 ): ?> 
								<?php while ( have_rows('pwp-syncer_gau') ) : the_row(); ?>
									<input type="hidden" id="camp_gau" name="gau" value="<?php echo esc_html( get_sub_field( 'gau_code' ) ); ?>">
								<?php endwhile; ?>
							<?php endif;  ?>
							
							
							<div class="mb-5">
								<!--<p class="mb-2"><?php //echo __('Select Your Gift Amount:'); ?></p>-->
								<div id="camp_donation-amount-level-container" class="camp_donation-amount-level-container row">
									<?php 
									
									if( have_rows('pwp-syncer_gift_table') ): 
										$i = 0;
										while ( have_rows('pwp-syncer_gift_table') ) : the_row(); ?>
											<div class="col-4 col-sm-4 col-md-6 col-lg-4 mb-2">		
												<?php if ( get_sub_field( 'image_url' ) ) :?>
													<!--img src="<?php echo get_sub_field( 'image_url' ); ?>" alt="campaign-image"-->
													<div class="wfc_donation-amount-level-container-img">
														<div style="background-image: url(<?php echo get_sub_field( 'image_url' ); ?>);"></div>
													</div>
												<?php endif; ?>

												<input type="radio" id="item-<?php echo $i; ?>" name="amount" value="<?php echo get_sub_field( 'amount' ); ?>" <?php echo ( $i == 0 ) ? 'checked' : '' ; ?>>
												
												<label for="item-<?php echo $i; ?>" class="mb-0 <?php echo ($i == 0) ? 'bg2': 'text2';?>" ><span class="cur-symbol">$</span> <?php echo get_sub_field( 'amount' ); ?>
													<i class="d-block"><?php echo get_sub_field( 'impact_statement' ); ?></i>
												</label>
											</div>									
											<?php 

											$i++; 
										endwhile; 
									endif; ?>
									<div class="col-4 col-sm-4 col-md-6 col-lg-4 mb-2 other-amount-box">		
										<input type="radio" id="item-<?php echo $i; ?>" name="amount" value="0">
										<label for="item-<?php echo $i; ?>" class="mb-0 text2">OTHER AMOUNT</label>
									</div>
								</div>
							</div>
							
							<div class="other-amt-container d-none">
								<p class="d-inline-block mb-5"><?php echo $best_gift_label; ?><span> $ </span></p>
								<input id="camp-best-gift" name="otr" placeholder="Enter an amount of your choice" class="ml-2 form-control d-inline-block" type="number" value="<?php echo esc_html( $default_amount ); ?>" min="2" max="10000">
							</div>

							<div class="tribute-container d-block">
								<label class="tribute-label">Donate in memory of a loved one
								  <input id="tribute_gift" type="checkbox" name="tribute_gift" <?php echo $checked; ?> >
								  <span class="checkmark"></span>
								</label>
							
								
								<div class="tribute-field-container mt-3" style="display: none;"> 
									<span>My donation is in memory of</span>
									<div class="row">
										<div class="d-inline-block col-12 col-md-6">
											<label class="d-block" for="tribute_fname">First Name<span class="field-important">*</span></label>
											<input id="tribute_fname" class="form-control" type="text" value="" name="tribute_fname" required>
										</div>
										<div class="d-inline-block col-12 col-md-6">
											<label class="d-block" for="tribute_lname">Last Name<span class="field-important">*</span></label>
											<input id="tribute_lname" class="form-control" type="text" value="" name="tribute_lname" required>
										</div>
									</div>
								</div>
								
							</div>
							<input type="hidden" name="tribute_type" value="Memorial">
							
							<p>Select Your Payment Method:</p>
							
							<div class="camp-payment-type-container">
								<!--input type="radio" id="payment-1" name="payment-type" value="credit-card">
								<label for="payment-1">CREDIT CARD</label-->
								<?php if( have_rows('pwp-syncer_payment_methods') ): ?>
									<?php while ( have_rows('pwp-syncer_payment_methods') ) : the_row(); ?>
										<?php if( get_sub_field( 'querysting_key' ) == 'paypal' ): ?>
											<div class="paypal-btn-wrapper">
												<div class="paypal-btn-overlay"></div>
												<input class="payment_method camp-button btn rounded-0 paypal-button" 
												type="submit"
												value=""
												querystr="<?php echo get_sub_field( 'querysting_key' ); ?>"
												>
											</div>
										<?php else: ?>
											<input class="payment_method camp-button btn bg5 rounded-0" 
											type="submit" 
											value="<?php echo get_sub_field( 'payment_label' ); ?>" 
											querystr="<?php echo get_sub_field( 'querysting_key' ); ?>"
											style="background-color: <?php echo get_theme_mod('color_2'); ?>"
											>
										<?php endif; ?>
									<?php endwhile;?>
								<?php endif; ?>
								
							</div>
							
						</form>
					</div>
				</div>
				<div class="camp-ask-section col-md-12 col-lg-6 mb-3">
					<?php if ( $campaign_video_embed ) : ?>
					<div class="row camp-ask-video-container mb-3">
						<div class="col">
							<?php echo $campaign_video_embed; ?>
						</div>
					</div>
					<?php endif; ?>
					<?php if ( $campaign_leading_paragraph || $campaign_ask || $other_ways_to_give || $ssl_seal || $gateway_seal ) : ?>
					<div class="row camp-ask-details">
						<div class="col">
							<div class="camp-border p-3">
								<?php if ( $campaign_leading_paragraph ) :?>
								<p class="campaign-leading <?php echo ($campaign_leading_color) ? $campaign_leading_color : ''; ?>"><b> 
								<?php echo $campaign_leading_paragraph; ?>
								</b></p>
								<?php endif; ?>
								
								<?php if ( $campaign_ask ) : ?>
								<p style="white-space: break-spaces;"><?php echo $campaign_ask; ?></p>
								<?php endif; ?>

								<?php if ( $other_ways_to_give ) : ?>
								<p 
									class="camp-button btn btn-secondary rounded-0" 
									style="background-color: <?php echo get_theme_mod('color_2');?>; font-weight: initial;"> 
									<?php echo $other_ways_to_give; ?>
								</p>
								<?php endif; ?>
							
							<?php if ( $ssl_seal || $gateway_seal ) : ?>
								<div class="row camp-seal">
									<?php if ( $ssl_seal ) :?>
									<div class="col-12 col-lg-6">
										<img src="<?php echo wp_get_attachment_url($ssl_seal); ?>" alt="seal-img">
									</div>
									<?php endif; ?>
									<?php if ( $gateway_seal ) :?>
									<div class="col-12 col-lg-6">
										<img src="<?php echo wp_get_attachment_url($gateway_seal); ?>" alt="seal-img">
									</div>
									<?php endif; ?>
								</div>
							<?php endif; ?>		
							</div>
						</div>							
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
	<?php
}

genesis();