<?php

add_filter( 'genesis_attr_site-footer', 'wfc_add_site_footer_class' );
/**
* Adds extra class to site-footer element.
 * 
 * @param array $attributes Attributes of the site footer element.
 * 
 * @return array
 */
function wfc_add_site_footer_class( $attributes ) {

	$attributes['class'] .= ' container-fluid outer';

	return $attributes;
}

/**
*	Replace header with topbar section temporarily
*/
add_action( 'genesis_setup', function() {
	remove_action( 'genesis_header', 'genesis_do_header' );
});


add_action( 'genesis_header', 'wfc_render_topbar_section' ); 
/**
* Render Topbar section in header
*
* @since 1.0.0
*
*/
function wfc_render_topbar_section() {
	echo wfc_get_section( 'topbar' );
}



add_action( 'genesis_header', 'wfc_render_header_section' );
/**
* Render Header Section
*
* @since 1.0.0
*
*/
function wfc_render_header_section() {
	echo wfc_get_section( 'header' );
}


add_action( 'genesis_before_header', 'wfc_render_offcanvas_section', 8 ); 
/**
* Render offcanvas section
*
* @since 1.0.0
*
*/
function wfc_render_offcanvas_section() {
	echo wfc_get_section('offcanvas');
}


add_action( 'genesis_header', 'wfc_render_side_slide_callout', 8 );
/**
* Render Header Section
*
* @since 1.0.0
*
*/
function wfc_render_side_slide_callout() {
	echo wfc_get_section( 'callout-side-slide' );
}


add_action( 'genesis_after_header', 'wfc_do_breadcrumbs' );
/**
 * Add breadcrumbs after header
 *
 * @author Rowelle Gem Daguman
 */
function wfc_do_breadcrumbs() {
	echo wfc_get_section( 'breadcrumbs' );
}

// add_action( 'genesis_after_header', 'wfc_do_hero', 11 );
// /**
//  * Add the hero section after the breadcrumbs
//  */
// function wfc_do_hero() {
	
// 	$post_id = wfc_get_page_id();
	
// 	$hero_type = get_post_meta( $post_id, 'wfc_hero_type', true );
	
// 	if ( $hero_type == 'hero-w-cta' ) {
// 		echo wfc_get_section( 'hero-with-cta' );
// 	} else if ( $hero_type == 'hero-simple' ) {
// 		echo wfc_get_section( 'hero-simple' );
// 	} else {
// 		echo wfc_get_section( 'hero' );
// 	}
	
// }

/**
 * Add the extended hero renderer
 */
function wfc_do_extended_hero() {
	
	$post_id 	= wfc_get_page_id();
	$hero_type 	= get_post_meta( $post_id, 'wfc_hero_type', true );
	
	if ( $hero_type == 'hero-simple' ) {
		echo wfc_get_section( 'hero-simple' );
	} elseif ( $hero_type == 'hero-all-image' ) {
		echo wfc_get_section( 'hero-all-image' );
	} elseif ( $hero_type == 'hero-rev' ) {
		echo wfc_get_section( 'hero-rev-slider' );
	}
}
add_action( 'genesis_after_header', 'wfc_do_extended_hero', 11 );

/**
 *  render_submenu_section
 *  render Sticky Submentu section based from the singular post settings
 */
function render_submenu_section() {
	echo wfc_get_section( 'sticky-submenu' );
}
add_action( 'genesis_after_header', 'render_submenu_section', 12 );

/**
 *  render_submenu_as_tiles
 *  render Submetu as tiles section based from the singular post settings
 */
function render_submenu_as_tiles() {
	echo wfc_get_section( 'tile-submenu' );
}
add_action( 'genesis_before_footer', 'render_submenu_as_tiles', 0 );


function render_blog_post_hero_section() {
	$post_id 	= wfc_get_page_id();
	$hero_type 	= get_post_meta( $post_id, 'wfc_hero_type', true );

	if ( $hero_type == 'hero-blog-banner' ) {
		echo wfc_get_section( 'hero-blog-banner' );
	}
}
add_action( 'genesis_after_header', 'render_blog_post_hero_section', 13 );


/**
 * render_single_posts_action_buttons
 * Render action button section
 */
function render_single_posts_action_buttons() {

	if ( is_archive() ){
		return;	
	}
	
	if ( is_front_page() ) {
		return;
	} 

	global $post;

	$id		        = wfc_get_page_id();
	$show_tiles   	= get_post_meta( $id, 'show_tiles', true ); 
	
	if ( $show_tiles != '1' ) {
		echo wfc_get_section( 'action-buttons' );
	}
}
add_action( 'genesis_after_header', 'render_single_posts_action_buttons', 14 );

/**
 * Render News and Events section
 */
add_action( 'genesis_before_footer', 'news_and_events_section' , 0 );
function news_and_events_section() {
	global $post;

	$id		        = wfc_get_page_id();
	$show_section   = get_post_meta( $id, 'show_ane_section', true ); 

	if ( $show_section == '1' ) {
		echo wfc_get_section( 'articles-news-events' );
	}
}

/**
 * Render News and Events section
 */
add_action( 'genesis_after_loop', 'disclaimenr_section' );
function disclaimenr_section() {
	global $post;

	$id		        = wfc_get_page_id();
	$show_section 	= get_post_meta( $id, 'show_disclaimer_section', true ); 
	
	if ( $show_section == '1') {
		echo wfc_get_section( 'disclaimer' );
	}
}

/**
 * Remove genesis_do_footer
 * 
 * @author Rowelle Gem Daguman
 */
add_action( 'genesis_footer', function() {
	remove_action( 'genesis_footer', 'genesis_do_footer' );
}, 5 );

add_action( 'genesis_footer', 'wfc_do_footer' );
/**
 * Renders the footer section.
 */
function wfc_do_footer() {
	echo wfc_get_section( 'footer' );
}

add_action( 'genesis_footer', 'wfc_scrolltotop_footer' );
/**
 * Renders the Scroll to Top footer section.
 */
function wfc_scrolltotop_footer() {
	echo wfc_get_section( 'scroll-to-top' );
}

add_action( 'genesis_after_footer', 'wfc_do_copyright' );
/**
 * Add copyright after footer.
 * 
 * @author Rowelle Gem Daguman
 */
function wfc_do_copyright() {
	echo wfc_get_section( 'copyright' );
}


add_action( 'wp_footer', 'wfc_render_search_popup' );

/**
 * Function Name: wfc_render_search_popup() 
 * Author: Jimson Rudinas
 * Short Description: This function is for custom search popup
 *
 * @since 1.0.0
 */
function wfc_render_search_popup() {

	$search_string = isset( $_GET[ 's' ] ) ? $_GET[ 's' ] : '';

	?>
	<div class="search-popup">
		<div class="container">
			<span class="search-popup-close"><i class="fas fa-times"></i></span>
			<div class="search-popup-wrap">
				<form role="search" method="get" action="<?php echo home_url( '/' ); ?>">
					<div class="search-input-wrap mb-4">
						<input id="popup-search-input" class="h4 border4" type="text" name="s" placeholder="<?php _e( 'Search' ); ?>" value="<?php echo $search_string; ?>"/>
						<label for="popup-search-input"><i class="far fa-search"></i></label>
					</div>
					<div class="text-center">
						<button type="submit" class="btn btn-secondary">Search</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php
}

add_action( 'genesis_before_content', 'wfc_add_site_inner_row_opening_wrapper', 4 );
/**
 * Add opening wrapper right after the content-sidebar-wrap element.
 */
function wfc_add_site_inner_row_opening_wrapper() {
	echo '<div class="row">';
}

add_action( 'genesis_after_content', 'wfc_add_site_inner_row_closing_wrapper', 1000 );
/**
 * Add closing wrapper the added wrapper after content-sidebar-wrap element.
 */
function wfc_add_site_inner_row_closing_wrapper() {
	echo '</div>';
}

add_filter( 'genesis_attr_site-inner', 'wfc_add_site_inner_class', 99 );
/**
 * Adds extra class to entry element.
 * 
 * @param array $attributes Attributes of the entry element.
 * 
 * @return array
 */
function wfc_add_site_inner_class( $attributes ) {

	$attributes['class'] .= ' container-fluid outer';

	return $attributes;
}

add_filter( 'genesis_attr_content-sidebar-wrap', 'wfc_add_content_sidebar_wrap_class', 99 );
/**
 * Adds extra class to entry element.
 * 
 * @param array $attributes Attributes of the entry element.
 * 
 * @return array
 */
function wfc_add_content_sidebar_wrap_class( $attributes ) {

	$attributes['class'] .= ' wrapper inner';

	return $attributes;
}

add_action( 'genesis_before_sidebar_widget_area', 'wfc_add_sidebar_search' );
/**
 * Render news search bar in sidebar
 */
function wfc_add_sidebar_search() {
	
	if ( is_page('news') || is_singular( 'post' ) ) {
		$search = isset($_GET['ascmpsrch']) ? $_GET['ascmpsrch'] : '';
		?>
		<section class="sidebar-search-wrapper mb-3">
			<form class="search-form" method="get" action="/news">
				<div class="search-form-group d-flex">
					<input class="search-form-input" type="search" name="ascmpsrch" value="<?php echo $search; ?>" placeholder="Search . . .">							
					<button class="btn btn-default search-btn" type="submit"><i class="fas fa-search"></i></button>	
				</div> 
			</form>
		</section>
		<?php
	}
	
}