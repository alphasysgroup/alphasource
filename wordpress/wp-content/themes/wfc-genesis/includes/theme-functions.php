<?php
/**
 * WFC Genesis.
 *
 * This file adds theme functions to the WFC Genesis Child Theme.
 *
 * @package WFC_Genesis/Core
 * @author  AlphaSys
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Add Theme Support for Yoast SEO Breadcrumbs
add_theme_support( 'yoast-seo-breadcrumbs' );

/**
 * Enqueue WFC Genesis theme style sheet at higher priority
 */ 
add_action( 'genesis_setup', function() {

	
	//* Remove the entry title (requires HTML5 theme support)
	remove_action( 'genesis_entry_header', 'genesis_do_post_title' );	
	
	// Remove structural wraps support
	remove_theme_support( 'genesis-structural-wraps' );
	remove_post_type_support( 'page', 'genesis-layouts' );
	remove_post_type_support( 'post', 'genesis-layouts' );
	remove_action( 'genesis_meta', 'genesis_load_stylesheet' );
	add_action( 'wp_enqueue_scripts', 'genesis_enqueue_main_stylesheet', 15 );


	// register additional sidebar
	genesis_register_layout(
		'sidebaralt-content', // A layout slug of your choice. Used in body classes. 
		[
			'label' => __( 'Secondary Sidebar, Content', '' ),
			'img'   => get_stylesheet_directory_uri() . '/assets/img/scs.gif',
			'type'  => [ 'site' ],
		]
	);
	
	// register genesis layout settings to custom post types
	$post_types 	= array_keys( get_cptonomy_post_type_objects() );
	$post_types[] 	= 'document'; 			// add plugin created post types here

	foreach ( $post_types as $post_type ) {
		add_post_type_support( $post_type, 'genesis-layouts' );
	} 
	
	genesis_register_sidebar( array(
		'id'		=> 'page-widget-mobile-footer',
		'name'		=> __( 'Pancare Foundation Mobile Footer Widget ', 'nabm' ),
		'description'	=> __( 'This is the widget area for a specific page.', 'nabm' ),
	) );
	
	genesis_register_sidebar( array(
		'id'		=> 'page-widget-footer1',
		'name'		=> __( 'Pancare Foundation Widget ', 'nabm' ),
		'description'	=> __( 'This is the widget area for a specific page.', 'nabm' ),
	) );
	
	genesis_register_sidebar( array(
		'id'		=> 'page-widget-footer2',
		'name'		=> __( 'Widget Footer Column 2', 'nabm' ),
		'description'	=> __( 'This is the widget area for a specific page.', 'nabm' ),
	) );
	
	genesis_register_sidebar( array(
		'id'		=> 'page-widget-footer3',
		'name'		=> __( 'Widget Footer Column 3', 'nabm' ),
		'description'	=> __( 'This is the widget area for a specific page.', 'nabm' ),
	) );
	
	genesis_register_sidebar( array(
		'id'		=> 'page-widget-footer4',
		'name'		=> __( 'Widget Footer Column 4', 'nabm' ),
		'description'	=> __( 'This is the widget area for a specific page.', 'nabm' ),
	) );
	
	genesis_register_sidebar( array(
		'id'		=> 'page-widget-footer5',
		'name'		=> __( 'Widget Footer Column 5', 'nabm' ),
		'description'	=> __( 'This is the widget area for a specific page.', 'nabm' ),
	) );

} );

/**
 * Enqueues scripts and styles.
 */
add_action( 'wp_enqueue_scripts', function() {


	$dir_uri = get_stylesheet_directory_uri();
	$cdn_uri = '//cdnjs.cloudflare.com/ajax/libs';

	$styles = [
		[ 'js-offcanvas', '//npmcdn.com/js-offcanvas/dist/_css/minified/js-offcanvas.css' ],
		[ 'fontawesome', $dir_uri . '/assets/lib/fontawesome-pro/css/all.min.css' ],
		[ 'bootstrap', $cdn_uri . '/twitter-bootstrap/4.3.1/css/bootstrap.min.css' ],
		[ 'fullpage', $cdn_uri . '/fullPage.js/3.0.7/fullpage.min.css' ],
		[ 'wfc-card', $dir_uri . '/assets/css/card.css' ],
	];

	$scripts = [
		[ 'bootstrap', $cdn_uri . '/twitter-bootstrap/4.3.1/js/bootstrap.min.js', ['jquery'] ],
		[ 'match-height', $cdn_uri . '/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js' ],
		[ 'js-offcanvas', '//npmcdn.com/js-offcanvas/dist/_js/js-offcanvas.pkgd.min.js' ],
		[ 'fullpage', $cdn_uri . '/fullPage.js/3.0.7/fullpage.min.js' ],
		[ 'wfc-custom', $dir_uri . '/assets/js/custom.js' ],
	];
	
	foreach ( $styles as $style ) {
		wp_enqueue_style( $style[0], $style[1] );
	}

	foreach ( $scripts as $script ) {
		$dep = isset( $script[2] ) ? $script[2] : [];
		wp_enqueue_script( $script[0], $script[1], $dep, null, true );
	}

} );

/**
 * Enqueue admin scripts and styles.
 */
add_action( 'admin_enqueue_scripts', function() {

	$dir_uri 	= get_stylesheet_directory_uri();
	$cdn_uri 	= '//cdnjs.cloudflare.com/ajax/libs';
	$styles 	= [];
	$scripts 	= [
		[ 'custom-admin', $dir_uri . '/assets/js/custom-admin.js' ],
	];
	
	foreach ( $styles as $style ) {
		wp_enqueue_style( $style[0], $style[1] );
	}

	foreach ( $scripts as $script ) {
		$dep = isset( $script[2] ) ? $script[2] : [];
		wp_enqueue_script( $script[0], $script[1], $dep, null, true );
	}
} );


/**
 * Defines all required Class.
 */
$require_classes = [
	/* Adds helper functions */
	'/includes/helper-functions.php',

	/* Adds custom sections and controls to Customizer. */
	'/includes/customizer/customizer.php',

	/* Adds TGM Plugin Activation support. */
	'/includes/tgm-plugin/tgm-plugin-init.php',

	/* Optimization */
	'/includes/optimization/disable-emojis.php', 
	'/includes/optimization/scripts-styles.php', 
	'/includes/optimization/clean-menu.php',

	/* WFC Yoast Breadcrumbs */
	'/includes/yoast-breadcrumbs.php', 

	/*This is for the sidebar*/
	'/includes/widgets/class-document-download-widget.php', 

	/*Sidebar Menu Widget*/
	'/includes/widgets/class-sidebar-menu-widget.php', 
	
];


foreach ( $require_classes as $class ) {
	require_once get_stylesheet_directory() . $class;
}

/**
 * Initialize the off canvas library.
 */
add_action( 'wp_footer', function() { 
   
    ?>

    <script type="text/javascript">
        jQuery( document ).ready( function( $ ) {
          $( document ).trigger( 'enhance' );
        });
    </script>

    <?php

}, 11 );

/**
 * Displays error when WFC Toolkit plugin is not
 * installed and active.
 */
add_action( 'admin_notices', function() {
	if ( ! is_plugin_active( 'wfc-toolkit/wfc-toolkit.php' ) ) {
		?>
		<div class="notice notice-error is-dismissible">
			<p><?php _e( '<strong>Error:</strong> WFC Toolkit plugin is required to install when using WFC Genesis child theme.', 'wfc-genesis' ); ?></p>
		</div>
		<?php
	}
} );

add_action( 'save_post', 'wfc_delete_post_related_transients', 10, 3 );
/**
 * Deletes all related transients of the post.
 *
 * @param int $post_id Post ID.
 * @param WP_Post $post Post object.
 * @param bool $update Whether this is an existing post being updated or not.
 */
function wfc_delete_post_related_transients( $post_id, $post, $update ) {

	if ( $update ) {
		// Deletes all WFC transient on the currently updated post.
		wfc_delete_post_transients( $post_id );

		/*
		 * Deletes all WFC image transient from posts with the
		 * post type slug like the currently edited post's slug.
		 * This part is for the fail graceful. 
		 */
		if ( post_type_exists( $post->post_name ) ) {
			$ids = get_posts( array(
				'fields' 			=> 'ids',
				'posts_per_page' 	=> -1,
				'post_type' 		=> $post->post_name
			) );

			array_walk( $ids, 'wfc_delete_post_image_transients' );
		}
	}

	// Make sure to run only once when updating post.
	remove_action( 'save_post', 'wfc_delete_post_related_transients' );
}


/**
 * children_as_accordion_shortcode
 * render Children as Accordion from a repeater meta
 * Attributes:
 *  - acf_field - the repeater meta key
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`)
 * 
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */
add_shortcode( 'children_as_accordion', 'children_as_accordion_shortcode' );

function children_as_accordion_shortcode( $atts ){
	
	$atts = shortcode_atts( array(
		'acf_field' 	=> '',
		'title_key' 	=> 'title',
		'content_key'	=> 'content'
	), $atts );
	
	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 
	
	$children		= get_field( $atts['acf_field'] );
	
	if (  ! empty( $children ) ) :
	
		$children_key 	= array_keys( $children );
		$children_key 	= array_reverse( $children_key, true );
		$children_key 	= array_pop( $children_key );
		
		ob_start(); ?>
			
			<div class="accordion wfc-accordion" id="children-accordion">
				<?php foreach ( $children as $key => $child ): 
					if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
						continue;
					endif; ?>
					
					<div class="card">
						<?php if ( !empty( $atts['title_key'] && !empty( $child[ $atts['title_key'] ] ) ) ) : ?>	
							<a class="card-header" href="<?php echo esc_attr( '#collapse-' . $key ); ?>" data-toggle="collapse" data-target="<?php echo esc_attr( '#collapse-' . $key ); ?>" aria-expanded="true" aria-controls="<?php echo esc_attr( 'collapse-' . $key ); ?>">
								<button class="btn"><?php echo $child[ $atts['title_key'] ]; ?></button>	
							</a>		
						<?php endif; ?>
						<?php if ( !empty( $atts['content_key'] && !empty( $child[ $atts['content_key'] ] ) ) ) : ?>
							<div id="<?php echo esc_attr( 'collapse-' . $key ); ?>" class="<?php echo esc_attr( 'collapse' . ( $children_key === $key ? ' show' : '' ) ); ?>" data-parent="#children-accordion">
								<div class="card-body">
									<?php echo $child[ $atts['content_key'] ]; ?>
								</div>
							</div>
						<?php endif; ?>	
					</div>
					
				<?php endforeach; ?>
			</div>
			
		<?php return ob_get_clean();
	endif;
}

/**
 * children_as_ht_amv_shortcode
 * render Children as Horizontal Tab/ Accordion Mobile View from a repeater meta
 * Attributes:
 *  - acf_field - the repeater meta key
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`)
 *  
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */
add_shortcode( 'children_as_ht_amv', 'children_as_ht_amv_shortcode' );
 
function children_as_ht_amv_shortcode( $atts ){
	 
	$atts = shortcode_atts( array(
		
		'acf_field' 	=> '',
		'title_key' 	=> 'title',
		'content_key'	=> 'content'
	
	), $atts );

	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 

	$children		= get_field( $atts['acf_field'] );
	
	if (  ! empty( $children ) ) :
	
		$children_key 	= array_keys( $children );
		$children_key 	= array_reverse( $children_key, true );
		$children_key 	= array_pop( $children_key );
	
		ob_start(); ?>
			
			<div class="wfc-horizontal-tab d-none d-md-block">
				<ul class="nav nav-tabs">
					<?php foreach ( $children as $key => $child ): 
						if ( empty( $child[ $atts['title_key'] ] ) ) : 
							continue; 
						endif; ?>
						
						<li class="nav-item">
							<a class="<?php echo esc_attr( 'nav-link border-bottom' . ( $children_key === $key ? ' active' : '' ) ); ?>" href="<?php echo esc_attr( '#collapse-' . $key ); ?>" data-toggle="pill" role="tab" aria-controls="<?php echo esc_attr( 'collapse-' . $key ); ?>">
								<?php echo $child[ $atts['title_key'] ]; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<div class="tab-content border border-top-0">
					<?php foreach ( $children as $key => $child ): 
						
						if ( empty( $child[ $atts['content_key'] ] ) ) : 
								continue; 
						endif; ?>
						
						<div class="<?php echo esc_attr( 'tab-pane fade' . ( $children_key === $key ? ' show active' : '' ) ); ?>" id="<?php echo esc_attr( 'collapse-' . $key ); ?>" role="tabpanel">
							<?php echo $child[ $atts['content_key'] ]; ?>
						</div>
						
					<?php endforeach; ?>
				</div>
			</div>
			<div class="accordion wfc-accordion d-block d-md-none" id="children-accordion">
				<?php foreach ( $children as $key => $child ): 
							if ( empty( $child[ $atts['title_key'] ] ) 
									&& empty( $child[ $atts['content_key'] ] ) ) :
								continue;
							endif; ?>
							
					<div class="card">
						<a class="card-header" href="<?php echo esc_attr( '#collapse-' . $key ); ?>" data-toggle="collapse" data-target="<?php echo esc_attr( '#collapse-' . $key ); ?>" aria-expanded="true" aria-controls="<?php echo esc_attr( 'collapse-' . $key ); ?>">
							<button class="btn"><?php echo $child[ $atts['title_key'] ]; ?></button>
						</a>
						<div id="<?php echo esc_attr( 'collapse-' . $key ); ?>" class="<?php echo esc_attr( 'collapse' . ( $children_key === $key ? ' show' : '' ) ); ?>" data-parent="#children-accordion">
							<div class="card-body">
								<?php echo $child[ $atts['content_key'] ]; ?>
							</div>
						</div>
					</div>
					
				<?php endforeach; ?>
			</div>
			
		<?php return ob_get_clean();
	endif;
}
 
/**
 * children_as_ht_dmv_shortcode
 * render Children as Horizontal Tab/ Dropdown Mobile View from a repeater meta
 * Attributes:
 *  - acf_field - the repeater meta key
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`)
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */

add_shortcode( 'children_as_ht_dmv', 'children_as_ht_dmv_shortcode' );

function children_as_ht_dmv_shortcode( $atts ){
	$atts = shortcode_atts( array(
		
		'acf_field' 	=> '',
		'title_key' 	=> 'title',
		'content_key'	=> 'content'
	
	), $atts );
	
	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 
	
	$children		= get_field( $atts['acf_field'] );
	
	if (  ! empty( $children ) ) :
		
		$children_key 	= array_keys( $children );
		$children_key 	= array_reverse( $children_key, true );
		$children_key 	= array_pop( $children_key );
	
		ob_start(); ?>
		
			<div class="wfc-horizontal-tab d-none d-md-block">
				<ul class="nav nav-tabs">
					<?php foreach ( $children as $key => $child ): 
						
						if ( empty( $child[ $atts['title_key'] ] )) : 
							continue; 
						endif; ?>
						
						<li class="nav-item">
							<a class="<?php echo esc_attr( 'nav-link border-bottom p-3' . ( $children_key === $key ? ' active' : '' ) ); ?>" href="<?php echo esc_attr( '#collapse-' . $key ); ?>" data-toggle="pill" role="tab" aria-controls="<?php echo esc_attr( 'collapse-' . $key ); ?>">
								<?php echo $child[ $atts['title_key'] ]; ?>
							</a>
						</li>
						
					<?php endforeach; ?>
				</ul>
				<div class="tab-content border border-top-0">
					<?php foreach ( $children as $key => $child ): 
						
						if ( empty( $child[ $atts['content_key'] ] ) ) : 
							 continue; 
						endif; ?>
						
						<div class="<?php echo esc_attr( 'tab-pane fade' . ( $children_key === $key ? ' show active' : '' ) ); ?>" id="<?php echo esc_attr( 'collapse-' . $key ); ?>" role="tabpanel">
							<?php echo $child[ $atts['content_key'] ]; ?>
						</div>
						
					<?php endforeach; ?>
				</div>
			</div>
			<div class="wfc-dropdown-toggle d-block d-md-none">
				<div>
					<select id="children-dropdown" class="form-control">
						<?php foreach ( $children as $key => $child ): 
							
							if ( empty( $child[ $atts['title_key'] ] ) ) : 
								 continue; 
							endif; ?>
							
							<option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $child[ $atts['title_key'] ] ); ?></option>
						
						<?php endforeach; ?>
					</select>
				</div>
				<div class="tab-content border">
					<?php foreach ( $children as $key => $child ): 
						
						if ( empty( $child[ $atts['content_key'] ] ) ) : 
							 continue; 
						endif; ?>
						
						<div class="tab-pane fade" id="<?php echo esc_attr( 'collapse-' . $key ); ?>" role="tabpanel">
							<?php echo $child[ $atts['content_key'] ]; ?>
						</div>
						
					<?php endforeach; ?>
				</div>
				<script type="text/javascript">
					(function($) {
						$('#children-dropdown').on('change', function(e) {
							var val = $(this).val();

							$('.wfc-dropdown-toggle .tab-content .tab-pane').removeClass('show active');
							$('.wfc-dropdown-toggle .tab-content #collapse-' + val).addClass('show active');
						}).trigger('change');
					})(window.jQuery);
				</script>
			</div>
		
		<?php return ob_get_clean();
	endif;
}  
 
/**
 * children_as_paragraph_shortcode
 * render Children as Paragraph from a repeater meta
 *  * Attributes:
 *  - acf_field - the repeater meta key
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`) 
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */
 add_shortcode( 'children_as_paragraph', 'children_as_paragraph_shortcode' );
 
 function children_as_paragraph_shortcode( $atts ){
	$atts = shortcode_atts( array(
		'acf_field'		=> '',
		'title_key'		=> 'title',
		'content_key'	=> 'content'
	), $atts ); 
	
	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 

	$children = get_field( $atts['acf_field'] ); 

	if (  ! empty( $children ) ) :
		ob_start(); ?>
			<div class="wfc-children-paragraph">
				<?php foreach ( $children as $key => $child ): 
					if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
						continue;
					endif; ?>
					<div class="paragraph-wrapper">
						<h2><?php echo $child[ $atts['title_key'] ]; ?></h2>
						<div>
							<?php echo $child[ $atts['content_key'] ]; ?>
						</div>
					</div>
				<?php endforeach;?>
			</div>
		<?php return ob_get_clean();
	endif;
	
 }
 
 
 /**
 * children_as_vt_amv_shortcode
 * render Children as Vertical Tab/Accordion Mobile View from a repeater meta
 *  * Attributes:
 *  - acf_field - the repeater meta key
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`) 
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */
add_shortcode( 'children_as_vt_amv', 'children_as_vt_amv_shortcode' );

function children_as_vt_amv_shortcode( $atts ){
	$atts = shortcode_atts( array(
		'acf_field'		=> '',
		'title_key'		=> 'title',
		'content_key'	=> 'content'
	), $atts ); 
	
	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 

	$children = get_field( $atts['acf_field'] ); 

	if (  ! empty( $children ) ) :

		$first_key = array_keys( $children );
		$first_key = array_reverse( $first_key, true );
		$first_key = array_pop( $first_key ); 

		ob_start(); ?>
			<div class="accordion wfc-accordion d-block d-md-none" id="children-accordion">
				<?php foreach ( $children as $key => $child ): 
					if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
						continue;
					endif; ?>
					<div class="card">
						<a class="card-header" href="<?php echo esc_attr( '#collapse-' . $key ); ?>" data-toggle="collapse" data-target="<?php echo esc_attr( '#collapse-' . $key ); ?>" aria-expanded="true" aria-controls="<?php echo esc_attr( 'collapse-' . $key ); ?>">
							<button class="btn"><?php echo $child[ $atts['title_key'] ]; ?></button>
						</a>
						<div id="<?php echo esc_attr( 'collapse-' . $key ); ?>" class="<?php echo esc_attr( 'collapse' . ( $first_key === $key ? ' show' : '' ) ); ?>" data-parent="#children-accordion">
							<div class="card-body">
								<?php echo $child[ $atts['content_key'] ]; ?>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
			<div class="wfc-vertical-tab d-none d-md-flex">
				<ul class="nav nav-tabs flex-column border-bottom-0" role="tablist">
					<?php foreach ( $children as $key => $child ): 
						if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
							continue;
						endif; ?>
						<li class="nav-item">
							<a class="<?php echo esc_attr( 'nav-link' . ( $first_key === $key ? ' active' : '' ) ); ?>" id="<?php echo esc_attr( 'tab-' . $key ); ?>" data-toggle="tab" href="<?php echo esc_attr( '#tab-panel-' . $key ); ?>" role="tab" aria-controls="<?php echo esc_attr( 'tab-panel-' . $key ); ?>" aria-selected="true"><?php echo $child[ $atts['title_key'] ]; ?></a>
						</li>
					<?php endforeach; ?>
				</ul>
				<!-- Tab panes -->
				<div class="tab-content w-100 border">
						<?php foreach ( $children as $key => $child ): 
							if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
								continue;
							endif; ?>
						<div class="<?php echo esc_attr( 'tab-pane' . ( $first_key === $key ? ' active' : '' ) ); ?>" id="<?php echo esc_attr( 'tab-panel-' . $key ); ?>" role="tabpanel" aria-labelledby="<?php echo esc_attr( 'tab-' . $key ); ?>">
							<?php echo $child[ $atts['content_key'] ]; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		<?php return ob_get_clean();
	endif;
}

/**
 * render_children_as_list
 * render children as list from a repeater meta
 * Attributes:
 *  - acf_field - the repeater meta key
 *  - image_key - the image subfield (defaults to `image`)
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`)
 *  - url_key - the url subfield (defaults to `url`)
 *  
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */
function render_children_as_list( $atts ) { 

	$atts = shortcode_atts( array(
		'acf_field'		=> '',
		'image_key'		=> 'image',
		'title_key'		=> 'title',
		'content_key'	=> 'content',
		'url_key'		=> 'url',
	), $atts ); 

	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 
	
	$children = get_field( $atts['acf_field'] ); 

	if (  ! empty( $children ) ) :
		ob_start(); ?>

		<div class="wfc-children-list">

			<?php 
			foreach ( $children as $child ) : 

				if ( empty( $child[ $atts['image_key'] ] )
					&& empty( $child[ $atts['title_key'] ] ) 
					&& empty( $child[ $atts['content_key'] ] ) 
					&& empty( $child[ $atts['url_key'] ] ) ) :
						continue;
				endif; ?>
				
				<div class="card">
					<?php if ( !empty( $atts['image_key'] ) && !empty( $child[ $atts['image_key'] ] ) ) : ?>
						<img class="img-fluid" src="<?php echo $child[ $atts['image_key'] ]; ?>" />
					<?php endif; ?>

					<div class="card-body">
						<?php if ( !empty( $atts['title_key'] ) && !empty( $child[ $atts['title_key'] ] ) ) : ?>
							<h4><?php echo $child[ $atts['title_key'] ]; ?></h4>
						<?php endif; ?>
						
						<?php if ( !empty( $atts['content_key'] ) && !empty( $child[ $atts['content_key'] ] ) ) : ?>
							<div><?php echo wp_trim_words( $child[ $atts['content_key'] ] ); ?></div>
						<?php endif; ?>

						<?php if ( !empty( $atts['url_key'] ) && !empty( $child[ $atts['url_key'] ] ) ) : ?>
							<a href="<?php echo esc_url( $child[ $atts['url_key'] ] ); ?>"><?php esc_html_e( 'View more...', 'wfc-genesis' ); ?></a>
						<?php endif; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>

		<?php
		return ob_get_clean();
	endif;
}
add_shortcode( 'children_as_list', 'render_children_as_list' );

/**
 * render_children_as_vt_dmv
 * render children as vertical tab from a repeater meta
 * Attributes:
 *  - acf_field - the repeater meta key
 *  - title_key - the title subfield (defaults to `title`)
 *  - content_key - the content subfield (defaults to `content`)
 * 
 * @param array $atts Shortcode args/attributes 
 * @return string The shortcode content
 */
function render_children_as_vt_dmv( $atts ) {
	$atts = shortcode_atts( array(
		'acf_field'		=> '',
		'title_key'		=> 'title',
		'content_key'	=> 'content',
	), $atts ); 
	
	// return empty content if the repeater field is empty
	if ( empty( $atts['acf_field'] ) ) {
		return '';
	} 

	if ( ! function_exists('get_field') ) {
		return '';
	} 

	$children = get_field( $atts['acf_field'] ); 
	
	if ( ! empty( $children ) ) : 

		$first_key = array_keys( $children );
		$first_key = array_reverse( $first_key, true );
		$first_key = array_pop( $first_key ); 
		
		ob_start(); ?>

		<div class="wfc-vertical-tab d-none d-md-flex">
			<ul class="nav nav-tabs flex-column border-bottom-0" role="tablist">
				<?php 
				foreach ( $children as $key => $child ): 

					if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
							continue;
					endif; ?>
					
					<li class="nav-item">
						<a class="<?php echo esc_attr( 'nav-link' . ( $first_key === $key ? ' active' : '' ) ); ?>" id="<?php echo esc_attr( 'tab-' . $key ); ?>" data-toggle="tab" href="<?php echo esc_attr( '#tab-panel-' . $key ); ?>" role="tab" aria-controls="<?php echo esc_attr( 'tab-panel-' . $key ); ?>" aria-selected="true"><?php echo $child[ $atts['title_key'] ]; ?></a>
					</li>
				<?php endforeach; ?>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content w-100 border">
				<?php 
				foreach ( $children as $key => $child ): 
					
					if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
						continue;
					endif; ?>

					<div class="<?php echo esc_attr( 'tab-pane' . ( $first_key === $key ? ' active' : '' ) ); ?>" id="<?php echo esc_attr( 'tab-panel-' . $key ); ?>" role="tabpanel" aria-labelledby="<?php echo esc_attr( 'tab-' . $key ); ?>">
						<?php echo $child[ $atts['content_key'] ]; ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="wfc-dropdown-toggle d-block d-md-none">
			<div>
				<select id="children-dropdown" class="form-control">
					<?php 
					foreach ( $children as $key => $child ): 
						
						if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
							continue;
						endif; ?>

						<option value="<?php echo esc_attr( $key ); ?>"><?php echo esc_html( $child[ $atts['title_key'] ] ); ?></option>
					<?php endforeach; ?>
				</select>
			</div>
			<div class="tab-content border">
				<?php 
				foreach ( $children as $key => $child ): 

					if ( empty( $child[ $atts['title_key'] ] ) && empty( $child[ $atts['content_key'] ] ) ) :
						continue;
					endif; ?>

					<div class="tab-pane fade" id="<?php echo esc_attr( 'collapse-' . $key ); ?>" role="tabpanel">
						<?php echo $child[ $atts['content_key'] ]; ?>
					</div>
				<?php endforeach; ?>
			</div>
			<script type="text/javascript">
				(function($) {
					$('#children-dropdown').on('change', function(e) {
						var val = $(this).val();

						$('.wfc-dropdown-toggle .tab-content .tab-pane').removeClass('show active');
						$('.wfc-dropdown-toggle .tab-content #collapse-' + val).addClass('show active');
					}).trigger('change');
				})(window.jQuery);
			</script>
		</div>

		<?php
		return ob_get_clean();
	endif;
}
add_shortcode( 'children_as_vt_dmv', 'render_children_as_vt_dmv' );



/**
 * register_customization_scripts
 * 
 * @param  
 * @return 
 */
function register_customization_scripts() {
    wp_enqueue_style( 'custom-css', get_stylesheet_directory_uri() . '/assets/css/customization.css' );
}
add_action( 'wp_enqueue_scripts', 'register_customization_scripts' );


/** 
 *  Modify Secondary sidebar template position
 */
remove_action( 'genesis_after_content_sidebar_wrap', 'genesis_get_sidebar_alt' );
add_action( 'genesis_before_content', 'genesis_get_sidebar_alt' );

/** 
 *  Add Class to body, when using the custom layout 
 */
add_filter( 'body_class', function( $classes ) { 
	$site_layout = genesis_site_layout(); 

	if ( $site_layout ) {
		$classes[] = $site_layout; 
	} 
	
	return $classes; 
} ); 


/**
 * register_pancare_customizer_ctrls
 * Register custom customizer controls
 * 
 */
function register_pancare_customizer_ctrls( $customizer_settings, $wp_customize ) {
	/*=== Register Panel ===*/
	$customizer_settings['panels'][]	= array(
		'id' 		=> 'section_conf',
		'title' 	=> esc_html__( 'Section Settings', 'wfc-genesis' ),
		'priority'	=> 22
	);



	/*=== Register Sections ===*/
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_topbar',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Topbar', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_front_page',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Front Page', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_spost_action_button',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Singular Posts Action Buttons', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_sticky_submenu',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Sticky Submenu', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_tile_submenu',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Tile Submenu', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_callout_button',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Callout Button', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_disclaimer',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Disclaimer', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_ane',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( 'Articles, News & Events', 'wfc-genesis' )
	);
	$customizer_settings['sections'][] = array(
		'id' 		=> 'section_404',
		'panel' 	=> 'section_conf',
		'title' 	=> esc_html__( '404 Section Content', 'wfc-genesis' )
	);
	
	$customizer_settings['sections'][]	= array(
		'id' 		=> 'newsletter_sign_up',
		'title' 	=> esc_html__( 'Newsletter Sign Up', 'wfc-genesis' ),
	);


	/*=== Register Controls ===*/

	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_404',
		'id' 		=> 'section_404_button_label',
		'default'	=> 'Get In Touch',
		'label' 	=> esc_html__( 'Button Label', 'wfc-genesis' ),
	);	

	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_404',
		'id' 		=> 'section_404_button_href',
		'default'	=> '/',
		'label' 	=> esc_html__( 'Button Link', 'wfc-genesis' ),
	);	

	/** Disclaimer */
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_disclaimer',
		'id' 		=> 'section_disclaimer_sc',
		'label' 	=> esc_html__( 'Section Content', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_disclaimer',
		'id' 		=> 'section_disclaimer_s',
		'default'	=> '',
		'label' 	=> esc_html__( 'Panel Shortcode', 'wfc-genesis' ),
	);

	/** articles, news, and events */
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_ane',
		'id' 		=> 'section_ane_sc',
		'label' 	=> esc_html__( 'Section Content', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_ane',
		'id' 		=> 'section_ane_s',
		'default'	=> '',
		'label' 	=> esc_html__( 'Panel Shortcode', 'wfc-genesis' ),
	);

	/** Topbar */
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_topbar',
		'id' 		=> 'section_topbar_colors',
		'label' 	=> esc_html__( 'Colors', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_topbar',
		'id' 		=> 'section_topbar_bg_color',
		'default'	=> '#3C455C',
		'label' 	=> esc_html__( 'Background Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_topbar',
		'id' 		=> 'section_topbar_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'Text Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_topbar',
		'id' 		=> 'section_topbar_url',
		'label' 	=> esc_html__( 'Shop Url', 'wfc-genesis' ),
	);

	/** Latest News Section */
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_latest_news',
		'label' 	=> esc_html__( 'Box Section', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'section'		=> 'section_front_page',
		'id'			=> 'section_front_page_bg_image',
		'default'		=> '',
		'label'			=> esc_html__( 'Section Background', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_min_height',
		'default'	=> '735px',
		'label' 	=> esc_html__( 'Min height', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_min_card_height',
		'default'	=> '520px',
		'label' 	=> esc_html__( 'Min card height', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_latest_news_cl',
		'label' 	=> esc_html__( 'Box Section: Left Card', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_category',
		'default'	=> '',
		'label' 	=> esc_html__( 'Category', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_title',
		'default'	=> '',
		'label' 	=> esc_html__( 'Title', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_subtitle',
		'default'	=> '',
		'label' 	=> esc_html__( 'Subtitle', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_content',
		'default'	=> '',
		'label' 	=> esc_html__( 'Content', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'section'		=> 'section_front_page',
		'id'			=> 'section_front_page_cl_image',
		'default'		=> '',
		'label'			=> esc_html__( 'Image', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_cta_text',
		'default'	=> 'Read More',
		'label' 	=> esc_html__( 'CTA Text', 'wfc-genesis' ),
	);		
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_cta_link',
		'default'	=> '',
		'label' 	=> esc_html__( 'CTA Link', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'checkbox',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_cta_nt',
		'default'	=> 0,
		'label' 	=> esc_html__( 'Open link in new tab', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_cta_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'CTA Color', 'wfc-genesis' ),
	);		
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_cta_bg',
		'default'	=> '#323D52',
		'label' 	=> esc_html__( 'CTA Background Color', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_bg',
		'default'	=> '#289F74',
		'label' 	=> esc_html__( 'Background Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cl_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'Text Color', 'wfc-genesis' ),
	);
	//-
	
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_latest_news_cr',
		'label' 	=> esc_html__( 'Box Section: Right Card', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_category',
		'default'	=> '',
		'label' 	=> esc_html__( 'Category', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_title',
		'default'	=> '',
		'label' 	=> esc_html__( 'Title', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_subtitle',
		'default'	=> '',
		'label' 	=> esc_html__( 'Subtitle', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_content',
		'default'	=> '',
		'label' 	=> esc_html__( 'Content', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'section'		=> 'section_front_page',
		'id'			=> 'section_front_page_cr_image',
		'default'		=> '',
		'label'			=> esc_html__( 'Image', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_cta_text',
		'default'	=> 'Read More',
		'label' 	=> esc_html__( 'CTA Text', 'wfc-genesis' ),
	);		
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_cta_link',
		'default'	=> '',
		'label' 	=> esc_html__( 'CTA Link', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'checkbox',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_cta_nt',
		'default'	=> 0,
		'label' 	=> esc_html__( 'Open link in new tab', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_cta_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'CTA Color', 'wfc-genesis' ),
	);		
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_cta_bg',
		'default'	=> '#323D52',
		'label' 	=> esc_html__( 'CTA Background Color', 'wfc-genesis' ),
	);	
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_bg',
		'default'	=> '#662D91',
		'label' 	=> esc_html__( 'Background Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_front_page_cr_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'Text Color', 'wfc-genesis' ),
	);


	/** AdRotate */
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_conf_adrotate_section',
		'label' 	=> esc_html__( 'AdRotate Section', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_conf_adrotate_section_title',
		'default'	=> 'Sponsors',
		'label' 	=> esc_html__( 'Section Title', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'number',
		'section' 	=> 'section_front_page',
		'id' 		=> 'section_conf_adrotate_section_group_id',
		'default'	=> 0,
		'label' 	=> esc_html__( 'Adrotate Group ID', 'wfc-genesis' ),
	);
	

	/** Banner  */
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_sticky_submenu',
		'id' 		=> 'section_sticky_submenu_text_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'Text Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_sticky_submenu',
		'id' 		=> 'section_sticky_submenu_bg_color',
		'default'	=> '#3d465b',
		'label' 	=> esc_html__( 'Background Color', 'wfc-genesis' ),
	);

	/** Brand Settings > Logos and Images  */
	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'section'		=> 'logos_and_images',
		'id'			=> 'hero_watermark',
		'default'		=> '',
		'priority'		=> 100,
		'label'			=> esc_html__( 'Light Hero Watermark Image', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'section'		=> 'logos_and_images',
		'id'			=> 'hero_watermark_dark',
		'default'		=> '',
		'priority'		=> 100,
		'label'			=> esc_html__( 'Dark Hero Watermark Image', 'wfc-genesis' ),
	);

	/** Brand Settings > Typography */
	$customizer_settings['controls'][] = array( 
		'type' 			=> 'googlefonts',
		'section' 		=> 'typography',
		'id' 			=> 'banner_font',
		'label' 		=> esc_html__( 'Banner Font', 'wfc-genesis' ),
		'description' 	=> esc_html__( 'Select and configure the font for your Hero Title.', 'wfc-genesis' ),
	);


	// Safari
	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'typography',
		'id' 		=> 'safari_typograpgy',
		'label' 	=> esc_html__( 'Browser Specific Typography', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array( 
		'type' 			=> 'googlefonts',
		'section' 		=> 'typography',
		'id' 			=> 'base_font_safari',
		'label' 		=> esc_html__( 'Safari Banner Font', 'wfc-genesis' ),
		'description' 	=> esc_html__( 'Select and configure the font for your content.', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array( 
		'type' 			=> 'googlefonts',
		'section' 		=> 'typography',
		'id' 			=> 'headings_font_safari',
		'label' 		=> esc_html__( 'Safari Headings Font', 'wfc-genesis' ),
		'description' 	=> esc_html__( 'Select and configure the font for your content.', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array( 
		'type' 			=> 'googlefonts',
		'section' 		=> 'typography',
		'id' 			=> 'btn_inp_font_safari',
		'label' 		=> esc_html__( 'Safari Buttons and Inputs Font', 'wfc-genesis' ),
		'description' 	=> esc_html__( 'Select and configure the font for your input fields and buttons.', 'wfc-genesis' ),
	);

		
	/** Site Configuration > Site Dimensions */
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'dimensions',
		'id' 		=> 'hero_height_simple_sm',
		'default'	=> '225px',
		'label' 	=> esc_html__( 'Simple Hero (small)', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'dimensions',
		'id' 		=> 'hero_height_simple_md',
		'default'	=> '250px',
		'label' 	=> esc_html__( 'Simple Hero (medium)', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'dimensions',
		'id' 		=> 'hero_height_simple_lg',
		'default'	=> '295px',
		'label' 	=> esc_html__( 'Simple Hero (large)', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array(
		'type' 		=> 'separator',
		'section' 	=> 'section_spost_action_button',
		'id' 		=> 'section_spost_action_button_addthis_settings',
		'label' 	=> esc_html__( 'AddThis Settings', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'section_spost_action_button',
		'id' 		=> 'section_spost_action_button_addthis_shortcode',
		'default'	=> '',
		'label' 	=> esc_html__( 'AddThis Share Shortcode', 'wfc-genesis' ),
	);

	/** Tile submenu settings */
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'separator',
		'section' 	=> 'section_tile_submenu',
		'id' 		=> 'section_tile_submenu_separator',
		'label' 	=> esc_html__( 'Tile Section Background', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'id'			=> 'section_tile_submenu_bg_grey',
		'section'		=> 'section_tile_submenu',
		'default'		=> '',
		'priority'		=> 100,
		'label'			=> esc_html__( 'Greyscale Background Image', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array(
		'type'			=> 'image',
		'id'			=> 'section_tile_submenu_bg_color',
		'section'		=> 'section_tile_submenu',
		'default'		=> '',
		'priority'		=> 100,
		'label'			=> esc_html__( 'Colored Background Image', 'wfc-genesis' ),
	);

	/** Callout settings */
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'separator',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_separator',
		'label' 	=> esc_html__( 'Button Settings', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_name',
		'default'	=> 'TELEHEALTH',
		'label' 	=> esc_html__( 'Button Name', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_c_title',
		'default'	=> 'TELEHEALTH',
		'label' 	=> esc_html__( 'Content Title', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_c_phone',
		'default'	=> '1300 881 698',
		'label' 	=> esc_html__( 'Content Phone', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_c_button_label',
		'default'	=> 'Book Now',
		'label' 	=> esc_html__( 'Content Button Label', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'text',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_c_button_link',
		'default'	=> '#',
		'label' 	=> esc_html__( 'Content Button Link', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'Content Button Text Color', 'wfc-genesis' ),
	);$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_bg_color',
		'default'	=> '#3C455C',
		'label' 	=> esc_html__( 'Content Button Background Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_callout_text_color',
		'default'	=> '#fff',
		'label' 	=> esc_html__( 'Call out Text Color', 'wfc-genesis' ),
	);
	$customizer_settings['controls'][] = array( 
		'type' 		=> 'color',
		'section' 	=> 'section_callout_button',
		'id' 		=> 'section_callout_button_callout_bg_color',
		'default'	=> '#289F74',
		'label' 	=> esc_html__( 'Call out Background Color', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'genesis_scripts',
		'id' 		=> 'header_scripts_home',
		'default'	=> '',
		'label' 	=> esc_html__( 'Home Page Header Scripts', 'wfc-genesis' ),
	);

	$customizer_settings['controls'][] = array( 
		'type' 		=> 'textarea',
		'section' 	=> 'genesis_scripts',
		'id' 		=> 'footer_scripts_home',
		'default'	=> '',
		'label' 	=> esc_html__( 'Home Page Footer Scripts', 'wfc-genesis' ),
	);

	/** Newsletter Sign Up Settings **/
	$customizer_settings['controls'][] = array( 
		'type' 			=> 'textarea',
		'section' 		=> 'newsletter_sign_up',
		'id' 			=> 'interest_mapping',
		'default'		=> '',
		'label' 		=> esc_html__( 'Interest Mapping', 'wfc-genesis' ),
		'description' 	=> "Enter mapping for interest. Mappings should be comma(,) separated and the <strong>SF => Interest value</strong> should be separated with (=>).<br><br>Example:<br>AS_Biliary_Cancer__c=>Biliary cancer,<br>AS_Liver_Cancer__c=>Liver cancer<br><br>Where <strong>AS_Biliary_Cancer__c</strong> is the SF field(checkbox) and <strong>Liver cancer</strong> is a category value saved in cookie post."
	);

	return $customizer_settings;
}
add_filter( 'wfc_customizer_settings', 'register_pancare_customizer_ctrls', 10, 2 );

/**
 * append_dynamic_banner_css
 * modify dynamic.css output
 */
function append_dynamic_banner_css( $css ) {
	/** Topbar */
	$section_topbar_color 		= get_theme_mod( 'section_topbar_color', '#fff' );
	$css .= '.topbar.custom-topbar a, .topbar.custom-topbar i.fa, .topbar.custom-topbar i.fas {';
	$css .= sprintf( 'color: %s!important;', $section_topbar_color );
	$css .= '}';

	$section_topbar_bg_color 	= get_theme_mod( 'section_topbar_bg_color', '#3C455C' );
	$css .= '.topbar.custom-topbar {';
	$css .= sprintf( 'background-color: %s;', $section_topbar_bg_color );
	$css .= '}';

	$link_hover_color	 		= get_theme_mod( 'link_hover_color', '#ffffff' );

	$css .= 'a:hover {';
	$css .= sprintf( 'color: %s;', $link_hover_color );
	$css .= '}';

	/* Heading */
	$banner_font = get_theme_mod( 'banner_font', '' );
	$fallback_font = get_theme_mod( 'fallback_font', 'Arial, Helvetica Neue, Helvetica, sans-serif' );
	if ( ! empty( $banner_font ) ) {
		$css .= '.banner-heading {';
		$css .= sprintf( 'font-family: \'%s\', %s;', $banner_font, $fallback_font );
		$css .= '}';
	}

	// browser specific
	$fallback_font 			= get_theme_mod( 'fallback_font', 'Arial, Helvetica Neue, Helvetica, sans-serif' );
	$base_font_safari 		= get_theme_mod( 'base_font_safari', '' );
	$headings_font_safari 	= get_theme_mod( 'headings_font_safari', '' );
	$btn_inp_font_safari 	= get_theme_mod( 'btn_inp_font_safari', '' );

	// Safari (from 6.1 to 10.0) 
	$css .= "@media screen and (min-color-index:0) and(-webkit-min-device-pixel-ratio:0) {";
		$css .= "@media {";

			/* Base */
			if ( ! empty( $base_font_safari ) ) {
				$css .= 'body {';
				$css .= sprintf( 'font-family: \'%s\', %s!important;', $base_font_safari, $fallback_font );
				$css .= '}';
			}

			/* Heading */
			if ( ! empty( $headings_font_safari ) ) {
				$css .= 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {';
				$css .= sprintf( 'font-family: \'%s\', %s!important;', $headings_font_safari, $fallback_font );
				$css .= '}';
			}

			/* Buttons */
			if ( ! empty( $btn_inp_font_safari ) ) {
				$css .= 'button, input, select, textarea {';
				$css .= sprintf( 'font-family: \'%s\', %s!important;', $btn_inp_font_safari, $fallback_font );
				$css .= '}';
			}

		$css .= "}";
	$css .= "}";


	// Safari (10.1+)
	$css .= "@media not all and (min-resolution:.001dpcm) {";
		$css .= "@media {";
			
			/* Base */
			if ( ! empty( $base_font_safari ) ) {
				$css .= 'body {';
				$css .= sprintf( 'font-family: \'%s\', %s!important;', $base_font_safari, $fallback_font );
				$css .= '}';
			}

			/* Heading */
			if ( ! empty( $headings_font_safari ) ) {
				$css .= 'h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {';
				$css .= sprintf( 'font-family: \'%s\', %s!important;', $headings_font_safari, $fallback_font );
				$css .= '}';
			}

			/* Buttons */
			if ( ! empty( $btn_inp_font_safari ) ) {
				$css .= 'button, input, select, textarea {';
				$css .= sprintf( 'font-family: \'%s\', %s!important;', $btn_inp_font_safari, $fallback_font );
				$css .= '}';
			}

		$css .= "}";
	$css .= "}";



	$sidebar_menu_title_color 	= get_theme_mod( 'color_palette_2', '' );
	if ( ! empty( $sidebar_menu_title_color ) ) {
		$css .= '.sidebar.widget-area .widget_nav_menu h4.widget-title {';
		$css .= sprintf( 'color: %s;', $sidebar_menu_title_color );
		$css .= sprintf( 'border-bottom-color: %s;', $sidebar_menu_title_color );
		$css .= '}';
	}

	$sidebar_menu_item_color 	= get_theme_mod( 'color_palette_5', '' );
	if ( ! empty( $sidebar_menu_item_color ) ) {
		$css .= '.sidebar.widget-area .widget_nav_menu li > a {';
		$css .= sprintf( 'color: %s;', $sidebar_menu_item_color );
		$css .= '}';
	}
	
	$site_max_width	= get_theme_mod( 'max_site_width', '100rem' );
	if ( ! empty( $site_max_width ) ) {
		$css .= 'header.site-header, .submenu-sticky {';
		$css .= sprintf( 'max-width: %s;', $site_max_width );
		$css .= '}';
	}

	return $css;
}
add_filter( 'wfc_dynamic_css', 'append_dynamic_banner_css' );

/**
 * get_cptonomy_post_type_objects
 * Return the post types saved in CPT Onomies
 *
 * @author Vincent Liong
 *
 * @since 1.0.0
 *
 * @return array
 * 
 */
function get_cptonomy_post_type_objects() {
	$cptonomies = get_option('custom_post_type_onomies_custom_post_types');

	return ! empty( $cptonomies ) ? $cptonomies : array();
}

/**
 * handle_sidebar_layout_failgraceful
 * Handle Genesis sidebar layout failgraceful output
 *
 * @since 1.0.0
 *
 */
function handle_sidebar_layout_failgraceful() {
    global $post;
   
	$id 		= wfc_get_page_id();
	$post_types = array_keys( get_cptonomy_post_type_objects() );
	
	/**
     *  if is archive get the layout settings from the archive page
     */
    if ( is_post_type_archive( $post_types ) ) {
        
        add_filter( 'genesis_site_layout', function(){
			$layout = get_post_meta( $id, '_genesis_layout', true );

			// only apply theme defaults if meta is not set/is equal to 'default'
			if ( ! metadata_exists( 'post', $id, '_genesis_layout' ) || empty( $layout ) ) {
				$layout = genesis_get_option( 'site_layout' );
			}

            return $layout;
        } );
	} 
}
add_action( 'template_redirect', 'handle_sidebar_layout_failgraceful' );


/*Genesis Layout Setting*/

add_action( 'add_meta_boxes', 'custom_genesis_layout_setting');

function custom_genesis_layout_setting(){
	 add_meta_box( 'genesis_inpost_layout_box', __( 'Layout Settings', 'genesis' ), 'custom_layout_setting', 'post', 'normal', 'high' );
	 add_meta_box( 'genesis_inpost_layout_box', __( 'Layout Settings', 'genesis' ), 'custom_layout_setting', 'page', 'normal', 'high' );
}

function custom_layout_setting(){
	genesis_meta_boxes()->show_meta_box( 'genesis-inpost-layout-box' );
}



/**
 * Modify Genesis breadcrumbs
 */
add_filter( 'genesis_single_crumb', 'rename_cancer_breadcrumb' , 10, 2 );
function rename_cancer_breadcrumb( $crumb, $args ){
    if ( !is_singular('cancer') ) {
        return $crumb;
    }

    $search_for		= '/Cancer/';
    $replace_with 	= 'Cancer Information';

    return preg_replace( $search_for, $replace_with, $crumb );
}

add_filter( 'wpseo_breadcrumb_links', 'wpseo_breadcrumb_linksasd' );
function wpseo_breadcrumb_linksasd( $crumbs ) {
	
	if ( is_singular('post') ) {
		array_splice( $crumbs, 1, 0, array( array( 'ptarchive'=>'post' ) ) );
	}

	return $crumbs;
}

/**
 * Modify Yoast breadcrumbs
 */
add_filter( 'wpseo_breadcrumb_single_link_info', 'yoast_rename_cancer_breadcrumb', 10, 2);
function yoast_rename_cancer_breadcrumb( $crumb, $pos ) {
    if ( $pos = 1 && is_singular('cancer') && isset( $crumb['text'] ) && $crumb['text'] == 'Cancer' ) {
		$replace_with 	= 'Cancer Information';
		$crumb['text'] 		= $replace_with;
	}

	if ( $pos = 1 && is_singular('post') && isset( $crumb['text'] ) && $crumb['text'] == 'Posts' ) {
		$replace_with 	= 'News';
		$crumb['text'] 		= $replace_with;
		$crumb['url'] 		= site_url('news');
	}
	
	return $crumb;
}

/** Manage ASCM search  */
function handle_ascm_search_bar_visibility() { 
	$post_id				= wfc_get_page_id();
	$show_ascm_search_bar	= get_post_meta( $post_id, 'show_ascm_search_bar', 1 );
	
	if( ! $show_ascm_search_bar ) : ?>
	<style>
		.ascm-panels-postgallery-cont .ascm-panels-postgallerysearch-main-cont {
			display: none!important;
		}
	</style>
<?php
	endif;
}
add_action('wp_head', 'handle_ascm_search_bar_visibility');

add_filter( 'nav_menu_link_attributes', 'add_class_to_items_link', 10, 3 );

function add_class_to_items_link( $atts, $item, $args ) {
	global $post;
	
	$parentId	= $post->post_parent;
	$childId	= $item->object_id;
	
	if( $args->menu->slug == 'main-menu' && $parentId == $childId ){
		$atts['class'] = 'parent-active';
	}

  return $atts;
}


function custom_panels_dynamic_query( $panel ) {


	// print_r( new WP_Query([
	// 	'post_type' => ['post','events'],
	// 	'tax_query'	=> [
	// 		[
	// 			'taxonomy' 	=> 'category',
	// 			'field' 	=> 'slug',
	// 			'terms' 	=> 'news',
	// 			'operator' 	=> 'IN',
	// 		]
	// 	]
	// ]) );

	global $wpdb;

	$panel_id					= $panel['data']['post']->ID;
	
	$cp_data_source				= ( function_exists('get_field') ) ? get_field( 'cp_data_source', $panel_id ) : [];
	$cp_data_source				= ( !empty( $cp_data_source ) ) ? wp_list_pluck( $cp_data_source, 'cp_card_type', 'cp_post_type' ) : [];
	array_change_key_case( $cp_data_source, CASE_LOWER );
	$post_types  				= array_keys( $cp_data_source );

	$ascm_pg_cancer_types		= get_post_meta( $panel_id, 'cancer_type', 1 );
	$ascm_pg_cancer_types		= ( !empty( $ascm_pg_cancer_types ) ) ? $ascm_pg_cancer_types : [];

	$ascm_pg_category_types		= get_post_meta( $panel_id, 'category_type', 1 );
	$ascm_pg_category_types		= ( !empty( $ascm_pg_category_types ) ) ? $ascm_pg_category_types : [];

	$query_from					= [];
	$query_where_cat			= [];
	$query_where_post_types		= [];
	$query_where_menu_order		= [];

	$query_search_keyword		= '';

	$has_other_cptonomy_pt		= 0;
	$has_common_categories		= 0;
	$has_common_pt				= 0;


	// Cancer Types tax
	if ( !empty( $ascm_pg_cancer_types ) && is_array( $post_types ) && in_array('document', $post_types ) ) :
		$query_from[]			= "INNER JOIN {$wpdb->prefix}postmeta AS cancer_types ON ({$wpdb->prefix}posts.ID = cancer_types.post_id)"; 

		$cancer_types			= join( '\', \'', $ascm_pg_cancer_types );

		$query_where_cat[] 		= "( cancer_types.meta_key = '_custom_post_type_onomies_relationship' AND cancer_types.meta_value IN ('{$cancer_types}') ) ";
	endif;

	// Category types meta
	if ( !empty( $ascm_pg_category_types ) && is_array( $post_types ) ) :
		foreach ( $post_types as $post_type ) :
			
			if ( $post_type == 'document' ) :
				$query_from[]			= "INNER JOIN {$wpdb->prefix}postmeta AS {$post_type}_meta ON ({$wpdb->prefix}posts.ID = {$post_type}_meta.post_id)"; 
				
				foreach ( $ascm_pg_category_types as $category_type ) :	
					$query_where_cat[] 	= "( {$post_type}_meta.meta_key = 'document_category' AND {$post_type}_meta.meta_value LIKE '%:\"{$category_type}\";%' ) ";
				endforeach;	
			else :
				$has_other_cptonomy_pt 	= 1;

				// revised
				$has_common_categories	= 1;
			endif;
		endforeach;
	endif;

	// Category types tax cptonomy
	if ( $has_other_cptonomy_pt ) :
		$query_from[]			= "INNER JOIN {$wpdb->prefix}postmeta ON ({$wpdb->prefix}posts.ID = {$wpdb->prefix}postmeta.post_id)"; 
		$common_categories		= join( '\', \'', $ascm_pg_category_types );
		$query_where_cat[] 		= "( {$wpdb->prefix}postmeta.meta_key = '_custom_post_type_onomies_relationship' AND {$wpdb->prefix}postmeta.meta_value IN ('{$common_categories}') ) ";
	endif;

	// Category types tax builtin
	if ( $has_common_categories ) :
	
		$query_from[]			= "INNER JOIN {$wpdb->prefix}term_relationships AS common ON ({$wpdb->prefix}posts.ID = common.object_id)"; 
		$common_categories		= join( '\', \'', $ascm_pg_category_types );
		$query_where_cat[] 		= "( common.term_taxonomy_id IN ('{$common_categories}') ) ";
	endif;

	$ascmpsrch = !empty( $_GET['ascmpsrch'] ) ? sanitize_text_field( $_GET['ascmpsrch'] ) : '';

	if ( !empty( $ascmpsrch ) ) :
		$query_search_keyword = "
		AND (
			(wp_posts.post_title LIKE '%{$ascmpsrch}%')
			OR 
			(wp_posts.post_excerpt LIKE '%{$ascmpsrch}%')
			OR 
			(wp_posts.post_content LIKE '%{$ascmpsrch}%')
		)";
	endif;
	

	// Build the FROM clause
	$query_from 				= ( !empty( $query_from ) ) ? join( ' ', $query_from ) : '';

	// Build the category WHERE clause
	if ( !empty( $query_where_cat ) ) :
		$query_where_cat 		= join(' OR ', $query_where_cat);
		$query_where_cat 		=  "AND ( {$query_where_cat} )";
	else :
		$query_where_cat 		= '';
	endif;


	// Build the postype WHERE clause
	$query_where_post_types		=  join( '\', \'', $post_types );
	$query_where_post_types 	=  "AND {$wpdb->prefix}posts.post_type IN ('{$query_where_post_types}')";


	$postgallery_maxnumofitems 	= isset($panel['data']['standard']['postgallery_maxnumofitems']) ? $panel['data']['standard']['postgallery_maxnumofitems'] : '9';
	$postgallery_maxnumofitems 	= (int)$postgallery_maxnumofitems;

	$postgallery_orderby		= isset($panel['data']['standard']['postgallery_orderby']) ? $panel['data']['standard']['postgallery_orderby'] : 'post_title';
	$postgallery_orderby		= ( $postgallery_orderby == 'date' ) ? 'post_date' : $postgallery_orderby;

	$postgallery_sortorder 		= isset($panel['data']['standard']['postgallery_sortorder']) ? $panel['data']['standard']['postgallery_sortorder'] : 'DESC';

	if ( get_query_var('paged') ) {
		$paged 	= get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged 	= get_query_var('page');
	} else {
		$paged 	= 1;
	}

	$offset 	= ( ( $paged - 1 ) * $postgallery_maxnumofitems );

	$all_results = $wpdb->get_results("
		SELECT 
			{$wpdb->prefix}posts.ID 
		FROM 
			{$wpdb->prefix}posts 
			{$query_from}
		WHERE 
			1 = 1
			{$query_where_cat}
			{$query_where_post_types}
			{$query_search_keyword}
			AND {$wpdb->prefix}posts.post_status = 'publish'
		GROUP BY 
			{$wpdb->prefix}posts.ID
	");
	
	$results = [];

	if ( !empty( $all_results ) ) {
		
		$res_ids = join( '\', \'', wp_list_pluck( $all_results, 'ID' ) );

		$results = $wpdb->get_results("
			SELECT 
				{$wpdb->prefix}posts.ID 
			FROM 
				{$wpdb->prefix}posts
			WHERE 
				{$wpdb->prefix}posts.ID IN ('{$res_ids}')
			ORDER BY 
				{$wpdb->prefix}posts.{$postgallery_orderby} {$postgallery_sortorder}
			LIMIT {$offset}, {$postgallery_maxnumofitems}
		");
	}

	return [
		'paginated' => $results,
		'all'		=> $all_results
	];
}

add_action( 'pdwp_donation_complete', 'wfc_sync_additional_fields_to_sf' );

/**
 * Sync additional data to Opportunity.
 *
 * @param array 	$donation 	Donation data.
 */
function wfc_sync_additional_fields_to_sf( $donation ) {

	if ( isset( $donation['donation']['tribute_gift'] ) && $donation['donation']['tribute_gift'] == 'on' && class_exists( 'Pronto_Ultimate_SF_Oauth' ) && ! empty( $donation['donation']['SalesforceResponse']['OpportunityId'] ) ) {

		$oauth = new Pronto_Ultimate_SF_Oauth( 'wfc-donation-wp/webforce-connect-donation.php' );

		$opp_id = $donation['donation']['SalesforceResponse']['OpportunityId'];
		$type = empty( $donation['donation']['tribute_type'] ) ? '' : $donation['donation']['tribute_type'];
		$fname = empty( $donation['donation']['tribute_first_name'] ) ? '' : $donation['donation']['tribute_first_name'];
		$lname = empty( $donation['donation']['tribute_last_name'] ) ? '' : $donation['donation']['tribute_last_name'];

		$data = array(
			'npsp__Tribute_Type__c' => 'Memorial',
			'npsp__Honoree_Name__c' => sprintf( '%s %s', $fname, $lname ),
		);

		$result = $oauth->api_request( 'data/v49.0/sobjects/Opportunity/' . $opp_id, $data, 'update' );
	}
}

/**
 * WP Admin fix meta boxes overlap
 */
add_action(
	'admin_head',
	function() {
		echo '<style>.edit-post-visual-editor>.block-editor__typewriter, .edit-post-visual-editor>.block-editor__typewriter>div, .edit-post-visual-editor>.block-editor__typewriter>div>.block-editor-writing-flow, .edit-post-visual-editor>.block-editor__typewriter>div>.block-editor-writing-flow>.block-editor-writing-flow__click-redirect { height: auto!important; }</style>'; // phpcs:ignore
	}
);

/**
 * WP Admin fix meta boxes overlap
 */
add_action(
	'admin_head',
	function() {
		echo '<style>.edit-post-visual-editor>.block-editor__typewriter, .edit-post-visual-editor>.block-editor__typewriter>div, .edit-post-visual-editor>.block-editor__typewriter>div>.block-editor-writing-flow, .edit-post-visual-editor>.block-editor__typewriter>div>.block-editor-writing-flow>.block-editor-writing-flow__click-redirect { height: auto!important; }</style>'; // phpcs:ignore
	}
);


/**
 *  Home Page Header Script
 */
function output_home_page_header_script() { 
	if ( ! is_front_page() ) {
		return;
	} ?>

	<?php echo get_theme_mod('header_scripts_home', ''); ?>
<?php
}
add_action('wp_head', 'output_home_page_header_script');

/**
 *  Home Page Footer Script
 */
function output_home_page_footer_script() { 
	if ( ! is_front_page() ) {
		return;
	} ?>

	<?php echo get_theme_mod('footer_scripts_home', ''); ?>
<?php
}
add_action('wp_footer', 'output_home_page_footer_script');

add_action( 'save_post', 'set_shadow_event_meta_category', 10,3 );
function set_shadow_event_meta_category( $post_id, $post, $update ) {

    // Only set for post_type = post!
    if ( 'events' !== $post->post_type ) {
        return;
    }
    
	$post_cat = wp_get_post_terms( $post_id, 'category', array( 'fields' => 'names' ) );

	$post_cat = implode( ", ", $post_cat );

	update_post_meta( $post_id, '_post_cat', $post_cat );
	update_post_meta( $post_id, 'post_category', $post_cat );
}

add_action( 'wfc_on_create_cookie', 'wfc_create_cookie_post' );
/**
 * Create cookie post when a new visitor visited the site.
 *
 * @param array $cookie_data Cookie data from cookie tracker.
 */
function wfc_create_cookie_post( $cookie_data ) {

	if ( ! is_admin() && ! wp_doing_ajax() ) {
		$existing_cookie_post = wfc_get_existing_cookie_post( $cookie_data['cookie'] );
		$cookie_post_id = 0;

		if ( $existing_cookie_post === false ) {

			$new_cookie_args = array(
				'post_type' => 'cookie',
				'post_status' => 'publish',
				'post_title' => 'Cookie - ' . $cookie_data['cookie']
			);

			$cookie_post_id = wp_insert_post( $new_cookie_args );

			update_post_meta( $cookie_post_id, '_cookie_tracker_id', $cookie_data['cookie'] );
		}
	}
}

add_action( 'template_redirect', 'wfc_track_viewed_pages' );
/**
 * Tracks visited pages using the WFC Cookie tracker from
 * WFC Toolkit.
 */
function wfc_track_viewed_pages() {

	global $post;

	if ( function_exists( 'get_field' ) && ! is_admin() && is_a( $post, 'WP_Post' ) ) {
		$field_name = 'viewed_pages';
		$current_dt = date( 'Y-m-d H:i:s' );
		$cookie_id = wfc_tracker()->get_cookie_id();

		$existing_cookie_post = wfc_get_existing_cookie_post( $cookie_id );
		$viewed_pages_val = get_field( $field_name, $existing_cookie_post );

		// Only add to viewed pages if the page is not added yet.
		if ( ! $viewed_pages_val || array_search( $post->ID, array_column( $viewed_pages_val, 'post_id' ) ) === false ) {

			$data = array(
				'post_type' => $post->post_type,
				'post_id' => $post->ID,
				'date_time' => $current_dt
			);

			add_row( $field_name, $data, $existing_cookie_post->ID );
		}
	}
}

add_action( 'template_redirect', 'wfc_track_audience_score' );
/**
 * Tracks audience score from visited post of any post type
 * that has audience taxonomy.
 */
function wfc_track_audience_score() {

	global $post;

	wfc_track_terms_of_post( $post, 'audience', 'audience_score' );

	remove_action( 'template_redirect', 'wfc_track_audience_score' );
}

add_action( 'template_redirect', 'wfc_track_category_score' );
/**
 * Tracks audience score from visited post of any post type
 * that has audience taxonomy.
 */
function wfc_track_category_score() {

	global $post;

	wfc_track_terms_of_post( $post, 'category', 'category_score' );

	remove_action( 'template_redirect', 'wfc_track_category_score' );
}

add_action( 'posts_where', 'wfc_modify_where_clause_for_acf_repeater' );
/**
 * Modify WHERE clause to allow meta_query for
 * ACF repeater fields. Mainly, viewed_pages repeater field.
 * 
 * @param string $where Where clause from WP_Query.
 * 
 * @return string
 */
function wfc_modify_where_clause_for_acf_repeater( $where ) {

	return str_replace( "= 'viewed_pages$", "LIKE 'viewed_pages%", $where );
}

add_action( 'add_meta_boxes', 'wfc_add_reporting_metabox' );
/**
 * Register metabox for Reporting/Insights.
 */
function wfc_add_reporting_metabox() {

	add_meta_box( 'reporting-insights', __( 'Reporting/Insights', 'manyrivers' ), 'wfc_reporting_metabox_display', 'post' );
}

add_action( 'wp_ajax_reporting_insights', 'wfc_reporting_insights_ajax_handler' );
/**
 * Ajax handler that returns the number of cookies that visited
 * a particular post.
 */
function wfc_reporting_insights_ajax_handler() {

	$id = absint( $_POST['post_id'] );
	$resp = array(
		'cookies' 		=> 0,
		'cookies_7d' 	=> 0,
		'cookies_30d' 	=> 0,
	);

	$last_7d = date( 'Y-m-d H:i:s', strtotime( '-7 day' ) );
	$last_30d = date( 'Y-m-d H:i:s', strtotime( '-30 day' ) );

	$cookies 		= wfc_get_cookies_that_visited( get_post($id) );
	$cookies_7d 	= wfc_get_cookies_that_visited( get_post($id), $last_7d );
	$cookies_30d 	= wfc_get_cookies_that_visited( get_post($id), $last_30d );

	$resp['cookies'] = count( $cookies );
	$resp['cookies_7d'] = count( $cookies_7d );
	$resp['cookies_30d'] = count( $cookies_30d );

	echo json_encode( $resp );
	die();
}

/**
 * Retargeting/Lookalike metabox display.
 * 
 * @param WP_Post $post Current post
 */
function wfc_reporting_metabox_display( $post ) {
	?>
	<style type="text/css">
		.reporting {
			border-bottom: solid 1px #eee;
		}

		.loader {
			position: absolute;
			display: flex;
			height: 100%;
			width: 100%;
			top: 0;
			left: 0;
			background-color: #ffffff;
			opacity: 0;
			visibility: hidden;
			align-items: center;
		}

		.loader .spinner {
			visibility: visible;
			margin: -20px auto 0;
			float: none;
		}

		.loading {
			opacity: 1;
			visibility: visible;
		}
	</style>
	<div class="reporting">
		<h4>Number of cookies that visited this <a href="<?php echo get_permalink( $post ); ?>">page</a></h4>
		<div>
			<p>Total: <strong><span class="cookies-total">0</span></strong></p>
			<p>Last 7 days: <strong><span class="cookies-7days">0</span></strong></p>
			<p>Last 30: <strong><span class="cookies-30days">0</span></strong></p>
		</div>
		<div class="loader loading">
			<span class="spinner"></span>
		</div>
	</div>
	<script type="text/javascript">
		(function($) {
			var params = {
				action: 'reporting_insights',
				post_id: <?php echo $post->ID; ?>,
				security: '<?php echo wp_create_nonce( 'reporting_insights_' . $post->ID ); ?>',
			};

			$.post(ajaxurl, params, function(res) {
				res = JSON.parse(res);

				console.log(res);
				$('.cookies-total').text(res.cookies);
				$('.cookies-7days').text(res.cookies_7d);
				$('.cookies-30days').text(res.cookies_30d);

				$('.loader').removeClass('loading');
			});
		})(window.jQuery);
	</script>
	<?php
}

add_action( 'gform_after_submission', 'wfc_create_sf_contact_when_registered' );
/**
 * Create contact in SF when a user signs up in newsletter.
 *
 * @param array $entry Gravity Forms entry.
 */
function wfc_create_sf_contact_when_registered( $entry ) {

	if ( class_exists( 'Pronto_Ultimate_SF_Oauth' ) ) {

		try {

			$cookie_id = wfc_tracker()->get_cookie_id();
			$cookie_post = wfc_get_existing_cookie_post( $cookie_id );

			$categories = get_field( 'category_score', $cookie_post );

			$mapping_str = get_theme_mod( 'interest_mapping', '' );
			$mappings = explode( ',', $mapping_str );
			$clean_mapping = array();

			$cookie_data_to_sf = array();

			if ( ! empty( $mappings ) ) {
				foreach ( $mappings as $map ) {
					$map_arr = explode( '=>', trim( $map ) );

					if ( count( $map_arr ) == 2 ) {
						$key = trim( $map_arr[0] );
						$val = trim( $map_arr[1] );

						$clean_mapping[$key] = $val;
					}
				}
			}

			if ( ! empty( $categories ) ) {
				foreach ( $categories as $cat ) {

					$sf_field = array_search( $cat['term'], $clean_mapping );

					if ( $sf_field !== false ) {
						$cookie_data_to_sf[$sf_field] = TRUE;
					}
				}
			}

			$oauth  = new Pronto_Ultimate_SF_Oauth( 'wfc-base-wp/pronto-base.php' );

			$f_name = rgar( $entry, '4' );
			$l_name = rgar( $entry, '5' );
			$email 	= rgar( $entry, '2' );

			$id = false;

			$query = sprintf( "SELECT Id, FirstName FROM Contact WHERE FirstName = '%s' AND LastName = '%s' AND npe01__HomeEmail__c = '%s'", $f_name, $l_name, $email );
			$result = $oauth->api_request( "data/v49.0/query/?q=" . urlencode( $query ), null, 'retrieve' );

			if ( $result['status_code'] === 200 && ! empty( $result['sf_response']['records'][0]['Id'] ) ) {

				$id = $result['sf_response']['records'][0]['Id'];

				$data = array(
					'AS_Newsletter__c' => FALSE,
					'npe01__Preferred_Email__c' => 'Personal',
					'AS_Expressed_Interest__c' => TRUE,
					'AS_Marketing_Interest__c' => date( 'Y-m-d' )
				);

				$data = array_merge( $cookie_data_to_sf, $data );

				$result = $oauth->api_request( 'data/v49.0/sobjects/Contact/' . $id, $data, 'update' );
			} else {

				$data = array(
					'FirstName' => $f_name,
					'LastName' => $l_name,
					'npe01__HomeEmail__c' => $email,
					'AS_Newsletter__c' => FALSE,
					'npe01__Preferred_Email__c' => 'Personal',
					'AS_Expressed_Interest__c' => TRUE,
					'AS_Marketing_Interest__c' => date( 'Y-m-d' )
				);

				$data = array_merge( $cookie_data_to_sf, $data );

				$result = $oauth->api_request( 'data/v49.0/sobjects/Contact/', $data, 'create' );
			}


		} catch( Exception $e ) {
			wp_die( 'WFC Base: ' . $e->getMessage() );
		}
	}
}