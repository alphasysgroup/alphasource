<?php

class Sidebar_Menu_Widget extends WP_Widget {
 
    function __construct() {
 
        parent::__construct(
            'sidebar-menu-widget',  // Base ID
            'Sidebar Menu Widget'   // Name
        );
 
        add_action( 'widgets_init', function() {
            register_widget( 'Sidebar_Menu_Widget' );
        });
 
    }
 
    public function widget( $args, $instance ) {

        $parent_submenu     = get_field( 'sidebar_menu' );
      
        if ( $parent_submenu ) : ?>
        <section class="widget widget_nav_menu">
            <div class="widget-wrap"> <?php 
                foreach( $parent_submenu as $child_submenu ) : 
                    
                    $menu_title     = $child_submenu['sidebar-title'];
                    $menu_name      = $child_submenu['menu_name'];

                    if( empty(  $menu_title )){
                        $menu_title = $menu_name;
                    }

                    if ( !empty($menu_name) ) :
                        echo '<h4 class="widget-title">' .$menu_title. '</h4>';
                        wp_nav_menu(
                            array(
                                'menu' => $menu_name
                            )
                        );
                    endif;  
                endforeach; ?>
            </div>
        </section> 
        <?php endif;
    }
}

$Sidebar_Menu_Widget = new Sidebar_Menu_Widget();