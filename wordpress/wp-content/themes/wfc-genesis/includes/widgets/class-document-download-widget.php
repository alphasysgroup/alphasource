<?php

class Document_Download_Widget extends WP_Widget {
 
    function __construct() {
 
        parent::__construct(
            'download-button-widget',  // Base ID
            'Download Button Widget'   // Name
        );
 
        add_action( 'widgets_init', function() {
            register_widget( 'Document_Download_Widget' );
        });
 
    }
 
 
    public function widget( $args, $instance ) {

		$children = get_field( 'download_button' );
		
		if ( $children ) : 
			foreach( $children as $child ) :

				if( !$child['label'] == ''){
					$icon	= !empty( $child['icon']['ID'] ) ? wp_get_attachment_image_src( $child['icon']['ID'], 'thumbnail' ) : '';
					$icon	= !empty( $icon[0] ) ? $icon[0] : '';
					$target	= $child['tab'][0] == 'true' ? 'target="_blank"' : '';
				
					echo "<a {$target} style='color: {$child['color']};border-color: {$child['color']}' href='{$child['link']}' class='download-button-widget main-div'>";
						echo "<div class='icon-container' style='outline-color: {$child['color']}; background-color: {$child['color']};'>";
							echo "<div class='icon-box' style='background-image: url({$icon})'></div>";
						echo "</div>";
						echo "<div class='label-container'>";
							echo "<p class='label h5'>{$child['label']}</p>";
						echo "</div>";
					echo "</a>";
				}
			endforeach;
		endif;
    }
}
$document_download_widget = new Document_Download_Widget();