<?php

// Remove sidebar in 404 page
remove_action('genesis_sidebar', 'genesis_do_sidebar');


add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

/**
 * Replace 404 content with custom message
 */
remove_action('genesis_loop', 'genesis_do_loop');
add_action('genesis_loop', function() {
	$section_404_button_label = get_theme_mod('section_404_button_label', 'GET IN TOUCH'); 
	$section_404_button_href = get_theme_mod('section_404_button_href', '/'); 

	$title	= __( 'We couldn’t find what you’re looking for!', 'wfc-genesis'); ?>

	<p class="h2"><?php echo $title; ?></p>

	<p><?php _e('The information you are looking for doesn’t exist in this location. Please try our search function or select one of the popular links below.', 'wfc-genesis'); ?></p> 
	<p><?php _e('Browse our site for cancer information, practical support, emotional support, research and information on how to get involved with Pancare.', 'wfc-genesis'); ?></p>  
	<p class="mb-3"><?php _e('If you are still unable to find the information you are seeking, please get in touch and we will see if we can help.', 'wfc-genesis'); ?></p>
	
	<a class="btn px-3 hbg2 bg3 mb-5" href="<?php echo $section_404_button_href; ?>"><?php echo $section_404_button_label; ?></a>
	<?php
});

genesis();