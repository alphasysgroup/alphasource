<?php
/*
 * Title: Pancare Donation Template
 * Desc: Pancare Donation Template
 * Date: April 26, 2018
 * Author : Junjie Canonio
 */
if ( ! defined( 'WPINC' ) ) { die; }
/* ========================================= Note ===============================================
* Template customizable data
* The array fields below are customizable and can be use to programmatically sort the fields and 
* programmatically set the grid class for the fields. 
* NOTE: Webforce Connect - Donation is using bootstrap css framework. Please refer to the bootstrap 
* documentation to fully use the grid class funtionality on the donation form. 
==============================================================================================*/
$field_grids = array(
	'donor_type' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_company' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_title' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_first_name' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_last_name' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_anonymous' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_email' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_phone' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_address' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_country' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_state' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_postcode' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_suburb' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_comment' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),	

	'payment_type_credit_card' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'payment_type_bank' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'card_number' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'ccv' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'name_on_card' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'expiry_month' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
	),
	'expiry_year' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
	),
	'pdwp_bank_code' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'pdwp_account_number' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'pdwp_account_holder_name' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
);


/* ========================================= Note ===============================================
* This is where you enqueue your styles and scripts. You can register your own and call the slug
* of your registered styles/scripts in here.
/* ==============================================================================================*/
/*
WFC_DonationFormRenderer::enqueue_styles_scripts(
	array(
		'wfc-BootstrapCDN-css',
	),
	array()
);
*/
//WFC_DonationCASAddressAutocomplete::setup_googleaddressautocomplete();

/* ========================================= Note ===============================================
* Template Donation form with fields
* Below are the important standard html and static function that is being use for the whole donation form. 
* NOTE: Webforce Connect - Donation already provides the needed functionality for donation and it is 
* highly recommended not to delete any of the static fucntions called on this template for it will
* surely cause major issues. 
==============================================================================================*/


$amount 	= !empty( $_GET['otr'] ) ? sanitize_text_field( $_GET['otr'] ) : 0;
$amount 	= !empty( $amount ) ? $amount : 0;
$isTribute	= !empty( $_GET['tribute_gift'] ) && $_GET['tribute_gift'] == 'on' ? true : false;
$honoree_fname = !empty( $_GET['tribute_fname'] ) ? sanitize_text_field( $_GET['tribute_fname'] ) : '';
$honoree_lname = !empty( $_GET['tribute_lname'] ) ? sanitize_text_field( $_GET['tribute_lname'] ) : '';
$tribute_text = sprintf( '%s %s', $honoree_fname, $honoree_lname );

$recurrence = !empty( $_GET['type'] ) ? sanitize_text_field( $_GET['type'] ) : 'one-off';
$recurrence = ( !empty( $recurrence ) && $recurrence == 'one-off' ) ? 'ONE-TIME' : 'MONTHLY'; ?>

<?php WFC_DonationFormRenderer::get_error($configs); ?>
<?php WFC_DonationFormRenderer::render_form_tag($configs,'start'); ?>
	<div class="pancare-donation-container">
		<div class="row no-gutters">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<h5 class="mb-3 text4 section-header-title"><?php echo __('Donation Summary', 'webforce-connect-donation'); ?></h5>
			</div>
		</div>
		<div class="row no-gutters last-row">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<h5 class="section-header-title">Amount: <span>$<?php echo $amount; ?></span></h5>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<h5 class="section-header-title">Donation Type: <span><?php echo $recurrence; ?></span></h5>
			</div>
			<?php if ( $isTribute ) : ?>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6">
				<h5 class="section-header-title">My donation is in memory of: <span><?php echo $tribute_text; ?></span></h5>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<div class="wfc_donation-amount-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Donation Amount', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_donation_amount($configs)?>
		<?php WFC_DonationFormRenderer::render_donation_tibutefield($configs)?>
	</div>	
	<div class="wfc_donation-frequency-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Donation Type', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_donation_type($configs)?>
	</div>	
	<div class="wfc_donation-donor-details-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span class="text4 section-header-title"><?php echo __('Your Details', 'webforce-connect-donation'); ?></span>
			</div>
		</div>

    <?php WFC_DonationFormRenderer::render_form_field($configs,$field_grids); ?>
	</div>
	<div class="wfc_donation-payment-gateway-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Payment Gateway', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_gateway($configs)?>
	</div>	
	<div class="wfc_donation-payment-details-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span class="text4 section-header-title"><?php echo __('Payment Details', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<div class="payment-details-loader-wrapper">
			<div class="custom-loader-container" style="display:<?php echo $_GET['payment_method'] == 'paypal' ? flex : none; ?>">
				<div class="custom-loader"></div>
			</div>
			<div class="custom-details-wrapper" style="display:<?php echo $_GET['payment_method'] == 'paypal' ? none : block; ?>">
				<?php WFC_DonationFormRenderer::render_payment_details($configs,$field_grids); ?>
			</div>			
		</div>
	</div>	
	
	<div class="wfc_donation-header-container-title row">
		<div class="col-12 col-sm-12 col-md-6 col-lg-6">
			<p class="mb-1">Donations over $2 or more are tax-deductible.</p>
			<p>By making a donation to the Pancare Foundation you agree to the <a href="/privacy-policy" target="_blank">privacy policy</a>.</p>
		</div>	
	</div>
	<div class="row">
		<div class="col-12 col-sm-12 col-md-6 col-lg-6">
			<?php WFC_DonationFormRenderer::render_google_captcha($configs); ?>
		</div>	
	</div>
	<div class="row">
		<div class="col-12 col-sm-12 col-md-6 col-lg-12">
			<?php WFC_DonationFormRenderer::render_submitbtn($configs); ?>
		</div>
	</div>
	<div class="custom-hidden-fields">
		<input type="hidden" name="tribute_gift" value="<?php echo isset( $_GET['tribute_gift'] ) ? $_GET['tribute_gift'] : 'off'; ?>">
		<input type="hidden" name="tribute_type" value="<?php echo isset( $_GET['tribute_type'] ) ? $_GET['tribute_type'] : ''; ?>">
		<input type="hidden" name="tribute_first_name" value="<?php echo isset( $_GET['tribute_fname'] ) ? $_GET['tribute_fname'] : ''; ?>">
		<input type="hidden" name="tribute_last_name" value="<?php echo isset( $_GET['tribute_lname'] ) ? $_GET['tribute_lname'] : ''; ?>">
	</div>
	<!--div id="inputnolabels"></div-->
	<?php WFC_DonationFormRenderer::system_hidden_fields($configs); ?>
<?php WFC_DonationFormRenderer::render_form_tag($configs,'end'); ?>
<?php 
/* ========================================= Note ===============================================
* Template custom JS script tag 
* It is recommended to properly add all of the custom JS that is being use for this template 
* inside the JS script tag below. 
==============================================================================================*/
?>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		let urlString = window.location.href,
			tempUrl = new URL(urlString),
			paymentMethod = tempUrl.searchParams.get('payment_method'),
			campaignId = tempUrl.searchParams.get('sf_campaign_id'),
			tributeGift = tempUrl.searchParams.get('tribute_gift'),
			gauId = tempUrl.searchParams.get('gau'),
			selector = '[wfc-don-gateway="Paypal"]',
			btnToClick = document.querySelectorAll(selector);

		if (paymentMethod == 'paypal') {
			let ccWrapper = $('.wfc_donation-payment-details-main-container').find('#stripe-card-number'),
				customLoader = $('.wfc_donation-payment-details-main-container .custom-loader-container'),
				customPaymentDetails = $('.wfc_donation-payment-details-main-container .custom-details-wrapper');
				findEl = setInterval(function() {
					if(ccWrapper) {
						customLoader.css('display', 'none');
						customPaymentDetails.css('display', 'block');
						let test = $(btnToClick).click();console.log(`zxcv`,btnToClick,test);
						clearInterval(findEl);
					}
				}, 2000);
		}
		
		donorTypeToggle();
		$('#donor_on_behalf').change(function() {
			donorTypeToggle();
		});
		
		function donorTypeToggle() {
			if( $('#donor_on_behalf').is(':checked') ) {
				$('.donor_company_container').show();
				$('#donor_type').val('business');
				$('#donor_company').removeAttr('disabled');
			} else {
				$('.donor_company_container').hide();
				$('#donor_type').val('individual');
				$('#donor_company').attr('disabled', 'disabled');
				$('#donor_company').val('');
			}
		}
		
		$("#donor_email").attr("pattern","[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$");
		
		if ( tributeGift && tributeGift == 'on' && campaignId ) {
			$("input[name='campaignId']").val(campaignId);
		}
		
		if ( tributeGift && tributeGift == 'on' && gauId ) {
			$("input[name='gauId']").val(gauId);
		}
		
	}); 
</script>
<style>
	.custom-loader-container{
        margin-bottom: 30px;
        transition: 200ms ease-out;
        justify-content: center;
    }
	.custom-loader {
        margin-top: 30px;
        border: 16px solid #f3f3f3;
        border-top: 16px solid <?php echo $configs['template_color']; ?> !important;
        border-radius: 50%;
        width: 150px;
        height: 150px;
        animation: spin 2s linear infinite;

        display: inline-block;
        vertical-align: middle;
        opacity: .7;
        filter: alpha(opacity=70);
        margin-left: 10px;
        transition: 200ms ease-out;
    }

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .custom-details-wrapper #wfc-paypal-button-container {
    	display: flex;
    	justify-content: center;
    }
    .wfc-donation-credit-card-wrapper #stripe-card-number,
    .wfc-donation-credit-card-wrapper #stripe-card-expiry,
    .wfc-donation-credit-card-wrapper #stripe-card-cvc {
    	background-color: transparent !important;
    }
</style>