<?php
/*
 * Title: Custom Post Gallery
 * Type: post_gallery
 * Settings: post_gallery
 * Desc: Default card for ASCM Panels recipe.
 * Date: April 5, 2019
 *
 * Varaiables:
 * $panel - Contains the data needed for the Penel.
 *
 * Static functions usages:
 * ASCM_Panels_Renderer::enqueue_style_scripts() - This is where you enqueue your styles and scripts. You can register your own and call the slug
 *
 *
 * This template can be overridden by copying it to yourtheme/ascm-templates/panels/recipes/default.php.
 * @package ascm-templates/panels/recipes/
 * @version 1.0.0.0
 */
	ASCM_Panels_Renderer::enqueue_styles_scripts(
		array(
			'bootstrap'
		),
		array(
			'ascm-panels-public-js',
		)
	);

	$panel_id 				= $panel['data']['post']->ID;

	$outerwrapclass 		= $panel['data']['standard']['outerwrapclass'];
	$innerwrapclass 		= $panel['data']['standard']['innerwrapclass'];

	$cp_data_source				= ( function_exists('get_field') ) ? get_field( 'cp_data_source', $panel_id ) : [];
	$cp_data_source				= ( !empty( $cp_data_source ) ) ? wp_list_pluck( $cp_data_source, 'cp_card_type', 'cp_post_type' ) : [];
	array_change_key_case( $cp_data_source, CASE_LOWER );
	
	$post_types  				= array_keys( $cp_data_source );

	$searchelement_config	= array(
		'search_fld' => array(
			'placeholder' => 'Search . . .'
		),
		'search_btn' => array(
			'label' => 'Search'
		)
	);

	$config = array(
		'pageinfo' => array(
			'label' => 'Page ',
			'class' => '',
		),
		'firstpage_btn' => array(
			'label' => 'FIRST',
			'class' => '',
		),
		'previouspage_btn' => array(
			'label' => 'PREV',
			'class' => '',
		),
		'pagenum_btn' => array(
			'range' => 3,
			'class' => '',
		),
		'nextpage_btn' => array(
			'label' => 'NEXT',
			'class' => '',
		),
		'lastpage_btn' => array(
			'label' => 'LAST',
			'class' => '',
		)
	); 

	if ( empty( $post_types ) ):
		return;
	endif; 
	
	$postgallery_maxnumofitems 	= isset($panel['data']['standard']['postgallery_maxnumofitems']) ? $panel['data']['standard']['postgallery_maxnumofitems'] : '9';
	$postgallery_maxnumofitems 	= (int)$postgallery_maxnumofitems;

	$q = custom_panels_dynamic_query( $panel );

	$posts = [];
	$posts['query'] 				= new stdClass();
	$posts['query']->max_num_pages = ceil( count( $q['all'] ) / $postgallery_maxnumofitems ); ?>

<section id="<?php echo 'ascm-panels-main-cont-'.$panel_id; ?>" class="panel container-fluid outer <?php echo 'ascm-panels-main-cont-'.$panel_id .' '.$outerwrapclass; ?>">
	<div class="wrapper inner">
		<div class="row">
			<div id="<?php echo 'ascm-panels-sub-cont-'.$panel_id; ?>" class="col-sm col-md col-lg <?php echo 'ascm-panels-sub-cont-'.$panel_id.' '.$innerwrapclass; ?>">
				
				<?php ASCM_Panels_Renderer::render_title( $panel ); ?>
				<?php ASCM_Panels_Renderer::render_content( $panel ); ?>

		        <div class="ascm-panels-postgallery-cont">
					<?php 
					
					ASCM_Panels_CPTQueries::render_postgallerysearchelements( $searchelement_config, $panel ); 

					$postgallery_itemsperrowlrg = isset( $panel['data']['standard']['postgallery_itemsperrowlrg'] ) ? $panel['data']['standard']['postgallery_itemsperrowlrg'] : 'fourperrow';
					$postgallery_itemsperrowmed = isset( $panel['data']['standard']['postgallery_itemsperrowmed'] ) ? $panel['data']['standard']['postgallery_itemsperrowmed'] : 'twoperrow';

					if ( $postgallery_itemsperrowlrg == 'fourperrow' ) {
						$grid_class_lrg = 'col-lg-3 .col-xl-3';
					} elseif ( $postgallery_itemsperrowlrg == 'threeperrow' ) {
					 	$grid_class_lrg = 'col-lg-4 col-xl-4';
					} elseif ( $postgallery_itemsperrowlrg == 'twoperrow' ) {
					 	$grid_class_lrg = 'col-lg-6 col-xl-6';
					} else {
						$grid_class_lrg = 'col-lg-12 col-xl-12';
					}

					if ( $postgallery_itemsperrowmed == 'twoperrow' ) {
						$grid_class_med = 'col-sm-6 col-md-6';
					} else {
						$grid_class_med = 'col-sm-12 col-md-12';
					}

					$grid_class = 'col-12 '.$grid_class_med.' '.$grid_class_lrg; ?>

					<div id="<?php echo 'ascm-panels-postgallerylist-'.$panel_id; ?>" class="row ascm-panels-postgallerylist-cont <?php echo 'ascm-panels-postgallerylist-'.$panel_id; ?>">
						<?php 

						if ( !empty( $q['paginated'] ) ) :
							foreach ( $q['paginated'] as $key => $result ) :

								$value 				= get_post( $result->ID );
								
								$post_id 			= isset( $value->ID ) ? $value->ID : '';
								$post_title 		= isset( $value->post_title ) ? $value->post_title : '';
								$post_title 		= mb_strimwidth( $post_title, 0, 30, '...');
								$post_thumbnail_url = get_the_post_thumbnail_url( $post_id, 'full');
								$post_thumbnail_url = !empty( $post_thumbnail_url ) ? $post_thumbnail_url : 'https://via.placeholder.com/600x400';
								$post_content 		= isset( $value->post_content ) ? $value->post_content : '';
								$post_content 		= wp_trim_words( $post_content, 20 );
								$post_permalink 	= get_permalink( $post_id ); 
								$individual_pt 		= get_post_type( $post_id ); ?>

								<div id="<?php esc_html_e( 'ascm-panels-postgallerylist-item-'.$post_id, 'ascm' ); ?>" class="ascm-panels-postgallerylist-item <?php esc_html_e($grid_class, 'ascm'); ?>" post-id="<?php esc_html_e( $post_id, 'ascm' ); ?>">
									<?php 

									if ( isset( $cp_data_source[ $individual_pt ] ) && function_exists ('wfc_get_card') ) :
										echo wfc_get_card( $cp_data_source[ $individual_pt ], $post_id);

									else : ?>

										<div class="ascm-panels-postgallerylist-item-sub-cont">
											<div class="ascm-panels-postgallerylist-item-featuredimg"><img src="<?php esc_html_e($post_thumbnail_url, 'ascm'); ?>"/></div>
											<div class="ascm-panels-postgallerylist-item-info">
												<div class="ascm-panels-postgallerylist-item-title"><span><?php esc_html_e($post_title, 'ascm'); ?></span></div>
												<div class="ascm-panels-postgallerylist-item-content"><span><?php esc_html_e($post_content, 'ascm'); ?></span></div>
												<div class="ascm-panels-postgallerylist-item-redirect"><a href="<?php esc_html_e($post_permalink, 'ascm') ?>">Read More</a> </div>
											</div>
										</div>

									<?php 
									endif; ?>

								</div> 
								<?php

							endforeach;
						endif; ?> 
						
					</div>

					<?php
						if ( !empty( $q['all'] ) ) :
							ASCM_Panels_CPTQueries::render_postgallerypaginationelements( $posts, $config ); 
						endif; 
					?>
		        </div>
			</div>
		</div>
	</div>
</section>

