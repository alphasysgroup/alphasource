<?php
/*
 * Title: Dynamic Post Gallery
 * Type: post_gallery
 * Settings: post_gallery
 * Desc: Dynamic post gallery that is based on cookie tracker.
 * Date: February 18, 2021
 *
 * Varaiables:
 * $panel - Contains the data needed for the Penel.
 *
 * Static functions usages:
 * ASCM_Panels_Renderer::enqueue_style_scripts() - This is where you enqueue your styles and scripts. You can register your own and call the slug
 *
 *
 * This template can be overridden by copying it to yourtheme/ascm-templates/panels/recipes/default.php.
 * @package ascm-templates/panels/recipes/
 * @version 1.0.0.0
 */

global $post;

$exclude = array();

if ( is_single() && ! empty( $post ) ) {
	$exclude = array( $post->ID );
}

ASCM_Panels_Renderer::enqueue_styles_scripts(
	array(
		'bootstrap'
	),
	array(
		'ascm-panels-public-js',
	)
);

$panel_id 				= $panel['data']['post']->ID;

$outerwrapclass 		= $panel['data']['standard']['outerwrapclass'];
$innerwrapclass 		= $panel['data']['standard']['innerwrapclass'];

$data_source			= ( function_exists('get_field') ) ? get_field( 'dpg_data_source', $panel_id ) : [];
$data_source			= ( !empty( $data_source ) ) ? wp_list_pluck( $data_source, 'card_type', 'post_type' ) : [];
array_change_key_case( $data_source, CASE_LOWER );

$post_types = array_keys( $data_source );

$max_items 	= isset( $panel['data']['standard']['postgallery_maxnumofitems'] ) ? absint( $panel['data']['standard']['postgallery_maxnumofitems'] ) : 3;

if ( ! empty( $post_types ) ) :

	// Process post overrides if there's any.
	$overrides = get_field( 'post_override', $panel_id );
	$overrides = empty( $overrides ) ? array() : array_map( 'absint', explode( ',', $overrides ) );

	$overrides_p = array();
	$items = array();

	if ( ! empty( $overrides ) ) {

		$overrides_args = array(
			'post_type' => $post_types,
			'post_status' => 'publish',
			'posts_per_page' => $max_items,
			'post__in' => array_diff( $overrides, $exclude )
		);

		if ( ! empty( $exclude ) ) {
			$overrides_args['post__not_in'] = $exclude;
		}

		$overrides_query = new WP_Query( $overrides_args );
		$overrides_p = $overrides_query->get_posts();

		$items = array_merge( $items, $overrides_p );
	}

	$overrides_count = count( $overrides_p );

	// Get items based on cookies if post overrides is not enough.
	if ( $overrides_count < $max_items ) {

		$exclude = array_merge( $exclude, wp_list_pluck( $overrides_p, 'ID' ) );

		$posts_per_cookie = wfc_get_posts_by_cookie_scores( $post_types, $max_items - $overrides_count, $exclude );
		$items = array_merge( $items, $posts_per_cookie );
	}

	$item_count = count( $items );

	// Get additional posts if items is less than $max_items set in panel.
	if ( $item_count < $max_items ) {

		$exclude = array_merge( $exclude, wp_list_pluck( $posts_per_cookie, 'ID' ) );

		$filler_post_args = array(
			'post_type' => $post_types,
			'posts_per_page' => $max_items - count( $items ),
			'post_status' => 'publish',
		);

		if ( ! empty( $exclude ) ) {
			$filler_post_args['post__not_in'] = $exclude;
		}

		$filler_post = new WP_Query( $filler_post_args );

		$items = array_merge( $items, $filler_post->get_posts() );
	}
	?>

	<section id="<?php echo 'ascm-panels-main-cont-'.$panel_id; ?>" class="panel container-fluid outer <?php echo 'ascm-panels-main-cont-'.$panel_id .' '.$outerwrapclass; ?>">
		<div class="wrapper inner">
			<div class="row">
				<div id="<?php echo 'ascm-panels-sub-cont-'.$panel_id; ?>" class="col-sm col-md col-lg <?php echo 'ascm-panels-sub-cont-'.$panel_id.' '.$innerwrapclass; ?>">
					
					<?php ASCM_Panels_Renderer::render_title( $panel ); ?>
					<?php ASCM_Panels_Renderer::render_content( $panel ); ?>

			        <div class="ascm-panels-postgallery-cont">
						<?php

						$postgallery_itemsperrowlrg = isset( $panel['data']['standard']['postgallery_itemsperrowlrg'] ) ? $panel['data']['standard']['postgallery_itemsperrowlrg'] : 'fourperrow';
						$postgallery_itemsperrowmed = isset( $panel['data']['standard']['postgallery_itemsperrowmed'] ) ? $panel['data']['standard']['postgallery_itemsperrowmed'] : 'twoperrow';

						if ( $postgallery_itemsperrowlrg == 'fourperrow' ) {
							$grid_class_lrg = 'col-lg-3 .col-xl-3';
						} elseif ( $postgallery_itemsperrowlrg == 'threeperrow' ) {
						 	$grid_class_lrg = 'col-lg-4 col-xl-4';
						} elseif ( $postgallery_itemsperrowlrg == 'twoperrow' ) {
						 	$grid_class_lrg = 'col-lg-6 col-xl-6';
						} else {
							$grid_class_lrg = 'col-lg-12 col-xl-12';
						}

						if ( $postgallery_itemsperrowmed == 'twoperrow' ) {
							$grid_class_med = 'col-sm-6 col-md-6';
						} else {
							$grid_class_med = 'col-sm-12 col-md-12';
						}

						$grid_class = 'col-12 '.$grid_class_med.' '.$grid_class_lrg; ?>

						<div id="<?php echo 'ascm-panels-postgallerylist-'.$panel_id; ?>" class="row ascm-panels-postgallerylist-cont <?php echo 'ascm-panels-postgallerylist-'.$panel_id; ?>">
							<?php 

							if ( ! empty( $items ) ) :
								foreach ( $items as $item ) :
									
									$post_thumbnail_url = get_the_post_thumbnail_url( $item->ID, 'full');
									$post_thumbnail_url = empty( $post_thumbnail_url ) ? 'https://via.placeholder.com/600x400' : $post_thumbnail_url;

									?>

									<div id="<?php esc_html_e( 'ascm-panels-postgallerylist-item-'.$item->ID, 'ascm' ); ?>" class="ascm-panels-postgallerylist-item <?php esc_html_e($grid_class, 'ascm'); ?>" post-id="<?php esc_html_e( $item->ID, 'ascm' ); ?>">
										<?php 

										if ( isset( $data_source[$item->post_type] ) && function_exists( 'wfc_get_card' ) ) :
											echo wfc_get_card( $data_source[$item->post_type], $item->ID );
										else : ?>
											<div class="ascm-panels-postgallerylist-item-sub-cont">
												<div class="ascm-panels-postgallerylist-item-featuredimg"><img src="<?php echo $post_thumbnail_url; ?>"/></div>
												<div class="ascm-panels-postgallerylist-item-info">
													<div class="ascm-panels-postgallerylist-item-title"><span><?php esc_html_e( mb_strimwidth( $item->post_title, 0, 30, '...'), 'wfc-genesis' ); ?></span></div>
													<div class="ascm-panels-postgallerylist-item-content"><span><?php esc_html_e( wp_trim_words( $item->post_content, 20 ), 'wfc-genesis' ); ?></span></div>
													<div class="ascm-panels-postgallerylist-item-redirect"><a href="<?php echo get_permalink( $item->ID ); ?>">Read More</a> </div>
												</div>
											</div>
										<?php 
										endif; ?>

									</div> 
									<?php

								endforeach;
							endif; ?> 
							
						</div>
			        </div>
				</div>
			</div>
		</div>
	</section>
<?php else : ?>
	<p style="text-align: center;">Could not determine which post type to display.</p>
<?php endif; ?>