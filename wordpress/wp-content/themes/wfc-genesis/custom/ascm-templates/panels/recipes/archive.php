<?php
/*
 * Title: Custom Archive Panel
 * Type: custom
 * Desc: Recent Posts card for ASCM Panels recipe.
 * Date: April 22, 2020
 *
 * Varaiables:
 * $panel - Contains the data needed for the Panel.
 *
 * Static functions usages:
 * ASCM_Panels_Renderer::enqueue_style_scripts() - This is where you enqueue your styles and scripts. You can register your own and call the slug
 * ASCM_Panels_Helper::get_recentposts($panel) - This function fetches and returns a list of available recent post base on the settings set for the Panel.
 * ASCM_Panels_Renderer::render_title($panel) - This function renders the title of the panel.
 * ASCM_Panels_Renderer::render_content($panel) - This function renders the content of the panel.
 * ASCM_Panels_Renderer::render_recent_posts_list($panel, $recent_posts) - This function renders the list of recent post base on the data provided by the "ASCM_Panels_Helper::get_recentposts($panel)" function and panel settings.
 *
 * This template can be overridden by copying it to yourtheme/ascm-templates/panels/recipes/recent_posts.php.
 * 
 * @since 1.1.7
 */

$panel_id		= $panel['data']['post']->ID;
$img 			= wfc_get_image( $panel_id, 'featured',  'large' );

$cp_data_source	= ( function_exists('get_field') ) ? get_field( 'cp_data_source', $panel_id ) : [];
$cp_data_source	= ( !empty( $cp_data_source ) ) ? wp_list_pluck( $cp_data_source, 'cp_card_type', 'cp_post_type' ) : [];
array_change_key_case( $cp_data_source, CASE_LOWER );

$post_types  	= array_keys( $cp_data_source );

$img_pos  		= get_post_meta( $panel_id, 'ap_image_position', 1 );
$img_pos  		= ( !empty( $img_pos ) ) ? $img_pos : '';

$content_class	= ( $img_pos == 'left' ) ? 'order-md-1' : 'order-md-0';
$img_class		= ( $img_pos == 'left' ) ? 'order-md-0' : 'order-md-1';

$outerwrapclass = $panel['data']['standard']['outerwrapclass'];
$innerwrapclass = $panel['data']['standard']['innerwrapclass'];

global $wpdb; ?>
<section id="<?php echo esc_attr( 'panel-' . $panel_id ); ?>" class="container-fluid outer panel-archive<?php echo empty( $outerwrapclass ) ? '' : ' ' . $outerwrapclass ; ?>">
	<div class="wrapper inner<?php echo empty( $innerwrapclass ) ? '' : ' ' . $innerwrapclass ; ?>">
		<div class="row">
			<div class="col-sm col-md col-lg">
				<div class="row panel-archive-head">
					<div class="col-sm col-md-6 col-lg-6 <?php echo $content_class; ?> order-1">
						<div class="panel-archive-content">
							<?php ASCM_Panels_Renderer::render_title( $panel ); ?>
							<?php ASCM_Panels_Renderer::render_content( $panel ); ?>
						</div>
					</div>
					<div class="col-sm col-md-6 col-lg-6 <?php echo $img_class; ?> order-0">
						<div style="background-image: url(<?php echo $img; ?>);" class="panel-archive-img panel-bg-img"></div>
					</div>
				</div>
				<div class="row panel-archive-cards">
				<?php
					
					if ( empty( $post_types ) ):
						return;
					endif; 
					
					$panel['data']['standard']['postgallery_maxnumofitems'] = 3;
					$panel['data']['standard']['postgallery_sortorder'] 	= 'ASC';
					$panel['data']['standard']['postgallery_orderby']		= 'menu_order';

					$q = custom_panels_dynamic_query( $panel );

					if ( !empty( $q['paginated'] ) ):
						global $post;

						foreach( $q['paginated'] as $result ) :
							$post 			= get_post( $result->ID );

							setup_postdata( $post );

							$post_id 		= get_the_ID();
							$individual_pt 	= get_post_type( $post_id );

							if ( isset( $cp_data_source[ $individual_pt ] ) ) :
								echo '<div class="col-sm col-md-4 col-lg-4 panel-archive-item">';

								echo wfc_get_card( $cp_data_source[ $individual_pt ], $post_id ); 
									
								echo '</div>';
							endif;

						endforeach;

						wp_reset_postdata();
					endif; ?>
				</div>
			</div>
		</div>
	</div>
</section>
