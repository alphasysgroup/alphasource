jQuery( document ).ready( function( $ ) { 
	let btn = $('#back-to-top')

	$(window).scroll(function() {       
        if ($(window).scrollTop() > 300) {
            btn.addClass('show');
        } else {
            btn.removeClass('show');
        }
	});
	
    $('#back-to-top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
	});

	// $.each($('.wfc_donation-donor-details-container :input'), function(index, element){
	// 	var contentField		= $(this);
	// 	console.log(contentField);
	// 	var isRequired			= $(element).prev('label').find('span').length;
	// 	var placeholder			= contentField.attr('wfc-don-label');
	// 	var placeholder			= (isRequired) ? placeholder + ' *' : placeholder;
	// 	contentField.attr('placeholder', placeholder );
	// });
	// $.each($('.wfc_donation-payment-details-container :input'), function(index, element){
	// 	var contentField		= $(this);
	// 	var placeholder			= contentField.attr('wfc-don-label') + ' *';
	// 	contentField.attr('placeholder', placeholder );
	// });

	// $('#expiry_month option:first-child').html('Exp. Month *');
	// $('#expiry_year option:first-child').html('Exp. Year *');
	
	$('#camp_donation-amount-level-container label').on('click', function() {
		$('#camp_donation-amount-level-container label').removeClass('bg2').addClass('text2');
		$(this).addClass('bg2').removeClass('text2');
	});

	$('.camp-frequency-radio-container ').on('click', function() {
		$('.camp-frequency-radio-container ').removeClass('camp-frequency-active');
		$(this).addClass('camp-frequency-active');
	});
	
	//custom script goes here

	/*
	 * Toggle full screen popup search.
	 */

	$( '.menu-search-popup > a, .search-popup-close, .btn-menu-search' ).on( 'click', function( e ) {
		e.preventDefault();
		$('body').toggleClass('search-popup-open');
		var search_popup = $( '.search-popup' );
		search_popup.fadeToggle( function() {
			search_popup.find( 'input' ).focus();
		} );
	} );

	/* Toggle menu */
	$( '.nav-toggle' ).on('click', function(){
		$('body').toggleClass('offcanvas-menu-open');
		setToggleMenuState();
	});

	$('.site-offcanvas .sub-menu').collapse({
		toggle: true
	});
	
	function setToggleMenuState(){
		if( $(window).width() <= 767.98 ) {
			$('.site-offcanvas .sub-menu').collapse('hide');
			$('.menu-item-has-children').removeClass('expanded');
		} else {
			$('.site-offcanvas .sub-menu').collapse('show');
		}
	}
	
	$( window ).resize(function(){        
		setToggleMenuState();
	});

	var menuPointer = "<i class='sub-menu-pointer far fa-angle-down'></i>";
	$('.site-offcanvas .menu > .menu-item').each(function(i, e){
		if ( $(this).find('ul').length ) {
			$(this).prepend(menuPointer);
		}
	})
	
	$('.site-offcanvas .sub-menu-pointer').on('click', function(){
		$(this).parent().toggleClass('expanded');
		$(this).parent().find('.sub-menu').collapse('toggle');
	})

	let linkIcon = tags_links_icon.links_icon,
		tagIcon = tags_links_icon.tags_icon,
		iconTargets = [ '.tags', '.tag', '.links', '.link' ];

	iconTargets.forEach(iconTarget => {
		let target = $('.site-container').find(iconTarget);

		if(target.length > 0) {
			if(iconTarget == '.tag')
				$(target.selector).prepend(tagIcon);
			if(iconTarget == '.link')
				$(target.selector).prepend(linkIcon);
			if(iconTarget == '.tags')
				$(`${target.selector} a`).prepend(tagIcon);
			if(iconTarget == '.links')
				$(`${target.selector} a`).prepend(linkIcon);
		}
	});



	/** sticky submenu */
		// When the user scrolls the page, execute myFunction
		// window.onscroll = function() { stickySubmenuRecalculate() };

		// // Get the header
		// var header = document.getElementById("submenu-section");

		// // Get the offset position of the navbar
		// var sticky = header.offsetTop;

		// // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
	function recalculateSizeOnScroll() {
		// var scrollTop     	= $(window).scrollTop(),
		// 	elementOffset 	= $('.submenu-section').offset().top,
		// 	distance		= (elementOffset - scrollTop) || 0,
		// 	topbarHeight	= $('.custom-topbar').outerHeight() || 0,
		// 	menuHeight		= $('.main-header-menu').outerHeight() || 0;

		// console.log(scrollTop, elementOffset, distance);

		// if (distance <=  (topbarHeight + menuHeight )) {
		// 	if ( !$('.submenu-section').hasClass('submenu-sticky') ) {	
		// 		$('.submenu-section').addClass('submenu-sticky');
		// 	}
		// } else {
		// 	if ( $('.submenu-section').hasClass('submenu-sticky') ) {	
		// 		$('.submenu-section').removeClass('submenu-sticky');
		// 	}
		// }

		/** Recalculate  */
		// When the user scrolls the page, execute myFunction
		// Get the header
		

	}

	$(document).scroll( function() {
		let scrollWatcher 	= document.getElementById("submenu-section-scroll-watch"),
			header 			= document.getElementById("submenu-section"),
			calloutWrapper	= document.getElementById("callout-wrapper"),
			siteInner 		= document.getElementById("submenu-dynamic-spacer");
		
		if (scrollWatcher) {
			// let navMenuHeight 		= ( ( $('.custom-topbar').outerHeight() || 0 ) + ($('.main-header-menu').outerHeight() || 0) ),
			let navMenuHeight 		= $('.site-header').outerHeight() || 0,
				stickyNavbarHeight 	= $('.submenu-section').outerHeight() || 0;

			// Get the offset position of the navbar
			let sticky = scrollWatcher.offsetTop - navMenuHeight;

			// Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
			if (window.pageYOffset > sticky) {
				header.classList.add("submenu-sticky");
				
				header.style.top = `${navMenuHeight}px`;

				siteInner.style.paddingTop = `${stickyNavbarHeight}px`;
				
				calloutWrapper.style.top = `calc( 100% + ${(stickyNavbarHeight + 100)}px )`;
			} else {
				header.classList.remove("submenu-sticky");
				siteInner.style.paddingTop = 0;
				
				calloutWrapper.style.top = `100%`;
			}
		}
	} );

	
	$('.callout-side-slide').on( 'click', function() {
		$(this).toggleClass('open');
		$(this).parent().toggleClass('open');
	});

	$('.sticky-submenu-mobile').on('click', function(){
		$(this).parents('.submenu-section').toggleClass('mobileActive');
	});

	// Custom Disable of Paypal Button on Monthly Payment Selection
	$('.camp-frequency-container .camp-frequency-radio-container[for="monthly"]').on('click', function() {
		$('.camp-payment-type-container .paypal-btn-overlay').css('display', 'block');
		$('.camp-payment-type-container .paypal-button').css({'filter': 'grayscale(100%)', 'pointer-events': 'none'});
	});
	$('.camp-frequency-container .camp-frequency-radio-container[for="one-off"]').on('click', function() {
		$('.camp-payment-type-container .paypal-btn-overlay').css('display', 'none');
		$('.camp-payment-type-container .paypal-button').css({'filter': 'none', 'pointer-events': 'auto'});
	});

	/* Post gallery search */
	$(".ascm-panels-postgallerysearch-sub-cont > input").on('keyup', function (e) {
		if (e.key === 'Enter' || e.keyCode === 13) {
			let button = $(this).parent().children('button');

			button.trigger('click');
		}
	});
});