(function($){

    $(document).ready(function(){
		
		var camp_amount = $('#camp_amount').val();
		var form = $('#camp-donation-details');
		
		tributeToggle();
		$('#tribute_gift').change(function() {
			tributeToggle();
		});
		
		function tributeToggle() {
			if( $('#tribute_gift').is(':checked') ) {
				$('.tribute-field-container').show();
				$('#tribute_fname').prop('required',true);
				$('#tribute_lname').prop('required',true);
			} else {
				$('.tribute-field-container').hide();
				$('#tribute_fname').prop('required',false);
				$('#tribute_lname').prop('required',false);
			}
		}
		
		$('#camp_donation-amount-level-container > div').on('click', function() {
			var t = $(this).find('input[type=radio]');
			
			
			if ( $(this).hasClass('other-amount-box') ) {
				$('.other-amt-container').removeClass('d-none');
				$('#camp-best-gift').val('').prop('required', true).focus();
				
			} else {
				$('#camp-best-gift').val(t.val()).prop('required', false);
				$('.other-amt-container').addClass('d-none');
			}
		});
		
		$(':submit').on('click',  function(e){
			var querystr = $(this).attr('querystr');
			form.append('<input type="hidden" value="'+querystr+'" name="payment_method">');
			$('#camp_donation-amount-level-container input').attr('disabled', 'disabled');
			$('#camp-payment-type-container input').attr('disabled', 'disabled');
		});
		

		
    });
	
})(jQuery);