<?php
/**
 *  Submenu Section
 * 
 */ 
global $post;

$url_referer		= rtrim(wp_get_referer(),"/");
$parse_referer  	= parse_url( $url_referer, PHP_URL_PATH);
$pathmainFragments	= explode('/', $parse_referer);
$parse_referer_end	    = end($pathmainFragments);

$id		        = wfc_get_page_id();
$show_submenu   = get_post_meta( $id, 'show_submenu', true ); 
$menu_as        = get_post_meta( $id, 'menu_as', true ); 
$menu_name      = get_post_meta( $id, 'menu_name', true ); 

$menu_items     = [];

if ( $menu_as == 'Wordpress Menu' ) {
    $raw_menu_items = wp_get_nav_menu_items( $menu_name );

    if ( !empty( $raw_menu_items ) && is_array( $raw_menu_items ) ) {

        foreach ( $raw_menu_items as $raw_menu_item ) {
            $menu_items[ $raw_menu_item->ID ] = array(
                'url'       => $raw_menu_item->url,
                'title'     => $raw_menu_item->title,
                'id'        => $raw_menu_item->ID,
                'class'     => $raw_menu_item->object_id,
                'target'    => $raw_menu_item->target
            );
        }
    }
    
} elseif ( $menu_as == 'Child pages' ) {
	$child_pages = get_children( array(
		'post_parent' => $post->ID,
		'post_type'   => $post->post_type,
		'post_status' => 'publish',
		'orderby'     => 'menu_order',
		'order'       => 'ASC',
		'numberposts' => -1,
    ) );

    foreach ( $child_pages as $child_page ) {

        $menu_items[ $child_page->ID ] = array(
            'url'   => get_the_permalink( $child_page->ID ),
            'title' => $child_page->post_title,
            'id'    => $child_page->ID,
            'class' => $child_page->ID
        );
    }
} elseif ( $menu_as == 'Sibling pages' ) {
	$child_pages = get_children( array(
		'post_parent' => $post->post_parent,
        'post_type'   => $post->post_type,
		'post_status' => 'publish',
		'orderby'     => 'menu_order',
		'order'       => 'ASC',
		'numberposts' => -1,
    ) );  
    foreach ( $child_pages as $child_page ) {
        $menu_items[ $child_page->ID ] = array(
            'url'   => get_the_permalink( $child_page->ID ),
            'title' => $child_page->post_title,
            'class'    => $child_page->ID,
        );
    }
    
}

$text_color     = get_theme_mod( 'section_sticky_submenu_text_color', '#fff' );
$bg_color       = get_theme_mod( 'section_sticky_submenu_bg_color', '#3D465B' );

if ( $show_submenu == '1' && !empty( $menu_items ) ) : ?>

    <section id="submenu-section-scroll-watch"></section>
    <section id="submenu-section" class="container-fluid outer submenu-section" style="background-color: <?php echo $bg_color; ?>">
        <div class="wrapper inner"> 
            <div class="row">
                <div class="col-sm col-md col-lg submenu-col">
                    <ul class="menu">
                    <?php
                        foreach ( $menu_items as $menu_item_k => $menu_item ) :
  
                            $url_trim 			= rtrim($menu_item['url'],"/");
                            $parse_url_trim     = parse_url($url_trim, PHP_URL_PATH);
                            $pathFragments	 	= explode('/', $parse_url_trim);
                            $parse_url 	        = end($pathFragments);
                            
                            $url_param			= isset($_GET['stories_cat']) ? $_GET['stories_cat'] : '';
                            $targe_blank        = $menu_item['target'] == '_blank' ? 'target="_blank"' : '';

                            $active = 'no-active';

                            if( (is_singular( 'stories' ) &&  $parse_referer_end  ==  $parse_url) || is_page($menu_item['class']) || is_single($menu_item['class']) || $url_param == $parse_url ){
                                $active = 'active';
                            }
                            
                            echo "<li class=' {$active} menu-item menu-item-{$menu_item_k}'><a class='submenu-anchor' {$targe_blank} href='{$menu_item['url']}'>{$menu_item['title']}</a></li>";
                        endforeach; ?>
                    </ul>
                    <div class="sticky-submenu-mobile">
                        
                        <button class="btn btn-link" style="color: <?php echo $text_color; ?>">PAGE MENU <i class="fas fa-bars"></i></buton>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="submenu-dynamic-spacer"></section>
    <style>
        .active .submenu-anchor{
            font-weight: bold;
        }
        .submenu-section .submenu-anchor {
            color: <?php echo $bg_color; ?>;
        }    
        @media (min-width: 62rem) {
            .submenu-section .submenu-anchor {
                color: <?php echo $text_color; ?>;
            }  
        }
    </style>
<?php 
endif;