<?php
global $post;

$wfc_rev_sc = get_post_meta( $post->ID, 'wfc_rev_sc', true ); ?>
<section class="container-fluid outer hero rev-slider" >
    <div class="wrapper inner">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
            <?php add_revslider( $wfc_rev_sc ); ?>
            </div>
        </div>
    </div>
</section>
