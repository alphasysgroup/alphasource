<?php

global $post;

$url 				= rtrim( wp_get_referer(),"/" );
$parse_referer  	= parse_url( $url, PHP_URL_PATH );
$path_fragments	 	= explode( '/', $parse_referer );
$parse_referer_end 	= end( $path_fragments );
$url_param			= isset( $_GET['stories_cat']) ? $_GET['stories_cat'] : $parse_referer_end;
$url_param			= !empty( $url_param ) ? $url_param : 'stories-of-hope';
$post_id 			= $post->ID;

$posts = get_posts( array(
    'post_type'         => 'stories',
    'post_status'       => 'publish', 
    'posts_per_page'    => -1,
	'order'            	=> 'DESC',
	'tax_query' => array(
        array(
            'taxonomy' => 'category',
            'field'    => 'slug',
            'terms'    => array( $url_param ),
        ),
    ),
) );

$archive_url = (false) ? '' : '';

if ( count( $posts ) > 1 ) : ?>
	<div class="row">
		<div class="col"style="text-align: center; color: #289F74">
			<hr style="border-top: 2px solid rgb(0 0 0 / 25%);"> 
			<p class="h2" style="margin: 40px 0;">More Stories of Hope</p>
		</div>
	</div>
	<?php
	
	$css_parent		= count($posts) == 2 ? 'justify-content: center' : '';

	echo "<div class='row story-row-parent' style='{$css_parent}'>";

	$first_post 	= $posts[0]; 
	$last_post 		= end($posts);

	foreach ( $posts as $key => $post ) :
		if ( $post->ID == $post_id ) :

			$prev			= isset( $posts[ $key - 1 ]) ? $posts[ $key - 1 ] : $last_post;
			$next			= isset($posts[ $key + 1 ]) ? $posts[ $key + 1 ] : $first_post ;
		
			if ( $prev->ID != $post_id ) :
				$previous_image		=	get_the_post_thumbnail_url( $prev->ID, 'post-thumbnail') ;
				$previous_title		= 	$prev->post_title;
				$previous_content	= 	$prev->post_content;
				$previous_permalink	= 	get_the_permalink($prev->ID); ?>

				<div class="col-12 col-lg-6 story-col">
					<a class="story" href="<?php echo $previous_permalink; ?>?stories_cat=<?php echo $url_param; ?>">
						<div class="story-card">
							<div class="story-row no-gutters" >
								<div class="col card-img-top story-bg" style="background-image: url(<?php echo $previous_image; ?>); " ></div>
								<div class="col story-content">
									<div class="card-body">
										<p class="card-text h5"><?php echo $previous_title; ?></p>
										<button href="#" class="btn btn-light btn-story">Continue Reading</button>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			<?php
			endif;

			if ( $next->ID != $post_id && $next->ID != $prev->ID ) :
				$next_image			=	get_the_post_thumbnail_url( $next->ID, 'post-thumbnail') ;
				$next_title			=	$next->post_title;
				$next_content		= 	$next->post_content;
				$next_permalink		= 	get_the_permalink($next->ID); ?>

				<div class="col-12 col-lg-6 story-col">
					<a class="story" href="<?php echo $next_permalink;?>?stories_cat=<?php echo $url_param; ?>">
						<div class="story-card">
							<div class="story-row no-gutters" >
								<div class="col card-img-top story-bg" style="background-image: url(<?php echo $next_image; ?>); " ></div>
								<div class="col story-content">
									<div class="card-body">
										<p class="card-text h5"><?php echo $next_title; ?></p>
										<button href="#" class="btn btn-light btn-story">Continue Reading</button>
									</div>
								</div>
							</div>
						</div>
					</a>
				</div>
			<?php
			endif;

		endif;

	endforeach; ?>

	</div>

	<div class="row">
		<div class="col" style="text-align: center;">
			<a href="/<?php echo $url_param; ?>" class="btn btn-light btn-story-view-more">View More</a>
		</div>
	</div>
<?php
endif;

	

