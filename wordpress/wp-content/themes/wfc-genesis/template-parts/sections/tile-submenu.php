<?php
/**
 *  Submenu Section
 * 
 */ 
global $post;

$id		        = wfc_get_page_id();

$show_tiles     = get_post_meta( $id, 'show_tiles', true ); 
$menu_as        = get_post_meta( $id, 'menu_as', true ); 
$menu_name      = get_post_meta( $id, 'menu_name', true );  

$menu_items     = [];

if ( $menu_as == 'Wordpress Menu' ) {
    $raw_menu_items = wp_get_nav_menu_items( $menu_name );
    if ( !empty( $raw_menu_items ) && is_array( $raw_menu_items ) ) {

        foreach ( $raw_menu_items as $raw_menu_item ) {
            $menu_items[ $raw_menu_item->ID ] = array(
                'url'       => $raw_menu_item->url,
                'title'     => $raw_menu_item->title,
                'bg'        => get_post_meta( $raw_menu_item->object_id, 'banner_color', true ),
                'target'    => $raw_menu_item->target
            );
        }
    }
} elseif ( $menu_as == 'Child pages' ) {
	$child_pages = get_children( array(
		'post_parent' => $post->ID,
		'post_type'   => $post->post_type,
		'post_status' => 'publish',
		'orderby'     => 'menu_order',
		'order'       => 'ASC',
		'numberposts' => -1,
    ) );
    
    foreach ( $child_pages as $child_page ) {
        $menu_items[ $child_page->ID ] = array(
            'url'   => get_the_permalink( $child_page->ID ),
            'title' => $child_page->post_title,
            'bg'    => get_post_meta( $child_page->ID, 'banner_color', true ),
        );
    }
} elseif ( $menu_as == 'Sibling pages' ) {
	$child_pages = get_children( array(
		'post_parent' => $post->post_parent,
        'post_type'   => $post->post_type,
        'exclude'     => $post->ID,
		'post_status' => 'publish',
		'orderby'     => 'menu_order',
		'order'       => 'ASC',
		'numberposts' => -1,
    ) );
    
    foreach ( $child_pages as $child_page ) {
        $menu_items[ $child_page->ID ] = array(
            'url'   => get_the_permalink( $child_page->ID ),
            'title' => $child_page->post_title,
            'bg'    => get_post_meta( $child_page->ID, 'banner_color', true ),
        );
    }
}

$tile_section_background  = get_post_meta( $id, 'tile_section_background', true );
$tile_section_layout      = get_post_meta( $id, 'tile_section_layout', true );

$text_color                     = get_theme_mod( 'section_sticky_submenu_text_color', '#fff' );
$section_tile_submenu_bg_grey   = get_theme_mod('section_tile_submenu_bg_grey', '');
$section_tile_submenu_bg_color  = get_theme_mod('section_tile_submenu_bg_color', '');

$section_bg   = '';

if ( $tile_section_background == 'color-image' ) {
    $section_bg = $section_tile_submenu_bg_color;
} elseif ( $tile_section_background == 'grey-image' ) {
    $section_bg = $section_tile_submenu_bg_grey;
}


if ( $show_tiles == '1' && !empty( $menu_items ) ) :
    echo "<section class='container-fluid outer tile-submenu-section'  style='background-image: url({$section_bg});'>"; ?>
        <div class="wrapper inner"> 
            <div class="tile-row <?php echo $tile_section_layout; ?>">
            <?php
                foreach ( $menu_items as $menu_item_k => $menu_item ) :
                    $target = $menu_item['target'] == '_blank' ? 'target="_blank"' : '';

                    echo "<div class='tile-col' style='background-color: {$menu_item['bg']}'>";
                        echo "<a style='color: {$text_color}' {$target} href='{$menu_item['url']}' class='h2 tile-item'>{$menu_item['title']}</a>";
                    echo '</div>';
                endforeach; ?>
            </div>
        </div>
    </section>
<?php 
endif;