<?php
/**
 *  Hero section (For Pages) 
 * 
 */
    global $post;

    $id		    = wfc_get_page_id();
    $height_sm  = get_theme_mod( 'hero_height_simple_sm', '225px' );
    $height_md  = get_theme_mod( 'hero_height_simple_md', '250px' );
	$height_lg 	= get_theme_mod( 'hero_height_simple_lg', '295px' );
    
    $banner_text    = get_post_meta( $id, 'banner_text', 1 );	
    // $banner_text    = !empty( $banner_text ) ? $banner_text : wfc_get_page_title_by_path();	

    $text_color             = get_post_meta( $id, 'text_color', 1 );	
    $banner_color           = get_post_meta( $id, 'banner_color', 1 );	
    $hero_watermark         = get_theme_mod( 'hero_watermark', '' );
    $hero_watermark_dark    = get_theme_mod( 'hero_watermark_dark', '' );
    
    $wfc_show_watermark         = get_post_meta( $id, 'wfc_show_watermark', 1 );
    $wfc_show_watermark_type    = get_post_meta( $id, 'wfc_show_watermark_type', 1 );
?>
<style>
    .hero-column {
        min-height: <?php echo $height_sm; ?>;
    }

    @media (min-width: 36rem) {
        .hero-column {
            min-height: <?php echo $height_md; ?>;
        }
    }

    @media (min-width: 62rem) {
        .hero-column {
			min-height: <?php echo $height_lg; ?>;
        }
    }
</style>
<?php if ( !empty( $banner_text ) ) : ?>
    <section class="container-fluid outer hero simple" style="background-color: <?php echo $banner_color; ?>;">
        <div class="wrapper inner">
            <div class="row">
                <div class="col-sm col-md col-lg hero-column <?php echo ( !$wfc_show_watermark ) ? 'justify-content-center' : '';?>">
                    <h1 class="hero-title banner-heading" style="color: <?php echo $text_color; ?>"><?php echo $banner_text; ?></h1>
                    <?php if ( $wfc_show_watermark && $wfc_show_watermark_type == 'dark' ) : ?>
                        <img class="hero-watermark" src="<?php echo $hero_watermark_dark; ?>"/>
                    <?php elseif ( $wfc_show_watermark && $wfc_show_watermark_type == 'light' ) : ?>
                        <img class="hero-watermark" src="<?php echo $hero_watermark; ?>"/>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>