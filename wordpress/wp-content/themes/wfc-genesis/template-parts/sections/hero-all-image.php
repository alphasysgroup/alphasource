<?php
/**
 *  Hero Template 
 *  Template for All Image Hero
 * 
 */ 

    global $post;

    $id		            = wfc_get_page_id();

	$wfc_adaptive_image = get_post_meta( $id, 'wfc_adaptive_image', true );
	
	$bg_lg_xpos     = get_post_meta( $id, 'wfc_heroimages_large-xpos', true );
    $bg_lg_ypos     = get_post_meta( $id, 'wfc_heroimages_large-ypos', true );
    $bg_lg_pos      = ( ! empty( $bg_lg_ypos ) && ! empty( $bg_lg_xpos ) ) ? "{$bg_lg_ypos} {$bg_lg_xpos}" : 'center';
	
	if ( ! empty( $wfc_adaptive_image ) ) {
        /**
         * Check if the heroimages meta for small exists and use it. If it doesn't, check if the meta for medium exists and use it,
         * If medium also doesn't exist, check if the meta for large exists. If it also doesn't exist, fail graceful.
         */
        $bg_sm = ! empty( get_post_meta( $id, 'wfc_heroimages_small', true ) ) ? get_post_meta( $id, 'wfc_heroimages_small', true ) : get_post_meta( $id, 'wfc_heroimages_medium', true );
        $bg_sm = ! empty( $bg_sm ) ? $bg_sm : get_post_meta( $id, 'wfc_heroimages_large', true );
        $bg_sm = ! empty( $bg_sm ) ? $bg_sm : wfc_get_image( $id, 'hero', 'small' );

        /**
         * Check if the heroimages meta for medium exists and use it. If it doesn't, check if the meta for large exists and use it.
         * If it doesn't, fail graceful.
         */
        $bg_md = ! empty( get_post_meta( $id, 'wfc_heroimages_medium', true ) ) ? get_post_meta( $id, 'wfc_heroimages_medium', true ) : get_post_meta( $id, 'wfc_heroimages_large', true );
        $bg_md = ! empty( $bg_md ) ? $bg_md : wfc_get_image( $id, 'hero', 'medium' );

        /**
         * Check if the heroimages meta for large exists and use it. If it doesn't, fail graceful
         */
        $bg_lg = ! empty( get_post_meta( $id, 'wfc_heroimages_large', true ) ) ? get_post_meta( $id, 'wfc_heroimages_large', true ) : wfc_get_image( $id, 'hero', 'large' );
            
		$bg_sm_xpos     = get_post_meta( $id, 'wfc_heroimages_small-xpos', true );
		$bg_sm_ypos     = get_post_meta( $id, 'wfc_heroimages_small-ypos', true );  
		$bg_sm_pos      = ( !empty( $bg_sm_ypos ) && !empty( $bg_sm_xpos ) ) ? "{$bg_sm_ypos} {$bg_sm_xpos}" : 'center';

		$bg_md_xpos     = get_post_meta( $id, 'wfc_heroimages_medium-xpos', true );
		$bg_md_ypos     = get_post_meta( $id, 'wfc_heroimages_medium-ypos', true ); 
		$bg_md_pos      = ( !empty( $bg_md_ypos ) && !empty( $bg_md_xpos ) ) ? "{$bg_md_ypos} {$bg_md_xpos}" : 'center';
	
	} else {
        $bg_lg = wfc_get_image( $id, 'hero', 'large' );
		
		$bg_sm = $bg_lg;
		$bg_md = $bg_lg;
		
		$bg_sm_pos = $bg_lg_pos;
		$bg_md_pos = $bg_lg_pos;
		
	}
	
    $height_sm  = is_front_page() ? get_theme_mod( 'hero_height_home_sm', '18.75rem' ) : get_theme_mod( 'hero_height_other_sm', '18.75rem' );
    $height_md  = is_front_page() ? get_theme_mod( 'hero_height_home_md', '28.125rem' ): get_theme_mod( 'hero_height_other_md', '28.125rem' );
	$height_lg 	= is_front_page() ? get_theme_mod( 'hero_height_home_lg', '37.5rem' ) : get_theme_mod( 'hero_height_other_lg', '37.5rem' );
    
    $hero_watermark         = get_theme_mod( 'hero_watermark', '' );
    $hero_watermark_dark    = get_theme_mod( 'hero_watermark_dark', '' );
    
    $wfc_show_watermark         = get_post_meta( $id, 'wfc_show_watermark', 1 );
    $wfc_show_watermark_type    = get_post_meta( $id, 'wfc_show_watermark_type', 1 );

    //wfc_get_page_title_by_path();	
    $banner_text    = get_post_meta( $id, 'banner_text', 1 );	
    $text_color     = get_post_meta( $id, 'text_color', 1 );	
    $banner_color   = get_post_meta( $id, 'banner_color', 1 );

    $wfc_show_watermark     = get_post_meta( $id, 'wfc_show_watermark', 1 );	
    $wfc_show_down_arrow    = get_post_meta( $id, 'wfc_show_down_arrow', 1 );	
    $wfc_show_banner_text   = get_post_meta( $id, 'wfc_show_banner_text', 1 );

    $wfc_show_cta           = get_post_meta( $id, 'wfc_show_cta', 1 );
    $wfc_hero_cta_url       = get_post_meta( $id, 'wfc_hero_cta_url', 1 );
    $wfc_hero_cta_label     = get_post_meta( $id, 'wfc_hero_cta_label', 1 );

    $wfc_above_title_image     = get_post_meta( $id, 'wfc_above_title_image', 1 );
?>
<style>
    .hero.all-image {
        background-image: url(<?php echo $bg_sm; ?>);
        background-position: <?php echo $bg_sm_pos; ?>;
    }
    .hero .hero-column {
        min-height: <?php echo $height_sm; ?>;
    }

    @media (min-width: 36rem) {
        .hero.all-image {
            background-image: url(<?php echo $bg_md; ?>);
            background-position: <?php echo $bg_md_pos; ?>;
        }
        .hero .hero-column {
			min-height: <?php echo $height_md; ?>;
        }
    }

    @media (min-width: 62rem) {
        .hero.all-image {
            background-image: url(<?php echo $bg_lg; ?>);
            background-position: <?php echo $bg_lg_pos; ?>;
        }
        .hero .hero-column {
			min-height: <?php echo $height_lg; ?>;
        }
    }
</style>
<section class="container-fluid outer hero all-image">
    <div class="wrapper inner">
        <div class="row">
            <div class="col-sm col-md col-lg hero-column">
                <div class="content-wrap">
                    <?php if ( $wfc_above_title_image ) : ?>
                        <img class="above-heading-img" src="<?php echo $wfc_above_title_image; ?>"/>
                    <?php endif; ?>

                    <?php if ( $wfc_show_banner_text ) : ?>
                        <h1 class="hero-title banner-heading" style="color: <?php echo $text_color; ?>"><?php echo $banner_text; ?></h1>
                    <?php endif; ?>
                    
                    <?php if ( $wfc_show_cta ) : ?>
                        <a href="<?php echo $wfc_hero_cta_url; ?>" style="border-color: <?php echo $text_color;?>;color: <?php echo $text_color;?>" class="btn hero-cta"><?php echo $wfc_hero_cta_label; ?></a>
                    <?php endif; ?>
                    
                    <?php if ( $wfc_show_down_arrow ) : ?>
                        <a href="" class="btn arrow-down fa-2x" style="color: <?php echo $banner_color;?>"><i class="fa fa-angle-down"></i></a>
                    <?php endif; ?>
                </div>

                <?php if ( $wfc_show_watermark && $wfc_show_watermark_type == 'dark' ) : ?>
                    <img class="hero-watermark" src="<?php echo $hero_watermark_dark; ?>"/>
                <?php elseif ( $wfc_show_watermark && $wfc_show_watermark_type == 'light' ) : ?>
                    <img class="hero-watermark" src="<?php echo $hero_watermark; ?>"/>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
