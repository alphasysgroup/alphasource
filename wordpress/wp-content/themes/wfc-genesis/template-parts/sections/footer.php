<!-- Footer -->
<div class="wrapper inner" style="color: #e8dddd;">
	<div class="mobile-footer-section ">
		<?php
			genesis_widget_area ('page-widget-mobile-footer', array(
				'before' => '<div class="mobile-footer-widget"><div class="wrap">',
				'after' => '</div></div>',
			) );
		?>
	
	</div>
	<div class="pancre-footer">
		<div class="row text-center text-md-left mt-5">
			<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3 column-1" >
				<?php
					genesis_widget_area ('page-widget-footer1', array(
						'before' => '<div class="page-widget"><div class="wrap">',
						'after' => '</div></div>',
					) );
				?>
			</div>
			<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3 column-2" >
				<?php
					genesis_widget_area ('page-widget-footer2', array(
						'before' => '<div class="page-widget"><div class="wrap">',
						'after' => '</div></div>',
					) );
				?>
			</div>
			<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3 column-3" >
				<?php
					genesis_widget_area ('page-widget-footer3', array(
						'before' => '<div class="page-widget"><div class="wrap">',
						'after' => '</div></div>',
					) );
				?>
			</div>
			<div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3 column-4" >
				<?php
					genesis_widget_area ('page-widget-footer4', array(
						'before' => '<div class="page-widget"><div class="wrap">',
						'after' => '</div></div>',
					) );
				?>
			</div>
		</div>
		<div class="row d-flex justify-content-center mb-md-0 mb-4">
			<div class="col-6 d-flex justify-content-center" >
				<?php
					genesis_widget_area ('page-widget-footer5', array(
						'before' => '<div class="page-widget-last-row"><div class="widget-last-row-wrap">',
						'after' => '</div></div>',
					) );
				?>
			</div>
			<div class="col-10" style="text-align: center">
				<div class="description">
					<p style="line-height: 1; line-height: 1.5; font-size: 12px; text-align: center;"><?php echo genesis_get_option( 'footer_text' );?></p>
				</div>
			</div>
		</div>
	</div>
</div>
