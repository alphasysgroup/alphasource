<?php
/**
 * Callout Side Slide
 * 
 */	
$section_callout_button_name            = get_theme_mod('section_callout_button_name', 'TELEHEALTH' );
$section_callout_button_c_title         = get_theme_mod('section_callout_button_c_title', 'TELEHEALTH' );
$section_callout_button_c_phone         = get_theme_mod('section_callout_button_c_phone', '1300 881 698' );
$section_callout_button_c_button_label  = get_theme_mod('section_callout_button_c_button_label', 'Book Now' );
$section_callout_button_c_button_link   = get_theme_mod('section_callout_button_c_button_link', '#' );
$section_callout_button_color        = get_theme_mod('section_callout_button_color', '#fff' );
$section_callout_button_bg_color        = get_theme_mod('section_callout_button_bg_color', '#3C455C' );
$section_callout_button_callout_text_color  = get_theme_mod('section_callout_button_callout_text_color', '#fff' );
$section_callout_button_callout_bg_color    = get_theme_mod('section_callout_button_callout_bg_color', '#289F74' ); ?>
<div class="callout-wrapper" id="callout-wrapper"> 
    <div class="callout-side-slide d-none d-md-flex" style="background-color: <?php echo $section_callout_button_callout_bg_color; ?>;">
        <div class="cs-visible">
            <span class="label" style="color: <?php echo $section_callout_button_callout_text_color; ?>;"><?php echo $section_callout_button_name; ?></span>
            <span class="icon" style="border-color: <?php echo $section_callout_button_callout_text_color; ?>;">
                <i class="fa fa-phone" style="color: <?php echo $section_callout_button_callout_text_color; ?>;" aria-hidden="true"></i>
            </span>
        </div>
        <div class="cs-hidden">
            <p class="h5" style="color: <?php echo $section_callout_button_callout_text_color; ?>;"><?php echo $section_callout_button_c_title; ?></p>
            <p class="h5" style="color: <?php echo $section_callout_button_callout_text_color; ?>;"><?php echo $section_callout_button_c_phone; ?></p>
            <a class="btn" style="color: <?php echo $section_callout_button_color; ?>;background-color: <?php echo $section_callout_button_bg_color; ?>;" href="<?php echo $section_callout_button_c_button_link; ?>"><?php echo $section_callout_button_c_button_label; ?></a>
        </div>
    </div>

    <div class="callout-side-slide slide-mobile d-flex d-md-none" style="background-color: <?php echo $section_callout_button_callout_bg_color; ?>;">
        <div class="cs-visible">
            <span class="icon" style="border-color: <?php echo $section_callout_button_callout_text_color; ?>;">
                <i class="fa fa-phone" style="color: <?php echo $section_callout_button_callout_text_color; ?>;" aria-hidden="true"></i>
            </span>
        </div>
        <div class="cs-hidden">
            <p class="h5" style="color: <?php echo $section_callout_button_callout_text_color; ?>;"><?php echo $section_callout_button_c_title . ' | ' . $section_callout_button_c_phone; ?></p>
        </div>
    </div>
</div>
