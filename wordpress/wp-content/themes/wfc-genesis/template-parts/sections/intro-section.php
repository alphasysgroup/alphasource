<?php


    $section_front_page_bg_image    = get_theme_mod('section_front_page_bg_image', '');
    $section_front_page_min_height  = get_theme_mod('section_front_page_min_height', '735px');
    $section_front_page_min_card_height  = get_theme_mod('section_front_page_min_card_height', '520px');

    $section_front_page_cl_bg           = get_theme_mod('section_front_page_cl_bg', '#289F74');
    $section_front_page_cl_color        = get_theme_mod('section_front_page_cl_color', '#FFF'); 
    $section_front_page_cl_category     = get_theme_mod('section_front_page_cl_category', ''); 
    $section_front_page_cl_title        = get_theme_mod('section_front_page_cl_title', ''); 
    $section_front_page_cl_subtitle     = get_theme_mod('section_front_page_cl_subtitle', ''); 
    $section_front_page_cl_content      = get_theme_mod('section_front_page_cl_content', ''); 
    $section_front_page_cl_cta_text     = get_theme_mod('section_front_page_cl_cta_text', ''); 
    $section_front_page_cl_cta_link     = get_theme_mod('section_front_page_cl_cta_link', '#'); 
    $section_front_page_cl_cta_nt     = get_theme_mod('section_front_page_cl_cta_nt', 0); 
    $section_front_page_cl_cta_color     = get_theme_mod('section_front_page_cl_cta_color', '#fff'); 
    $section_front_page_cl_cta_bg     = get_theme_mod('section_front_page_cl_cta_bg', '#323D52'); 
    $section_front_page_cl_image     = get_theme_mod('section_front_page_cl_image', ''); 

    $section_front_page_cr_bg           = get_theme_mod('section_front_page_cr_bg', '#662D91');
    $section_front_page_cr_color        = get_theme_mod('section_front_page_cr_color', '#FFF'); 
    $section_front_page_cr_category        = get_theme_mod('section_front_page_cr_category', ''); 
    $section_front_page_cr_title        = get_theme_mod('section_front_page_cr_title', ''); 
    $section_front_page_cr_subtitle     = get_theme_mod('section_front_page_cr_subtitle', ''); 
    $section_front_page_cr_content      = get_theme_mod('section_front_page_cr_content', ''); 
    $section_front_page_cr_cta_text     = get_theme_mod('section_front_page_cr_cta_text', ''); 
    $section_front_page_cr_cta_link     = get_theme_mod('section_front_page_cr_cta_link', '#');
    $section_front_page_cr_cta_nt     = get_theme_mod('section_front_page_cr_cta_nt', 0); 
    $section_front_page_cr_cta_color     = get_theme_mod('section_front_page_cr_cta_color', '#fff'); 
    $section_front_page_cr_cta_bg     = get_theme_mod('section_front_page_cr_cta_bg', '#323D52'); 
    $section_front_page_cr_image     = get_theme_mod('section_front_page_cr_image', ''); 
    ?>
<style>
    .intro-section p {
        color: <?php echo $section_front_page_cl_color; ?>
    }
    .lg-card {
        min-height: <?php echo $section_front_page_min_card_height; ?>
    }
</style>
<section class="intro-section container-fluid outer" style="background-image: url(<?php echo $section_front_page_bg_image; ?>)">
    <div class="wrapper inner">
        <div class="row">
            <div class="col-sm col-md col-lg">
                <div class="lg-cards-wrap" style="min-height: <?php echo $section_front_page_min_height; ?>;"> 
                    <div class="lg-card card-left" style="color: <?php echo $section_front_page_cl_color; ?>;background-color: <?php echo $section_front_page_cl_bg; ?>">
                        <p class="category"><strong><?php echo $section_front_page_cl_category; ?></strong></p>
                        <div class="title-wrap">
                            <p class="h2 title" style="border-bottom-color: <?php echo $section_front_page_cl_color; ?>"><?php echo $section_front_page_cl_title; ?></p>
                        </div>
                        <div class="dynamic-content-wrap">
                            <?php if ( !empty( $section_front_page_cl_image ) ) : ?>
                                <div class="dynamic-content-left">
                                    <img src="<?php echo $section_front_page_cl_image;?>" />
                                </div>
                            <?php endif; ?>
                            <div class="dynamic-content-right">
                                <p class="h4 subtitle"><?php echo $section_front_page_cl_subtitle; ?></p>
                                <p class="content"><?php echo $section_front_page_cl_content; ?></p>
                            </div>
                        </div>
                        <a  style="color:<?php echo $section_front_page_cl_cta_color; ?>;background-color: <?php echo $section_front_page_cl_cta_bg; ?>"
                            class="btn" 
                            <?php echo ( (boolean) $section_front_page_cl_cta_nt ) ? "target='_blank'" : "" ?>
                            href="<?php echo $section_front_page_cl_cta_link; ?>"><?php echo $section_front_page_cl_cta_text; ?></a>
                    </div>
                    <div class="lg-card card-right" style="color: <?php echo $section_front_page_cr_color; ?>;background-color: <?php echo $section_front_page_cr_bg; ?>">
                        <p class="category"><strong><?php echo $section_front_page_cr_category; ?></strong></p>
                        <div class="title-wrap">
                            <p class="h2 title" style="border-bottom-color: <?php echo $section_front_page_cr_color; ?>"><?php echo $section_front_page_cr_title; ?></p>
                        </div> 
                        <div class="dynamic-content-wrap">
                            <div class="dynamic-content-left">
                                <p class="h4 subtitle"><?php echo $section_front_page_cr_subtitle; ?></p>
                                
								<p class="content" style="max-width:none;"><?php echo $section_front_page_cr_content; ?></p>
                            </div>
                            <?php if ( !empty( $section_front_page_cr_image ) ) : ?>
                                <div class="dynamic-content-right">
                                    <img src="<?php echo $section_front_page_cr_image;?>" />
                                </div>
                            <?php endif; ?>
                        </div>
                        <a  style="color:<?php echo $section_front_page_cr_cta_color; ?>;background-color: <?php echo $section_front_page_cr_cta_bg; ?>"
                            class="btn" 
                            <?php echo ( (boolean) $section_front_page_cr_cta_nt ) ? "target='_blank'" : "" ?> 
                            href="<?php echo $section_front_page_cr_cta_link; ?>"><?php echo $section_front_page_cr_cta_text; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>