<?php
/*
*	Topbar Section
*/

$shop_url = get_theme_mod( 'section_topbar_url', '' );

?>
<section class="topbar custom-topbar container-fluid outer">
	<div class="wrapper inner">
		<div class="row">
			<div class="col-sm col-md col-lg">
				<div class="d-flex justify-content-between">
					<nav class="primary-nav"style="display: flex;">
						<?php 
							if ( has_nav_menu( 'top-bar' ) ) {
								$args = array(
									'menu_class' => 'nav',        
									'theme_location' => 'top-bar',
									'container_class' => 'main-nav',
									'item_spacing' => 'discard'
								);
								wp_nav_menu( $args );
							}
						?>											
						<!-- Search -->
						<div class="search-section">
							 <form class="search-form" method="get" action="<?php echo site_url(); ?>">
								<div class="search-form-group d-flex">
									<input class="form-control search-form-input w-auto" type="search" name="s" placeholder="Search">							
									<button class="btn btn-default search-btn" type="submit"><i class="fas fa-search"></i></button>	
								</div> 
							</form>
						</div>
						<!-- For Button Shop-->
						<div class="shop-content" style="display: flex">
							<a href="<?php echo $shop_url; ?>" target="_blank" class="btn shop-btn"><i class="fas fa-ribbon" style="margin: 0 5px; font-size: 20px;"></i>Shop</i></a>	
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>
</section>

