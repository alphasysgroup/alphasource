<?php
/**
 *  Pre Footer Ad Section
 * 
 * 
 */

$title      = get_theme_mod( 'section_conf_adrotate_section_title', 'Sponsors' );
$group_id   = get_theme_mod( 'section_conf_adrotate_section_group_id', 0 ); ?>

<section class="pre-footer-ad-section">
    <?php if ( function_exists('adrotate_group') && !empty( $group_id ) ) : ?>
	<div class="container">
        <div class="row">
            <div class="col-sm col-md col-lg">
                <?php if ( $title ) : ?>
                    <p class="h5 title"><?php echo $title; ?></p>
                <?php endif; ?>
                <div class="ad-col">
                    <?php echo adrotate_group( $group_id ); ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</section>
