<?php
$posts = get_posts( array(
    'post_type'         => 'post',
    'post_status'       => 'publish', 
    'posts_per_page'    => -1,
	'order'            	=> 'DESC',
) );

if ( !empty( $posts  ) ) :
	
	$current_id = get_the_ID();
	
	// To get ID of first post in custom post type 
	$first_post 	= $posts[0]; 


	// To get ID of last post in custom post type 
	$last_post 	= end($posts);

	// Get other posts in post type
	$next_post		= get_next_post();
	$next_post		= empty( $next_post ) ? $last_post : $next_post;

	$previous_post	= get_previous_post();
	$previous_post	= empty( $previous_post ) ? $first_post : $previous_post;

	//Next Post
	$next_id			= 	$next_post->ID;
	$next_image			=	get_the_post_thumbnail_url( $next_id, 'post-thumbnail') ;
	$next_title			=	$next_post->post_title;
	$next_content		= 	$next_post->post_content;
	$next_permalink		= 	get_the_permalink($next_id);

	//Previous Post
	$previous_id		= 	$previous_post->ID;
	$previous_image		=	 get_the_post_thumbnail_url( $previous_id, 'post-thumbnail') ;
	$previous_title		= 	 $previous_post->post_title;
	$previous_content	= 	$previous_post->post_content;
	$previous_permalink	= 	get_the_permalink($previous_id);

	if ( $current_id != $next_id && $current_id != $previous_id ) : ?>
        <section class="container-fluid outer before-after-slim mt-4">
            <div class="wrapper inner"> 
                <div class="row story-row-parent no-gutters">		
                    <div class="col-12 col-lg-6 story-col m-0 border-top border-bottom ">
                        <a class="story text-left py-4 pr-0 pr-sm-5" href="<?php echo $next_permalink; ?>">
                            <p class="mb-0"><i class="fa fa-chevron-left mr-2"></i> Previous</p>
                            <p class="mt-3 mb-2 h4 text3"><?php echo $next_title; ?></p>
                            <p class=""><?php echo get_the_date('', $next_id); ?></p>
                        </a>
                    </div>	
                    <?php if ( $previous_id != $next_id ) : ?>
                        <div class="col-12 col-lg-6 story-col m-0 border-top border-bottom ">
                            <a class="story text-right py-4 pl-0 pl-sm-5" href="<?php echo $previous_permalink; ?>">
                                <p class="mb-0">Next <i class="fa fa-chevron-right ml-2"></i></p>
                                <p class="mt-3 mb-2 h4 text3"><?php echo $previous_title; ?></p>
                            <p class=""><?php echo get_the_date('', $previous_id); ?></p>
                            </a>
                        </div>
                    <?php endif;  ?>
                </div>
            </div>
        </section>
	<?php endif; 
endif; 
