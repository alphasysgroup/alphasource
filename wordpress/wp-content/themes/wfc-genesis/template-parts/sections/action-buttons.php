<?php
/**
 * 
 * 
 */	
 
global $post;
$addthis_shortcode			= get_theme_mod('section_spost_action_button_addthis_shortcode', ''); 
$isDisableCrumb				= get_post_meta( $post->ID, 'disable_breadcrumb', true );
$pancare_classbreadcrumbs	= ''; 

if( $isDisableCrumb ){
	$pancare_classbreadcrumbs = 'd-none';
}

?>

<section class="container-fluid outer ">
	<div class="wrapper inner">
		<div class="row mt-4 mb-4">
			<?php if ( ! is_front_page() ) : ?>
				<div class=" <?php echo $pancare_classbreadcrumbs; ?> ctrl-breadcrumbs col-sm col-md col-lg breadcrumbs align-items-center">
					<?php WFC_Yoast_Breadcrumbs::yoast_breadcrumbs_markup(); ?>
				</div>
			<?php endif; ?>
			<div class="media-button singlular-post-action-button col-auto ctrl-grp ml-auto">
				<?php if ( get_post_meta( $post->ID, '_at_widget', true ) ) : ?>
					<div class="adthis-sc-wrap">
						<i class="fa fa-share-alt text5" aria-hidden="true"></i>
						<?php echo do_shortcode( $addthis_shortcode ); ?> 
					</div>
				<?php endif; ?>
				<button class="btn text5" type="button" onclick="window.print()"></i><i class="fas fa-print"></i></button>
			</div>
		</div>
	</div>
</section>