<?php
/**
 * Disclaimer section
 */
$sc = get_theme_mod('section_disclaimer_s', '' ); ?>
<section class="container-fluid outer articles-news-events">
	<div class="wrapper inner">
		<div class="row">
			<div class="col-sm col-md col-lg">
                <?php echo do_shortcode( $sc ); ?>
            </div>
        </div>
    </div>
</section>