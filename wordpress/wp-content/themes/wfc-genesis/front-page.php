<?php

//* Force full-width-content layout setting
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

add_action( 'genesis_after_header', 'render_submenu_as_tiles', 11 );
add_action('genesis_after_header', function(){
    echo wfc_get_section('intro-section');
}, 11);

//* Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );
remove_action( 'genesis_before_footer', 'render_submenu_as_tiles', 12 );

add_action('genesis_before_footer', function() {
    echo wfc_get_section('pre-footer-ad');
});

genesis();