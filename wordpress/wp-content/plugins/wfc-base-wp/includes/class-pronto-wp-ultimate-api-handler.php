<?php

// Load the generic class for API class.
if( !class_exists( 'Pronto_Wp_Ultimate_Api' ) )
	require_once plugin_dir_path( __FILE__ ) . 'apis/abstract-pronto-wp-ultimate-api.php';

/**
 * It handles every API used in Pronto Ultimate plugins.
 *
 * Last updated: 08/17/2017
 *
 * @link 		https://alphasys.com.au/
 *
 * @version 	1.0
 * @package		Pronto_Wp_Portal
 * @subpackage	Pronto_Wp_Portal/includes
 * @author 		AlphaSys <carl@alphasys.com.au>
 */
class Pronto_Wp_Ultimate_Api_Handler {

	/**
	 * @since 	1.0
	 * @var 	$instance 		Single instance of this class.
	 * @access 	protected
	 */
	protected static $instance 	= null;

	/**
	 * @since 	1.0
	 * @var 	$namespace 		Namespace for the API.
	 * @access 	private
	 */
	private $namespace 			= 'prontoultimate';

	/**
	 * @since 	1.0
	 * @var 	$apis 			Container of all API instance.
	 * @access 	private
	 */
	private $apis 				= array();

	/**
	 * @since 	1.0
	 * @var 	$api_dirs 		Valid API directories.
	 * @access 	private
	 */
	private $api_dirs 			= array();

	/**
	 * Prevents multiple instance.
	 *
	 * @since 	1.0
	 * @access 	protected
	 */
	protected function __construct() {
	}

	/**
	 * Prevents unserializing of an instance of this class.
	 *
	 * @since 	1.0
	 * @access 	protected
	 */
	protected function __wakeup() {
	}

	/**
	 * Prevents cloning.
	 *
	 * @since 	1.0
	 * @access 	protected
	 */
	protected function __clone() {
	}

	/**
	 * Prevents cloning.
	 *
	 * @static
	 * @since 	1.0
	 * @access 	public
	 * @return 	Pronto_Wp_Ultimate_Api_Handler
	 */
	public static function get_instance() {
		if( null === self::$instance )
			self::$instance = new Pronto_Wp_Ultimate_Api_Handler();

		return self::$instance;
	}

	/**
	 * Initialize APIs.
	 *
	 * @since 	1.0
	 * @access 	public
	 */
	public function init() {
		if ( ! defined( 'PRONTO_WP_API_INIT' ) ) {
			add_action( 'after_setup_theme', array($this, 'prepare_api_routes'));
			define( 'PRONTO_WP_API_INIT', true );
		}
	}

	/**
	 * Callback for after_setup_theme action
	 */
	public function prepare_api_routes() {
		$this->setup_default_directories();

		$this->collect_files();

		add_action( 'rest_api_init', array( $this, 'set_api_routes' ) );
	}

	/**
	 * Set all API routes.
	 *
	 * @since 	1.0
	 * @access 	public
	 */
	public function set_api_routes() {
		foreach ( $this->apis as $api ) {
			$obj_name = get_class( $api );
			$route_name = $this->generate_route_name( $obj_name );

			if( "" != $route_name ) {
				$success = register_rest_route(
						$this->namespace . '/' . $api->get_version(),
						'/' . $route_name,
						array(
								'methods' 	=> $api->get_method(),
								'callback'	=> array( $api, 'api_callback' )
							)
					);
			}
		}
	}

	/**
	 * Setup default directories.
	 *
	 * @since 	1.0
	 * @access 	private
	 */
	private function setup_default_directories() {
		$this->api_dirs[] = plugin_dir_path( __FILE__ ) . 'apis';

		$this->api_dirs = apply_filters( 'pronto_wp_ultimate_api_dirs', $this->api_dirs );
	}

	/**
	 * Collect all API files.
	 *
	 * @since 	1.0
	 * @access 	private
	 */
	private function collect_files() {
		if( empty( $this->apis ) && !empty( $this->api_dirs ) ) {
			foreach ( $this->api_dirs as $dir ) {
				$dir = rtrim( $dir, '/' );

				if( strpos( $dir, substr( $dir, -5 ) ) === strpos( $dir, '/apis' ) )
					$dir = substr( $dir, 0, strlen( $dir ) - 5 );

				foreach ( glob( $dir . '/apis/class-pronto-wp-*-api.php' ) as $api_file ) {
					if( file_exists( $api_file ) )
						$this->apis[] = require_once ( $api_file );
				}
			}
		}

		$this->apis = apply_filters( 'pronto_wp_ultimate_apis', $this->apis );
	}

	/**
	 * Generate name for the route based on the given class name.
	 *
	 * @since 	1.0
	 * @access 	private
	 */
	private function generate_route_name( $class_name ) {
		$prefix = 'Pronto_Wp_';
		$route_name = "";

		if( 0 === strpos( $class_name, $prefix ) ) {
			$prefix_len = strlen( $prefix );
			$route_name_len = strlen( $class_name ) - $prefix_len - 4;
			$route_name = substr( $class_name, $prefix_len, $route_name_len );
			$route_name = strtolower( $route_name );
			$route_name = str_replace( '_', '', $route_name );
		}

		return $route_name;
	}

	/**
	 * Filter out invalid directories.
	 *
	 * @since 	1.0
	 * @access 	private
	 */
	private function clean_dirs( $dir ) {
		$plugin_dir_len = strlen( WP_PLUGIN_DIR . '/' );
		$api_path = substr( $dir, $plugin_dir_len, strlen( $dir ) - $plugin_dir_len );

		return ( 0 === strpos( $api_path, 'pronto-wp-' ) && file_exists( $dir ) );
	}
}

/**
 * Function `pronto_ultimate_api`
 *
 * Filter out invalid directories.
 *
 * @since 	1.0.0
 * @return 	Pronto_Wp_Ultimate_Api_Handler
 */
function pronto_ultimate_api() {
	return Pronto_Wp_Ultimate_Api_Handler::get_instance();
}
