<?php

if( ! class_exists( 'Pub_List_Table' ) ) {
   
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );

	class Pub_List_Table extends WP_List_Table {

		/**
		 * The columns of the table
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      array    $columns    
		 */
		private $columns;

		/**
		 * The table list sortable
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      boolean    $sortable 
		 */
		private $sortable;

		/**
		 * The pagination item limit per page
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $per_page 
		 */
		private $per_page;

		/**
		 * The table actions
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $actions
		 */
		private $actions;

		/**
		 * The table column to be hidden
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $hidden 
		 */
		private $hidden;

		/**
		 * The data row count
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      int    $rowcount 
		 */
		private $rowcount;

		/**
		 * Define the core functionality of the plugin.
		 *
		 * @since    1.0.0
		 */
		public function __construct($data=array(), $columns=array(), $per_page=5, $sortable=false, $actions=array() , $hidden=array(), $rowcount ){
			parent::__construct();

			$this->items = $data;

			//table columns.
			$this->columns = $columns;

			//field to be hide.
			$this->hidden = array();

			//pagination item per page.
			$this->per_page = $per_page;

			//table actions.
			$this->actions = $actions;

			$this->rowcount = $rowcount;

			//sortable columns.
			if($sortable){
				$this->sortable = $this->get_sortable_columns();
			}
		}

		function prepare_items() {

			$columns  = $this->get_columns();

			$this->_column_headers = array( $columns, $this->hidden, $this->sortable );

			usort( $this->items, array( &$this, 'usort_reorder' ) );

			$current_page = $this->get_pagenum();

			// $total_items = count( $this->items );
			$total_items = $this->rowcount;

			// only ncessary because we have sample data
			$found_data = array_slice( $this->items, ( ( $current_page-1 )* $this->per_page ), $this->per_page );

			$this->set_pagination_args( array(
				'total_items' => $total_items,
				'per_page'    => $this->per_page 
			) );

			// $this->items = $found_data;
		}

		function no_items() {
			_e( 'No record found, dude.' );
		}

		function column_default( $item, $column_name ) {

			if (array_key_exists($column_name, $this->items[0])){
				return $item[ $column_name ];
			}else{
				return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes	
			}
		}

		function get_sortable_columns() {

			$sortable_columns = array();

			foreach($this->columns as $index=>$item){
				$sortable_columns[$index] =  array( $index, false);
			}

			return $sortable_columns;
		}

		function get_columns(){

		    $columns = array(
		        // 'cb'        => '<input type="checkbox" />',
		    );

	    	return array_merge($columns, $this->columns);
		}

		function usort_reorder( $a, $b ) {

			// If no sort, default to title
			$orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'ID';
			// If no order, default to asc
			$order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'desc';
			// Determine sort order
			$result = strnatcmp( $a[$orderby], $b[$orderby] );
			// Send final sort direction to usort
			return ( $order === 'asc' ) ? $result : -$result;
		}

		function column_title($item){

			$actions = array();

			if(!is_array($this->actions))
				return $item['title'];

			foreach($this->actions as $action){
				array_push($actions, sprintf('<a href="?page=%s&action=%s&ID=%s">'.ucfirst($action).'</a>',$_REQUEST['page'],strtolower($action),$item['ID']));
			}

			return sprintf('%1$s %2$s', $item['title'], $this->row_actions($actions) );

		}

		function get_bulk_actions() {
			$actions = array(
				// 'delete'    => 'Delete'
			);
			return $actions;
		}

		function column_cb($item) {
		    return sprintf(
		        '<input type="checkbox" name="rid[]" value="%s" />', isset( $item['ID'] ) ? $item['ID'] : ''
		    );    
		}

		public function search_form($label, $id){

			?>
			<form method="post" style="float: right">
				<?php echo $this->search_box( $label, $id ); ?>
			</form>

			<?php
		}

		function search_data($id, $array) {

		   foreach ($array as $key => $val) {
		       if ($val['uid'] === $id) {
		           return $key;
		       }
		   }

		   return null;
		}

		function table_data_search($key, $value, $array) {
		  
	 		$results = array();
	         
	        if (is_array($array)) {
		        if (isset($array[$key]) && $array[$key] == $value) {
		        	$results[] = $array;
		        }
	         
		        foreach ($array as $subarray) {
		        	$results = array_merge($results, $this->table_data_search($subarray, $key, $value));
		        }
	        }
	         
	        return $results;

		}
	} //class
}
