<?php
/**
 * Class for the building ui-textarea elements
 *
 * @package    Cherry_Framework
 * @subpackage Class
 * @author     Cherry Team <support@cherryframework.com>
 * @copyright  Copyright (c) 2012 - 2015, Cherry Team
 * @link       http://www.cherryframework.com/
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-3.0.html
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'UI_PDwpeditor' ) ) {

	/**
	 * Class for the building UI_PDwpeditor elements.
	 */
	class UI_PDwpeditor extends UI_Element implements I_UI {
		/**
		 * Default settings
		 *
		 * @var array
		 */
		private $defaults_settings = array(
			'id'			=> 'cherry-ui-textarea-id',
			'name'			=> 'cherry-ui-textarea-name',
			'value'			=> '',
			'placeholder'	=> '',
			'rows'			=> '10',
			'cols'			=> '20',
			'label'			=> '',
			'class'			=> '',
			'master'		=> '',
		);

		/**
		 * Constructor method for the UI_Textarea class.
		 *
		 * @since  4.0.0
		 */
		function __construct( $args = array() ) {
			$this->defaults_settings['id'] = 'cherry-ui-textarea-' . uniqid();
			$this->settings = wp_parse_args( $args, $this->defaults_settings );

			add_action( 'admin_enqueue_scripts', array( __CLASS__, 'enqueue_assets' ) );
		}

		/**
		 * Render html UI_PDwpeditor.
		 *
		 * @since  4.0.0
		 */
		public function render() {
			// Turn on the output buffer
			ob_start();
			$html = '';
			$class = $this->settings['class'];
			$class .= ' ' . $this->settings['master'];
			?>
			<div class="<?php echo 'cherry-ui-container'.esc_attr( $class ) ?>"> 
			<?php if ( '' !== $this->settings['label'] ) { ?>
					<label class="cherry-label" for="<?php esc_attr( $this->settings['id'] ) ?>"> 
					<?php echo $this->settings['label']; ?>
					</label>
					
			<?php } ?>
					
				<?php 
				wp_editor( $this->settings['value'] , $this->settings['id'] )  ; 
				?>
					
			</div>
			<?php 
			// Store the contents of the buffer in a variable
			$editor_contents = ob_get_clean();

			// Return the content you want to the calling function
			return $editor_contents;
		}

		/**
		 * Enqueue javascript and stylesheet UI_Textarea
		 *
		 * @since  4.0.0
		 */
		public static function enqueue_assets() {
			wp_enqueue_style(
				'ui-textarea',
				esc_url( Cherry_Core::base_url( 'assets/min/ui-textarea.min.css', __FILE__ ) ),
				array(),
				'1.0.0',
				'all'
			);
		}
	}
}
