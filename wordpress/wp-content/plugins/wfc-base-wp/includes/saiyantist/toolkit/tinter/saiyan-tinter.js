/**
 * Saiyan Tinter toolkit
 *
 * @author Von Sienard Vibar
 * @since 1.0.0
 */

(function($) {
    SaiyanToolkit.tinter = {
        tinterClass: 'saiyan-tinter',
        tinterUI: $('<div></div>').addClass('saiyan-tinter')
            .css('cssText', 'width: 100% !important; height: 100% !important')
            .css({
                'position': 'fixed',
                'display': 'flex',
                'background-color': 'rgba(0, 0, 0, 0.8)',
                'top': '0',
                'left': '0',
                // 'width': '100vw',
                // 'height': '100vh',
                'opacity': '0.0',
                'z-index': '9990'
            }).append(
                /*
                $('<img />').attr('src', wfc_don_tinter.plugin_dir + 'css/img/loading_tail.svg').css({
                    'width': '200px',
                    'height': '200px',
                    'margin': 'auto'
                })
                */
            ),

        // update for < es6 backward compatibility
        show: function(color, timeout, callback) {
            color = color || null;
            timeout = timeout || null;
            callback = callback || null;

            var body = $('body');

            typeof color !== 'undefined'

            if (body.find('.' + this.tinterClass).length === 0) {
                if (color !== null) {
                    this.tinterUI.css('background-color', color);
                }
                body.append(
                    this.tinterUI.animate({
                        'opacity': '0.7'
                    }, 300, 'easeOutCubic', function() {
                        if (timeout !== null) {
                            setTimeout(function() {
                                if (callback !== null) callback();

                                SaiyanToolkit.tinter.hide();
                            }, timeout);
                        }
                    })
                )
            }
        },

        hide: function() {
            var body = $('body');
            var tinter = body.find('.' + this.tinterClass);

            if (tinter.length === 0) console.log('Saiyan tinter is already closed');
            else {
                tinter.animate({
                    'opacity': '0.0'
                }, 300, 'easeOutCubic', function() {
                    tinter.remove();
                })
            }
        }
    }
})(jQuery);