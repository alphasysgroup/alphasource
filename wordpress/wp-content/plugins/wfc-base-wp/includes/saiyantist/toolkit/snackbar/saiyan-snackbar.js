/**
 * Saiyan Snackbar toolkit
 *
 * @author Von Sienard Vibar
 * @since 1.0.0
 */

(function($) {
    SaiyanToolkit.snackbar = {
        snackbarClass: 'saiyan-snackbar',
        snackbarUI:
            $('<div></div>')
                .addClass('saiyan-snackbar-wrapper')
                .append(
                    $('<div></div>').addClass('saiyan-snackbar')
                ),

        status: {
            info: 'info',
            success: 'success',
            warning: 'warning',
            error: 'error'
        },

        show: function(message, timeout, status, callback) {
            status = status || this.status.info;
            callback = callback || null;

            // if (typeof status === 'undefined') status = 'info';
            var $this = this;
            var body = $('body');

            if (body.find('.' + $this.snackbarClass).length === 0) {
                body.append(this.snackbarUI);
            }

            this.snackbarUI.find('.' + this.snackbarClass).html(message).addClass('show ' + status);

            // TODO fix bug for status change
            if (typeof timeout !== 'undefined' && timeout !== null) {
                setTimeout(function() {
                    $this.hide();
                    if (callback !== null) callback();
                }, timeout);
            }
        },

        hide: function() {
            this.snackbarUI.html('').attr('class', this.snackbarClass);
        }
    }
})(jQuery);