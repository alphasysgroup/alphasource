/**
 * Saiyan Spinner toolkit
 *
 * @author Von Sienard Vibar
 * @since 1.0.0
 */

(function($) {
    SaiyanToolkit.spinner = {
        spinnerClass: 'saiyan-spinner',
        spinnerUI:
            $('<div></div>')
                .addClass('saiyan-spinner')
                .css({
                    'position': 'fixed',
                    'display': 'flex',
                    'top': '0',
                    'left': '0',
                    'width': '100%',
                    'height': '100%',
                    'opacity': '0.0',
                    'z-index': '9992'
                }).append(
                    $('<div></div>')
                        .css({
                            'width': '200px',
                            'height': '200px',
                            'margin': 'auto'
                        })
                        .append(
                            $('<img />')
                        )
                ),

        // update for < es6 backward compatibility
        show: function(message, type, timeout, callback) {
            message = message || null;
            type = type || 'tail';
            timeout = timeout || null;
            callback = callback || null;

            var body = $('body');

            if (body.find('.' + this.spinnerClass).length === 0) {
                var src = navigator.userAgent.search(/(MSIE)\s(8\.0|9\.0|10\.0|11\.0)/) > 0
                    ? saiyan_toolkit_spinner.gif_path + 'loading_fallback_spinner.gif'
                    : saiyan_toolkit_spinner.svg_path + 'loading_' + type + '.svg';

                this.spinnerUI.find('img')
                    .attr('src', src);

                if (message !== null) {
                    this.spinnerUI.find('img').after(
                        $('<div>' + message + '</div>').css({
                            'color': 'white',
                            'text-align': 'center'
                        })
                    )
                }

                body.append(
                    this.spinnerUI.animate({
                        'opacity': '1.0'
                    }, 300, 'easeOutCubic')
                );

                if (timeout !== null) {
                    setTimeout(function() {
                        SaiyanToolkit.spinner.hide();

                        if (callback !== null) callback();
                    }, timeout);
                }
            }
        },

        hide: function() {
            var body = $('body');
            var spinner = body.find('.' + this.spinnerClass);

            if (spinner.length === 0) console.log('Saiyan spinner is already closed');
            else {
                spinner.animate({
                    'opacity': '0.0'
                }, 300, 'easeOutCubic', function() {
                    spinner.remove();
                })
            }
        }
    }
})(jQuery);


// #fdfdfd
// #e7e7e7
// #c1c1c1