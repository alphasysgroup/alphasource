<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Toolkit')) {

	/**
	 * Singleton core class for handling the Saiyan toolkits
	 *
	 * Class Saiyan_Toolkit
	 *
	 * @author Von Sienard Vibar
	 * @since 1.0.0
	 */
	class Saiyan_Toolkit {

		protected static $instance;

		private $toolkits = array();

		private function __construct() {
			wp_enqueue_script('jquery-effects-core');
			$this->print_saiyan_toolkit_variable();
			$this->load_assets();
		}

		/**
		 * Returns the instance of itself
		 *
		 * @return Saiyan_Toolkit
		 *
		 * @author Von Sienard Vibar
		 */
		public static function get_instance() {
			if (self::$instance === null) self::$instance = new self();
			return self::$instance;
		}

		/**
		 * Loads array of toolkit's assets specified that will be used for JS scripting
		 *
		 * @param array $toolkits Array of toolkits
		 *
		 * @author Von Sienard Vibar
		 */
		public function load_toolkits(array $toolkits) {
			foreach ($toolkits as $toolkit) {
				if (in_array($toolkit, $this->toolkits)) {
					wp_enqueue_script('saiyan-toolkit-' . $toolkit . '-js');
					wp_enqueue_style('saiyan-toolkit-' . $toolkit . '-css');
				}
			}
		}

		/**
		 * Returns an array of toolkits
		 *
		 * @return array An array of toolkits
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function get_toolkits() {
			return $this->toolkits;
		}

		/**
		 * Returns the svg path for the spinner toolkit
		 *
		 * @return string SVG path for the spinner toolkit
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function get_spinner_svg_dir() {
			return plugin_dir_path(dirname(__FILE__)) . 'toolkit/spinner/svg/';
		}

		/**
		 * Returns the SVG file of the selected svg type.
		 * Will return the svg dir path if svg type is not supplied.
		 *
		 * @param null optional $svg The svg type
		 *
		 * @return string SVG url path
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function get_spinner_svg_url($svg = null) {
			if (isset($svg))
				return plugin_dir_url(dirname(__FILE__)) . 'toolkit/spinner/svg/loading_' . $svg . '.svg';

			return plugin_dir_url(dirname(__FILE__)) . 'toolkit/spinner/svg/';
		}

		/**
		 * Registers toolkit's assets
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		private function load_assets() {
			$toolkit_dir = plugin_dir_path(dirname(__FILE__)) . 'toolkit/';
			$toolkits = scandir($toolkit_dir);

			foreach ($toolkits as $toolkit) {
				if ($toolkit !== '.' && $toolkit !== '..' && $toolkit !== basename(__FILE__)) {
					wp_register_script(
						'saiyan-toolkit-' . $toolkit . '-js',
						plugin_dir_url(dirname(__FILE__)) . 'toolkit/' . $toolkit . '/saiyan-' . $toolkit . '.js',
						array('jquery'),
						'1.0.0',
						false
					);

					$this->load_css($toolkit);

					if ($toolkit === 'spinner') {
						wp_localize_script('saiyan-toolkit-' . $toolkit . '-js', 'saiyan_toolkit_spinner', array(
							'svg_path' => plugin_dir_url(dirname(__FILE__)) . 'toolkit/' . $toolkit . '/svg/',
							'gif_path' => plugin_dir_url(dirname(__FILE__)) . 'toolkit/' . $toolkit . '/gif/'
						));
					}

					array_push($this->toolkits, $toolkit);
				}
			}
		}

		/**
		 * Checks and registers styles if exist on the specific toolkit directory
		 *
		 * @param string $toolkit The toolkit
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		private function load_css($toolkit) {
			$toolkit_dir = plugin_dir_path(dirname(__FILE__)) . 'toolkit/' . $toolkit;
			$contents = scandir($toolkit_dir);

			foreach ($contents as $content) {
				if ($content !== '.' && $content !== '..') {
					if (pathinfo($content, PATHINFO_EXTENSION) === 'css') {
						wp_register_style(
							'saiyan-toolkit-' . $toolkit . '-css',
							plugin_dir_url(dirname(__FILE__)) . 'toolkit/' . $toolkit . '/saiyan-' . $toolkit . '.css'
						);
					}
				}
			}
		}

		/**
		 * Prints out the SaiyanToolkit JS object that act as the parent of the toolkits
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		private function print_saiyan_toolkit_variable() {
			echo '<script>var SaiyanToolkit = {};</script>';
		}
	}
}