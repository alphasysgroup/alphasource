/**
 * Saiyan Toast toolkit
 *
 * @author Von Sienard Vibar
 * @since 1.0.0
 */

(function($) {
    SaiyanToolkit.toast = {
        toastClass: 'saiyan-toast',
        toastUI:
            $('<div></div>').addClass('saiyan-toast'),

        show: function(message, timeout, callback) {
            timeout = timeout || null;
            callback = callback || null;

            var body = $('body');

            if (body.find('.' + this.toastClass).length === 0) {
                body.append(this.toastUI);
            }

            $(this.toastUI).html(message).addClass('show');

            if (timeout !== null) {
                setTimeout(function() {
                    if (callback !== null) callback();

                    SaiyanToolkit.toast.hide();
                }, timeout);
            }
        },

        hide: function() {
            $(this.toastUI).removeClass('show');
        }
    }
})(jQuery);