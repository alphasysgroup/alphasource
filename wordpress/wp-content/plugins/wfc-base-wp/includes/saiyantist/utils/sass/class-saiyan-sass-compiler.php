<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_SASS_Compiler')) {
    if (! class_exists('scssc')) require_once 'lib/scss.inc.php';

	/**
	 * Class Saiyan_SASS_Compiler
	 *
	 * Class that handles and compiles .scss file to .css file
	 *
	 * @author Von Sienard Vibar
	 * @since 1.0.0
	 */
	class Saiyan_SASS_Compiler {

		private $scss;

		private $scss_dir;
		private $css_dir;
		public $vars;

		/**
		 * Saiyan_SASS_Compiler constructor.
		 *
		 * Constructor for the class which initializes the scssc() class.
		 *
		 * @throws Exception
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function __construct($config) {
			if (! isset($config) && ! is_array($config) && empty($config))
				throw new Exception('configuration needed before printing css');

			if (isset($config['scss_dir'])) $this->scss_dir = $config['scss_dir'];
			else throw new Exception('scss_dir is needed');

			if (isset($config['css_dir'])) $this->css_dir = $config['css_dir'];
			else throw new Exception('css_dir is needed');

			if (isset($config['vars']) && is_array($config['vars']) && ! empty($config['vars']))
				$this->vars = $config['vars'];
			else throw new Exception('vars is either not set or invalid');

			$this->scss = new scssc();
		}

		/**
		 * Setter for the $scss_dir.
		 *
		 * @param string $dir .scss directory path
		 *
		 * @return $this|null Returns self, null if doesn't exist
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function set_scss_dir($dir) {
			if (is_dir($dir))
				$this->scss_dir = $dir;
			else
				echo 'Directory \'' . $dir . '\' doesn\'t exist';

			return $this;
		}

		/**
		 * Setter for the $css_dir.
		 *
		 * @param string $dir .css directory path
		 *
		 * @return $this|null Returns self, null if doesn't exist
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function set_css_dir($dir) {
			if (is_dir($dir))
				$this->css_dir = $dir;
			else
				echo 'Directory \'' . $dir . '\' doesn\'t exist';

			return $this;
		}

		/**
		 * Sets variables that will be passed on the .scss file that will be compiled to .css.
		 *
		 * @param array $vars Array of variables in key-value format
		 *
		 * @return $this Returns self
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function set_variables(array $vars) {
			$vars = isset($vars) && is_array($vars) ? $vars : array();
			$this->scss->setVariables($vars);
			return $this;
		}

		/**
		 * Set import path for the SASS directives.
		 *
		 * @param string $path
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function set_import_path($path) {
			$this->scss->setImportPaths($path);
		}

		/**
		 * Returns the compiled css from the provided SASS file.
		 *
		 * @param string $input_file SASS file
		 * @param bool $minify Optional. It will compressed the .css file if true, else will use the standard one
		 *
		 * @return string Compiled CSS
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function get_compiled_css($input_file, $minify = false) {
			$scss_file = $this->scss_dir . $input_file;

			if (! file_exists($scss_file)) die('File \'' . $input_file . '\' doesn\'t exist');

			$this->scss->setFormatter(
				$minify ? new scss_formatter_compressed() : new scss_formatter()
			);

			$this->scss->setVariables($this->vars);

			$scss_handle = fopen($scss_file, 'r');
			$scss_data = fread($scss_handle, filesize($scss_file));
			fclose($scss_handle);

			return $this->scss->compile($scss_data);
		}

		/**
		 * Renders the .css file on the set css directory from the .scss file.
		 *
		 * @param string $input_file The full file name of the .scss file
		 * @param string $output_file The full file name of the .css file
		 * @param bool $minify Optional. It will compressed the .css file if true, else will use the standard one
		 *
		 * @throws Exception
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public function print_css($input_file, $output_file, $minify = false) {
			if (empty($this->scss_dir) && empty($this->css_dir)) return;

			if (! file_exists($this->scss_dir . $input_file)) {
				echo 'File \'' . $input_file . '\' doesn\'t exist';
				return;
			}

			if ($minify) $this->scss->setFormatter(new scss_formatter_compressed());
			else $this->scss->setFormatter(new scss_formatter());

			$this->scss->setVariables($this->vars);

			/*
			if (! file_put_contents(
				$this->css_dir . $output_file,
				$this->scss->compile(file_get_contents($this->scss_dir . $input_file))
			)) echo 'Unable to create \'' . $output_file . '\'';
			*/

			$scss_file = $this->scss_dir . $input_file;
			$css_file = $this->css_dir . $output_file;

			$css_handle = fopen($css_file, 'w+') or die('Cannot create file: ' . $css_file);
			$scss_handle = fopen($scss_file, 'r') or die('Cannot open file: ' . $scss_file);

			$scss_data = fread($scss_handle, filesize($scss_file));
			$compiled_css = $this->scss->compile($scss_data);

			fwrite($css_handle, $compiled_css) or die('Failed to write data on the file: ' . $css_file);
		}
	}
}