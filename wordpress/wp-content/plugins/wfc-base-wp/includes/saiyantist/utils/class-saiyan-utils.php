<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Utils')) {
	/**
	 * Class Saiyan_Utils
	 *
	 * Class with static function that will be used all over the plugin.
	 *
	 * @author Von Sienard Vibar
	 * @since 1.0.0
	 */
	class Saiyan_Utils {

		/**
		 * Creates directory on wp-contents/upload folder based on the directory name provided.
		 *
		 * @param string $dir_name Directory name
		 * @param null $permission Optional. Permission to be set on the folder
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public static function create_content_upload_dir($dir_name, $permission = null) {
			$upload_dir = wp_upload_dir();
			$upload_base_dir = $upload_dir['basedir'];
			$content_upload_dir = $upload_base_dir . '/' . $dir_name;

			if (! is_dir($content_upload_dir))
				mkdir(
					$content_upload_dir,
					isset($permission) ? $permission : 0777
				);
		}

		/**
		 * Gets the full path of the provided directory name.
		 *
		 * @param string $dir_name Directory name
		 * @param string $type Directory type: dir or url
		 *
		 * @return string Full path of the directory
		 *
		 * @author Von Sienard Vibar
		 * @since 1.0.0
		 */
		public static function get_content_upload_dir($dir_name, $type = 'dir') {
			$upload_dir = wp_upload_dir();
			if (! in_array($type, array('dir', 'url'))) die('Invalid directory type. Use dir or url');
			$upload_base_dir = $upload_dir['base' . $type];
			$content_upload_dir = $upload_base_dir . '/' . $dir_name;

			switch ($type) {
				case 'dir':
					if (is_dir($content_upload_dir))
						return $content_upload_dir;
					else die('Invalid directory ' . $content_upload_dir);
					break;
				case 'url':
					return $content_upload_dir;
					break;
				default:
					return '';
			}
		}

		/**
	     * Name : get_visitor_browser
	     * Description: Gets the current browser of the visitor.
	     * LastUpdated : March 20, 2019
	     * @author Junjie Canonio
	     * @return string //browser of visitor
	     * @since  1.0.0
	     */
		public static function get_visitor_browser() {

			$arr_browsers = ["Firefox", "Chrome", "Safari", "Opera", 
                "MSIE", "Trident", "Edge"];
			$agent = $_SERVER['HTTP_USER_AGENT'];
			 
			$visitor_browser = '';
			foreach ($arr_browsers as $browser) {
			    if (strpos($agent, $browser) !== false) {
			        $visitor_browser = $browser;
			        break;
			    }   
			}
			return $visitor_browser;
		}
	}
}