<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan')) {

	define('MODULES_PATH', 'modules/');

	/**
	 * Class Saiyan
     *
     * Core class that loads and renders Saiyan modules
     *
     * @since 1.0.0
     * @author Von Sienard Vibar
	 */
	class Saiyan {

		/**
         * Holds the instance of itself
         *
		 * @var $instance
		 */
		protected static $instance;

		/**
         * Holds the list of module names found inside the modules folder
         *
		 * @var array List of modules found in the modules folder
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		public static $module_dirs = array();

		public static $configurator;

		private function __construct() {
		    require_once 'class-saiyan-handler.php';
			require_once 'class-saiyan-helpers.php';

			require_once 'modules/class-saiyan-module-base.php';
			// require_once 'modules/class-saiyan-module-interface.php';
			require_once 'modules/class-saiyan-module-container-interface.php';

			require_once 'utils/class-saiyan-utils.php';
			require_once 'utils/sass/class-saiyan-sass-compiler.php';

			$this->load_modules_dir();
		}

		/**
         * Creates and returns the instance of itself if it is not instantiated yet
         *
		 * @return Saiyan
		 */
		public static function tist() {
			if (self::$instance === null) self::$instance = new self();
			return self::$instance;
		}

		/**
         * Function that renders the module mapping
         *
		 * @param array $mapping
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		public function render_module_mapping(array $mapping) {
			$this->styles_and_scripts();
			if (! Saiyan_Helpers::validate_mapping($mapping)) throw new InvalidArgumentException('Invalid mapping');
			?>
            <div class="saiyan-wrapper">
                <?php
                try {
	                foreach ($mapping as $id => $module) {
	                    $mod_obj = null;
		                $mod_obj[$id] = $module;
	                    // print_r(key($mod_obj));
		                Saiyan_Handler::render_module($mod_obj);
	                }
                } catch (Exception $exception) {
	                echo '<b>Error: </b>' . $exception->getMessage() . ' on line ' . $exception->getLine() . ' <small>(' . $exception->getFile() . ')</small>';
                }
                ?>
            </div>
            <?php
		}

		/**
         * A recursive function that renders the module by its type.
         * If the module is a container, then wrap it and render its child modules
         *
		 * @param array $module The module object
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
         *
         * @deprecated Use Saiyan_Handler::render_module() instead
		 */
		private function render_module(array $module) {
			if ( empty( $module ) ) {
				throw new InvalidArgumentException( 'Invalid module mapping, it might be empty or something.' );
			}
			if ( ! Saiyan_Helpers::validate_module_type( $module['type'] ) ) {
				throw new InvalidArgumentException( 'Module type "' . $module['type'] . '" is not valid' );
			}
			if ( ! Saiyan_Helpers::validate_settings( $module['settings'] ) ) {
				throw new InvalidArgumentException( 'Parameters is not an array' );
			}

			$type     = $module['type'];
			$settings = $module['settings'];

			$module_object = null;

			switch ($type) {
				case 'settingsform':
                    $module_object = new Saiyan_Module_settingsform($settings);
                    break;
				case 'datatable':
				    // if ($object === null) throw new InvalidArgumentException('An object is needed to display data for datatable');
					$module_object = new Saiyan_Module_Datatable($settings);
					break;
                case 'card':
                    $module_object = new Saiyan_Module_Card($settings);
                    break;
                case 'text':
	                if (! isset($module['content'])) throw new InvalidArgumentException('Content is empty.');
                    $module_object = new Saiyan_Module_Text($module['content'], $settings);
                    break;
                case 'input':
                    $module_object = new Saiyan_Module_Input($settings);
                    break;
                case 'button':
                    $module_object = new Saiyan_Module_Button($settings);
                    break;

                case 'wpeditor':
                    $module_object = new Saiyan_Module_WPEditor($settings);
                    break;
                case 'wizard':
                    if (! isset($module['steps'])) throw new InvalidArgumentException('Steps is needed for the wizard module');
                    $module_object = new Saiyan_Module_Wizard($module['steps'], $settings);
                    break;

                case 'datetime-picker':
                    $module = new Saiyan_Module_Datetime_Picker();
                    break;
			}

			if ($module_object instanceof ISaiyan_Container) {
			    if ($module['children'] === null && ! is_array($module['children'])) throw new InvalidArgumentException('Children is required for container modules');
			    $children = $module['children'];
			    $module_object->start();
			    $module_object->render_module();
			    foreach ($children as $child_module) {
			        $this->render_module($child_module);
                    // print_r($child_module['params']);
                }
			    $module_object->end();
            }
            else $module_object->render_module();
		}

		// TODO Add functionality for setting module global theme
		public function config(array $configuration) {
		    $configurator = self::$configurator = new Saiyan_Configurator();

		    if (isset($configuration['theme'])) $configurator->set_theme($configuration['theme']);

		    return $this;
        }

		/**
		 * These function must be called at the start during the plugin loads to load necessary hooks needed
         * to run the modules
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		public function load() {
			add_action('wp_ajax_st_settingsform_ajax_controller', array('Saiyan_Module_SettingsForm', 'st_settingsform_ajax_controller'));
			add_action('wp_ajax_st_datatable_ajax_controller', array('Saiyan_Module_Datatable', 'st_datatable_ajax_controller'));
			add_action('wp_ajax_st_item_structure_renderer', array('Saiyan_Module_SortableItem', 'st_item_structure_renderer'));
			add_action('wp_ajax_st_datetime_picker_ajax', array('Saiyan_Module_Datetime_Picker', 'st_datetime_picker_ajax'));

			add_action('admin_enqueue_scripts', array($this, 'enqueue_styles_and_scripts'));
        }

		/**
		 * Loads and stores a list of modules inside the modules directory
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        private function load_modules_dir() {
	        $module_dir = plugin_dir_path(dirname(__FILE__)) . 'saiyantist/modules/';
	        $modules = scandir( $module_dir );
	        foreach ($modules as $module_dir) {

	        	if( $module_dir != '.' && $module_dir != '..'){
	        		$module_dir_pos = strpos($module_dir, '.php');
	        		if($module_dir_pos == false){
				        require_once 'modules/' . $module_dir . '/class-saiyan-module-' . $module_dir . '.php';
			            array_push(self::$module_dirs, $module_dir);
	        		}
		        }
            }
        }

		/**
		 * Register styles and scripts needed in these framework
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        public function enqueue_styles_and_scripts() {
	        /**
	         * Enqueue Saiyan css
	         */
	        wp_register_style(
                'saiyan-css',
                plugin_dir_url(dirname(__FILE__)) . 'saiyantist/assets/css/saiyantist.css'
            );

	        /**
	         * Enqueue Saiyan js
	         */
	        wp_register_script(
		        'saiyan-core-js',
		        plugin_dir_url(dirname(__FILE__)) . 'saiyantist/assets/js/saiyantist-core.js',
		        array('jquery'),
		        '1.0.0',
		        true
	        );

	        if (! wp_style_is('saiyan-bootstrap-css', 'enqueued'))
		        wp_register_style(
			        'saiyan-bootstrap-css',
			        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
		        );
	        if (! wp_script_is('saiyan-bootstrap-js', 'enqueued'))
		        wp_register_script(
			        'saiyan-bootstrap-js',
			        "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
		        );
        }

		/**
		 * Enqueue styles and scripts needed in these framework
         *
         * @since 1.0.0
         * @author Junjie Canonio
		 */
		public static function styles_and_scripts($forcecall = null) {

			if($forcecall == 'force'){
				/**
		         * Enqueue Saiyan css
		         */
		        wp_enqueue_style(
	                'saiyan-css',
	                plugin_dir_url(dirname(__FILE__)) . 'saiyantist/assets/css/saiyantist.css'
	            );

		        /**
		         * Enqueue Saiyan js
		         */
		        wp_enqueue_script(
			        'saiyan-core-js',
			        plugin_dir_url(dirname(__FILE__)) . 'saiyantist/assets/js/saiyantist-core.js',
			        array('jquery'),
			        '1.0.0',
			        true
		        );
			}else{
				wp_enqueue_style( 'saiyan-css' );
				wp_enqueue_style( 'saiyan-bootstrap-css' );

				wp_enqueue_script( 'saiyan-core-js' );
				wp_enqueue_script( 'saiyan-bootstrap-js' );
			}
		}

	}

	/**
	 * Class Saiyan_Configurator
     *
     * Class that stores and modifies configurations that will be used by the framework
	 */
	class Saiyan_Configurator {

		/**
         * Contains default configuration settings
         *
		 * @var array $settings Contains default configuration settings
		 */
	    private $settings = array(
            'theme' => 'wordpress'
        );

	    public function __construct() {
	    }

		/**
         * Setter for the theme configuration
         *
		 * @param string $theme
         * @author Von Sienard Vibar
		 */
	    public function set_theme($theme) {
	        $this->settings['theme'] = $theme;
        }

		/**
         * Getter for the theme configuration
         *
		 * @return mixed
         * @author Von Sienard Vibar
		 */
        public function get_theme() {
	        return $this->settings['theme'];
        }
	}
}