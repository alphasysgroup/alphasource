(function($) {
    $.fn.saiyanTabs = function() {
        const t = $(this);

        t.tabs();

        if (st_tabs.direction === 'vertical')
            t.addClass('ui-tabs-vertical ui-helper-clearfix');
    }
}(jQuery));