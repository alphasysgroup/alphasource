<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Tabs')) {
	/**
	 * Class Saiyan_Module_Tabs
     *
     * Datatable module core class
     *
     * @since 1.0.0
     * @author Junjie Canonio
	 */
	class Saiyan_Module_Tabs extends Saiyan_Module_Base {

		private $defaults = array(
			'id'        => '',
			'class'     => '',
			'name'      => 'saiyan-tabs-name',
			'style'     => '',
			'direction' => 'horizontal',
		);

		private $tabs = array();

		public function __construct($id, array $tabs, $settings = array()) {
			parent::__construct($id, 'tabs', $settings, $this->defaults);

			$this->tabs =  $tabs;
		}

		public function load_assets() {
			
		    wp_enqueue_script('jquery-ui-core');
		    wp_enqueue_script('jquery-ui-tabs');
		    
			wp_enqueue_style('st-module-tabs-css', plugin_dir_url(dirname(__FILE__)) . 'tabs/assets/css/tabs.css');
			wp_enqueue_script('st-module-tabs-js', plugin_dir_url(dirname(__FILE__)) . 'tabs/assets/js/tabs.js', array('jquery'), '1.0.0');

			Saiyan_Helpers::wp_localize_script('st-module-tabs-js', 'st_tabs', array(
				self::$id . '-container' => array(
					'direction' => self::$settings['direction']
				)
			));
		}

		protected function module_construction() {
            ?>
            <label
                id="<?php echo esc_attr(self::$settings['id']) ?>"
                class="saiyan-switch-container"
            >
                <input
                    id="<?php echo esc_attr(self::$id) ?>"
                    name="<?php echo esc_attr(self::$settings['name']) ?>"
                    type="checkbox"
					<?php echo self::$settings['switch'] === 'on' ? 'checked' : ''; ?>
					<?php echo self::$settings['required'] ? 'required' : ''; ?>
                />
                <span class="switch-area">
                    <span class="switch-button"></span>
                </span>
            </label>
            <?php
		}
	}

}