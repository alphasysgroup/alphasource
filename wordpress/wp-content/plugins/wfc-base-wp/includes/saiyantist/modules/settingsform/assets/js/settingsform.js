/**
* @author: Junjie Canonio
*/
(function($) {

    $.fn.st_settings_form= function() {
   		var form = this;
      	var form_id = form.attr('id').replace('-container', '');
   		var formset = $('#'+form_id);

   		var submit_btn = st_settingsform[form.attr('id')].settings.submit_id;
   		$( '#' + submit_btn ).click(function() {
		  	form.submit();		  
		});

   		form.on( "submit", function( event ) {
			event.preventDefault();
			var form_value = formset.serializeArray()
			st_settingsform_submit( form_value );
			SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving</div>',null,'info');
		});
	 

	   	function st_settingsform_submit( form_value ) {
	        return new Promise(function (resolve, reject) {
	            $.ajax({
	                url: st_settingsform[form.attr('id')].ajaxurl,
	                type:'POST',
	                dataType:'json',
	                data:{
	                    'action': 'st_settingsform_ajax_controller', 
	                    nonce: st_settingsform[form.attr('id')].nonce,
	                    'settings': st_settingsform[form.attr('id')].settings, 
	                    'data': form_value
	                },
	                success:function(response){
	                	//console.log(response);
	                    resolve( {
	                        status: 'success',
	                       	response: response
	                    } );
	                    SaiyantistCore.snackbar.hide();  
	                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Successful</div>',3000,'success');

                        $( '#' + submit_btn ).trigger('saiy an-settingsform:success');
	                },
	                error: function(response) {
	                    reject( response );
	                    SaiyantistCore.snackbar.hide(); 
	                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Failed</div>',3000,'error');   
	                }
	            });
	        });
	   	}

   	};



}(jQuery));