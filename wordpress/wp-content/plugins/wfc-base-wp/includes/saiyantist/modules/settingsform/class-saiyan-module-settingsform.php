<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_SettingsForm')) {
	/**
	 * Class Saiyan_Module_SettingsForm
     *
     * Datatable module core class
     *
     * @since 1.0.0
     * @author Junjie Canonio
	 */
	class Saiyan_Module_SettingsForm extends Saiyan_Module_Base implements ISaiyan_Container {

		private $defaults = array(
			'id'         => '',
			'class'      => 'saiyan-settingsform',
			'name'       => 'saiyan-settingsform-name',
			'style'      => '',
			'action'     => '',
			'title'      => '',
			'action'     => '',
			'enctype'    => '',
			'method'     => 'post',
			'wp_option'=> array(
				'enable'     => false,
				'slug_name'	 => '',
			),
			'wp_action'  => null,
			'submit_id'  => '',
            'data'       => array(),
            'dependency' => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'settingsform', $settings, $this->defaults);
		}

		public function start() {

			$settings = self::$settings;

			$attr_id = isset($settings['id']) ? $settings['id'] : 'saiyan-settingsform-id';
			$attr_class = isset($settings['class']) ? 'saiyan-settingsform  container-fluid'.$settings['class'] : 'saiyan-settingsform  container-fluid';
			$attr_name = isset($settings['name']) ? $settings['name'] : 'saiyan-settingsform-name';
			$attr_style = isset($settings['style']) ? $settings['style'] : '';
			$attr_title = isset($settings['title']) ? $settings['title'] : '';

			$attr_action = isset($settings['action']) ? $settings['action'] : '';
			$attr_method = isset($settings['method']) ? $settings['method'] : 'post';

			$attr_enctype = isset($settings['enctype']) ? $settings['enctype'] : '';
			$attr_submit_id = isset($settings['submit_id']) ? $settings['submit_id'] : '';			
			?>


			<div 
				id="<?php echo self::$id.'_container';?>"
				class="<?php echo self::$id.'_container saiyan-module-wrapper';?>"
                <?php parent::print_data_attribs(self::$settings['data']); ?>
			>			
				<?php if(self::$settings['title'] != ''):?>
				<div
					class="<?php echo self::$id.'_title_container';?> wfc_settingsform_title_container"
	                data-saiyan-module="settingsform"
				>	
					<h2><?php echo self::$settings['title']; ?></h2>			
				</div>
				<?php endif;?>
				<form 
					id="<?php echo self::$id; ?>"
					class="<?php echo $attr_class; ?>" 
					name="<?php echo $attr_name; ?>" 
					<?php Saiyan_Helpers::print_style(self::$settings['style']); ?> 
					action="<?php echo $attr_action; ?>" 
					method="<?php echo $attr_method; ?>" 
					<?php if(!empty($attr_enctype)):?>
					enctype="<?php echo $attr_enctype; ?>"
					<?php endif;?>
				>			
					<div class="row">
			<?php
		}

		public function end() {
			?>		
					</div>
				</form>
			</div>
			<?php
		}

		public function load_assets() {
			wp_enqueue_script(
				'st-module-settingsform-js',
				plugin_dir_url(dirname(__FILE__)) . 'settingsform/assets/js/settingsform.js'
			);

			/**
			 * Inline script to bind .saiyanDatatable to its datatable.
             * (Support for multiple datatables in one page.)
			 */
			wp_add_inline_script('st-module-settingsform-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').st_settings_form();');
			
			// wp_localize_script(
			// 	'st-module-settingsform-js',
			// 	'st_settingsform',
			// 	array(
			// 		'nonce'    => wp_create_nonce('ajax-nonce'),
			// 		'ajaxurl'  => admin_url('admin-ajax.php'),
			// 		'settings' => self::$settings,
			// 	)
			// );

			Saiyan_Helpers::wp_localize_script('st-module-settingsform-js', 'st_settingsform', array(
	            self::$id . '-container' => array(
		            'nonce'    => wp_create_nonce('ajax-nonce'),
					'ajaxurl'  => admin_url('admin-ajax.php'),
					'settings' => self::$settings,
	            )
            ));

			wp_enqueue_style(
				'st-module-settingsform-css',
				plugin_dir_url(dirname(__FILE__)) . 'settingsform/assets/css/settingsform.css'
			);
		}

		protected function module_construction() {
		}

		/**
		 * Function that handles ajax requests
         *
         * @since 1.0.0
         * @author Junjie Canonio
		 */
		public static function st_settingsform_ajax_controller( ) {
			$data = isset($_POST['data']) ? $_POST['data'] : array(); 

			$settings = isset($_POST['settings']) ? $_POST['settings'] : array(); 
			$slug_name = 
			isset($settings['wp_option']['slug_name']) ? $settings['wp_option']['slug_name'] : '';
			$wp_action = isset($settings['wp_action']) ? $settings['wp_action'] : '';

			$option_arr = array();

			if(is_array($data)){
				foreach ($data as $key => $value) {
					$option_arr[$value['name']] = stripslashes($value['value']);
				}
				update_option( $slug_name, $option_arr);
			}

	    	echo json_encode(
	    		array(
	    			'settings'   => $settings,
	    			'data'	     => $data,
	    			'option_arr' => $option_arr,	
	    		)
	    	);

			do_action($wp_action);

	    	die();
		}
	}
}