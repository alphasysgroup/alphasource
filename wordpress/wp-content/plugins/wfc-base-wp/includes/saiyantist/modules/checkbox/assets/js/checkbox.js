(function($) {
    $.fn.saiyanCheckbox = function() {
        const c = $(this);

        c.find('input[type=\'checkbox\']').on('change', function() {
            var $this = $(this);
            $this.attr('value', $this.prop('checked') ? 'true' : 'false');
            $this.prev().attr('value', $this.prop('checked') ? 'true' : 'false');
        });
    }
}(jQuery));