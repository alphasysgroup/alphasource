<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Checkbox')) {
	class Saiyan_Module_Checkbox extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'         => '',
			'id'                => '',
			'class'             => 'saiyan-checkbox',
			'name'              => '',
			'gridclass'         => '',
			'style'             => null,
			'checkboxes'        => array(
                /*
				array(
					'title'     => 'Check 1',
					'value'     => 'checkbox-1',
				),
				array(
					'title'     => 'Check 2',
					'value'     => 'checkbox-2'
				),
				array(
					'title'     => 'Check 3',
					'value'     => 'checkbox-3'
				)
                */
			),
            'checked'           => array(
                /*
                'checkbox-1'
                */
            ),
			'label'             => array(
				'hidden'        => false,
				'value'         => 'Label here',
				'position'      => 'top',
			),
			'description'       => '&nbsp;',
            'data'              => array(),
            'dependency'        => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'checkbox', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-checkbox-css', plugin_dir_url(dirname(__FILE__)) . 'checkbox/assets/css/checkbox.css');
			wp_enqueue_script('st-module-checkbox-js', plugin_dir_url(dirname(__FILE__)) . 'checkbox/assets/js/checkbox.js');

			wp_add_inline_script('st-module-checkbox-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanCheckbox();');
		}

		protected function module_construction() {
			$checked = self::$settings['checked'];
            ?>
            <div class="saiyan-grouped-module">
	            <?php foreach (self::$settings['checkboxes'] as $checkbox) : ?>
                    <label class="saiyan-checkbox-item">
                        <span class="checkbox-title"><?php echo esc_html($checkbox['title']); ?></span>
                        <input
                            id="<?php echo esc_attr(self::$id); ?>"
                            type="hidden"
                            name="<?php echo esc_attr(self::$settings['name']); ?>[<?php echo esc_attr($checkbox['value']); ?>]"
                            value="<?php echo in_array($checkbox['value'], $checked) ? 'true' : 'false' ?>"
                        />
                        <input
                            id="<?php echo esc_attr(self::$id); ?>"
                            type="checkbox"
                            name="<?php echo esc_attr(self::$settings['name']); ?>[<?php echo esc_attr($checkbox['value']); ?>]"
                            value="<?php echo in_array($checkbox['value'], $checked) ? 'true' : 'false' ?>"
				            <?php echo in_array($checkbox['value'], $checked) ? 'checked' : '' ?>
                            <?php parent::print_data_attribs(self::$settings['data']); ?>
                        />
                        <span class="checkbox-checkmark"></span>
                    </label>
	            <?php endforeach; ?>
            </div>
            <?php
		}
	}
}