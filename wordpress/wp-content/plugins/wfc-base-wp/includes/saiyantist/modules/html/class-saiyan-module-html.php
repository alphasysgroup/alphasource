<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_HTML')) {
	class Saiyan_Module_HTML extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'class'         => 'saiyan-html',
			'name'          => '',
			'style'         => null,
			'type'          => 'html',
			'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Html',
		        'position'  => 'top',
	        ),
		    'description'   => '',
			'dependency'    => null,
		);

		private $content;

		public function __construct($id, $content, $settings = array()) {
			parent::__construct($id, 'html', $settings, $this->defaults);

			$this->content = $content;
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Junjie Canonio
		 */
		public function load_assets() {
			// TODO: Implement load_assets() method.
		}

		protected function module_construction() {
			// TODO: Implement module_construction() method.
			echo $this->content;
		}
	}
}