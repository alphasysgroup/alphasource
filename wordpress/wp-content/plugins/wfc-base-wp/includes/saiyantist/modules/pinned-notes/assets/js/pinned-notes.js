(function($) {
    $.fn.saiyanPinnedNotes = function() {
        const pn = $(this).find('.pinned-notes');
        const pins = pn.find('.pins');
        const notes = pn.find('.notes');

        const pinOffsets = [];
        pins.find('.pin').each(function() {
            pinOffsets.push($(this).offset().top);
        });

        pins.find('.pin').on('click', function(e) {
            e.stopPropagation();

            var $this = $(this);
            var pinId = $this.attr('data-pin-id');
            var targetNote = notes.find('.note[data-note-id=\'' + pinId + '\']');

            if ($this.hasClass('active')) {
                if (pins.find('.pin.active').length > 0) {
                    $this.removeClass('active');
                    targetNote.removeClass('active');
                }

                pn.removeClass('has-active');
                pins.css('top', '0');
            } else {
                pins.find('.pin.active').removeClass('active');
                notes.find('.note.active').removeClass('active');

                $this.addClass('active');
                targetNote.addClass('active');

                var offset = -pinOffsets[pinId - 1] + 40;
                pn.addClass('has-active');
                pins.css('top', offset + 'px');
            }
        });

        notes.find('.note').on('click', function(e) {
            e.stopPropagation();
        });

        $(document).on('click', function() {
            pn.removeClass('has-active');
            pins.find('.pin').removeClass('active');
            notes.find('.note').removeClass('active');
            pins.css('top', '0');
        })
    }
})(jQuery);