<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_PinnedNotes')) {
	class Saiyan_Module_PinnedNotes extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'    => '',
			'name'  => '',
			'class' => 'saiyan-pinned-notes',
			'style' => null,
            'data'  => array()
		);

		/**
		 * Pins and notes that will be rendered on the accordion.
		 *
		 * @var array Accordion sections
		 */
		private $pins_notes;

		public function __construct( $id, $pins_notes = array(), $settings = array() ) {
			parent::__construct( $id, 'pinned_notes', $settings, $this->defaults );

			if (count($pins_notes) > 5)
				throw new InvalidArgumentException('Pinned notes is upto 5 pins only');

			$this->pins_notes = $pins_notes;
		}

		/**
		 * Method that will contain enqueues for styles and scripts, and anything related to it
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		protected function load_assets() {
			wp_enqueue_style('st-module-pinned-notes-css', plugin_dir_url(dirname(__FILE__)) . 'pinned-notes/assets/css/pinned-notes.css');
			wp_enqueue_script('st-module-pinned-notes-js', plugin_dir_url(dirname(__FILE__)) . 'pinned-notes/assets/js/pinned-notes.js', array('jquery'), '1.0.0');

			wp_add_inline_script('st-module-pinned-notes-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanPinnedNotes();');
		}

		/**
		 * Method that will contain the structure of the module that will be rendered
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		protected function module_construction() {
			?>
			<div class="pinned-notes" <?php parent::print_data_attribs(self::$settings['data']); ?>>
				<div class="pins">
					<?php for ($i=0, $count=count($this->pins_notes); $i<$count; $i++) : ?>
						<?php $pin_type = plugin_dir_url(dirname(__FILE__)) . 'pinned-notes/assets/svg/' . $this->pins_notes[$i]['pin'] . '.svg'; ?>
						<div class="pin" data-pin-id="<?php echo $i + 1; ?>">
							<img src="<?php echo $pin_type; ?>" />
						</div>
					<?php endfor; ?>
				</div>
				<div class="notes">
					<?php for ($i=0, $count=count($this->pins_notes); $i<$count; $i++) : ?>
						<?php $note = $this->pins_notes[$i]['note']; ?>
						<section class="note" data-note-id="<?php echo $i + 1; ?>">
							<h4 class="note-header"><?php echo $note['header']; ?></h4>
							<div class="note-content">
								<?php
								foreach ($note['content'] as $key => $content) {
									$mod_obj = null;
									$mod_obj[$key] = $content;
									Saiyan_Handler::render_module($mod_obj);
								}
								?>
							</div>
						</section>
					<?php endfor; ?>
				</div>
			</div>
			<?php
		}
	}
}