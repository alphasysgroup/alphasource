<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Slider')) {
	class Saiyan_Module_Slider extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'class'         => 'saiyan-slider',
			'gridclass'     => '',
			'name'          => '',
			'style'         => null,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
			'description'   => '&nbsp;',
			'min'           => 1,
			'max'           => 100,
			'value'         => 1,
			'orientation'   => 'horizontal',
            'data'          => array(),
            'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'slider', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-slider-css', plugin_dir_url(dirname(__FILE__)) . 'slider/assets/css/slider.css');
			wp_enqueue_style('jquery-ui-slider');
			wp_enqueue_script('jquery-ui-core');
			wp_enqueue_script('jquery-ui-slider', null, array('jquery'), null, false);
			wp_enqueue_script('st-module-slider-js', plugin_dir_url(dirname(__FILE__)) . 'slider/assets/js/slider.js', array('jquery'), '1.0.0');

			/**
			 * Inline script to bind .saiyanSlider to its slider.
			 * (Support for multiple datatables in one page.)
			 */
			wp_add_inline_script('st-module-slider-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanSlider();');
		}

		protected function module_construction() {
            ?>
            <input
                id="<?php echo esc_attr(self::$id); ?>"
                class="slider"
                type="range"
                name="<?php echo esc_attr(self::$settings['name']); ?>"
				<?php echo isset(self::$settings['min']) ? 'min=\'' . self::$settings['min'] . '\'' : ''; ?>
				<?php echo isset(self::$settings['max']) ? 'max=\'' . self::$settings['max'] . '\'' : ''; ?>
				<?php echo isset(self::$settings['value']) ? 'value=\'' . self::$settings['value'] . '\'' : ''; ?>
				<?php echo 'data-orientation=\'' . self::$settings['orientation'] . '\''; ?>
            />
            <div class="slider-ui"></div>
            <?php
		}
	}
}