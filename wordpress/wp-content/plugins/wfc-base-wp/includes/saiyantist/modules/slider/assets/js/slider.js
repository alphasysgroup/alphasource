(function($) {
    $.fn.saiyanSlider = function() {
        const s = $(this).find('.slider');
        const sui = s.next();

        var min = parseInt(s.attr('min'));
        var max = parseInt(s.attr('max'));
        var val = parseInt(s.val());
        var orientation = s.attr('data-orientation');

        sui.slider({
            range: 'min',
            value: val,
            min: min,
            max: max,
            orientation: orientation,
            start: function(event, ui) {
                $(ui.handle).attr('data-value', ui.value);
            },
            slide: function(event, ui) {
                s.val(ui.value);
                $(ui.handle).attr('data-value', ui.value);
            }
        });
    }
}(jQuery));