<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_WPEditor')) {
	class Saiyan_Module_WPEditor extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'                => '',
			'class'             => 'saiyan-wpeditor',
			'gridclass'         => '',
			'name'              => '',
			'label'             => array(
				'hidden'        => false,
				'value'         => 'Label here',
				'position'      => 'top',
			),
			'description'       => '&nbsp;',
			'content'           => '',
			'wpeditor_settings' => array(),
			'dependency'        => null
		);

		/**
		 * ISaiyan_Module constructor.
		 *
		 * Constructor for Saiyan modules object
		 *
		 * @param array $settings
		 */
		public function __construct($id, $settings = array()) {
		    parent::__construct($id, 'wpeditor', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
		    if (Saiyan::$configurator->get_theme() !== 'wordpress')
			    wp_enqueue_style('st-module-wpeditor-css', plugin_dir_url(dirname(__FILE__)) . 'wpeditor/assets/css/wpeditor.css');

			add_filter('mce_css', function($mce_css) {
				return plugin_dir_url(dirname(__FILE__)) . 'wpeditor/assets/css/editor-style.css';
			});
		}

		protected function module_construction() {
			wp_editor(
				self::$settings['content'],
				self::$id,
				self::$settings['wpeditor_settings']
			);
		}
	}
}