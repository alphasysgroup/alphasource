<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Select')) {
	class Saiyan_Module_Select extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'     => '',
			'id'            => '',
			'class'         => 'saiyan-select',
			'gridclass'     => '',
			'name'          => '',
			'style'         => null,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
			'description'   => '&nbsp;',
			'options'       => array(
                /*
				array(
					'title' => 'Option 1',
					'value' => 1,
                    'disabled' => true
				),
				array(
					'title' => 'Option 2',
					'value' => 2,
                    'disabled' => true
				),
				array(
					'title' => 'Option 3',
					'value' => 3,
                    'disabled' => false
				),
				array(
					'title' => 'Option 4',
					'value' => 4,
				),
				array(
					'title' => 'Option 5',
					'value' => 5,
				)
                */
			),
			'selected'      => '',
            'required'      => false,
            'readonly'      => false,
            'data'          => array(),
            'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'select', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-select-css', plugin_dir_url(dirname(__FILE__)) . 'select/assets/css/select.css');
		}

		function module_construction() {
		    ?>
            <select
                id="<?php echo esc_attr(self::$id); ?>"
                class="<?php echo esc_attr(self::$settings['class']); ?>"
                name="<?php echo esc_attr(self::$settings['name']); ?>"
                <?php echo self::$settings['required'] ? 'required' : '' ?>
                <?php echo self::$settings['readonly'] ? 'readonly' : '' ?>
                <?php Saiyan_Helpers::print_style(self::$settings['style']); ?>
                <?php parent::print_data_attribs(self::$settings['data']) ?>
            >
				<?php foreach (self::$settings['options'] as $option) : ?>
                    <option
                        value="<?php echo esc_attr($option['value']); ?>"
						<?php echo $option['value'] == self::$settings['selected'] ? 'selected' : ''; ?>
                        <?php echo isset($option['disabled']) && $option['disabled'] ? 'disabled' : ''; ?>
                    ><?php echo esc_html($option['title']); ?></option>
				<?php endforeach; ?>
            </select>
            <?php
		}
	}
}