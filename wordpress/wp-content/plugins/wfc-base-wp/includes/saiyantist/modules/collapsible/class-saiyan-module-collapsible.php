<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Collapsible')) {
	class Saiyan_Module_Collapsible extends Saiyan_Module_Base implements ISaiyan_Container {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type' => '',
			'id'        => '',
			'class'     => 'saiyan-collapsible',
			'name'      => '',
			'style'     => null,
			'title'     => 'Title'
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'collapsible', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			// TODO: Implement load_assets() method.
		}

		/**
		 * Contains the markup of the starting tags of the container
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function start() {
			?>
			<div
                class="saiyan-collapsible-container"
                data-saiyan-module="collapsible"
            >
				<div id="<?php echo esc_attr(self::$id); ?>" class="<?php echo esc_attr(self::$settings['class'])  ?>">
			<?php
		}

		/**
		 * Contains the markup of the ending tags of the container
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function end() {
			?>
				</div>
			</div>
			<?php
		}

		protected function module_construction() {
			// TODO: Implement module_construction() method.
            ?>
            <div class="header">
                <i class="fa fa-chevron-down"></i>
                <span><?php echo esc_html(self::$settings['title']) ?></span>
            </div>
            <?php
		}
	}
}