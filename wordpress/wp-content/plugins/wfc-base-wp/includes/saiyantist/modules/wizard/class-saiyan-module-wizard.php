<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Wizard')) {
	class Saiyan_Module_Wizard extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'class'         => 'saiyan-wizard',
			'name'          => '',
			'style'         => null,
			'action'        => '',
			'method'        => 'POST',
			'direction'     => 'horizontal',
            'data'          => array(),
            'dependency'    => null
		);

		private $steps = array();

		/**
		 * ISaiyan_Module constructor.
		 *
		 * Constructor for Saiyan modules object
		 *
		 * @param array $steps
		 * @param array $settings
		 */
		public function __construct($id, array $steps, $settings = array()) {
			parent::__construct($id, 'wizard', $settings, $this->defaults);

			$this->steps = $steps;
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-wizard-css', plugin_dir_url(dirname(__FILE__)) . 'wizard/assets/css/wizard.css');
			wp_enqueue_script('st-module-wizard-js', plugin_dir_url(dirname(__FILE__)) . 'wizard/assets/js/wizard.js', array('jquery'), '1.0.0');
			wp_enqueue_script("jquery-effects-core");

			wp_add_inline_script('st-module-wizard-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanWizard();');
		}

		protected function module_construction() {
            ?>
            <form
                id="<?php echo self::$id; ?>"
                action="<?php echo self::$settings['action']; ?>"
                method="<?php echo self::$settings['method']; ?>"
                <?php parent::print_data_attribs(self::$settings['data']); ?>
            >
                <ul class="tabs">
					<?php for ($i=0, $count=count($this->steps); $i<$count; $i++) : ?>
                        <li class="<?php echo $i == 0 ? 'active' : ''; ?>" data-tab-id="<?php echo $i + 1; ?>"><div><?php echo $this->steps[$i]['title']; ?></div></li>
					<?php endfor; ?>
                </ul>
                <div class="steps">
					<?php for ($i=0, $count=count($this->steps); $i<$count; $i++) : ?>
                        <section class="<?php echo $i == 0 ? 'active' : ''; ?>" data-section-id="<?php echo $i + 1; ?>">
                            <div class="step-header">
								<?php echo $this->steps[$i]['title']; ?>
                            </div>
                            <div class="step-body">
								<?php foreach ($this->steps[$i]['content'] as $content) : ?>
									<?php Saiyan_Handler::render_module($content); ?>
								<?php endforeach; ?>
                            </div>
                            <div class="step-footer">
                                <small>(* Required Fields)</small>
                            </div>
                        </section>
					<?php endfor; ?>
                </div>
                <div class="navigation">
                    <button class="saiyan-btn-primary" type="button" data-step-action="next"><i class="fa fa-chevron-right"></i></button>
                    <button class="saiyan-btn-primary" type="button" data-step-action="prev" disabled><i class="fa fa-chevron-left"></i></button>
                </div>
            </form>
            <?php
		}
	}
}