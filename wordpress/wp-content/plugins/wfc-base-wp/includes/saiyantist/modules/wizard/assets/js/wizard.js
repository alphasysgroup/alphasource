(function($) {
    $.fn.saiyanWizard = function() {
        const w = $(this);
        const t = w.find('.tabs');
        const steps = w.find('.steps');
        const btnStep = w.find('button[data-step-action]');

        btnStep.on('click', function() {
            const $this = $(this);
            var action = $this.attr('data-step-action');

            if (action === 'next' && validateSectionFields()) return false;

            navigator(action);

            if (action !== 'submit') {
                btnUpdate();
                sectionSwitch();
            }
        });

        w.on('click', '.tabs li.done:not(.active)', function() {
            t.find('.active').removeClass('active');
            $(this).addClass('active');

            btnUpdate();
            sectionSwitch();
        });

        function navigator(direction) {
            var curTab = w.find('.tabs li.active');
            
            switch (direction) {
                case 'prev':
                    curTab.removeClass('active').addClass('done');
                    curTab.prev().addClass('active done');
                    break;
                case 'next':
                    curTab.removeClass('active').addClass('done');
                    curTab.next().addClass('active done');
                    break;
            }
        }

        function btnUpdate() {
            var btn = null;
            w.find('button[data-step-action=\'prev\']').prop('disabled', t.find('li').first().hasClass('active'));
            if (t.find('li').last().hasClass('active')) {
                btn = w.find('button[data-step-action=\'next\']');
                btn.attr('type', 'submit').attr('data-step-action', 'submit');
                btn.find('.fa').attr('class', 'fa').addClass('fa-check');
            }
            else {
                btn = w.find('button[data-step-action=\'submit\']');
                btn.attr('type', 'button').attr('data-step-action', 'next');
                btn.find('.fa').attr('class', 'fa').addClass('fa-chevron-right');
            }
        }

        function sectionSwitch() {
            var curSec = steps.find('section.active');

            curSec.hide(400, 'easeOutQuad', function() {
                $(this).removeClass('active');
            });

            var curTab = w.find('.tabs li.active');
            steps.find('section[data-section-id=\'' + curTab.attr('data-tab-id') + '\']').show(400, 'easeOutQuad', function() {
                $(this).addClass('active');
            });
        }

        function validateSectionFields() {
            var isValid = false;
            steps.find('section.active input[required]').each(function() {
                if (! this.reportValidity()) {
                    isValid = true;
                    return false;
                }
            });

            return isValid;
        }
    }
}(jQuery));