<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Radio')) {
	class Saiyan_Module_Radio extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'     => '',
			'id'            => '',
			'class'         => 'saiyan-radio',
			'gridclass'     => '',
			'name'          => '',
			'style'         => null,
			'label'         => array(
				'hidden'    => false,
				'value'     => '',
				'position'  => 'top',
			),
			'description'   => '&nbsp;',
			'radios'        => array(
                /*
				array(
					'title' => 'Radio 1',
					'value' => 'radio-1'
				),
				array(
					'title' => 'Radio 2',
					'value' => 'radio-2'
				),
				array(
					'title' => 'Radio 3',
					'value' => 'radio-3'
				)
                */
			),
			'checked'       => '',
            'readonly'      => false,
            'data'          => array(),
            'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'radio', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-radio-css', plugin_dir_url(dirname(__FILE__)) . 'radio/assets/css/radio.css');
		}

		protected function module_construction() {
            ?>
            <div
                class="saiyan-grouped-module"
                <?php echo self::$settings['readonly'] ? 'readonly' : ''; ?>
                <?php parent::print_data_attribs(self::$settings['data']); ?>
            >
	            <?php foreach (self::$settings['radios'] as $radio) : ?>
                    <label class="saiyan-radio-item">
                        <span class="radio-title"><?php echo $radio['title']; ?></span>
                        <input
                            id="<?php echo esc_attr(self::$id); ?>"
                            type="radio"
                            name="<?php echo esc_attr(self::$settings['name']); ?>"
                            value="<?php echo esc_attr($radio['value']) ?>"
				            <?php echo $radio['value'] == self::$settings['checked'] ? 'checked' : ''; ?>
                        />
                        <span class="radio-checkmark"></span>
                    </label>
	            <?php endforeach; ?>
            </div>
            <?php
		}
	}
}