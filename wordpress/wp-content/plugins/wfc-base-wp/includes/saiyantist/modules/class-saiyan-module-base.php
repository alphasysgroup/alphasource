<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Base')) {
	/**
	 * Class Saiyan_Module_Base
	 *
	 * Parent class of all module classes
	 */
	abstract class Saiyan_Module_Base {

		/**
		 * Variable for storing and retrieving theme that will be used for theming.
		 * Object contains 'main' key for the main color, and 'accent' for the accent/secondary color
		 *
		 * @var array $theme
		 *
		 * @deprecated Theme will be stored and accessed on the Saiyan_Configurator class
		 */
		private $theme = array(
			'main' => 'default',
			'accent' => 'default'
		);

		protected static $id;

		/**
		 * Variable for storing and retrieving settings
		 *
		 * @var array $settings
		 */
		protected static $settings = array();

		private $module_name;

		public function __construct($module_id, $module_name, $settings, $defaults) {
		    // self::$id = $module_id;
		    $this->module_name = $module_name;
		    self::$settings = $this->parse_settings($settings, $defaults);

		    $key = $module_id;
		    $id = self::$settings['id'];

			if (is_int($key) && empty($id))
				throw new InvalidArgumentException('You must set a key and/or ID for the module ' . $this->module_name . '(' . json_encode($settings) . ')');

			if ($key === $id) self::$id = $key;
			if (is_int($key) && ! empty($id)) self::$id = $id;
			if (is_string($key) && empty($id)) self::$id = $key;
		}

		/**
         * Method that will contain enqueues for styles and scripts, and anything related to it
         *
		 * @return mixed
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		protected abstract function load_assets();

		/**
         * Method that will contain the structure of the module that will be rendered
         *
		 * @return mixed
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		protected abstract function module_construction();

		/**
		 * This will render the module that is constructed from the module_construction method.
         * This is called on the Saiyan_Handler::render_module()
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		public final function render_module() {
			$this->load_assets();

			?>
                <div
                    id="<?php echo $this->module_name !== 'wpeditor' ? esc_attr(self::$id) . '-container' : ''; ?>"
                    <?php $this->print_class($this->module_name === 'separator'); // apply no margin for the separator module ?>
                    data-saiyan-module="<?php echo $this->module_name; ?>"
                    <?php $this->print_dependency(); ?>
                    <?php // if module type is input and the input type is none dont display the container ?>
                    <?php echo $this->module_name === 'input' && self::$settings['type'] ==='hidden' ? 'style="display: none;"' : '' ?>
                >
                    <?php if (isset(self::$settings['label'])) : ?>
                        <?php if (self::$settings['label']['position'] === 'top') : ?>
                            <?php $this->render_label(); ?>
                            <div class="saiyan-module">
                                <?php $this->module_construction(); ?>
                            </div>
                            <?php $this->render_description(); ?>
                        <?php elseif (self::$settings['label']['position'] === 'left') : ?>
                            <?php $this->render_label(); ?>
                            <?php $this->render_description(); ?>
                            <div class="saiyan-module">
                                <?php $this->module_construction(); ?>
                            </div>
                        <?php endif; ?>
                    <?php else : ?>
                        <div class="saiyan-module">
                            <?php $this->module_construction(); ?>
                        </div>
                    <?php endif; ?>
                </div>
			<?php
		}

		/**
         * Parses settings from its supplied settings to its set defaults.
         * Supports multi-dimensional array.
         *
		 * @param array $settings Settings to be parse
		 * @param array $defaults Default settings
		 *
		 * @return array Returns array of parsed settings
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		private function parse_settings(&$settings = array(), $defaults = array()) {
		    $result = $defaults;
		    foreach ($settings as $key => &$value) {
		        if (is_array($value) && isset($result[$key]))
		            $result[$key] = $this->parse_settings($value, $result[$key]);
		        else $result[$key] = $value;
            }
            return $result;
        }

		/**
		 * Renders the description view
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		protected function render_description() {
			?>
            <small><?php echo html_entity_decode(self::$settings['description']); ?></small>
			<?php
		}

		/**
		 * Renders the label view
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		protected function render_label() {
            if (self::$settings['label']['hidden'] == false && self::$settings['label']['position'] == 'top') {
                ?>
                <!-- <label for="<?php // echo esc_attr(self::$id);?>" style="width: 100%;"><?php // echo esc_attr(self::$settings['label']['value']);?></label> -->
                <label style="width: 100%;"><?php echo esc_attr(self::$settings['label']['value']);?></label>
                <?php
            }
            if (self::$settings['label']['hidden'] == false && self::$settings['label']['position'] == 'left') {
                ?>
                <!-- <label for="<?php // echo esc_attr(self::$id);?>" style="width: 50%;float: left;"><?php // echo esc_attr(self::$settings['label']['value']);?></label> -->
                <label style="width: 50%;float: left;"><?php echo esc_attr(self::$settings['label']['value']);?></label>
                <?php
            }
        }

		/**
		 * Prints out the class attribute based from the module settings
         *
         * @param bool $no_margin Optional to render with no margin. (Used for the separator module.)
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        private function print_class($no_margin = false) {
		    echo 'class="saiyan-module-wrapper ' . ($no_margin ? 'no-margin ' : ' ') .
                 'saiyan-' . $this->module_name . ' ' . self::$settings['class'] . ' ' .
                 (isset(self::$settings['gridclass']) ? self::$settings['gridclass'] . ' ' : ' ') .
                 Saiyan::$configurator->get_theme() .
                 '"';
        }

		/**
		 * Prints out the dependency attributes based from the module settings
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        private function print_dependency() {
		    if (isset(self::$settings['dependency'])) {
		        if (! is_array(self::$settings['dependency']))
		            throw new InvalidArgumentException('Invalid dependency setting. Should contain id and trigger_value, and optionally trigger_action');

		        if (! isset(self::$settings['dependency']['id']))
		            throw new InvalidArgumentException('ID is needed for the dependency');
		        echo 'data-saiyan-dependency-id="#' . self::$settings['dependency']['id'] . '-container" ';

		        if (! isset(self::$settings['dependency']['trigger_value']))
		            throw new InvalidArgumentException('Trigger Action is needed for the dependency. It should be based on the value of the trigger ID.');
		        echo 'data-saiyan-dependency-trigger-value="' . self::$settings['dependency']['trigger_value'] . '" ';

		        if (isset(self::$settings['dependency']['trigger_action']))
		            echo 'data-saiyan-dependency-trigger-action="' . self::$settings['dependency']['trigger_action'] . '"';
            }
        }

		/**
         * Prints data attribute on the specified element tag that is placed on the module construction.
         *
		 * @param array $data Key-Value combination of data as the key will be appended on a 'data-' string
         * then the value will be the value of that data
         *
         * @author Von Sienard Vibar
         * @since 1.0.0
		 */
        protected function print_data_attribs(array $data) {
            if (empty($data)) return;

            $data_attrib = '';

            foreach ($data as $key => $value) {
                $data_attrib .= 'data-' . $key . '="' . $value . '"';
            }

            echo $data_attrib;
        }
	}
}