(function($) {
    $.fn.saiyanColorPicker = function() {
        const c = $(this).find('.colorpicker');

        c.wpColorPicker({
            defaultColor: st_color_picker[$(this).attr('id')].color
        });

        c.parent().next().trigger('click');
    }
}(jQuery));