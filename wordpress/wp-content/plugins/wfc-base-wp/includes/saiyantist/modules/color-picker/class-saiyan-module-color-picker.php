<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_ColorPicker')) {
	class Saiyan_Module_ColorPicker extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'     => '',
			'id'            => '',
			'class'         => 'saiyan-color-picker',
			'gridclass'     => '',
			'name'          => '',
			'style'         => null,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
			'description'   => '&nbsp;',
			'color'         => '#1283a6',
            'data'          => array(),
            'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'color-picker', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('wp-color-picker');
			wp_enqueue_script('wp-color-picker');

			wp_enqueue_style('st-module-color-picker-css', plugin_dir_url(dirname(__FILE__)) . 'color-picker/assets/css/color-picker.css');
			wp_enqueue_script('st-module-color-picker-js', plugin_dir_url(dirname(__FILE__)) . 'color-picker/assets/js/color-picker.js', array('jquery'), '1.0.0');

			Saiyan_Helpers::wp_localize_script('st-module-color-picker-js', 'st_color_picker', array(
                self::$id . '-container' => array(
                    'color' => self::$settings['color']
                )
            ));

			wp_add_inline_script('st-module-color-picker-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanColorPicker();');
		}

		protected function module_construction() {
            ?>
            <input
                id="<?php echo esc_attr(self::$id) ?>"
                class="colorpicker"
                name="<?php echo esc_attr(self::$settings['name']); ?>"
                <?php parent::print_data_attribs(self::$settings['data']); ?>
            />
            <?php
		}
	}
}