<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Media')) {
	class Saiyan_Module_Media extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'name'          => '',
			'class'         => 'saiyan-media',
			'gridclass'     => '',
			'style'         => null,
			'file_types'    => array('image'),
			'multiple'      => true,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
            'value'         => array(),
            'description'   => '&nbsp;',
            'data'          => array(),
            'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'media', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_media();
			wp_enqueue_style('st-module-media-css', plugin_dir_url(dirname(__FILE__)) . 'media/assets/css/media.css');
			wp_enqueue_script('st-module-media-js', plugin_dir_url(dirname(__FILE__)) . 'media/assets/js/media.js', array('jquery'), '1.0.0');

			Saiyan_Helpers::wp_localize_script('st-module-media-js', 'st_media', array(
				self::$id . '-container' => array(
                    'file_types' => self::$settings['file_types'],
					'multiple' => self::$settings['multiple']
				)
			));

			wp_add_inline_script('st-module-media-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanMedia();');
		}

		protected function module_construction() {
            ?>
            <div
                class="saiyan-media-container <?php echo self::$settings['multiple'] ? 'multiple' : 'single'; ?>"
                <?php parent::print_data_attribs(self::$settings['data']); ?>
            >
                <button
                    id="<?php echo esc_attr(self::$id); ?>"
                    class="mediapicker"
                    type="button">
                    <?php if (empty(self::$settings['value'])) : ?>
                        <i class="fa fa-plus"></i>
                        <input
                            type="hidden"
                            name="<?php echo self::$id; ?>"
                            value="<?php echo self::$settings['value']; ?>" />
                    <?php else : ?>
                        <span class="action">
                            <i class="fa fa-image"></i>
                        </span>
                        <input
                            type="hidden"
                            name="<?php echo self::$id; ?>"
                            value="<?php echo self::$settings['value']; ?>" />
                        <img src="<?php echo wp_get_attachment_thumb_url(self::$settings['value']); ?>" />
                    <?php endif; ?>
                </button>
            </div>
            <?php
		}
	}
}