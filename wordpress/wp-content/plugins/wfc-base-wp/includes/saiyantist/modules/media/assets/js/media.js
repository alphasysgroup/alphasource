(function($) {
    $.fn.saiyanMedia = function() {
        const m = $(this).find('.mediapicker');
        var multiple = st_media[m.attr('id') + '-container'].multiple;

        var mp = wp.media.frames.file_frame = wp.media({
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
            library: {
                type: st_media[m.attr('id') + '-container'].file_types
            },
            multiple: multiple
        });

        /*
        m.on('click', function() {
            mp.open();
        });
        */

        $(document).on('click', '#' + m.attr('id') + '-container .mediapicker', function() {
            mp.open();
        });

        mp.on('open', function() {
            var selection = mp.state().get("selection");

            $('.saiyan-media-item').each(function(index, item) {
                var id = $(item).attr('data-media-id');
                var attachment = wp.media.attachment(id);
                attachment.fetch();
                selection.add(attachment ? [attachment] : []);
            });
        });

        mp.on('select', function() {
            var attachment = mp.state().get('selection').toJSON();

            if (multiple) {
                attachment.forEach(function(item) {
                    if (! isItemExist(item.id)) {
                        $('<div></div>')
                            .addClass('saiyan-media-item')
                            .attr('data-media-id', item.id)
                            .attr('data-media-filename', item.filename)
                            .append(
                                $('<span></span>')
                                    .addClass('action')
                                    .append(
                                        $('<i></i>')
                                            .addClass('fa fa-times')
                                    ),
                                $('<input />')
                                    .attr('type', 'hidden')
                                    .attr('name', m.attr('id') + '-item[]')
                                    .val(item.url),
                                $('<img src=\'' + (item.type === 'image' ? item.sizes.thumbnail.url : item.icon) + '\' />')
                            )
                            .insertBefore(m)
                            .hide()
                            .fadeIn(300);
                    }
                });
            } else {
                var item = attachment[0];
                m.empty();
                m.append(
                    $('<span></span>')
                        .addClass('action')
                        .append(
                            $('<i></i>')
                                .addClass('fa fa-image')
                        ),
                    $('<input />')
                        .attr('type', 'hidden')
                        .attr('name', m.attr('id'))
                        .val(item.id),
                    $('<img src=\'' + (item.type === 'image' ? (typeof item.sizes.thumbnail !== 'undefined' ? item.sizes.thumbnail.url : item.sizes.full.url) : item.icon) + '\' />')
                )
            }

            // added custom event that triggers after the image is selected
            $('body').trigger('saiyan-media:image');
        });

        $(document).on('click', '.saiyan-media-item .action', function() {
            var item = $(this).parent();
            item.fadeOut(500, function() {
                item.remove();
            })
        });

        function isItemExist(itemID) {
            var isItemExist = false;
            $('.saiyan-media-item').each(function(a, b, c) {
                // console.log($(b).attr('data-media-id'));
                if ($(b).attr('data-media-id') == itemID) {
                    isItemExist = true;
                    return false;
                }
            });

            return isItemExist;
        }
    }
}(jQuery));