<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Text')) {
	class Saiyan_Module_Text extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'     => '',
			'id'            => '',
			'class'         => 'saiyan-datatable',
			'name'          => '',
			'style'         => null,
			'type'          => 'p',
			'content'       => 'Content',
            'data'          => array(),
            'dependency'    => null
		);

		private $content;

		public function __construct($id, string $content, $settings = array()) {
			parent::__construct($id, 'text', $settings, $this->defaults);

			$this->content = $content;
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			// TODO: Implement load_assets() method.
		}

		protected function module_construction() {
            ?>
            <p data-saiyan-module="text" <?php parent::print_data_attribs(self::$settings['data']); ?>><?php echo $this->content; ?></p>
            <?php
		}
	}
}