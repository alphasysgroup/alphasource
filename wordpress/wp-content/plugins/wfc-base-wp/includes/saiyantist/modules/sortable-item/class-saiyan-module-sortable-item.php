<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_SortableItem')) {
	class Saiyan_Module_SortableItem extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'             => '',
			'id'                    => '',
			'class'                 => 'saiyan-sortable-item',
			'gridclass'             => '',
			'name'                  => '',
			'style'                 => null,
			'width'                 => '50%',
			'label'                 => array(
				'hidden'            => false,
				'value'             => 'Label here',
				'position'          => 'top',
			),
            'description'           => '&nbsp;',
            'item_title'            => 'Item',
            'base_title_id'         => '',
			'add_item'              => true,
			'add_item_title'        => 'Add Item',
            'mode'                  => 'basic',  // basic, advance
            'item_deletable'        => true,
            'empty_message'         => 'Add Item',
            'data'                  => array(),
            'dependency'            => null
		);

		/**
         * Variable where the structure of the item will be stored
         *
		 * @var array $item_struct
		 */
		private $item_struct;

		/**
         * A copy of $item_struct which will be used for parsing values and indices
         * on its item objects that will be displayed on the main Sortable section area
         *
		 * @var array $based_item_struct
		 */
		private $based_item_struct;

		/**
         * Contains array of values based on the item structure
         *
		 * @var array $item_values
		 */
		private $item_values;

		// list
        // duplicator

		public function __construct($id, $item_struct, $item_values = null, $settings = array()) {
			parent::__construct($id, 'sortable-item', $settings, $this->defaults);

			$this->item_struct = $item_struct;
			$this->based_item_struct = $item_struct;
			$this->item_values = $item_values;
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_script('jquery-ui-sortable');

			wp_enqueue_style('st-module-sortable-item-css', plugin_dir_url(dirname(__FILE__)) . 'sortable-item/assets/css/sortable-item.css');
			wp_enqueue_script('st-module-sortable-item-js', plugin_dir_url(dirname(__FILE__)) . 'sortable-item/assets/js/sortable-item.js', array('jquery'), '1.0.0');

			wp_add_inline_script('st-module-sortable-item-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanSortableItem();');
		}

		protected function module_construction() {
			$id = self::$id;
			$name = self::$settings['name'];
			$width = self::$settings['width'];
			$item_title = self::$settings['item_title'];
			$base_title_id = self::$settings['base_title_id'];
			$add_item = self::$settings['add_item'];
			$add_item_title = self::$settings['add_item_title'];
			$mode = self::$settings['mode'];
			$item_deletable = self::$settings['item_deletable'];
			$empty_message = self::$settings['empty_message'];

			// hold sortable-item settings and use them after items are being rendered
            // these will prevent from overriding module settings
			$hold_settings = self::$settings;

			if (! in_array($mode, array('basic', 'advance')))
				throw new InvalidArgumentException('Invalid mode, the required modes are \'basic\' and \'advance\'');
			?>
            <div
                    class="items <?php echo $mode; ?>"
                    data-saiyan-sortable-size="0"
                    style="width: <?php echo esc_attr($width); ?>"
                    <?php parent::print_data_attribs(self::$settings['data']); ?>
            >
				<?php if ($mode === 'basic') : ?>
					<?php // render basic sortable item using single module ?>
					<?php if ($this->item_values !== null) : ?>
                        <?php for ($i=0, $count=count($this->item_values); $i<$count; $i++) : ?>
                            <div class="item" data-saiyan-sortable-index="<?php echo $this->item_values[$i]['index']; ?>">
                                <?php
                                $key = key($this->item_struct);
                                // print_r(key($this->item_struct));
                                $this->item_struct[$key]['settings'] = wp_parse_args($this->item_values[$i]['settings'], $this->item_struct[$key]['settings']);

                                Saiyan_Handler::render_module($this->item_struct);
                                ?>
                            </div>
                        <?php endfor; ?>
						<?php $this->validate_and_localize_base_title_id($mode, $id, $base_title_id); ?>
					<?php endif; ?>
				<?php elseif ($mode === 'advance') : ?>
					<?php // render advance sortable item using structured item containing multiple modules ?>
					<?php if ($this->item_values != null) : ?>
						<?php for ($i=0, $count=count($this->item_values); $i<$count; $i++) : ?>
							<?php $hold_struct = $this->based_item_struct; ?>
                            <div class="collapsible" data-saiyan-sortable-index="<?php echo $this->item_values[$i]['index']; ?>">
                                <div class="handle-bar">
                                    <button type="button" class="saiyan-btn-primary" data-item-action="expand-collapse"><i class="fa fa-chevron-down"></i></button>
                                    <span><?php echo esc_html($item_title . ' ' . ($i + 1)); ?></span>
                                    <!-- TODO Add functionality for hiding delete button if item is non-deletable -->
                                    <button
                                        type="button"
                                        class="saiyan-btn-primary"
                                        data-item-action="delete"
                                        data-item-deletable="<?php echo isset($this->item_values[$i]['non_deletable']) && $this->item_values[$i]['non_deletable'] ? 'true' : 'false'; ?>"
                                        <?php echo isset($this->item_values[$i]['non_deletable']) && $this->item_values[$i]['non_deletable'] ? 'style=\'opacity: 0.0; pointer-events: none;\'' : ''; ?>
                                    >
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </div>
                                <div class="content" style="display: none;">
                                    <?php /* ?>
									<?php for ($j=0, $item_count=count($this->based_item_struct); $j<$item_count; $j++) : ?>
										<?php $this->parse_value($this->based_item_struct[$j], $this->item_values[$i]['values'][$j]); ?>
										<?php // print_r($this->item_values[$i]); ?>
										<?php $this->based_item_struct[$j]['settings']['id'] .= '-' . $this->item_values[$i]['index']; ?>
										<?php $this->based_item_struct[$j]['settings']['name'] .= '-' . $this->item_values[$i]['index']; ?>
										<?php
										if (isset($this->based_item_struct[$j]['settings']['dependency'])
										    && $this->based_item_struct[$j]['settings']['dependency'] !== null)
											$this->based_item_struct[$j]['settings']['dependency']['id'] .= '-' . $this->item_values[$i]['index'];
										?>
										<?php Saiyan_Handler::render_module($this->based_item_struct[$j]); ?>
									<?php endfor; ?>
                                    <?php */ ?>
                                    <?php
                                    foreach ($this->based_item_struct as $item_key => $item_value) {
                                        // print_r($item_key);
                                        // print_r($item_value);

                                        $this->parse_value($item_value, isset($this->item_values[$i]['values'][$item_key]) ? $this->item_values[$i]['values'][$item_key] : '');

                                        $hold_item_key = $item_key;
                                        $item_key .= '-' .$this->item_values[$i]['index'];
	                                    $item_value['settings']['id'] .= '-' .$this->item_values[$i]['index'];
                                        $item_value['settings']['name'] .= '-' .$this->item_values[$i]['index'];

                                        // prints data attrib on specified module based by module key
	                                    $item_value['settings']['data'] = array();
                                        if (isset($this->item_values[$i]['data'])) {
                                            foreach ($this->item_values[$i]['data'] as $key => $data) {
                                                if ($hold_item_key === $key)
                                                    $item_value['settings']['data'] = $data;
                                            }
                                        }

                                        if (isset($item_value['settings']['dependency'])
                                            && $item_value['settings']['dependency'] !== null)
                                            $item_value['settings']['dependency']['id'] .= '-' . $this->item_values[$i]['index'];

                                        // var_dump(isset($this->item_values[$i]['values'][$item_key]) ? $this->item_values[$i]['values'][$item_key] : '');

                                        $mod_obj = null;
                                        $mod_obj[$item_key] = $item_value;
                                        Saiyan_Handler::render_module($mod_obj);
                                    }
                                    ?>
                                </div>
                            </div>
							<?php $this->based_item_struct = $hold_struct; ?>
						<?php endfor; ?>
                    <?php else : ?>
                        <div class="saiyan-empty-message">
                            <?php echo $empty_message; ?>
                        </div>
					<?php endif; ?>
					<?php $this->validate_and_localize_base_title_id($mode, $id, $base_title_id); ?>
				<?php endif; ?>
            </div>
			<?php if ($mode === 'advance' && $add_item) : ?>
                <div class="add-item-container" style="width: <?php echo esc_attr($width); ?>">
                    <button class="saiyan-btn-primary" type="button">
                        <i class="fa fa-plus"></i>
                        <span class="show"><?php echo esc_html($add_item_title); ?></span>
                        <span>Cancel</span>
                    </button>
                    <div class="item-structure" style="display: none;">
                        <div class="item-structure-wrapper">
							<?php foreach ($this->item_struct as $item_key => $item_part) : ?>
                                <?php
                                $mod_obj = null;
                                $mod_obj[$item_key] = $item_part;
                                ?>
								<?php Saiyan_Handler::render_module($mod_obj); ?>
							<?php endforeach; ?>
                        </div>
                        <button class="saiyan-btn-primary" type="button">
                            <span><?php echo esc_html($add_item_title); ?></span>
                        </button>
                    </div>
                </div>
			<?php endif; ?>
            <input
                id="<?php echo esc_attr($id); ?>"
                type="hidden"
                name="<?php echo esc_attr($name); ?>"
                value="<?php echo esc_attr(json_encode($this->get_item_value_data($mode))); ?>"
            />
			<?php
            self::$settings = $hold_settings;
		}

		/**
		 * Function that handles AJAX requests, used for rendering item based on JSON object
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		public static function st_item_structure_renderer() {
		    $json = stripslashes($_POST['json']);
		    $json_object = json_decode($json, true);
		    // print_r($json_object);

		    // var_dump(json_last_error());

            try {
                ?>
                <div
                    class="collapsible item-collapse"
                    data-saiyan-module="sortable-item"
                    data-saiyan-sortable-index="<?php echo $_POST['index']; ?>"
                >
                    <div class="handle-bar">
                        <button type="button" class="saiyan-btn-primary" data-item-action="expand-collapse"><i class="fa fa-chevron-down"></i></button>
                        <span>Title</span>
                        <button type="button" class="saiyan-btn-primary" data-item-action="delete"><i class="fa fa-trash"></i></button>
                    </div>
                    <div class="content">
                        <?php foreach ($json_object as $module) : ?>
                        <?php // print_r($module); ?>
                        <?php Saiyan_Handler::render_module($module); ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <?php
            } catch (Exception $exception) {
                echo $exception;
            }

		    wp_die();
        }

		/**
         * Validates the base title ID from the settings if it is valid,
         * then localizes the ID to be used on the javascript side.
         *
		 * @param string $base_title_id Base Title ID from the item structure
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        private function validate_and_localize_base_title_id($mode, $id, $base_title_id) {
            if ($mode === 'basic') {
	            Saiyan_Helpers::wp_localize_script('st-module-sortable-item-js', 'st_sortable', array(
		            $id . '-container' => array(
			            'mode' => $mode,
		            )
	            ));
            }
            else {
	            $allowed_field = array('input', 'select', 'radio', 'checkbox', 'textarea');
	            $id_exist = false;
	            $base_title_id = str_replace('[]', '', $base_title_id);
	            foreach ($this->item_struct as $item) {
		            if ($base_title_id === $item['settings']['id']) {
			            if (! in_array($item['type'], $allowed_field))
				            throw new InvalidArgumentException('Selected field not valid for the base title');
			            $id_exist = true;
			            break;
		            }
	            }

	            if ($id_exist) {
		            Saiyan_Helpers::wp_localize_script('st-module-sortable-item-js', 'st_sortable', array(
			            $id . '-container' => array(
				            'mode' => $mode,
				            'base_title_id' => $base_title_id
			            )
		            ));
	            }
	            else throw new InvalidArgumentException('ID \'' . $base_title_id . '\' not found on the item structure');
            }
        }

		/**
         * Gets the array object of the parsed item and values that will be encoded
         * on the module's hidden input.
         *
		 * @param string $mode Sortable Mode (basic|advance)
		 *
		 * @return array Array object of parsed item and values.
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        private function get_item_value_data($mode) {
            $item_vals = array();
            if ($mode === 'basic') {
                switch (reset($this->item_struct)['type']) {
                    case 'checkbox':
	                    for ($i=0, $count=count($this->item_values); $i<$count; $i++) {
	                        $item = null;
	                        // print_r($this->item_values[$i]);
		                    $item[$this->item_values[$i]['settings']['checkboxes'][0]['value']] = array(
                                'title' => $this->item_values[$i]['settings']['checkboxes'][0]['title'],
                                'value' => in_array(
                                            $this->item_values[$i]['settings']['checkboxes'][0]['value'],
                                            (isset($this->item_values[$i]['settings']['checked']) ? $this->item_values[$i]['settings']['checked'] : array())
                                        ) ? 'true' : 'false'
                            );
		                    $item_vals[$i] = $item;
	                    }
                        break;
                }
            }
            else if ($mode === 'advance') {
	            for ($i=0, $count=count($this->item_values); $i<$count; $i++) {
		            $item = null;
		            $iv = null;
		            // print_r($this->item_struct);
		            /*
		            for ($j=0, $item_count=count($this->item_struct); $j<$item_count; $j++) {
			            switch ($this->item_struct[$j]['type']) {
				            case 'switch':
					            $iv[$this->item_struct[$j]['settings']['name']] = $this->item_values[$i]['values'][$j] === 'on' ? 'true' : 'false';
					            break;
				            default:
					            $iv[$this->item_struct[$j]['settings']['name']] = $this->item_values[$i]['values'][$j];
					            break;
			            }
		            }
		            */

		            foreach ($this->item_struct as $item_key => $item_value) {
		                // print_r($item_key);
		                switch ($item_value['type']) {
                            case 'switch':
	                            $iv[$this->item_struct[$item_key]['settings']['name']] = isset($this->item_values[$i]['values'][$item_key]) ? ($this->item_values[$i]['values'][$item_key] === 'on' ? 'true' : 'false') : '';
                                break;
                            default:
	                            $iv[$this->item_struct[$item_key]['settings']['name']] = isset($this->item_values[$i]['values'][$item_key]) ? $this->item_values[$i]['values'][$item_key] : '';
                                break;
                        }
                    }

		            if (isset($this->item_values[$i]['non_deletable']) && $this->item_values[$i]['non_deletable'] == 'true') {
			            $item['item-' . $this->item_values[$i]['index']] = array(
				            'values' => $iv,
				            'non_deletable' => isset($this->item_values[$i]['non_deletable']) ? 'true' : 'false'
			            );
                    }
                    else {
	                    $item['item-' . $this->item_values[$i]['index']] = array(
		                    'values' => $iv
	                    );
                    }

		            $item_vals[$i] = $item;
	            }
            }

            return $item_vals;
        }

		/**
         * Function that parses the value based on its module type
         *
		 * @param array $item References of the module item
		 * @param array|string $value Value to be parsed on the module
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		private function parse_value(&$item, $value) {
		    // print_r($item['settings']);
		    switch ($item['type']) {
                case 'switch':
                    $item['settings']['switch'] = $value;
                    break;
                case 'input':
                    $item['settings']['value'] = $value;
                    break;
                case 'radio':
                    $item['settings']['checked'] = $value;
                    break;
                case 'checkbox':
                    $item['settings']['checked'] = $value;
                    break;
                case 'currency-picker':
                    $item['settings']['value'] = $value;
                    break;
                case 'select':
                    $item['settings']['selected'] = $value;
                    break;
                case 'textarea':
                    $item['settings']['value'] = $value;
                    break;
                case 'media':
                    $item['settings']['value'] = $value;
                    break;
            }
        }

		/**
         * Returns parsed values. Returns the default values if $values is empty or null
         *
		 * @param array $values     Values to be passed
		 * @param array $defaults   Default values
		 *
		 * @return array Parsed values
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        public static function get_value($values, $defaults) {
	        if (isset($values)) {
		        $parsed_values = array();
		        $form_values = json_decode($values, true);
		        for ($i=0, $count=count($form_values); $i<$count; $i++) {
			        $val['index'] = str_replace('item-', '', key($form_values[$i]));
			        $vals = array();
			        foreach ($form_values[$i]['item-' . $val['index']]['values'] as $item_key => $item_values) {
			            if (in_array($item_values, array('true', 'false')))
			                $item_values = $item_values == 'true' ? 'on' : 'false';

                        // array_push($vals, $item_values);
                        $vals[$item_key] = $item_values;
			        }

			        $val['values'] = $vals;

			        if (isset($form_values[$i]['item-' . $val['index']]['non_deletable']) && $form_values[$i]['item-' . $val['index']]['non_deletable'] == 'true') {
				        // echo $form_values[$i]['item-' . $val['index']]['non_deletable'];
				        $val['non_deletable'] = 'true';
                    }

			        array_push($parsed_values, $val);
			        unset($val);
		        }

		        return $parsed_values;
	        }
	        else return $defaults;

	        // return $parsed_values;
        }
	}
}