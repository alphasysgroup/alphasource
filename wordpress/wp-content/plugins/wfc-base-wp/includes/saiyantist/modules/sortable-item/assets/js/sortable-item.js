(function($) {
    $.fn.saiyanSortableItem = function() {
        const s = $(this);
        const items = s.find('.items');
        const a = s.find('.add-item-container');
        const is = a.find('.item-structure');
        var size = parseInt(s.attr('data-saiyan-sortable-size'));
        var emptyMessage = s.find('.saiyan-empty-message');

        // console.log(s.attr('id'));
        // console.log(st_sortable[s.attr('id')]);

        var mode = st_sortable[s.attr('id')].mode;

        s.find('.items').sortable({
            stop: function(event, ui) {
                if (mode === 'basic')
                    storeSerialzedItemBasic();
                else if (mode === 'advance')
                    storeSerializedItemsAdvance();
            }
        });


        if (typeof st_sortable[s.attr('id')] !== 'undefined') {
            var largest = 0;
            s.find('.collapsible').each(function() {
                var index = parseInt($(this).attr('data-saiyan-sortable-index'));
                if (index > largest) largest = index;
            });

            // console.log(largest);

            $('.collapsible .content input[id^=\'' + st_sortable[s.attr('id')].base_title_id + '\'], .collapsible .content select[id^=\'' + st_sortable.base_title_id + '\']').each(function() {
                updateItemTitle($(this));
            }).on('keyup change', function() {
                updateItemTitle($(this));
            });

            a.find('> button').on('click', function() {
                var $this = $(this);
                var fa = $this.find('i.fa');
                var positiveTitle = $this.find('span:first-of-type');
                var negativeTitle = $this.find('span:last-of-type');

                if (! is.is(':visible')) {
                    fa.addClass('cancel');
                    is.slideDown(300, function() {
                        is.addClass('show');
                    });
                    positiveTitle.removeClass('show');
                    negativeTitle.addClass('show');
                }
                else {
                    fa.removeClass('cancel');
                    is.removeClass('show');
                    setTimeout(function() {
                        is.slideUp(300);
                    }, 300);
                    positiveTitle.addClass('show');
                    negativeTitle.removeClass('show');
                }
            });

            // console.log(serializeItemStructure(is.find('[data-saiyan-module]')));

            a.find('.item-structure > button').on('click', function() {
                var $this = $(this);
                if (s.find('.collapsible').length === 0)
                    s.find('.saiyan-empty-message').remove();

                largest++;
                SaiyantistCore.ajaxLoader(
                    ajaxurl,
                    SaiyantistCore.actionType.post,
                    {
                        action: 'st_item_structure_renderer',
                        json: JSON.stringify(serializeItemStructure(is.find('[data-saiyan-module]'))),
                        index: largest
                    },
                    null,
                    function(response) {
                        items.append($(response).hide().fadeIn(300));

                        // updates the st_media object after appending the media module
                        // at the same time binding the saiyanMedia to the appended media module
                        if (typeof st_media !== 'undefined') {
                            var lastMediaModule = items.find('div[data-saiyan-module=\'media\']').last();
                            st_media[lastMediaModule.attr('id')] = st_media[Object.keys(st_media)[0]];

                            lastMediaModule.saiyanMedia();
                        }

                        $('html, body').animate({
                            scrollTop: s.find('.collapsible').last().offset().top - 100
                        }, 800);
                        $('.collapsible .content input[id^=\'' + st_sortable[s.attr('id')].base_title_id + '\'], .collapsible .content select[id^=\'' + st_sortable.base_title_id + '\']').each(function() {
                            updateItemTitle($(this));
                        }).on('keyup change', function() {
                            updateItemTitle($(this));
                        });
                        items.attr('data-saiyan-sortable-size', size+=1);

                        /*
                        $(response).find('.content div[data-saiyan-dependency-id]').each(function() {
                            var $this = $(this);
                            var triggerId = $this.data('saiyanDependencyId');
                            var triggerElem = $(triggerId);

                            console.log(triggerElem);

                            SaiyantistCore.triggerBind($this, triggerElem);
                        });
                        */

                        SaiyantistCore.findAndBind();

                        SaiyantistCore.ajaxLoaderComplete();

                        storeSerializedItemsAdvance();

                        $this.trigger('saiyan-sortableitem:additem');
                    }
                )
            });

            s.on('click', '.collapsible .handle-bar button[data-item-action=\'expand-collapse\']', function() {
                var parent = $(this).parent().parent();
                var content = parent.find('.content');

                // console.log(content);

                if (! content.is(':visible')) {
                    parent.addClass('item-collapse');
                    content.slideDown(300);
                }
                else {
                    parent.removeClass('item-collapse');
                    content.slideUp(300);
                }
            });

            s.on('click', '.collapsible .handle-bar button[data-item-action=\'delete\']', function() {
                var $this = $(this);
                var parent = $this.parent().parent();

                parent.fadeOut(300, function() {
                    $this.trigger('saiyan-sortableitem:deleteitem');

                    parent.remove();

                    storeSerializedItemsAdvance();

                    if (s.find('.collapsible').length === 0)
                        items.append(emptyMessage);
                });

                s.attr('data-saiyan-sortable-size', size-=1);
            });

            items.on('change keyup', 'div[data-saiyan-module] input, div[data-saiyan-module] select, div[data-saiyan-module] textarea', function() {
                if (mode === 'basic')
                    storeSerialzedItemBasic();
                else if (mode === 'advance')
                    storeSerializedItemsAdvance();
            });

            // added custom event that triggers after the image is selected on media module
            // which will update serialized values
            $('body').on('saiyan-media:image', function() {
                if (mode === 'basic')
                    storeSerialzedItemBasic();
                else if (mode === 'advance')
                    storeSerializedItemsAdvance();
            });

            function serializeItemStructure(item) {
                var arr = [];

                item.each(function(index, elem) {
                    var $this = $(elem);
                    var type = $this.attr('data-saiyan-module');
                    var childrenItem = $(elem).find('[data-saiyan-module]');

                    // console.log($this.find('input, select, textarea'));

                    var settings = {};
                    var _field;

                    _field = $this.find('input, select, textarea');
                    // console.log($this.attr('class'));
                    settings['id'] = $this.attr('id').replace('-container', '') + '-' + largest;
                    settings['name'] = _field.attr('name') + '-' + largest;
                    settings['class'] = $this.attr('class');
                    settings['required'] = _field.prop('required');
                    settings['placeholder'] = _field.attr('placeholder');

                    var label = {};
                    label['value'] = $this.find('> label').text();
                    settings['label'] = label;

                    settings['description'] = $this.find('small').text();

                    if (typeof $this.data('saiyanDependencyId') !== 'undefined' && typeof $this.data('saiyanDependencyTriggerValue') !== 'undefined') {
                        var dependency = {};

                        // console.log($this.data('saiyanDependencyId').substr(1, $this.data('saiyanDependencyId').length));

                        dependency['id'] = $this.data('saiyanDependencyId').substr(1, $this.data('saiyanDependencyId').length).replace('-container', '') + '-' + largest;
                        dependency['trigger_value'] = $this.data('saiyanDependencyTriggerValue');

                        if (typeof $this.data('saiyanDependencyTriggerAction') !== 'undefined')
                            dependency['trigger_action'] = $this.data('saiyanDependencyTriggerAction');

                        settings['dependency'] = dependency;
                    }

                    switch (type) {
                        case 'switch':
                            settings['switch'] = _field.is(':checked') ? 'on' : 'off';
                            break;
                        case 'input':
                            settings['type'] = _field.attr('type');
                            settings['min'] = _field.attr('min');
                            settings['max'] = _field.attr('max');
                            settings['value'] = _field.val();
                            break;
                        case 'radio':
                            settings['radios'] = [];
                            settings['checked'] = $this.find('input[type=\'radio\']:checked').val();
                            $this.find('input[type=\'radio\']').each(function() {
                                settings['radios'].push({
                                    'title': $(this).prev().text(),
                                    'value': $(this).val()
                                });
                            });
                            break;
                        case 'checkbox':
                            settings['checkboxes'] = [];
                            settings['checked'] = [];
                            $this.find('input[type=\'checkbox\']').each(function() {
                                settings['checkboxes'].push({
                                    'title': $(this).prev().text(),
                                    'name': $(this).attr('name')
                                });
                            });
                            $this.find('input[type=\'checkbox\']:checked').each(function() {
                                settings['checked'].push($(this).attr('name'));
                            });
                            break;
                        case 'select':
                            settings['options'] = [];
                            _field.find('option').each(function() {
                                settings['options'].push({
                                    'title': $(this).text(),
                                    'value': $(this).val(),
                                    'disabled': $(this).prop('disabled')
                                })
                            });
                            settings['selected'] = _field.val();
                            break;
                        case 'currency-picker':
                            settings['value'] = _field.val();
                            break;
                        case 'textarea':
                            settings['resize'] = _field.attr('class').replace('resize-', '');
                            settings['rows'] = _field.attr('rows');
                            settings['cols'] = _field.attr('cols');
                            settings['maxlength'] = _field.attr('maxlength');
                            settings['value'] = _field.val();
                            break;
                        case 'media':
                            settings['multiple'] = ! $this.find('.saiyan-media-container').hasClass('single');
                            settings['value'] = _field.val();
                            break;
                    }

                    var mod = {};
                    if (childrenItem.length > 0) {
                        var children = serializeItemStructure(childrenItem);
                        /*
                        arr.push({
                            type,
                            settings,
                            children
                        });
                        */
                        mod[$this.attr('id').replace('-container', '') + '-' + largest] = {
                            type,
                            settings,
                            children
                        };
                    }
                    else {
                        /*
                        arr.push({
                            type,
                            settings
                        });
                        */
                        mod[$this.attr('id').replace('-container', '') + '-' + largest] = {
                            type,
                            settings
                        };
                    }

                    arr.push(mod);
                });

                return arr;
            }

            function storeSerialzedItemBasic() {
                var arr = [];
                items.find('.item').each(function() {
                    var $this = $(this);
                    var module = $this.find('div[data-saiyan-module]');

                    var key = null;
                    switch (module.data('saiyanModule')) {
                        case 'checkbox':
                            key = /\[([^)]+)\]/.exec(module.find('input').attr('name'))[1];
                            arr.push({
                                [key]: {
                                    'title': module.find('.checkbox-title').text(),
                                    'value': module.find('input').val()
                                }
                            })
                            break;
                        default:
                            key = module.find('input').attr('name');
                            arr.push({
                                [key]: module.find('input').val()
                            })
                            break;
                    }
                });

                // console.log(arr);

                items.next().attr('value', JSON.stringify(arr));
            }

            function storeSerializedItemsAdvance() {
                var arr = [];
                items.find('.collapsible').each(function() {
                    var $this = $(this);
                    var modules = $this.find('.content div[data-saiyan-module]');
                    var index = $this.data('saiyanSortableIndex');

                    var item = {};
                    var fields = {};

                    modules.each(function() {
                        var type = $(this).data('saiyanModule');
                        var field = $(this).find('input, select, textarea');
                        var name = field.attr('name').replace('-' + index.toString(), '');
                        var value;

                        switch(type) {
                            case 'radio':
                                value = $(this).find('input:checked').val();
                                break;
                            case 'switch':
                                value = $(this).find('input[type=\'checkbox\']').is(':checked') ? 'true' : 'false';
                                break;
                            default:
                                value = field.val();
                                break;
                        }

                        fields[name] = value;
                    });

                    // console.log($this.find('button[data-item-action=\'delete\']').data('itemDeletable'));

                    if ($this.find('button[data-item-action=\'delete\']').data('itemDeletable') === true) {
                        item['item-' + index] = {
                            'values': fields,
                            'non_deletable': 'true'
                        };
                    }
                    else {
                        item['item-' + index] = {
                            'values': fields
                        };
                    }

                    arr.push(item);
                });

                s.find('> .saiyan-module > input[type=\'hidden\']').attr('value', JSON.stringify(arr));
            }

            function updateItemTitle(element) {
                var $this = $(element);
                var parent = $this.parent().parent().parent().parent();

                var text = $this.val();

                if ($this.prop('tagName') == 'SELECT') {
                    text = $this.find('option:selected').text();
                }
                // console.log($this.val());
                parent.find('.handle-bar span').text(text);
            }
        }
    };
}(jQuery));