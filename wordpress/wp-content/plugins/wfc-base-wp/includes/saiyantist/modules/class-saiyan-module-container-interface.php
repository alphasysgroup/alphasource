<?php

/**
 * Interface ISaiyan_Container
 *
 * Default template for the Saiyan modules of a type container
 */
interface ISaiyan_Container {

	/**
	 * Contains the markup of the starting tags of the container
	 *
	 * @return mixed
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public function start();

	/**
	 * Contains the markup of the ending tags of the container
	 *
	 * @return mixed
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public function end();
}