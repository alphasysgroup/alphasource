<?php

/**
 * Interface ISaiyan_Module
 *
 * Default template for the Saiyan modules
 *
 * @since 1.0.0
 * @author Von Sienard Vibar
 */
interface ISaiyan_Module {

	/**
	 * Method to be used on loading its asset.
	 * Should be called inside the constructor of the module object
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public function load_assets();

	/**
	 * Method that will be used to render the markup defined in its body.
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public function render_module();
}