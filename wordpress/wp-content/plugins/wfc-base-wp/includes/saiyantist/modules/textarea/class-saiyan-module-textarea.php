<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Textarea')) {
	class Saiyan_Module_Textarea extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'     => '',
			'id'            => '',
			'class'         => 'saiyan-textarea',
			'name'          => '',
			'style'         => null,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
			'description'   => '&nbsp;',
			'rows'          => 5,
			'cols'          => null,
			'maxlength'     => null,
			'placeholder'   => '',
			'resize'        => 'vertical', // both, none, vertical, horizontal
			'value'         => '',
			'required'      => false,
			'readonly'      => false,
			'data'          => array(),
			'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'textarea', $settings, $this->defaults);
		}

		protected function load_assets() {
			wp_enqueue_style('st-module-textarea-css', plugin_dir_url(dirname(__FILE__)) . 'textarea/assets/css/textarea.css');
		}

		protected function module_construction() {
			?>
			<textarea
                id="<?php echo esc_attr(self::$id); ?>"
				class="resize-<?php echo esc_attr(self::$settings['resize']); ?>"
                name="<?php echo esc_attr(self::$settings['name']) ?>"
                <?php Saiyan_Helpers::print_style(self::$settings['style']); ?>
				<?php echo self::$settings['rows'] !== null ? 'rows="' . esc_attr(self::$settings['rows']) . '" ' : ' '; ?>
				<?php echo self::$settings['cols'] !== null ? 'cols="' . esc_attr(self::$settings['cols']) . '" ' : ' '; ?>
				<?php echo self::$settings['maxlength'] !== null ? 'maxlength="' . esc_attr(self::$settings['cols']) . '" ' : ' '; ?>
				<?php echo self::$settings['placeholder'] !== '' ? 'placeholder="' . esc_attr(self::$settings['placeholder']) . '" ' : ' ';?>
                <?php echo self::$settings['required'] ? 'required' : ''; ?>
                <?php echo self::$settings['readonly'] ? 'readonly' : ''; ?>
                <?php parent::print_data_attribs(self::$settings['data']); ?>
			><?php echo esc_html(self::$settings['value']) ?></textarea>
			<?php
		}
	}
}