<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Accordion')) {
	class Saiyan_Module_Accordion extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type' => '',
			'id'        => '',
			'class'     => 'saiyan-accordion',
			'name'      => '',
			'style'     => null,
		);

		/**
         * Sections that will be rendered on the accordion.
         *
		 * @var array Accordion sections
		 */
		private $section;

		public function __construct($id, $section = array(), $settings = array()) {
			parent::__construct($id, 'accordion', $settings, $this->defaults);

			$this->section = $section;
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
            wp_enqueue_script('jquery-ui-accordion');

			wp_enqueue_style('st-module-accordion-css', plugin_dir_url(dirname(__FILE__)) . 'accordion/assets/css/accordion.css');
			wp_enqueue_script('st-module-accordion-js', plugin_dir_url(dirname(__FILE__)) . 'accordion/assets/js/accordion.js', array('jquery'), '1.0.0');

			wp_add_inline_script('st-module-accordion-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanAccordion();');
		}

		protected function module_construction() {
            ?>
            <div class="accordion">
				<?php foreach ($this->section as $section) : ?>
                    <h3 class="saiyan-accordion-title"><?php echo $section['title']; ?></h3>
                    <div class="saiyan-accordion-wrapper">
						<?php if (isset($section['description']) && $section['description'] !== '') : ?>
                            <h4 class="description"><?php echo $section['description']; ?></h4>
						<?php endif; ?>
                        <div class="content">
							<?php foreach ($section['content'] as $key => $content) : ?>
                                <?php
                                $mod_obj = null;
                                $mod_obj[$key] = $content;
								Saiyan_Handler::render_module($mod_obj);
								?>
							<?php endforeach; ?>
                        </div>
                    </div>
				<?php endforeach; ?>
            </div>
            <?php
		}
	}
}