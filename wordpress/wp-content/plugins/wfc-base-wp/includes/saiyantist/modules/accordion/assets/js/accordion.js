(function($) {
    $.fn.saiyanAccordion = function() {
        const a = $(this).find('.accordion');

        a.accordion({
            heightStyle: "content"
        });
    }
}(jQuery));