<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Switch')) {
	class Saiyan_Module_Switch extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'class'         => 'saiyan-switch',
			'name'          => 'saiyan-switch-name',
			'style'         => null,
            'switch'        => 'off',
            'gridclass'     => '',
            'required'      => false,
            'readonly'      => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
            'description'   => '&nbsp;',
            'data'          => array(),
            'dependency'    => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'switch', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-switch-css', plugin_dir_url(dirname(__FILE__)) . 'switch/assets/css/switch.css');
			wp_enqueue_script('st-module-switch-js', plugin_dir_url(dirname(__FILE__)) . 'switch/assets/js/switch.js');

			wp_add_inline_script('st-module-switch-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanSwitch();');
		}

		protected function module_construction() {
            ?>
            <label
                id="<?php echo esc_attr(self::$id) ?>"
                class="saiyan-switch-container"
	            <?php echo self::$settings['readonly'] ? 'readonly' : ''; ?>
            >
                <input

                    name="<?php echo esc_attr(self::$settings['name']) ?>"
                    type="hidden"
                    value="<?php echo self::$settings['switch'] === 'on' ? 'true' : 'false'; ?>"
                />
                <input
                    id="<?php echo esc_attr(self::$id); ?>"
                    name="<?php echo esc_attr(self::$settings['name']) ?>"
                    type="checkbox"
                    value="true"
					<?php echo self::$settings['switch'] === 'on' ? 'checked' : ''; ?>
					<?php echo self::$settings['required'] ? 'required' : ''; ?>
                    <?php parent::print_data_attribs(self::$settings['data']) ?>
                />
                <span class="switch-area">
                    <span class="switch-button"></span>
                </span>
            </label>
            <?php
		}
	}
}