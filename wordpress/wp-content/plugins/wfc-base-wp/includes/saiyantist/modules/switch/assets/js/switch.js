(function($) {
    $.fn.saiyanSwitch = function() {
        const s = $(this);

        s.find('input[type=\'checkbox\']').on('change', function() {
            var $this = $(this);

            $this.prev().attr('value', $this.prop('checked') ? 'true' : 'false');
        })
    }
}(jQuery));