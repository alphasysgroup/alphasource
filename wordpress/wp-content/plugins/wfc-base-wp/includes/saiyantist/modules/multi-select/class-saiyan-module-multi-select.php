<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_MultiSelect')) {
    class Saiyan_Module_MultiSelect extends Saiyan_Module_Base {

        /**
         * Default settings for the module
         *
         * @var array $defaults
         */
        private $defaults = array(
            'post_type'     => '',
            'id'            => '',
            'class'         => 'saiyan-select',
            'gridclass'     => '',
            'name'          => '',
            'style'         => null,
            'label'         => array(
                'hidden'    => false,
                'value'     => 'Label here',
                'position'  => 'top',
            ),
            'description'   => '&nbsp;',
            'options'       => array(
                /*
				array(
					'title' => 'Option 1',
					'value' => 1,
                    'disabled' => true
				),
				array(
					'title' => 'Option 2',
					'value' => 2,
                    'disabled' => true
				),
				array(
					'title' => 'Option 3',
					'value' => 3,
                    'disabled' => false
				),
				array(
					'title' => 'Option 4',
					'value' => 4,
				),
				array(
					'title' => 'Option 5',
					'value' => 5,
				)
                */
            ),
            'selected'      => array(),
            'required'      => false,
            'readonly'      => false,
            'data'          => array(),
            'dependency'    => null
        );

        public function __construct($id, $settings = array()) {
            parent::__construct($id, 'multi-select', $settings, $this->defaults);
        }

        /**
         * Method that will contain enqueues for styles and scripts, and anything related to it
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
         */
        protected function load_assets() {
            wp_enqueue_style(
                'st-module-multi-select-select2-css',
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css'
            );
            wp_enqueue_style(
                'st-module-multi-select-css',
                plugin_dir_url(dirname(__FILE__)) . 'multi-select/assets/css/multi-select.css'
            );

            wp_enqueue_script(
                'st-module-multi-select-select2-js',
                'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js',
                array(),
                '',
                true
            );
            wp_enqueue_script(
                'st-module-multi-select-js',
                plugin_dir_url(dirname(__FILE__)) . 'multi-select/assets/js/multi-select.js',
                array('jquery', 'st-module-multi-select-select2-js'),
                '',
                true
            );

            Saiyan_Helpers::wp_localize_script('st-module-multi-select-js', 'st_multi_select', array(
                self::$id . '-container' => array(
                    // 'date_format' => self::$settings['date_format']
                )
            ));

            // wp_add_inline_script('st-module-multi-select-select2-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanMultiSelect();');
        }

        /**
         * Method that will contain the structure of the module that will be rendered
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
         */
        protected function module_construction() {
            if (empty(self::$settings['selected'])) {
                self::$settings['selected'] = array();
            }
            ?>
            <select
                id="<?php echo esc_attr(self::$id); ?>"
                class="<?php echo esc_attr(self::$settings['class']); ?>"
                name="<?php echo esc_attr(self::$settings['name']); ?>"
                <?php echo self::$settings['required'] ? 'required' : '' ?>
                <?php echo self::$settings['readonly'] ? 'readonly' : '' ?>
                <?php Saiyan_Helpers::print_style(self::$settings['style']); ?>
                <?php parent::print_data_attribs(self::$settings['data']) ?>
                multiple
            >
                <?php foreach (self::$settings['options'] as $option) : ?>
                    <option
                        value="<?php echo esc_attr($option['value']); ?>"
                        <?php echo in_array($option['value'], self::$settings['selected']) ? 'selected' : ''; ?>
                        <?php echo isset($option['disabled']) && $option['disabled'] ? 'disabled' : ''; ?>
                    ><?php echo esc_html($option['title']); ?></option>
                <?php endforeach; ?>
            </select>
            <script>
                jQuery(document).ready(function($) {
                    $('#<?php echo esc_attr(self::$id) ?>-container').saiyanMultiSelect();
                });
            </script>
            <?php
        }
    }
}