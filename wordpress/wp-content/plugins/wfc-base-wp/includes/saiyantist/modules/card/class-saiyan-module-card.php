<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Card')) {
	/**
	 * Class Saiyan_Module_Card
	 *
	 * Card module core class.
	 * A container type module should implement ISaiyan_Container interface
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	class Saiyan_Module_Card extends Saiyan_Module_Base implements ISaiyan_Container {

		private $defaults = array(
			'id'    => '',
			'class' => 'saiyan-card',
			'name'  => '',
			'style' => '',
			'title' => 'Title'
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'card', $settings, $this->defaults);
		}

		public function start() {
			?>
			<div
                id="<?php echo esc_attr(self::$id); ?>"
                class="saiyan-card" <?php Saiyan_Helpers::print_style(self::$settings['style']); ?>
                data-saiyan-module="card"
            >
				<div class="saiyan-card-content">
			<?php
		}

		public function end() {
			?>
				</div>
			</div>
			<?php
		}

		public function load_assets() {
			wp_enqueue_style(
				'st-module-card-css',
				plugin_dir_url(dirname(__FILE__)) . 'card/assets/css/card.css'
			);
		}

		/*
		public function render_module() {
			?>
			<h2><?php echo self::$settings['title']; ?></h2>
			<?php
		}
		*/

		protected function module_construction() {
            ?>
            <h2><?php echo self::$settings['title']; ?></h2>
            <?php
		}
	}
}