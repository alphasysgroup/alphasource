(function($) {
    $.fn.saiyanDatePicker = function() {
        const d = $(this).find('.date-picker');
        const t = d.next();
        const cal = $(this).find('.saiyan-date-picker-icon');
        const c = $(this).find('button.saiyan-date-picker-clear');

        d.datepicker({
            firstDay: 0,
            dateFormat: st_date_picker[$(this).attr('id')].date_format,
            beforeShow: function(input, inst) {
                $('#ui-datepicker-div').addClass('saiyan-date-picker');
            },
            onSelect: function(dateText, inst) {
                let timestamp = Math.floor(Date.parse(dateText)/1000.0);

                t.val(timestamp);
                c.addClass('active');
            }
        });

        cal.on('click', function() {
            d.datepicker('show');
        });

        c.on('click', function() {
            d.val('');
            t.val('');

            c.removeClass('active');
        });
    }
}(jQuery));