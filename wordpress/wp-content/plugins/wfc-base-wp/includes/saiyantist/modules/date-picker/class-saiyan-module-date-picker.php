<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Date_Picker')) {
    class Saiyan_Module_Date_Picker extends Saiyan_Module_Base {

        private $defaults = array(
            'id'            => '',
            'name'          => 'st-date-picker',
            'class'         => 'st-date-picker',
            'style'         => null,
            /*
            'type' => array(
                'Bootstrap' => 'bootstrap',
                'Propeller' => 'propeller'
            ),*/
            'placeholder'   => '',
            'date_format'   => 'yy-mm-dd',
            'value'         => '',
            'data'          => array(),
            'dependency'    => null,
        );

        public function __construct($id,  $settings = array() ) {
            parent::__construct($id,  'date-picker', $settings, $this->defaults );
            self::$settings['class'] .= ' ' . $this->defaults['class'] . ' ' . Saiyan::$configurator->get_theme();

            $this->load_assets();

        }

        /**
         * Method that will contain enqueues for styles and scripts, and anything related to it
         *
         * @return mixed
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
         */
        protected function load_assets() {
            // TODO: Implement load_assets() method.

            wp_enqueue_style(
                'jquery-ui-date-picker',
                // 'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css',
                plugin_dir_url(dirname(__FILE__)) . 'date-picker/assets/css/jquery-ui.css',
                array(),
                '1.0.0',
                'all'
            );
            wp_enqueue_style('st-module-date-picker-js', plugin_dir_url(dirname(__FILE__)) . 'date-picker/assets/css/date-picker.css', array(), '1.0.0', 'all');

            wp_enqueue_script('jquery-ui-datepicker');
            wp_enqueue_script('st-module-date-picker-js', plugin_dir_url(dirname(__FILE__)) . 'date-picker/assets/js/date-picker.js', array('jquery'), '1.0.0');

            Saiyan_Helpers::wp_localize_script('st-module-date-picker-js', 'st_date_picker', array(
                self::$id . '-container' => array(
                    'date_format' => self::$settings['date_format']
                )
            ));

            wp_add_inline_script('st-module-date-picker-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanDatePicker();');
        }

        /**
         * Method that will contain the structure of the module that will be rendered
         *
         * @return mixed
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
         */
        protected function module_construction() {
            // TODO: Implement module_construction() method.
            ?>
            <div class="saiyan-date-picker-container">
                <div class="saiyan-date-picker-icon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input
                        id="<?php esc_attr_e(self::$id); ?>"
                        class="date-picker"
                        type="text"
                        name="<?php esc_attr_e(self::$settings['name']); ?>"
                        value="<?php esc_attr_e(self::$settings['value']); ?>"
                        placeholder="<?php esc_attr_e(self::$settings['placeholder']); ?>"
                        readonly />
                <input
                        id="<?php esc_attr_e(self::$id . '-timestamp'); ?>"
                        class="date-picker-timestamp"
                        type="hidden"
                        name="<?php esc_attr_e(self::$settings['name'] . '-timestamp'); ?>"
                        value="<?php esc_attr_e(strtotime(self::$settings['value'])); ?>" />
                <button class="saiyan-date-picker-clear" type="button">
                    <i class="fa fa-close"></i>
                </button>
            </div>
            <?php
        }
    }
}