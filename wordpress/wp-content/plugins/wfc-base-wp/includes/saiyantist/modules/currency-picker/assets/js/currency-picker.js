(function($) {
    $.fn.saiyanCurrencyPicker = function() {
        const c = $(this);
        var code = c.find('select:first-of-type');
        var country = c.find('select:nth-of-type(2)');
        var symbol = c.find('select:last-of-type');

        code.on('change', function() {
            update($(this).val());
        });

        country.on('change', function() {
            update($(this).val());
        });

        symbol.on('change', function() {
            update($(this).val());
        });

        function update(value) {
            code.val(value);
            country.val(value);
            symbol.val(value);
        }
    }
}(jQuery));