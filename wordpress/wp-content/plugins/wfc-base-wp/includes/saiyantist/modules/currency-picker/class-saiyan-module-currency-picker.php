<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_CurrencyPicker')) {
	class Saiyan_Module_CurrencyPicker extends Saiyan_Module_Base {

		/**
		 * Currency array object
		 *
		 * @var array
		 */
		public $currency = array(
			'AED' => array(
				'country' => 'United Arab Emirates', // ?
				'symbol' => '&#1583;.&#1573;', // ?
			),
			'AFN' => array(
				'country' => 'Afghanistan',
				'symbol' => '&#65;&#102;'
			),
			'ALL' => array(
				'country' => 'Albania',
				'symbol' => '&#76;&#101;&#107;'
			),
			'AMD' => array(
				'country' => 'Armenia',
				'symbol' => ''
			),
			'ANG' => array(
				'country' => 'Netherlands Antilles',
				'symbol' => '&#402;'
			),
			'AOA' => array(
				'country' => 'Angola', // ?
				'symbol' => '&#75;&#122;' // ?
			),
			'ARS' => array(
				'country' => 'Argentina',
				'symbol' => '&#36;'
			),
			'AUD' => array(
				'country' => 'Australia',
				'symbol' => '&#36;'
			),
			'AWG' => array(
				'country' => 'Aruba',
				'symbol' => '&#402;'
			),
			'AZN' => array(
				'country' => 'Azerbaijan',
				'symbol' => '&#1084;&#1072;&#1085;'
			),
			'BAM' => array(
				'country' => 'Bosnia-Herzegovina',
				'symbol' => '&#75;&#77;'
			),
			'BBD' => array(
				'country' => 'Barbados',
				'symbol' => '&#36;'
			),
			'BDT' => array(
				'country' => 'Bangladesh', // ?
				'symbol' => '&#2547;'
			),
			'BGN' => array(
				'country' => 'Bulgaria',
				'symbol' => '&#1083;&#1074;'
			),
			'BHD' => array(
				'country' => 'Bahrain', // ?
				'symbol' => '.&#1583;.&#1576;' // ?
			),
			'BIF' => array(
				'country' => 'Burundi', // ?
				'symbol' => '&#70;&#66;&#117;' // ?
			),
			'BMD' => array(
				'country' => 'Bermuda',
				'symbol' => '&#36;'
			),
			'BND' => array(
				'country' => 'Brunei Darussalam',
				'symbol' => '&#36;'
			),
			'BOB' => array(
				'country' => 'Bolivia',
				'symbol' => '&#36;&#98;'
			),
			'BRL' => array(
				'country' => 'Brazil',
				'symbol' => '&#82;&#36;'
			),
			'BSD' => array(
				'country' => 'Bahamas',
				'symbol' => '&#36;'
			),
			'BTN' => array(
				'country' => 'Bhutan', // ?
				'symbol' => '&#78;&#117;&#46;' // ?
			),
			'BWP' => array(
				'country' => 'Botswana',
				'symbol' => '&#80;'
			),
			'BYR' => array(
				'country' => 'Belarus',
				'symbol' => '&#112;&#46;'
			),
			'BZD' => array(
				'country' => 'Belize',
				'symbol' => '&#66;&#90;&#36;'
			),
			'CAD' => array(
				'country' => 'Canada',
				'symbol' => '&#36;'
			),
			'CDF' => array(
				'country' => 'Congo, Dem. Republic',
				'symbol' => '&#70;&#67;'
			),
			'CHF' => array(
				'country' => 'Liechtenstein',
				'symbol' => '&#67;&#72;&#70;'
			),
			'CLF' => array(
				'country' => '', // ?
				'symbol' => '' // ?
			),
			'CLP' => array(
				'country' => 'Chile',
				'symbol' => '&#36;'
			),
			'CNY' => array(
				'country' => 'China',
				'symbol' => '&#165;'
			),
			'COP' => array(
				'country' => 'Colombia;',
				'symbol' => '&#36;'
			),
			'CRC' => array(
				'country' => 'Costa Rica',
				'symbol' => '&#8353;'
			),
			'CUP' => array(
				'country' => 'Cuba',
				'symbol' => '&#8396;'
			),
			'CVE' => array(
				'country' => 'Cape Verde', // ?
				'symbol' => '&#36;' // ?
			),
			'CZK' => array(
				'country' => 'Czech Rep.',
				'symbol' => '&#75;&#269;'
			),
			'DJF' => array(
				'country' => 'Djibouti', // ?
				'symbol' => '&#70;&#100;&#106;' // ?
			),
			'DKK' => array(
				'country' => 'Faroe Islands',
				'symbol' => '&#107;&#114;'
			),
			'DOP' => array(
				'country' => 'Dominican Republic',
				'symbol' => '&#82;&#68;&#36;'
			),
			'DZD' => array(
				'country' => 'Algeria', // ?
				'symbol' => '&#1583;&#1580;' // ?
			),
			'EGP' => array(
				'country' => 'Egypt',
				'symbol' => '&#163;'
			),
			'ETB' => array(
				'country' => 'Ethiopia',
				'symbol' => '&#66;&#114;'
			),
			'EUR' => array(
				'country' => 'European Union',
				'symbol' => '&#8364;'
			),
			'FJD' => array(
				'country' => 'Fiji',
				'symbol' => '&#36;'
			),
			'FKP' => array(
				'country' => 'Falkland Islands (Malvinas)',
				'symbol' => '&#163;'
			),
			'GBP' => array(
				'country' => 'Great Britain',
				'symbol' => '&#163;'
			),
			'GEL' => array(
				'country' => 'Georgia', // ?
				'symbol' => '&#4314;' // ?
			),
			'GHS' => array(
				'country' => 'Ghana',
				'symbol' => '&#162;'
			),
			'GIP' => array(
				'country' => 'Gibraltar',
				'symbol' => '&#163;'
			),
			'GMD' => array(
				'country' => 'Gambia', // ?
				'symbol' => '&#68;' // ?
			),
			'GNF' => array(
				'country' => 'Guinea', // ?
				'symbol' => '&#70;&#71;' // ?
			),
			'GTQ' => array(
				'country' => '',
				'symbol' => '&#81;'
			),
			'GYD' => array(
				'country' => 'Guyana',
				'symbol' => '&#36;'
			),
			'HKD' => array(
				'country' => 'Hong Kong',
				'symbol' => '&#36;'
			),
			'HNL' => array(
				'country' => 'Honduras',
				'symbol' => '&#76;'
			),
			'HRK' => array(
				'country' => 'Croatia',
				'symbol' => '&#107;&#110;'
			),
			'HTG' => array(
				'country' => 'Haiti', // ?
				'symbol' => '&#71;' // ?
			),
			'HUF' => array(
				'country' => 'Hungary',
				'symbol' => '&#70;&#116;'
			),
			'IDR' => array(
				'country' => 'Indonesia',
				'symbol' => '&#82;&#112;'
			),
			'ILS' => array(
				'country' => 'Israel',
				'symbol' => '&#8362;'
			),
			'INR' => array(
				'country' => 'India',
				'symbol' => '&#8377;'
			),
			'IQD' => array(
				'country' => 'Iraq', // ?
				'symbol' => '&#1593;.&#1583;' // ?
			),
			'IRR' => array(
				'country' => 'Iran',
				'symbol' => '&#65020;'
			),
			'ISK' => array(
				'country' => 'Iceland',
				'symbol' => '&#107;&#114;'
			),
			'JEP' => array(
				'country' => '',
				'symbol' => '&#163;'
			),
			'JMD' => array(
				'country' => 'Jamaica',
				'symbol' => '&#74;&#36;'
			),
			'JOD' => array(
				'country' => 'Jordan', // ?
				'symbol' => '&#74;&#68;' // ?
			),
			'JPY' => array(
				'country' => 'Japan',
				'symbol' => '&#165;'
			),
			'KES' => array(
				'country' => 'Kenya', // ?
				'symbol' => '&#75;&#83;&#104;' // ?
			),
			'KGS' => array(
				'country' => 'Kyrgyzstan',
				'symbol' => '&#1083;&#1074;'
			),
			'KHR' => array(
				'country' => 'Cambodia',
				'symbol' => '&#6107;'
			),
			'KMF' => array(
				'country' => 'Comoros', // ?
				'symbol' => '&#67;&#70;' // ?
			),
			'KPW' => array(
				'country' => 'Korea-North',
				'symbol' => '&#8361;'
			),
			'KRW' => array(
				'country' => 'Korea-South',
				'symbol' => '&#8361;'
			),
			'KWD' => array(
				'country' => 'Kuwait', // ?
				'symbol' => '&#1583;.&#1603;' // ?
			),
			'KYD' => array(
				'country' => 'Cayman Islands',
				'symbol' => '&#36;'
			),
			'KZT' => array(
				'country' => 'Kazakhstan',
				'symbol' => '&#1083;&#1074;'
			),
			'LAK' => array(
				'country' => 'Laos',
				'symbol' => '&#8365;'
			),
			'LBP' => array(
				'country' => 'Lebanon',
				'symbol' => '&#163;'
			),
			'LKR' => array(
				'country' => 'Sri Lanka',
				'symbol' => '&#8360;'
			),
			'LRD' => array(
				'country' => 'Liberia',
				'symbol' => '&#36;'
			),
			'LSL' => array(
				'country' => 'Lesotho', // ?
				'symbol' => '&#76;' // ?
			),
			'LTL' => array(
				'country' => 'Lithuania',
				'symbol' => '&#76;&#116;'
			),
			'LVL' => array(
				'country' => 'Latvia',
				'symbol' => '&#76;&#115;'
			),
			'LYD' => array(
				'country' => 'Libya', // ?
				'symbol' => '&#1604;.&#1583;' // ?
			),
			'MAD' => array(
				'country' => 'Morocco', //?
				'symbol' => '&#1583;.&#1605;.' //?
			),
			'MDL' => array(
				'country' => 'Moldova',
				'symbol' => '&#76;'
			),
			'MGA' => array(
				'country' => '', // ?
				'symbol' => '&#65;&#114;' // ?
			),
			'MKD' => array(
				'country' => 'Macedonia',
				'symbol' => '&#1076;&#1077;&#1085;'
			),
			'MMK' => array(
				'country' => 'Myanmar',
				'symbol' => '&#75;'
			),
			'MNT' => array(
				'country' => 'Mongolia',
				'symbol' => '&#8366;'
			),
			'MOP' => array(
				'country' => 'Macau', // ?
				'symbol' => '&#77;&#79;&#80;&#36;' // ?
			),
			'MRO' => array(
				'country' => 'Mauritania', // ?
				'symbol' => '&#85;&#77;' // ?
			),
			'MUR' => array(
				'country' => 'Mauritius', // ?
				'symbol' => '&#8360;' // ?
			),
			'MVR' => array(
				'country' => 'Maldives', // ?
				'symbol' => '&#1923;' // ?
			),
			'MWK' => array(
				'country' => 'Malawi',
				'symbol' => '&#77;&#75;'
			),
			'MXN' => array(
				'country' => 'Mexico',
				'symbol' => '&#36;'
			),
			'MYR' => array(
				'country' => 'Malaysia',
				'symbol' => '&#82;&#77;'
			),
			'MZN' => array(
				'country' => 'Mozambique',
				'symbol' => '&#77;&#84;'
			),
			'NAD' => array(
				'country' => 'Namibia',
				'symbol' => '&#36;'
			),
			'NGN' => array(
				'country' => 'Nigeria',
				'symbol' => '&#8358;'
			),
			'NIO' => array(
				'country' => 'Nicaragua',
				'symbol' => '&#67;&#36;'
			),
			'NOK' => array(
				'country' => 'Norway',
				'symbol' => '&#107;&#114;'
			),
			'NPR' => array(
				'country' => 'Nepal',
				'symbol' => '&#8360;'
			),
			'NZD' => array(
				'country' => 'New Zealand',
				'symbol' => '&#36;'
			),
			'OMR' => array(
				'country' => 'Oman',
				'symbol' => '&#65020;'
			),
			'PAB' => array(
				'country' => 'Panama',
				'symbol' => '&#66;&#47;&#46;'
			),
			'PEN' => array(
				'country' => 'Peru',
				'symbol' => '&#83;&#47;&#46;'
			),
			'PGK' => array(
				'country' => 'Papua New Guinea', // ?
				'symbol' => '&#75;' // ?
			),
			'PHP' => array(
				'country' => 'Philippines',
				'symbol' => '&#8369;'
			),
			'PKR' => array(
				'country' => 'Pakistan',
				'symbol' => '&#8360;'
			),
			'PLN' => array(
				'country' => 'Poland',
				'symbol' => '&#122;&#322;'
			),
			'PYG' => array(
				'country' => 'Paraguay',
				'symbol' => '&#71;&#115;'
			),
			'QAR' => array(
				'country' => 'Qatar',
				'symbol' => '&#65020;'
			),
			'RON' => array(
				'country' => 'Romania',
				'symbol' => '&#108;&#101;&#105;'
			),
			'RSD' => array(
				'country' => 'Serbia',
				'symbol' => '&#1044;&#1080;&#1085;&#46;'
			),
			'RUB' => array(
				'country' => 'Russia',
				'symbol' => '&#1088;&#1091;&#1073;'
			),
			'RWF' => array(
				'country' => 'Rwanda',
				'symbol' => '&#1585;.&#1587;'
			),
			'SAR' => array(
				'country' => 'Saudi Arabia',
				'symbol' => '&#65020;'
			),
			'SBD' => array(
				'country' => 'Solomon Islands',
				'symbol' => '&#36;'
			),
			'SCR' => array(
				'country' => 'Seychelles',
				'symbol' => '&#8360;'
			),
			'SDG' => array(
				'country' => 'Sudan', // ?
				'symbol' => '&#163;' // ?
			),
			'SEK' => array(
				'country' => 'Sweden',
				'symbol' => '&#107;&#114;'
			),
			'SGD' => array(
				'country' => 'Singapore',
				'symbol' => '&#36;'
			),
			'SHP' => array(
				'country' => 'Saint Helena',
				'symbol' => '&#163;'
			),
			'SLL' => array(
				'country' => 'Sierra Leone', // ?
				'symbol' => '&#76;&#101;' // ?
			),
			'SOS' => array(
				'country' => 'Somalia',
				'symbol' => '&#83;'
			),
			'SRD' => array(
				'country' => 'Suriname',
				'symbol' => '&#36;'
			),
			'STD' => array(
				'country' => 'Sao Tome and Principe', // ?
				'symbol' => '&#68;&#98;' // ?
			),
			'SVC' => array(
				'country' => 'El Salvador',
				'symbol' => '&#36;'
			),
			'SYP' => array(
				'country' => 'Syria',
				'symbol' => '&#163;'
			),
			'SZL' => array(
				'country' => 'Swaziland', // ?
				'symbol' => '&#76;' // ?
			),
			'THB' => array(
				'country' => 'Thailand',
				'symbol' => '&#3647;'
			),
			'TJS' => array(
				'country' => 'Tajikistan', // ? TJS (guess)
				'symbol' => '&#84;&#74;&#83;' // ? TJS (guess)
			),
			'TMT' => array(
				'country' => 'Turkmenistan',
				'symbol' => '&#109;'
			),
			'TND' => array(
				'country' => 'Tunisia',
				'symbol' => '&#1583;.&#1578;'
			),
			'TOP' => array(
				'country' => 'Tonga',
				'symbol' => '&#84;&#36;'
			),
			'TRY' => array(
				'country' => 'Turkey', // New Turkey Lira (old symbol used)
				'symbol' => '&#8356;' // New Turkey Lira (old symbol used)
			),
			'TTD' => array(
				'country' => 'Trinidad and Tobago',
				'symbol' => '&#36;'
			),
			'TWD' => array(
				'country' => 'Taiwan',
				'symbol' => '&#78;&#84;&#36;'
			),
			'TZS' => array(
				'country' => 'Tanzania',
				'symbol' => ''
			),
			'UAH' => array(
				'country' => 'Ukraine',
				'symbol' => '&#8372;'
			),
			'UGX' => array(
				'country' => 'Uganda',
				'symbol' => '&#85;&#83;&#104;'
			),
			'USD' => array(
				'country' => 'USA',
				'symbol' => '&#36;'
			),
			'UYU' => array(
				'country' => 'Uruguay',
				'symbol' => '&#36;&#85;'
			),
			'UZS' => array(
				'country' => 'Uzbekistan',
				'symbol' => '&#1083;&#1074;'
			),
			'VEF' => array(
				'country' => 'Venezuela',
				'symbol' => '&#66;&#115;'
			),
			'VND' => array(
				'country' => 'Vietnam',
				'symbol' => '&#8363;'
			),
			'VUV' => array(
				'country' => 'Vanuatu',
				'symbol' => '&#86;&#84;'
			),
			'WST' => array(
				'country' => 'Samoa',
				'symbol' => '&#87;&#83;&#36;'
			),
			'XAF' => array(
				'country' => 'Central African Republic',
				'symbol' => '&#70;&#67;&#70;&#65;'
			),
			'XCD' => array(
				'country' => 'Antarctica',
				'symbol' => '&#36;'
			),
			'XDR' => array(
				'country' => '',
				'symbol' => ''
			),
			'XOF' => array(
				'country' => 'Mali',
				'symbol' => ''
			),
			'XPF' => array(
				'country' => 'Wallis and Futuna Islands',
				'symbol' => '&#70;'
			),
			'YER' => array(
				'country' => 'Yemen',
				'symbol' => '&#65020;'
			),
			'ZAR' => array(
				'country' => 'South Africa',
				'symbol' => '&#82;'
			),
			'ZMK' => array(
				'country' => '', // ?
				'symbol' => '&#90;&#75;' // ?
			),
			'ZWL' => array(
				'country' => '',
				'symbol' => '&#90;&#36;'
			)
		);

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'post_type'     => '',
			'id'            => '',
			'class'         => 'saiyan-currency-picker',
			'gridclass'     => '',
			'name'          => '',
			'style'         => null,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
            'description'   => '&nbsp;',
            'value'         => '',
            'data'          => array(),
            'dependency'    => null,
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'currency-picker', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public function load_assets() {
			wp_enqueue_style('st-module-currency-picker-css', plugin_dir_url(dirname(__FILE__)) . 'currency-picker/assets/css/currency-picker.css');
			wp_enqueue_script('st-module-currency-picker-js', plugin_dir_url(dirname(__FILE__)) . 'currency-picker/assets/js/currency-picker.js', array('jquery'), '1.0.0');

			wp_add_inline_script('st-module-currency-picker-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanCurrencyPicker();');
		}

		public function get_currency_country($currency_code) {

		}

		public function get_currency_symbol($currency_code) {

		}

		protected function module_construction() {
            ?>
            <div class="saiyan-grouped-module" <?php parent::print_data_attribs(self::$settings['data']); ?>>
                <select name="<?php echo esc_attr(self::$settings['name']) . '_currency' ?>">
		            <?php foreach ($this->currency as $key => $currency) : ?>
                        <option value="<?php echo esc_attr($key); ?>" <?php echo $key === self::$settings['value'] ? 'selected' : '' ?>>
				            <?php echo esc_html($key); ?>
                        </option>
		            <?php endforeach; ?>
                </select>
                <select name="<?php echo esc_attr(self::$settings['name']) . '_country' ?>">
		            <?php foreach ($this->currency as $key => $currency) : ?>
                        <option value="<?php echo esc_attr($key); ?>" <?php echo $key === self::$settings['value'] ? 'selected' : '' ?>>
				            <?php echo esc_html($currency['country']); ?>
                        </option>
		            <?php endforeach; ?>
                </select>
                <select name="<?php echo esc_attr(self::$settings['name']) . '_currency_symbol' ?>">
		            <?php foreach ($this->currency as $key => $currency) : ?>
                        <option value="<?php echo esc_attr($key); ?>" <?php echo $key === self::$settings['value'] ? 'selected' : '' ?>>
				            <?php echo esc_html($currency['symbol']); ?>
                        </option>
		            <?php endforeach; ?>
                </select>
            </div>
            <?php
		}
	}
}