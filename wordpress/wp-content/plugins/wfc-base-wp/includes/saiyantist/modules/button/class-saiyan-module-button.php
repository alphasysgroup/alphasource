<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Button')) {
	/**
	 * Class Saiyan_Module_Button
     *
     * Datatable module core class
     *
     * @since 1.0.0
     * @author Junjie Canonio
	 */
	class Saiyan_Module_Button extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'class'         => '',
			'name'          => 'saiyan-button-name',
			'type'          => 'button',
			'style'         => '',
			'gridclass'     => '',
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
			'content'       => 'Submit',
            'description'   => '',
            'readonly'      => false,
            'disabled'      => false,
            'data'          => array(),
            'dependency'    => null
		);

		/**
		 * ISaiyan_Module constructor.
		 *
		 * Constructor for Saiyan modules object
		 *
         * @param string $id
		 * @param array $settings
		 */
		public function __construct($id,  array $settings = array() ) {
			parent::__construct($id, 'button', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Junjie Canonio
		 */
		public function load_assets() {
			 wp_enqueue_style('st-module-button-css', plugin_dir_url(dirname(__FILE__)) . 'button/assets/css/button.css');

			// add_filter('mce_css', function($mce_css) {
			// 	return plugin_dir_url(dirname(__FILE__)) . 'wpeditor/assets/css/editor-style.css';
			// });
		}

		protected function module_construction() {
			$settings = self::$settings;

			$attr_id = isset( $settings['id'] ) ? $settings['id'] : 'saiyan-button-id';
			$attr_name = isset( $settings['name'] ) ? $settings['name'] : 'saiyan-button-name';

			$attr_class = isset( $settings['class'] ) ? $settings['class'] : 'st_button';
			$attr_class = 'saiyan-button '.$attr_class;

			$attr_type = isset($settings['type']) ? $settings['type'] : '';
			$attr_style = isset( $settings['style'] ) ? $settings['style'] : '';

			$attr_readonly = isset( $settings['readonly'] ) ? $settings['readonly'] : '';
			$attr_readonly = ($attr_readonly == true) ? 'pointer-events: none;' : '';

			$content = isset( $settings['content'] ) ? $settings['content'] : '';
            ?>
            <button
                id="<?php echo esc_attr(self::$id);?>"
                class="<?php echo esc_attr($attr_class);?>"
                name="<?php echo esc_attr($attr_name);?>"
                type="<?php echo esc_attr($attr_type);?>"
                <?php Saiyan_Helpers::print_style($attr_style.$attr_readonly); ?>
                <?php echo $settings['disabled'] ? 'disabled' : ''; ?>
                <?php parent::print_data_attribs(self::$settings['data']); ?>
            ><?php echo $content;?></button>
            <?php
		}
	}
}	