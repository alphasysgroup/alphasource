<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists( 'Saiyan_Module_Input' )) {
	/**
	 * Class Saiyan_Module_Datatable
     *
     * Datatable module core class
     *
     * @since 1.0.0
     * @author Junjie Canonio
	 */
	class Saiyan_Module_Input extends Saiyan_Module_Base {

		/**
		 * Default settings for the module
		 *
		 * @var array $defaults
		 */
		private $defaults = array(
			'id'            => '',
			'class'         => 'saiyan-input',
			'name'          => 'saiyan-input-name',
			'type'          => 'text',
			'style'         => '',
			'value'			=> '',
			'gridclass'     => '',
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Label here',
				'position'  => 'top',
			),
            'description'   => '&nbsp;',
            'placeholder'   => '',
            'required'      => false,
            'readonly'      => false,
            'data'          => array(),
            'dependency'    => null
		);

		/**
		 * ISaiyan_Module constructor.
		 *
		 * Constructor for Saiyan modules object
		 *
		 * @param array $settings
		 */
		public function __construct($id, $settings = array() ) {
			parent::__construct($id, 'input', $settings, $this->defaults);
		}

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @since 1.0.0
		 * @author Junjie Canonio
		 */
		public function load_assets() {
			// wp_enqueue_style('st-module-wpeditor-css', plugin_dir_url(dirname(__FILE__)) . 'wpeditor/assets/css/wpeditor.css');

			// add_filter('mce_css', function($mce_css) {
			// 	return plugin_dir_url(dirname(__FILE__)) . 'wpeditor/assets/css/editor-style.css';
			// });

			wp_enqueue_style('st-module-input-css', plugin_dir_url(dirname(__FILE__)) . 'input/assets/css/input.css');
		}

		protected function module_construction() {
			$settings = self::$settings;
			$allowed_types = array('text', 'password', 'number', 'email', 'hidden', 'search', 'tel', 'url');
			$attr_id = isset( $settings['id'] ) ? $settings['id'] : 'saiyan-input-ddddddddid';
			$attr_name = isset( $settings['name'] ) ? $settings['name'] : 'saiyan-input-name';
			$attr_class = isset( $settings['class'] ) ? $settings['class'] : '';
			$attr_class = 'saiyan-input '.$attr_class;
			$attr_type = isset($settings['type']) ? $settings['type'] : '';

			if (! in_array($attr_type, $allowed_types))
				throw new InvalidArgumentException('Input type \'' . $attr_type . '\' not allowed or invalid');

			$attr_value = isset($settings['value']) ? $settings['value'] : '';
			$attr_style = isset( $settings['style'] ) ? $settings['style'] : '';
			$label_hidden = isset( $settings['label']['hidden'] ) ? $settings['label']['hidden'] : '';
			$label_value = isset( $settings['label']['value'] ) ? $settings['label']['value'] : '';
			$label_position = isset( $settings['label']['position'] ) ? $settings['label']['position'] : '';
			$attr_placeholder = isset( $settings['placeholder'] ) ? $settings['placeholder'] : '';

			$attr_required = (isset($settings['required']) && $settings['required'] == true) ? 'required' : '';
			$attr_readonly = (isset($settings['readonly']) && $settings['readonly'] == true) ? 'readonly' : '';
            ?>
            <input
                id="<?php echo esc_attr(self::$id);?>"
                name="<?php echo esc_attr($attr_name);?>"
                type="<?php echo esc_attr($attr_type); ?>"
                value="<?php echo esc_attr($attr_value);?>"
                placeholder="<?php echo esc_attr($attr_placeholder);?>"
				<?php // TODO add condition for input type number to display min max attrib ?>
				<?php echo isset(self::$settings['min']) ? 'min="' . esc_attr(self::$settings['min']) . '"' : ''; ?>
				<?php echo isset(self::$settings['max']) ? 'max="' . esc_attr(self::$settings['max']) . '"' : ''; ?>
				<?php echo esc_attr($attr_required); ?>
				<?php echo esc_attr($attr_readonly); ?>
				<?php if($label_hidden == false && $label_position == 'top'): ?>
					<?php Saiyan_Helpers::print_style($attr_style.'width: 100%;'); ?>
				<?php elseif($label_hidden == false && $label_position == 'right'):?>
					<?php Saiyan_Helpers::print_style($attr_style.'width: 50%;float: left;'); ?>
				<?php elseif($label_hidden == false && $label_position == 'bottom'):?>
					<?php Saiyan_Helpers::print_style($attr_style.'width: 100%;'); ?>
				<?php elseif($label_hidden == false && $label_position == 'left'):?>
					<?php Saiyan_Helpers::print_style($attr_style.'width: 50%;float: left;'); ?>
				<?php endif; ?>
                <?php parent::print_data_attribs(self::$settings['data']); ?>
            />
            <?php
		}
	}
}	