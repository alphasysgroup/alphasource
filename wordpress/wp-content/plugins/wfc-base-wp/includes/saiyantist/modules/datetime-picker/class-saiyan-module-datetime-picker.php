<?php
defined('ABSPATH') or die('Access Denied: Restricted');

if (! class_exists('Saiyan_Module_Datetime_Picker')) {

	class Saiyan_Module_Datetime_Picker extends Saiyan_Module_Base {


		private $defaults = array(
			'id'           => '',
			'name'         => 'st-datetime-picker',
			'class'        => 'st-datetime-picker',
			'style'        => null,
			/*
			'type' => array(
				'Bootstrap' => 'bootstrap',
				'Propeller' => 'propeller'
			),*/
			'days_of_week' => '',
			'date_format'  => '',
            'data'         => array(),
            'dependency'   => null,
		);

		/**
		 * @var array
		 */

		/**
		 * Method to be used on loading its asset.
		 * Should be called inside the constructor of the module object
		 *
		 * @param array $settings
		 *
		 * @since 1.0.0
		 * @author Excel Pulmano
		 */

		public function __construct($id,  $settings = array() ) {
			parent::__construct($id,  'datetime-picker', $settings, $this->defaults );
			self::$settings['class'] .= ' ' . $this->defaults['class'] . ' ' . Saiyan::$configurator->get_theme();

			$this->load_assets();

		}

		public function load_assets() {
			wp_enqueue_style( 'wfc-BootstrapCDN-css' );
			wp_enqueue_style( 'wfc-BootstrapCDN-font-awesome-css' );
			wp_enqueue_style( 'st-datetime-picker-css', plugin_dir_url( dirname( __FILE__ ) ) . '/datetime-picker/assets/css/datetimepicker.css' );
			wp_enqueue_style( 'bootstrap-datetime-picker-css', plugin_dir_url( dirname( __FILE__ ) ) . '/datetime-picker/assets/css/bootstrap-datetimepicker.css' );


			wp_enqueue_script( 'wfc-BootstrapCDN-js' );
			wp_enqueue_script( 'popper-js', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js' );
			wp_enqueue_script( 'moment-with-locales-js', plugin_dir_url( dirname( __FILE__ ) ) . '/datetime-picker/assets/js/moment.min.js', array() );
			wp_enqueue_script( 'bootstrap-datetime-picker-js', plugin_dir_url( dirname( __FILE__ ) ) . '/datetime-picker/assets/js/bootstrap-datetimepicker.js', array() );
			wp_enqueue_script( 'st-datetimepicker-js', plugin_dir_url( dirname( __FILE__ ) ) . '/datetime-picker/assets/js/datetimepicker.js' );

			Saiyan_Helpers::wp_localize_script( 'st-datetimepicker-js', 'st_datetimepicker', array(
				self::$id . '-container' => array(
					'disableddate' => self::$settings['days_of_week'],
					'timeformat'   => self::$settings['date_format']
				)
			) );

			wp_add_inline_script('st-datetimepicker-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanDatetimepicker();');
		}


		/**
		 * Method that will be used to render the markup defined in its body.
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Excel Pulmano
		 */

		protected function module_construction() {
			$settings = self::$settings;

			$attr_id    = isset( $settings['id'] ) ? $settings['id'] : 'st-datetime-picker-id';
			$attr_class = isset( $settings['class'] ) ? $settings['class'] : 'st-datetime-picker';
			$attr_name  = isset( $settings['name'] ) ? $settings['name'] : 'st-datetime-picker';
			?>
			<div class="<?php echo $attr_class; ?> input-append date" data-provide="datetimepicker" <?php parent::print_data_attribs(self::$settings['data']); ?>>
				<input id="<?php echo self::$id; ?>" class="span2 field saiyan-textfield" size="16" type="text" name="<?php echo $attr_name; ?>">
				<div class="saiyan-btn-group button-group">
					<span class="add-on saiyan-btn-primary"><i class="fa fa-times"></i></span>
					<span class="add-on saiyan-btn-primary"><i class="fa fa-calendar"></i></span>
				</div>
			</div>
			<?php
		}
	}
}


