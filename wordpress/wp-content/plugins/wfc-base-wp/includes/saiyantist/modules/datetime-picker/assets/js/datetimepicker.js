(function($) {
    $.fn.saiyanDatetimepicker = function () {

        const a = $(this).find('[data-provide=\'datetimepicker\']');
        const b = a.next();
        var form = st_datetimepicker[$(this).attr('id')].timeformat;
        var disable = st_datetimepicker[$(this).attr('id')].disableddate;

        a.datetimepicker({
            format: form.toString(),
            autoclose: true,
            fontAwesome:true,
            showMeridian: true,
            inline: false,
            todayBtn: true,
            clearBtn:true,
            daysOfWeekDisabled: disable.toString()
        });

        b.children().find('.fa-calendar').on('click', function () {
            // noinspection JSAnnotator
            a.datetimepicker('show')
        });

        b.children().find('.fa-times').on('click', function () {
            a.empty().val('');
        });
    };
})(jQuery);