(function($) {
    $.fn.saiyanNavigator = function() {
        const n = $(this).find('.navigator');
        const wayPoints = n.find('.saiyan-way-points:not(.saiyan-navigator-accordion-sections) > .saiyan-way-point');
        const wayPointsId = getWaypointsId(wayPoints);

        const accordion = '#' + st_module_navigator[$(this).attr('id')].link_to_navigator + '-container';
        const nAccordionWayPoints = n.find('.saiyan-navigator-accordion-sections > .saiyan-way-point');

        let isAccordionWayPointsClicked = false;

        // console.log($(accordion));

        wayPoints.on('click', function(e) {
            e.stopPropagation();

            let $this = $(this);
            let waypoint = $this.attr('data-saiyan-waypoint');

            wayPoints.removeClass('active');
            $this.addClass('active');

            scrollToWaypoint(waypoint);

            if (wayPoints.hasClass('saiyan-way-point-accordion active'))
                n.find('.saiyan-navigator-accordion-sections').addClass('show');
            else
                n.find('.saiyan-navigator-accordion-sections').removeClass('show');
        });

        nAccordionWayPoints.on('click', function(e) {
            e.stopPropagation();

            let $this = $(this);
            let id = parseInt($this.attr('data-saiyan-navigator-accordion-section-id'));

            isAccordionWayPointsClicked = true;
            
            nAccordionWayPoints.removeClass('active');
            $this.addClass('active');

            $(accordion).find('.accordion').accordion({
                active: id,
                activate: function(event, ui) {
                    let targetWayPoint = $(accordion).find('.accordion').find('h3[aria-controls=\'ui-id-' + ((id + 1) * 2) + '\']');

                    if (isAccordionWayPointsClicked)
                        scrollToWaypoint(targetWayPoint.attr('id'));

                    isAccordionWayPointsClicked = false;
                }
            });
        });

        $(accordion).find('.accordion').on('accordionactivate', function(event, ui) {
            let activeAccordionPanel = $(ui.newHeader[0]);
            let id = (parseInt(activeAccordionPanel.attr('aria-controls').replace('ui-id-', '')) / 2) - 1;
            // console.log(id);

            nAccordionWayPoints.removeClass('active');
            n.find('.saiyan-navigator-accordion-sections > .saiyan-way-point[data-saiyan-navigator-accordion-section-id=\'' + id + '\']').addClass('active');
        });

        $(window).scroll(function() {
            $.each(wayPointsId, function(i, val) {
                let section = $('#' + val);
                if (typeof section === 'undefined') return;

                let waypointNavigator = n.find('li[data-saiyan-waypoint=\'' + val + '\']');

                if (isVisibleInViewport(section)) {
                    waypointNavigator.addClass('active');
                } else {
                    waypointNavigator.removeClass('active');
                }
            })
        });

        $(document).on('click', function() {
            n.find('.saiyan-navigator-accordion-sections').removeClass('show');
        });

        function isVisibleInViewport(elem) {
            let $elem = $(elem);
            let $window = $(window);

            let documentViewTop = $window.scrollTop();
            let documentViewBottom = documentViewTop + $window.height();

            let elementTop = $elem.offset().top;
            let elementBottom = elementTop + $elem.height();

            /*
            return (
                (elementBottom <= documentViewBottom) && (elementTop >= documentViewTop)
            );
            */

            return (
                (elementTop >= documentViewTop && elementTop < documentViewBottom) ||
                (elementBottom > documentViewTop && elementBottom <= documentViewBottom) ||
                ($elem.height() > $window.height() && elementTop <= documentViewTop && elementBottom >= documentViewBottom)
            );
        }

        function scrollToWaypoint(waypoint) {
            let waypointElement = $(document).find('#' + waypoint);
            let windowTop = $(window).scrollTop();
            let timeout = Math.abs(windowTop - (waypointElement.offset().top - 50));
            timeout = Math.max(500, Math.min(timeout, 1500));
            // console.log(timeout);
            $('html, body').animate({
                'scrollTop': waypointElement.offset().top - 50
            }, timeout)
        }

        function getWaypointsId(waypoints) {
            let waypointsId = [];
            waypoints.each(function() {
                waypointsId.push($(this).attr('data-saiyan-waypoint'));
            });
            return waypointsId;
        }
    }
})(jQuery);