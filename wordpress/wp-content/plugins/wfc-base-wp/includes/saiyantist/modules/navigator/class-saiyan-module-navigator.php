<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Navigator')) {
	class Saiyan_Module_Navigator extends Saiyan_Module_Base {

		private $defaults = array(
			'id' => '',
			'class' => 'saiyan-navigator',
			'style' => null,
            'link_to_accordion' => null
		);

		private $way_points;

		public function __construct( $id, $way_points = array(), $settings = array()) {
			parent::__construct( $id, 'navigator', $settings, $this->defaults );

			$this->way_points = $way_points;
		}

		/**
		 * Method that will contain enqueues for styles and scripts, and anything related to it
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		protected function load_assets() {
			wp_enqueue_style('st-module-navigator-css', plugin_dir_url(dirname(__FILE__)) . 'navigator/assets/css/navigator.css');
			wp_enqueue_script('st-module-navigator-js', plugin_dir_url(dirname(__FILE__)) . 'navigator/assets/js/navigator.js', array('jquery'), '1.0.0');

			Saiyan_Helpers::wp_localize_script('st-module-navigator-js', 'st_module_navigator', array(
				self::$id . '-container' => array(
					'link_to_navigator' => self::$settings['link_to_accordion']
				)
			));

			wp_add_inline_script('st-module-navigator-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanNavigator();');
		}

		/**
		 * Method that will contain the structure of the module that will be rendered
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		protected function module_construction() {
			?>
			<div class="navigator">
				<ul class="saiyan-way-points">
					<?php foreach ($this->way_points as $key => $way_point) : ?>
                        <?php if (self::$settings['link_to_accordion'] === $key) : ?>
                            <li class="saiyan-way-point saiyan-way-point-accordion" data-saiyan-waypoint="<?php echo self::$settings['link_to_accordion'] . '-container'; ?>">
                                <div class="saiyan-way-point-title"><?php echo $way_point; ?></div>
                            </li>
                        <?php else : ?>
                            <li class="saiyan-way-point" data-saiyan-waypoint="<?php echo $key; ?>">
                                <div class="saiyan-way-point-title"><?php echo $way_point; ?></div>
                            </li>
                        <?php endif; ?>
					<?php endforeach; ?>
				</ul>
                <?php if (isset(self::$settings['link_to_accordion'])) : ?>
                    <ul class="saiyan-way-points saiyan-navigator-accordion-sections">
	                    <?php
	                    $sections = Saiyan_Handler::get_rendered_module(self::$settings['link_to_accordion'])['sections'];
	                    foreach ($sections as $id => $section) {
		                    ?>
                            <li class="saiyan-way-point <?php echo $id === 0 ? 'active' : ''; ?>" data-saiyan-navigator-accordion-section-id="<?php echo $id; ?>">
                                <div class="saiyan-way-point-title"><?php echo $section['title']; ?></div>
                            </li>
		                    <?php
	                    }
	                    ?>
                    </ul>
                <?php endif; ?>
			</div>
			<?php
		}
	}
}