<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Separator')) {
	class Saiyan_Module_Separator extends Saiyan_Module_Base {

		private $defaults = array(
			'id'            => '',
			'class'         => 'saiyan-separator',
			'gridclass'     => '',
			'visibility'    => 'visible', // visible, hidden
			'align'         => 'center', // left, center, align
			'size'          => 2,
			'width'         => '100%',
            'style'         => null
		);

		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'separator', $settings, $this->defaults);
		}

		/**
		 * Method that will contain enqueues for styles and scripts, and anything related to it
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		protected function load_assets() {
			wp_enqueue_style('st-module-separator-css', plugin_dir_url(dirname(__FILE__)) . 'separator/assets/css/separator.css');
		}

		/**
		 * Method that will contain the structure of the module that will be rendered
		 *
		 * @return mixed
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		protected function module_construction() {
			?>
			<hr
				align="<?php echo esc_attr(self::$settings['align']); ?>"
				size="<?php echo esc_attr(self::$settings['size']); ?>"
				width="<?php echo esc_attr(self::$settings['width']); ?>"
                <?php Saiyan_Helpers::print_style(self::$settings['style']) ?>
				<?php echo 'visibility="' . esc_attr(self::$settings['visibility']) . '"'; ?>
			>
			<?php
		}
	}
}