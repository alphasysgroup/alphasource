<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('Saiyan_Module_Datatable')) {
	/**
	 * Class Saiyan_Module_Datatable
     *
     * Datatable module core class
     *
     * @since 1.0.0
     * @author Von Sienard Vibar
	 */
	class Saiyan_Module_Datatable extends Saiyan_Module_Base {

		/**
         * Default settings for the module
         *
		 * @var array $defaults
		 */
		private $defaults = array(
            'post_type' => '',
			'id'        => '',
			'class'     => 'saiyan-datatable',
			'name'      => '',
			'style'     => null,
			'columns'   => array(
				array(
					'title'     => 'Title',
					'sortable'  => true,
					'meta_data' => 'post_title'
				),
				array(
					'title'     => 'Date',
					'sortable'  => true,
					'meta_data' => 'post_date'
				),
				array(
					'title'     => 'Status',
					// 'sortable'  => true,
					'meta_data' => 'post_status'
				)
			),
			'show_add_button'       => true,
			'show_search'           => true,
			'search_options'        => array(
                'placeholder' => 'Search...'
            ),
			'show_filter'           => true,
			'filter_options'        => array(
                'options'           => array(
                    'All'           => 'any',
                    'Published'     => 'publish',
                    'Draft'         => 'draft',
                    'Trash'         => 'trash'
                ),
                'selected_option'   => 'All'
            ),
			'show_number_of_rows'   => true,
			'number_of_rows'        => 5,
			'show_pagination'       => true,
            'show_loader'           => true,
            'empty_message'         => 'End of the line...',
            'data'                  => array(),
            'dependency'            => null,
		);

		/**
		 * Saiyan_Module_Datatable constructor.
		 *
		 * @param array $settings
		 */
		public function __construct($id, $settings = array()) {
			parent::__construct($id, 'datatable', $settings, $this->defaults);
		}

		public function load_assets() {
		    // wp_add_inline_script('st-datatable-bind', '$(\'#' . esc_attr(self::$settings['id']) . '\').saiyanDatatable();');
			/**
			 * Register Saiyantist css
			 */
			wp_enqueue_style('st-module-datatable-css', plugin_dir_url(dirname(__FILE__)) . 'datatable/assets/css/datatable.css');
			wp_enqueue_style('wfc-BootstrapCDN-font-awesome-css');

			/**
			 * Register Saiyantist js
			 */
			wp_enqueue_script('st-module-datatable-js', plugin_dir_url(dirname(__FILE__)) . 'datatable/assets/js/datatable.js', array('jquery'), '1.0.0');

			/**
			 * Inline script to bind .saiyanDatatable to its datatable.
             * (Support for multiple datatables in one page.)
			 */
			wp_add_inline_script('st-module-datatable-js', 'jQuery(\'#' . esc_attr(self::$id) . '-container\').saiyanDatatable();');

			Saiyan_Helpers::wp_localize_script(
				'st-module-datatable-js',
				'st_datatable',
				array(
					self::$id . '-container' => array(
						'ajaxurl' => admin_url('admin-ajax.php'),
						'settings' => self::$settings,
						'post_type' => self::$settings['post_type'],
						'empty_message' => self::$settings['empty_message']
					)
                )
			);
		}

		protected function module_construction() {
			$result = new WP_Query(array(
				'post_type' => self::$settings['post_type'],
				'post_status' => self::$settings['filter_options']['options'][self::$settings['filter_options']['selected_option']],
				'posts_per_page' => self::$settings['number_of_rows'],
				'offset' => 0
			));
			?>
            <div
                    class="saiyan-datatable-wrapper"
                    data-saiyan-module="datatable"
                    <?php parent::print_data_attribs(self::$settings['data']); ?>
            >
				<?php if (self::$settings['show_add_button'] || self::$settings['show_search'] || self::$settings['show_filter'] || self::$settings['show_number_of_rows'] || self::$settings['show_pagination']) : ?>
                    <div class="saiyan-datatable-options">
						<?php if (self::$settings['show_add_button']) : ?>
                            <a href="<?php echo admin_url() . 'post-new.php?post_type=' . self::$settings['post_type']; ?>" class="saiyan-btn-primary" data-action="add"><i class="fa fa-plus"></i></a>
						<?php endif; ?>
						<?php if (self::$settings['show_search']) : ?>
                            <input class="saiyan-textfield" type="search" placeholder="<?php echo self::$settings['search_options']['placeholder']; ?>" />
						<?php endif; ?>
						<?php if (self::$settings['show_filter']) : ?>
                            <select class="saiyan-select" name="filter">
								<?php foreach (self::$settings['filter_options']['options'] as $key => $value) : ?>
                                    <option value="<?php echo $value ?>"><?php echo $key; ?></option>
								<?php endforeach; ?>
                            </select>
						<?php endif; ?>
						<?php if (self::$settings['show_number_of_rows']) : ?>
                            <select class="saiyan-select" name="number-of-rows">
                                <option value="5">Show 5 items</option>
                                <option value="10">Show 10 items</option>
                                <option value="20">Show 20 items</option>
                            </select>
						<?php endif; ?>
						<?php if (self::$settings['show_pagination']) : ?>
                            <button class="saiyan-btn-primary" type="button" data-pagination-direction="prev" disabled><i class="fa fa-chevron-left"></i></button>
                            <select class="saiyan-select" name="page">
								<?php for ($page=1; $page<=$result->max_num_pages; $page++) : ?>
                                    <option value="<?php echo $page; ?>"><?php echo $page; ?></option>
								<?php endfor; ?>
                            </select>
                            <button class="saiyan-btn-primary" type="button" data-pagination-direction="next"><i class="fa fa-chevron-right"></i></button>
						<?php endif; ?>
                    </div>
				<?php endif; ?>
                <table id="<?php echo esc_attr(self::$settings['id']); ?>" class="<?php echo esc_attr(self::$settings['class']); ?> row-5" <?php Saiyan_Helpers::print_style(self::$settings['style']); ?>>
                    <thead>
                    <tr>
						<?php foreach (self::$settings['columns'] as $column) : ?>
                            <th data-meta-row="<?php echo $column['meta_data']; ?>" <?php echo isset($column['sortable']) && $column['sortable'] ? 'sortable' : ''; ?>><?php echo $column['title']; ?></th>
						<?php endforeach; ?>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
					foreach ($result->posts as $data) {
						$data_arr = (array)$data;
						?>
                        <tr>
							<?php foreach (self::$settings['columns'] as $column) : ?>
                                <td><?php echo $this->post_object($data_arr, $column['meta_data']); ?></td>
							<?php endforeach; ?>
                            <td class="action-row" style="width:20px;">
                                <a href="<?php echo get_permalink($data->ID); ?>" title="View"><i class="fa fa-eye"></i></a>
                            </td>
                            <td class="action-row" style="width:20px;">
                                <a href="<?php echo get_edit_post_link($data->ID); ?>" title="Edit"><i class="fa fa-edit"></i></a>
                            </td>
                            <td class="action-row red" style="width:20px;">
                                <a href="javascript:" title="Trash" data-post-action="trash" data-id="<?php echo $data->ID; ?>" data-nonce="<?php echo wp_create_nonce('st_trash_post_nonce_' . $data->ID); ?>"><i class="fa fa-trash"></i></a>
                            </td>
                        </tr>
						<?php
					}
					?>
                    </tbody>
                </table>
				<?php if (! self::$settings['show_loader']) : ?>
                    <div class="saiyan-progress-wrapper">
                        <div class="progress-bar">
                            <div class="message">Message</div>
                        </div>
                    </div>
				<?php endif; ?>
            </div>
			<?php
		}

		/**
		 * Function that handles ajax requests
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
		public function st_datatable_ajax_controller() {
		    $operation = $_POST['operation'];

		    if ($operation === 'update_table') {
			    $query = isset($_POST['query']) ? $_POST['query'] : '';
			    $post_type = $_POST['post_type'];
			    $post_status = $_POST['filter'];
			    $row_count = $_POST['row_count'];
			    $offset = $_POST['offset'];
			    $order_by = $_POST['order_by'];
			    $order = strtoupper($_POST['order']);
			    $json_object = array();
			    $response = null;

			    $result = new WP_Query(array(
				    's' => esc_sql($query),
				    'post_type' => $post_type,
				    'post_status' => $post_status,
				    'posts_per_page' => $row_count,
				    'offset' => $offset,
				    'orderby' => $order_by,
				    'order' => $order
			    ));
			    $object = null;
			    foreach ($result->posts as $data) {
				    $data_arr = (array)$data;
				    // var_dump($data_arr);
				    foreach ($_POST['settings']['columns'] as $column) {
					    $object[$column['meta_data']] = self::post_object($data_arr, $column['meta_data']);
				    }

				    $object['_view_url'] = get_permalink($data->ID);
				    $object['_edit_url'] = get_edit_post_link($data->ID, '');
				    $object['_post_id'] = $data->ID;
				    $object['_trash_nonce'] = wp_create_nonce('st_trash_post_nonce_' . $data->ID);
				    $object['_untrash_nonce'] = wp_create_nonce('st_untrash_post_nonce_' . $data->ID);
				    $object['_delete_nonce'] = wp_create_nonce('st_delete_post_nonce_' . $data->ID);

				    array_push($json_object, $object);
			    }

			    // $response['is_last'] = $result->current_post+1 === $result->post_count || $result->post_count < $row_count;
			    $response['is_last'] = $result->max_num_pages * $row_count == ($offset + $row_count);
			    $response['result'] = $json_object;
			    // $response['result_count'] = $result->max_num_pages * $row_count . ' ' . ($offset + $row_count);
			    $response['result_count'] = $result->found_posts;
			    if ($result->found_posts == 0) $response['empty_message'] = $_POST['empty_message'];
			    $response['page_count'] = $result->max_num_pages;

			    echo json_encode($response);
            }
            else {
		        $id = absint($_POST['id']);
		        $nonce = 'st_' . $operation . '_post_nonce_' . $id;
		        $response = null;
		        if (check_ajax_referer($nonce, 'nonce', false)) {
			        switch ($operation) {
				        case 'trash':
                            $response = wp_trash_post($id) ? 1 : 0;
					        break;
				        case 'untrash':
                            $response = wp_untrash_post($id) ? 1 : 0;
					        break;
				        case 'delete':
                            $response = wp_delete_post($id) ? 1 : 0;
					        break;
			        }
                }
                else $response = 0;

		        echo $response;
            }

		    wp_die();
        }

		/**
         * Returns formatted string in HTML based from the post attribute
         *
		 * @param array $arr Post object
		 * @param string $key Post key
		 *
		 * @return string Formatted string based from post attribute
         *
         * @since 1.0.0
         * @author Von Sienard Vibar
		 */
        private static function post_object(array $arr, $key) {
		    if (strpos($key, 'date'))
		        return '<span>' . Saiyan_Helpers::get_formalize_date_string($arr[$key]) . '</span>' .
                       '<br>' .
                       '<small>' . Saiyan_Helpers::get_formalize_time_string($arr[$key]) . '</small>';
		    else if (strpos($key, 'status'))
		        return '<span>' . ucfirst($arr[$key]) . '</span>';
		    else return '<div>' . $arr[$key] . '</div>';
        }
	}
}