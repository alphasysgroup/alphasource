(function($) {
    $.fn.saiyanDatatable = function() {
        const t = $(this);
        const toptions = t.prev();
        const filter = toptions.find('.saiyan-select[name=\'filter\']');
        const numberOfRows = toptions.find('.saiyan-select[name=\'number-of-rows\']');
        const pagination = toptions.find('.saiyan-select[name=\'page\']');
        const search = toptions.find('.saiyan-textfield');
        const tb = t.find('tbody');
        var tr = tb.find('tr');
        var rowCount = 0;
        var offset = 0;
        var orderBy = '';
        var order = '';

        function preInit(tr, callback) {
            tr.css({
                'transform': 'translateY(50px)',
                'opacity': '0.0'
            });
            setTimeout(callback, 300);
        }

        function load(tr, callback) {
            let i = 0;

            setTimeout(function() {
                slideRow(tr[i]);
            }, 300);

            function slideRow(row) {
                $(row).css({
                    'transform': 'translateY(0)',
                    'opacity': '1.0'
                });

                i++;

                setTimeout(function() {
                    if (i < tr.length) slideRow(tr[i]);
                }, 100);
            }

            if (typeof callback !== 'undefined') {
                setTimeout(function() {
                    callback();
                }, (300-150) * tr.length);
            }
        }

        // 2 6 5 1 7
        // 1 6 5 2 7
        // 1 2 6 5 7
        // 1 2 5 6 7

        function sort(array, order) {
            let cur = array[0];
            let caret = 1;

            do {
                for (let i=caret, count=array.length; i<count; i++) {
                    if (order === 'asc'
                        ? $(cur).text() > $(array[i]).text()
                        : $(cur).text() < $(array[i]).text()) {
                        let holdCard = array[i];
                        array[i] = array[caret-1];
                        cur = array[caret-1] = holdCard;
                    }
                }

                cur = array[caret++];

                // console.log(array);
            } while (caret < array.length);

            return array;
        }

        preInit(tb.find('tr'));
        load(tb.find('tr'));

        function grabData() {
            return {
                'action': 'st_datatable_ajax_controller',
                'operation': 'update_table',
                'query': search.val(),
                'filter': filter.val(),
                'offset': (pagination.val()-1) * numberOfRows.val() < 0 ? 0 : (pagination.val()-1) * numberOfRows.val(),
                'row_count': numberOfRows.val(),
                'order_by': orderBy,
                'order': order,
                'settings': st_datatable.settings,
                'post_type': st_datatable.post_type,
                'empty_message': st_datatable.empty_message
            };
        }

        function loadDataTableContent(response) {
            var json = JSON.parse(response);

            preInit(tb.find('tr'), function() {
                tb.empty();
                t.attr('class', 'saiyan-datatable').addClass('row-' + numberOfRows.val());
                // t.find('thead tr th[sortable]').removeClass('sort');

                var result = json['result'];
                var count = result.length;
                if (count > 0) {
                    rowCount = numberOfRows.val();
                    for (var i=0; i<count; i++) {
                        var tr = $('<tr></tr>');
                        for (var key in result[i]) {
                            if (key.substr(0, 1) !== '_') {
                                tr.append(
                                    $('<td></td>').html(result[i][key])
                                );
                            }
                        }

                        if (filter.val() !== 'trash') {
                            tr.append(
                                $('<td></td>')
                                    .addClass('action-row')
                                    .append(
                                        $('<a></a>')
                                            .attr('href', result[i]['_view_url'])
                                            .attr('title', 'View')
                                            .append(
                                                $('<i></i>').addClass('fa fa-eye')
                                            )
                                    ),
                                $('<td></td>')
                                    .addClass('action-row')
                                    .append(
                                        $('<a></a>')
                                            .attr('href', result[i]['_edit_url'])
                                            .attr('title', 'Edit')
                                            .append(
                                                $('<i></i>').addClass('fa fa-edit')
                                            )
                                    ),
                                $('<td></td>')
                                    .addClass('action-row red')
                                    .append(
                                        $('<a></a>')
                                            .attr('href', 'javascript:')
                                            .attr('title', 'Trash')
                                            .attr('data-id', result[i]['_post_id'])
                                            .attr('data-post-action', 'trash')
                                            .attr('data-nonce', result[i]['_trash_nonce'])
                                            .append(
                                                $('<i></i>').addClass('fa fa-trash')
                                            )
                                    )
                            );
                        }
                        else {
                            tr.append(
                                $('<td></td>').addClass('action-row'),
                                $('<td></td>')
                                    .addClass('action-row')
                                    .append(
                                        $('<a></a>')
                                            .attr('href', 'javascript:')
                                            .attr('title', 'Restore')
                                            .attr('data-id', result[i]['_post_id'])
                                            .attr('data-post-action', 'untrash')
                                            .attr('data-nonce', result[i]['_untrash_nonce'])
                                            .append(
                                                $('<i></i>').addClass('fa fa-undo')
                                            )
                                    ),
                                $('<td></td>')
                                    .addClass('action-row red')
                                    .append(
                                        $('<a></a>')
                                            .attr('href', 'javascript:')
                                            .attr('title', 'Delete')
                                            .attr('data-id', result[i]['_post_id'])
                                            .attr('data-post-action', 'delete')
                                            .attr('data-nonce', result[i]['_delete_nonce'])
                                            .append(
                                                $('<i></i>').addClass('fa fa-trash')
                                            )
                                    )
                            );
                        }

                        tb.append(tr);
                    }
                    if (count < rowCount) {
                        for (var i=0; i<rowCount-count; i++) {
                            tb.append(
                                $('<tr></tr>')
                            )
                        }
                    }
                }
                else {
                    tb.append(
                        $('<div></div>')
                            .addClass('saiyan-table-empty-message')
                            .html('<div>' + json['empty_message'] + '</div>')
                            .hide()
                            .fadeIn()
                    );
                }

                tr = tb.find('tr');
                tr.css({
                    'transform': 'translateY(50px)',
                    'opacity': '0.0'
                });

                load(tb.find('tr'), function() {
                    SaiyantistCore.ajaxLoaderComplete();
                });
            });

            console.log(pagination.val() == 1);
            toptions.find('button[data-pagination-direction=\'prev\']').prop('disabled', pagination.val() == 1);
            toptions.find('button[data-pagination-direction=\'next\']').prop('disabled', json['is_last']);
        }

        function updatePagination(pageCount) {
            pagination.empty();
            for (var page=1; page<=pageCount; page++) {
                pagination.append(
                    $('<option></option>')
                        .val(page)
                        .text(page)
                )
            }
        }

        t.find('thead tr th[sortable]').on('click', function(e) {
            let $this = $(this);
            let i = $this[0].cellIndex;

            orderBy = $this.attr('data-meta-row');

            if (!$this.hasClass('sort')) {
                t.find('thead tr th[sortable]').removeClass('sort');
                $this.addClass('sort');
            }
            else $this.removeClass('sort');

            // console.log(t.find('tbody td:nth-of-type(' + (i+1) + ')'));

            // sort([2, 6, 5, 1, 7, 5]);

            // console.log(sort(t.find('tbody td:nth-of-type(' + (i+1) + ')')));

            order = $this.hasClass('sort') ? 'asc' : 'desc';

            $this.addClass('load');

            /*
            preInit(tb.find('tr'), function() {
                /*
                sort(
                    t.find('tbody td:nth-of-type(' + (i+1) + ')'),
                    $this.hasClass('sort') ? 'asc' : 'desc'
                ).each(function() {
                    tb.append($(this).parent());
                });
                *

                load(tb.find('tr'), function() {
                    $this.removeClass('load');
                });
            });
            */

            SaiyantistCore.ajaxLoader(
                st_datatable.ajaxurl,
                SaiyantistCore.actionType.post,
                grabData(),
                null,
                function(response) {
                    $this.removeClass('load');
                    updatePagination(JSON.parse(response)['page_count']);
                    loadDataTableContent(response);
                }
            );
        });

        numberOfRows.on('change', function() {
            pagination.val(1);

            SaiyantistCore.ajaxLoader(
                st_datatable.ajaxurl,
                SaiyantistCore.actionType.post,
                grabData(),
                null,
                function(response) {
                    updatePagination(JSON.parse(response)['page_count']);
                    loadDataTableContent(response);
                }
            );
        });

        toptions.find('button[data-pagination-direction]').on('click', function() {
            var direction = $(this).attr('data-pagination-direction');
            rowCount = numberOfRows.val();
            // console.log(pagination);

            if (direction === 'prev') offset = offset-- <= 0 ? 0 : offset--;// = rowCount * (offset <= 0 ? 0 : offset--);
            else if (direction === 'next') offset++;// = rowCount * (offset >= offset ? rowCount : offset++);

            // console.log(offset);

            pagination.val(offset + 1);

            SaiyantistCore.ajaxLoader(
                st_datatable.ajaxurl,
                SaiyantistCore.actionType.post,
                grabData(),
                null,
                function(response) {
                    loadDataTableContent(response);
                }
            );
        });

        pagination.on('change', function() {
            offset = $(this).val() - 1;

            SaiyantistCore.ajaxLoader(
                st_datatable.ajaxurl,
                SaiyantistCore.actionType.post,
                grabData(),
                null,
                function(response) {
                    loadDataTableContent(response);
                }
            );
        });

        search.on('keyup', function(e) {
            if (e.which === 13) {
                pagination.val(1);

                SaiyantistCore.ajaxLoader(
                    st_datatable.ajaxurl,
                    SaiyantistCore.actionType.post,
                    grabData(),
                    null,
                    function(response) {
                        updatePagination(JSON.parse(response)['page_count']);
                        loadDataTableContent(response);
                    }
                );
            }
        });

        $(document).on('click', 'table.saiyan-datatable a[data-post-action]', function() {
            var $this = $(this);
            var operation = $this.attr('data-post-action');
            var id = $this.attr('data-id');
            var nonce = $this.attr('data-nonce');

            SaiyantistCore.ajaxLoader(
                st_datatable.ajaxurl,
                SaiyantistCore.actionType.post,
                {
                    'action': 'st_datatable_ajax_controller',
                    'operation': operation,
                    'id': id,
                    'nonce': nonce
                },
                function() {
                    if (operation === 'delete') {
                        if (! confirm('Are you sure you want to delete these post?')) return false;
                    }
                },
                function(response) {
                    if (response === '1') {
                        pagination.val(1);

                        SaiyantistCore.ajaxLoader(
                            st_datatable.ajaxurl,
                            SaiyantistCore.actionType.post,
                            grabData(),
                            null,
                            function(response) {
                                updatePagination(JSON.parse(response)['page_count']);
                                loadDataTableContent(response);
                            }
                        );
                    }
                }
            );
        });

        filter.on('change', function() {
            pagination.val(1);

            SaiyantistCore.ajaxLoader(
                st_datatable.ajaxurl,
                SaiyantistCore.actionType.post,
                grabData(),
                null,
                function(response) {
                    updatePagination(JSON.parse(response)['page_count']);
                    loadDataTableContent(response);
                }
            );
        });

        // return this;
    }
}(jQuery));