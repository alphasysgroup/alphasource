<?php

defined('ABSPATH') or die('No script kiddie please!');

if (!class_exists('Saiyan_Handler')) {
	class Saiyan_Handler {

		private static $rendered_module = null;

		/**
		 * A recursive function that renders the module by its type.
		 * If the module is a container, then wrap it and render its child modules
		 *
		 * @param array $mod_obj The module object
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar
		 */
		public static function render_module($mod_obj) {
			if (empty($mod_obj)) throw new InvalidArgumentException('Invalid module mapping, it might be empty or something.');

			$id = key($mod_obj);
			$module = $mod_obj[$id];

			if (! Saiyan_Helpers::validate_module_type($module['type'])) throw new InvalidArgumentException( 'Module type "' . $module['type'] . '" is not valid');
			// if (! Saiyan_Helpers::validate_settings($module['settings'])) throw new InvalidArgumentException('Parameters is not an array');

			$type = $module['type'];
			$settings = isset($module['settings']) ? $module['settings'] : null;
			
			$module_object = null;

			switch ($type) {
				case 'settingsform':
					$module_object = new Saiyan_Module_SettingsForm($id, $settings);
					break;
				case 'datatable':
					// if ($object === null) throw new InvalidArgumentException('An object is needed to display data for datatable');
					$module_object = new Saiyan_Module_Datatable($id, $settings);
					break;
                case 'date-picker':
                    $module_object = new Saiyan_Module_Date_Picker($id, $settings);
                    break;
				case 'datetime-picker':
					$module_object = new Saiyan_Module_Datetime_Picker($id, $settings);
					break;
				case 'card':
					$module_object = new Saiyan_Module_Card($id, $settings);
					break;
				case 'text':
					if (! isset($module['content'])) throw new InvalidArgumentException('Content is empty.');
					$module_object = new Saiyan_Module_Text($id, $module['content'], $settings);
					break;
				case 'html':
					if (! isset($module['content'])) throw new InvalidArgumentException('Content is empty.');
					$module_object = new Saiyan_Module_HTML($id, $module['content'], $settings);
					break;					
				case 'input':
					$module_object = new Saiyan_Module_Input($id, $settings);
					break;
				case 'textarea':
					$module_object = new Saiyan_Module_Textarea($id, $settings);
					break;
				case 'button':
					$module_object = new Saiyan_Module_Button($id, $settings);
					break;
				case 'radio':
					$module_object = new Saiyan_Module_Radio($id, $settings);
					break;
				case 'checkbox':
					$module_object = new Saiyan_Module_Checkbox($id, $settings);
					break;
				case 'slider':
					$module_object = new Saiyan_Module_Slider($id, $settings);
					break;
				case 'switch':
					$module_object = new Saiyan_Module_Switch($id, $settings);
					break;
				case 'select':
					$module_object = new Saiyan_Module_Select($id, $settings);
					break;
				case 'separator':
					$module_object = new Saiyan_Module_Separator($id, $settings);
					break;
				case 'currency-picker':
					$module_object = new Saiyan_Module_CurrencyPicker($id, $settings);
					break;
				case 'wpeditor':
					$module_object = new Saiyan_Module_WPEditor($id, $settings);
					break;
				case 'media':
					$module_object = new Saiyan_Module_Media($id, $settings);
					break;
				case 'color-picker':
					$module_object = new Saiyan_Module_ColorPicker($id, $settings);
					break;
                case 'multi-select':
                    $module_object = new Saiyan_Module_MultiSelect($id, $settings);
                    break;
				case 'tabs':
					if (! isset($module['tabs'])) throw new InvalidArgumentException('Tabs is needed for the tabs module');
					$module_object = new Saiyan_Module_Tabs($id, $module['tabs'], $settings);
					break;
				case 'accordion':
					if (! isset($module['sections'])) throw new InvalidArgumentException('Sections is needed for the accordion module');
					$module_object = new Saiyan_Module_Accordion($id, $module['sections'], $settings);
					break;
				case 'pinned-notes':
					if (! isset($module['pins_notes'])) throw new InvalidArgumentException('Pins Notes is needed for the pinned-notes module');
					$module_object = new Saiyan_Module_PinnedNotes($id, $module['pins_notes'], $settings);
					break;
				case 'navigator':
					if (! isset($module['way_points'])) throw new InvalidArgumentException('Way Points is needed for the navigator module');
					$module_object = new Saiyan_Module_Navigator($id, $module['way_points'], $settings);
					break;
				case 'sortable-item':
					if (! isset($module['item_struct'])) throw new InvalidArgumentException('Item structure (contains modules) is needed for the sortable item module');
					$module_object = new Saiyan_Module_SortableItem($id, $module['item_struct'], isset($module['item_values']) ? $module['item_values'] : null, $settings);
					break;
				case 'collapsible':
					$module_object = new Saiyan_Module_Collapsible($id, $settings);
					break;
				case 'wizard':
					if (! isset($module['steps'])) throw new InvalidArgumentException('Steps is needed for the wizard module');
					$module_object = new Saiyan_Module_Wizard($id, $module['steps'], $settings);
					break;
			}

			if ($module_object == null) {
				throw new InvalidArgumentException('Module type "' . $type . '" not initialized');
			}

			self::$rendered_module[$id] = $module;

			// echo '<div data-saiyan-module="' . $type . '">';

			if ($module_object instanceof ISaiyan_Container) {
				if ( $module['children'] === null && ! is_array($module['children'])) throw new InvalidArgumentException('Children is required for container modules');
				$children = $module['children'];
				$module_object->start();
				$module_object->render_module();
				foreach ($children as $key => $child_module) {
					$mod_obj = null;
					$mod_obj[$key] = $child_module;
					self::render_module($mod_obj);
					// print_r($child_module['params']);
				}
				$module_object->end();
			}
			else $module_object->render_module();

			// echo '</div>';
		}

		/**
		 * Returns the module setup from the given module ID.
		 *
		 * @param string $id The key/ID from the module setup (render_module_mapping)
		 *
		 * @return array The module setup
		 * @throws Exception
		 */
		public static function get_rendered_module($id) {
			if (! empty(self::$rendered_module) && isset(self::$rendered_module[$id]))
				return self::$rendered_module[$id];
			else throw new Exception('Module ' . $id . ' not found');
		}
	}
}