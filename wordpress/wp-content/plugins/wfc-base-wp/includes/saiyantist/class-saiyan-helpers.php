<?php

/**
 * Class Saiyan_Helpers
 *
 * A class containing static methods that will handle common stuffs needed in the core modules
 *
 * @since 1.0.0
 * @author Von Sienard Vibar
 */
class Saiyan_Helpers {

	/**
	 * Checks if the module type is valid
	 *
	 * @param string $type
	 *
	 * @return bool
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function validate_module_type($type) {
		return in_array($type, Saiyan::$module_dirs);
	}

	/**
	 * Checks if the parameters is valid
	 *
	 * @param $params
	 *
	 * @return bool
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function validate_settings($params) {
		if (! is_array($params)) return false;
		return true;
	}

	/**
	 * Checks if the theme contains the main and accent object
	 *
	 * @param $theme
	 *
	 * @return bool
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function validate_theme($theme) {
		if (! is_array($theme)) return false;
		if (empty($theme)) return false;
		if (! isset($theme['main']) && ! isset($theme['accent'])) return false;
		return true;
	}

	/**
	 * Validates the module mapping
	 *
	 * @param $mapping
	 *
	 * @return bool
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function validate_mapping($mapping) {
		$module = reset($mapping);
		if (! is_array($mapping)) return false;
		if (empty($mapping)) return false;
		if (! isset($module['type']) && ! isset($module['params'])) return false;
		return true;
	}

	/**
	 * Prints out style attribute on the element from the module settings
	 *
	 * @param $style
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function print_style($style) {
		echo !empty($style) ? 'style=\'' . $style . '\'' : '';
	}

	/**
	 * Returns a formalized Datetime string
	 *
	 * @param string $date Datetime string
	 *
	 * @return false|string Formatted Datetime string
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function get_formalize_datetime_string($date) {
		$pattern = 'M j, Y g:i A';
		return date($pattern, strtotime($date));
	}

	/**
	 * Returns a formalized Date string
	 *
	 * @param string $date Datetime string
	 *
	 * @return false|string Formatted Date string
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function get_formalize_date_string($date) {
		$pattern = 'M j, Y';
		return date($pattern, strtotime($date));
	}

	/**
	 * Returns a formalized Datetime string
	 *
	 * @param string $date Datetime string
	 *
	 * @return false|string Formatted Time string
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function get_formalize_time_string($date) {
		$pattern = 'g:i A';
		return date($pattern, strtotime($date));
	}

	/**
	 * Custom wp_localize_script that has a functionality that checks
	 * if the object name exist. If it exist, parse data to its existing data from the object name
	 *
	 * @param string        $handle         Handle name for the enqueue script
	 * @param string        $object_name    Object name to be passed on the js controller
	 * @param array|mixed   $l10n           Data to be passed on the js controller
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar
	 */
	public static function wp_localize_script($handle, $object_name, $l10n) {
		global $wp_scripts;
		$data = $wp_scripts->get_data($handle, 'data');

		if (empty($data)) {
			wp_localize_script($handle, $object_name, $l10n);
		}
		else {
			if (! is_array($data)) {
				$data = json_decode(str_replace('var ' . $object_name . ' = ', '', substr($data, 0, -1)), true);
			}
			foreach ($data as $key => $value) {
				$localize_data[$key] = $value;
			}
			foreach ($l10n as $key => $value) {
				$localize_data[$key] = $value;
			}
			$wp_scripts->add_data($handle, 'data', '');
			wp_localize_script($handle, $object_name, $localize_data);
		}
	}
}