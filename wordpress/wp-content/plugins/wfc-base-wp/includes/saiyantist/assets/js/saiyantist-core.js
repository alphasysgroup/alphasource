/**
 * Contains functionalities that will be used throughout the modules
 * @type {{}}
 *
 * @since 1.0.0
 * @author Von Sienard Vibar
 */
var SaiyantistCore = {};

(function($) {
    SaiyantistCore = {
        actionType: {
            post: 'POST',
            get: 'GET'
        },

        ajaxLoaderUI: null,
        snackbarUI: null,

        init: function() {
            $('body').append(
                $('<div></div>')
                    .addClass('saiyan-progress-wrapper')
                    .append(
                        $('<div></div>')
                            .addClass('progress-bar'),
                        $('<div></div>')
                            .addClass('snackbar')
                    )
            );

            this.ajaxLoaderUI = $('body > .saiyan-progress-wrapper .progress-bar');
            this.snackbarUI = $('body > .saiyan-progress-wrapper .snackbar');

            this.findAndBind();
        },

        // update for < es6 backward compatibility
        ajaxLoader: function(action, type, data, preCallback, postCallback, message) {
            message = message || null;

            var progressBar = this.ajaxLoaderUI;
            progressBar.addClass('load');
            $.ajax({
                url: action,
                type: type,
                data: data,
                beforeSend: preCallback,
                xhr: function () {
                    var xhr = $.ajaxSettings.xhr();
                    xhr.onprogress = function (e) {
                        // For downloads
                        if (e.lengthComputable) {
                            // console.log(e.loaded / e.total);
                            progressBar.css('width', (e.loaded / e.total) * 100 + '%');
                        }
                    };
                    xhr.upload.onprogress = function (e) {
                        // For uploads
                        if (e.lengthComputable) {
                            // console.log(e.loaded / e.total);
                        }
                    };
                    return xhr;
                }
            }).done(function (e) {
                if (message !== null) {
                    setTimeout(function() {
                        progressBar.css({
                            'margin': 'auto',
                            'width': '300px'
                        });

                        setTimeout(function() {
                            // progressBar.addClass('show-message');
                            SaiyantistCore.snackbar.show(message, null, SaiyantistCore.snackbar.status.success);

                            setTimeout(function() {
                                progressBar.removeClass('load');
                                SaiyantistCore.snackbar.hide();
                                postCallback(e);
                            }, 5000);
                        }, 350);
                    }, 1000);
                }
                else {
                    postCallback(e);
                }
            }).fail(function (jqHXR, status) {
                console.log('error!');
                SaiyantistCore.snackbar.show(status, 5000, SaiyantistCore.snackbar.status.error);
            });
        },
        ajaxLoaderComplete: function() {
            this.ajaxLoaderUI.css({
                // 'margin': 'auto',
                'width': '0'
            });
            setTimeout(function() {
                SaiyantistCore.ajaxLoaderUI.removeClass('load show-message');
            }, 3000);
        },

        snackbar: {
            status: {
                info: 'info',
                success: 'success',
                warning: 'warning',
                error: 'error'
            },
            // update for < es6 backward compatibility
            show: function(message, timeout, status, callback) {
                callback = callback || null;

                if (typeof status === 'undefined') status = 'info';
                SaiyantistCore.snackbarUI.html(message).addClass('show ' + status);
                // TODO fix bug for status change
                if (typeof timeout !== 'undefined' && timeout !== null) {
                    setTimeout(function() {
                        SaiyantistCore.snackbar.hide();
                        if (callback !== null) callback();
                    }, timeout);
                }
            },
            hide: function() {
                SaiyantistCore.snackbarUI.html('').attr('class', 'snackbar');
            }
        },


        tinter: {
            show: function() {
                var tinterClass = 'saiyan-tinter';
                var body = $('body');

                if (body.find('.' + tinterClass).length === 0) {
                    body.append(
                        $('<div class="' + tinterClass + '"></div>').css({
                            'position': 'fixed',
                            'background-color': 'rgba(255, 255, 255, 0.5)',
                            'top': '0',
                            'left': '0',
                            'width': '100%',
                            'height': '100%',
                            'opacity': '0.0',
                            'z-index': '9991'
                        }).animate({
                            'opacity': '1.0'
                        }, 300, 'easeOutCubic')
                    )
                }
            },
            hide: function() {
                var tinterClass = 'saiyan-tinter';
                var body = $('body');
                var tinter = body.find('.' + tinterClass);

                if (tinter.length === 0) console.log('Saiyan tinter is already closed');
                else {
                    tinter.animate({
                        'opacity': '0.0'
                    }, 300, 'easeOutCubic', function() {
                        tinter.remove();
                    })
                }
            }
        },


        findAndBind: function() {
            $('[data-saiyan-dependency-id]').each(function() {
                var $this = $(this);
                var triggerId = $this.data('saiyanDependencyId');
                var triggerElem = $(triggerId);

                SaiyantistCore.triggerBind($this, triggerElem);

                triggerElem.on('change', function() {
                    SaiyantistCore.triggerBind($this, $(this));
                });
            })
        },
        triggerBind: function(elem, triggerElem) {
            var $trigger = triggerElem;
            var parent = $trigger.closest('[data-saiyan-module]');
            var moduleType = parent.data('saiyanModule');
            var triggerVal = null;
            var $thisTriggerVal = elem.data('saiyanDependencyTriggerValue');
            var $thisTriggerAction = typeof elem.data('saiyanDependencyTriggerAction') !== 'undefined' ?
                elem.data('saiyanDependencyTriggerAction') : 'show_hide';

            elem.css('transition', '300ms ease-out');

            switch (moduleType) {
                case 'switch':
                    triggerVal = $trigger.closest('[data-saiyan-module]').find('input').is(':checked');

                    switch ($thisTriggerVal) {
                        case 'on':
                            if (triggerVal) SaiyantistCore.triggerBindActionPositive(elem, $thisTriggerAction);
                            else SaiyantistCore.triggerBindActionNegative(elem, $thisTriggerAction);
                            break;
                        case 'off':
                            if (! triggerVal) SaiyantistCore.triggerBindActionPositive(elem, $thisTriggerAction);
                            else SaiyantistCore.triggerBindActionNegative(elem, $thisTriggerAction);
                            break;
                    }
                    break;
                case 'radio':
                    triggerVal = $trigger.closest('[data-saiyan-module]').find('input:checked').val();

                    if (triggerVal === $thisTriggerVal) SaiyantistCore.triggerBindActionPositive(elem, $thisTriggerAction);
                    else SaiyantistCore.triggerBindActionNegative(elem, $thisTriggerAction);
                    break;
            }
        },
        triggerBindActionPositive: function(elem, action) {
            if (action === '') action = 'show_hide';
            switch (action) {
                case 'show_hide':
                    elem.slideDown();
                    break;
                case 'access':
                    elem.removeClass('not-accessible');
                    break;
            }
        },
        triggerBindActionNegative: function(elem, action) {
            if (action === '') action = 'show_hide';
            switch (action) {
                case 'show_hide':
                    elem.slideUp();
                    break;
                case 'access':
                    elem.addClass('not-accessible');
                    break;
            }
        }
    };

    $(document).ready(function() {
        SaiyantistCore.init();
    });
})(jQuery);