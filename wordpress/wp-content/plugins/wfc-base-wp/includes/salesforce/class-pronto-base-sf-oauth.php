<?php 

/**
* Proonto Ultimate Salesforce Class
*/
class Pronto_Ultimate_SF_Oauth extends Pronto_Ultimate_Logs
{
	/**
    * Name : client_id
    * Description: salesforce client id
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
	public $client_id;

	/**
    * Name : client_secret
    * Description: salesforce client secret
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $client_secret;

	/**
    * Name : login_url
    * Description: salesforce login url
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $login_url;

	/**
    * Name : token_url
    * Description: salesforce api urls
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $token_url;

	/**
    * Name : callback_url
    * Description: salesforce connected apps callback url
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $callback_url;

	/**
    * Name : access_token
    * Description: salesforce access token
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $access_token;

	/**
    * Name : refresh_token
    * Description: salesforce refresh token
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $refresh_token;

	/**
    * Name : instance_url
    * Description: salesforce org instance url
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $instance_url;

	/**
    * Name : connect
    * Description: plugin settings where to connect sandbox/production
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $connect;

	/**
    * Name : plugin
    * Description: determine which plugin calling salesforce api
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
    public $plugin;

    /**
     * Salesforce Instance ID
     *
     * @var string $sf_instance_id
     *
     * @author Von Sienard Vibar <von@alphasys.com.au>
     *
     * @LastUpdate September 10, 2019
     */
    public $sf_instance_id;

	/**
    * Name : __construct
    * Description: class construct function, sets credentials and tokens 
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $plugin plugin path that calls sf api class
    * @param string $sf_instance_id Salesforce instance ID
    * @since    1.0.0
    */
	function __construct( $plugin, $sf_instance_id = '' )
	{
		/**
		* grab credentials
		*/
		$this->grab_credentials();

		/**
		* grab tokens
		*/
		$this->grab_tokens();

		/**
		* get path
		*/
		$this->plugin = $plugin;

        /**
         * get Salesforce instance ID
         */
        if ($sf_instance_id !== '') {
            $this->set_sf_instance($sf_instance_id);
        } else {
            $wfcbase_multiinstance_settings = get_option('WFCBASE_MultiInstanceSettings');

            if (isset($wfcbase_multiinstance_settings['default_instance']) && ! empty($wfcbase_multiinstance_settings['default_instance'])) {
                $this->set_sf_instance($wfcbase_multiinstance_settings['default_instance']);
            } else {
                return array(
                    'status' => 'failed',
                    'message' => 'Please provide a Salesforce instance and set it as default'
                );
            }
        }
	}

	/**
    * Name : grab_credentials
    * Description: grab set credentials
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $mode whether sandbox or production
    * @since    1.0.0
    */
	private function grab_credentials( $mode = null ) {
		$data = get_option( 'pub_oauth_credential', 0 );
		if( $data != 0 ) {

			if( null !== $mode ) {
				$this->connect = $mode;
			} else {
				$this->connect = isset( $data['connect'] ) ? $data['connect'] : 'sandbox';
			}

			$con = ( $this->connect== 'sandbox' ) ? 'sandbox' : 'live';
			$this->client_id = isset( $data["{$con}_client_id"] ) ? trim( $data["{$con}_client_id"] ) : '';
			$this->client_secret = isset( $data["{$con}_client_secret"] ) ? trim( $data["{$con}_client_secret"] ) : '';
			$this->login_url = ( $con == 'sandbox' ) ? 'https://test.salesforce.com' : 'https://login.salesforce.com';
			$this->callback_url = isset( $data["{$con}_callback_url"] ) ? trim( $data["{$con}_callback_url"] ) : '';
		}
	}

    /**
     * Gets the Salesforce instance credentials from the set Salesforce instance ID
     *
     * @author Von Sienard Vibar <von@alphasys.com.au>
     *
     * @LastUpdated September 10, 2019
     */
	public function grab_sf_instance_credentials() {
        $sforginstancesettings = get_option($this->sf_instance_id);

        $wfc_base_instancetype = isset($sforginstancesettings['wfc_base_instancetype']) ? $sforginstancesettings['wfc_base_instancetype'] : 'sandbox';

        $this->client_id = isset($sforginstancesettings['wfc_base_clientid']) ? $sforginstancesettings['wfc_base_clientid'] : '';
        $this->client_secret = $wfc_base_clientsecret = isset($sforginstancesettings['wfc_base_clientsecret']) ? $sforginstancesettings['wfc_base_clientsecret'] : '';
        $this->login_url = $wfc_base_instancetype == 'production' ? 'https://login.salesforce.com' : 'https://test.salesforce.com';
        $this->callback_url = isset($sforginstancesettings['wfc_base_callbackurl']) ? $sforginstancesettings['wfc_base_callbackurl'] : get_site_url() . '/wp-admin/admin-post.php?action=authorization&wfcbaseinst=' . $this->sf_instance_id;
    }

	/**
    * Name : grab_tokens
    * Description: grab stored tokens
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @since    1.0.0
    */
	private function grab_tokens() {
		$tokens = get_option( "pronto_ultimate_{$this->connect}_tokens", 0 );
		if( $tokens != 0 ) {
			$this->access_token = isset( $tokens['access_token'] ) ? trim( $tokens['access_token'] ) : '';
			$this->refresh_token = isset( $tokens['refresh_token'] ) ? trim( $tokens['refresh_token'] ) : '';
			$this->instance_url = isset( $tokens['instance_url'] ) ? trim( $tokens['instance_url'] ) : '';
		}
	}

    /**
     * Gets the Salesforce instance tokens stored after authorization is success from the set Salesforce instance ID
     *
     * @author Von Sienard Vibar <von@alphasys.com.au>
     *
     * @LastUpdated September 11, 2019
     */
	private function grab_sf_instance_tokens() {
        $sforginstancesettings = get_option($this->sf_instance_id);

        if (isset($sforginstancesettings['tokens']) && ! empty($sforginstancesettings['tokens'])) {

            $tokens = $sforginstancesettings['tokens'];

            $this->access_token = isset( $tokens['access_token'] ) ? trim( $tokens['access_token'] ) : '';
            $this->refresh_token = isset( $tokens['refresh_token'] ) ? trim( $tokens['refresh_token'] ) : '';
            $this->instance_url = isset( $tokens['instance_url'] ) ? trim( $tokens['instance_url'] ) : '';
        }
    }

    /**
     * Sets the current Salesforce instance
     *
     * @param string $sf_instance_id
     *
     * @author Von Sienard Vibar <von@alphasys.com.au>
     *
     * @LastUpdated September 11, 2018
     */
	public function set_sf_instance($sf_instance_id) {
	    $this->sf_instance_id = $sf_instance_id;

	    $this->grab_sf_instance_credentials();
	    $this->grab_sf_instance_tokens();
    }

	/**
    * Name : send
    * Description: send api to salesforce
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $query will be your sf custom query or url of sf rest api /apexrest/ or data/v37.0/query?q=' . urlencode( $query )
    * @param string or object $fragment data request to salesforce
    * @param string $method type of api request
    * @param boolean $logs determine if api request will be save to logs
    * @param boolean $resync determine if api request will be resync if fails
    * @return object data
    * @since    1.0.0
    */
	private function send( $query, $fragment = null, $method = 'authorize', $logs = true, $resync = false ) {

		/**
		* checking url
		*/
		if( $method == 'authorize' ) {
			/**
			* authorization token url
			*/
			$this->token_url = $this->login_url;
		} else {
			$this->token_url = $this->instance_url;
		}
		$this->token_url = $this->token_url . '/services/' . $query;
		
        $curl = curl_init( $this->token_url );
        curl_setopt( $curl, CURLOPT_HEADER, false );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );

        /**
		* for salesforce api queries
		*/
        if( $method != 'authorize' && $method != 'revoke' ) {
        	curl_setopt( $curl, CURLOPT_HTTPHEADER, array(
        		"Authorization: OAuth " . $this->access_token,
				"Content-type: application/json"
			) );
        } elseif( $method == 'revoke' ) {
        	curl_setopt( $curl, CURLOPT_HTTPHEADER, array(
				"Content-type: application/x-www-form-urlencoded"
			) );
        }

        /**
		* curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_1);
		*/
        curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, TRUE );
        curl_setopt( $curl, CURLOPT_CAINFO, plugin_dir_path(__FILE__) . "cacert.pem" );

        /**
		* -Create record-
		*/
		if( strtolower( $method ) == "create" )
		{
			$content = json_encode( $fragment );
			curl_setopt( $curl, CURLOPT_POST, TRUE );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $content );
		}

		/**
		* -Update record-
		*/
		else if( strtolower( $method ) == "update" )
		{
			$content = json_encode( $fragment );
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "PATCH" );
			curl_setopt( $curl, CURLOPT_POSTFIELDS, $content );
		}

		/**
		* -Delete record-
		*/
		else if( strtolower( $method ) == "delete")
		{
			curl_setopt( $curl, CURLOPT_CUSTOMREQUEST, "DELETE" );
		}

		/**
		* -Authorization-
		*/
		else if( strtolower( $method ) == "authorize" || strtolower( $method ) == "revoke" ) {

			curl_setopt( $curl, CURLOPT_POST, true );
        	curl_setopt( $curl, CURLOPT_POSTFIELDS, $fragment );
		}

		/**
		* -retrieve record-
		*/
		else
		{
			curl_setopt( $curl, CURLOPT_POST, FALSE);
		}

        $response = json_decode( curl_exec( $curl ), true );
        $status = curl_getinfo( $curl, CURLINFO_HTTP_CODE );

        $result = array(
        	'curl_response' => array(
			    "curl_error" => curl_error($curl),
			    "curl_errno" => curl_errno($curl)
       		)
		);

		if( $status == 200 || $status == 201 || $status == 204 ) {
			if( !empty( $response ) ) {
	        	foreach( $response as $key=>$item ){
					$result['sf_response'][$key] = $item;
				}
        	}
		} elseif( $status == 400 &&  isset( $response['error_description'] ) 
			&& $response['error_description'] == 'expired authorization code' ) {

			/**
			* access code has been expired
			*/
            return array(
            	'status' => 'failed',
            	'message' => 'new code required'
            );

		} else {
			if( is_object( $response ) || is_array( $response ) )
			{
				foreach( $response as $key=> $item ){
					$result['sf_error'][$key] = $item;
				}
			}
		}

        $result['status_code'] = $status;

        curl_close($curl);

        /**
		* log request
		*/
        if( $logs == true ) {
        	$this->log( $fragment, $method, $result, $resync );
        }

        return($result);
    }

	/**
    * Name : redirect_to_get_access_code
    * Description: redirect to salesforce for authorization
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param
    * @return 
    * @since    1.0.0
    */
	public function redirect_to_get_access_code() {
		$auth_url = $this->login_url . "/services/oauth2/authorize?response_type=code&client_id=" . $this->client_id . "&redirect_uri=" . urlencode( $this->callback_url );
        header( 'Location: ' . $auth_url );
	}

	/**
    * Name : validate_authorization
    * Description: validating authorization
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param
    * @return 
    * @since    1.0.0
    */
	public function validate_authorization() {

		/**
		* get access code
		*/
        if ( !isset( $_GET['code'] ) ) {
            $this->redirect_to_get_access_code();
        }

        /**
		* get access token
		*/
        $fragment = "grant_type=authorization_code"
        . "&code=" . $_GET['code']
        . "&client_id=" . $this->client_id
        . "&client_secret=" . $this->client_secret
        . "&redirect_uri=" . urlencode( $this->callback_url );

        /**
		* validate code
		*/
        $oauth = $this->send( 'oauth2/token', $fragment );
        if( isset( $oauth['status'] ) && $oauth['message'] == 'new code required' ) {
        	$this->redirect_to_get_access_code();
        } elseif( isset( $oauth['sf_error'] ) ) {
        	$error['status'] = 'failed';
        	$error['result'] = $oauth['sf_error'];
        	return $error;
        } elseif( $oauth['curl_response']['curl_errno'] != 0 ) {
        	$error['status'] = 'failed';
        	$error['result'] = $oauth['curl_response'];
        	return $error;
        } else {
        	return $oauth;
        }
	}

	/**
    * Name : auth_with_refresh_token
    * Description: request data with refresh token
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param
    * @return 
    * @since    1.0.0
    */
	public function auth_with_refresh_token() {
		$fragment = "grant_type=refresh_token"
		. "&client_id=" . $this->client_id
		. "&client_secret=" . $this->client_secret
		. "&refresh_token=" . $this->refresh_token;

		$oauth = $this->send( 'oauth2/token', $fragment );

		/**
		* grab access token
		*/
		if( isset( $oauth['status_code'] ) && $oauth['status_code'] == 200 ) {
			$this->access_token = isset( $oauth['sf_response']['access_token'] ) ? 
        									$oauth['sf_response']['access_token'] : '';
		}
		return $oauth;
	}

	/**
    * Name : api_request
    * Description: request api to salesforce
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $query will be your sf custom query or url of sf rest api /apexrest/ or data/v37.0/query?q=' . urlencode( $query )
    * @param string or object $fragment data request to salesforce
    * @param string $method type of api request
    * @param boolean $logs determine if api request will be save to logs
    * @param boolean $resync determine if api request will be resync if fails
    * @return object data
    * @since    1.0.0
    */
	public function api_request( $query, $fragment = null, $method = 'authorize', $logs = true, $resync = false ) {
		$oauth = $this->auth_with_refresh_token();
		if( isset( $oauth['status_code'] ) && $oauth['status_code'] == 200 ) {
			return $this->send( $query, $fragment, $method, $logs, $resync );
		} else {
			return $oauth;
		}
	}

	/**
    * Name : revoke_refresh_token
    * Description: invalidate refersh token
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $token current token wish to revoke
    * @return boolean
    * @since    1.0.0
    */
	public function revoke_refresh_token( $token = null ) {
		if( $token !== null ) {
			$revoke_token = $token;
		} else {
			$revoke_token = $this->refresh_token;
		}

		$fragment = "token=" . $revoke_token;
		$response = $this->send( 'oauth2/revoke', $fragment, 'revoke' );
		if( $response['status_code'] == 200 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
    * Name : is_connected
    * Description: check salesforce connection
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $mode sandbox or production
    * @return boolean
    * @since    1.0.0
    */
	public function is_connected( $mode = null ) {

		// grab credentials
		$this->grab_credentials( $mode );

		// grab tokens
		$this->grab_tokens();

		$query = urlencode( 'SELECT count() FROM Contact' );
		$data = $this->api_request( 'data/v37.0/query?q=' . $query, '', 'retrieve' );
		if( $data['status_code'] == 200 ) {
			return true;
		} else {
			return false;
		}
	}

	/**
    * Name : api_limits
    * Description: get salesforce api limits
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $mode sandbox or production
    * @return object data
    * @since    1.0.0
    */
	public function api_limits( $mode = null ) {

		// grab credentials
		$this->grab_credentials( $mode );

		// grab tokens
		$this->grab_tokens();

		return $this->api_request( 'data/v39.0/limits', '', 'retrieve' );
	}

	/**
    * Name : log
    * Description: logging salesforce api request
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param string $fragment data api request
    * @param string $method method api request
    * @param string $result api result
    * @return
    * @since    1.0.0
    */
	public function log( $fragment, $method, $result, $resync ) {
		$advance_settings = get_option( 'pub_advance_settings', 0 );
		if( !isset( $advance_settings['logging'] ) || $advance_settings['logging'] == 'true' ) {
			$user = function_exists( 'wp_get_current_user' ) ? wp_get_current_user() : null;

			if( isset( $result['sf_error'] ) ) {
				$status = 'FAILED';
				$description = $result['sf_error'];
			} elseif( $result['curl_response']['curl_errno'] != 0 ) {
				$status = 'FAILED';
				$description = $result['curl_response'];
			} else {
				$status = 'SUCCESS';
				$description = "{$method} successful";
			}

			$logs = $this->pub_log_add(
				date("Y-m-d H:i:s"), 
				$this->get_plugin_details()['Name'], 
				isset( $user->user_email ) ? $user->user_email : 'System', 
				0, 
				$this->token_url, 
				is_array( $fragment ) ? json_encode( $fragment ) : $fragment,
				strtoupper( $method ), 
				$status, 
				is_array( $description ) ? json_encode( $description ) : $description,
				$resync,
                $this->sf_instance_id
			);
		}
	}

	/**
    * Name : escapeJsonString
    * Description: escape json string for saving safe to database 
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param
    * @return
    * @since    1.0.0
    */
	public function escapeJsonString( $value ) {
	    $escapers = array("\\", "/", "\"", "\n", "\r", "\t", "\x08", "\x0c");
	    $replacements = array("\\\\", "\\/", "\\\"", "\\n", "\\r", "\\t", "\\f", "\\b");
	    $result = str_replace($escapers, $replacements, $value);
	    return $result;
	}

	/**
    * Name : get_plugin_details
    * Description: get plugin who call sf rest api method
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param
    * @return
    * @since    1.0.0
    */
	public function get_plugin_details() {
		if ( ! function_exists( 'get_plugins' ) ) {
	        require_once ABSPATH . 'wp-admin/includes/plugin.php';
	    }

		$plugin_data = get_plugins( '/' . explode( '/', $this->plugin )[0] );
		if( sizeof( $plugin_data ) > 0 ) {		
			foreach ( $plugin_data as $plugin ) {
				return $plugin;
			}
		}

		return array(
			'Name' => '',
		    'PluginURI' => '',
		    'Version' => '',
		    'Description' => '',
		    'Author' => '',
		    'AuthorURI' => '',
		    'TextDomain' => '',
		    'DomainPath' => '',
		    'Network' => '',
		    'Title' => '',
		    'AuthorName' => ''
		);
	}
}

?>