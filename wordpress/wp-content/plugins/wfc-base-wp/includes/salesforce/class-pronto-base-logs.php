<?php 

	/**
	* Pronto avatar logs
	*/
	class Pronto_Ultimate_Logs
	{

		/**
	    * Name : pub_log_table_create
	    * Description: create logs table for pronto ultimnate base plugin
	    * LastUpdated : July 4, 2017
	    * @author   Danryl
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public static function pub_log_table_create() {
			global $wpdb;
			$db_name = DB_NAME;
			$old_table_name = $wpdb->prefix . "sf_api_logs";
			$table_name = $wpdb->prefix . "pu_base_sf_api_logs";
      		//table is not created. you may create the table here.
			if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
				$old_table_exist = self::pub_log_checkif_table_rename();
				if (!$old_table_exist) {
					$create_table_query = "CREATE TABLE $table_name (publogid bigint(20) unsigned NOT NULL auto_increment, date datetime NOT NULL default '0000-00-00 00:00:00', plugin_name varchar(50) NOT NULL, user varchar(50) NOT NULL, count int(10) unsigned NOT NULL, query varchar(800) NOT NULL, data varchar(800) NOT NULL, method varchar(50) NOT NULL, status varchar(50) NOT NULL, description varchar(800) NOT NULL, resync boolean not null default 0, sf_instance VARCHAR(100) NOT NULL DEFAULT '', PRIMARY KEY (publogid))";
					require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
					dbDelta($create_table_query);

				} else {
					$wpdb->query("RENAME TABLE " . $old_table_name . " TO " . $table_name);
				}
			}

            $row = $wpdb->get_results("SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema='{$db_name}' AND table_name='{$table_name}' AND column_name='sf_instance'");

            if (empty($row)) {
                $wpdb->query("ALTER TABLE $table_name ADD sf_instance VARCHAR(100) NOT NULL DEFAULT ''");
            }
		}

		/**
	    * Name : pub_log_checkif_table_rename
	    * Description: check if table is going to be rename
	    * LastUpdated : July 4, 2017
	    * @author   Danryl
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public static function pub_log_checkif_table_rename() {
			global $wpdb;
			$old_table_name = $wpdb->prefix . "sf_api_logs";
			$table_name = $wpdb->prefix . "pu_base_sf_api_logs";
			if ($wpdb->get_var("SHOW TABLES LIKE '$old_table_name'") == $old_table_name) {
				if ($wpdb->get_var("SHOW COLUMNS FROM " . $old_table_name . " LIKE 'uactid'") == 'uactid') {
					return true;
				}
				return false;
			}
			return false;
		}

		/**
	    * Name : pub_is_table_logs_exist
	    * Description: check if table is exist in database
	    * LastUpdated : Aug 11, 2017
	    * @author   Danryl
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function pub_is_table_logs_exist() {
			global $wpdb;
			$table_name = $wpdb->prefix . "pu_base_sf_api_logs";
			if( $result = $wpdb->query("SHOW TABLES LIKE '$table_name'") ) {
				return true;
			}
			return false;
		}

		public function pub_count_logs() {
			global $wpdb;
			$table_name = $wpdb->prefix . "pu_base_sf_api_logs";
			if( $this->pub_is_table_logs_exist() ) {
				return $wpdb->get_var( "SELECT COUNT(*) FROM $table_name" );
			} else {
				return false;
			}
		}

		/**
	    * Name : pub_get_logs
	    * Description: get pronto ultimate base logs
	    * LastUpdated : Aug 11, 2017
	    * @author   Junjie Canonio, Danryl Carpio
	    * @param    array $filter, for additional filters for query
	    * @param    array $additional, for additional string query
	    * @return
	    * @since    1.0.0
	    */
		public function pub_get_logs( 
			$filter = null,
			$orderByField = null,
			$orderByDirection = 'ASC',
			$limit = null,
			$offset = null 
		) {
			global $wpdb;

			if( !$this->pub_is_table_logs_exist() ) {
				return false;
			}

			$table_name = $wpdb->prefix . "pu_base_sf_api_logs";
			$sql = "SELECT * FROM $table_name";

			$parameters = [];
			if( null !== $filter ) {
				$sql .= ' WHERE';
				$i = 0;
				foreach( $filter as $fieldName => $value ) {
					$operator = $value['operator'];
					$fieldValues = $value['value'];

					$sql .= ( $i != 0 ) ? ' AND' : '';
					if ($operator === 'IN') {
						$token = ( gettype( $fieldValues[0] ) == 'string' ) ? '%s' : '%d';
						$parameters = array_merge($parameters, $fieldValues);
						$sql .= " $fieldName IN (" . implode(',', array_fill(0, count($fieldValues), $token)) . ")";
					} else {
						$fieldValue = $fieldValues; // only one value if not an IN clause
						$token = ( gettype( $fieldValue ) == 'string' ) ? '%s' : '%d';
						$parameters[] = $fieldValue;
						$sql .= " $fieldName $operator $token";
					}
					$i++;
				}
			}

			if (null !== $orderByField) {
				$sql .= " ORDER BY $orderByField $orderByDirection";
			}

			if (null !== $limit) {
				$limit = (int)$limit;
				$sql .= " LIMIT $limit";
			}

			if (null !== $offset) {
				$offset = (int)$offset;
				$sql .= " OFFSET $offset";
			}

			if (empty($parameters)) {
                return $wpdb->get_results( $sql, ARRAY_A );
            } else {
                return $wpdb->get_results( $wpdb->prepare( $sql, $parameters ), ARRAY_A );
            }


		}

		/**
	    * Name : pub_log_add
	    * Description: add pronto ultimate base logs
	    * LastUpdated : Aug 11, 2017
	    * @author   Danryl
	    * @param    string $date, log date
	    * @param    string $plugin_name, plugin name who reqeusted api
	    * @param    string $user, login user
     	* @param    string $count, re-sync counter
        * @param    string $query, query url for api request
        * @param    string or object $data, data request for api
        * @param    string $method, api request method
        * @param    string $status, status of api request
        * @param    string or object $description, description of api request
	    * @return   false if error, number if success represents rows affected
	    * @since    1.0.0
	    */
		public function pub_log_add( $date, $plugin_name, $user, $count, $query, $data, $method, $status, $description, $resync, $sf_instance = '' ) {
			global $wpdb;
	        $table_name = $wpdb->prefix . "pu_base_sf_api_logs";

	        if( $this->pub_is_table_logs_exist() ) {
	        	return $wpdb->query( $wpdb->prepare( "INSERT INTO $table_name (date, plugin_name, user, count, query, data, method, status, description, resync, sf_instance) VALUES ( %s, %s, %s, %d, %s, %s, %s, %s, %s, %d, %s )", $date, $plugin_name, $user, $count, $query, $data, $method, $status, $description, $resync, $sf_instance ) );
	        } else {
	        	return false;
	        }
		}

		/**
	    * Name : pub_log_update
	    * Description: update pronto ultimate base logs
	    * LastUpdated : Aug 11, 2017
	    * @author   Danryl
	    * @param    array $data, data wish to be updated
	    * @param    int $id, id of logs
	    * @return   false if error, number if success represents rows affected
	    * @since    1.0.0
	    */
		public function pub_log_update( $data, $id ) {
			global $wpdb;
	        $table_name = $wpdb->prefix . "pu_base_sf_api_logs";
	        $update = null;
	        if( !empty( $data ) ) {
	        	$i = 0;
	        	$values = array();
	        	foreach( $data  as $key => $value ) {
	        		$i++;
	        		$add = ( $i != 1 ) ? ', ' : '';
	        		$val = ( gettype( $value ) == 'string' ) ? '%s' : '%d';
	        		$values[] = $value;
	        		$update .= "{$add}{$key} = {$val}";
	        	}

	        	if( $this->pub_is_table_logs_exist() ) {
	        		return $wpdb->query( $wpdb->prepare( "UPDATE $table_name SET $update WHERE publogid = %d", array_merge( $values, array( $id ) ) ) );
	        	} else {
	        		return false;
	        	}
	        } else {
	        	return false;
	        }
		}

		/**
	    * Name : pub_log_delete
	    * Description: delete pronto ultimate base logs
	    * LastUpdated : Aug 11, 2017
	    * @author   Danryl
	    * @param    int $id, id of logs
	    * @return   false if error, number if success represents rows affected
	    * @since    1.0.0
	    */
		public function pub_log_delete( $id = null ) {
			global $wpdb;
	        $table_name = $wpdb->prefix . "pu_base_sf_api_logs";

	        if( $this->pub_is_table_logs_exist() ) {
	        	if( $id === null ) {
	        		return $wpdb->query( "TRUNCATE TABLE $table_name" );
	        	} else {
	        		return $wpdb->query( $wpdb->prepare( "DELETE FROM $table_name WHERE publogid = %d", $id ) );
	        	}
	        } else {
	        	return false;
	        }
		}
	}
?>