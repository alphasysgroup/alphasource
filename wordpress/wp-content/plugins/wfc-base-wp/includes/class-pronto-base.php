<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Pronto_Base
 * @subpackage Pronto_Base/includes
 * @author     Alphasys Pty. Ltd. <danryl@alphasys.com.au>
 */
class Pronto_Base {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Pronto_Base_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * A reference to an instance of cherry framework core class.
	 *
	 * @since 1.0.0
	 * @var   object
	 */
	public $core = null;

	/**
	 * A reference to an instance of the salesforce oauth class
	 *
	 * @since 1.0.0
	 * @var   object
	 */
	public $sf_oauth = null;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'pronto-base';
		$this->version = '1.0.0';

		/**
		 * Set constant path to the plugin directory.
		 *
		 * @since 1.0.0
		 */
		if (!defined('PUB_DIR')) {
			define( 'PUB_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		/**
		 * Set constant path to the plugin URI.
		 *
		 * @since 1.0.0
		 */
		if (!defined('PUB_URL')) {
			define( 'PUB_URL', trailingslashit( plugin_dir_url( __FILE__ ) ) );
		}

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Pronto_Base_Loader. Orchestrates the hooks of the plugin.
	 * - Pronto_Base_i18n. Defines internationalization functionality.
	 * - Pronto_Base_Admin. Defines all hooks for the admin area.
	 * - Pronto_Base_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-base-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-base-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-pronto-base-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-pronto-base-public.php';

		/**
		 * The class responsible for logging salesforce api request
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/salesforce/class-pronto-base-logs.php';

		/**
		 * The class responsible for connecting your website to slaesforce
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/salesforce/class-pronto-base-sf-oauth.php';

		/**
		 * Load custom table list
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-base-table.php';

		/**
		 * Load pronto ultimate system status
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-base-system-status.php';

		/**
		 * Load pronto ultimate API handler.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-ultimate-api-handler.php';

		/**
		 * Load Saiyantist Core Module
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/saiyantist/class-saiyantist.php';

		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/saiyantist/toolkit/class-saiyan-toolkit.php';


		/**
		 * Multi Salesforce Instance
		 */
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/multi-salesforce-instance/multi-salesforce-instance-oauth.php';
		require_once plugin_dir_path(dirname(__FILE__)) . 'includes/multi-salesforce-instance/multi-salesforce-instance-settings-helper.php';

		/**
		 * Load Cherry Core Module
		 */
		// TODO to be removed
		// require_once plugin_dir_path(dirname(__FILE__)) . 'includes/cherry-framework/setup.php';

		Saiyan::tist()
			->config(array(
				'theme' => 'wordpress'
			))
			->load();

		$this->loader = new Pronto_Base_Loader();

		// Initialize API.
		pronto_ultimate_api()->init();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Pronto_Base_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Pronto_Base_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Pronto_Base_Admin( $this->get_plugin_name(), $this->get_version(), $this );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		/**
		 * initialize cherry framework
		 */
		//add_action( 'after_setup_theme', require( PUB_DIR . 'cherry-framework/setup.php' ), 0 );
		// TODO to be removed
		// add_action( 'after_setup_theme', array( $this, 'get_core' ), 1 );
		//add_action( 'after_setup_theme', array( 'Cherry_Core', 'load_all_modules' ), 2 );

		/**
		 * Load the admin menu.
		 */
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'pub_menu' );

		/**
		 * enqueue script admin
		 */
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'pub_scripts' );

		/**
		 * handle authorization and saving credentials action
		 */
		$this->loader->add_action( 'admin_post_pub_authorize', $plugin_admin, 'pub_authorization' );

		/**
		 * handle retriving tokens action
		 */
		// $this->loader->add_action('admin_post_authorization', $plugin_admin, 'pub_authorization_handler' );
		// $this->loader->add_action('admin_post_nopriv_authorization', $plugin_admin, 'pub_authorization_handler' );

		/**
		 *  handle changing connection
		 */
		$this->loader->add_action( 'wp_ajax_pub_connect_to', $plugin_admin, 'pub_change_connect_to' );

		/**
		 *  handle test connection
		 */
		$this->loader->add_action( 'wp_ajax_test_sf_connection', $plugin_admin, 'pub_test_connection' );

		/**
		 *  handle test connection
		 */
		$this->loader->add_action( 'wp_ajax_revoke_connection', $plugin_admin, 'pub_revoke_token' );

		/**
		 *  handle test connection
		 */
		// TODO to be removed
		// $this->loader->add_action( 'after_setup_theme', $plugin_admin, 'pub_cherry_handlers' );

		/**
		 *   add custom cron schedules
		 */
		$this->loader->add_action( 'cron_schedules', $plugin_admin, 'pub_cron_add_weekly' );

		/**
		 * add cron callback for data syncer
		 */
		$this->loader->add_action( 'pub_data_sycher', $plugin_admin, 'pub_data_syncer' );

		/**
		 * add cron for batch processing
		 */
		$this->loader->add_action( 'pub_batch_processing', $plugin_admin, 'pub_batch_process' );

		/**
		 * add cron callback for logs cleaning
		 */
		$this->loader->add_action( 'pub_logs_cleaner', $plugin_admin, 'pub_clean_logs' );

		/**
		 * menu re order
		 */
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'pub_menu_reorder', 9999999 );

		/**
		 * add content buffering
		 */
		$this->loader->add_action( 'init', $plugin_admin, 'pub_do_output_buffer' );

		if ( class_exists( 'Pronto_Ultimate_Logs' ) ) {

			/**
			 * create table
			 */
			add_action( 'activate_plugin', array( 'Pronto_Ultimate_Logs', 'pub_log_table_create' ) );
		}

		/*
		*Create Salesforce Instance ajax
		*/
		$this->loader->add_action( 'wp_ajax_wfc_base_createsfinstance_action',$plugin_admin, 'wfc_base_createsfinstance_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_base_createsfinstance_action',$plugin_admin, 'wfc_base_createsfinstance_callback');

		/*
		* Save settings ajax
		*/
		$this->loader->add_action( 'wp_ajax_wfc_base_savesforginstancesettings_action',$plugin_admin, 'wfc_base_savesforginstancesettings_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_base_savesforginstancesettings_action',$plugin_admin, 'wfc_base_savesforginstancesettings_callback');

		$this->loader->add_action( 'wp_ajax_wfc_fetch_sf_org_instance_list',$plugin_admin, 'wfc_fetch_sf_org_instance_list_callback');

		// $this->loader->add_action( 'wp_ajax_wfc_sf_org_instance_authorize',$plugin_admin, 'wfc_sf_org_instance_authorize_callback');

        $this->loader->add_action( 'admin_post_wfc_sf_org_instance_authorize',$plugin_admin, 'wfc_sf_org_instance_authorize_callback');

        $this->loader->add_action( 'admin_post_authorization',$plugin_admin, 'wfc_sf_org_instance_authorization_handler_callback');
        $this->loader->add_action( 'admin_post_nopriv_authorization',$plugin_admin, 'wfc_sf_org_instance_authorization_handler_callback');

        $this->loader->add_action( 'wp_ajax_wfc_sf_org_instance_revoke',$plugin_admin, 'wfc_sf_org_instance_revoke_callback');

        $this->loader->add_action( 'wp_ajax_wfc_sf_org_instance_test',$plugin_admin, 'wfc_sf_org_instance_test_callback');

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Pronto_Base_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Pronto_Base_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

	/**
    * Name : get_core
    * Description: Loads the core functions. These files are needed before loading anything else in the
	*              theme because they have required functions for use.
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return object cherry framework object class
    * @since    1.0.0
    */
	public function get_core() {

		/**
		 * Fires before loads the core theme functions.
		 *
		 * @since 1.0.0
		 */
		do_action( 'cherry_team_core_before' );

		global $chery_core_version;

		if ( null !== $this->core ) {
			return $this->core;
		}

		if ( 0 < sizeof( $chery_core_version ) ) {
			$core_paths = array_values( $chery_core_version );
			require_once( $core_paths[0] );
		} else {
			die( 'Class Cherry_Core not found' );
		}

		$this->core = new Cherry_Core( array(
			'base_dir' => PUB_DIR . 'cherry-framework',
			'base_url' => PUB_DIR . 'cherry-framework',
			'modules'  => array(
				'cherry-js-core' => array(
					'autoload' => true,
				),
				'cherry-ui-elements' => array(
					'autoload' => false,
				),
				'cherry-interface-builder' => array(
					'autoload' => false,
				),
				'cherry-utility' => array(
					'autoload' => false,
				),
				'cherry-handler' => array(
					'autoload' => false,
				),
				'cherry-term-meta' => array(
					'autoload' => false,
				),
				'cherry-post-meta' => array(
					'autoload' => false,
				),
				'cherry5-insert-shortcode' => array(
					'autoload' => false,
				),
			),
		) );
		return $this->core;
	}

	/**
    * Name : pub_search_in_array
    * Description: Seach key => value to multi dimentional array
    * LastUpdated : July 4, 2017
    * @author   Danryl
    * @param    array $array, data search
    * @param    string $key, array key need to be seach
    * @param    string $value, array value need to be seach
    * @return   object cherry framework object class
    * @since    1.0.0
    */
	public function pub_search_in_array( $array, $key, $value ) {
	    $results = array();
	    if (is_array($array)) {
	        if (isset($array[$key]) && $array[$key] == $value) {
	            $results[] = $array;
	        }
	        foreach ($array as $subarray) {
	            $results = array_merge($results, search($subarray, $key, $value));
	        }
	    }
	    return $results;
	}

}
