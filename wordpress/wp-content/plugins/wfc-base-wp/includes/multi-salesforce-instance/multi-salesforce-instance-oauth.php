<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('WFC_Base_MultiSFInstanceOauth')) {
	/**
	 * Class WFC_Base_MultiSFIntanceOauth
	 * Class that contains the helper functions for the admin dashboard functionality.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>, Von Sienard Vibar <von@alphasys.com.au>
	 * @since  1.0.0
	 * @LastUpdated  September 10, 2019
	 */
	class WFC_Base_MultiSFInstanceOauth {

	    public $sf_instance_id;
	    public $sf_instance_name;

		/**
		 * Initializes values and resources needed.
		 * LastUpdated : September 2, 2019
         *
         * @param object $plugin
         * @param string $sf_instance
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 * @since  1.0.0
		 */
		public function __construct($plugin, $sf_instance_id) {

		    parent::__construct($plugin);

            $this->sf_instance_id = $sf_instance_id;

            $this->grab_credentials();
        }

		/**
		 * Name : redirect_to_get_access_code
		 * Description: redirect to salesforce for authorization
		 * LastUpdated : September 2, 2017
		 * @param $instance_id
		 * @return void
		 * @author Junjie Canonio
		 * @since    1.0.0
		 */
		public function redirect_to_get_access_code() {

		}

		private function grab_credentials() {
            $sforginstancesettings = get_option($this->sf_instance_id);

            $wfc_base_instancetype = isset($sforginstancesettings['wfc_base_instancetype']) ? $sforginstancesettings['wfc_base_instancetype'] : 'sandbox';

		    $this->client_id = isset($sforginstancesettings['wfc_base_clientid']) ? $sforginstancesettings['wfc_base_clientid'] : '';
		    $this->client_secret = $wfc_base_clientsecret = isset($sforginstancesettings['wfc_base_clientsecret']) ? $sforginstancesettings['wfc_base_clientsecret'] : '';
		    $this->login_url = $wfc_base_instancetype == 'production' ? 'https://login.salesforce.com' : 'https://test.salesforce.com';
		    $this->callback_url = isset($sforginstancesettings['wfc_base_callbackurl']) ? $sforginstancesettings['wfc_base_callbackurl'] : get_site_url() . '/wp-admin/admin-post.php?action=authorization&wfcbaseinst=' . $this->sf_instance_id;
        }

        public static function test_connection($sf_instance_id) {
            $oauth  = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ), $sf_instance_id );
            $result = $oauth->api_request( 'data/v39.0/sobjects/' . 'Lead' . '/describe', '', 'retrieve', true );

            // var_dump($result);

            $response = null;

            if ($result['status_code'] === 400 || isset($result['sf_error'])) {
                $response['status'] = 'failed';
                $response['message'] = isset($result['sf_error'][0])
                    ? $result['sf_error'][0]['error_description']
                    : $result['sf_error']['error_description'];
            } else {
                $response['status'] = 'success';
                $response['message'] = 'Successfully connected to Salesforce';
            }

            return $response;
        }
	}
	// new WFC_Base_MultiSFInstanceOauth();
}