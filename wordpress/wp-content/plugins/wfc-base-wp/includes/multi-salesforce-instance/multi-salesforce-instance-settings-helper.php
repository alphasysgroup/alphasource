<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('WFC_Base_MultiSFInstanceSettingsHelper')) {
	/**
	 * Class WFC_Base_MultiIntanceSettingsHelper
	 * Class that contains the helper functions for the admin dashboard functionality.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 * @LastUpdated  August 30, 2019
	 */
	class WFC_Base_MultiSFInstanceSettingsHelper{

		/**
		 * Initializes values and resources needed.
		 * LastUpdated : August 30, 2019
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 * @since  1.0.0
		 */
		public function __construct() {
		}

		/**
		 * Renders the video embed code of the panel that is using Video recipe.
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param  $panel
		 *
		 * @since  1.0.0
		 *
		 * @LastUpdated   August 30, 2019
		 */
		public static function render_video_embed_code( $panel )
		{
			$panel_id = isset($panel['id']) ? $panel['id'] : '';
		}

		/**
		 * This will replace all special characters with underscore
		 *
		 * @param $string
		 * @return string|string[]|null
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @since  1.0.0
		 *
		 * @LastUpdated   September 2, 2019
		 */
		public static function generate_slug($string) {
			$string = str_replace(' ', '', $string); // Replaces all spaces with hyphens.
			$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
			return 'WFCBASE_SFI'.rand(1, 1000000).'_'.$string;
		}
	}
	new WFC_Base_MultiSFInstanceSettingsHelper();
}