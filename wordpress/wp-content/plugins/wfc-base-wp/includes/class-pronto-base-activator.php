<?php

/**
 * Fired during plugin activation
 *
 * @link       https://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pronto_Base
 * @subpackage Pronto_Base/includes
 * @author     Alphasys Pty. Ltd. <danryl@alphasys.com.au>
 */
class Pronto_Base_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		/**
	    * Name : pub_data_sycher
	    * Description: cron for logs data syncer
	    * LastUpdated : July 4, 2017
	    * @author Danryl
	    * @param 
	    * @return 
	    * @since    1.0.0
	    */
		if ( !wp_next_scheduled ( 'pub_data_sycher' ) ) {
			wp_schedule_event( time(), 'daily', 'pub_data_sycher' );
		}


		/**
	    * Name : pub_logs_cleaner
	    * Description: cron for logs cleaner
	    * LastUpdated : July 4, 2017
	    * @author Danryl
	    * @param 
	    * @return 
	    * @since    1.0.0
	    */
		if ( !wp_next_scheduled ( 'pub_logs_cleaner' ) ) {
			wp_schedule_event( time(), 'daily', 'pub_logs_cleaner' );
		}
	}

}
