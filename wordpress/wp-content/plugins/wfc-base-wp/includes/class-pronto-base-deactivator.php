<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pronto_Base
 * @subpackage Pronto_Base/includes
 * @author     Alphasys Pty. Ltd. <danryl@alphasys.com.au>
 */
class Pronto_Base_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {


		/**
	    * Name : pub_data_sycher
	    * Description: cron for logs data syncer
	    * LastUpdated : July 4, 2017
	    * @author Danryl
	    * @param 
	    * @return 
	    * @since    1.0.0
	    */
		wp_clear_scheduled_hook( 'pub_data_sycher' );


		/**
	    * Name : pub_logs_cleaner
	    * Description: cron for logs cleaner
	    * LastUpdated : July 4, 2017
	    * @author Danryl
	    * @param 
	    * @return 
	    * @since    1.0.0
	    */
		wp_clear_scheduled_hook( 'pub_logs_cleaner' );
	}

}
