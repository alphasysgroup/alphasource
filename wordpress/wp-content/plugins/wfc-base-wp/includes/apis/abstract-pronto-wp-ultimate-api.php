<?php
/**
 * It handles every API used in Pronto Ultimate plugins.
 *
 * Last updated: 08/17/2017
 *
 * @link 		https://alphasys.com.au/
 *
 * @version 	1.0
 * @package		Pronto_Wp_Portal
 * @subpackage	Pronto_Wp_Portal/includes/apis
 * @author 		AlphaSys <carl@alphasys.com.au>
 * @abstract
 */
abstract class Pronto_Wp_Ultimate_Api {

	/**
	 * Requires all child classes to define this class. 
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	WP_REST_Request 	$request		Standard API request format.
	 */
	abstract public function api_callback( WP_REST_Request $request );

	/**
	 * Set default values:
	 * * `$method`
	 * * `$version`
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	string 		$method			Method used in API.
	 * @param 	string 		$version		Version of the api that is concatenated with namespace.
	 */
	public function __construct( $method = 'POST', $version = 'v1' ) {
		$this->set_method( $method );
		$this->set_version( $version );
	}

	/**
	 * Used to set `$method` property.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	string 		$method		Method used in API.
	 */
	public function set_method( $method ) {
		$acceptable_methods = array( 'post', 'get' );

		if( in_array( strtolower( $method ), $acceptable_methods ) )
			$this->method = $method;
	}

	/**
	 * Retrieves the `$method` property.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @return 	string
	 */
	public function get_method() {
		return $this->method;
	}

	/**
	 * Used to set `$version` property.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	string 		$version		Version of the api that is concatenated with namespace.
	 */
	public function set_version( $version ) {
		$this->version = $version;
	}

	/**
	 * Retrieves the `$version` property.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @return 	string
	 */
	public function get_version() {
		return $this->version;
	}

}