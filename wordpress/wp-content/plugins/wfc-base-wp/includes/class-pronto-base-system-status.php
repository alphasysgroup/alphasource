<?php 
	
	/**
	* Pronto Ultimate System Status
	*/
	class PU_System_Status
	{
		
		function __construct()
		{
			
		}

		/**
	    * Name : get_environment_info
	    * Description: get server and wordpress information
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl, by Powered by woocommerce
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_environment_info() {

			global $wpdb;

			// Figure out cURL version, if installed.
			$curl_version = '';
			if ( function_exists( 'curl_version' ) ) {
				$curl_version = curl_version();
				$curl_version = $curl_version['version'] . ', ' . $curl_version['ssl_version'];
			}

			// WP memory limit
			$wp_memory_limit = $this->let_to_num( WP_MEMORY_LIMIT );
			if ( function_exists( 'memory_get_usage' ) ) {
				$wp_memory_limit = max( $wp_memory_limit, $this->let_to_num( @ini_get( 'memory_limit' ) ) );
			}

			return array(
				'home_url'                  => get_option( 'home' ),
				'site_url'                  => get_option( 'siteurl' ),
				'wp_version'                => get_bloginfo( 'version' ),
				'wp_multisite'              => is_multisite(),
				'wp_memory_limit'           => $wp_memory_limit,
				'wp_debug_mode'             => ( defined( 'WP_DEBUG' ) && WP_DEBUG ),
				'wp_cron'                   => ! ( defined( 'DISABLE_WP_CRON' ) && DISABLE_WP_CRON ),
				'language'                  => get_locale(),
				'server_info'               => $_SERVER['SERVER_SOFTWARE'],
				'php_version'               => phpversion(),
				'php_post_max_size'         => $this->let_to_num( ini_get( 'post_max_size' ) ),
				'php_max_execution_time'    => ini_get( 'max_execution_time' ),
				'php_max_input_vars'        => ini_get( 'max_input_vars' ),
				'curl_version'              => $curl_version,
				'suhosin_installed'         => extension_loaded( 'suhosin' ),
				'max_upload_size'           => wp_max_upload_size(),
				'mysql_version'             => ( ! empty( $wpdb->is_mysql ) ? $wpdb->db_version() : '' ),
				'default_timezone'          => date_default_timezone_get(),
				'fsockopen_or_curl_enabled' => ( function_exists( 'fsockopen' ) || function_exists( 'curl_init' ) ),
				'soapclient_enabled'        => class_exists( 'SoapClient' ),
				'domdocument_enabled'       => class_exists( 'DOMDocument' ),
				'gzip_enabled'              => is_callable( 'gzopen' ),
				'mbstring_enabled'          => extension_loaded( 'mbstring' ),
			);
		}

		/**
	    * Name : get_active_plugins
	    * Description: get active plugins
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl, by Powered by woocommerce
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_active_plugins() {
			require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
			require_once( ABSPATH . 'wp-admin/includes/update.php' );

			if ( ! function_exists( 'get_plugin_updates' ) ) {
				return array();
			}

			// Get both site plugins and network plugins
			$active_plugins = (array) get_option( 'active_plugins', array() );
			if ( is_multisite() ) {
				$network_activated_plugins = array_keys( get_site_option( 'active_sitewide_plugins', array() ) );
				$active_plugins            = array_merge( $active_plugins, $network_activated_plugins );
			}

			$active_plugins_data = array();
			$available_updates   = get_plugin_updates();

			foreach ( $active_plugins as $plugin ) {
				$data           = get_plugin_data( WP_PLUGIN_DIR . '/' . $plugin );
				$dirname        = dirname( $plugin );
				$version_latest = '';
				$slug           = explode( '/', $plugin );
				$slug           = explode( '.', end( $slug ) );
				$slug           = $slug[0];

				if ( isset( $available_updates[ $plugin ]->update->new_version ) ) {
					$version_latest = $available_updates[ $plugin ]->update->new_version;
				}

				// convert plugin data to json response format.
				$active_plugins_data[] = array(
					'plugin'            => $plugin,
					'name'              => $data['Name'],
					'version'           => $data['Version'],
					'version_latest'    => $version_latest,
					'url'               => $data['PluginURI'],
					'author_name'       => $data['AuthorName'],
					'author_url'        => esc_url_raw( $data['AuthorURI'] ),
					'network_activated' => $data['Network'],
				);
			}

			return $active_plugins_data;
		}

		/**
	    * Name : get_security_info
	    * Description: get security information of the site
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl, by Powered by woocommerce
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_security_info() {
			$check_page = get_home_url();
			return array(
				'secure_connection' => 'https' === substr( $check_page, 0, 5 ),
				'hide_errors'       => ! ( defined( 'WP_DEBUG' ) && defined( 'WP_DEBUG_DISPLAY' ) && WP_DEBUG && WP_DEBUG_DISPLAY ) || 0 === intval( ini_get( 'display_errors' ) ),
			);
		}

		/**
	    * Name : get_theme_info
	    * Description: get active theme information
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl, by Powered by woocommerce
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_theme_info() {
			$active_theme = wp_get_theme();

			// Get parent theme info if this theme is a child theme, otherwise
			// pass empty info in the response.
			if ( is_child_theme() ) {
				$parent_theme      = wp_get_theme( $active_theme->Template );
				$parent_theme_info = array(
					'parent_name'           => $parent_theme->Name,
					'parent_version'        => $parent_theme->Version,
					'parent_version_latest' => $this->get_latest_theme_version( $parent_theme ),
					'parent_author_url'     => $parent_theme->{'Author URI'},
				);
			} else {
				$parent_theme_info = array( 'parent_name' => '', 'parent_version' => '', 'parent_version_latest' => '', 'parent_author_url' => '' );
			}

			/**
			 * Scan the theme directory for all WC templates to see if our theme
			 * overrides any of them.
			 */
			$override_files = array( array(
				'file'         => '',
				'version'      => '',
				'core_version' => '',
			) );

			$active_theme_info = array(
				'name'                    => $active_theme->Name,
				'version'                 => $active_theme->Version,
				'version_latest'          => $this->get_latest_theme_version( $active_theme ),
				'author_url'              => esc_url_raw( $active_theme->{'Author URI'} ),
				'is_child_theme'          => is_child_theme(),
				'overrides'               => $override_files
			);

			return array_merge( $active_theme_info, $parent_theme_info );
		}

		/**
	    * Name : get_database_info
	    * Description: get active theme information
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl, by Powered by woocommerce
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_database_info() {
			global $wpdb;

			// WC Core tables to check existence of
			$tables = array(
				'pu_base_sf_api_logs',
			);

			$table_exists = array();
			foreach ( $tables as $table ) {
				$table_exists[ $table ] = ( $wpdb->get_var( $wpdb->prepare( "SHOW TABLES LIKE %s;", $wpdb->prefix . $table ) ) === $wpdb->prefix . $table );
			}

			// Return all database info. Described by JSON Schema.
			return array(
				'database_prefix'        => $wpdb->prefix,
				'database_tables'        => $table_exists,
			);
		}

		/**
	    * Name : get_wordpress_environment
	    * Description: get wordpress environments
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_wordpress_environment() {
			$mappings = array(
				'home_url',
				'site_url',
				'wp_version',
				'wp_multisite',
				'wp_memory_limit',
				'wp_debug_mode',
				'wp_cron',
				'language'
			);
			$environment = $this->get_environment_info();
			$info = array();
			foreach ( $mappings as $data ) {
				$info[$data] = $environment[$data];
			}
			return $info;
		}

		/**
	    * Name : get_server_environment
	    * Description: get server environments
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl
	    * @param 
	    * @return
	    * @since    1.0.0
	    */
		public function get_server_environment() {
			$mappings = array(
				'server_info',
				'php_version',
				'php_post_max_size',
				'php_max_execution_time',
				'php_max_input_vars',
				'curl_version',
				'suhosin_installed',
				'max_upload_size',
				'mysql_version',
				'default_timezone',
				'fsockopen_or_curl_enabled',
				'soapclient_enabled',
				'domdocument_enabled',
				'gzip_enabled',
				'mbstring_enabled'
			);
			$environment = $this->get_environment_info();
			$info = array();
			foreach ( $mappings as $data ) {
				$info[$data] = $environment[$data];
			}
			return $info;
		}

		/**
	    * Name : get_latest_theme_version
	    * Description: get server environments
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl
	    * @param $theme, active theme information
	    * @return array, theme info
	    * @since    1.0.0
	    */
		private function get_latest_theme_version( $theme ) {
			include_once( ABSPATH . 'wp-admin/includes/theme.php' );

			$api = themes_api( 'theme_information', array(
				'slug'     => $theme->get_stylesheet(),
				'fields'   => array(
					'sections' => false,
					'tags'     => false,
				),
			) );

			$update_theme_version = 0;

			// Check .org for updates.
			if ( is_object( $api ) && ! is_wp_error( $api ) ) {
				$update_theme_version = $api->version;

			// Check WooThemes Theme Version.
			} elseif ( strstr( $theme->{'Author URI'}, 'woothemes' ) ) {
				$theme_dir = substr( strtolower( str_replace( ' ','', $theme->Name ) ), 0, 45 );

				if ( false === ( $theme_version_data = get_transient( $theme_dir . '_version_data' ) ) ) {
					$theme_changelog = wp_safe_remote_get( 'http://dzv365zjfbd8v.cloudfront.net/changelogs/' . $theme_dir . '/changelog.txt' );
					$cl_lines  = explode( "\n", wp_remote_retrieve_body( $theme_changelog ) );
					if ( ! empty( $cl_lines ) ) {
						foreach ( $cl_lines as $line_num => $cl_line ) {
							if ( preg_match( '/^[0-9]/', $cl_line ) ) {
								$theme_date         = str_replace( '.' , '-' , trim( substr( $cl_line , 0 , strpos( $cl_line , '-' ) ) ) );
								$theme_version      = preg_replace( '~[^0-9,.]~' , '' ,stristr( $cl_line , "version" ) );
								$theme_update       = trim( str_replace( "*" , "" , $cl_lines[ $line_num + 1 ] ) );
								$theme_version_data = array( 'date' => $theme_date , 'version' => $theme_version , 'update' => $theme_update , 'changelog' => $theme_changelog );
								set_transient( $theme_dir . '_version_data', $theme_version_data , DAY_IN_SECONDS );
								break;
							}
						}
					}
				}

				if ( ! empty( $theme_version_data['version'] ) ) {
					$update_theme_version = $theme_version_data['version'];
				}
			}

			return $update_theme_version;
		}

		/**
	    * Name : get_override_templates
	    * Description: get theme template override for pronto ultimate portal
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl
	    * @param
	    * @return array template name
	    * @since    1.0.0
	    */
		public function get_override_templates() {
			$tmp_dir = get_template_directory() . '/pu-templates';
			$override = array();
			if( file_exists( $tmp_dir ) ) {
				$templates = scandir($tmp_dir);
				foreach ( $templates as $file ) {
					if( !is_file( $file ) && !is_dir( $file ) ) {
						$temp_name = $this->get_template_name( $file );
						if( !empty( $temp_name ) ) {
							$override[] =  $this->get_template_name( $file );
						}
					}
				}
			}
			return $override;
		}

		/**
	    * Name : get_template_name
	    * Description: get template name
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl
	    * @param
	    * @return string template name
	    * @since    1.0.0
	    */
		public function get_template_name( $file ) {
			$temp_file = '';
			switch ( $file ) {
				case 'pronto-profile.php':
					$temp_file = 'Profile';
				break;
				case 'pronto-register.php':
					$temp_file = 'Register';
				break;
				case 'pronto-password-lost.php':
					$temp_file = 'Lost Password';
				break;
				case 'pronto-password-reset.php':
					$temp_file = 'Reset Password';
				break;
				case 'pronto-login.php':
					$temp_file = 'Login';
				default:
					$temp_file = null;
			}
			return $temp_file;
		}

		/**
	    * Name : let_to_num
	    * Description: convert size 
	    * LastUpdated : Aug 22, 2017
	    * @author   Danryl
	    * @param $size integer
	    * @return string size
	    * @since    1.0.0
	    */
		private function let_to_num( $size ) {
			$l   = substr( $size, -1 );
			$ret = substr( $size, 0, -1 );
			switch ( strtoupper( $l ) ) {
				case 'P':
				$ret *= 1024;
				case 'T':
				$ret *= 1024;
				case 'G':
				$ret *= 1024;
				case 'M':
				$ret *= 1024;
				case 'K':
				$ret *= 1024;
			}
			return $ret;
		}
	}
	
?>