<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://alphasys.com.au/
 * @since             1.2.0
 * @package           Pronto_Base
 *
 * @wordpress-plugin
 * Plugin Name:       Webforce Connect - Base
 * Plugin URI:        http://alphasys.com.au/
 * Description:       This plugin contains the core functionality of connecting Wordpress to Salesforce.
 * Version: 1.2.0
 * Author:            AlphaSys Pty. Ltd.
 * Author URI:        https://alphasys.com.au/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pu-base
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pronto-base-activator.php
 */
function activate_pronto_base() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pronto-base-activator.php';
	Pronto_Base_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pronto-base-deactivator.php
 */
function deactivate_pronto_base() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pronto-base-deactivator.php';
	Pronto_Base_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pronto_base' );
register_deactivation_hook( __FILE__, 'deactivate_pronto_base' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-pronto-base.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pronto_base() {

	$plugin = new Pronto_Base();
	$plugin->run();

}
run_pronto_base();
