<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/public
 * @author     Alphasys Pty. Ltd. <danryl@alphasys.com.au>
 */
class Pronto_Base_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		/**
	    * register bootstrap css
	    */
		wp_register_style( 
			'wfc-BootstrapCDN-css', 
			plugin_dir_url( __FILE__ ) . '../includes/bootstrap/css/bootstrap.css', 
			array(), $this->version, 
			'all' 
		);
		wp_register_style( 
			'wfc-BootstrapCDN-font-awesome-css', 
			'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', 
			array(), $this->version, 
			'all' 
		);

		/**
	    * register animate css
	    */
		wp_register_style( 
			'wfc-animate-css', 
			plugin_dir_url( __FILE__ ) . '../includes/animate/animate.css', 
			array(), $this->version, 
			'all' 
		);
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

	}

}
