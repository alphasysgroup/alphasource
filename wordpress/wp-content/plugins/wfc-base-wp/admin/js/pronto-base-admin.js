jQuery(document).ready(function($){

	var href_links = [
		'admin.php?page=pronto-base', 
		'admin.php?page=pronto-portal',
		'admin.php?page=pronto-wp-syncer',
		'admin.php?page=wfc-donation',
		'admin.php?page=pronto-fundraising',
		'admin.php?page=fundraising-p2p',
		'admin.php?page=pu-system-status',
		'admin.php?page=pronto-base-logs',
		'admin.php?page=donation-list-page',
		'edit.php?post_type=puf_campaign',
	];

	var class_link = [
		'wfc-portal-menu-btn-link',
		'WPPortal-back_button',
		'wfc-sync-menu-btn-link',
		'WPSyncer-back_button',
		'wfc-don-menu-btn-link',
		'wfc-puf-menu-btn-link',
	];

	var id_link = [
		'authorize_btn',
		'WPPortal-back_button',
		'WPPortal-back_button2',
		'WPPortal-back_button3',
		'WPSyncer-back_button',
		'WPSyncer-pwp_postfields-redirect',
		'WPSyncer-pwp_syncer-redirect',
	];



	$(document).on('click', 'a , button', function() {
		var elem = $(this);
		if ( $.inArray( elem.attr('href'), href_links) != -1) {
			ToggleWFCTinter();
		}
        
        if ( $.inArray( elem.attr('class'), class_link) != -1) {
			ToggleWFCTinter();
		}

		if ( $.inArray( elem.attr('id'), id_link) != -1) {
			ToggleWFCTinter();
		}
    });

	SaiyanToolkit.spinner.show(null, 'wfc');
	$('body .saiyan-spinner').css('pointer-events', 'none');
	$('body .saiyan-spinner img').css('opacity', '0.0');
	function ToggleWFCTinter(){
		$(window).bind('beforeunload', function() {
			$('body .saiyan-spinner img').animate({
				'opacity': '1.0'
			}, 300, 'linear', function() {
				$('body .saiyan-spinner').css('pointer-events', 'all');
			});
			SaiyanToolkit.tinter.show('#000000');	
		})
	}
});
