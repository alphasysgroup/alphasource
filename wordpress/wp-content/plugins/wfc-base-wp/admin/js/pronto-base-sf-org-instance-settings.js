jQuery(document).ready(function($){
    console.log(wfc_base_sf_org_instance_settings_param);

    /**
     * Save donation settings
     */
    $('#wfc_base_savesalesforceinstance_btn').on('click', function(){
        var savebtnelem = $(this);

        savebtnelem.attr('disabled','disabled');
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving</div>',null,'info');
        $.ajax({
            url: wfc_base_sf_org_instance_settings_param.url,
            type:'POST',
            dataType:'json',
            data:{
                'action': 'wfc_base_savesforginstancesettings_action',
                nonce: wfc_base_sf_org_instance_settings_param.nonce,
                data : $('#wfc_base_salesforceinstance_settings').serializeArray(),
            },
            success:function(response){
                console.log(response);
                savebtnelem.removeAttr('disabled');
                var msg = response.msg;
                SaiyantistCore.snackbar.hide();
                if(response.status == 'Success'){
                    msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                    SaiyantistCore.snackbar.show(msg,3000,'success');
                    setTimeout(function(){
                        window.location.replace(response.settings_url);
                    }, 3000);
                }else{
                    msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                    SaiyantistCore.snackbar.show(msg,3000,'error');
                }
            },
            error: function(response) {
                console.log(response);
                savebtnelem.removeAttr('disabled');
                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
            }
        });
    });

    /**
     * Redirect back to instance list
     */
    $('#wfc_base_authorize_btn').on('click', function(){
        var savebtnelem = $(this);

        var form = $('#wfc_base_salesforceinstance_settings');

        form.submit();

        /*
        savebtnelem.attr('disabled','disabled');
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving and Authenticating</div>',null,'info');
        $.ajax({
            url: wfc_base_sf_org_instance_settings_param.url,
            type:'POST',
            dataType:'json',
            data:{
                'action': 'wfc_sf_org_instance_authorize',
                'authenticate': 'true',
                nonce: wfc_base_sf_org_instance_settings_param.nonce,
                data : form.serializeArray()
                *
                {
                    name: form.find('#wfc_base_salesforceinstance_name').val(),
                    slug: form.find('#wfc_base_salesforceinstance_slug').val(),
                    client_id: form.find('#wfc_base_clientid').val(),
                    client_secret: form.find('#wfc_base_clientsecret').val(),
                    callback_url: form.find('#wfc_base_callbackurl').val(),
                    instance_type: form.find('#wfc_base_instancetype').val(),
                    set_instance_default: form.find('#wfc_base_setinstancedefault').prop('checked')
                }
                *
            },
            /* success:function(response){
                console.log(response);
                */
                /*
                savebtnelem.removeAttr('disabled');
                var msg = response.msg;
                SaiyantistCore.snackbar.hide();
                if(response.status == 'Success'){
                    msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                    SaiyantistCore.snackbar.show(msg,3000,'success');
                    setTimeout(function(){
                        //window.location.replace(response.settings_url);
                    }, 3000);
                }else{
                    msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                    SaiyantistCore.snackbar.show(msg,3000,'error');
                }
                *
            },
            error: function(response) {
                console.log(response);
                savebtnelem.removeAttr('disabled');
                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
            }
        });
        */
    });

    $('#wfc_base_revoke_btn').on('click', function() {
        if (confirm('Are you sure you want to revoke this Salesforce Instance?')) {

            var sfInstanceID = $('input[name=\'wfc_base_salesforceinstance_slug\']').val();

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'wfc_sf_org_instance_revoke',
                    sf_instance_id: sfInstanceID
                },
                success: function(response) {
                    console.log(response);

                    if (response.success) {
                        window.location.reload();
                    }
                }
            })
        }
    });

    /**
     * Redirect back to instance list
     */
    $('#wfc_base_backsalesforceinstancelist_btn').on('click', function(){
        window.location.replace(wfc_base_sf_org_instance_settings_param.instance_list_url);
    });
});