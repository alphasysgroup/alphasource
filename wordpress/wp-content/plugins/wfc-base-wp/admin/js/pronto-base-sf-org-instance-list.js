(function($) {

    var Pronto_Base_SF_Org_Instance_List = {

        options: {},

        addNewURL: '',

        init: function(options) {
            this.options = options;
            this.bind();

            this.fetchList();
        },

        bind: function() {
            var self = Pronto_Base_SF_Org_Instance_List;

            this.options.addButton.on('click', function() {self.modalOpen()});
            this.options.modal.closeButton.on('click', function() {self.modalClose()});
            this.options.modal.container.on('click', function() {self.modalClose()});
            this.options.modal.container.find('.wfc-base-modal').on('click', function(e) {
                e.stopPropagation();
            });

            this.options.modal.addButton.on('click', function() {self.addNew()});
        },

        fetchList: function() {

            var self = Pronto_Base_SF_Org_Instance_List;

            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    action: 'wfc_fetch_sf_org_instance_list'
                },
                success: function(response) {
                    //console.log(response);

                    self.buildList(response);
                }
            })
        },

        buildList: function(response) {

            var _default = response.default_instance;
            var list = response.instances;
            var default_ribbon_img = response.default_ribbon_img;

            this.addNewURL = response.add_new_instance_url;

            console.log(list);
            if (list.length > 0){

                this.options.list.empty();

                for (var key in list) {
                    var item = list[key];
                    var isDefault = item.wfc_base_salesforceinstance_slug === _default;

                    //console.log(item);

                    var itemMarkup =
                        $('<li class="list-group-item ' + item.wfc_base_instancetype + '"></li>').append(
                            (isDefault ? $('<img src="' + default_ribbon_img + '" />') : ''),
                            $('<div class="row"></div>').append(
                                $('<div class="wfc-don-list-item col-3"></div>').append(
                                    (
                                        isDefault
                                            ? $('<strong>' + item.wfc_base_salesforceinstance_name + '</strong>')
                                            : $('<span>' + item.wfc_base_salesforceinstance_name + '</span>')
                                    )
                                ),
                                $('<div class="wfc-don-list-item col-3"></div>').append(
                                    (
                                        isDefault
                                            ? $('<strong>' + item.wfc_base_salesforceinstance_slug + '</strong>')
                                            : $('<span>' + item.wfc_base_salesforceinstance_slug + '</span>')
                                    )
                                ),
                                $('<div class="wfc-don-list-item col-3"></div>').append(
                                    (
                                        typeof item.tokens !== 'undefined'
                                            ? isDefault
                                            ? $('<strong>' + item.tokens.instance_url + '</strong>')
                                            : $('<span>' + item.tokens.instance_url + '</span>')
                                            : ''
                                    )
                                ),
                                $('<div class="col-1"></div>').append(
                                    (isDefault ? $('<strong></strong>') : $('<span></span>')).append(
                                        typeof item.instances_connection_status !== 'undefined' && item.instances_connection_status.status === 'success'
                                            ? $('<i class="fa fa-plug" style="font-size: 36px; color: green;"></i>')
                                            : $('<i class="fa fa-plug" style="font-size: 36px; color: red;"></i>')
                                    )
                                ),
                                $('<div class="col-1"></div>').append(
                                    $('<span></span>').append(
                                        $('<a href="' + item.wfc_base_salesforceinsntace_settings_url + '"></a>').append(
                                            $('<i class="fa fa-gear" style="font-size: 36px;"></i>')
                                        )
                                    )
                                )
                            )
                        );

                    this.options.list.append(itemMarkup);
                }

                this.options.list.easyPaginate({
                    paginateElement: 'li',
                    elementsPerPage: 10,
                    effect: 'climb'
                });
            } else{
                $('#pronto-base-sf-org-instance-list').empty();
                $('#pronto-base-sf-org-instance-list').append('<li class="list-group-item"><div>There are no Saleforce Instances created yet.</div></li>');
            }


        },

        addNew: function() {
            var createbtnelem = $('#wfc-base-create-sfinstance-btn');

            // window.location.href = this.addNewURL;
            var salesforceInstanceName = $('input[name=\'wfc-base-salesforce-instance-name\']').val();

            createbtnelem.attr('disabled','disabled');
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Creating Salesforce Instance</div>',null,'info');

            $.ajax({
                url: wfc_base_sf_org_instance_list_param.url,
                type:'POST',
                dataType:'json',
                data:{
                    'action': 'wfc_base_createsfinstance_action',
                    nonce: wfc_base_sf_org_instance_list_param.nonce,
                    'instance_name' : salesforceInstanceName,
                },
                success:function(response){
                    console.log(response);

                    var msg = response.msg;
                    SaiyantistCore.snackbar.hide();
                    if(response.status == 'Success'){
                        msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                        SaiyantistCore.snackbar.show(msg,3000,'success');
                        setTimeout(function(){
                            window.location.replace(response.settings_url);
                        }, 3000);
                    }else if(response.status == 'Failed') {
                        createbtnelem.removeAttr('disabled');
                        msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                        SaiyantistCore.snackbar.show(msg,3000,'error');
                    }else{
                        createbtnelem.removeAttr('disabled');
                        msg = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + msg + '</div>';
                        SaiyantistCore.snackbar.show(msg,3000,'error');
                    }
                },
                error: function(response) {
                    console.log(response);
                    createbtnelem.removeAttr('disabled');
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
                }
            });
        },

        modalOpen: function() {
            this.options.modal.container.addClass('show-modal');
        },

        modalClose: function() {
            this.options.modal.container.removeClass('show-modal');
        }
    };

    $(document).ready(function() {
        Pronto_Base_SF_Org_Instance_List.init({
            list: $('#pronto-base-sf-org-instance-list'),
            addButton: $('#wfc-base-add-salesforce-btn'),
            modal: {
                container: $('#wfc-base-modal-container'),
                closeButton: $('#wfc-base-modal-container button[data-action=\'close\']'),
                addButton: $('#wfc-base-modal-container button[data-action=\'add\']')
            }
        })
    })

})(jQuery);