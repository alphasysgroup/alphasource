<?php
if (isset($_GET['reset']) && $_GET['reset'] == 1) {
	delete_option('pub_oauth_credential');
}

$settings = get_option( 'pub_oauth_credential', 0 );

$connect = isset( $settings['connect'] ) ? $settings['connect'] : 'sandbox';
$messages = get_option( "{$connect}_authrorization_message", 0 );
$tokens = get_option( "pronto_ultimate_{$connect}_tokens", 0 );

$sandbox_client_id = (isset( $settings['sandbox_client_id'] ) && !empty( $settings['sandbox_client_id'] ) ) ? $settings['sandbox_client_id'] : '';
$sandbox_client_secret = (isset( $settings['sandbox_client_secret'] ) && !empty( $settings['sandbox_client_secret'] ) ) ? $settings['sandbox_client_secret'] : '';

$sandbox_callback_url = 
( isset( $settings['sandbox_callback_url'] ) && !empty( $settings['sandbox_callback_url'] ) ) ? $settings['sandbox_callback_url'] : get_site_url() . '/wp-admin/admin-post.php?action=authorization';

$live_client_id  = (isset( $settings['live_client_id'] ) && !empty( $settings['live_client_id'] ) ) ? $settings['live_client_id'] : '';
$live_client_secret  = (isset( $settings['live_client_secret'] ) && !empty( $settings['live_client_secret'] ) ) ? $settings['live_client_secret'] : '';

$live_callback_url = 
( isset( $settings['live_callback_url'] ) && !empty( $settings['live_callback_url'] ) ) ? $settings['live_callback_url'] : get_site_url() . '/wp-admin/admin-post.php?action=authorization';

function pronto_base_sf_org_instance_list_markup() {
    ob_start();
    include 'pronto-base-sf-org-instance-list.php';
    return ob_get_clean();
}

$authorization_tab = array(
    'action' => array(
		'type' => 'input',
        'settings' => array(
        	'id' => 'action',
        	'name' => 'action',
            'type' => 'hidden',
            'value' => 'pub_authorize',
            'label' => array(
            	'hidden' => true
            )
        )
    ),
	'connect' => array(
		'type' => 'radio',
        'settings' => array(
        	'id' => 'connect',
	        'name' => 'connect',
	        'gridclass' => 'col-md-12 col-lg-12',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Salesforce Organization',
		        'position'  => 'top',
	        ),
	        'radios' => array(
	        	array(
	        		'title' => 'Sandbox',
			        'value' => 'sandbox'
		        ),
		        array(
			        'title' => 'Production',
			        'value' => 'production'
		        )
	        ),
	        'checked' => $connect
        )
    ),	
    'sandbox_client_id' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'sandbox_client_id',
	        'name' => 'sandbox_client_id',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Client ID',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $sandbox_client_id,
	        'description' => 'Your sandbox salesforce consumer key.',
	        'dependency' => array(
	        	'id' => 'connect',
	        	'trigger_value' => 'sandbox',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'sandbox_client_secret' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'sandbox_client_secret',
	        'name' => 'sandbox_client_secret',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Client Secret',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $sandbox_client_secret,
	        'description' => 'Your sandbox salesforce consumer key.',
	        'dependency' => array(
	        	'id' => 'connect',
	        	'trigger_value' => 'sandbox',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'sandbox_callback_url' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'sandbox_callback_url',
	        'name' => 'sandbox_callback_url',
	        'gridclass' => 'col-md-12 col-lg-12',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Redirect URI',
		        'position'  => 'top',
	        ),
	        'readonly' => true,
	        'value' => $sandbox_callback_url,
	        'description' => 'Callback URL needed for Salesforce Sandbox Connected App.',
	        'dependency' => array(
	        	'id' => 'connect',
	        	'trigger_value' => 'sandbox',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'live_client_id' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'live_client_id',
	        'name' => 'live_client_id',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Client ID',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $live_client_id,
	        'description' => 'Your live salesforce consumer client id.',
	        'dependency' => array(
	        	'id' => 'connect',
	        	'trigger_value' => 'production',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'live_client_secret' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'live_client_secret',
	        'name' => 'live_client_secret',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Client Secret',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $live_client_secret,
	        'description' => 'Your live salesforce consumer secret.',
	        'dependency' => array(
	        	'id' => 'connect',
	        	'trigger_value' => 'production',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'live_callback_url' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'live_callback_url',
	        'name' => 'live_callback_url',
	        'gridclass' => 'col-md-12 col-lg-12',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Redirect URI',
		        'position'  => 'top',
	        ),
	        'readonly' => true,
	        'value' => $live_callback_url,
	        'description' => 'Callback URL needed for Salesforce Production Connected App.',
	        'dependency' => array(
	        	'id' => 'connect',
	        	'trigger_value' => 'production',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'authorize_btn' => array(
	  	'type' => 'button',
	  	'settings' => array(
	        'id'    => 'authorize_btn',
	        'class' => 'authorize_btn',
	        'name'  => 'authorize_btn',
	        'type'  => 'button',
	        'style' => 'float:left;margin-top:20px;',
	        'gridclass'=> 'col-md-12 col-lg-12',
	        'label'     => array(
		        'hidden'    => true,
		        'value'     => '',
		        'position'  => 'top',
		    ),
		    'content' => 'Authorize'
	    )
	),	
);
$authorization_form = array(
	'pronto-ultimate-base' => array(
		'type' => 'settingsform',
		'settings' => array(
			'id'        => 'pronto-ultimate-base',
			'class'     => '',
			'name'      => 'pronto-ultimate-base',
			'style'     => 'width: 100%;',
			'title'     => '',
			'action'    => get_site_url() . '/wp-admin/admin-post.php',
			'enctype'   => 'application/x-www-form-urlencoded',
			'method'    => 'post',
			'wp_option' => array(
				'enable'     => false,
				'slug_name'  => '',
			),
			'submit_id' => '',
		),
		'children' => $authorization_tab,
    ),
);
$advance_settings = get_option( 'pub_advance_settings', 0 );
$logging = isset($advance_settings['logging']) ? $advance_settings['logging'] : 'false';
$logging = ($logging == 'true') ? 'on' : 'off';
$cron_schedule = isset( $advance_settings['cron_schedule'] ) ? $advance_settings['cron_schedule'] : 'daily';
$logs_limit = isset( $advance_settings['logs_limit'] ) ? $advance_settings['logs_limit'] : '500';
$logs_cleaner = isset( $advance_settings['logs_cleaner'] ) ? $advance_settings['logs_cleaner'] : 'daily';

$advance_settings_children = array(
	'logging' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'logging',
			'class'    => 'logging',
			'name'     => 'logging',
            'switch'   => $logging,
            'gridclass' => 'col-md-12 col-lg-12',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Enable logging',
				'position'  => 'top',
			),
			'description' => 'If enable, it will log all salesforce api requests.'
		)
	),
	'cron_schedule' => array(
		'type' => 'select',
        'settings' => array(
        	'id' => 'cron_schedule',
	        'name' => 'cron_schedule',
	        'gridclass' => 'col-md-12 col-lg-12',
	        'style'   => 'width:100%;',
	        'readonly' => false,
	        'options' => array(
	        	array(
	        		'title' => 'Every Hour',
			        'value' => 'hourly'
		        ),
		        array(
			        'title' => 'Every Day',
			        'value' => 'daily'
		        ),
		        array(
			        'title' => 'Every Week',
			        'value' => 'weekly'
		        ),
		        array(
			        'title' => 'Every Month',
			        'value' => 'monthly'
		        ),
	        ),
	        'label'     => array(
				'hidden'    => false,
				'value'     => 'Cron Schedules',
				'position'  => 'top',
			),
	        'description' => 'Schedules when to run the Salesforce Syncer.',
	        'selected' => $cron_schedule,
        )
    ),	
    'logs_limit' => array(
		'type' => 'select',
        'settings' => array(
        	'id' => 'logs_limit',
	        'name' => 'logs_limit',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'style'   => 'width:100%;',
	        'readonly' => false,
	        'options' => array(
	        	array(
	        		'title' => '100 Records',
			        'value' => '100'
		        ),
		        array(
	        		'title' => '500 Records',
			        'value' => '500'
		        ),
		        array(
	        		'title' => '1000 Records',
			        'value' => '1000'
		        ),
		        array(
	        		'title' => '1500 Records',
			        'value' => '1500'
		        ),
	        ),
	        'label'     => array(
				'hidden'    => false,
				'value'     => 'Logs Limit',
				'position'  => 'top',
			),
	        'description' => 'Set a total logs stored to database.',
	        'selected' => $logs_limit,
        )
    ),
    'logs_cleaner' => array(
		'type' => 'select',
        'settings' => array(
        	'id' => 'logs_cleaner',
	        'name' => 'logs_cleaner',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'style'   => 'width:100%;',
	        'readonly' => false,
	        'options' => array(
	        	array(
	        		'title' => 'Every Hour',
			        'value' => 'hourly'
		        ),
		        array(
			        'title' => 'Every Day',
			        'value' => 'daily'
		        ),
		        array(
			        'title' => 'Every Week',
			        'value' => 'weekly'
		        ),
		        array(
			        'title' => 'Every Month',
			        'value' => 'monthly'
		        ),
	        ),
	        'label'     => array(
				'hidden'    => false,
				'value'     => 'Logs Cleaner Cron Schedules',
				'position'  => 'top',
			),
	        'description' => 'Schedules when to run the Logs Cleaner.',
	        'selected' => $logs_cleaner,
        )
    ),
	'advance_btn' => array(
	  	'type' => 'button',
	  	'settings' => array(
	        'id'    => 'advance_btn',
	        'class' => 'advance_btn',
	        'name'  => 'advance_btn',
	        'type'  => 'button',
	        'style' => 'float:left;margin-top:20px;',
	        'gridclass'=> 'col-12',
	        'label'     => array(
		        'hidden'    => true,
		        'value'     => '',
		        'position'  => 'top',
		    ),
		    'content' => 'SAVE SETTINGS'
	    )
	),	
);
$advance_settings_form = array(
	'pub_advance_settings' => array(
		'type' => 'settingsform',
		'settings' => array(
			'id'        => 'pub_advance_settings',
			'class'     => '',
			'action'    => '',
			'name'      => 'pub_advance_settings',
			'style'     => 'width: 100%;',
			'title'     => '',
			'action'    => '',
			'method'    => 'post',
			'wp_option' => array(
				'enable'     => false,
				'slug_name'  => 'pub_advance_settings',
			),
			'submit_id' => 'advance_btn',
		),
		'children' => $advance_settings_children,
    ),
);
$accordion = array(
	'wfc_base' => array(
	    'type' => 'accordion',
	    'settings' => array(
	        'id' => 'wfc_base',
	        // 'gridclass' => 'col-md-12 col-lg-12',
	        'gridclass' => '',
	    ),
	    'sections' => array(
	        array(
	            'title' => 'Authorization',
	            'description' => 'Authorize your site to Salesforce.',
	            // 'content' => $authorization_form,
                'content' => array(
                    'my_html' => array(
                        'type' => 'html',
                        'settings' => array(
                            'id'    => 'my_html',
                            'class' => 'my_html',
                            'name'  => 'my_html',
                            'gridclass '=> 'col-md-6',
                            'label'     => array(
                                'hidden'    => true,
                            )
                        ),
                        'content' => call_user_func('pronto_base_sf_org_instance_list_markup')
                    ),
                )
	        ),
	        array(
	            'title' => 'Advanced Settings',
	            'description' => 'Developer tools and general settings for donation.',
	            'content' => $advance_settings_form,
	        ),
	    )
	),
);
?>
<div class="wfc_base_settings_main_cont">
	<h2>Webforce Connect - Base</h2>
	<?php Saiyan::tist()->render_module_mapping($accordion); ?>	
</div>
<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<style type="text/css">
	#action-container{
		margin-bottom: 0px;
	}

	.wfc_base_settings_main_cont{
		margin-right: 20px;
	}

    .wfc_base_settings_main_cont h2{
        padding: 25px 15px;
        color: #ffffff;
        background-color: #1497c1;
        margin-bottom: 0;
    }

	.pu-base-wrap {
		background: #ffffff;
		padding: 15px;
		border-radius: 5px;
	}

	input, textarea, select {
		background: #ffffff !important;
	}

	.pu-base-wrap .hidden{
		display: none;
	}

	.org-status-error {
		color: #dc3232;
		font-weight: bold;
	}

	.org-status-success {
		color: #46b450;
		font-weight: bold;
	}

	.loader-wrapper {
	   width: 50px;
	   height: 50px;
	   position: absolute;
	}

	.loader1, .loader2 {
	    width: 100%;
	    height: 100%;
	    border-radius: 50%;
	    background-color: #48c569;
	    opacity: 0.6;
	    position: absolute;
	    top: 0;
	    left: 0;
	    -webkit-animation: tm-bounce 2.0s infinite ease-in-out !important;
	    animation: tm-bounce 2.0s infinite ease-in-out !important;
	}

	.loader2 {
	  -webkit-animation-delay: -1.0s !important;
	  animation-delay: -1.0s !important;
	}

    @-webkit-keyframes tm-bounce {
  	0%, 100% {
    -webkit-transform: scale(0);
    transform: scale(0); }
    50% {
      -webkit-transform: scale(1);
      transform: scale(1); } }

      @keyframes tm-bounce {
        0%, 100% {
          -webkit-transform: scale(0);
          transform: scale(0); }
          50% {
            -webkit-transform: scale(1);
            transform: scale(1); } }

	pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; max-width: 768px; overflow: auto;}
	.string { color: green; }
	.number { color: darkorange; }
	.boolean { color: blue; }
	.null { color: magenta; }
	.key { color: red; }
</style>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		var connect = '<?php echo $connect ?>';
		var messages = $.parseJSON( '<?php echo ( $messages != 0 ) ? json_encode( $messages ) : 0; ?>' );

		// change event sandbox or live authorize
		$('input[type="radio"][name="connect"]').on( 'change', function() {

			var radio_connect = $(this);
		 	if( radio_connect.val() == 'sandbox' ) {
		 		$('.elem-sandbox').show();
		 		$('.elem-live').hide();
		 	} else {
		 		$('.elem-sandbox').hide();
		 		$('.elem-live').show();
		 	}

		 	setTimeout(function() {
		 		$('.org-status').remove();
		 		$('.pub-messages').remove();

			 	$.ajax({
			 		type:"POST",
			 		data: {
			 			action: "pub_connect_to",
						formdata: $('#pronto-ultimate-base').serializeArray()
			 		},
			 		url: ajaxurl,
			 		success: function( response ) {

			 			if( response.messages == 0 ) {
			 				var statusClass = ( response.messages.status == 'success' ) ? 'updated' : 'error';
			 					$('.wfc_base_settings_main_cont').before( "<div class='error pub-messages'><p><strong>STATUS</strong>: Not Authenticated with Salesforce.</p></div>" );
			 			} else {
			 				if( response.messages.result ) {
			 					var data = '';
			 					$.each( response.messages.result, function(k, v) {
			 						data += "<p><strong>"+k+"</strong>: "+v+".</p>";
			 					});
			 					var statusClass = ( response.messages.status == 'success' ) ? 'updated' : 'error';
			 					$('.wfc_base_settings_main_cont').before( "<div class='"+statusClass+" pub-messages'>"+data+"</div>" );
			 				}
			 			}

			 			var btnText = ( response.messages.status && response.messages.status == 'success' ) ? localize_text.btn_txt2 : localize_text.btn_txt;
			 			$('#authorize_btn').text( btnText );
			 		}
			 	});
		 	}, 0);
		});

		// add test connection button
		$('input[type="radio"][name="connect"]').each(function() {
			$(this).after( "<a id='"+$(this).val()+"' class='test-connection'>"+localize_text.view_details+"</a>" );
		});

		$('.test-connection').css({
			'margin-left': '10px',
			'cursor' : 'pointer',
			'font-weight': 'bold',
			'text-decoration': 'underline',
			'font-size': '10px'
		}).on( 'click', function(){
			var connection = $(this);
			connection.after( '<span class="spinner"></span>' );
			$('.spinner').css({
				'visibility' : 'visible',
				'position': 'absolute',
				'background-color': '#ffffff'
			});

			$('#connect-messages-status').remove();
			$('.connection-limit-fields').remove();
			
			$.ajax({
		 		type:"POST",
		 		data: { 
		 			action: "test_sf_connection",
		 			testconnection: connection.attr('id')
		 		},
		 		url: ajaxurl,
		 		success: function( response ) {
		 			setTimeout(function(){
						$('#TB_ajaxContent').css({
							'width' : ''
						});
					}, 0);

		 			if( response ) {
		 				$('.spinner').remove();
		 				$('.revoke-connection').remove();

		 				var connectStats = null;
		 				var connectColor = null;

		 				if( response.status == true ) {
		 					connectStats = 'Connected';
		 					connectColor = 'org-status-success';
		 				} else {
		 					connectStats = 'Not Connected';
		 					connectColor = 'org-status-error';
		 				}

		 				$('#connection-status').text( connectStats ).removeClass().addClass(connectColor);
		 				$('#wfc_base_auth_thickbox').trigger('click');
		 				if( connectStats == 'Connected' ) {

		 					$('#revoke-btn').after( '<a id="'+connection.attr('id')+'" class="revoke-connection button button-primary" style="margin-left: 8px;">Revoke</a>' );

		 					// revoking connection
		 					$('.revoke-connection').on('click', function() {

		 						$('.revoke-status').remove();
		 						$('.connection-limit-fields').remove();

		 						var btn = $(this);
		 						btn.after( '<span class="spinner"></span>' );
		 						$('.spinner').css({
		 							'visibility' : 'visible',
		 							'position': 'absolute',
		 							'background-color': '#ffffff'
		 						});

		 						$.ajax({
		 							type:"POST",
		 							data: { 
		 								action: "revoke_connection",
		 								revoke: connection.attr('id')
		 							},
		 							url: ajaxurl,
		 							success: function( response ) {
		 								if( response ) {
		 									$('.spinner').remove();
		 									if( response.status == true ) {
		 										window.alert('Token is successfully revoked.');
		 										$('#connection-status').text( 'Not Connected' ).removeClass().addClass('org-status-error');

		 										$('#connect-messages-status').remove();
		 										$('#connection-messages').append( "<div style='margin-left: 0px;' id='connect-messages-status' class='error'><p><strong>STATUS</strong>: Token is revoked.</p></div>" );
		 										btn.remove();
		 										location.reload();
		 									} else {
		 										window.alert('Failed to revoked token.');
		 									}
		 								}
		 							}
		 						});
		 					});
		 				}


		 				// instance url 
		 				var instance_url = ( response.tokens.instance_url ) ? response.tokens.instance_url : 'NONE';
		 				$('#connection-instance-url').text( instance_url );

		 				// access token 
		 				var access_token = ( response.tokens.access_token ) ? 'STORED' : 'NONE';
		 				$('#connection-access-token').text( access_token );

		 				// access token 
		 				var refresh_token = ( response.tokens.refresh_token ) ? 'STORED' : 'NONE';
		 				$('#connection-refresh-token').text( refresh_token );

		 				var messages = ( response.messages.status ) ? response.messages.result : 'Not Authenticated with Salesforce.';

		 				if( typeof messages == 'string' ) {
		 					$('#connection-messages').append( "<div style='margin-left: 0px;' id='connect-messages-status' class='error'>"+messages+"</div>" );
		 				} else {
		 					var messages_data = '';
		 					var statusColor = ( response.messages.status && response.messages.status == 'success' ) ? 'updated' : 'error' ;
		 					$.each( response.messages.result, function( k, v ){
		 						messages_data += '<p><strong>'+ k.toUpperCase() +'</strong>: '+v+'.</p>';
		 					});	 					
		 					$('#connection-messages').append( "<div style='margin-left: 0px;' id='connect-messages-status' class='"+statusColor+"'>"+messages_data+"</div>" );
		 				}


		 				if( response.limits ) {

		 					var textareas = [];
		 					$.each( response.limits, function( k, v ){
		 						$('#connection-list tbody').append('<tr valign="top" class="connection-limit-fields"><td>'+k+'</td><td><textarea id="'+k+'">'+JSON.stringify(v)+'</textarea></td></tr>');
		 						textareas.push(k);
		 					});

		 					beautifyJSONTextarea( textareas );
		 				}
		 			}
		 		}
		 	});

			function beautifyJSONTextarea( elems ) {
				$.each(elems, function(v, k){
					var obj = $( '#' + k );
					if( IsJsonString( obj.val() ) ) {
						var str = syntaxHighlight( $.parseJSON( obj.val() ) );
						$( '#' + k ).after("<pre>"+str+"</pre>");
						$( '#' + k ).hide();
					}
				});
			}

			function syntaxHighlight(json) {
				if (typeof json != 'string') {
					json = JSON.stringify(json, undefined, 2);
				}
				json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
					var cls = 'number';
					if (/^"/.test(match)) {
						if (/:$/.test(match)) {
							cls = 'key';
						} else {
							cls = 'string';
						}
					} else if (/true|false/.test(match)) {
						cls = 'boolean';
					} else if (/null/.test(match)) {
						cls = 'null';
					}
					return '<span class="' + cls + '">' + match + '</span>';
				});
			}

			function IsJsonString(str) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}
				return true;
			}
		});

		// trigger first click
		var trigger = ( connect == 'sandbox' ) ? 'first' : 'last';
		$('input[type="radio"][name="connect"]:'+trigger+'').trigger("change");

		// submit 
		$('#authorize_btn').on('click', function(){
			$('#pronto-ultimate-base').submit();
		});
	});
</script>

<?php add_thickbox(); ?>
<div id="connection-popup" style="display:none;">
	<table id="connection-list" class="wp-list-table widefat plugins" style="margin-top:10px; border:none;">
		<tbody>
			<tr valign="top">
				<td><?php echo __('Salesforce Connection Status', 'pu-base'); ?></td>
				<td><label id="connection-status"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Revoke Connection', 'pu-base') ?></td>
				<td><label id="revoke-btn"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Instance URL', 'pu-base'); ?></td>
				<td><label id="connection-instance-url"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Access Token', 'pu-base'); ?></td>
				<td><label id="connection-access-token"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Refresh Token', 'pu-base'); ?></td>
				<td><label id="connection-refresh-token"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Messages', 'pu-base'); ?></td>
				<td><label id="connection-messages"></label></td>
			</tr>
		</tbody>
	</table>
</div>
<a href="#TB_inline?width=400&height=500&inlineId=connection-popup" id="wfc_base_auth_thickbox" class="thickbox" style="display: none;"></a>	
