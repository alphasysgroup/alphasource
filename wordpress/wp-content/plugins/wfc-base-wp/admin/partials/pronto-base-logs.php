
<div class="wrap">

	<!-- LIST OF LOGS -->
	<?php if( !isset( $_GET['action'] ) ): ?>

		<style type="text/css">
			.column-ID {width: 5%}
			.column-date {width: 6%}
			.column-title {width: 10%}
			.column-user {width: 15%}
			.column-limit {width: 6%}
			.column-method {width: 8%}
			.column-status {width: 6% }

			.sync-status-success {
				border-right: 3px solid #46b450;
    			height: 40px;
			}

			.sync-status-failed {
				border-right: 3px solid #dc3232;
    			height: 40px;
			}

/*			#btn-delete-all {
				position: absolute;
			    top: 15px;
			    left: 50px;
			}*/

		</style>

	    <h1 class="wp-heading-inline"><?php esc_html_e("Logs", "pu-base"); ?></h1>

		<a href="<?php echo admin_url( 'admin.php?page=pronto-base-logs&action=deleteall' ); ?>" class="page-title-action" id="btn-delete-all"><?php echo esc_html_e( 'Delete All', 'pu-base' ); ?></a>
 
	    <?php /**echo $table->search_form( 'Search', 'search_id' ) */ ?>

	    <?php echo $table->prepare_items(); ?>

	    <?php echo $table->display(); ?>

	<!-- LOGS FULL DETAILS -->
	<?php elseif( isset( $_GET['action'] ) && $_GET['action'] == 'details' ): ?>

		<h2><?php esc_html_e("Sync details", "pu-base"); ?></h2>

		<style type="text/css">
			.pu-details-wrap {
				background: #ffffff;
				padding: 15px;
				border-radius: 5px;
			}

			input, textarea, select {
				background: #ffffff;
			}

			pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; max-width: 768px; overflow: auto;}
			.string { color: green; }
			.number { color: darkorange; }
			.boolean { color: blue; }
			.null { color: magenta; }
			.key { color: red; }

            body.webforce-connect_page_pronto-base-logs h2 {
                padding: 25px 15px !important;
                color: #ffffff;
                background-color: #1497c1;
                margin-bottom: 0;
            }

            .pu-details-wrap {
                padding: 30px;
                box-shadow: 0 2px 10px 0 rgba(0, 0, 0, 0.1);
                overflow: hidden;
            }

		</style>

		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$('.saiyan-module input, .saiyan-module textarea').attr('disabled', 'disabled').css({
					'background': '#f1f1f1',
				});

				// beautify textarea
				beautifyJSONTextarea( ['description', 'query', 'data'] );
				function beautifyJSONTextarea( elems ) {
					$.each(elems, function(v, k){
						var obj = $( '#' + k );
						if( IsJsonString( obj.val() ) ) {
							var str = syntaxHighlight( $.parseJSON( obj.val() ) );
							$( '#' + k ).after("<pre>"+str+"</pre>");
							$( '#' + k ).hide();
						}
					});
				}

				function syntaxHighlight(json) {
				    if (typeof json != 'string') {
				         json = JSON.stringify(json, undefined, 2);
				    }
				    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
				        var cls = 'number';
				        if (/^"/.test(match)) {
				            if (/:$/.test(match)) {
				                cls = 'key';
				            } else {
				                cls = 'string';
				            }
				        } else if (/true|false/.test(match)) {
				            cls = 'boolean';
				        } else if (/null/.test(match)) {
				            cls = 'null';
				        }
				        return '<span class="' + cls + '">' + match + '</span>';
				    });
				}

				function IsJsonString(str) {
				    try {
				        JSON.parse(str);
				    } catch (e) {
				        return false;
				    }
				    return true;
				}
			});
		</script>

		<?php

			// grab id
			$log_id = isset( $_GET['ID'] ) ? $_GET['ID'] : '';

			// get logs
			$current_log = $sf_api->pub_get_logs( array(
				'publogid' => array(
					'value' => $log_id, 
					'operator' => '='
				)
			) );

			/*
			$builder = $includes->get_core()->modules['cherry-interface-builder'];

			$builder->register_form(
				array(
					'pronto-ultimate-base' => array(
						'type' => 'form',
						'action' => get_site_url() . '/wp-admin/admin-post.php'
					)
				)
			);

			$fields = array();
			foreach ( $current_log[0] as $key => $field ) {
				if( !empty( $field ) ) {
					$fields[] = array(
						'id'          => $key,
						'type'        => ( $key == 'query' || $key == 'data' || $key == 'description' ) ? 'textarea' : 'text',
						'parent'      => 'pronto-ultimate-base',
						'value'       =>  $field,
						'title'       => ( $key == 'plugin_name' ) ? esc_html__( 'Plugin Name', 'pu-base' ) : ( $key == 'query' ) ? esc_html__( 'Endpoint URL', 'pu-base' ) : esc_html__( ucwords( $key ), 'pu-base' ),
						'description' => '',
						'class' 	  => 'elem-details'
					);
				}
			}
			$builder->register_control( $fields );
			*/

			$sortable_fields = array();
			foreach ($current_log[0] as $key => $field) {
			    if (! empty($field)) {
			        $sortable_fields[$key] = array(
                        'type' => ($key == 'query' || $key == 'data' || $key == 'description') ? 'textarea' : 'input',
                        'settings' => array(
                            'id' => $key,
                            'class' => 'elem-details',
                            'gridclass' => 'col-md-12',
                            'style' => 'width: 100%;',
                            'label' => array(
                                'value' => ($key == 'plugin_name') ? esc_html__('Plugin Name', 'pu-base') : ($key == 'query') ? esc_html__('Endpoint URL', 'pu-base') : esc_html__(ucwords( $key ), 'pu-base')
                            ),
                            'value' => $field
                        )
                    );
                }
            }
		?>

		<div class="pu-details-wrap">
			<?php // $builder->render(); ?>
            <?php
            Saiyan::tist()->render_module_mapping($sortable_fields);
            ?>
		</div>

	<!-- DELETE INDIVIDUAL LOGS -->
	<?php elseif( isset( $_GET['action'] ) && $_GET['action'] == 'delete' ): ?>
 		<?php
 			// grab id
			$log_id = isset( $_GET['ID'] ) ? $_GET['ID'] : '';

			// delete logs
			$response = $sf_api->pub_log_delete( $log_id );

			// redirect
			wp_redirect( 'admin.php?page=pronto-base-logs' );
 		?>

 	<!-- DELETE ALL LOGS -->
 	<?php elseif( isset( $_GET['action'] ) && $_GET['action'] == 'deleteall' ): ?>
 		<?php
 			// delete all logs
			$response = $sf_api->pub_log_delete();

			// redirect
			wp_redirect( 'admin.php?page=pronto-base-logs' );
 		?>
	<?php endif; ?>
</div>