<?php
/**
 * @author: Junjie Canonio <junjie@alphasys.com.au>
 */
if(isset($_GET['salesforceinstance'])):
	//$salesforceinstance = $_GET['salesforceinstance'];
	$salesforceinstance_settings_path = plugin_dir_path( __FILE__ ).'pronto-base-sf-org-instance-settings.php';
	require_once $salesforceinstance_settings_path;
?>
<?php else: ?>
<?php
	if(isset($_GET['delsfins']) && !empty($_GET['delsfins'])){
		delete_option($_GET['delsfins']);

		$WFCBASE_MultiInstanceSettings = get_option('WFCBASE_MultiInstanceSettings');

		$instances_arr = isset($WFCBASE_MultiInstanceSettings['instances']) ? $WFCBASE_MultiInstanceSettings['instances'] : array();

        foreach ($instances_arr as $key => $value ){
            if ($value == $_GET['delsfins']){
                unset($instances_arr[$key]);
            }
        }
        $instances_arr = array_unique($instances_arr);

        $WFCBASE_MultiInstanceSettings['instances'] = $instances_arr;
        update_option( 'WFCBASE_MultiInstanceSettings', $WFCBASE_MultiInstanceSettings);

    }
?>
<div class="wfc-don-gateway-list-container" style="padding: 20px;">
    <div class="container-fluid wfc-don-gateway-list-item-title">
        <div class="row">
            <div class="col-3">
                <span><?php echo esc_html__('Name', 'pu-base'); ?></span>
            </div>
            <div class="col-3">
                <span><?php echo esc_html__('Slug', 'pu-base'); ?></span>
            </div>
            <div class="col-3">
                <span><?php echo esc_html__('URL', 'pu-base'); ?></span>
            </div>
            <div class="col-1">
                <span><?php echo esc_html__('Status', 'pu-base'); ?></span>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
    <ul id="pronto-base-sf-org-instance-list" class="list-group">
        <li class="list-group-item"><div>Fetching Salesforce Instances ...... </div></li>
    </ul>
</div>
<?php
    wp_enqueue_style('wfc-BootstrapCDN-font-awesome-css');
    Saiyan::tist()->render_module_mapping(array(
        'wfc-base-add-salesforce-btn' => array(
            'type' => 'button',
            'settings' => array(
                'id'    => 'wfc-base-add-salesforce-btn',
                'class' => 'wfc-base-add-salesforce-btn',
                'name'  => 'wfc-base-add-salesforce-btn',
                'style' => 'margin:0px 20px 10px 20px;',
                'label'     => array(
                    'hidden'    => true,
                    'value'     => '',
                    'position'  => 'top',
                ),
                'content' => '<i id="wfc-don-resync-gateway-icon" class="fa fa-plus" style="margin-right: 8px;"></i>Add New Salesforce Instance'
            )
        ),
    ));
?>
<div id="wfc-base-modal-container">
    <div class="wfc-base-modal">
        <div class="wfc-base-header">
            <h4><?php echo __('Add New Salesforce Instance'); ?></h4>
        </div>
        <div class="wfc-base-body">
            <label>
                <span>New Salesforce Instance Name</span>
                <br>
                <input type="text" maxlength="40" name="wfc-base-salesforce-instance-name" placeholder="(e.g. My Salesforce Instance)" />
            </label>
        </div>
        <div class="wfc-base-footer">
            <div class="wfc-base-buttons-wrapper">
                <button id="wfc-base-create-sfinstance-btn" type="button" data-action="add"><?php echo __('Create Now'); ?></button>
                <button type="button" data-action="close"><?php echo __('Cancel'); ?></button>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
