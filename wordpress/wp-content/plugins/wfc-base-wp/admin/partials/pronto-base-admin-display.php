<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/admin/partials
 */



	$settings = get_option( 'pub_oauth_credential', 0 );
	$connect = isset( $settings['connect'] ) ? $settings['connect'] : 'sandbox';
	$messages = get_option( "{$connect}_authrorization_message", 0 );
	$tokens = get_option( "pronto_ultimate_{$connect}_tokens", 0 );

	$advance_settings = get_option( 'pub_advance_settings', 0 );

	$builder = $admin->includes->get_core()->modules['cherry-interface-builder'];
	$builder->register_form(
		array(
			'pronto-ultimate-base' => array(
				'type' => 'form',
				'action' => get_site_url() . '/wp-admin/admin-post.php'
			)
		)
	);

	$builder->register_component(
	    array(
	        'pub-component' => array(
	            'type'        => 'component-tab-horizontal',
	            'parent'      => 'pronto-ultimate-base',
	            'title'       => esc_html__( 'WebForce Connect - Base', 'pu-base' ),
	            'description' => esc_html__( 'Authorize your site to salesforce', 'pu-base' ),
	        )
	    )
	);

	$builder->register_settings( array(
		'pub-tab' => array(
			'parent' => 'pub-component',
			'scroll' => true,
			'title'  => esc_html__( 'Authorization', 'pu-base' ),
		),
		'pub-tab-advance' => array(
			'parent' => 'pub-component',
			'scroll' => true,
			'title'  => esc_html__( 'Advanced Settings', 'pu-base' ),
		)
	) );

	$builder->register_control( array(
		'connect' => array(
			'type'        => 'radio',
			'parent' 	  => 'pub-tab',
			'label'       => esc_html__( 'Authorize', 'pu-base' ),
			'value'       => $connect,
			'options'	  => array(
				'sandbox' => array( 'label' => 'Sandbox', 'img_src' => '', 'slave' => '' ),
				'production' => array( 'label' => 'Production', 'img_src' => '', 'slave' => '' )
			)
		),
		'sandbox_client_id' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => ( isset( $settings['sandbox_client_id'] ) && !empty( $settings['sandbox_client_id'] ) ) ? $settings['sandbox_client_id'] : '',
			'title'       => esc_html__( 'Client ID', 'pu-base' ),
			'description' => esc_html__( 'Your sandbox salesforce consumer key.', 'pu-base' ),
			'class' 	  => 'elem-sandbox'
		),
		'live_client_id' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => ( isset( $settings['live_client_id'] ) && !empty( $settings['live_client_id'] ) ) ? $settings['live_client_id'] : '',
			'title'       => esc_html__( 'Client ID', 'pu-base' ),
			'description' => esc_html__( 'Your live salesforce consumer key.', 'pu-base' ),
			'class'       => 'elem-live'
		),
		'sandbox_client_secret' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => ( isset( $settings['sandbox_client_secret'] ) && !empty( $settings['sandbox_client_secret'] ) ) ? $settings['sandbox_client_secret'] : '',
			'title'       => esc_html__( 'Client Secret', 'pu-base' ),
			'description' => esc_html__( 'Your sandbox salesforce consumer secret.', 'pu-base' ),
			'class' 	  => 'elem-sandbox'
		),
		'live_client_secret' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => ( isset( $settings['live_client_secret'] ) && !empty( $settings['live_client_secret'] ) ) ? $settings['live_client_secret'] : '',
			'title'       => esc_html__( 'Client Secret', 'pu-base' ),
			'description' => esc_html__( 'Your live salesforce consumer secret.', 'pu-base' ),
			'class'       => 'elem-live'
		),
		'sandbox_callback_url' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => ( isset( $settings['sandbox_callback_url'] ) && !empty( $settings['sandbox_callback_url'] ) ) ? $settings['sandbox_callback_url'] : get_site_url() . '/wp-admin/admin-post.php?action=authorization',
			'title'       => esc_html__( 'Redirect URI', 'pu-base' ),
			'description' => esc_html__( '', 'pu-base' ),
			'class' 	  => 'elem-sandbox'
		),
		'live_callback_url' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => ( isset( $settings['live_callback_url'] ) && !empty( $settings['live_callback_url'] ) ) ? $settings['live_callback_url'] : get_site_url() . '/wp-admin/admin-post.php?action=authorization',
			'title'       => esc_html__( 'Redirect URI', 'pu-base' ),
			'description' => esc_html__( '', 'pu-base' ),
			'class'       => 'elem-live'
		),
		'action' => array(
			'type'        => 'text',
			'parent'      => 'pub-tab',
			'value'       => 'pub_authorize',
			'title'       => esc_html__( '', 'pu-base' ),
			'description' => esc_html__( '', 'pu-base' ),
			'class'       => 'hidden'
		),
		'logs' => array(
			'type'        => 'switcher',
			'parent'      => 'pub-tab-advance',
			'value'       => isset( $advance_settings['logging'] ) ? $advance_settings['logging'] : true,
			'title'       => esc_html__( 'Enable Logs', 'pu-base' ),
			'description' => esc_html__( 'If enable, it will log all salesforce api requests', 'pu-base' ),
		),
		'cron_schedules' => array(
			'type'        => 'select',
			'parent'      => 'pub-tab-advance',
			'value'       => isset( $advance_settings['cron_schedule'] ) ? $advance_settings['cron_schedule'] : 'daily',
			'title'       => esc_html__( 'Cron Schedules', 'pu-base' ),
			'description' => esc_html__( 'Schedules when to run the Salesforce Syncer', 'pu-base' ),
			'class'       => '',
			'options'    => array(
				'hourly' => 'Every Hour',
				'daily' => 'Every Day',
				'weekly' => 'Every Week',
				'monthly' => 'Every Month'
			)
		),
		'logs_limit' => array(
			'type'        => 'select',
			'parent'      => 'pub-tab-advance',
			'value'       => isset( $advance_settings['logs_limit'] ) ? $advance_settings['logs_limit'] : '500',
			'title'       => esc_html__( 'Logs Limit', 'pu-base' ),
			'description' => esc_html__( 'Set a total logs stored to database', 'pu-base' ),
			'class'       => '',
			'options'    => array(
				'100' => '100 Records',
				'500' => '500 Records',
				'1000' => '1000 Records',
				'1500' => '1500 Recprds'
			)
		),
		'cron_schedules_cleaner' => array(
			'type'        => 'select',
			'parent'      => 'pub-tab-advance',
			'value'       => isset( $advance_settings['logs_cleaner'] ) ? $advance_settings['logs_cleaner'] : 'daily',
			'title'       => esc_html__( 'Logs Cleaner Cron Schedules', 'pu-base' ),
			'description' => esc_html__( 'Schedules when to run the Logs Cleaner', 'pu-base' ),
			'class'       => '',
			'options'    => array(
				'hourly' => 'Every Hour',
				'daily' => 'Every Day',
				'weekly' => 'Every Week',
				'monthly' => 'Every Month'
			)
		),
		'advance_btn' => array(
			'title' => '',
			'type' => 'button',
			'style' => 'primary',
			'content' => '<span class="text">' . esc_html__( 'Save Settings', 'pu-base' ) . '</span>',
			'parent' => 'pub-tab-advance',
		),
		'authorize_btn' => array(
			'title' => '',
			'type' => 'button',
			'style' => 'primary',
			'content' => '<span class="text">' . esc_html__( 'Authenticate', 'pu-base' ) . '</span>',
			'parent' => 'pub-tab',
		)
	) );

?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<style type="text/css">
	.pu-base-wrap {
		background: #ffffff;
		padding: 15px;
		border-radius: 5px;
	}

	input, textarea, select {
		background: #ffffff !important;
	}

	.pu-base-wrap .hidden{
		display: none;
	}

	.org-status-error {
		color: #dc3232;
		font-weight: bold;
	}

	.org-status-success {
		color: #46b450;
		font-weight: bold;
	}

	.loader-wrapper {
	   width: 50px;
	   height: 50px;
	   position: absolute;
	}

	.loader1, .loader2 {
	    width: 100%;
	    height: 100%;
	    border-radius: 50%;
	    background-color: #48c569;
	    opacity: 0.6;
	    position: absolute;
	    top: 0;
	    left: 0;
	    -webkit-animation: tm-bounce 2.0s infinite ease-in-out !important;
	    animation: tm-bounce 2.0s infinite ease-in-out !important;
	}

	.loader2 {
	  -webkit-animation-delay: -1.0s !important;
	  animation-delay: -1.0s !important;
	}

    @-webkit-keyframes tm-bounce {
  	0%, 100% {
    -webkit-transform: scale(0);
    transform: scale(0); }
    50% {
      -webkit-transform: scale(1);
      transform: scale(1); } }

      @keyframes tm-bounce {
        0%, 100% {
          -webkit-transform: scale(0);
          transform: scale(0); }
          50% {
            -webkit-transform: scale(1);
            transform: scale(1); } }

	pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; max-width: 768px; overflow: auto;}
	.string { color: green; }
	.number { color: darkorange; }
	.boolean { color: blue; }
	.null { color: magenta; }
	.key { color: red; }
</style>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		var connect = '<?php echo $connect ?>';
		var messages = $.parseJSON( '<?php echo ( $messages != 0 ) ? json_encode( $messages ) : 0; ?>' );

		// change event sandbox or live authorize
		$('.pu-base-wrap input[type="radio"][name="connect"]').on( 'change', function() {

			var radio_connect = $(this);
		 	if( radio_connect.val() == 'sandbox' ) {
		 		$('.elem-sandbox').show();
		 		$('.elem-live').hide();
		 	} else {
		 		$('.elem-sandbox').hide();
		 		$('.elem-live').show();
		 	}

		 	setTimeout(function() {
		 		$('.org-status').remove();
		 		$('.pub-messages').remove();

			 	$.ajax({
			 		type:"POST",
			 		data: {
			 			action: "pub_connect_to",
						formdata: $('#pronto-ultimate-base').serializeArray()
			 		},
			 		url: ajaxurl,
			 		success: function( response ) {
			 			if( response.messages == 0 ) {
			 				var statusClass = ( response.messages.status == 'success' ) ? 'updated' : 'error';
			 					$('.pu-base-wrap .pub-component h2').after( "<div class='error pub-messages'><p><strong>STATUS</strong>: Not Authenticated with Salesforce.</p></div>" );
			 			} else {
			 				if( response.messages.result ) {
			 					var data = '';
			 					$.each( response.messages.result, function(k, v) {
			 						data += "<p><strong>"+k+"</strong>: "+v+".</p>";
			 					});
			 					var statusClass = ( response.messages.status == 'success' ) ? 'updated' : 'error';
			 					$('.pu-base-wrap .pub-component h2').after( "<div class='"+statusClass+" pub-messages'>"+data+"</div>" );
			 				}
			 			}

			 			var btnText = ( response.messages.status && response.messages.status == 'success' ) ? localize_text.btn_txt2 : localize_text.btn_txt;
			 			$('#authorize_btn').text( btnText );
			 		}
			 	});
		 	}, 0);
		});

		// add test connection button
		$('.pu-base-wrap input[type="radio"][name="connect"]').each(function() {
			$(this).siblings('label').after( "<a id='"+$(this).val()+"' class='test-connection'>"+localize_text.view_details+"</a>" );
		});

		$('.test-connection').css({
			'margin-left': '10px',
			'cursor' : 'pointer',
			'font-weight': 'bold',
			'text-decoration': 'underline',
			'font-size': '10px'
		}).on( 'click', function(){
			var connection = $(this);
			connection.after( '<span class="spinner"></span>' );
			$('.spinner').css({
				'visibility' : 'visible',
				'position': 'absolute',
				'background-color': '#ffffff'
			});

			$('#connect-messages-status').remove();
			$('.connection-limit-fields').remove();
			
			$.ajax({
		 		type:"POST",
		 		data: { 
		 			action: "test_sf_connection",
		 			testconnection: connection.attr('id')
		 		},
		 		url: ajaxurl,
		 		success: function( response ) {
		 			setTimeout(function(){
						$('#TB_ajaxContent').css({
							'width' : ''
						});
					}, 0);

		 			if( response ) {
		 				$('.spinner').remove();
		 				$('.revoke-connection').remove();

		 				var connectStats = null;
		 				var connectColor = null;

		 				if( response.status == true ) {
		 					connectStats = 'Connected';
		 					connectColor = 'org-status-success';
		 				} else {
		 					connectStats = 'Not Connected';
		 					connectColor = 'org-status-error';
		 				}

		 				$('#connection-status').text( connectStats ).removeClass().addClass(connectColor);
		 				$('.thickbox').trigger('click');
		 				if( connectStats == 'Connected' ) {
		 					$('#revoke-btn').after( '<a id="'+connection.attr('id')+'" class="revoke-connection button button-primary" style="margin-left: 8px;">Revoke</a>' );

		 					// revoking connection
		 					$('.revoke-connection').on('click', function() {

		 						$('.revoke-status').remove();
		 						$('.connection-limit-fields').remove();

		 						var btn = $(this);
		 						btn.after( '<span class="spinner"></span>' );
		 						$('.spinner').css({
		 							'visibility' : 'visible',
		 							'position': 'absolute',
		 							'background-color': '#ffffff'
		 						});

		 						$.ajax({
		 							type:"POST",
		 							data: { 
		 								action: "revoke_connection",
		 								revoke: connection.attr('id')
		 							},
		 							url: ajaxurl,
		 							success: function( response ) {
		 								if( response ) {
		 									$('.spinner').remove();
		 									if( response.status == true ) {
		 										window.alert('Token is successfully revoked.');
		 										$('#connection-status').text( 'Not Connected' ).removeClass().addClass('org-status-error');

		 										$('#connect-messages-status').remove();
		 										$('#connection-messages').append( "<div style='margin-left: 0px;' id='connect-messages-status' class='error'><p><strong>STATUS</strong>: Token is revoked.</p></div>" );
		 										btn.remove();
		 										location.reload();
		 									} else {
		 										window.alert('Failed to revoked token.');
		 									}
		 								}
		 							}
		 						});
		 					});
		 				}


		 				// instance url 
		 				var instance_url = ( response.tokens.instance_url ) ? response.tokens.instance_url : 'NONE';
		 				$('#connection-instance-url').text( instance_url );

		 				// access token 
		 				var access_token = ( response.tokens.access_token ) ? 'STORED' : 'NONE';
		 				$('#connection-access-token').text( access_token );

		 				// access token 
		 				var refresh_token = ( response.tokens.refresh_token ) ? 'STORED' : 'NONE';
		 				$('#connection-refresh-token').text( refresh_token );

		 				var messages = ( response.messages.status ) ? response.messages.result : 'Not Authenticated with Salesforce.';

		 				if( typeof messages == 'string' ) {
		 					$('#connection-messages').append( "<div style='margin-left: 0px;' id='connect-messages-status' class='error'>"+messages+"</div>" );
		 				} else {
		 					var messages_data = '';
		 					var statusColor = ( response.messages.status && response.messages.status == 'success' ) ? 'updated' : 'error' ;
		 					$.each( response.messages.result, function( k, v ){
		 						messages_data += '<p><strong>'+ k.toUpperCase() +'</strong>: '+v+'.</p>';
		 					});	 					
		 					$('#connection-messages').append( "<div style='margin-left: 0px;' id='connect-messages-status' class='"+statusColor+"'>"+messages_data+"</div>" );
		 				}


		 				if( response.limits ) {

		 					var textareas = [];
		 					$.each( response.limits, function( k, v ){
		 						$('#connection-list tbody').append('<tr valign="top" class="connection-limit-fields"><td>'+k+'</td><td><textarea id="'+k+'">'+JSON.stringify(v)+'</textarea></td></tr>');
		 						textareas.push(k);
		 					});

		 					beautifyJSONTextarea( textareas );
		 				}
		 			}
		 		}
		 	});

			function beautifyJSONTextarea( elems ) {
				$.each(elems, function(v, k){
					var obj = $( '#' + k );
					if( IsJsonString( obj.val() ) ) {
						var str = syntaxHighlight( $.parseJSON( obj.val() ) );
						$( '#' + k ).after("<pre>"+str+"</pre>");
						$( '#' + k ).hide();
					}
				});
			}

			function syntaxHighlight(json) {
				if (typeof json != 'string') {
					json = JSON.stringify(json, undefined, 2);
				}
				json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
					var cls = 'number';
					if (/^"/.test(match)) {
						if (/:$/.test(match)) {
							cls = 'key';
						} else {
							cls = 'string';
						}
					} else if (/true|false/.test(match)) {
						cls = 'boolean';
					} else if (/null/.test(match)) {
						cls = 'null';
					}
					return '<span class="' + cls + '">' + match + '</span>';
				});
			}

			function IsJsonString(str) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}
				return true;
			}
		});

		// trigger first click
		var trigger = ( connect == 'sandbox' ) ? 'first' : 'last';
		$('.pu-base-wrap input[type="radio"][name="connect"]:'+trigger+'').trigger("change");

		// submit 
		$('#authorize_btn').on('click', function(){
			$('#pronto-ultimate-base').submit();
		});
	});
</script>

<?php add_thickbox(); ?>
<div id="connection-popup" style="display:none;">
	<table id="connection-list" class="wp-list-table widefat plugins" style="margin-top:10px; border:none;">
		<tbody>
			<tr valign="top">
				<td><?php echo __('Salesforce Connection Status', 'pu-base'); ?></td>
				<td><label id="connection-status"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Revoke Connection', 'pu-base'); ?></td>
				<td><label id="revoke-btn"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Instance URL', 'pu-base'); ?></td>
				<td><label id="connection-instance-url"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Access Token', 'pu-base'); ?></td>
				<td><label id="connection-access-token"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Refresh Token', 'pu-base'); ?></td>
				<td><label id="connection-refresh-token"></label></td>
			</tr>
			<tr valign="top">
				<td><?php echo __('Messages', 'pu-base'); ?></td>
				<td><label id="connection-messages"></label></td>
			</tr>
		</tbody>
	</table>
</div>
<a href="#TB_inline?width=400&height=500&inlineId=connection-popup" class="thickbox" style="display: none;"></a>	

<div class="wrap">
	<div class="pu-base-wrap">
		<?php echo $builder->render(); ?>
	</div>
</div>