<?php
	$wp_environment = $status->get_wordpress_environment();
	$server_environment = $status->get_server_environment();
	$database = $status->get_database_info();
	$security = $status->get_security_info();
	$plugins = $status->get_active_plugins();
	$themes = $status->get_theme_info();
	$overrides = $status->get_override_templates();
?>

<style type="text/css">
	table {
		width: 100%;
		background: #fff;
		border: 1px solid #e5e5e5;
		-webkit-box-shadow: 0 1px 1px rgba(0,0,0,.04);
		box-shadow: 0 1px 1px rgba(0,0,0,.04);
		margin-bottom: 20px;
	}

	th {
		border-bottom: 1px solid #e1e1e1;
		text-align: left;
		padding-left: 15px;
	}

	h2 {
		margin: 0;
		padding: 8px;
		font-size: 16px;
		color: #7d7d7d;
	}

	td {
		padding: 10px 10px 10px 25px;
		color: #7d7d7d;
	}

	table td:first-child {
		width: 42%;
	}

</style>

<div class="wrap">
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'WordPress Environment', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $wp_environment ) ): ?>
				<?php foreach( $wp_environment as $k => $v ): ?>
					<tr>
						<td><?php echo ucwords( str_replace( '_' , ' ', $k ) ) ?></td>
						<td><?php 
							if( gettype( $v ) == 'boolean' ) {
								if( $v ) {
									echo '<span style="color:#7ad03a;" class="dashicons dashicons-yes"></span>';
								} else {
									echo '<span style="color:#d03a3a" class="dashicons dashicons-no"></span>';
								}
							} else {
								if( $k == 'wp_memory_limit' ) {
									if ( $v < 67108864 ) {
										echo '<span class="dashicons dashicons-warning"></span> ' . sprintf( __( '%1$s - We recommend setting memory to at least 64MB. See: %2$s', 'pu-base' ), size_format( $v ), '<a href="https://codex.wordpress.org/Editing_wp-config.php#Increasing_memory_allocated_to_PHP" target="_blank">' . __( 'Increasing memory allocated to PHP', 'pu-base' ) . '</a>' );
									} else {
										echo size_format( $v );
									}
								} else {
									if (filter_var($v, FILTER_VALIDATE_URL) === FALSE) {
										echo $v;
									} else {
										?><a href="<?php echo $v; ?>"><?php echo $v; ?></a><?php
									}
								}
							}
						?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'Server environment', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $server_environment ) ): ?>
				<?php foreach( $server_environment as $k => $v ): ?>
					<tr>
						<td><?php echo ucwords( str_replace( '_' , ' ', $k ) ) ?></td>
						<td><?php 
							if( gettype( $v ) != 'boolean' ) {
								if( $k == 'php_post_max_size' || $k == 'max_upload_size' ) {
									echo esc_html( size_format( $v ) );
								} else {
									echo $v;
								}
							} else {
								if( $v ) {
									echo '<span style="color:#7ad03a;" class="dashicons dashicons-yes"></span>';
								} else {
									echo '<span style="color:#d03a3a" class="dashicons dashicons-no"></span>';
								}
							}
						?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'Database', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $database ) ): ?>
				<?php foreach( $database as $k => $v ): ?>
					<tr>
						<?php if( $k != 'database_tables' ): ?>
							<td><?php echo ucwords( str_replace( '_' , ' ', $k ) ) ?></td>
							<td><?php echo $v; ?></td>
						<?php else: ?>
							<?php foreach( $v as $key => $value ): ?>
								<td><?php echo $key ?></td>
								<td><?php
									if( gettype( $value ) != 'boolean' ) {
										echo $value;
									} else {
										if( $value ) {
											echo '<span style="color:#7ad03a;" class="dashicons dashicons-yes"></span>';
										} else {
											echo '<span style="color:#d03a3a" class="dashicons dashicons-no"></span>';
										}
									}
								?></td>
							<?php endforeach; ?>
						<?php endif; ?>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'Security', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $security ) ): ?>
				<?php foreach( $security as $k => $v ): ?>
					<tr>
						<td><?php echo ucwords( str_replace( '_' , ' ', $k ) ) ?></td>
						<td><?php 
							if( gettype( $v ) != 'boolean' ) {
								echo $v;
							} else {
								if( $v ) {
									echo '<span style="color:#7ad03a;" class="dashicons dashicons-yes"></span>';
								} else {
									echo '<span style="color:#d03a3a" class="dashicons dashicons-no"></span>';
								}
							}
						?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'Active Plugins', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $plugins ) ): ?>
				<?php foreach( $plugins as $k => $v ): ?>
					<tr>
						<td><?php 
						if( filter_var($v['url'], FILTER_VALIDATE_URL) === FALSE ) {
							echo $v['name']; 
						} else {
							?><a href="<?php echo $v['url']; ?>"><?php echo $v['name']; ?></a><?php
						}
						?></td>
						<td> By <?php
						if (filter_var($v['author_url'], FILTER_VALIDATE_URL) === FALSE) {
							echo $v['author_name']; 
						} else {
							?>
								<a href="<?php echo $v['author_url']; ?>"><?php echo $v['author_name']; ?></a>
							<?php
						}
						?> - <?php echo $v['version']; ?></td>
					</tr>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'Theme', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $themes ) ): ?>
				<?php foreach( $themes as $k => $v ): ?>
					<?php if( in_array( $k, array( 'name', 'version', 'author_url', 'is_child_theme' ) ) ) : ?>
					<tr>
						<td><?php echo ucwords( str_replace( '_' , ' ', $k ) ) ?></td>
						<td><?php
							if( gettype( $v ) != 'boolean' ) {
								if (filter_var($v, FILTER_VALIDATE_URL) === FALSE) {
									echo $v;
								} else {
									?><a href="<?php echo $v; ?>"><?php echo $v; ?></a><?php
								}
							} else {
								if( $v ) {
									echo '<span style="color:#7ad03a;" class="dashicons dashicons-yes"></span>';
								} else {
									echo '<span style="color:#d03a3a" class="dashicons dashicons-no"></span>';
								}
							}
						?></td>
					</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endif; ?>
		</tbody>
	</table>
	<table>
		<thead>
			<tr>
				<th colspan="3">
					<h2><?php echo esc_html_e( 'Overrides', 'pu-base'); ?></h2>
				</th>
			</tr>
		</thead>
		<tbody>
			<?php if( !empty( $overrides ) ): ?>
				<?php foreach( $overrides as $v ): ?>
					<tr>
						<td><?php echo ucwords( str_replace( '_' , ' ', $v ) ) ?> Template</td>
						<td><span style="color:#d03a3a;font-weight:bold;"><?php echo __('Override', 'pu-base'); ?></span></td>
					</tr>
				<?php endforeach; ?>
			<?php else: ?>
				<tr>
					<td>-</td>
					<td>-</td>
				</tr>
			<?php endif; ?>
		</tbody>
	</table>
</div>