<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://alphasys.com.au/
 * @since      1.0.0
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Pronto_Base
 * @subpackage Pronto_Base/admin
 * @author     AlphaSys Pty. Ltd. <danryl@alphasys.com.au>
 */
class Pronto_Base_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The main class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $includes
	 */
	private $includes;

	/**
	 * The main class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $includes
	 */
	private $sf_api;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 * @param      class object    $parent    the main class of the plugin.
	 */
	public function __construct( $plugin_name, $version, $parent ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		/**
		 * Grab main plugin file class available to admin class
		 *
		 * @since    1.0.0
		 */
		$this->includes = $parent;

		/**
		 * Initialize the pronto ultimate sf oauth class and set its properties.
		 *
		 * @since    1.0.0
		 */
		$this->sf_api = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );
	}

    /**
    * Name : enqueue_styles
    * Description: Register the stylesheets for the admin area.
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */ 
	public function enqueue_styles() {

		/**
	    * enqueue plugin admin css
	    *
	    * LastUpdated : july 17, 2017
	    * @author Danryl
	    */
		wp_enqueue_style( 
			$this->plugin_name, 
			plugin_dir_url( __FILE__ ) . 'css/pronto-base-admin.css', 
			array(), 
			$this->version, 
			'all' 
		);

		/**
	    * register bootstrap css
	    */
		wp_register_style( 
			'wfc-BootstrapCDN-css', 
			plugin_dir_url( __FILE__ ) . '../includes/bootstrap/css/bootstrap.css', 
			array(), $this->version, 
			'all' 
		);

		wp_register_style( 
			'wfc-BootstrapCDN-font-awesome-css', 
			'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', 
			array(), $this->version, 
			'all' 
		);

		/**
	    * register animate css
	    */
		wp_register_style( 
			'wfc-animate-css', 
			plugin_dir_url( __FILE__ ) . '../includes/animate/animate.css', 
			array(), $this->version, 
			'all' 
		);

        wp_register_style(
            'pronto-base-sf-org-instance-list-css',
            plugin_dir_url( __FILE__ ) . 'css/pronto-base-sf-org-instance-list.css',
            array(), $this->version,
            'all'
        );
	}

    /**
    * Name : enqueue_scripts
    * Description: Register the JavaScript for the admin area.
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */ 
	public function enqueue_scripts() {

		Saiyan_Toolkit::get_instance()->load_toolkits(array(
		    'tinter', 'spinner'
		));

		/**
	    * enqueue script for base admin
	    * LastUpdated : August 10, 2018
	    * @author Junjie Canonio
	    */
		wp_enqueue_script( 
			'pronto-base-admin-js', 
			plugin_dir_url( __FILE__ ) . 'js/pronto-base-admin.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);


		/**
	    * enqueue script for admin advanced settings page
	    *
	    * LastUpdated : july 17, 2017
	    * @author Danryl
	    * @since    1.0.0
	    */
		wp_register_script( 
			'pronto-base-advance-settings', 
			plugin_dir_url( __FILE__ ) . 'js/pronto-base-advance-settings.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);

		/**
		 * enqueue script for admin sf org instance settings
		 */
		wp_register_script(
			'wfc-base-sf-org-instance-settings-js',
			plugin_dir_url( __FILE__ ) . 'js/pronto-base-sf-org-instance-settings.js',
			array( 'jquery' ),
			$this->version,
			true
		);

        wp_register_script(
            'wfc-base-sf-org-instance-list-js',
            plugin_dir_url( __FILE__ ) . 'js/pronto-base-sf-org-instance-list.js',
            array( 'jquery' ),
            $this->version,
            true
        );

        wp_register_script(
            'wfc-base-jquery-easypaginate-js',
            plugin_dir_url( __FILE__ ) . 'js/pronto-base-jquery.easyPaginate.js',
            array( 'jquery' ),
            $this->version,
            true
        );

		/**
	    * register bootstrap js
	    */
		wp_register_script( 
			'wfc-BootstrapCDN-js', 
			plugin_dir_url( __FILE__ ) . '../includes/bootstrap/js/bootstrap.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);
		wp_register_script( 
			'wfc-BootstrapCDN-min-js', 
			plugin_dir_url( __FILE__ ) . '../includes/bootstrap/js/bootstrap.bundle.js', 
			array( 'jquery' ), 
			$this->version, 
			false 
		);
	}

    /**
    * Name : pub_scripts
    * Description: Enqueue scripts entended for page
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */ 
	public function pub_scripts( $hook ) {

		/**
		* page prefix
		*/
		$prefix = 'webforce-connect_page_';
		switch ( $hook ) {
			case $prefix . 'pronto-base':

				/**
				* enqueue authorize script
				*/
				wp_enqueue_script( 'pronto-base-advance-settings' );

				$translation_array = array(
					'btn_txt' => __( 'Authenticate', 'pu-base' ),
					'btn_txt2' => __( 'Re-Authenticate', 'pu-base' ),
					'view_details' => __( 'View Details', 'pu-base' ),
				);
				wp_localize_script( 'pronto-base-advance-settings', 'localize_text', $translation_array );
			case 'admin_page_wfc-base-multi-instance-settings':

				wp_enqueue_style( 'wfc-BootstrapCDN-css' );
				wp_enqueue_style('pronto-base-sf-org-instance-list-css');

				/**
				 * enqueue authorize script
				 */
				wp_enqueue_script( 'wfc-base-sf-org-instance-settings-js' );
				wp_enqueue_script( 'wfc-base-sf-org-instance-list-js' );
				wp_enqueue_script( 'wfc-base-jquery-easypaginate-js' );

				/*
				* Localiz script
				*/
				wp_localize_script(
					'wfc-base-sf-org-instance-settings-js',
					'wfc_base_sf_org_instance_settings_param',
					array(
						'url' => admin_url('admin-ajax.php'),
						'nonce' => wp_create_nonce('ajax-nonce'),
						'instance_list_url' => admin_url( 'admin.php?page=pronto-base'),
					)
				);

				/*
				* Localize script
				*/
				wp_localize_script(
					'wfc-base-sf-org-instance-list-js',
					'wfc_base_sf_org_instance_list_param',
					array(
						'url' => admin_url('admin-ajax.php'),
						'nonce' => wp_create_nonce('ajax-nonce')
					)
				);
			break;



		}
	}

    /**
    * Name : pub_cherry_handlers
    * Description: cherry handlers function
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */ 
	public function pub_cherry_handlers() {

		// advance settings handler
		// TODO to be removed
		/*
		$this->pub_advance_settings();

		if( is_admin() ) {
			$this->includes->get_core()->init_module( 'cherry-interface-builder', array() );
		}
		*/
	}
 
    /**
    * Name : pub_view
    * Description: Register the _view for the admin area.
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param    $template - will be the name of admin partials files
    * @param    $params - variables available for template
    * @return   html template
    * @since    1.0.0
    */ 
	public function pub_view( $template, $params = array() ) {
		ob_start();
		extract( $params );
		require( PUB_DIR . '../admin/partials/' . $template . '.php' );
		$var = ob_get_contents();
		ob_end_clean();
		return $var;
	}

    /**
    * Name : pub_view
    * Description: pronto ultimate base plugin menu
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param    
    * @return  
    * @since    1.0.0
    */
	public function pub_menu() {

		global $submenu;
		add_menu_page(
			__('Webforce Connect', 'pu-base'),
			__('Webforce Connect', 'pu-base'),
			'manage_options',
			'pronto-ultimate',
			'',
			plugin_dir_url( __FILE__ ).'images/webforce-connect.png',	
			100
		);

		add_submenu_page(
			'pronto-ultimate',
			esc_html__( 'Base', 'pu-base' ),
			esc_html__( 'Base', 'pu-base' ),
			'manage_options',
			'pronto-base',
			array( $this, 'pub_page' )
		);

		add_submenu_page(
			null,
			esc_html__( 'Base', 'pu-base' ),
			esc_html__( 'Base', 'pu-base' ),
			'manage_options',
			'wfc-base-multi-instance-settings',
			array( $this, 'wfc_base_mult_instance_list_page' )
		);

		add_submenu_page(
			'pronto-ultimate',
			esc_html__( 'Status', 'pu-base' ),
			esc_html__( 'Status', 'pu-base' ),
			'manage_options',
			'pu-system-status',
			array( $this, 'pub_system_status' )
		);

		add_submenu_page(
	        'pronto-ultimate',
	        esc_html__( 'Logs', 'pu-base' ),
	        esc_html__( 'Logs', 'pu-base' ),
	        'manage_options',
	        'pronto-base-logs',
	        array( $this, 'pub_logs_page' )
      	);
		unset( $submenu['pronto-ultimate'][0] );
	}

    /**
    * Name : pub_menu_reorder
    * Description: re order menu make logs to last
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param    
    * @return  
    * @since    1.0.0
    */
	public function pub_menu_reorder() {
		global $submenu;

		if( isset( $submenu['pronto-ultimate'][2] ) && 
			isset( $submenu['pronto-ultimate'][3] ) ) {
			$status = $submenu['pronto-ultimate'][2];
			$logs = $submenu['pronto-ultimate'][3];

			unset( $submenu['pronto-ultimate'][2] );
			unset( $submenu['pronto-ultimate'][3] );

			$submenu['pronto-ultimate'][] = $status;
			$submenu['pronto-ultimate'][] = $logs;
		}
	}

    /**
    * Name : pub_page
    * Description: display pronto ultimate base settings template
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param    
    * @return  
    * @since    1.0.0
    */
	public function pub_page() {
		$data['admin'] = $this;
		echo $this->pub_view( 'pronto-base-admin-display2', $data );
	}

	/**
	 * Name : WFC
	 * Description: display pronto ultimate base settings template
	 * LastUpdated : July 4, 2017
	 * @author Danryl
	 * @param
	 * @return
	 * @since    1.0.0
	 */
	public function wfc_base_mult_instance_list_page() {
		$data['admin'] = $this;
		echo $this->pub_view( 'pronto-base-sf-org-instance-list', $data );
	}

    /**
    * Name : pub_logs_page
    * Description: logs list page
    * LastUpdated : July 4, 2017
    * @author , Junjie Canonio, Danryl Carpio
    * @param    
    * @return  
    * @since    1.0.0
    */
	public function pub_logs_page() {
		if( class_exists( 'Pub_List_Table' ) ) {
			$paged = isset( $_GET['paged'] ) ? $_GET['paged']-1 : 0;
			$perpage = 10;
			$offset = $paged * $perpage;
			$total = $this->sf_api->pub_count_logs();

			$list = array();
			$logs_list = $this->sf_api->pub_get_logs(NULL,'publogid', 'DESC', $perpage, $offset);
			if( sizeof( $logs_list ) > 0 ) {
				foreach( $logs_list as $item ) {

					$limit = isset( $item['count'] ) ? $item['count'] : 0;
					$class = ( $limit < 5 ) ? 'sync-limit-notreach' : 'sync-limit-reach'; 

					$row_data = array();
					$row_data['ID'] = $item['publogid'];
					$row_data['date'] = $item['date'];
					$row_data['title'] = $item['plugin_name'];
					$row_data['user'] = $item['user'];
					$row_data['method'] = '<div class="sync-method-'.strtolower( $item['method'] ).'">' . $this->pub_get_icon( $item
					['method'] ) . $item['method'] .'</div>';
					$row_data['status'] = '<div class="sync-status-'.strtolower( $item['status'] ).'">'. $item['status'] . '</div>';
					$row_data['sf_instance'] = '<div class="">'. $item['sf_instance'] . '</div>';
					$row_data['limit'] = '<div class="'.$class.'">' . $limit . '</div>';
					$list[] = $row_data;
				}
			}

			$columns = array(
				'ID' => 'ID',
	            'date' => 'Date',
				'title' => 'Plugin Name',
	            'user' => 'User',
	            'limit' => 'Count',
	            'method' => 'Method',
                'sf_instance' => 'SF Instance',
	            'status' => 'Status',
	        );

			$data['includes'] = $this->includes;
			$data['sf_api'] = $this->sf_api;

			$data['table'] = new Pub_List_Table(
				$list, 
				$columns, 
				10, 
				true, 
				array( 'details', 'delete' ), 
				array(), 
				$total
			);
			echo $this->pub_view( 'pronto-base-logs', $data );
		} else {
			esc_html_e( "Pub_List_Table class does not exist.", 'pu-base' );
		}
	}

	public function pub_system_status() {
		$data['admin'] = $this;
		$data['status'] = class_exists( 'PU_System_Status' ) ? new PU_System_Status() : null;
		echo $this->pub_view( 'pronto-base-system-status', $data );
	}

	/**
	 * Ajax callback for saving Saleforce Org Instance settings.
	 *
	 * @author Junjie Canonio
	 * @LastUpdated  September 13, 2019
	 */
	public function wfc_base_createsfinstance_callback(){
		if (isset($_POST['action']) && $_POST['action'] == 'wfc_base_createsfinstance_action') {
			$instance_name = isset($_POST['instance_name']) ? $_POST['instance_name'] : '';
			$instance_name_nospaces = str_replace(' ', '', $instance_name);

			if (empty($instance_name_nospaces)){
				$response_arr['status'] = 'Failed';
				$response_arr['msg'] = 'Please provide Instance Name.' ;
			}else {
				$wfc_base_salesforceinstance_slug = WFC_Base_MultiSFInstanceSettingsHelper::generate_slug($instance_name);

				$sforginstancesettings_args = array(
					'wfc_base_salesforceinstance_name' 	 => $instance_name,
					'wfc_base_salesforceinstance_slug' 	 => $wfc_base_salesforceinstance_slug
				);
				update_option( $wfc_base_salesforceinstance_slug, $sforginstancesettings_args);

				$WFCBASE_MultiInstanceSettings = get_option('WFCBASE_MultiInstanceSettings');

				$instances_arr = isset($WFCBASE_MultiInstanceSettings['instances']) ? $WFCBASE_MultiInstanceSettings['instances'] : array();
				array_push($instances_arr,$wfc_base_salesforceinstance_slug);
				$instances_arr = array_unique($instances_arr);

				$WFCBASE_MultiInstanceSettings['instances'] = $instances_arr;
				update_option( 'WFCBASE_MultiInstanceSettings', $WFCBASE_MultiInstanceSettings);

				$response_arr['status'] = 'Success';
				$response_arr['settings_url'] = admin_url( 'admin.php?page=wfc-base-multi-instance-settings&salesforceinstance='.$wfc_base_salesforceinstance_slug);
				$response_arr['msg'] = 'Instance Successfully Created' ;
			}

			$response_arr['data'] = $_POST;
			$response_arr['instance_name'] = $instance_name;
			$response_arr['sforginstancesettings_args'] = $sforginstancesettings_args;
			echo json_encode($response_arr);
			wp_die();
		}
	}

	/**
	 * Ajax callback for saving Saleforce Org Instance settings.
	 *
	 * @author Junjie Canonio, Von Sienard Vibar
	 * @LastUpdated  September 11, 2019
	 */
	public function wfc_base_savesforginstancesettings_callback() {
		if(isset($_POST['action']) && $_POST['action'] =='wfc_base_savesforginstancesettings_action'){

			$data_arr = isset($_POST['data']) ? $_POST['data'] : array();

			$optiondon_arr = array();
			if(is_array($data_arr)){
				foreach ($data_arr as $key => $value) {
					$optiondon_arr[$value['name']] = $value['value'];
				}
			}

			$wfc_base_salesforceinstance_name = isset($optiondon_arr['wfc_base_salesforceinstance_name']) ? $optiondon_arr['wfc_base_salesforceinstance_name'] : '';
			$wfc_base_salesforceinstance_name_clean = WFC_Base_MultiSFInstanceSettingsHelper::generate_slug($wfc_base_salesforceinstance_name);
			$wfc_base_salesforceinstance_slug = isset($optiondon_arr['wfc_base_salesforceinstance_slug']) ? $optiondon_arr['wfc_base_salesforceinstance_slug'] : '';
			$wfc_base_salesforceinstance_slug = !empty($wfc_base_salesforceinstance_slug) ? $wfc_base_salesforceinstance_slug : $wfc_base_salesforceinstance_name_clean;
			$wfc_base_clientid = isset($optiondon_arr['wfc_base_clientid']) ? $optiondon_arr['wfc_base_clientid'] : '';
			$wfc_base_clientsecret = isset($optiondon_arr['wfc_base_clientsecret']) ? $optiondon_arr['wfc_base_clientsecret'] : '';
			$wfc_base_callbackurl = isset($optiondon_arr['wfc_base_callbackurl']) ? $optiondon_arr['wfc_base_callbackurl'] : '';
			$wfc_base_callbackurl = !empty($wfc_base_salesforceinstance_slug) ? $wfc_base_callbackurl : get_site_url() . '/wp-admin/admin-post.php?action=authorization&wfcbaseinst='.$wfc_base_salesforceinstance_name_clean;
			$wfc_base_instancetype = isset($optiondon_arr['wfc_base_instancetype']) ? $optiondon_arr['wfc_base_instancetype'] : '';
			$wfc_base_setinstancedefault = isset($optiondon_arr['wfc_base_setinstancedefault']) ? $optiondon_arr['wfc_base_setinstancedefault'] : 'true';

			$sforginstancesettings = get_option($wfc_base_salesforceinstance_slug);

			$sforginstancesettings_args = array(
				'wfc_base_salesforceinstance_name' 	 => $wfc_base_salesforceinstance_name,
				'wfc_base_salesforceinstance_slug' 	 => $wfc_base_salesforceinstance_slug,
				'wfc_base_clientid' 	 => $wfc_base_clientid,
				'wfc_base_clientsecret' 	  => $wfc_base_clientsecret,
				'wfc_base_callbackurl' => $wfc_base_callbackurl,
				'wfc_base_instancetype' 		 => $wfc_base_instancetype,
				'wfc_base_setinstancedefault' 		 => $wfc_base_setinstancedefault,
			);

			if (isset($sforginstancesettings['authorization_message']) && ! empty($sforginstancesettings['authorization_message'])) {
                $sforginstancesettings_args['authorization_message'] = $sforginstancesettings['authorization_message'];
            }

            if (isset($sforginstancesettings['tokens']) && ! empty($sforginstancesettings['tokens'])) {
                $sforginstancesettings_args['tokens'] = $sforginstancesettings['tokens'];
            }


			if (empty($wfc_base_salesforceinstance_name)){
				$response_arr['status'] = 'Failed';
				$response_arr['msg'] = 'Please provide Instance Name.' ;
			}else if (empty($wfc_base_clientid)){
				$response_arr['status'] = 'Failed';
				$response_arr['msg'] = 'Please provide Client ID.' ;
			}else if (empty($wfc_base_clientsecret)){
				$response_arr['status'] = 'Failed';
				$response_arr['msg'] = 'Please provide Client Secret.' ;
			}else{
				update_option( $wfc_base_salesforceinstance_slug, $sforginstancesettings_args);

				$WFCBASE_MultiInstanceSettings = get_option('WFCBASE_MultiInstanceSettings');
				if ($wfc_base_setinstancedefault == 'true'){
					if (isset($WFCBASE_MultiInstanceSettings['default_instance']) && !empty($WFCBASE_MultiInstanceSettings['default_instance'])){
						$default_instance = $WFCBASE_MultiInstanceSettings['default_instance'];
						$default_instance_settings = get_option($default_instance);
						$default_instance_settings['wfc_base_setinstancedefault'] = 'false';
						update_option( $default_instance, $default_instance_settings);
					}
					$WFCBASE_MultiInstanceSettings['default_instance'] = $wfc_base_salesforceinstance_slug;
				}else{
					if (isset($WFCBASE_MultiInstanceSettings['default_instance']) && !empty($WFCBASE_MultiInstanceSettings['default_instance']) && $WFCBASE_MultiInstanceSettings['default_instance'] == $wfc_base_salesforceinstance_slug){
						unset($WFCBASE_MultiInstanceSettings['default_instance']);
					}
				}

				$instances_arr = isset($WFCBASE_MultiInstanceSettings['instances']) ? $WFCBASE_MultiInstanceSettings['instances'] : array();
				array_push($instances_arr,$wfc_base_salesforceinstance_slug);
				$instances_arr = array_unique($instances_arr);

				$WFCBASE_MultiInstanceSettings['instances'] = $instances_arr;
				update_option( 'WFCBASE_MultiInstanceSettings', $WFCBASE_MultiInstanceSettings);

				if(isset($_POST['authenticate']) && $_POST['authenticate'] == 'true'){
					$response_arr['authenticate'] = 'true';
					WFC_Base_MultiSFInstanceOauth::redirect_to_get_access_code($wfc_base_salesforceinstance_slug);
				}

				$response_arr['status'] = 'Success';
				$response_arr['settings_url'] = admin_url( 'admin.php?page=wfc-base-multi-instance-settings&salesforceinstance='.$wfc_base_salesforceinstance_slug);
				$response_arr['msg'] = 'Save Successful' ;
			}

			$response_arr['data'] = $data_arr;
			$response_arr['sforginstancesettings'] = $sforginstancesettings_args;
			echo json_encode($response_arr);
			wp_die();
		}
	}

    /**
    * Name : pub_get_icon
    * Description: get method icon
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param    $method - will be the sf api method type
    * @return   html span tag with class (dashicons)
    * @since    1.0.0
    */
	public function pub_get_icon( $method ) {
		$method = strtolower( $method );
		if( $method == 'create' ) {
			return '<span class="dashicons dashicons-welcome-add-page"></span> ';
		} elseif( $method == 'update' ) {
			return '<span class="dashicons dashicons-welcome-write-blog"></span> ';
		} elseif( $method == 'delete' ) {
			return '<span class="dashicons dashicons-trash"></span> ';
		} elseif( $method == 'authorize' ) {
			return '<span class="dashicons dashicons-cloud"></span> ';
		} elseif( $method == 'revoke' ) {
			return '<span class="dashicons dashicons-dismiss"></span> ';
		} else {
			return '<span class="dashicons dashicons-arrow-up-alt"></span> ';
		}
	}

    /**
    * Name : pub_authorization
    * Description: Pronto Ultimate Base Handle AUthorization
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_authorization() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'pub_authorize' ) {

			/**
			* grab and save creditials
			*/
			$data = $_POST;

			update_option( 'pub_oauth_credential', array(
				'connect' => isset( $data['connect'] ) ? $data['connect'] : 'sandbox',
				'sandbox_client_id' => isset( $data['sandbox_client_id'] ) ? $data['sandbox_client_id'] : '',
				'live_client_id' => isset( $data['live_client_id'] ) ? $data['live_client_id'] : '',
				'sandbox_client_secret' => isset( $data['sandbox_client_secret'] ) ? $data['sandbox_client_secret'] : '',
				'live_client_secret' => isset( $data['live_client_secret'] ) ? $data['live_client_secret'] : '',
				'sandbox_callback_url' => isset( $data['sandbox_callback_url'] ) ? $data['sandbox_callback_url'] : '',
			    'live_callback_url' => isset( $data['live_callback_url'] ) ? $data['live_callback_url'] : '',
			    'action' => isset( $data['action'] ) ? $data['action'] : '',
			) );

			/**
			* redirect to salesforce for authrorization
			*/
			$this->sf_api->redirect_to_get_access_code();
		}
	}

    /**
    * Name : pub_authorization_handler
    * Description: Get Access token, refresh token and instance url
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_authorization_handler() {
		if( isset( $_GET['action'] ) && $_GET['action'] == 'authorization' ) {

			// authentication error occured
			if( isset( $_GET['error'] ) ) {

				/**
				*  save error message
				*/
				update_option( "{$this->sf_api->connect}_authrorization_message", array( 
					'status' => 'failed',
					'result' => array(
						'error' => isset( $_GET['error'] ) ? $_GET['error'] : '',
						'error_description' => isset( $_GET['error_description'] ) ? $_GET['error_description'] : '',
						) 
					)
				);

			} else {

				/**
				*  authorize website
				*/
				$response = $this->sf_api->validate_authorization();

				/**
				*  check authorization status
				*/
	 			if( isset( $response['status'] ) && $response['status'] == 'failed' ) {

	 				/**
					*  save error message
					*/
	 	 			update_option( "{$this->sf_api->connect}_authrorization_message", $response );

	 			} else {

	 				/**
					* save success message
					*/
	 			 	update_option( "{$this->sf_api->connect}_authrorization_message", array(
	 			 		'status' => 'success',
	 			 		'result' => array(
	 			 			'Success' => esc_html__( 'Successfully Authenticated with Salesforce', 'pu-base' )
	 			 		)
	 			 	) );

	 			 	/**
					* grab sf response
					*/
	 			 	$sf_response = $response['sf_response'];

	 			 	/**
					* save tokens to database
					*/
	 				update_option( "pronto_ultimate_{$this->sf_api->connect}_tokens", array(
	 					'access_token' => isset( $sf_response['access_token'] ) ? $sf_response['access_token'] : '',
	 					'refresh_token' => isset( $sf_response['refresh_token'] ) ? $sf_response['refresh_token'] : '',
	 					'instance_url' => isset( $sf_response['instance_url'] ) ? $sf_response['instance_url'] : ''
	 				) );
	 			}
			}

 			// redirect back to settings page page
 			header('Location: ' . get_site_url() . '/wp-admin/admin.php?page=pronto-base');
		}
	}

    /**
    * Name : pub_change_connect_to
    * Description: action handler when changing conenction ( sandbox/production )
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_change_connect_to() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'pub_connect_to' ) {
			$data = array();
			$accepted = array(
				'connect',
				'sandbox_client_id',
				'live_client_id',
				'sandbox_client_secret',
				'live_client_secret',
				'sandbox_callback_url',
				'live_callback_url',
				'action'
			);
			if( isset( $_POST['formdata'] ) ) {
				foreach ( $_POST['formdata'] as $fields ) {
					if( in_array( $fields['name'], $accepted ) ) {
						$data[$fields['name']] = $fields['value'];
					}
				}
			}
			update_option( 'pub_oauth_credential', $data );
			
			$data_connect = isset($data['connect']) ? $data['connect'] : '';
			wp_send_json(array(
				'messages' => get_option( "{$data_connect}_authrorization_message", 0 ),
				'tokens' => get_option( "pronto_ultimate_{$data_connect}_tokens", 0 )
			));
		}
	}

    /**
    * Name : pub_test_connection
    * Description: testing connection to salesforce
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_test_connection() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'test_sf_connection' ) {

			/**
			* get api limits
			*/
			$response = $this->sf_api->api_limits( $_POST['testconnection'] );
			if( isset( $response['status_code'] ) && $response['status_code'] == 200 ) {
				$status = true;
			} else {
				$status = false;
			}

			wp_send_json(array(
				'status' => $status,
				'limits' => isset( $response['sf_response'] ) ? $response['sf_response'] : array(),
				'messages' => get_option( "{$_POST['testconnection']}_authrorization_message", 0 ),
				'tokens' => get_option( "pronto_ultimate_{$_POST['testconnection']}_tokens", 0 )
			));
		}
	}

    /**
    * Name : pub_revoke_token
    * Description: revoke stored token
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_revoke_token() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'revoke_connection' ) {
			$token = get_option( "pronto_ultimate_{$_POST['revoke']}_tokens" );
			$revoke = $this->sf_api->revoke_refresh_token( $token['refresh_token'] );

			if( $revoke ) {
				update_option( "{$_POST['revoke']}_authrorization_message", array(
 			 		'status' => 'failed',
 			 		'result' => array(
 			 			'STATUS' => esc_html__( 'Token is revoked', 'pu-base' )
 			 		)
 			 	) );
			}

			wp_send_json(array(
				'status' => $revoke
			));
		}
	}

    /**
    * Name : pub_data_syncer
    * Description: batch that run every hour to sync failed salesforce data
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_data_syncer() {

		/**
		* get logs
		*/
		$logs = $this->sf_api->pub_get_logs( array(
			'status' => array(
				'value' => "'failed'",
				'operator' => '='
			),
			'count' => array(
				'value' => 5,
				'operator' => '<'
			),
			'method' => array(
				'value' => array('create','update','delete'),
				'operator' => 'IN'
			),
			'resync' => array(
				'value' => true,
				'operator' => '='
			)
		) );

		if( sizeof( $logs ) > 0 ) {
			foreach( $logs as $item ) {
				/**
				* run background batch processing 
				*/
				delete_transient( 'doing_cron' );
				wp_schedule_single_event( time() + 1, 'pub_batch_processing', array( $item ) );
				spawn_cron();
			}
		}
	}

	/**
    * Name : pub_batch_process
    * Description: batch process, handler for re-syncing data to salefsorce
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param $item = array(), this will be the logs record 
    * @return 
    * @since    1.0.0
    */
	public function pub_batch_process( $item ) {

		$query = explode( 'services/', $item['query'] )[1];

		/**
		* send request to salesfoce
		*/
		$response = $this->sf_api->api_request(
			$query, 
			$item['data'], 
			strtolower( $item['method'] ),
			false
		);

		/**
		* parse salesforce response
		*/
		if( isset( $response['sf_error'] ) ) {
			$item['status'] = 'FAILED';
			$item['description'] = $response['sf_error'];
		} elseif( $response['curl_response']['curl_errno'] != 0 ) {
			$item['status'] = 'FAILED';
			$item['description'] = $response['curl_response'];
		} else {
			$item['status'] = 'SUCCESS';
			$item['description'] = "{$item['method']} successful";
		}

		/**
		* set date
		*/
		$item['date'] = date("Y-m-d H:i:s");
		$item['count'] = $item['count'] += 1;

		$this->sf_api->pub_log_update( array(
			'date' => $item['date'],
			'plugin_name' => $item['plugin_name'],
			'user' => $item['user'],
			'count' => $item['count'],
			'query' => $item['query'],
			'data' => is_array( $item['data'] ) ? json_encode( $item['data'] ) : $item['data'],
			'method' => $item['method'],
			'status' => $item['status'],
			'description' => is_array( $item['description'] ) ? json_encode( $item['description'] ) : $item['description']
		),$item['publogid'] );
	}

	/**
    * Name : pub_advance_settings
    * Description: advance settings handler
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_advance_settings() {
		// TODO to be removed
		/*
		$this->includes->get_core()->init_module(
			'cherry-handler' ,
			array(
				'id'           => 'pub_advance_settings',
				'action'       => 'pub_advance_settings',
				'capability'   => 'manage_options',
				'callback'     => $this->pub_advance_settings_callback(),
				'sys_messages' => array(
					'invalid_base_data' => esc_html__( 'Unable to process the request without nonce or server error', 'pu-base' ),
					'no_right'          => esc_html__( 'No right for this action', 'pu-base' ),
					'invalid_nonce'     => esc_html__( 'Stop CHEATING!!!', 'pu-base' ),
					'access_is_allowed' => esc_html__( 'Options save successfully.','pu-base' ),
				 	'wait_processing'   => esc_html__( 'Please wait, processing the previous request', 'pu-base' ),
				),
			)
		);
		*/
	}

	/**
    * Name : pub_advance_settings_callback
    * Description: advance settings callback
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_advance_settings_callback() {
		if( isset( $_POST['action'] ) && $_POST['action'] === 'pub_advance_settings' && wp_verify_nonce( $_POST['nonce'], pub_advance_settings ) ) {

			/**
			* update advance settings
			*/
			update_option( 'pub_advance_settings', $_POST['data'] );

			/**
			* clear schedule hook
			*/
			wp_clear_scheduled_hook( 'pub_data_sycher' );
			if ( !wp_next_scheduled ( 'pub_data_sycher' ) ) {
				wp_schedule_event( time(), $_POST['data']['cron_schedule'] , 'pub_data_sycher' );
			}

			/**
			* clear schedule hook
			*/
			wp_clear_scheduled_hook( 'pub_logs_cleaner' );
			if ( !wp_next_scheduled ( 'pub_logs_cleaner' ) ) {
				wp_schedule_event( time(), $_POST['data']['logs_cleaner'] , 'pub_logs_cleaner' );
			}

			wp_send_json(array(
				'message' => esc_html__( 'Settings is saved successfully', 'pu-base' ),
				'type' => 'success-notice'
			));
		}
	}

	/**
    * Name : pub_cron_add_weekly
    * Description: add custom cron schedule
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_cron_add_weekly( $schedules ) {
	 	/**
		* Adds once weekly to the existing schedules.
		*/
	 	$schedules['weekly'] = array(
	 		'interval' => 604800,
	 		'display' => __( 'Once Weekly' )
	 	);

	 	$schedules['monthly'] = array(
	 		'interval' => 2635200,
	 		'display' => __( 'Once Monthly' )
	 	);

	 	return $schedules;
	}

	/**
    * Name : pub_clean_logs
    * Description: logs cleaner callback
    * LastUpdated : July 4, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_clean_logs() {
		$records = $this->sf_api->pub_get_logs( NULL, 'publogid', 100 );
		$settings = get_option( 'pub_advance_settings', array() );
		$total_size = sizeof( $records );
		$limit = isset( $settings['logs_limit'] ) ? (int)$settings['logs_limit'] : 500;
		if( $total_size > $limit ) {
			$size = $total_size - $limit;
			for ( $i=0; $i < $size; $i++ ) {
				$this->sf_api->pub_log_delete( $records[$i]['publogid'] );
			}
		}
	}

	/**
    * Name : pub_do_output_buffer
    * Description: enable content/output buffering for plugin
    * LastUpdated : July 5, 2017
    * @author Danryl
    * @param 
    * @return 
    * @since    1.0.0
    */
	public function pub_do_output_buffer() {
	    ob_start();
	}

    /**
     * AJAX Callback that returns the default Salesforce instance and a list of created Salesforce instances.
     *
     * @author Von Sienard Vibar
     *
     * @LastUpdated September 9, 2019
     */
	public function wfc_fetch_sf_org_instance_list_callback() {

        if( ! isset( $_POST['action'] ) && $_POST['action'] != 'wfc_fetch_sf_org_instance_list' ) wp_die();

	    $response = null;
	    $wfcbase_multiinstancesettings = get_option('WFCBASE_MultiInstanceSettings');

	    // print_r($wfcbase_multiinstancesettings);

	    $default_instance = $wfcbase_multiinstancesettings['default_instance'];
	    $instances = $wfcbase_multiinstancesettings['instances'];

        $sf_instances = array();

	    if (isset($default_instance)) {
            $instance_option = get_option($default_instance);
            $instance_option['wfc_base_salesforceinsntace_settings_url'] = admin_url() . 'admin.php?page=wfc-base-multi-instance-settings&salesforceinstance=' . $instance_option['wfc_base_salesforceinstance_slug'];
		    $instance_option['instances_connection_status'] = WFC_Base_MultiSFInstanceOauth::test_connection($default_instance);

            array_push($sf_instances, $instance_option);
        }

	    foreach ($instances as $instance) {
	        // print_r(get_option($instance));

            $instance_option = get_option($instance);
		    $instance_option['instances_connection_status'] = WFC_Base_MultiSFInstanceOauth::test_connection($instance);

            if ($instance_option['wfc_base_salesforceinstance_slug'] === $default_instance) continue;

            $instance_option['wfc_base_salesforceinsntace_settings_url'] = admin_url() . 'admin.php?page=wfc-base-multi-instance-settings&salesforceinstance=' . $instance_option['wfc_base_salesforceinstance_slug'];

	        array_push($sf_instances, $instance_option);
        }

        $response['default_instance'] = $default_instance;
	    $response['instances'] = $sf_instances;
	    $response['default_ribbon_img'] = plugin_dir_url(__FILE__) . 'images/wfc_base_default_salesforceinstance.png';
	    $response['add_new_instance_url'] = admin_url( 'admin.php?page=wfc-base-multi-instance-settings&salesforceinstance');

        wp_send_json($response);
    }

    /**
     * Callback the will start the Salesforce authorization
     *
     * @author Von Sienard Vibar
     *
     * @LastUpdated September 10, 2019
     */
    public function wfc_sf_org_instance_authorize_callback() {

        if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_sf_org_instance_authorize' ) {

            /**
             * grab and save creditials
             */
            $data = $_POST;

            $name = $data['wfc_base_salesforceinstance_name'];
            $slug = $data['wfc_base_salesforceinstance_slug'];
            $client_id = $data['wfc_base_clientid'];
            $client_secret = $data['wfc_base_clientsecret'];
            $callback_url = $data['wfc_base_callbackurl'];
            $instance_type = $data['wfc_base_instancetype'];
            $set_instance_default = $data['wfc_base_setinstancedefault'];

            update_option($slug , array(
                'wfc_base_salesforceinstance_name'  => $name,
                'wfc_base_salesforceinstance_slug'  => $slug,
                'wfc_base_clientid'                 => $client_id,
                'wfc_base_clientsecret'             => $client_secret,
                'wfc_base_callbackurl'              => $callback_url,
                'wfc_base_instancetype'             => $instance_type,
                'wfc_base_setinstancedefault'       => $set_instance_default,
            ) );

            /**
             * redirect to salesforce for authrorization
             */
            $this->sf_api->set_sf_instance($slug);
            $this->sf_api->redirect_to_get_access_code();
        }
    }

    /**
     * Callback after Salesforce authorization which will get and store the tokens once success
     *
     * @author Von Sienard Vibar <von@alphasys.com.au>
     *
     * @LastUpdated September 11, 2019
     */
    public function wfc_sf_org_instance_authorization_handler_callback() {
        if( isset( $_GET['action'] ) && $_GET['action'] == 'authorization' ) {

            $wfcbaseinst = $_GET['wfcbaseinst'];

            $sf_instance = get_option($wfcbaseinst);

            // authentication error occured
            if( isset( $_GET['error'] ) ) {

                /**
                 *  save error message
                 */
                $sf_instance['authorization_message'] = array(
                    'status' => 'failed',
                    'result' => array(
                        'error' => isset( $_GET['error'] ) ? $_GET['error'] : '',
                        'error_description' => isset( $_GET['error_description'] ) ? $_GET['error_description'] : '',
                    )
                );

            } else {

                /**
                 *  authorize website
                 */
                $this->sf_api->set_sf_instance($wfcbaseinst);
                $response = $this->sf_api->validate_authorization();

                /**
                 *  check authorization status
                 */
                if( isset( $response['status'] ) && $response['status'] == 'failed' ) {

                    /**
                     *  save error message
                     */
                    $sf_instance['authorization_message'] = $response;

                } else {

                    /**
                     * save success message
                     */
                    $sf_instance['authorization_message'] = array(
                        'status' => 'success',
                        'result' => array(
                            'Success' => esc_html__( 'Successfully Authenticated with Salesforce', 'pu-base' )
                        )
                    );

                    /**
                     * grab sf response
                     */
                    $sf_response = $response['sf_response'];

                    /**
                     * save tokens to database
                     */
                    $sf_instance['tokens'] = array(
                        'access_token' => isset( $sf_response['access_token'] ) ? $sf_response['access_token'] : '',
                        'refresh_token' => isset( $sf_response['refresh_token'] ) ? $sf_response['refresh_token'] : '',
                        'instance_url' => isset( $sf_response['instance_url'] ) ? $sf_response['instance_url'] : ''
                    );
                }
            }

            update_option($wfcbaseinst, $sf_instance);

            var_dump($sf_instance);

            // redirect back to settings page page
            header('Location: ' . get_site_url() . '/wp-admin/admin.php?page=wfc-base-multi-instance-settings&salesforceinstance=' . $wfcbaseinst . '&authorized');
        }
    }

    public function wfc_sf_org_instance_revoke_callback() {

        if (! isset($_POST['action']) && $_POST['action'] !== 'wfc_sf_org_instance_revoke') wp_die();

        $sf_instance_id = $_POST['sf_instance_id'];

        $sf_instance = get_option($sf_instance_id);

        unset($sf_instance['tokens']);
        unset($sf_instance['authorization_message']);

        update_option($sf_instance_id, $sf_instance);

        wp_send_json_success();
    }
}