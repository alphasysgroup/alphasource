=== Plugin Name ===
Contributors: Alphasys
Donate link: https://alphasys.com.au
Tags: comments, spam, salesforce, alphasys
Requires at least: WordPress 4.5
Tested up to: WordPress 4.9.8
Stable tag: 1.0.0
Version: 1.0.0
License: GPLv2 or later
License URI: https://alphasys.com.au

This plugin contains the core functionality of connecting Wordpress to Salesforce.

== Description ==

Webforce Connect – Base contains the core functionality of all the Webforce Connect products and is to connect Wordpress to Salesforce. Webforce Connect – Base includes various of css frameworks and other utility libraries that is being on most of
the Webforce Connect products.

== Installation ==

Upon installation we highly reccomend to view the official Webforce Connect – Base documentation.

== Screenshots ==

Screenshots are included on the official Webforce Connect – Base documentation

== Changelog ==

= 1.0 =
* The original admin UI/UX has been updated.

== Arbitrary section ==

The Salesforce organization used should have the following Salesforce Packages below:
* Webforce Connect

== A brief Markdown Example ==

1. Full connection to Salesforce.
2. Easy to modify admin configurations.