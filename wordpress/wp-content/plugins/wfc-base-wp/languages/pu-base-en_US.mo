��    9      �      �      �  >   �     �     �               -     :  	   H  !   R     t  	   y     �     �     �  
   �     �     �  2   �  "         #     0     5  
   Q     \     e  	   n     x  $   �     �     �     �     �     �       &     +   8     d     m  #   �     �     �  *   �     �       R        [     l     y     �     �  "   �  %   �  %     (   '     P     h  �  �  >   #
     b
     o
     ~
     �
     �
     �
  	   �
  !   �
     �
  	   �
     �
            
        *     6  2   C  "   v     �     �     �  
   �     �     �  	   �     �  $   �          /     <     J     \     y  &   �  +   �     �     �  #   �          9  *   @     k     x  R   ~     �     �     �             "   .  %   Q  %   w  (   �     �     �   %1$s - We recommend setting memory to at least 64MB. See: %2$s Access Token Active Plugins Advanced Settings AlphaSys Pty. Ltd. Authenticate Authorization Authorize Authorize your site to salesforce Base Client ID Client Secret Cron Schedules Database Delete All Enable Logs Endpoint URL If enable, it will log all salesforce api requests Increasing memory allocated to PHP Instance URL Logs Logs Cleaner Cron Schedules Logs Limit Messages Override Overrides Plugin Name Pub_List_Table class does not exist. Re-Authenticate Redirect URI Refresh Token Revoke Connection Salesforce Connection Status Save Settings Schedules when to run the Logs Cleaner Schedules when to run the Salesforce Syncer Security Server environment Set a total logs stored to database Settings is saved successfully Status Successfully Authenticated with Salesforce Sync details Theme This plugin contains the core functionality of connecting Wordpress to Salesforce. Token is revoked View Details WebForce Connect WebForce Connect - Base WordPress Environment Your live salesforce consumer key. Your live salesforce consumer secret. Your sandbox salesforce consumer key. Your sandbox salesforce consumer secret. http://alphasys.com.au/ https://alphasys.com.au/ Project-Id-Version: Pronto Ultimate - Base
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-19 07:10+0000
PO-Revision-Date: 2018-07-19 07:23+0000
Last-Translator: admin <von@alphasys.com.au>
Language-Team: English (United States)
Language: en_US
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/ %1$s - We recommend setting memory to at least 64MB. See: %2$s Access Token Active Plugins Advanced Settings AlphaSys Pty. Ltd. Authenticate Authorization Authorize Authorize your site to salesforce Base Client ID Client Secret Cron Schedules Database Delete All Enable Logs Endpoint URL If enable, it will log all salesforce api requests Increasing memory allocated to PHP Instance URL Logs Logs Cleaner Cron Schedules Logs Limit Messages Override Overrides Plugin Name Pub_List_Table class does not exist. Re-Authenticate Redirect URI Refresh Token Revoke Connection Salesforce Connection Status Save Settings Schedules when to run the Logs Cleaner Schedules when to run the Salesforce Syncer Security Server environment Set a total logs stored to database Settings is saved successfully Status Successfully Authenticated with Salesforce Sync details Theme This plugin contains the core functionality of connecting Wordpress to Salesforce. Token is revoked View Details WebForce Connect WebForce Connect - Base WordPress Environment Your live salesforce consumer key. Your live salesforce consumer secret. Your sandbox salesforce consumer key. Your sandbox salesforce consumer secret. http://alphasys.com.au/ https://alphasys.com.au/ 