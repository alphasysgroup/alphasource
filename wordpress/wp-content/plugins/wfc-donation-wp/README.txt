=== Plugin Name ===
Contributors: Alphasys
Donate link: https://alphasys.com.au
Tags: comments, spam, donation, salesforce, alphasys
Requires at least: WordPress 4.5
Tested up to: WordPress 4.9.8
Stable tag: 2.0.0.0
Version: 2.0.0.0
License: GPLv2 or later
License URI: https://alphasys.com.au

Webforce Connect – Donation allows you to accept online donations on your Wordpress Websites.

== Description ==

Webforce Connect – Donation uses the most stable Salesforce package that hundles donation payments 
(NPSP , ProntoPayments and ProtoGiving ).Using Webforce Connect - Base, Webforce Connect – Donation
easy accepts web donation and link it to Salesforce. With the donations made on web going to Salesforce
we can easily manage every ongoing donation coming in.  

== Installation ==

Upon installation we highly reccomend to view the official Webforce Connect – Donation documentation.

== Screenshots ==

Screenshots are included on the official Webforce Connect – Donation documentation

== Changelog ==

= 2.0 =
*Webforce Connect – Donation is now using oath2 on when connecting Salesforce.

== Arbitrary section ==

Webforce Connect – Donation needs to be with Webforce Connect - Base to connect to Salesforce.
The Salesforce organization used should have the following Salesforce Packages below:
* NPSP
* ProntoPayments
* ProntoGiving
* PPAPI
* Webforce Connect

== A brief Markdown Example ==

1. Caters multiple payment gateway like eWay, Ezidebit, Paydock and Paypal.
2. Secure web payment.
3. Full connection to Salesforce.
4. Easy to modify admin configurations.
5. Flexible donation form.

