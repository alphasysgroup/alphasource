<?php
/**
 *
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Webforce_Connect_Donation
 *
 *
 *
 * Plugin Name:       Webforce Connect - Donation
 *
 * Plugin URI:        https://alphasys.com.au
 *
 * Description:       Webforce Connect – Donation allows you to accept online donations on your WordPress Websites.
 *
 * Version: 2.4.2
 *
 * Author:            AlphaSys Pty. Ltd.
 *
 * Author URI:        https://alphasys.com.au
 *
 * License:           GPL-2.0+
 *
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * Text Domain:       webforce-connect-donation
 *
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {die;}


define( 'WFC_DONATION_VERSION', '2.4.2' );

define('WFC_PLUGIN_BASEDIR', plugin_dir_path(__FILE__));


function activate_webforce_connect_donation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webforce-connect-donation-activator.php';
	Webforce_Connect_Donation_Activator::activate();
	Webforce_Connect_Donation_Activator::init_wfc_donation_settings();
}


function deactivate_webforce_connect_donation() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-webforce-connect-donation-deactivator.php';
	Webforce_Connect_Donation_Deactivator::deactivate();
	
}

register_activation_hook( __FILE__, 'activate_webforce_connect_donation' );
register_deactivation_hook( __FILE__, 'deactivate_webforce_connect_donation' );



require plugin_dir_path( __FILE__ ) . 'includes/class-webforce-connect-donation.php';


function run_webforce_connect_donation() {

	$plugin = new Webforce_Connect_Donation();
	$plugin->run();

}
run_webforce_connect_donation();

require_once 'includes/class-webforce-connect-donation-plugin-prereq.php';
new Webforce_Connect_Donation_PluginPrereq(plugin_basename(__FILE__), array(
    'wfc-base' => array(
        'name' => 'Webforce Connect - Base',
        'class' => 'Pronto_Base',
        'version' => '1.1.0.2'
    )
));
