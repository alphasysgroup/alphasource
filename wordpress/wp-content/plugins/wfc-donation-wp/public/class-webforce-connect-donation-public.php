<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @since      1.0.0
 *
 * @package    Webforce_Connect_Donation
 * @author     Alphasys Pty. Ltd.
 */

class Webforce_Connect_Donation_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
     * The global settings of the plugin
     *
     * @since    3.0.0
     * @access   public
     * @var      string    $wfc_don_Settings    The global settings of the plugin
     */
    private $wfc_don_Settings = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $parent ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		/*
		* main class of the plugin
		*/		
		$this->class = $parent;

		$this->wfc_don_Settings = get_option('pronto_donation_settings', 0);

		/*
		* load payment modules
		*/			
		$gateway_used = array('eway','ezidebit', 'ech', 'paypal', 'stripe', 'nab', 'pin', 'securepay', 'paymentfile', 'westpac');
		$WFC_DonationPaymentGateway = new WFC_DonationPaymentGateway();
		$WFC_DonationPaymentGateway->wfc_donation_load_gatewaymodules($gateway_used, $parent);
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		
		wp_register_style(
            'webforce-connect-donation-public-css',
            plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-public.css', 
            array(), 
            $this->version, 
            'all' 
        );
        wp_register_style(
            'webforce-connect-donation-harmony-address-validation-css',
            plugin_dir_url(__FILE__) . 'css/pronto-donation-harmony-address-validation.css',
            array(),
            $this->version,
            'all'
        );

		// register FontAwesome CDN
		wp_register_style(
			'webforce-connect-donation-font-awesome',
			'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css',
			array(),
			$this->version,
			'all'
		);

		wp_register_style(
			'webforce-connect-donation-number-handler-css',
			plugin_dir_url(__FILE__) . 'css/webforce-connect-donation-number-handler-inc/intlTelInput.css',
			array(),
			$this->version,
			'all'
		);

		wp_register_style(
			'webforce-connect-donation-number-handler-credit-css',
			plugin_dir_url(__FILE__) . 'css/webforce-connect-donation-number-handler-inc/credit.css',
			array(),
			$this->version,
			'all'
		);

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_register_script( 
            $this->plugin_name, 
            plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-public.js', 
            array( 'jquery' ), 
            $this->version, 
            true
        );

		/*
		* Register the JS SCRIPT for donation form.
		*/
		wp_register_script( 
			'webforce-connect-donation-form-js', 
			plugin_dir_url( __FILE__ ) . '../includes/wfc-donation-inc/js/webforce-connect-donation-form.js', 
			array( 'jquery' ), 
			$this->version,
			true
		);

		/*
		* Register the JS SCRIPT for donation handler.
		*/
		wp_register_script( 
			'webforce-connect-donation-handler-js', 
			plugin_dir_url( __FILE__ ) . '../includes/wfc-donation-inc/js/webforce-connect-donation-handler.js', 
			array( 'jquery' ), 
			$this->version,
			true
		);

		/*
		* Register the JS SCRIPT for gateway handler.
		*/
		wp_register_script( 
			'webforce-connect-donation-gateway-handler-js', 
			plugin_dir_url( __FILE__ ) . '../includes/wfc-donation-inc/js/webforce-connect-gateway-handler.js', 
			array( 'jquery' ), 
			$this->version,
			true
		);		

        /*
        * Register the JS SCRIPT for google address validation.
        */
        wp_register_script( 
            'webforce-connect-donation-google-address-validation-js', 
            plugin_dir_url( __FILE__ ) . 'js/webforce-connect-google-address-validation.js', 
            array( 'jquery' ), 
            $this->version,
	        true
        );

		/*
		* Register the JS SCRIPT for harmony address validation.
		*/
        wp_register_script(
            'webforce-connect-donation-harmony',
            plugin_dir_url(__FILE__) . 'js/harmony-1.6.3.min.js',
            array('jquery'),
            $this->version,
	        true
        );  
        wp_register_script(
            'webforce-connect-donation-harmony-ui',
            plugin_dir_url(__FILE__) . 'js/harmony-ui-1.6.3.min.js',
            array('jquery'),
            $this->version,
	        true
        ); 
        wp_register_script( 
            'webforce-connect-donation-harmony-address-validation-js', 
            plugin_dir_url( __FILE__ ) . 'js/webforce-connect-harmony-address-validation.js', 
            array( 'jquery' ), 
            $this->version,
	        true
        ); 

        /*
         * Register the JS SCRIPT for credit card validation.
         */
		wp_register_script(
			'webforce-connect-donation-creditcard-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-creditcard.js',
			array( 'jquery' ),
			$this->version,
			true
		);


		/*
		 * Register the JS SCRIPT for Number handler.
		 */
		wp_register_script(
			'webforce-connect-donation-number-handler-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-number-handler.js',
			array( 'jquery' ),
			$this->version,
			true
		);
		wp_register_script(
			'webforce-connect-donation-number-handler-tel-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-number-handler-inc/intlTelInput.min.js',
			array( 'jquery' ),
			$this->version,
			true
		);
		wp_register_script(
			'webforce-connect-donation-number-handler-util-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-number-handler-inc/utils.js',
			array( 'jquery' ),
			$this->version,
			true
		);
		wp_register_script(
			'webforce-connect-donation-number-handler-card-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-number-handler-inc/jquery.creditCardValidator.js',
			array( 'jquery' ),
			$this->version,
			true
		);

		wp_register_script(
			'webforce-connect-geo-js',
			plugin_dir_url( __FILE__ ) . 'js/geo.js',
			array( 'jquery' ),
			$this->version,
			true
		);



		/*
         * plugin public localize script
         */
        wp_localize_script($this->plugin_name, 'pdwp_ajax', array(
            'url'        => admin_url('admin-ajax.php'),
            'plugin_url' => trailingslashit( plugin_dir_url( __FILE__ ) ),
            'nonce'      => wp_create_nonce('ajax-nonce'),
            'donation_dnsr_single' => ( isset( $this->wfc_don_Settings->donation_dnsr_single ) 
             && $this->wfc_don_Settings->donation_dnsr_single == 'true' ) ? 'true' : 'false',
            'donation_dnsr_recurring' => ( isset( $this->wfc_don_Settings->donation_dnsr_recurring ) 
             && $this->wfc_don_Settings->donation_dnsr_recurring == 'true' ) ? 'true' : 'false',
        ));

		/*
		 * plugin public loading bar
		 */
		wp_register_script(
			'webforce-connect-donation-loading-bar-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-loading-bar.js',
			array( 'jquery' ),
			$this->version,
			true
		);

		/*
		 * plugin public tinter
		 */
		wp_register_script(
			'webforce-connect-donation-tinter-js',
			plugin_dir_url( __FILE__ ) . 'js/webfroce-connect-donation-tinter.js',
			array( 'jquery' ),
			$this->version,
			true
		);

		wp_localize_script(
			'webforce-connect-donation-tinter-js',
			'wfc_don_tinter',
			array('plugin_dir' => plugin_dir_url(__FILE__))
		);

		/*
		 * register jquery ui
		 */
		wp_register_script(
			'webforce-connect-donation-jquery-ui-js',
			'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js',
			array('jquery'),
			$this->version,
			true
		);


		$user_browser = Saiyan_Utils::get_visitor_browser();
		if ($user_browser == 'MSIE' || $user_browser == 'Trident' || $user_browser == 'Edge') {
			wp_enqueue_script(
				'webforce-connect-donation-promise-auto',
				'https://cdnjs.cloudflare.com/ajax/libs/es6-promise/4.1.1/es6-promise.auto.min.js',
				array('jquery'),
				$this->version,
				true
			);
		}

		/*
		 * Registering script file for the wizard template
		 */
		wp_register_script(
		    'webforce-connect-donation-wizard-template-standard-js',
            plugin_dir_url(__FILE__) . 'js/webforce-connect-donation-wizard-template-standard.js',
            array('jquery'),
            $this->version,
            true
        );

        wp_register_script(
            'webforce-connect-donation-wizard-template-advance-js',
            plugin_dir_url(__FILE__) . 'js/webforce-connect-donation-wizard-template-advance.js',
            array('jquery'),
            $this->version,
            true
        );
	}
	
	
	/**
	 * This function will create shortcode for donation pages.
	 *
	 *
	 * @author Junjie Canonio
	 *
	 * @param $form
	 * @return string
	 * @since  3.0.0
	 *
	 * @LastUpdated   April 26, 2018
	 *
	 */
    public function wfc_donation_form($form){
	    global $wp;
	    $home_url_donation = home_url( $wp->request );

    	if (isset($form['id']) && !empty($form['id']) && strpos($home_url_donation, 'wp-json') == false){

		    /*
			* call siayantist utilities
			*/
		    Saiyan::styles_and_scripts('force');

		    /*
			* call siayantist toolkit
			*/
		    Saiyan_Toolkit::get_instance()->load_toolkits(array(
			    'tinter', 'spinner'
		    ));
	    }

	    /*
		* Get donation Salesforce Instance
		*/
	    $sfinstance = get_post_meta( $form['id'], 'donation_form_sfinstance', true );

		/*
		* enqueue_script
		*/
		wp_enqueue_script($this->plugin_name);
	    wp_enqueue_script('webforce-connect-donation-handler-js');
	    wp_enqueue_script('webforce-connect-donation-form-js');
	    wp_enqueue_script('webforce-connect-donation-gateway-handler-js');
	    wp_enqueue_script('webforce-connect-donation-creditcard-js');

	    wp_enqueue_script('webforce-connect-donation-number-handler-js');
	    wp_enqueue_script('webforce-connect-donation-number-handler-tel-js');
	    wp_enqueue_script('webforce-connect-donation-number-handler-util-js');
	    wp_enqueue_script('webforce-connect-donation-number-handler-card-js');

		wp_enqueue_script('webforce-connect-donation-loading-bar-js');
		wp_enqueue_script('webforce-connect-donation-tinter-js');

		wp_enqueue_script('webforce-connect-donation-jquery-ui-js');

		wp_enqueue_script('webforce-connect-geo-js');

	    WFC_DonationEncryptDecrypt::wfc_don_enqueue_styles_scripts();
	    WFC_DonationPaymentGateway::wfc_donation_load_gatewayscripts($form['id'],$sfinstance);
	

		/*
		* enqueue_style
		*/
        wp_enqueue_style('webforce-connect-donation-public-css');
		wp_enqueue_style('webforce-connect-donation-number-handler-css');


		/*
         * get donation form campaign
         */
    	$campaign_id = ( isset( $_GET['campaign_id'] ) && !empty( $_GET['campaign_id'] ) ) ? $_GET['campaign_id'] : '';
    	if(!empty($campaign_id)){

    		$form['campaign_id'] = $campaign_id;    		
    	}

        /*
         * get donation form info
         */
        $form_info = get_post($form['id']);

        /*
         * check if form/page id does not exist
         */
        if (null == $form_info
            || false === get_post_status($form['id'])
            || $form_info->post_type != 'pdwp_donation') {
            return "<p class='error' style='color:red;'>Donation form ID doesn't exist.</p>";
        }

        /*
         * get form session cache
         */
        $cacheasessionid = isset($_GET['e']) ? $_GET['e'] : '';
        $session_cache = get_transient('pdwp_form_cache_' . $cacheasessionid);

	    /*
	     * delete paypal redirect cache form
	     */
	    // if(isset($session_cache['paypal_redirect']) && $session_cache['paypal_redirect'] == 'true'){
	    // 	if (isset($session_cache['post_meta_id'])) {
	    // 		$this->class->wfc_donation_data(array($session_cache['post_meta_id']), 'delete');
	    // 	}
	    // }


	    /*
        * Hidden fields
        */
        global $post;
        $WFC_DonationForms = new WFC_DonationForms();

        $page_ID = isset($post->ID) ? $post->ID : 0;
    	$setting = get_option( 'pronto_donation_settings', 0 );

		$form_currency = get_post_meta( $form['id'], 'donation_cc_currency', true );
		$currency_symbol = !empty( $form_currency ) ? $WFC_DonationForms->wfc_get_currencySymbol( $form_currency ) : '';

		$enable_validation 	= isset( $setting->enable_validation ) ? $setting->enable_validation : 'false';
		$google_api_key = isset( $setting->google_api_key ) ? $setting->google_api_key : '';
	    $google_prioritized_country = isset( $setting->google_prioritized_country ) ? $setting->google_prioritized_country : '';
	    $register_custom_set_address = isset( $setting->register_custom_set_address ) ? $setting->register_custom_set_address : '[]';

		$harmony_rapid_displaymode = isset( $setting->harmony_rapid_displaymode ) ? $setting->harmony_rapid_displaymode : 'detailcardmode';
		$harmony_overridablefields = isset( $setting->harmony_overridablefields ) ? $setting->harmony_overridablefields : 'false';
		$harmony_rapid_sourceoftruth = isset( $setting->harmony_rapid_sourceoftruth ) ? $setting->harmony_rapid_sourceoftruth : 'AUPAF';
		$harmony_rapid_username = isset( $setting->harmony_rapid_username ) ? $setting->harmony_rapid_username : '';
		$harmony_rapid_password = isset( $setting->harmony_rapid_password ) ? $setting->harmony_rapid_password : '';

        if($enable_validation == 'true'){
            if ($google_api_key != '') {
                wp_enqueue_script(
                    'webforce-connect-donation-google-address-validation-lib',
                    'https://maps.googleapis.com/maps/api/js?key=' . $google_api_key . '&language=en&libraries=places',
                    true
                );
                wp_enqueue_script('webforce-connect-donation-google-address-validation-js');
            }
        }else{
        	if(!empty($harmony_rapid_username) && !empty($harmony_rapid_password)){
	            wp_enqueue_style( 'webforce-connect-donation-harmony-address-validation-css' );
	            wp_enqueue_script(
	                'webforce-connect-donation-harmony-jquery-ui',
	                'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js',
	                array('jquery'),
	                $this->version,
	                false
	            );
	            wp_enqueue_script('webforce-connect-donation-harmony');
	            wp_enqueue_script('webforce-connect-donation-harmony-ui');
	            wp_enqueue_script('webforce-connect-donation-harmony-address-validation-js');
        	}
        }
        


		$role = current_user_can('administrator');

		$padding  = get_post_meta( $form['id'], 'donation_form_btn_padding', true );
		$fontsize = get_post_meta( $form['id'], 'donation_form_btn_font_size', true );
		$spinner_bg_size = 'background-size:'.$fontsize.'px '.$fontsize.'px;';
		$spinner_width   = 'width:'.$fontsize.'px;';
		$spinner_height  = 'height:'.$fontsize.'px;';
		$spinner_margin  = 'margin-top:'.$padding.'px;';

		/*
         * get donation form campaign source
         */
		if (isset($_GET['campaign_source']) && !empty($_GET['campaign_source'])) {
			$form['campaign_id'] = $_GET['campaign_source'];
		}else{
			if (empty($form['campaign_id'])) {
				$form['campaign_id'] = trim( get_post_meta( $form['id'], 'donation_form_sf_campaign_'.$sfinstance, true ) );
			}
		}

        $hidden = array(
        	'sfinstance' => $sfinstance,
			'p' => $page_ID,
			'nonce' => wp_create_nonce( 'donation' ),
			'currency' => $currency_symbol .','. $form_currency,
			'campaignId' => $form['campaign_id'],
			'gauId' => trim( get_post_meta( $form['id'], 'donation_form_sf_gau_'.$sfinstance, true ) ),
			'unique_token' => md5( uniqid( "", true ) ),
			'donation_campaign' => $form['id'],
			'donation_formtitle' => get_the_title($form['id']),
			'donation_formfilter' => get_post_meta( $form['id'], 'donation_form_filter', true ),
			'campaignSessionId' => uniqid(),
			'thankyou_url' => get_post_meta( $form['id'], 'donation_form_thankyou_url', true ),
			'enable_validation' => $enable_validation,
			'google_api_key' => $google_api_key,
	        'google_prioritized_country' => $google_prioritized_country,
	        'register_custom_set_address' => $register_custom_set_address,
			'harmony_rapid_displaymode' => $harmony_rapid_displaymode,
			'harmony_overridablefields' => $harmony_overridablefields,
			'harmony_rapid_sourceoftruth' => $harmony_rapid_sourceoftruth,
			'harmony_rapid_username' => $harmony_rapid_username,
			'harmony_rapid_password' => $harmony_rapid_password,
			'field_type' => ( isset( $setting->donation_debug_mode ) && $setting->donation_debug_mode == 'true' && $role ) ? 'text' : 'hidden',
			'formstyle' => trim( get_post_meta( $form['id'], 'donation_form_template_style', true ) ),
		//	'form_userrequired_fields' => $this->_getRequiredFields($form['id']),
			'spinnerstyle' => $spinner_bg_size.$spinner_width.$spinner_height.$spinner_margin,
			'wfc_cartdonationtypes' => isset( $session_cache['wfc_cartdonationtypes'] ) ? $session_cache['wfc_cartdonationtypes'] : '',
			'wfc_carttype' => isset( $session_cache['wfc_carttype'] ) ? $session_cache['wfc_carttype'] : 'not_cart',
			'pdwp_sub_amt' => isset( $session_cache['pdwp_sub_amt'] ) ? $session_cache['pdwp_sub_amt'] : 0,
            'template_color' => get_post_meta($form['id'], 'donation_form_btn_color', true),
		);


        /*
        * Gateway
        */
        $WFC_DonationDataHandler = new WFC_DonationDataHandler();
		$get_gateway_config = $WFC_DonationDataHandler->get_gateway_config($form['id'],$sfinstance);
        $get_amount_config = $WFC_DonationDataHandler->get_amount_config($form['id']);
        $get_donationtypes = $WFC_DonationDataHandler->get_donationtypes($form['id']);
        $get_paymentdetials_config = $WFC_DonationDataHandler->get_paymentdetials_config($form['id']);
        $get_formstyle_config = $WFC_DonationDataHandler->get_formstyle_config($form['id']);
        $get_tributefield_config = $WFC_DonationDataHandler->get_tributefield_config($form['id']);
        $get_failuremessage_config = $WFC_DonationDataHandler->get_failuremessage_config($form['id']);





        /*
         * custom url donation type
         */
        $url_dontype = array(
        	'type' => (isset($_GET['type']) && !empty($_GET['type'])) ? $_GET['type'] : '',
        );	

        /*
         * custom url amount
         */
        $url_amount = array(
        	'amt' => (isset($_GET['amt']) && !empty($_GET['amt'])) ? round( preg_replace("/[^0-9.]/", "", $_GET['amt']) ) : '',
        	'otr' => (isset($_GET['otr']) && !empty($_GET['otr'])) ? round( preg_replace("/[^0-9.]/", "", $_GET['otr']) ) : '',
        );

  
        /*
         * necessary parameters for template
         */
        $form_body = array(
            'configs' => array(
            	'url_dontype' => $url_dontype,
            	'url_amount' => $url_amount,
                'formId' => $form['id'], /* donation form id */
                'cache' => $session_cache,
                'settings' => $this->wfc_don_Settings,
				'hidden'   => $hidden,
				'gateway_config'  => $get_gateway_config,
                'amount_config'   => $get_amount_config,
                'donationtypes_config'   => $get_donationtypes,
                'paymentdetials_config'   => $get_paymentdetials_config,
                'formstyle_config'        => $get_formstyle_config,
                'tributefield_config'     => $get_tributefield_config,
                'failuremessage_config'   => $get_failuremessage_config,
				'template_btn_padding'    => get_post_meta($form['id'], 'donation_form_btn_padding', true),
				'template_btn_font_size'  => get_post_meta($form['id'], 'donation_form_btn_font_size', true),
				'template_btn_width'      => get_post_meta($form['id'], 'donation_form_btn_width', true),
				'template_btn_font_color' => get_post_meta($form['id'], 'donation_form_btn_font_color', true),
				'template_color'          => get_post_meta($form['id'], 'donation_form_btn_color', true),
            ),
           // 'view'                    => new Render_Donate_Field( $this->class, $form ), /* initialize class */

        );

        /*
         * template
         */
        $template_mode = get_post_meta($form['id'], 'donation_form_template_mode', true);
        if ($template_mode == 'wizard') {
            $template = get_post_meta($form['id'], 'donation_form_template_style2', true);
        } else {
            $template = get_post_meta($form['id'], 'donation_form_template_style', true);
        }

 
        /*
         * render default template
         */
        $tmpl = $this->class->pdwp_view_tmpl( $template, $form_body );
        if ($tmpl == false) {
        	$default_template = plugin_dir_path(__FILE__).'templates/normal/donation-template.php';
        	$tmpl = $this->class->pdwp_view_tmpl( $default_template, $form_body );
        }


        return '<div id="pronto-donation-wplugin"> ' . $tmpl . ' </div>';
    }

    /**
     * Ajax callback that validates and sanitize donation page user inputs.
     *
     * @author Junjie Canonio
     * @since    3.0.0
     *
     * @LastUpdated  April 30, 2018
     */
    public function wfc_clean_and_validate(){
        if (isset($_POST['action']) && $_POST['action'] == 'clean_validate_donor') {

            if (!wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
                die('busted');
            }

            /*
	         * call all custom begin filter
	         */
	        if (has_filter('wfc_donation_begin') == true) {
	            $custom_donation_begin_filter = apply_filters('wfc_donation_begin', $_POST);
	            $_POST = $custom_donation_begin_filter;
	        }

	        /*
	         * call all custom begin action
	         */
	        do_action( 'wfc_donation_begin_act', array(
		        'post' => $_POST,
		        'main_class' => $this->class
	        ));

            /*
             * clean data string
             */
            $WFC_DonationDataHandler = new WFC_DonationDataHandler();
            $post = $WFC_DonationDataHandler->clean_form_data($_POST);


            $field_error = $WFC_DonationDataHandler->validate_form_data($post);
            $errors = isset($field_error['errors']) ? $field_error['errors'] : array();

            WFC_DonationFormRenderer::set_error($errors, $post);

            if (!empty($errors)) {
                /*
                 * error occured
                 */
                if ((isset($post['campaign_sourcemode']) && $post['campaign_sourcemode'] == 'CampaignSourceMode') &&
                	isset($post['campaignId']) && !empty($post['campaignId'])) {
                	$url = add_query_arg(
	                	array(
		                    'campaign_source' => $post['campaignId'],
		                    'e' => $post['campaignSessionId'],
		                    'p' => $post['p'],
		                ),
	                    get_home_url()
	                );

	                /*
			         * call all edit error url filter
			         */
	                if (has_filter('wfc_donation_errorurl_flt') == true) {
		                $wfc_donation_errorurl_flt = apply_filters('wfc_donation_errorurl_flt', $url, $post);
		                if (!empty($wfc_donation_errorurl_flt) && filter_var($wfc_donation_errorurl_flt, FILTER_VALIDATE_URL)) {
			                $url = $wfc_donation_errorurl_flt;
		                }
	                }
                }else{
	                $url = add_query_arg(
	                	array(
		                    'e' => $post['campaignSessionId'],
		                    'p' => $post['p'],
		                ),
	                    get_home_url()
	                );

	                /*
			         * call all edit error url filter
			         */
	                if (has_filter('wfc_donation_errorurl_flt') == true) {
		                $wfc_donation_errorurl_flt = apply_filters('wfc_donation_errorurl_flt', $url, $post);
		                if (!empty($wfc_donation_errorurl_flt) && filter_var($wfc_donation_errorurl_flt, FILTER_VALIDATE_URL)) {
			                $url = $wfc_donation_errorurl_flt;
		                }
	                }
                }

	            $WFC_DonationDataHandler = new WFC_DonationDataHandler();
	            $failuremessage_config = $WFC_DonationDataHandler->get_failuremessage_config($post['donation_campaign']);


	            $donation_form_override_failure_message = isset($failuremessage_config['donation_form_override_failure_message']) ? $failuremessage_config['donation_form_override_failure_message'] : 'false';
	            $donation_form_override_message_type = isset($failuremessage_config['donation_form_override_message_type']) ? $failuremessage_config['donation_form_override_message_type'] : 'plain_text';
	            $donation_form_override_message = isset($failuremessage_config['donation_form_override_message']) ? $failuremessage_config['donation_form_override_message'] :'There has been an issue with processing your donation. Please contact our administrator.';

	            if(isset($failuremessage_config[donation_form_error_message_behavior]) && $failuremessage_config[donation_form_error_message_behavior] == 'popup'){
		            $isPopupErrorMessage = true;
	            }else {
		            $isPopupErrorMessage = false;
	            }

	            $form_error = array();
	            $campaignSessionId = isset($post['campaignSessionId']) ? $post['campaignSessionId'] : '';
	            if($isPopupErrorMessage){
		            $form_error = get_transient('pdwp_errors' . $campaignSessionId);
		            $form_error = isset($form_error['form_error']) ? $form_error['form_error'] : array();
	            }


	            wp_send_json(array(
		            'ResponseFrom' => 'wfc_clean_and_validate',
		            'status' => 'failed',
		            'url'    => $url,
		            'post' => $post,
		            'isPopupErrorMessage' => $isPopupErrorMessage,
		            'campaignSessionId' => $campaignSessionId,
		            'form_error' => $form_error,
		            'override_failure_message' => $donation_form_override_failure_message,
		            'override_message_type' => $donation_form_override_message_type,
		            'override_message' => $donation_form_override_message
	            ));

            }else{
            	$gateway = get_option($post['gatewayid']);
            	$post['wfc_donation_tokenize'] = 
            	isset($gateway['wfc_donation_tokenize']) ? $gateway['wfc_donation_tokenize'] : 'false';

            	$post['timestamp']    = time();
                $post['status']       = 'pending';

                $post['post_meta_id'] = add_post_meta( 
                    $post['donation_campaign'], 
                    'pronto_donation_donor', 
                    stripslashes_deep( $post ) 
                );

                $post['pamynet_ref']  = $post['timestamp'] . '-' . mt_rand() . '-' . $post['post_meta_id'];
            	
                /*
                * WFC donation logs
                */
                $logs['validateDetails']['form'] =  $post;
                $post['donation_logs_id'] = wp_insert_post(
                    array(
                        'post_title'=>$post['pamynet_ref'], 
                        'post_type'=>'wfc_donation_logs', 
                        'post_content'=> serialize($logs),
                        'post_date' => date('Y-m-d H:i:s'),
                        'post_author' => 2,
                        'post_status' => 'publish',
                        'comment_status' => 'open',
                        'ping_status' => 'open'
                    )
                );

	            /*
				 * call all custom after form validation action hook
				 */
	            do_action( 'wfc_donation_after_form_validation_act', array(
		            'post' => $post,
		            'main_class' => $this->class
	            ));

	            wp_send_json(array(
		            'ResponseFrom' => 'wfc_clean_and_validate',
	                'status'   => 'success',
	                'donation' => $post,
	            ));
            }
        }
    }

    /**
     * Ajax callback that updates donation data during donation payment process.
     *
     * @author Junjie Canonio
     * @since    3.0.0
     *
     * @LastUpdated  May 30, 2018
     */
    public function wfc_donation_data()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'wfc_donation_data') {

            if (!wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
                die('busted');
            }

            $post = stripslashes_deep( $_POST['param']['donation'] );
            $data = $this->class->wfc_donation_data(
                array(
                    serialize( $post ),
                    $post['post_meta_id'],
                ),
                'update'
            );

            /*
	         * call all custom after_transaction filter
	         */
	        if (has_filter('wfc_donation_after_transaction_flt') == true) {
	            $custom_donation_begin_filter = apply_filters('wfc_donation_after_transaction_flt', $_POST);
	            $_POST = $custom_donation_begin_filter;
	        }

	        /*
             * call all custom after_transaction action
             */
            do_action( 'wfc_donation_after_transaction_act', array(
                'post' => $_POST,
                'main_class' => $this->class
            ));

            /*
            * WFC donation logs
            */
            $post = stripslashes_deep( $_POST['param']['donation'] );
            $post['form_userrequired_fields'] = '';
            $donation_logs_id = isset($post['donation_logs_id']) ? $post['donation_logs_id'] : 0;
            $response = isset($_POST['param']['response']) ? $_POST['param']['response'] : '';
            if($donation_logs_id != 0){
                $content_post = get_post($donation_logs_id);
                $logs = $content_post->post_content;
                $logs = maybe_unserialize($logs);
                if(!isset($logs['donationData']['payment_response'])){
                    $logs['donationData']['form'] = $post;
                    $logs['donationData']['payment_response'] = $response;

                    update_post_meta( $donation_logs_id, 'donationData1', serialize($logs) );
                }else{
                    $logs['donationData']['form2'] = $post;
                    update_post_meta( $donation_logs_id, 'donationData2', serialize($logs) );
                }
            }


            if ($data !== false) {
                wp_send_json(array(
	                'ResponseFrom' => 'wfc_donation_data',
                    'status'   => 'success',
                    'donation' => $post,
                ));
            } else {
                wp_send_json(array(
	                'ResponseFrom' => 'wfc_donation_data',
                    'status'   => 'failed',
                    'donation' => $post,
                ));
            }
        }
    }
	
	/**
	 * The Ajax callback function responsible for updating the donation Donor details during donation payment process.
	 *
	 * @author   Junjie Canonio
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 4, 2018
	 */
    public function wfc_sync_donation_data(){
        if (isset($_POST['action']) && $_POST['action'] == 'sync_donation_data') {

            if (!wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
                die('busted');
            }

            $post = stripslashes_deep($_POST['param']['donation']);
            $payment_source = stripslashes_deep($_POST['getPaymentSource']);
	        $donation_sfinstance = isset($post['donation_sfinstance']) ? $post['donation_sfinstance'] : '';

	           
           	$pdwp_donation_type = isset($post['pdwp_donation_type']) ? $post['pdwp_donation_type'] : 'one-off';

            if(isset($post['wfc_carttype']) && $post['wfc_carttype'] == 'advance_cart') {
            	// Get Donor short details
	            $donor_first_name = isset($post['donor_first_name']) ? $post['donor_first_name'] : '';
	            $donor_last_name = isset($post['donor_last_name']) ? $post['donor_last_name'] : '';
	            $donor_email = isset($post['donor_email']) ? $post['donor_email'] : '';
	            $donor_title = isset( $post['donor_title'] ) ? ucfirst( $post['donor_title'] ) : '';
	            $donor_address = isset($post['donor_address']) ? $post['donor_address'] : '';
	            $donor_suburb = isset($post['donor_suburb']) ? $post['donor_suburb'] : '';
	            $donor_state = isset($post['donor_state']) ? $post['donor_state'] : '';
	            $donor_postcode = isset($post['donor_postcode']) ? $post['donor_postcode'] : '';
	            $donor_country = isset($post['donor_country']) ? $post['donor_country'] : '';
	            $donor_newsletter = ( isset( $post['donor_newsletter'] ) && !empty( $post['donor_newsletter'] ) ) ? true : false;

	            // Get Gateway ID

	   //          if( isset( $post['gatewayid'] ) && !empty( $post['gatewayid'] ) ) {
				// 	$gate = explode( '_' , $post['gatewayid'] );
				// 	$gatewayid = $gate[1];
				// } else {
				// 	$gatewayid = NULL;
				// }

				if( isset( $post['gatewayid'] ) && !empty( $post['gatewayid'] ) ) {
					$post_gatewayid = str_replace($donation_sfinstance.'_',"",$post['gatewayid']);
					$gate = explode( '_' , $post_gatewayid );
					$gatewayid = $gate[1];
				} else {
					$gatewayid = NULL;
				}

				$ccname = isset($payment_source['ccname']) ? $payment_source['ccname'] : '';
				$ccno = isset($payment_source['ccno']) ? $payment_source['ccno'] : '';
				$expmonth = isset($payment_source['expmonth']) ? $payment_source['expmonth'] : '';
				$expyear = isset($payment_source['expyear']) ? $payment_source['expyear'] : '';
				$ccv = isset($payment_source['ccv']) ? $payment_source['ccv'] : '';

				$bsbaccount = isset($payment_source['bsbaccount']) ? $payment_source['bsbaccount'] : '';
				$bsbname = isset($payment_source['bsbname']) ? $payment_source['bsbname'] : '';
				$bsbno = isset($payment_source['bsbno']) ? $payment_source['bsbno'] : '';

				$token = isset($payment_source['token']) ? $payment_source['token'] : '';
				$type = isset($payment_source['type']) ? $payment_source['type'] : '';

				if ($type == 'cc') {
					$request_payment_source = array(
		    			'ccname' => filter_var( $ccname, FILTER_SANITIZE_STRING),
		    			'ccno' => filter_var( $ccno, FILTER_SANITIZE_STRING),
		    			'expmonth' => filter_var( $expmonth, FILTER_SANITIZE_STRING),
		    			'expyear' => filter_var( $expyear, FILTER_SANITIZE_STRING),
		    			'ccv' => filter_var( $ccv, FILTER_SANITIZE_STRING),
		    			'token' => filter_var( $token, FILTER_SANITIZE_STRING),
		    			'type' => filter_var( $type, FILTER_SANITIZE_STRING)
		    		);
		    		$request_payment_source_clean = array(
		    			'ccname' => filter_var( $ccname, FILTER_SANITIZE_STRING),
		    			'ccno' => '************'.substr(filter_var( $ccno, FILTER_SANITIZE_STRING),12),
		    			'expmonth' => filter_var( $expmonth, FILTER_SANITIZE_STRING),
		    			'expyear' => filter_var( $expyear, FILTER_SANITIZE_STRING),
		    			'ccv' => str_repeat('*', strlen(filter_var( $ccv, FILTER_SANITIZE_STRING))),
		    			'token' => filter_var( $token, FILTER_SANITIZE_STRING),
		    			'type' => filter_var( $type, FILTER_SANITIZE_STRING),
		    		);
				}elseif($type == 'bank') {
					$request_payment_source = array(
		    			'bsbname' => filter_var( $bsbname, FILTER_SANITIZE_STRING),
		    			'bsbaccount' => filter_var( $bsbaccount, FILTER_SANITIZE_STRING),
		    			'bsbno' => filter_var( $bsbno, FILTER_SANITIZE_STRING),
		    			'type' => filter_var( $type, FILTER_SANITIZE_STRING),
		    		);
		    		$request_payment_source_clean = array(
		    			'bsbname' => filter_var( $bsbname, FILTER_SANITIZE_STRING),
		    			'bsbaccount' => '*****'.substr( filter_var( $bsbaccount, FILTER_SANITIZE_STRING),5),
		    			'bsbno' => '**'.substr(filter_var( $bsbno, FILTER_SANITIZE_STRING),2),
		    			'type' => filter_var( $type, FILTER_SANITIZE_STRING)
		    		);
				}else {
					$request_payment_source = array();
					$request_payment_source_clean = array();
				}


	    		if (isset($request_payment_source['token']) && empty($request_payment_source['token'])) {
	    			unset($request_payment_source['token']);
	    		}
				
	            $request_data['strPS'] = array(
		            'firstname' => filter_var( $donor_first_name, FILTER_SANITIZE_STRING),
		            'lastname' => filter_var( $donor_last_name, FILTER_SANITIZE_STRING),
		            'email' => filter_var( $donor_email, FILTER_SANITIZE_STRING),
		            'salutation' => filter_var( $donor_title, FILTER_SANITIZE_STRING),
		            'gatewayId' => filter_var( $gatewayid, FILTER_SANITIZE_STRING),
		            "street" => filter_var( $donor_address, FILTER_SANITIZE_STRING),
		            "city" => filter_var( $donor_suburb, FILTER_SANITIZE_STRING),
		            "state" => filter_var( $donor_state, FILTER_SANITIZE_STRING),
		            "postcode" => filter_var( $donor_postcode, FILTER_SANITIZE_STRING),
		            "country" => filter_var( $donor_country, FILTER_SANITIZE_STRING),
		            "newsletter" => $donor_newsletter,
		            'payment_source' => $request_payment_source,
		    	);
		    	
	            $result_data = WFC_SalesforcePPAPI::sync_paymentsource($request_data,$donation_sfinstance);

	            $request_data['strPS']['payment_source'] = $request_payment_source_clean;
		    	$post['SF_PaymentSource']['request'] = $request_data;
	            $post['SF_PaymentSource']['result'] = $result_data;

	            $post['SF_PS_paymentSourceId'] = 
	            isset($result_data['sf_response']['paymentSourceId']) ?
	            $result_data['sf_response']['paymentSourceId'] : '';

	            $post['SF_PS_contactId'] =
	            isset($result_data['sf_response']['contactId']) ?
	            $result_data['sf_response']['contactId'] : '';

	            $data = $this->class->wfc_donation_data(
	                array(
	                    serialize( $post ),
	                    $post['post_meta_id'],
	                ),
	                'update'
	            );	


	            // call sync advance cart event
            	$syncadvancecartdonation_arr = array('donation_id' => $post['post_meta_id']);

            	$event_schedule = time() + 5;
				wp_clear_scheduled_hook(
					'wfc_don_event_syncadvancecartdonation', 
					array($syncadvancecartdonation_arr)
				);
			 	wp_schedule_single_event( 
			 		$event_schedule, 
			 		'wfc_don_event_syncadvancecartdonation', 
			 		array($syncadvancecartdonation_arr)
			 	);

	            wp_send_json(array(
		            'ResponseFrom' => 'wfc_sync_donation_data',
	                'statussync'   => 'success',
	                'donation' => $post,
	                'event_schedule' => $event_schedule,
	            ));           	
            }else{
	            if( $pdwp_donation_type == 'one-off'){

	                $event_schedule = time() + 5;
					wp_clear_scheduled_hook(
						'wfc_donation_syncsingledonation', 
						array(
							array('post_meta_id' => $post['post_meta_id'])
						)
					);
	                wp_schedule_single_event( 
	                	$event_schedule, 
	                	'wfc_donation_syncsingledonation', 
						array(
							array('post_meta_id' => $post['post_meta_id'])
						)
	                );

	                wp_send_json(array(
		                'ResponseFrom' => 'wfc_sync_donation_data',
	                    'statussync'   => 'success',
	                    'donation' => $post,
	                    'event_schedule' => $event_schedule,
	                ));                
	            }else{

	            	// Get Donor short details
		            $donor_first_name = isset($post['donor_first_name']) ? $post['donor_first_name'] : '';
		            $donor_last_name = isset($post['donor_last_name']) ? $post['donor_last_name'] : '';
		            $donor_email = isset($post['donor_email']) ? $post['donor_email'] : '';
		            $donor_title = isset( $post['donor_title'] ) ? ucfirst( $post['donor_title'] ) : '';
		            $donor_address = isset($post['donor_address']) ? $post['donor_address'] : '';
		            $donor_suburb = isset($post['donor_suburb']) ? $post['donor_suburb'] : '';
		            $donor_state = isset($post['donor_state']) ? $post['donor_state'] : '';
		            $donor_postcode = isset($post['donor_postcode']) ? $post['donor_postcode'] : '';
		            $donor_country = isset($post['donor_country']) ? $post['donor_country'] : '';
		            $donor_newsletter = ( isset( $post['donor_newsletter'] ) && !empty( $post['donor_newsletter'] ) ) ? true : false;

		            // Get Gateway ID

		   //          if( isset( $post['gatewayid'] ) && !empty( $post['gatewayid'] ) ) {
					// 	$gate = explode( '_' , $post['gatewayid'] );
					// 	$gatewayid = $gate[1];
					// } else {
					// 	$gatewayid = NULL;
					// }

					if( isset( $post['gatewayid'] ) && !empty( $post['gatewayid'] ) ) {
						$post_gatewayid = str_replace($donation_sfinstance.'_',"",$post['gatewayid']);
						$gate = explode( '_' , $post_gatewayid );
						$gatewayid = $gate[1];
					} else {
						$gatewayid = NULL;
					}


					$ccname = isset($payment_source['ccname']) ? $payment_source['ccname'] : '';
					$ccno = isset($payment_source['ccno']) ? $payment_source['ccno'] : '';
					$expmonth = isset($payment_source['expmonth']) ? $payment_source['expmonth'] : '';
					$expyear = isset($payment_source['expyear']) ? $payment_source['expyear'] : '';
					$ccv = isset($payment_source['ccv']) ? $payment_source['ccv'] : '';

					$bsbaccount = isset($payment_source['bsbaccount']) ? $payment_source['bsbaccount'] : '';
					$bsbname = isset($payment_source['bsbname']) ? $payment_source['bsbname'] : '';
					$bsbno = isset($payment_source['bsbno']) ? $payment_source['bsbno'] : '';

					$token = isset($payment_source['token']) ? $payment_source['token'] : '';
					$type = isset($payment_source['type']) ? $payment_source['type'] : '';

		            if ($type == 'cc') {
			            $request_payment_source = array(
				            'ccname' => filter_var( $ccname, FILTER_SANITIZE_STRING),
				            'ccno' => filter_var( $ccno, FILTER_SANITIZE_STRING),
				            'expmonth' => filter_var( $expmonth, FILTER_SANITIZE_STRING),
				            'expyear' => filter_var( $expyear, FILTER_SANITIZE_STRING),
				            'ccv' => filter_var( $ccv, FILTER_SANITIZE_STRING),
				            'token' => filter_var( $token, FILTER_SANITIZE_STRING),
				            'type' => filter_var( $type, FILTER_SANITIZE_STRING)
			            );
			            $request_payment_source_clean = array(
				            'ccname' => filter_var( $ccname, FILTER_SANITIZE_STRING),
				            'ccno' => '************'.substr(filter_var( $ccno, FILTER_SANITIZE_STRING),12),
				            'expmonth' => filter_var( $expmonth, FILTER_SANITIZE_STRING),
				            'expyear' => filter_var( $expyear, FILTER_SANITIZE_STRING),
				            'ccv' => str_repeat('*', strlen(filter_var( $ccv, FILTER_SANITIZE_STRING))),
				            'token' => filter_var( $token, FILTER_SANITIZE_STRING),
				            'type' => filter_var( $type, FILTER_SANITIZE_STRING),
			            );
		            }elseif($type == 'bank') {
			            $request_payment_source = array(
				            'bsbname' => filter_var( $bsbname, FILTER_SANITIZE_STRING),
				            'bsbaccount' => filter_var( $bsbaccount, FILTER_SANITIZE_STRING),
				            'bsbno' => filter_var( $bsbno, FILTER_SANITIZE_STRING),
				            'type' => filter_var( $type, FILTER_SANITIZE_STRING),
			            );
			            $request_payment_source_clean = array(
				            'bsbname' => filter_var( $bsbname, FILTER_SANITIZE_STRING),
				            'bsbaccount' => '*****'.substr( filter_var( $bsbaccount, FILTER_SANITIZE_STRING),5),
				            'bsbno' => '**'.substr(filter_var( $bsbno, FILTER_SANITIZE_STRING),2),
				            'type' => filter_var( $type, FILTER_SANITIZE_STRING)
			            );
		            }else {
						$request_payment_source = array();
						$request_payment_source_clean = array();
					}


		    		if (isset($request_payment_source['token']) && empty($request_payment_source['token'])) {
		    			unset($request_payment_source['token']);
		    		}
					
		            $request_data['strPS'] = array(
			            'firstname' => filter_var( $donor_first_name, FILTER_SANITIZE_STRING),
			            'lastname' => filter_var( $donor_last_name, FILTER_SANITIZE_STRING),
			            'email' => filter_var( $donor_email, FILTER_SANITIZE_STRING),
			            'salutation' => filter_var( $donor_title, FILTER_SANITIZE_STRING),
			            'gatewayId' => filter_var( $gatewayid, FILTER_SANITIZE_STRING),
			            "street" => filter_var( $donor_address, FILTER_SANITIZE_STRING),
			            "city" => filter_var( $donor_suburb, FILTER_SANITIZE_STRING),
			            "state" => filter_var( $donor_state, FILTER_SANITIZE_STRING),
			            "postcode" => filter_var( $donor_postcode, FILTER_SANITIZE_STRING),
			            "country" => filter_var( $donor_country, FILTER_SANITIZE_STRING),
			            "newsletter" => $donor_newsletter,
			            'payment_source' => $request_payment_source,
			    	);
			    	
		            $result_data = WFC_SalesforcePPAPI::sync_paymentsource($request_data,$donation_sfinstance);

		            $request_data['strPS']['payment_source'] = $request_payment_source_clean;
			    	$post['SF_PaymentSource']['request'] = $request_data;
		            $post['SF_PaymentSource']['result'] = $result_data;

		            $post['SF_PS_paymentSourceId'] = 
		            isset($result_data['sf_response']['paymentSourceId']) ?
		            $result_data['sf_response']['paymentSourceId'] : '';

		            $post['SF_PS_contactId'] =
		            isset($result_data['sf_response']['contactId']) ?
		            $result_data['sf_response']['contactId'] : '';

		            $data = $this->class->wfc_donation_data(
		                array(
		                    serialize( $post ),
		                    $post['post_meta_id'],
		                ),
		                'update'
		            );

		            $event_schedule = time() + 5;
					wp_clear_scheduled_hook( 
						'wfc_donation_syncrecurringdonation', 
	                	array(
	                		array('post_meta_id' => $post['post_meta_id'])
	                	) 
	                );
	                wp_schedule_single_event( 
	                	$event_schedule, 
	                	'wfc_donation_syncrecurringdonation', 
	                	array(
	                		array('post_meta_id' => $post['post_meta_id'])
	                	) 
	                );


                    wp_send_json(
                    	array(
		                    'ResponseFrom' => 'wfc_sync_donation_data',
	                        'statussync'   => 'success',
	                        'donation' => $post,
	                        'event_schedule' => $event_schedule,
                    	)
                    );
		           
	            }
            }

        }
    }
	
	/**
	 * The Ajax callback function that will reset donation form and display message if error occurs.
	 *
	 * @author   Junjie Canonio
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 9, 2018
	 */
    public function wfc_reset_form()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'wfc_reset_form') {

            if (!wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
                die('busted');
            }

            $post = $_POST['param']['donation'];
            
            if(isset($_POST['param']['response']) && !empty($_POST['param']['response'])){
                if(is_array($_POST['param']['response'])){
                    foreach ($_POST['param']['response'] as $key => $value) {
                        $field_error[] = $value;
                    }
                }else{
                   $field_error[] = isset($_POST['param']['response']) ? $_POST['param']['response'] : '';
                }
                WFC_DonationFormRenderer::set_error($field_error, $post);
            }

            $this->class->wfc_donation_data(array($post['post_meta_id']), 'update');

            /*
            * WFC donation logs
            */
            $donation_logs_id = isset($post['donation_logs_id']) ? $post['donation_logs_id'] : 0;
            $response = isset($_POST['param']['response']) ? $_POST['param']['response'] : '';
            if($donation_logs_id != 0){
                $content_post = get_post($donation_logs_id);
                $logs = $content_post->post_content;
                $logs = maybe_unserialize($logs);
                $logs['resetForm']['form'] = $post;
                $logs['resetForm']['response'] = $response;
                update_post_meta( $donation_logs_id, 'resetForm', serialize($logs) );
            }


            // error
            if ((isset($post['campaign_sourcemode']) && $post['campaign_sourcemode'] == 'CampaignSourceMode') &&
                	isset($post['campaignId']) && !empty($post['campaignId'])) {
				$url = add_query_arg(
	                array(
	                    'campaign_source' => $post['campaignId'],
	                    'e' => $post['campaignSessionId'],
	                    'p' => $post['p'],
	                ),
	                get_home_url()
	            );

	            /*
				 * call all edit error url filter
				 */
	            if (has_filter('wfc_donation_errorurl_flt') == true) {
		            $wfc_donation_errorurl_flt = apply_filters('wfc_donation_errorurl_flt', $url, $post);
		            if (!empty($wfc_donation_errorurl_flt) && filter_var(wfc_donation_errorurl_flt, FILTER_VALIDATE_URL)) {
			            $url = $wfc_donation_errorurl_flt;
		            }
	            }
			}else{
	            $url = add_query_arg(
	                array(
	                    'e' => $post['campaignSessionId'],
	                    'p' => $post['p'],
	                ),
	                get_home_url()
	            );

	            /*
				 * call all edit error url filter
				 */
	            if (has_filter('wfc_donation_errorurl_flt') == true) {
		            $wfc_donation_errorurl_flt = apply_filters('wfc_donation_errorurl_flt', $url, $post);
		            if (!empty($wfc_donation_errorurl_flt) && filter_var($wfc_donation_errorurl_flt, FILTER_VALIDATE_URL)) {
			            $url = $wfc_donation_errorurl_flt;
		            }
	            }
			}

	        $WFC_DonationDataHandler = new WFC_DonationDataHandler();
	        $failuremessage_config = $WFC_DonationDataHandler->get_failuremessage_config($post['donation_campaign']);

	        $donation_form_override_failure_message = isset($failuremessage_config['donation_form_override_failure_message']) ? $failuremessage_config['donation_form_override_failure_message'] : 'false';
	        $donation_form_override_message_type = isset($failuremessage_config['donation_form_override_message_type']) ? $failuremessage_config['donation_form_override_message_type'] : 'plain_text';
	        $donation_form_override_message = isset($failuremessage_config['donation_form_override_message']) ? $failuremessage_config['donation_form_override_message'] :'There has been an issue with processing your donation. Please contact our administrator.';

	        if(isset($failuremessage_config[donation_form_error_message_behavior]) && $failuremessage_config[donation_form_error_message_behavior] == 'popup'){
		        $isPopupErrorMessage = true;
	        }else {
		        $isPopupErrorMessage = false;
	        }

	        $form_error = array();
	        $campaignSessionId = isset($post['campaignSessionId']) ? $post['campaignSessionId'] : '';
            if($isPopupErrorMessage){
	            $form_error = get_transient('pdwp_errors' . $campaignSessionId);
	            $form_error = isset($form_error['form_error']) ? $form_error['form_error'] : array();
            }


            wp_send_json(array(
	            'ResponseFrom' => 'wfc_reset_form',
	            'status' => 'success',
	            'url'    => $url,
	            'isPopupErrorMessage' => $isPopupErrorMessage,
	            'campaignSessionId' => $campaignSessionId,
	            'form_error' => $form_error,
	            'override_failure_message' => $donation_form_override_failure_message,
	            'override_message_type' => $donation_form_override_message_type,
	            'override_message' => $donation_form_override_message
            ));
        }
    }
	
	/**
	 * Handles the syncing event of single donations.
	 *
	 * @author   Junjie Canonio
	 *
	 * @param      $donation
	 * @param null $recurringid
	 *
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 4, 2018
	 */
    public function wfc_sync_event_singledonation($donation, $recurringid = NULL) {
    	$post_meta_id = isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '';

    	if (!empty($post_meta_id)) {
	    	$WFC_DonationForms = new WFC_DonationForms();
			$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id($post_meta_id);
			$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
			$donation = maybe_unserialize( $donations );
			$donation = stripslashes_deep( $donation );

	        $WFC_DonationDataHandler = new WFC_DonationDataHandler();
	        $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($donation, $this->class, $recurringid);
	    }
    }
	
	/**
	 * Handles syncing of recurring donations.
	 *
	 * @author   Junjie Canonio
	 *
	 * @param array $donation
	
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 4, 2018
	 */
    public function wfc_sync_event_recurringdonation($donation) {
    	$post_meta_id = isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '';

    	if (!empty($post_meta_id)) {
    		// Fetch donation base on meta id
    		$WFC_DonationForms = new WFC_DonationForms();
			$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id($post_meta_id);
			$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
			$donation = maybe_unserialize( $donations );
			$donation = stripslashes_deep( $donation );

			// Syncing the donation to salesforce via PPAPI
	        $WFC_DonationDataHandler = new WFC_DonationDataHandler();
	        $result = $WFC_DonationDataHandler->wfc_sync_recurringdonation_tosf($donation, $this->class, '');

	        if((isset($result['SalesforceResponse']['RecurringId']) && !empty($result['SalesforceResponse']['RecurringId'])) &&
	            (isset($result['SalesforceResponse']['status']) && $result['SalesforceResponse']['status'] == 200 )){
	            
	            $RecurringId = 
	            isset($result['SalesforceResponse']['RecurringId']) ? $result['SalesforceResponse']['RecurringId'] : '';
	            

	            $event_schedule = time() + 5;
				wp_clear_scheduled_hook( 
					'wfc_donation_syncsingledonation', 
                	array(
                		array('post_meta_id' => $post_meta_id),
                		$RecurringId
                	) 
                );
	            wp_schedule_single_event( 
                	$event_schedule, 
                	'wfc_donation_syncsingledonation', 
                	array(
                		array('post_meta_id' => $post_meta_id),
                		$RecurringId
                	) 
                );
	        }    
    	}
    }
	
	/**
	 * An Ajax callback function responsible for redirecting from donation page to thankyou/failure pages.
	 *
	 * @author   Junjie Canonio
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 9, 2018
	 */
    public function wfc_complete_donation(){
        if (isset($_POST['action']) && $_POST['action'] == 'wfc_complete_donation') {
            if (!wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
                die('busted');
            }

            $post = $_POST['donation'];
            $payment_response = $post['payment_response'];

            $donation_campaign = $post['donation_campaign'];
            $pdwp_donation_type = $post['pdwp_donation_type'];


            /*
            * WFC donation logs
            */
            $post['form_userrequired_fields'] = '';
            $donation_logs_id = isset($post['donation_logs_id']) ? $post['donation_logs_id'] : 0;
            $response = isset($_POST['param']['response']) ? $_POST['param']['response'] : '';
            if($donation_logs_id != 0){
                $content_post = get_post($donation_logs_id);
                $logs = $content_post->post_content;
                $logs = maybe_unserialize($logs);
                $logs['completeDonation']['form'] = $post;
                update_post_meta( $donation_logs_id, 'completeDonation', serialize($logs) );
            }


            /*
             * check if payment transaction is success
             */
            if (isset($post['statusCode']) && $post['statusCode'] == 1) {
                /*
                 * cache unset
                 */
                if(isset($_SESSION['pdwp_rand'])){
                	unset( $_SESSION['pdwp_rand'] );
                }


                 /*
                 * delete cart items
                 */
                if(isset($_SESSION['pdwp_cart_id'])){
                	delete_transient( 'pdwp_cart_items' . $_SESSION['pdwp_cart_id'] );
                }

                /*
                 * delete cart id
                 */
                if(isset($_SESSION['pdwp_cart_id'])){
                	unset( $_SESSION['pdwp_cart_id'] );
                }


                $thankyou_url = (isset($post['thankyou_url'])) ? $post['thankyou_url'] : '';

                if (!empty($thankyou_url)) {

                    $thankyou_url_one_off = get_post_meta($donation_campaign, 'donation_form_thankyou_url_one_off', true);
                    $thankyou_url_recurring = get_post_meta($donation_campaign, 'donation_form_thankyou_url_recurring', true);

                    if ($pdwp_donation_type === 'one-off') {
                        if (! empty($thankyou_url_one_off)) {
                            // redirect to one-off

                            $thankyou_url = $thankyou_url_one_off;
                        }
                    } else {
                        if (! empty($thankyou_url_recurring)) {
                            // redirect to recurring

                            $thankyou_url = $thankyou_url_recurring;
                        }
                    }

                	/*
			         * call all custom filter
			         */
                	$filter_url = '';
			        if (has_filter('wfc_donation_thankyoupageredirecturl_flt') == true) {
			            $filter_url = apply_filters(
			            	'wfc_donation_thankyoupageredirecturl_flt', 
			            	array(
			            		'type' => 'custom',
			            		'url' => $thankyou_url,
			            		'pmid' => $post['post_meta_id'],
			            	)
			            );
			        }

                    if (!empty($filter_url)) {
                    	$thankyou_url = $filter_url;
                    }


                    /*
                     * Redirect to thank you URL set for this donation
                     */
                    wp_send_json(array(
	                    'ResponseFrom' => 'wfc_complete_donation',
                        'status' => 'success',
                        'url'    => $thankyou_url,
                    ));

                } else {
                    $setting = get_option( 'pronto_donation_settings', 0 );

                    /*
                     *  grab thankyou page id
                     */
                    $thankyou = isset($setting->pd_thank_you_page_id) ?
                    $setting->pd_thank_you_page_id : '';

                    $pageattr = array(
                        'p'  => $thankyou,
                        'ty' => $post['donation_campaign'],
                        'pmid' => $post['post_meta_id'],
                    );


                    /*
			         * call all custom filter
			         */
                    $filter_pageattr = array();
			        if (has_filter('wfc_donation_thankyoupageredirect_flt') == true) {
			            $filter_pageattr = apply_filters(
			            	'wfc_donation_thankyoupageredirect_flt', 
			            	array(
			            		'type' => 'standard',
			            		'pageattr' => $pageattr,
			            	)
			            );
			        }
			        
			        if (!empty($filter_pageattr) && is_array($filter_pageattr)) {
                    	$pageattr = $filter_pageattr;
                    }


                    /*
                     *  Redirect to Thank you Page
                     */
                    $url = add_query_arg(
                        $pageattr,
                        get_home_url()
                    );

                    wp_send_json(array(
	                    'ResponseFrom' => 'wfc_complete_donation',
                        'status' => 'success',
                        'url'    => $url,
                    ));
                }                
            }else{
                if( isset( $post['payment_gateway'] ) ) {
                    $gateway = isset($post['payment_gateway']) ? $post['payment_gateway'] : '';
                    $gateway 		= explode('_', $post['payment_gateway']);
					$gateway_key	= count( $gateway ) - 2;
					// $gateway 		= ucfirst( $gateway[0] );
					$gateway 		= isset( $gateway[ $gateway_key ] ) ? $gateway[ $gateway_key ] : ucfirst( $gateway[0] );
                 
                    $WFC_DonationPaymentGateway = new WFC_DonationPaymentGateway();
                    $field_error = $WFC_DonationPaymentGateway->wfc_gateway_parse_resp($payment_response,$gateway);

                    WFC_DonationFormRenderer::set_error($field_error, $post);

                    if (!empty($field_error)) {
                        /*
                         * error occured
                         */
                        if ((isset($post['campaign_sourcemode']) && $post['campaign_sourcemode'] == 'CampaignSourceMode') &&
                			isset($post['campaignId']) && !empty($post['campaignId'])) {
                        	$url = add_query_arg(
	                        	array(
		                            'campaign_source' => $post['campaignId'],
		                            'e' => $post['campaignSessionId'],
		                            'piv' => 'true',
		                            'p' => $post['p'],
		                        ),
	                            get_home_url()
	                        );

	                        /*
							 * call all edit error url filter
							 */
	                        if (has_filter('wfc_donation_errorurl_flt') == true) {
		                        $wfc_donation_errorurl_flt = apply_filters('wfc_donation_errorurl_flt', $url, $post);
		                        if (!empty($wfc_donation_errorurl_flt) && filter_var($wfc_donation_errorurl_flt, FILTER_VALIDATE_URL)) {
			                        $url = $wfc_donation_errorurl_flt;
		                        }
	                        }
                        }else{
	                        $url = add_query_arg(
	                        	array(
		                            'e' => $post['campaignSessionId'],
		                            'piv' => 'true',
		                            'p' => $post['p'],
		                        ),
	                            get_home_url()
	                        );

	                        /*
							 * call all edit error url filter
							 */
	                        if (has_filter('wfc_donation_errorurl_flt') == true) {
		                        $wfc_donation_errorurl_flt = apply_filters('wfc_donation_errorurl_flt', $url, $post);
		                        if (!empty($wfc_donation_errorurl_flt) && filter_var($wfc_donation_errorurl_flt, FILTER_VALIDATE_URL)) {
			                        $url = $wfc_donation_errorurl_flt;
		                        }
	                        }
                        }

	                    $WFC_DonationDataHandler = new WFC_DonationDataHandler();
	                    $failuremessage_config = $WFC_DonationDataHandler->get_failuremessage_config($post['donation_campaign']);

	                    $donation_form_override_failure_message = isset($failuremessage_config['donation_form_override_failure_message']) ? $failuremessage_config['donation_form_override_failure_message'] : 'false';
	                    $donation_form_override_message_type = isset($failuremessage_config['donation_form_override_message_type']) ? $failuremessage_config['donation_form_override_message_type'] : 'plain_text';
	                    $donation_form_override_message = isset($failuremessage_config['donation_form_override_message']) ? $failuremessage_config['donation_form_override_message'] :'There has been an issue with processing your donation. Please contact our administrator.';

	                    if(isset($failuremessage_config[donation_form_error_message_behavior]) && $failuremessage_config[donation_form_error_message_behavior] == 'popup'){
		                    $isPopupErrorMessage = true;
	                    }else {
		                    $isPopupErrorMessage = false;
	                    }

	                    $form_error = array();
	                    $campaignSessionId = isset($post['campaignSessionId']) ? $post['campaignSessionId'] : '';
	                    if($isPopupErrorMessage){
		                    $form_error = get_transient('pdwp_errors' . $campaignSessionId);
		                    $form_error = isset($form_error['form_error']) ? $form_error['form_error'] : array();
	                    }

                        wp_send_json(array(
	                        'ResponseFrom' => 'wfc_complete_donation',
                            'status' => 'failed',
                            'url'    => $url,
	                        'isPopupErrorMessage' => $isPopupErrorMessage,
	                        'campaignSessionId' => $campaignSessionId,
	                        'form_error' => $form_error,
	                        'override_failure_message' => $donation_form_override_failure_message,
	                        'override_message_type' => $donation_form_override_message_type,
	                        'override_message' => $donation_form_override_message
                        ));
                    }
                }
            }

        }
    }
	
	/**
	 * Overrides the thank you page title.
	 *
	 * @author   Junjie Canonio
	 *
	 * @param  string  $title wordpress title
	 * @param null $id
	 *
	 * @return mixed
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 28, 2018
	 */
    public function wfc_donation_thankyou_override_title($title , $id = null) {
        global $post;
        if (isset($_GET['ty']) && $_GET['ty'] != '' && is_numeric($_GET['ty']) && isset($post->ID) && get_post_type($post->ID) == 'page') {
            $tyID    = isset($_GET['ty']) ? $_GET['ty'] : '';    
            $thankyou_title = get_post_meta($tyID, 'donation_form_thankyou_page_title', true);

            $pronto_donation_settings = get_option( 'pronto_donation_settings', 0 );
            $pd_thank_you_page_id = isset($pronto_donation_settings->pd_thank_you_page_id) ? $pronto_donation_settings->pd_thank_you_page_id : '';

            if($post->ID ==$pd_thank_you_page_id  && in_the_loop()){
                $title = $thankyou_title;
            }
        }
        return $title;
    }
	
	/**
	 * Overrides the page content of the thank you page with the thank ypu content save on form settings.
	 *
	 * @author   Junjie Canonio
	 * @param $content
	 *
	 * @return mixed
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 28, 2018
	 */
    public function wfc_donation_thankyoucontent_override($content)
    {
        global $post;

        /*
        * Thankyou Page Display
        */
        if (isset($_GET['ty']) && $_GET['ty'] != '' && is_numeric($_GET['ty']) && get_post_type($post->ID) == 'page') {
            $tyID    = isset($_GET['ty']) ? $_GET['ty'] : '';

            $content =
            get_post_meta($tyID, 'donation_form_thankyou_page_content', true);

            $pd_settings = get_option('pronto_donation_settings', 0);
            $social_facebook         = isset($pd_settings->social_facebook) ? $pd_settings->social_facebook : 'false';
            $social_twitter          = isset($pd_settings->social_twitter) ? $pd_settings->social_twitter : 'false';
            $social_google_plus      = isset($pd_settings->social_google_plus) ? $pd_settings->social_google_plus : 'false';
            $social_linkedin         = isset($pd_settings->social_linkedin) ? $pd_settings->social_linkedin : 'false';

            $facebook_btn = WFC_Donation_Social_Sharing::get_instance()->prepare( PD_SOCIAL_FACEBOOK )->share();
            $twitter_btn = WFC_Donation_Social_Sharing::get_instance()->prepare( PD_SOCIAL_TWITTER )->share();
            $google_plus_btn = WFC_Donation_Social_Sharing::get_instance()->prepare( PD_SOCIAL_GOOGLE_PLUS )->share();
            $linkedin_btn = WFC_Donation_Social_Sharing::get_instance()->prepare( PD_SOCIAL_LINKEDIN )->share();
            
            if( $social_facebook !== 'false' ){
                $content =  str_replace( '[facebook]', $facebook_btn, $content );
            }else{
                $content =  str_replace( '[facebook]', '', $content );
            }

            if( $social_twitter !== 'false' ){
                $content =  str_replace( '[twitter]', $twitter_btn, $content );
            }else{
                $content =  str_replace( '[twitter]', '', $content );
            }

            if( $social_google_plus !== 'false' ){
                $content =  str_replace( '[google-plus]', $google_plus_btn, $content );
            }else{
                $content =  str_replace( '[google-plus]', '', $content );
            }

            if( $social_linkedin !== 'false' ){
                $content =  str_replace( '[linkedin]', $linkedin_btn, $content );
            }else{
                $content =  str_replace( '[linkedin]', '', $content );
            }

            $content = html_entity_decode($content);
        }
        
        return $content;
    }
	
	/**
	 *  This call back function will insert a script tag to the wordpress header containing the custom script
	 *
	 *
	 * @author   Junjie Canonio
	 * @since    3.0.0
	 *
	 * @LastUpdated  April 10, 2019
	 */
    public function wfc_donation_inserttothankyoupageheader(){
    	if (isset($_GET['pmid']) && !empty($_GET['pmid'])) {
    		$WFC_DonationForms = new WFC_DonationForms();
			$donation = $WFC_DonationForms->wfc_get_donation_by_meta_id($_GET['pmid']);
			$donation = isset($donation[0]['meta_value']) ? $donation[0]['meta_value'] : array();
			$donation = maybe_unserialize( $donation );
			$donation = stripslashes_deep( $donation );

	    	/*
	         * call all custom filter
	         */
	        if (has_filter('wfc_donation_inserttothankyoupageheader_flt') == true) {
	            $script = apply_filters('wfc_donation_inserttothankyoupageheader_flt', $donation);
	            echo $script;
	        }
    	}
    }
	
	/**
	 * Ajax callback that fetches selected modules settings/config.
	 *
	 * @author   Junjie Canonio
	 *
	 * @since    3.0.0
	 *
	 * @LastUpdated  May 3, 2018
	 */
    public function wfc_donation_get_paymentconfig(){
        if (isset($_POST['action']) && $_POST['action'] == 'wfc_gateway_config') {

            if (!wp_verify_nonce($_POST['nonce'], 'ajax-nonce')) {
                die('busted');
            }
			
            $gateway = get_option($_POST['gatewayid']);


            $gateway_type = explode( '_' , $_POST['gatewayid'] ); 
    		// $gateway_type = $gateway_type[0];
    		$gateway_id 	= $gateway_type[1];
			$gateway_id_key	= count( $gateway_type ) - 1;
			$gateway_id 	= isset( $gateway_type[ $gateway_id_key ] ) ? $gateway_type[ $gateway_id_key ] : $gateway_id;

			$gateway_type_key	= count( $gateway_type ) - 2;
			$gateway_type 		= isset( $gateway_type[ $gateway_type_key ] ) ? $gateway_type[ $gateway_type_key ] : $gateway_type[0];

    		$gateway_module = $this->wfc_getmodule($gateway_type);
            if (isset($gateway_module->restricted) && sizeof($gateway_module->restricted) > 0) {
                foreach ($gateway_module->restricted as $key => $value) {
                    unset($gateway[$value]);
                }
            }

	        if($gateway_type  == 'ezidebit'){
		        if(isset($gateway['wfc_donation_ezidebitpublickey'])){
			        unset($gateway['wfc_donation_ezidebitpublickey']);
		        }
	        }


			$config = array(
				'id' => $gateway_id,
				'type' => $gateway_type,
				'name' => isset($gateway['wfc_donation_gatewaylabel']) ? $gateway['wfc_donation_gatewaylabel'] : $gateway_type,
				// 'module' => ( $alert ) ? array() : $gateway_module,
				'module' => $gateway_module,
				'settings' => $gateway
			);

            wp_send_json(array(
                'status'  => 'success',
                'payment' => $config,
            ));
        }
    }

    /**
	* Loads payment modules
	*
	* @author Junjie Canonio
	* @param string
	* @return string
     *
     * @LastUpdated  July 11, 2018
	*/
    public function wfc_getmodule( $module ) {
    	$gateway = ucfirst( $module );
    	if( class_exists( $gateway ) )
    		return new $gateway;
    }
}
