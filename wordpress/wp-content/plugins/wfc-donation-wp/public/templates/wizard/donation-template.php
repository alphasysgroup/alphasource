<?php
/*
 * Title: Template One Wizard
 * Desc: Template One Wizard
 * Date: April 26, 2018
 * Author : Von Sienard Vibar
 */
if ( ! defined( 'WPINC' ) ) { die; }
/* ========================================= Note ===============================================
* Template customizable data
* The array fields below are customizable and can be use to programmatically sort the fields and
* programmatically set the grid class for the fields.
* NOTE: Webforce Connect - Donation is using bootstrap css framework. Please refer to the bootstrap
* documentation to fully use the grid class funtionality on the donation form.
==============================================================================================*/
$dondet_field_options = array(
    'donor_type' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_company' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_title' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
    ),
    'donor_first_name' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_last_name' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_anonymous' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
    ),
    'donor_email' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_phone' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_address' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
    ),
    'donor_country' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_state' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_postcode' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_suburb' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'donor_comment' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
    ),

    'payment_type_credit_card' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'payment_type_bank' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'card_number' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'ccv' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'name_on_card' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'expiry_month' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
    ),
    'expiry_year' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
    ),
    'pdwp_bank_code' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'pdwp_account_number' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
    ),
    'pdwp_account_holder_name' => array(
        'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
    ),
);
$paydet_field_options = array(
	'override_sorting' =>array(
		'override' => false,
		'sorting' => array(
			'card_number',
			'ccv',
			'name_on_card',
			'expiry_month',
			'expiry_year',
			'pdwp_account_number',
			'pdwp_account_holder_name',
			'pdwp_bank_code',
		),
	),
	'fields' => array(
		'payment_type_credit_card' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'payment_type_bank' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'card_number' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'ccv' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'name_on_card' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'expiry_month' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
		),
		'expiry_year' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
		),
		'pdwp_bank_code' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'pdwp_account_number' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
		),
		'pdwp_account_holder_name' => array(
			'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
		),
	),
);

?>
<?php
/* ========================================= Note ===============================================
* Template custom CSS style tag
* It is recommended to properly add all of the custom CSS that is being use for this template
* inside the CSS style tag below.
* NOTE: Webforce Connect - Donation standard CSS can be overiden but cannot be change for it is
* already predefined as the standard CSS for all the donation templates.
* NOTE: Please refer to the already provided MEDIA break to any CSS that is using MEDIA breaks.
* The MEDIA breaks provided below is using the bootstrap grid system.
==============================================================================================*/
?>
<style type="text/css">
    #pdwp-btn{
        margin-right: 0 !important;
        float:left;
    }

    @media (max-width: 360px) {
        .wizard-form ul.tabs {
            display: block !important;
        }
    }

    /*
	* Small devices (landscape phones, 576px and up)
	*/
    @media (min-width: 576px) {
        #pdwp-btn{
            margin-right: 5px;
            float:left;
        }
    }

    /*
	* Medium devices (tablets, 768px and up)
	*/
    @media (min-width: 768px) {
        #pdwp-btn{
            margin-right: 5px;
            float:right;
        }
    }

    /*
	* Large devices (desktops, 992px and up)
	*/
    @media (min-width: 992px) {
        #pdwp-btn{
            margin-right:20px;
            float:right;
        }
    }

    /*
	* Extra large devices (large desktops, 1200px and up)
	*/
    @media (min-width: 1200px) {
        #pdwp-btn{
            margin-right: 20px;
            float:right;
        }
    }


    .wizard-form {

    }

    .wizard-form ul.tabs {
        display: flex;
        list-style: none;
        margin: 0;
    }

    .wizard-form ul.tabs li {
        position: relative;
        background-color: #c4c4c4;
        color: white;
        font-size: 16px;
        margin: 0;
        padding: 10px 20px;
        z-index: 10;
        transition: 300ms ease-out;
    }

    .wizard-form ul.tabs li.active {
        background-color: #1284a3 !important;
        font-weight: 500;
    }

    .wizard-form ul.tabs li.done:not(.active) {
        background-color: #1497c1;
        cursor: pointer;
    }

    .wizard-form ul.tabs li:after {
        content: '';
        position: absolute;
        display: block;
        width: 0;
        height: 0;
        border-left: 20px solid transparent;
        border-right: 20px solid transparent;
        transition: 300ms ease-out;
    }

    .wizard-form ul.tabs li.active:after {
        border-top: 20px solid #1284a3 !important;
    }

    .wizard-form ul.tabs li.done:after {
        border-top: 0 solid #1497c1;
    }

    .wizard-form ul.tabs li.done:hover {
        background-color: #1284a3;
    }

    .wizard-form ul.tabs li.done:hover:after {
        border-top: 20px solid #1284a3;
    }

    .wizard-form .steps {
        box-shadow: 0 0 10px 0 rgba(0, 0, 0, 0.1);
    }

    .wizard-form .steps section {
        display: none;
        margin-top: 0;
    }

    .wizard-form .steps section .row {
        margin: 0;
    }

    .wizard-form .steps section.active {
        display: block;
    }

    .wizard-form .steps section .step-header,
    .wizard-form .steps section .step-body {
        /* padding: 20px; */
    }

    .wizard-form .steps section .step-body {
        background-color: #ebebeb;
    }

    .wizard-form .steps section .step-body input.wfc-don-invalid-field {
        box-shadow: 0 0 3px 0 red !important;
    }

    .wizard-form .steps section .step-body input.wfc-don-invalid-field + small,
    .wfc-don-invalid-field-message {
        color: red !important;
        font-size: 10px !important;
        opacity: 0.5;
    }

    .wizard-form .steps section .step-footer {
        border-top: 1px solid rgba(0, 0, 0, 0.05);
        padding: 10px;
    }

    .wizard-form .steps section .step-footer small {
        opacity: 0.8;
    }

    .wizard-form .navigation {
        display: flex;
        flex-flow: row-reverse;
    }

    .wizard-form .navigation button {
        position: relative;
        outline: none;
        height: auto;
        padding: 10px 20px;
        transition: 300ms ease-out;
    }

    .wizard-form .navigation button i.fa {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    .wizard-form .navigation button[disabled] {
        background-color: #d9d9d9;
        opacity: 0.8;
    }

    .wizard-form .navigation button.ready {
        background-color: #3ca829;
    }


    @keyframes awaiting {
        0% {
            background-color: #333;
        }
        50% {
            background-color: #1497c1;
        }
        100% {
            background-color: #333;
        }
    }

    .wizard-form .navigation button.awaiting {
        animation: awaiting 2000ms infinite;
    }


    .pd-container-recaptcha {
        float: left;
    }
</style>
<?php
/* ========================================= Note ===============================================
* This is where you enqueue your styles and scripts. You can register your own and call the slug
* of your registered styles/scripts in here.
/* ==============================================================================================*/
WFC_DonationFormRenderer::enqueue_styles_scripts(
    array(
        'wfc-BootstrapCDN-css',
        'webforce-connect-donation-font-awesome'
    ),
    array(
        'jquery-effects-core',
        // 'webforce-connect-donation-wizard-template-standard-js',
        'webforce-connect-donation-wizard-template-advance-js',
    )
);

/* ========================================= Note ===============================================
* Template Donation form with fields
* Below are the important standard html and static function that is being use for the whole donation form.
* NOTE: Webforce Connect - Donation already provides the needed functionality for donation and it is
* highly recommended not to delete any of the static fucntions called on this template for it will
* surely cause major issues.
==============================================================================================*/
?>
<div class="wizard-form">
	<?php WFC_DonationFormRenderer::get_error($configs)?>
	<?php WFC_DonationFormRenderer::render_form_tag($configs,'start')?>
    <!-- Wizard Step Tabs -->
    <ul class="tabs">
        <li class="active" data-tab-id="1">
            <div><?php echo __('Amount', 'webforce-connect-donation'); ?></div>
        </li>
        <li data-tab-id="2">
            <div><?php echo __('Details', 'webforce-connect-donation'); ?></div>
        </li>
        <li data-tab-id="3">
            <div><?php echo __('Payment', 'webforce-connect-donation'); ?></div>
        </li>
    </ul>

    <!-- Wizard Steps Container -->
    <div class="steps">

        <!-- Wizard Donation Section -->
        <section class="active wfc_donation-amount-main-container" data-section-id="1">
            <div class="wfc_donation-header-container-title row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <span><?php echo __('Donation Amount', 'webforce-connect-donation'); ?></span>
                </div>
            </div>
            <div class="step-body">
	            <?php WFC_DonationFormRenderer::render_donation_amount($configs)?>
	            <?php WFC_DonationFormRenderer::render_donation_tibutefield($configs)?>
            </div>
            <div class="wfc_donation-header-container-title row step-header">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <span><?php echo __('Donation Type', 'webforce-connect-donation'); ?></span>
                </div>
            </div>
            <div class="step-body">
		        <?php WFC_DonationFormRenderer::render_donation_type($configs)?>
            </div>
        </section>

        <!-- Wizard Donor Details Section -->
        <section class="wfc_donation-donor-details-main-container" data-section-id="2">
            <div class="wfc_donation-header-container-title row step-header">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <span><?php echo __('Donor Details', 'webforce-connect-donation'); ?></span>
                </div>
            </div>
            <div class="step-body">
	            <?php WFC_DonationFormRenderer::render_form_field($configs,$dondet_field_options)?>
            </div>
        </section>

        <!-- Wizard Payment Section -->
        <section class="wfc_donation-payment-gateway-main-container" data-section-id="3">
            <div class="wfc_donation-header-container-title row step-header">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <span><?php echo __('Payment Gateway', 'webforce-connect-donation'); ?></span>
                </div>
            </div>
            <div class="step-body">
	            <?php WFC_DonationFormRenderer::render_gateway($configs)?>
            </div>
            <div class="wfc_donation-header-container-title row step-header">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <span><?php echo __('Payment Details', 'webforce-connect-donation'); ?></span>
                </div>
            </div>
            <div class="step-body">
		        <?php WFC_DonationFormRenderer::render_payment_details($configs,$paydet_field_options)?>
            </div>
        </section>
    </div>

    <!-- Google reCaptcha (can be enabled or disabled on the Donation Settings) -->
    <div id="pdwp-recaptcha" style="display: none;">
	    <?php WFC_DonationFormRenderer::render_google_captcha($configs)?>
    </div>

    <!-- Wizard Navigation -->
    <div class="navigation">
        <!-- The Donate button for donate submission (hidden in default). It will show up once the Wizard steps reaches on its Payment stage -->
	    <?php WFC_DonationFormRenderer::render_submitbtn($configs)?>
        <!-- Default wizard navigation buttons. Once reached on the last step, the next button will be hidden and replaced with the Donate button -->
        <!-- The same style is applied (can be configured on the Form settings) except the background-color -->
        <button class="saiyan-btn-primary" type="button" data-step-action="next" style="<?php echo WFC_DonationFormRenderer::btn_donate_style($configs); ?> background-color: #4D4D4D; width: auto;"><i class="fa fa-chevron-right"></i></button>
        <button class="saiyan-btn-primary" type="button" data-step-action="prev" style="<?php echo WFC_DonationFormRenderer::btn_donate_style($configs); ?> background-color: #4D4D4D; width: auto;" disabled><i class="fa fa-chevron-left"></i></button>
    </div>
	<?php WFC_DonationFormRenderer::system_hidden_fields($configs)?>
	<?php WFC_DonationFormRenderer::render_form_tag($configs,'end')?>
</div>
<?php
/* ========================================= Note ===============================================
* Template custom JS script tag
* It is recommended to properly add all of the custom JS that is being use for this template
* inside the JS script tag below.
==============================================================================================*/
?>
<script type="text/javascript">
    jQuery('#pdwp-form-donate').attr('wfc-don-advance-wizard','true');

    /*
     * This is a sample callback binded on next button of the wizard where you can
     * add custom message on the custom field that you added on the template
     **/
    jQuery('.wizard-form').on('donation-wizard:next', function() {
        return {
            'another_field': {
                title: 'Another Field',
                message: 'Another Field is empty'
            },
            'another_field_step_2': {
                title: 'Another Field Step 2',
                message: 'Another Field Step 2 is empty'
            }
        }
    });
</script>
