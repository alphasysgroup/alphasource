<?php
/*
 * Title: Template One
 * Desc: Template One
 * Date: April 26, 2018
 * Author : Junjie Canonio
 */
if ( ! defined( 'WPINC' ) ) { die; }
/* ========================================= Note ===============================================
* Template customizable data
* The array fields below are customizable and can be use to programmatically sort the fields and 
* programmatically set the grid class for the fields. 
* NOTE: Webforce Connect - Donation is using bootstrap css framework. Please refer to the bootstrap 
* documentation to fully use the grid class funtionality on the donation form. 
==============================================================================================*/
$field_grids = array(
	'donor_type' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_company' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_title' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_first_name' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_last_name' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_anonymous' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_email' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_phone' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_address' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
	'donor_country' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_state' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_postcode' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_suburb' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'donor_comment' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),	

	'payment_type_credit_card' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'payment_type_bank' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'card_number' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'ccv' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'name_on_card' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'expiry_month' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
	),
	'expiry_year' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
	),
	'pdwp_bank_code' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'pdwp_account_number' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
	),
	'pdwp_account_holder_name' => array(
		'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
	),
);


/* ========================================= Note ===============================================
* This is where you enqueue your styles and scripts. You can register your own and call the slug
* of your registered styles/scripts in here.
/* ==============================================================================================*/
WFC_DonationFormRenderer::enqueue_styles_scripts(
	array(
		'wfc-BootstrapCDN-css',
	),
	array()
);

//WFC_DonationCASAddressAutocomplete::setup_googleaddressautocomplete();

/* ========================================= Note ===============================================
* Template Donation form with fields
* Below are the important standard html and static function that is being use for the whole donation form. 
* NOTE: Webforce Connect - Donation already provides the needed functionality for donation and it is 
* highly recommended not to delete any of the static fucntions called on this template for it will
* surely cause major issues. 
==============================================================================================*/
?>
<?php WFC_DonationFormRenderer::get_error($configs); ?>
<?php WFC_DonationFormRenderer::render_form_tag($configs,'start'); ?>
	<div class="wfc_donation-amount-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Donation Amount', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_donation_amount($configs)?>
		<?php WFC_DonationFormRenderer::render_donation_tibutefield($configs)?>
	</div>	
	<div class="wfc_donation-frequency-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Donation Type', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_donation_type($configs)?>
	</div>	
	<div class="wfc_donation-donor-details-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Donor Details', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
<!--        <input type="text" id="address1">-->
<!--        <input type="text" id="country1">-->
<!--        <input type="text" id="state1">-->
<!--        <input type="text" id="postcode1">-->
<!--        <input type="text" id="suburb1">-->
<!---->
<!--        <input type="text" id="address2">-->
<!--        <input type="text" id="country2">-->
<!--        <input type="text" id="state2">-->
<!--        <input type="text" id="postcode2">-->
<!--        <input type="text" id="suburb2">-->

        <?php WFC_DonationFormRenderer::render_form_field($configs,$field_grids); ?>
	</div>
	<div class="wfc_donation-payment-gateway-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Payment Gateway', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_gateway($configs)?>
	</div>	
	<div class="wfc_donation-payment-details-main-container">
		<div class="wfc_donation-header-container-title row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span><?php echo __('Payment Details', 'webforce-connect-donation'); ?></span>
			</div>
		</div>
		<?php WFC_DonationFormRenderer::render_payment_details($configs,$field_grids); ?>
	</div>	
	<div style="margin-top: 30px;margin-bottom: 30px;" class="row">
		<div class="col-12 col-sm-12 col-md-6 col-lg-6">
			<?php WFC_DonationFormRenderer::render_google_captcha($configs); ?>
		</div>	
		<div class="col-12 col-sm-12 col-md-6 col-lg-6">
			<?php WFC_DonationFormRenderer::render_submitbtn($configs); ?>
		</div>
	</div>
	<?php WFC_DonationFormRenderer::system_hidden_fields($configs); ?>
<?php WFC_DonationFormRenderer::render_form_tag($configs,'end'); ?>
<?php 
/* ========================================= Note ===============================================
* Template custom JS script tag 
* It is recommended to properly add all of the custom JS that is being use for this template 
* inside the JS script tag below. 
==============================================================================================*/
?>
<script type="text/javascript">
</script>