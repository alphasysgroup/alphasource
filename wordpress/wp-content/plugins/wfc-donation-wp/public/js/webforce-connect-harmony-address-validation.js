/**
* Harmony address validation
*/
jQuery(document).ready(function($){
	var source_of_truth = $("#harmony_rapid_sourceoftruth").val();

	var harmony_overridablefields = $("#harmony_overridablefields").val();
    var harmony_rapid_username = $("#harmony_rapid_username").val();
    var harmony_rapid_password = $("#harmony_rapid_password").val();

    if( harmony_rapid_username != '' && harmony_rapid_password != '' ){
    	console.log('WFC - Donation Harmony Address Validation Loaded');
    	/**
		* generate harmony address validation field
		*/
    	var harmony_addressfield = '<div id="donor_address_harmony_container" class="donor_address_harmony_container col-12 col-sm-12 col-md-12 col-lg-12">'
    	+ '<label class="wfc-don-field-label" for="donor_address_harmony">Provide Address here : </label>'
    	+ '<input type="text" id="donor_address_harmony" autocomplete="new-password"/>'
    	+ '</div>'
    	$('#donor_address_container').before(harmony_addressfield);
    	$("#donor_address_harmony").val($("#donor_address").val());

    	/**
		* detach and attach harmony address fields
		*/
    	var harmony_addressfields = $('#wfc-harmony-address-validation-main-cont').detach()
    	$('#donor_address_harmony_container').after(harmony_addressfields);

	    // Use the environment based on the site it is on
	    Harmony.useEnv("https://hosted.mastersoftgroup.com");

	    // Init the client with the demo api user name and credential
	    // We have created the following user and credential which you can use on localhost
	    Harmony.init(harmony_rapid_username, harmony_rapid_password, Harmony.AUSTRALIA);

	    // Use the JSONP protocol
	    Harmony.useProtocol(Harmony.JSONP);

	    var opt = {
			// min 3 chars to trigger the lookup
			minLength:3,
			// skip transaction call when address selected. You need to make your own call depending on your business flow.
			skipTransaction: true,
			// define your own call back function when address selected.
			onSelect: function(event, ui) {
				console.log(event);
				console.log(ui);
				if(ui.item.id != '' && ui.item.id != undefined){
					var buildingName = (ui.item.buildingName != null) ? ui.item.buildingName : '';
					if(buildingName.replace(/^\s+|\s+$/g, "").length != 0){
						buildingName = buildingName+' ';
					}

					var subdwelling = (ui.item.subdwelling != null) ? ui.item.subdwelling : '';
					if(subdwelling.replace(/^\s+|\s+$/g, "").length != 0){
						subdwelling = subdwelling+' ';
					}

					var streetNumber = (ui.item.streetNumber != null) ? ui.item.streetNumber : '';
					if(streetNumber.replace(/^\s+|\s+$/g, "").length != 0){
						streetNumber = streetNumber+' ';
					}

					var street = (ui.item.street != null) ? ui.item.street : '';
					if(street.replace(/^\s+|\s+$/g, "").length != 0){
						street = street+' ';
					}

					var street2 = (ui.item.street2 != null) ? ui.item.street2 : '';
					var fulladdress = subdwelling+buildingName+streetNumber+street+street2;
					fulladdress = (fulladdress.length == 0) ? ui.item.postal : fulladdress;


					$("#donor_address_harmony").addClass('wfc-don-harmony-valid-address');

					$("#donor_address").removeClass('wfc-don-harmony-invalid-address');
					$("#donor_address").addClass('wfc-don-harmony-valid-address');
					$("#donor_address").val(fulladdress);

					//$("#wfc-harmony-donor_address-field").html($("#donor_address_harmony").val());
					$("#wfc-harmony-donor_address-field").html(subdwelling+buildingName+streetNumber+street+street2);
					$("#wfc-harmony-donor_postcode-field").html(ui.item.postcode);
					$("#wfc-harmony-donor_suburb-field").html(ui.item.locality);
					$("#wfc-harmony-donor_state-field").html(ui.item.state);
					$("#wfc-harmony-dpid-field").html(ui.item.id);
				}else{
					$("#donor_address_harmony").removeClass('wfc-don-harmony-valid-address');
					
					$("#donor_address").removeClass('wfc-don-harmony-valid-address');
					$("#donor_address").addClass('wfc-don-harmony-invalid-address');

					$("#donor_address").val($("#donor_address_harmony").val());
				}

				if(ui.item.attributes.Barcode != '' && ui.item.attributes.Barcode != undefined){
					$("#donor_barcode").val(ui.item.attributes.Barcode);	
					$("#wfc-harmony-barcode-field").html(ui.item.attributes.Barcode);
				}

				if(ui.item.attributes.Latitude != '' && ui.item.attributes.Latitude != undefined){
					$("#donor_latitude").val(ui.item.attributes.Latitude);	
					$("#wfc-harmony-latitude-field").html(ui.item.attributes.Latitude);
				}
				
				if(ui.item.attributes.Longitude != '' && ui.item.attributes.Longitude != undefined){
					$("#donor_longitude").val(ui.item.attributes.Longitude);	
					$("#wfc-harmony-longitude-field").html(ui.item.attributes.Longitude);
				}
				
			}
	    };

	    // Configure the address lookup.
	    // "#rapidAddress" is referring to the input address element id
	    Harmony.UI.addressLookup($("#donor_address_harmony"), source_of_truth, opt);

	    Harmony.UI.addField(Harmony.POSTCODE, $("#donor_postcode"));
	    Harmony.UI.addField(Harmony.LOCALITY, $("#donor_suburb"));
	    Harmony.UI.addField(Harmony.STATE, $("#donor_state"));
	    Harmony.UI.addField(Harmony.ID, $("#donor_DPID"));  
	    Harmony.UI.addField(Harmony.ID, $("#donor_DPID"));   

	    if( source_of_truth == 'AUSOTS' || source_of_truth == 'AUPAF' || source_of_truth == 'GNAF' ){
	    	$("#donor_country").val('Australia');
	    	$("#wfc-harmony-donor_country-field").html('Australia');
	    }

	   	if( source_of_truth == 'NZPAF' ){
	    	$("#donor_country").val('New Zealand');
	    	$("#wfc-harmony-donor_country-field").html('Australia');
	    }

	    var pdwp_DPID_missing = "<span id='pdwp_DPID_missing' class='wfc-don-harmony-note'>Make sure to select from the suggested addresses.</span>"
		$('label[for=donor_address_harmony]').append(pdwp_DPID_missing);	
		$('label[for=donor_address_harmony]').addClass('wfc-don-harmony-note-label');	

		$('#donor_address_harmony').attr('autocomplete', 'new-password');
		$('#donor_address').attr('autocomplete', 'new-password');
		
		if (harmony_overridablefields == 'false') {
			$('#donor_address').attr('readonly', 'readonly');
			$('#donor_country').attr('readonly', 'readonly');
			$('#donor_state').attr('readonly', 'readonly');
			$('#donor_postcode').attr('readonly', 'readonly');
			$('#donor_suburb').attr('readonly', 'readonly');			
		}
		
		if($('#harmony_rapid_displaymode').val() == 'fieldmode') {
			$('#wfc-harmony-address-validation-main-cont').hide();
			$('#donor_address_container').show();
			$('#donor_country_container').show();
			$('#donor_state_container').show();
			$('#donor_postcode_container').show();
			$('#donor_suburb_container').show();
		}else{
			$('#wfc-harmony-address-validation-main-cont').show();
			$('#donor_address_container').hide();
			$('#donor_country_container').hide();
			$('#donor_state_container').hide();
			$('#donor_postcode_container').hide();
			$('#donor_suburb_container').hide();
		}


		CheckAddressIsValid();
		$("#donor_address_harmony").on('keyup',function(){
			if($("#donor_address").val() == '') {
				$("#donor_DPID").val('');
				CheckAddressIsValid();
			}
		});

		function CheckAddressIsValid(){
			setTimeout(function(){ 
				if($("#donor_DPID").val() != ''){
					$("#donor_address").removeClass('wfc-don-harmony-invalid-address');
					$("#donor_address").addClass('wfc-don-harmony-valid-address');
					$("#donor_address_harmony").addClass('wfc-don-harmony-valid-address');
				}else{
					$("#donor_address").removeClass('wfc-don-harmony-valid-address');
					$("#donor_address").addClass('wfc-don-harmony-invalid-address');
					$("#donor_address_harmony").removeClass('wfc-don-harmony-valid-address');
				}
			}, 300);
		}

		jQuery('#donor_address_harmony').keyup(function(){
			Harmony.address({ fullAddress: jQuery(this).val() }, source_of_truth, 
		    function(response) {
		        if (response.status == Harmony.SUCCESS) {
		        
		            // Do something with the response payload.
		            for (var i = 0; i < response.payload.length; i++) {
		                var address = response.payload[i];
		               
		            }

		            if (typeof address === 'undefined' || !address){
		            	$('#donor_address').val('');
						$('#donor_state').val('');
						$('#donor_postcode').val('');
						$('#donor_suburb').val('');		

						$('#donor_DPID').val('');		
						$('#donor_barcode').val('');		
						$('#donor_latitude').val('');		
						$('#donor_longitude').val('');		
		            	
		            	$("#donor_address").removeClass('wfc-don-harmony-valid-address');
		            	jQuery('#donor_address_harmony').addClass('wfc-don-harmony-notvalid');
		            }else{
		            	jQuery('#donor_address_harmony').removeClass('wfc-don-harmony-notvalid');
		            }
		        } 
		    });
		})




    }

});