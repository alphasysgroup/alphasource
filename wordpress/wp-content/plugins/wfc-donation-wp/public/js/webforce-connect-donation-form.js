jQuery(document).ready(function($){
	$('#pdwp-form-donate').submit(function(e) {
		e.preventDefault();

		var donationForm = getDonationFormDetails();

		//$('#pdwp-btn').attr('disabled', 'disabled');
		console.log(donationForm.donation);


	});	

	function getDonationFormDetails() {
		var details = $('#pdwp-form-donate').serializeArray();

		var donation = {};
		var creditcard = {
			card_number: $('#card_number').val(),
			ccv: $('#ccv').val(),
			name_on_card: $('#name_on_card').val(),
			expiry_month: $('#expiry_month').val(),
			expiry_year: $('#expiry_year').val()
		};

		$.each( details, function( k, v ) {
			if( v.name.indexOf( '[' ) > -1 || v.name.indexOf( ']' ) > -1 ) {
				var newkey = v.name.split('[')[0];
				donation[newkey] = v.value;
			} else {
				donation[v.name] = v.value;
			}
		});

		this.creditCardDetails = creditcard;

		// var gateway = donation.payment_gateway.split( '_' );
		// if( gateway[0] == 'nab' ) {
		// 	var bank = {
		// 		bank_code : $('#bank_code').val(),
		// 		bank_number : $('#account_number').val(),
		// 		bank_name : $('#account_name').val()
		// 	};
		// } else {
		// 	var bank = {
		// 		bank_code : $('#pdwp_bank_code').val(),
		// 		bank_number : $('#pdwp_account_number').val(),
		// 		bank_name : $('#pdwp_account_holder_name').val()
		// 	};
		// }
 
		return {
			donation : donation,
			//creditcard : creditcard,
			//bank : bank
		};
	}
});	