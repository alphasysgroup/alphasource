/**
 * Wizard Donation template logic interaction (advance)
 */
const $ = jQuery;

$('#pdwp-form-donate').attr('wfc-don-advance-wizard','');

$('#donor_email').attr('type', 'email');

const w = $('.wizard-form');
const t = w.find('.tabs');
const steps = w.find('.steps');
const btnStep = w.find('button[data-step-action]');
const donateBtn = w.find('.navigation > div.wfc_donation-submitbtn-container');
const recaptcha = w.find('#pdwp-recaptcha');

donateBtn.hide();

btnStep.on('click', function(e) {
    e.preventDefault();
    const $this = $(this);
    var action = $this.attr('data-step-action');

    if (action === 'next' && ! validateSectionFields()) return false;

    wizardNavigator(action);

    btnUpdate();
    sectionSwitch();
});

w.on('click', '.tabs li.done:not(.active)', function() {
    t.find('.active').removeClass('active');
    $(this).addClass('active');

    btnUpdate();
    sectionSwitch();
});

function wizardNavigator(direction) {
    var curTab = w.find('.tabs li.active');

    switch (direction) {
        case 'prev':
            curTab.removeClass('active').addClass('done');
            curTab.prev().addClass('active done');
            break;
        case 'next':
            curTab.removeClass('active').addClass('done');
            curTab.next().addClass('active done');
            break;
    }
}

function btnUpdate() {
    var btn = w.find('button[data-step-action=\'next\']');
    w.find('button[data-step-action=\'prev\']').prop('disabled', t.find('li').first().hasClass('active'));
    w.find('button[data-step-action=\'next\']').prop('disabled', t.find('li').last().hasClass('active'));
    if (t.find('li').last().hasClass('active')) {
        btn.prev().show();
        btn.hide();
        donateBtn.show();
        recaptcha.show();
    } else {
        btn.prev().hide();
        btn.show();
        donateBtn.hide();
        recaptcha.hide();
    }

    w.trigger('donation-wizard:button-update');
}

function sectionSwitch() {
    var curSec = steps.find('section.active');

    curSec.hide(400, 'easeOutQuad', function() {
        $(this).removeClass('active');
    });

    var curTab = w.find('.tabs li.active');
    steps.find('section[data-section-id=\'' + curTab.attr('data-tab-id') + '\']').show(400, 'easeOutQuad', function() {
        $(this).addClass('active');

        w.trigger('donation-wizard:section-switch', this);
    });
}

function validateSectionFields() {
    var isValid = true;
    var invalidFields = [];

    steps.find('section.active [required]').each(function() {
        // console.log(this.reportValidity());

        if (! this.checkValidity())
            invalidFields.push($(this).attr('name'));

        isValid &= this.checkValidity();
    });

    steps.find('section.active [required][name="' + invalidFields[0] + '"]').focus();

    //console.log(invalidFields);

    if (invalidFields.length > 0)
        showRequiredFieldsMessage(invalidFields);

    if (invalidFields.length === 0)
        resetHighlightInvalidFields();

    return isValid === 1;
}

function showRequiredFieldsMessage(invalidFields) {
    var eventResponse = w.triggerHandler('donation-wizard:next');
    var message = '';

    for (var i=0, count=invalidFields.length; i<count; i++) {
        var field = invalidFields[i];
        var fieldObject = typeof eventResponse !== 'undefined' ? eventResponse[field] : undefined;

        if (typeof fieldObject !== 'undefined')
            message += fieldObject.message + '<br>';
        else {
            if (field === 'pdwp_custom_amount') {
                var pdwpCustomAmountField = $('[name="pdwp_custom_amount"]');
                // console.log(pdwpCustomAmountField[0].validationMessage);
                // message += 'Custom Amount should be greater than or equal to ' + pdwpCustomAmountField.attr('min');
                message += pdwpCustomAmountField[0].validationMessage;
            } else {
                var attr = $('[name=' + field + ']').attr('wfc-don-label');

                if (typeof attr !== typeof undefined && attr !== false) {
                    message += $('[name=' + field + ']').attr('wfc-don-label') + ' is required <br>';
                }else{
                    message += $('[name=' + field + ']').attr('name') + ' is required <br>';
                }


            }

        }
    }

    /**
     * Fetch field error notification style
     */
    var field_error_notice_type = $('#field_error_notice_type').val();

    if(field_error_notice_type.indexOf("fieldglow") >= 0) {
        highlightInvalidFields(invalidFields, eventResponse);
    }

    if(field_error_notice_type.indexOf("snackbar") >= 0){
        if (message !== '') {
            SaiyantistCore.snackbar.hide();
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">' + message + '</div>',5000,'error');
        }
    }
}

function highlightInvalidFields(invalidFields, customFields) {
    resetHighlightInvalidFields();

    for (var i=0, count=invalidFields.length; i<count; i++) {
        var field = invalidFields[i];
        var fieldObject = typeof customFields !== 'undefined' ? customFields[field] : undefined;
        var elem = $('[name="' + field + '"]');
        var message = '';

        if (typeof fieldObject !== 'undefined') {
            message = fieldObject.message;
        } else {
            if (field === 'pdwp_custom_amount') {
                message = elem[0].validationMessage;
            } else {
                var attr = elem.attr('wfc-don-label');

                if (typeof attr !== 'undefined' && attr !== false) {
                    message = attr + ' is required';
                } else {
                    message = elem.attr('name') + ' is required';
                }
            }
        }

        elem.addClass('wfc-don-invalid-field');

        if (field == 'donor_phone'){
            elem.parent().after(
                $('<small class="wfc-don-invalid-field-message">' + message + '</small>')
            );
        }else{
            elem.after(
                $('<small class="wfc-don-invalid-field-message">' + message + '</small>')
            );
        }

    }
}

function resetHighlightInvalidFields() {
    $('.wfc-don-invalid-field').each(function() {
        $(this).next().remove();
        $(this).removeClass('wfc-don-invalid-field');
    })
}