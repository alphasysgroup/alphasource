/**
 * WFC Donation Loading Bar
 *
 * @author Von Sienard Vibar <von@alphasys.com.au>
 * @type {{}}
 */

var WFC_Donation_LoadingBar = {};

(function($) {
    WFC_Donation_LoadingBar = {
        loaderBarUI: $('<div></div>')
            .addClass('wfc-donation-loading-bar')
            .css({
                'position': 'fixed',
                'background-color': 'rgb(20, 151, 193)',
                'color': 'white',
                'text-align': 'center',
                'font-size': '12px',
                'bottom': '0',
                'width': '0',
                'padding': '3px',
                'opacity': '0.0',
                'transition': '500ms ease-out',
                'z-index' : '9999',
            }),

        init: function() {
            var body = $('body');
            if (body.find('wfc-donation-loading-bar').length === 0) {
                body.append(this.loaderBarUI);
            }
        },

        // update for < es6 backward compatibility
        load: function(message, percent, color, position) {
            color = color || null;
            position = position || null;

            if (percent < 0 || percent > 100) {
                console.log('invalid percent value, should be between 0 to 100');
                return false;
            }

            this.init();

            var loaderBarUI = $(this.loaderBarUI);

            loaderBarUI
                .animate({
                    'opacity': '1.0',
                }, 500)
                .css({
                    'width': percent + '%',
                    'background-color': color !== null ? color : 'rgb(20, 151, 193)'
                })
                .html(message);
            if (percent === 100) {
                setTimeout(function() {
                    loaderBarUI.animate({
                        'opacity': '0.0'
                    }, 800, null, function() {
                        setTimeout(function() {
                            loaderBarUI.remove();
                        }, 400);
                    })
                }, (800 * 2) + 200);
                // delay trigger after the loader reaches its percent value timeout
                // and trigger pause + additional delay for smoothing
            }
            if (position !== null && (position === 'top' || position === 'bottom')) {
                loaderBarUI.css(
                    position === 'top' ? {
                            'top': '0',
                            'bottom': 'auto'
                        } : {
                            'bottom': '0',
                            'top': 'auto'
                        }
                )
            }
        }
    };

    $(document).ready(function() {
        // WFC_Donation_LoadingBar.load('qwe', 100);
    });
})(jQuery);