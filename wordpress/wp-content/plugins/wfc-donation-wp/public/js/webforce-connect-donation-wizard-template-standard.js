/**
 * Wizard Donation template logic interaction (standard)
 */
const $ = jQuery;

$('#donor_email').attr('type', 'email');

const w = $('.wizard-form');
const t = w.find('.tabs');
const steps = w.find('.steps');
const btnStep = w.find('button[data-step-action]');
const donateBtn = w.find('.navigation > div.wfc_donation-submitbtn-container');
const recaptcha = w.find('#pdwp-recaptcha');

donateBtn.hide();

btnStep.on('click', function(e) {
    e.preventDefault();
    const $this = $(this);
    var action = $this.attr('data-step-action');

    if (action === 'next' && validateSectionFields()) return false;

    wizardNavigator(action);

    btnUpdate();
    sectionSwitch();
});

w.on('click', '.tabs li.done:not(.active)', function() {
    t.find('.active').removeClass('active');
    $(this).addClass('active');

    btnUpdate();
    sectionSwitch();
});

function wizardNavigator(direction) {
    var curTab = w.find('.tabs li.active');

    switch (direction) {
        case 'prev':
            curTab.removeClass('active').addClass('done');
            curTab.prev().addClass('active done');
            break;
        case 'next':
            curTab.removeClass('active').addClass('done');
            curTab.next().addClass('active done');
            break;
    }
}

function btnUpdate() {
    var btn = w.find('button[data-step-action=\'next\']');
    w.find('button[data-step-action=\'prev\']').prop('disabled', t.find('li').first().hasClass('active'));
    w.find('button[data-step-action=\'next\']').prop('disabled', t.find('li').last().hasClass('active'));
    if (t.find('li').last().hasClass('active')) {
        btn.prev().show();
        btn.hide();
        donateBtn.show();
        recaptcha.show();
    } else {
        btn.prev().hide();
        btn.show();
        donateBtn.hide();
        recaptcha.hide();
    }

    w.trigger('donation-wizard:button-update');
}

function sectionSwitch() {
    var curSec = steps.find('section.active');

    curSec.hide(400, 'easeOutQuad', function() {
        $(this).removeClass('active');
    });

    var curTab = w.find('.tabs li.active');
    steps.find('section[data-section-id=\'' + curTab.attr('data-tab-id') + '\']').show(400, 'easeOutQuad', function() {
        $(this).addClass('active');

        w.trigger('donation-wizard:section-switch', this);
    });
}

function validateSectionFields() {
    var isValid = false;
    steps.find('section.active input[required]').each(function() {
        // console.log(this.reportValidity());
        if (! this.reportValidity()) {
            isValid = true;
            return false;
        }
    });

    return isValid;
}