/**
 * Google Maps Autocomplete
 */
var location_lat = null;
var location_lng = null;
var google_prioritized_country = '';
var google_prioritized_country_code = '';

var autocomplete_element  = '';
var street_number_element = '';

var placeSearch, autocomplete, component,val,place,addressType,hasRoute;
var componentForm = {
	subpremise: 'short_name',
	street_number: 'short_name',
	route: 'long_name',
	locality: 'long_name',
	administrative_area_level_1: 'short_name',
	country: 'long_name',
	postal_code: 'short_name'
};

function initAutocomplete() {
	autocomplete_element  = document.querySelector('[pdwp-address="autocomplete"]');
	street_number_element = document.querySelector('[pdwp-address="street_number"]');
	/**
	 * Create the autocomplete object, restricting the search to geographical
	 * location types.
	 */
	autocomplete = new google.maps.places.Autocomplete(
		/** @type {!HTMLInputElement} */(autocomplete_element),
		{types: ['geocode']});

	/**
	 * When the user selects an address from the dropdown, populate the address
	 * fields in the form.
	 */
	autocomplete.addListener('place_changed', fillInAddress);

	google_prioritized_country_code = document.getElementById("google_prioritized_country_code").value;
	if(google_prioritized_country_code != '' && google_prioritized_country_code != null){
		autocomplete.setComponentRestrictions({'country': [google_prioritized_country_code]});
	}

}

function fillInAddress() {
	/**
	 * Get the place details from the autocomplete object.
	 */
	place = autocomplete.getPlace();

	/**
	 * Get each component of the address from the place details
	 * and fill the corresponding field on the form.
	 */
	var subpremise = '';
	for (var i = 0; i < place.address_components.length; i++) {
		addressType = place.address_components[i].types[0];
		if( addressType == 'subpremise' ) {
			if (componentForm[addressType]) {
				subpremise = place.address_components[i][componentForm[addressType]];
			}
		} else if( addressType == 'street_number' ) {
			if (componentForm[addressType]) {
				val = place.address_components[i][componentForm[addressType]];
				if( subpremise ) {
					val = subpremise +'/'+val;
				}
				document.querySelector('[pdwp-address="'+addressType+'"]').value = val;
				trigger_onchange(document.querySelector('[pdwp-address="'+addressType+'"]'));
				if(addressType=='route'){
					autocomplete_element.value = street_number_element.value+' '+val;
				}
			}
		} else {
			if (componentForm[addressType]) {
				val = place.address_components[i][componentForm[addressType]];
				if (addressType=='administrative_area_level_1') {
					setStateValue(val);
				}else{
					document.querySelector('[pdwp-address="'+addressType+'"]').value = val;
					trigger_onchange(document.querySelector('[pdwp-address="'+addressType+'"]'));
					if(addressType=='route'){
						autocomplete_element.value = street_number_element.value+' '+val;
					}
				}
			}
		}
	}
	for (var i = 0; i < place.address_components.length; i++) {
		addressType = place.address_components[i].types[0];
		hasRoute = false;
		if(addressType=='route'){
			hasRoute = true;
			break;
		}
	}
	if(hasRoute==false){
		autocomplete_element.value='';
	}
}


function setStateValue(stateval){
	setTimeout(function(){
		document.querySelector('[pdwp-address="administrative_area_level_1"]').value = stateval;
	}, 500);
}


/**
 * Bias the autocomplete object to the user's geographical location,
 * as supplied by the browser's 'navigator.geolocation' object.
 */
function geolocate() {

	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {

			if (location_lat == null || location_lng == null){
				var geolocation = {
					lat: position.coords.latitude,
					lng: position.coords.longitude
				};
			} else {
				var geolocation = {
					lat: location_lat,
					lng: location_lng
				};
				console.log('Prioritized Country : '+google_prioritized_country+' (Lat : '+location_lat+', Long : '+location_lng+')');
			}

			var circle = new google.maps.Circle({
				center: geolocation,
				radius: position.coords.accuracy
			});
			autocomplete.setBounds(circle.getBounds());
		});
	}
}

getCountryCoorindates();
function getCountryCoorindates(){
	var request = new XMLHttpRequest();
	google_prioritized_country = document.getElementById("google_prioritized_country").value;

	if(google_prioritized_country != ''){

		request.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json?address='+google_prioritized_country+'&key=AIzaSyCvRuwpTW8_-tqfLsxK-Gugb5TGZ8MpJSM ', true);
		request.onload = function() {
			// Begin accessing JSON data here
			var data = JSON.parse(this.response);

			if (request.status >= 200 && request.status < 400) {
				if (typeof data.results[0] !== 'undefined' && typeof data.results[0].geometry.location.lat !== 'undefined' && typeof data.results[0].geometry.location.lng !== 'undefined') {
					location_lat = data.results[0].geometry.location.lat;
					location_lng = data.results[0].geometry.location.lng;
				}

			} else {
				console.log('error');
			}
		}
		request.send();
	}
}



/**
 * Google Maps Autocomplete
 */
function trigger_onchange(element){
	if ("createEvent" in document) {
		var evt = document.createEvent("HTMLEvents");
		evt.initEvent("change", false, true);
		element.dispatchEvent(evt);
	}
	else{
		element.fireEvent("onchange");
	}
}

jQuery(document).ready(function($){

	setTimeout(function(){
		var CheckEnableAddressValidation = '';
		var CheckGoogleGeocodeAPIKey = '';

		CheckEnableAddressValidation = $('#enable_validation').val();
		CheckGoogleGeocodeAPIKey 	 = $('#google_api_key').val();

		if(CheckEnableAddressValidation == 'true' ) {
			HandleRegisterFields(CheckEnableAddressValidation,CheckGoogleGeocodeAPIKey);
		}
	}, 1000);

	function HandleRegisterFields(CheckEnableAddressValidation,CheckGoogleGeocodeAPIKey){
		$('#donor_address').attr('pdwp-address','autocomplete');
		/*$('#enable_validation').attr('pdwp-address','street_number'); */
		/*$('#donor_suburb').attr('pdwp-address','route'); */
		$('#donor_suburb').attr('pdwp-address','locality');
		$('#donor_state').attr('pdwp-address','administrative_area_level_1');
		$('#donor_postcode').attr('pdwp-address','postal_code');
		$('#donor_country').attr('pdwp-address','country');
		EnableAutoComplete(CheckEnableAddressValidation,CheckGoogleGeocodeAPIKey);
	}

	function EnableAutoComplete(enable_address_validation_value,google_geocode_api_key){
		var autocompleteElement = $('[pdwp-address="autocomplete"]');

		var autocompleteIsSet = false;
		if( autocompleteElement.length != 0){
			autocompleteIsSet = true;
		}

		if( autocompleteIsSet == true ){
			autocompleteElement.attr( 'placeholder', '' );
			if( $('#pdwp_custom_template').length != 0){
				if($('#pdwp_custom_template').val() == 'syo'){
					autocompleteElement.attr( 'placeholder', 'Address *' );
				}
			}

			if( $('#inputnolabels').length != 0){
				autocompleteElement.attr( 'placeholder', 'Address *' );
			}

			/**
			 * Check fields required for Autocomplete exist and create dummy if not
			 */
			if( $('[pdwp-address="street_number"]').length == 0){
				autocompleteElement.after('<input type="hidden" pdwp-address="street_number">');
			}
			if( $('[pdwp-address="route"]').length == 0){
				autocompleteElement.after('<input type="hidden" pdwp-address="route">');
			}
			if( $('[pdwp-address="locality"]').length == 0){
				autocompleteElement.after('<input type="hidden" pdwp-address="locality">');
			}
			if( $('[pdwp-address="administrative_area_level_1"]').length == 0){
				autocompleteElement.after('<input type="hidden" pdwp-address="administrative_area_level_1">');
			}
			if( $('[pdwp-address="postal_code"]').length == 0){
				autocompleteElement.after('<input type="hidden" pdwp-address="postal_code">');
			}
			if( $('[pdwp-address="country"]').length == 0){
				autocompleteElement.after('<input type="hidden" pdwp-address="country">');
			}

			if(enable_address_validation_value == 'true' ) {
				console.log('WFC - Donation Google Address Rest API Status : OK');

				excludeAddressFieldFromAutoFill();

				autocompleteElement.attr('onFocus', 'geolocate()');
				setTimeout(function(){
					initAutocomplete();
				}, 2000);

				autocompleteElement.on('focusout', function(){
					if($.trim(autocompleteElement.val()) ==''){
						$('[pdwp-address="street_number"]').val('');
						$('[pdwp-address="route"]').val('');
						$('[pdwp-address="country"]').val('');
						$('[pdwp-address="administrative_area_level_1"]').val('');
						$('[pdwp-address="postal_code"]').val('');
						$('[pdwp-address="locality"]').val('');
					}
					else{

						var address_value = autocompleteElement.val();
						var specify_country ='';

						if($('[pdwp-address="country"]').val()==''||$('[pdwp-address="country"]').val().length === 0){
							$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address_value+'&key='+google_geocode_api_key, function (data) {
								if(data['status']=='ZERO_RESULTS'){
									$('#adress_validation').text('* Invalid address');
								}else{
									$('#adress_validation').text('');
								}
							});
						}
						else{
							$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$('[pdwp-address="country"]').val()+'&key='+google_geocode_api_key, function (data) {
								$.each( data['results'][0]['address_components'], function( key, value ) {
									if(value['types'][0]=='country'){
										var specify_country = '&components=country:'+value['short_name'];

									}
								});
							});
							$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address_value+specify_country+'&key='+google_geocode_api_key, function (data) {
								if(data['status']=='ZERO_RESULTS'){
									$('#adress_validation').text('* Invalid address');
								}else{
									$('#adress_validation').text('');

								}
							});
						}
					}
				});

				autocompleteElement.on('click', function(){
					$('[pdwp-address="route"]').val('');
				});
			}
		}
	}

	function excludeAddressFieldFromAutoFill() {
		if($('#donor_address').attr('autocomplete') == 'off'){
			$('#donor_address').attr('autocomplete', 'new-password');
		}else{
			setTimeout(excludeAddressFieldFromAutoFill, 500);
		}
	}

});