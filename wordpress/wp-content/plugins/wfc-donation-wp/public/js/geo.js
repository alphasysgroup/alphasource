var j = jQuery;
var AS_GEO = function() {

    this.countries = [  
       {  
          "code":"AF",
          "name":"Afghanistan"
       },
       {  
          "code":"AX",
          "name":"Aland Islands"
       },
       {  
          "code":"AL",
          "name":"Albania"
       },
       {  
          "code":"DZ",
          "name":"Algeria"
       },
       {  
          "code":"AD",
          "name":"Andorra"
       },
       {  
          "code":"AO",
          "name":"Angola"
       },
       {  
          "code":"AI",
          "name":"Anguilla"
       },
       {  
          "code":"AQ",
          "name":"Antarctica"
       },
       {  
          "code":"AG",
          "name":"Antigua and Barbuda"
       },
       {  
          "code":"AR",
          "name":"Argentina"
       },
       {  
          "code":"AM",
          "name":"Armenia"
       },
       {  
          "code":"AW",
          "name":"Aruba"
       },
       {  
          "code":"AU",
          "name":"Australia"
       },
       {  
          "code":"AT",
          "name":"Austria"
       },
       {  
          "code":"AZ",
          "name":"Azerbaijan"
       },
       {  
          "code":"BS",
          "name":"Bahamas"
       },
       {  
          "code":"BH",
          "name":"Bahrain"
       },
       {  
          "code":"BD",
          "name":"Bangladesh"
       },
       {  
          "code":"BB",
          "name":"Barbados"
       },
       {  
          "code":"BY",
          "name":"Belarus"
       },
       {  
          "code":"BE",
          "name":"Belgium"
       },
       {  
          "code":"BZ",
          "name":"Belize"
       },
       {  
          "code":"BJ",
          "name":"Benin"
       },
       {  
          "code":"BM",
          "name":"Bermuda"
       },
       {  
          "code":"BT",
          "name":"Bhutan"
       },
       {  
          "code":"BO",
          "name":"Bolivia, Plurinational State of"
       },
       {  
          "code":"BQ",
          "name":"Bonaire, Sint Eustatius and Saba"
       },
       {  
          "code":"BA",
          "name":"Bosnia and Herzegovina"
       },
       {  
          "code":"BW",
          "name":"Botswana"
       },
       {  
          "code":"BV",
          "name":"Bouvet Island"
       },
       {  
          "code":"BR",
          "name":"Brazil"
       },
       {  
          "code":"IO",
          "name":"British Indian Ocean Territory"
       },
       {  
          "code":"BN",
          "name":"Brunei Darussalam"
       },
       {  
          "code":"BG",
          "name":"Bulgaria"
       },
       {  
          "code":"BF",
          "name":"Burkina Faso"
       },
       {  
          "code":"BI",
          "name":"Burundi"
       },
       {  
          "code":"KH",
          "name":"Cambodia"
       },
       {  
          "code":"CM",
          "name":"Cameroon"
       },
       {  
          "code":"CA",
          "name":"Canada"
       },
       {  
          "code":"CV",
          "name":"Cape Verde"
       },
       {  
          "code":"KY",
          "name":"Cayman Islands"
       },
       {  
          "code":"CF",
          "name":"Central African Republic"
       },
       {  
          "code":"TD",
          "name":"Chad"
       },
       {  
          "code":"CL",
          "name":"Chile"
       },
       {  
          "code":"CN",
          "name":"China"
       },
       {  
          "code":"CX",
          "name":"Christmas Island"
       },
       {  
          "code":"CC",
          "name":"Cocos (Keeling) Islands"
       },
       {  
          "code":"CO",
          "name":"Colombia"
       },
       {  
          "code":"KM",
          "name":"Comoros"
       },
       {  
          "code":"CG",
          "name":"Congo"
       },
       {  
          "code":"CD",
          "name":"Congo, the Democratic Republic of the"
       },
       {  
          "code":"CK",
          "name":"Cook Islands"
       },
       {  
          "code":"CR",
          "name":"Costa Rica"
       },
       {  
          "code":"CI",
          "name":"Cote d'Ivoire"
       },
       {  
          "code":"HR",
          "name":"Croatia"
       },
       {  
          "code":"CU",
          "name":"Cuba"
       },
       {  
          "code":"CW",
          "name":"Cura&ccedil;ao"
       },
       {  
          "code":"CY",
          "name":"Cyprus"
       },
       {  
          "code":"CZ",
          "name":"Czech Republic"
       },
       {  
          "code":"DK",
          "name":"Denmark"
       },
       {  
          "code":"DJ",
          "name":"Djibouti"
       },
       {  
          "code":"DM",
          "name":"Dominica"
       },
       {  
          "code":"DO",
          "name":"Dominican Republic"
       },
       {  
          "code":"EC",
          "name":"Ecuador"
       },
       {  
          "code":"EG",
          "name":"Egypt"
       },
       {  
          "code":"SV",
          "name":"El Salvador"
       },
       {  
          "code":"GQ",
          "name":"Equatorial Guinea"
       },
       {  
          "code":"ER",
          "name":"Eritrea"
       },
       {  
          "code":"EE",
          "name":"Estonia"
       },
       {  
          "code":"ET",
          "name":"Ethiopia"
       },
       {  
          "code":"FK",
          "name":"Falkland Islands (Malvinas)"
       },
       {  
          "code":"FO",
          "name":"Faroe Islands"
       },
       {  
          "code":"FJ",
          "name":"Fiji"
       },
       {  
          "code":"FI",
          "name":"Finland"
       },
       {  
          "code":"FR",
          "name":"France"
       },
       {  
          "code":"GF",
          "name":"French Guiana"
       },
       {  
          "code":"PF",
          "name":"French Polynesia"
       },
       {  
          "code":"TF",
          "name":"French Southern Territories"
       },
       {  
          "code":"GA",
          "name":"Gabon"
       },
       {  
          "code":"GM",
          "name":"Gambia"
       },
       {  
          "code":"GE",
          "name":"Georgia"
       },
       {  
          "code":"DE",
          "name":"Germany"
       },
       {  
          "code":"GH",
          "name":"Ghana"
       },
       {  
          "code":"GI",
          "name":"Gibraltar"
       },
       {  
          "code":"GR",
          "name":"Greece"
       },
       {  
          "code":"GL",
          "name":"Greenland"
       },
       {  
          "code":"GD",
          "name":"Grenada"
       },
       {  
          "code":"GP",
          "name":"Guadeloupe"
       },
       {  
          "code":"GT",
          "name":"Guatemala"
       },
       {  
          "code":"GG",
          "name":"Guernsey"
       },
       {  
          "code":"GN",
          "name":"Guinea"
       },
       {  
          "code":"GW",
          "name":"Guinea-Bissau"
       },
       {  
          "code":"GY",
          "name":"Guyana"
       },
       {  
          "code":"HT",
          "name":"Haiti"
       },
       {  
          "code":"HM",
          "name":"Heard Island and McDonald Islands"
       },
       {  
          "code":"VA",
          "name":"Holy See (Vatican City State)"
       },
       {  
          "code":"HN",
          "name":"Honduras"
       },
       {  
          "code":"HK",
          "name":"Hong Kong"
       },
       {  
          "code":"HU",
          "name":"Hungary"
       },
       {  
          "code":"IS",
          "name":"Iceland"
       },
       {  
          "code":"IN",
          "name":"India"
       },
       {  
          "code":"ID",
          "name":"Indonesia"
       },
       {  
          "code":"IR",
          "name":"Iran, Islamic Republic of"
       },
       {  
          "code":"IQ",
          "name":"Iraq"
       },
       {  
          "code":"IE",
          "name":"Ireland"
       },
       {  
          "code":"IM",
          "name":"Isle of Man"
       },
       {  
          "code":"IL",
          "name":"Israel"
       },
       {  
          "code":"IT",
          "name":"Italy"
       },
       {  
          "code":"JM",
          "name":"Jamaica"
       },
       {  
          "code":"JP",
          "name":"Japan"
       },
       {  
          "code":"JE",
          "name":"Jersey"
       },
       {  
          "code":"JO",
          "name":"Jordan"
       },
       {  
          "code":"KZ",
          "name":"Kazakhstan"
       },
       {  
          "code":"KE",
          "name":"Kenya"
       },
       {  
          "code":"KI",
          "name":"Kiribati"
       },
       {  
          "code":"KP",
          "name":"Korea, Democratic People's Republic of"
       },
       {  
          "code":"KR",
          "name":"Korea, Republic of"
       },
       {  
          "code":"KW",
          "name":"Kuwait"
       },
       {  
          "code":"KG",
          "name":"Kyrgyzstan"
       },
       {  
          "code":"LA",
          "name":"Lao People's Democratic Republic"
       },
       {  
          "code":"LV",
          "name":"Latvia"
       },
       {  
          "code":"LB",
          "name":"Lebanon"
       },
       {  
          "code":"LS",
          "name":"Lesotho"
       },
       {  
          "code":"LR",
          "name":"Liberia"
       },
       {  
          "code":"LY",
          "name":"Libya"
       },
       {  
          "code":"LI",
          "name":"Liechtenstein"
       },
       {  
          "code":"LT",
          "name":"Lithuania"
       },
       {  
          "code":"LU",
          "name":"Luxembourg"
       },
       {  
          "code":"MO",
          "name":"Macao"
       },
       {  
          "code":"MK",
          "name":"Macedonia, the former Yugoslav Republic of"
       },
       {  
          "code":"MG",
          "name":"Madagascar"
       },
       {  
          "code":"MW",
          "name":"Malawi"
       },
       {  
          "code":"MY",
          "name":"Malaysia"
       },
       {  
          "code":"MV",
          "name":"Maldives"
       },
       {  
          "code":"ML",
          "name":"Mali"
       },
       {  
          "code":"MT",
          "name":"Malta"
       },
       {  
          "code":"MQ",
          "name":"Martinique"
       },
       {  
          "code":"MR",
          "name":"Mauritania"
       },
       {  
          "code":"MU",
          "name":"Mauritius"
       },
       {  
          "code":"YT",
          "name":"Mayotte"
       },
       {  
          "code":"MX",
          "name":"Mexico"
       },
       {  
          "code":"MD",
          "name":"Moldova, Republic of"
       },
       {  
          "code":"MC",
          "name":"Monaco"
       },
       {  
          "code":"MN",
          "name":"Mongolia"
       },
       {  
          "code":"ME",
          "name":"Montenegro"
       },
       {  
          "code":"MS",
          "name":"Montserrat"
       },
       {  
          "code":"MA",
          "name":"Morocco"
       },
       {  
          "code":"MZ",
          "name":"Mozambique"
       },
       {  
          "code":"MM",
          "name":"Myanmar"
       },
       {  
          "code":"NA",
          "name":"Namibia"
       },
       {  
          "code":"NR",
          "name":"Nauru"
       },
       {  
          "code":"NP",
          "name":"Nepal"
       },
       {  
          "code":"NL",
          "name":"Netherlands"
       },
       {  
          "code":"NC",
          "name":"New Caledonia"
       },
       {  
          "code":"NZ",
          "name":"New Zealand"
       },
       {  
          "code":"NI",
          "name":"Nicaragua"
       },
       {  
          "code":"NE",
          "name":"Niger"
       },
       {  
          "code":"NG",
          "name":"Nigeria"
       },
       {  
          "code":"NU",
          "name":"Niue"
       },
       {  
          "code":"NF",
          "name":"Norfolk Island"
       },
       {  
          "code":"NO",
          "name":"Norway"
       },
       {  
          "code":"OM",
          "name":"Oman"
       },
       {  
          "code":"PK",
          "name":"Pakistan"
       },
       {  
          "code":"PS",
          "name":"Palestine"
       },
       {  
          "code":"PA",
          "name":"Panama"
       },
       {  
          "code":"PG",
          "name":"Papua New Guinea"
       },
       {  
          "code":"PY",
          "name":"Paraguay"
       },
       {  
          "code":"PE",
          "name":"Peru"
       },
       {  
          "code":"PH",
          "name":"Philippines"
       },
       {  
          "code":"PN",
          "name":"Pitcairn"
       },
       {  
          "code":"PL",
          "name":"Poland"
       },
       {  
          "code":"PT",
          "name":"Portugal"
       },
       {  
          "code":"QA",
          "name":"Qatar"
       },
       {  
          "code":"RE",
          "name":"Reunion"
       },
       {  
          "code":"RO",
          "name":"Romania"
       },
       {  
          "code":"RU",
          "name":"Russian Federation"
       },
       {  
          "code":"RW",
          "name":"Rwanda"
       },
       {  
          "code":"BL",
          "name":"Saint Barth&eacute;lemy"
       },
       {  
          "code":"SH",
          "name":"Saint Helena, Ascension and Tristan da Cunha"
       },
       {  
          "code":"KN",
          "name":"Saint Kitts and Nevis"
       },
       {  
          "code":"LC",
          "name":"Saint Lucia"
       },
       {  
          "code":"MF",
          "name":"Saint Martin (French part)"
       },
       {  
          "code":"PM",
          "name":"Saint Pierre and Miquelon"
       },
       {  
          "code":"VC",
          "name":"Saint Vincent and the Grenadines"
       },
       {  
          "code":"WS",
          "name":"Samoa"
       },
       {  
          "code":"SM",
          "name":"San Marino"
       },
       {  
          "code":"ST",
          "name":"Sao Tome and Principe"
       },
       {  
          "code":"SA",
          "name":"Saudi Arabia"
       },
       {  
          "code":"SN",
          "name":"Senegal"
       },
       {  
          "code":"RS",
          "name":"Serbia"
       },
       {  
          "code":"SC",
          "name":"Seychelles"
       },
       {  
          "code":"SL",
          "name":"Sierra Leone"
       },
       {  
          "code":"SG",
          "name":"Singapore"
       },
       {  
          "code":"SX",
          "name":"Sint Maarten (Dutch part)"
       },
       {  
          "code":"SK",
          "name":"Slovakia"
       },
       {  
          "code":"SI",
          "name":"Slovenia"
       },
       {  
          "code":"SB",
          "name":"Solomon Islands"
       },
       {  
          "code":"SO",
          "name":"Somalia"
       },
       {  
          "code":"ZA",
          "name":"South Africa"
       },
       {  
          "code":"GS",
          "name":"South Georgia and the South Sandwich Islands"
       },
       {  
          "code":"SS",
          "name":"South Sudan"
       },
       {  
          "code":"ES",
          "name":"Spain"
       },
       {  
          "code":"LK",
          "name":"Sri Lanka"
       },
       {  
          "code":"SD",
          "name":"Sudan"
       },
       {  
          "code":"SR",
          "name":"Suriname"
       },
       {  
          "code":"SJ",
          "name":"Svalbard and Jan Mayen"
       },
       {  
          "code":"SZ",
          "name":"Swaziland"
       },
       {  
          "code":"SE",
          "name":"Sweden"
       },
       {  
          "code":"CH",
          "name":"Switzerland"
       },
       {  
          "code":"SY",
          "name":"Syrian Arab Republic"
       },
       {  
          "code":"TW",
          "name":"Taiwan"
       },
       {  
          "code":"TJ",
          "name":"Tajikistan"
       },
       {  
          "code":"TZ",
          "name":"Tanzania, United Republic of"
       },
       {  
          "code":"TH",
          "name":"Thailand"
       },
       {  
          "code":"TL",
          "name":"Timor-Leste"
       },
       {  
          "code":"TG",
          "name":"Togo"
       },
       {  
          "code":"TK",
          "name":"Tokelau"
       },
       {  
          "code":"TO",
          "name":"Tonga"
       },
       {  
          "code":"TT",
          "name":"Trinidad and Tobago"
       },
       {  
          "code":"TN",
          "name":"Tunisia"
       },
       {  
          "code":"TR",
          "name":"Turkey"
       },
       {  
          "code":"TM",
          "name":"Turkmenistan"
       },
       {  
          "code":"TC",
          "name":"Turks and Caicos Islands"
       },
       {  
          "code":"TV",
          "name":"Tuvalu"
       },
       {  
          "code":"UG",
          "name":"Uganda"
       },
       {  
          "code":"UA",
          "name":"Ukraine"
       },
       {  
          "code":"AE",
          "name":"United Arab Emirates"
       },
       {  
          "code":"GB",
          "name":"United Kingdom"
       },
       {  
          "code":"US",
          "name":"United States"
       },
       {  
          "code":"UY",
          "name":"Uruguay"
       },
       {  
          "code":"UZ",
          "name":"Uzbekistan"
       },
       {  
          "code":"VU",
          "name":"Vanuatu"
       },
       {  
          "code":"VE",
          "name":"Venezuela, Bolivarian Republic of"
       },
       {  
          "code":"VN",
          "name":"Viet Nam"
       },
       {  
          "code":"VG",
          "name":"Virgin Islands, British"
       },
       {  
          "code":"WF",
          "name":"Wallis and Futuna"
       },
       {  
          "code":"EH",
          "name":"Western Sahara"
       },
       {  
          "code":"YE",
          "name":"Yemen"
       },
       {  
          "code":"ZM",
          "name":"Zambia"
       },
       {  
          "code":"ZW",
          "name":"Zimbabwe"
       }
    ];
    

    this.states = [  
     {  
        "code":"ACT",
        "name":"ACT",
        "c_code": "AU"
     },
     {  
        "code":"NSW",
        "name":"NSW",
        "c_code": "AU"
     },
     {  
        "code":"NT",
        "name":"NT",
        "c_code": "AU"
     },
     {  
        "code":"QLD",
        "name":"QLD",
        "c_code": "AU"
     },
     {  
        "code":"SA",
        "name":"SA",
        "c_code": "AU"
     },
     {  
        "code":"TAS",
        "name":"TAS",
        "c_code": "AU"
     },
     {  
        "code":"VIC",
        "name":"VIC",
        "c_code": "AU"
     },
     {  
        "code":"WA",
        "name":"WA",
        "c_code": "AU"
     },
      {  
        "name":"Acre",
        "code":"AC",
        "c_code": "BR"
     },
     {  
        "name":"Alagoas",
        "code":"AL",
        "c_code": "BR"
     },
     {  
        "name":"Amazonas",
        "code":"AM",
        "c_code": "BR"
     },
     {  
        "name":"AmapÃ¡",
        "code":"AP",
        "c_code": "BR"
     },
     {  
        "name":"Bahia",
        "code":"BA",
        "c_code": "BR"
     },
     {  
        "name":"CearÃ¡",
        "code":"CE",
        "c_code": "BR"
     },
     {  
        "name":"Distrito Federal",
        "code":"DF",
        "c_code": "BR"
     },
     {  
        "name":"EspÃ­rito Santo",
        "code":"ES",
        "c_code": "BR"
     },
     {  
        "name":"GoiÃ¡s",
        "code":"GO",
        "c_code": "BR"
     },
     {  
        "name":"MaranhÃ£o",
        "code":"MA",
        "c_code": "BR"
     },
     {  
        "name":"Minas Gerais",
        "code":"MG",
        "c_code": "BR"
     },
     {  
        "name":"Mato Grosso do Sul",
        "code":"MS",
        "c_code": "BR"
     },
     {  
        "name":"Mato Grosso",
        "code":"MT",
        "c_code": "BR"
     },
     {  
        "name":"Alberta",
        "code":"AB",
        "c_code": "CA"
     },
     {  
        "name":"British Columbia",
        "code":"BC",
        "c_code": "CA"
     },
     {  
        "name":"Manitoba",
        "code":"MB",
        "c_code": "CA"
     },
     {  
        "name":"New Brunswick",
        "code":"NB",
        "c_code": "CA"
     },
     {  
        "name":"Newfoundland and Labrador",
        "code":"NL",
        "c_code": "CA"
     },
     {  
        "name":"Nova Scotia",
        "code":"NS",
        "c_code": "CA"
     },
     {  
        "name":"Northwest Territories",
        "code":"NT",
        "c_code": "CA"
     },
     {  
        "name":"Nunavut",
        "code":"NU",
        "c_code": "CA"
     },
     {  
        "name":"Ontario",
        "code":"ON",
        "c_code": "CA"
     },
     {  
        "name":"Prince Edward Island",
        "code":"PE",
        "c_code": "CA"
     },
     {  
        "name":"Quebec",
        "code":"QC",
        "c_code": "CA"
     },
     {  
        "name":"Saskatchewan",
        "code":"SK",
        "c_code": "CA"
     },
     {  
        "name":"Yukon Territories",
        "code":"YT",
        "c_code": "CA"
     },
     {  
        "name":"ParÃ¡",
        "code":"PA",
        "c_code": "BR"
     },
     {  
        "name":"ParaÃ­ba",
        "code":"PB",
        "c_code": "BR"
     },
     {  
        "name":"Pernambuco",
        "code":"PE",
        "c_code": "BR"
     },
     {  
        "name":"PiauÃ­",
        "code":"PI",
        "c_code": "BR"
     },
     {  
        "name":"ParanÃ¡",
        "code":"PR",
        "c_code": "BR"
     },
     {  
        "name":"Rio de Janeiro",
        "code":"RJ",
        "c_code": "BR"
     },
     {  
        "name":"Rio Grande do Norte",
        "code":"RN",
        "c_code": "BR"
     },
     {  
        "name":"RondÃ´nia",
        "code":"RO",
        "c_code": "BR"
     },
     {  
        "name":"Roraima",
        "code":"RR",
        "c_code": "BR"
     },
     {  
        "name":"Rio Grande do Sul",
        "code":"RS",
        "c_code": "BR"
     },
     {  
        "name":"Santa Catarina",
        "code":"SC",
        "c_code": "BR"
     },
     {  
        "name":"Sergipe",
        "code":"SE",
        "c_code": "BR"
     },
     {  
        "name":"SÃ£o Paulo",
        "code":"SP",
        "c_code": "BR"
     },
     {  
        "name":"Tocantins",
        "code":"TO",
        "c_code": "BR"
     },
     {  
        "name":"Beijing",
        "code":"11",
        "c_code": "CN"
     },
     {  
        "name":"Tianjin",
        "code":"12",
        "c_code": "CN"
     },
     {  
        "name":"Hebei",
        "code":"13",
        "c_code": "CN"
     },
     {  
        "name":"Shanxi",
        "code":"14",
        "c_code": "CN"
     },
     {  
        "name":"Nei Mongol",
        "code":"15",
        "c_code": "CN"
     },
     {  
        "name":"Liaoning",
        "code":"21",
        "c_code": "CN"
     },
     {  
        "name":"Jilin",
        "code":"22",
        "c_code": "CN"
     },
     {  
        "name":"Heilongjiang",
        "code":"23",
        "c_code": "CN"
     },
     {  
        "name":"Shanghai",
        "code":"31",
        "c_code": "CN"
     },
     {  
        "name":"Jiangsu",
        "code":"32",
        "c_code": "CN"
     },
     {  
        "name":"Zhejiang",
        "code":"33",
        "c_code": "CN"
     },
     {  
        "name":"Anhui",
        "code":"34",
        "c_code": "CN"
     },
     {  
        "name":"Fujian",
        "code":"35",
        "c_code": "CN"
     },
     {  
        "name":"Jiangxi",
        "code":"36",
        "c_code": "CN"
     },
     {  
        "name":"Shandong",
        "code":"37",
        "c_code": "CN"
     },
     {  
        "name":"Henan",
        "code":"41",
        "c_code": "CN"
     },
     {  
        "name":"Hubei",
        "code":"42",
        "c_code": "CN"
     },
     {  
        "name":"Hunan",
        "code":"43",
        "c_code": "CN"
     },
     {  
        "name":"Guangdong",
        "code":"44",
        "c_code": "CN"
     },
     {  
        "name":"Guangxi",
        "code":"45",
        "c_code": "CN"
     },
     {  
        "name":"Hainan",
        "code":"46",
        "c_code": "CN"
     },
     {  
        "name":"Chongqing",
        "code":"50",
        "c_code": "CN"
     },
     {  
        "name":"Sichuan",
        "code":"51",
        "c_code": "CN"
     },
     {  
        "name":"Guizhou",
        "code":"52",
        "c_code": "CN"
     },
     {  
        "name":"Yunnan",
        "code":"53",
        "c_code": "CN"
     },
     {  
        "name":"Xizang",
        "code":"54",
        "c_code": "CN"
     },
     {  
        "name":"Shaanxi",
        "code":"61",
        "c_code": "CN"
     },
     {  
        "name":"Gansu",
        "code":"62",
        "c_code": "CN"
     },
     {  
        "name":"Qinghai",
        "code":"63",
        "c_code": "CN"
     },
     {  
        "name":"Ningxia",
        "code":"64",
        "c_code": "CN"
     },
     {  
        "name":"Xinjiang",
        "code":"65",
        "c_code": "CN"
     },
     {  
        "name":"Taiwan",
        "code":"71",
        "c_code": "CN"
     },
     {  
        "name":"Hong Kong",
        "code":"91",
        "c_code": "CN"
     },
     {  
        "name":"Macao",
        "code":"92",
        "c_code":"CN"
     },
     {  
        "name":"Clare",
        "code":"CE",
        "c_code":"IE"
     },
     {  
        "name":"Cavan",
        "code":"CN",
        "c_code":"IE"
     },
     {  
        "name":"Cork",
        "code":"CO",
        "c_code":"IE"
     },
     {  
        "name":"Carlow",
        "code":"CW",
        "c_code":"IE"
     },
     {  
        "name":"Dublin",
        "code":"D",
        "c_code":"IE"
     },
     {  
        "name":"Donegal",
        "code":"DL",
        "c_code":"IE"
     },
     {  
        "name":"Galway",
        "code":"G",
        "c_code":"IE"
     },
     {  
        "name":"Kildare",
        "code":"KE",
        "c_code":"IE"
     },
     {  
        "name":"Kilkenny",
        "code":"KK",
        "c_code":"IE"
     },
     {  
        "name":"Kerry",
        "code":"KY",
        "c_code":"IE"
     },
     {  
        "name":"Longford",
        "code":"LD",
        "c_code":"IE"
     },
     {  
        "name":"Louth",
        "code":"LH",
        "c_code":"IE"
     },
     {  
        "name":"Limerick",
        "code":"LK",
        "c_code":"IE"
     },
     {  
        "name":"Leitrim",
        "code":"LM",
        "c_code":"IE"
     },
     {  
        "name":"Laois",
        "code":"LS",
        "c_code":"IE"
     },
     {  
        "name":"Meath",
        "code":"MH",
        "c_code":"IE"
     },
     {  
        "name":"Monaghan",
        "code":"MN",
        "c_code":"IE"
     },
     {  
        "name":"Mayo",
        "code":"MO",
        "c_code":"IE"
     },
     {  
        "name":"Offaly",
        "code":"OY",
        "c_code":"IE"
     },
     {  
        "name":"Roscommon",
        "code":"RN",
        "c_code":"IE"
     },
     {  
        "name":"Sligo",
        "code":"SO",
        "c_code":"IE"
     },
     {  
        "name":"Tipperary",
        "code":"TA",
        "c_code":"IE"
     },
     {  
        "name":"Waterford",
        "code":"WD",
        "c_code":"IE"
     },
     {  
        "name":"Westmeath",
        "code":"WH",
        "c_code":"IE"
     },
     {  
        "name":"Wicklow",
        "code":"WW",
        "c_code":"IE"
     },
     {  
        "name":"Wexford",
        "code":"WX",
        "c_code":"IE"
     },
     {  
        "name":"Andaman and Nicobar Islands",
        "code":"AN",
        "c_code":"IN"
     },
     {  
        "name":"Andhra Pradesh",
        "code":"AP",
        "c_code":"IN"
     },
     {  
        "name":"Arunachal Pradesh",
        "code":"AR",
        "c_code":"IN"
     },
     {  
        "name":"Assam",
        "code":"AS",
        "c_code":"IN"
     },
     {  
        "name":"Bihar",
        "code":"BR",
        "c_code":"IN"
     },
     {  
        "name":"Chandigarh",
        "code":"CH",
        "c_code":"IN"
     },
     {  
        "name":"Chhattisgarh",
        "code":"CT",
        "c_code":"IN"
     },
     {  
        "name":"Daman and Diu",
        "code":"DD",
        "c_code":"IN"
     },
     {  
        "name":"Delhi",
        "code":"DL",
        "c_code":"IN"
     },
     {  
        "name":"Dadra and Nagar Haveli",
        "code":"DN",
        "c_code":"IN"
     },
     {  
        "name":"Goa",
        "code":"GA",
        "c_code":"IN"
     },
     {  
        "name":"Gujarat",
        "code":"GJ",
        "c_code":"IN"
     },
     {  
        "name":"Himachal Pradesh",
        "code":"HP",
        "c_code":"IN"
     },
     {  
        "name":"Haryana",
        "code":"HR",
        "c_code":"IN"
     },
     {  
        "name":"Jharkhand",
        "code":"JH",
        "c_code":"IN"
     },
     {  
        "name":"Jammu and Kashmir",
        "code":"JK",
        "c_code":"IN"
     },
     {  
        "name":"Karnataka",
        "code":"KA",
        "c_code":"IN"
     },
     {  
        "name":"Kerala",
        "code":"KL",
        "c_code":"IN"
     },
     {  
        "name":"Lakshadweep",
        "code":"LD",
        "c_code":"IN"
     },
     {  
        "name":"Maharashtra",
        "code":"MH",
        "c_code":"IN"
     },
     {  
        "name":"Meghalaya",
        "code":"ML",
        "c_code":"IN"
     },
     {  
        "name":"Manipur",
        "code":"MN",
        "c_code":"IN"
     },
     {  
        "name":"Madhya Pradesh",
        "code":"MP",
        "c_code":"IN"
     },
     {  
        "name":"Mizoram",
        "code":"MZ",
        "c_code":"IN"
     },
     {  
        "name":"Nagaland",
        "code":"NL",
        "c_code":"IN"
     },
     {  
        "name":"Odisha",
        "code":"OR",
        "c_code":"IN"
     },
     {  
        "name":"Punjab",
        "code":"PB",
        "c_code":"IN"
     },
     {  
        "name":"Puducherry",
        "code":"PY",
        "c_code":"IN"
     },
     {  
        "name":"Rajasthan",
        "code":"RJ",
        "c_code":"IN"
     },
     {  
        "name":"Sikkim",
        "code":"SK",
        "c_code":"IN"
     },
     {  
        "name":"Tamil Nadu",
        "code":"TN",
        "c_code":"IN"
     },
     {  
        "name":"Tripura",
        "code":"TR",
        "c_code":"IN"
     },
     {  
        "name":"Uttar Pradesh",
        "code":"UP",
        "c_code":"IN"
     },
     {  
        "name":"Uttarakhand",
        "code":"UT",
        "c_code":"IN"
     },
     {  
        "name":"West Bengal",
        "code":"WB",
        "c_code":"IN"
     },
     {  
      "name":"Agrigento",
      "code":"AG",
      "c_code":"IT"
     },
     {  
        "name":"Alessandria",
        "code":"AL",
        "c_code":"IT"
     },
     {  
        "name":"Ancona",
        "code":"AN",
        "c_code":"IT"
     },
     {  
        "name":"Aosta",
        "code":"AO",
        "c_code":"IT"
     },
     {  
        "name":"Ascoli Piceno",
        "code":"AP",
        "c_code":"IT"
     },
     {  
        "name":"L'Aquila",
        "code":"AQ",
        "c_code":"IT"
     },
     {  
        "name":"Arezzo",
        "code":"AR",
        "c_code":"IT"
     },
     {  
        "name":"Asti",
        "code":"AT",
        "c_code":"IT"
     },
     {  
        "name":"Avellino",
        "code":"AV",
        "c_code":"IT"
     },
     {  
        "name":"Bari",
        "code":"BA",
        "c_code":"IT"
     },
     {  
        "name":"Bergamo",
        "code":"BG",
        "c_code":"IT"
     },
     {  
        "name":"Biella",
        "code":"BI",
        "c_code":"IT"
     },
     {  
        "name":"Belluno",
        "code":"BL",
        "c_code":"IT"
     },
     {  
        "name":"Benevento",
        "code":"BN",
        "c_code":"IT"
     },
     {  
        "name":"Bologna",
        "code":"BO",
        "c_code":"IT"
     },
     {  
        "name":"Brindisi",
        "code":"BR",
        "c_code":"IT"
     },
     {  
        "name":"Brescia",
        "code":"BS",
        "c_code":"IT"
     },
     {  
        "name":"Barletta-Andria-Trani",
        "code":"BT",
        "c_code":"IT"
     },
     {  
        "name":"Bolzano",
        "code":"BZ",
        "c_code":"IT"
     },
     {  
        "name":"Cagliari",
        "code":"CA",
        "c_code":"IT"
     },
     {  
        "name":"Campobasso",
        "code":"CB",
        "c_code":"IT"
     },
     {  
        "name":"Caserta",
        "code":"CE",
        "c_code":"IT"
     },
     {  
        "name":"Chieti",
        "code":"CH",
        "c_code":"IT"
     },
     {  
        "name":"Carbonia-Iglesias",
        "code":"CI",
        "c_code":"IT"
     },
     {  
        "name":"Caltanissetta",
        "code":"CL",
        "c_code":"IT"
     },
     {  
        "name":"Cuneo",
        "code":"CN",
        "c_code":"IT"
     },
     {  
        "name":"Como",
        "code":"CO",
        "c_code":"IT"
     },
     {  
        "name":"Cremona",
        "code":"CR",
        "c_code":"IT"
     },
     {  
        "name":"Cosenza",
        "code":"CS",
        "c_code":"IT"
     },
     {  
        "name":"Catania",
        "code":"CT",
        "c_code":"IT"
     },
     {  
        "name":"Catanzaro",
        "code":"CZ",
        "c_code":"IT"
     },
     {  
        "name":"Enna",
        "code":"EN",
        "c_code":"IT"
     },
     {  
        "name":"ForlÃ¬-Cesena",
        "code":"FC",
        "c_code":"IT"
     },
     {  
        "name":"Ferrara",
        "code":"FE",
        "c_code":"IT"
     },
     {  
        "name":"Foggia",
        "code":"FG",
        "c_code":"IT"
     },
     {  
        "name":"Florence",
        "code":"FI",
        "c_code":"IT"
     },
     {  
        "name":"Fermo",
        "code":"FM",
        "c_code":"IT"
     },
     {  
        "name":"Frosinone",
        "code":"FR",
        "c_code":"IT"
     },
     {  
        "name":"Genoa",
        "code":"GE",
        "c_code":"IT"
     },
     {  
        "name":"Gorizia",
        "code":"GO",
        "c_code":"IT"
     },
     {  
        "name":"Grosseto",
        "code":"GR",
        "c_code":"IT"
     },
     {  
        "name":"Imperia",
        "code":"IM",
        "c_code":"IT"
     },
     {  
        "name":"Isernia",
        "code":"IS",
        "c_code":"IT"
     },
     {  
        "name":"Crotone",
        "code":"KR",
        "c_code":"IT"
     },
     {  
        "name":"Lecco",
        "code":"LC",
        "c_code":"IT"
     },
     {  
        "name":"Lecce",
        "code":"LE",
        "c_code":"IT"
     },
     {  
        "name":"Livorno",
        "code":"LI",
        "c_code":"IT"
     },
     {  
        "name":"Lodi",
        "code":"LO",
        "c_code":"IT"
     },
     {  
        "name":"Latina",
        "code":"LT",
        "c_code":"IT"
     },
     {  
        "name":"Lucca",
        "code":"LU",
        "c_code":"IT"
     },
     {  
        "name":"Monza and Brianza",
        "code":"MB",
        "c_code":"IT"
     },
     {  
        "name":"Macerata",
        "code":"MC",
        "c_code":"IT"
     },
     {  
        "name":"Messina",
        "code":"ME",
        "c_code":"IT"
     },
     {  
        "name":"Milan",
        "code":"MI",
        "c_code":"IT"
     },
     {  
        "name":"Mantua",
        "code":"MN",
        "c_code":"IT"
     },
     {  
        "name":"Modena",
        "code":"MO",
        "c_code":"IT"
     },
     {  
        "name":"Massa and Carrara",
        "code":"MS",
        "c_code":"IT"
     },
     {  
        "name":"Matera",
        "code":"MT",
        "c_code":"IT"
     },
     {  
        "name":"Naples",
        "code":"NA",
        "c_code":"IT"
     },
     {  
        "name":"Novara",
        "code":"NO",
        "c_code":"IT"
     },
     {  
        "name":"Nuoro",
        "code":"NU",
        "c_code":"IT"
     },
     {  
        "name":"Ogliastra",
        "code":"OG",
        "c_code":"IT"
     },
     {  
        "name":"Oristano",
        "code":"OR",
        "c_code":"IT"
     },
     {  
        "name":"Olbia-Tempio",
        "code":"OT",
        "c_code":"IT"
     },
     {  
        "name":"Palermo",
        "code":"PA",
        "c_code":"IT"
     },
     {  
        "name":"Piacenza",
        "code":"PC",
        "c_code":"IT"
     },
     {  
        "name":"Padua",
        "code":"PD",
        "c_code":"IT"
     },
     {  
        "name":"Pescara",
        "code":"PE",
        "c_code":"IT"
     },
     {  
        "name":"Perugia",
        "code":"PG",
        "c_code":"IT"
     },
     {  
        "name":"Pisa",
        "code":"PI",
        "c_code":"IT"
     },
     {  
        "name":"Pordenone",
        "code":"PN",
        "c_code":"IT"
     },
     {  
        "name":"Prato",
        "code":"PO",
        "c_code":"IT"
     },
     {  
        "name":"Parma",
        "code":"PR",
        "c_code":"IT"
     },
     {  
        "name":"Pistoia",
        "code":"PT",
        "c_code":"IT"
     },
     {  
        "name":"Pesaro and Urbino",
        "code":"PU",
        "c_code":"IT"
     },
     {  
        "name":"Pavia",
        "code":"PV",
        "c_code":"IT"
     },
     {  
        "name":"Potenza",
        "code":"PZ",
        "c_code":"IT"
     },
     {  
        "name":"Ravenna",
        "code":"RA",
        "c_code":"IT"
     },
     {  
        "name":"Reggio Calabria",
        "code":"RC",
        "c_code":"IT"
     },
     {  
        "name":"Reggio Emilia",
        "code":"RE",
        "c_code":"IT"
     },
     {  
        "name":"Ragusa",
        "code":"RG",
        "c_code":"IT"
     },
     {  
        "name":"Rieti",
        "code":"RI",
        "c_code":"IT"
     },
     {  
        "name":"Rome",
        "code":"RM",
        "c_code":"IT"
     },
     {  
        "name":"Rimini",
        "code":"RN",
        "c_code":"IT"
     },
     {  
        "name":"Rovigo",
        "code":"RO",
        "c_code":"IT"
     },
     {  
        "name":"Salerno",
        "code":"SA",
        "c_code":"IT"
     },
     {  
        "name":"Siena",
        "code":"SI",
        "c_code":"IT"
     },
     {  
        "name":"Sondrio",
        "code":"SO",
        "c_code":"IT"
     },
     {  
        "name":"La Spezia",
        "code":"SP",
        "c_code":"IT"
     },
     {  
        "name":"Syracuse",
        "code":"SR",
        "c_code":"IT"
     },
     {  
        "name":"Sassari",
        "code":"SS",
        "c_code":"IT"
     },
     {  
        "name":"Savona",
        "code":"SV",
        "c_code":"IT"
     },
     {  
        "name":"Taranto",
        "code":"TA",
        "c_code":"IT"
     },
     {  
        "name":"Teramo",
        "code":"TE",
        "c_code":"IT"
     },
     {  
        "name":"Trento",
        "code":"TN",
        "c_code":"IT"
     },
     {  
        "name":"Turin",
        "code":"TO",
        "c_code":"IT"
     },
     {  
        "name":"Trapani",
        "code":"TP",
        "c_code":"IT"
     },
     {  
        "name":"Terni",
        "code":"TR",
        "c_code":"IT"
     },
     {  
        "name":"Trieste",
        "code":"TS",
        "c_code":"IT"
     },
     {  
        "name":"Treviso",
        "code":"TV",
        "c_code":"IT"
     },
     {  
        "name":"Udine",
        "code":"UD",
        "c_code":"IT"
     },
     {  
        "name":"Varese",
        "code":"VA",
        "c_code":"IT"
     },
     {  
        "name":"Verbano-Cusio-Ossola",
        "code":"VB",
        "c_code":"IT"
     },
     {  
        "name":"Vercelli",
        "code":"VC",
        "c_code":"IT"
     },
     {  
        "name":"Venice",
        "code":"VE",
        "c_code":"IT"
     },
     {  
        "name":"Vicenza",
        "code":"VI",
        "c_code":"IT"
     },
     {  
        "name":"Verona",
        "code":"VR",
        "c_code":"IT"
     },
     {  
        "name":"Medio Campidano",
        "code":"VS",
        "c_code":"IT"
     },
     {  
        "name":"Viterbo",
        "code":"VT",
        "c_code":"IT"
     },
     {  
        "name":"Vibo Valentia",
        "code":"VV",
        "c_code":"IT"
     }, 
     {  
        "name":"Aguascalientes",
        "code":"AG",
        "c_code":"MX"
     },
     {  
        "name":"Baja California",
        "code":"BC",
        "c_code":"MX"
     },
     {  
        "name":"Baja California Sur",
        "code":"BS",
        "c_code":"MX"
     },
     {  
        "name":"Chihuahua",
        "code":"CH",
        "c_code":"MX"
     },
     {  
        "name":"Colima",
        "code":"CL",
        "c_code":"MX"
     },
     {  
        "name":"Campeche",
        "code":"CM",
        "c_code":"MX"
     },
     {  
        "name":"Coahuila",
        "code":"CO",
        "c_code":"MX"
     },
     {  
        "name":"Chiapas",
        "code":"CS",
        "c_code":"MX"
     },
     {  
        "name":"Federal District",
        "code":"DF",
        "c_code":"MX"
     },
     {  
        "name":"Durango",
        "code":"DG",
        "c_code":"MX"
     },
     {  
        "name":"Guerrero",
        "code":"GR",
        "c_code":"MX"
     },
     {  
        "name":"Guanajuato",
        "code":"GT",
        "c_code":"MX"
     },
     {  
        "name":"Hidalgo",
        "code":"HG",
        "c_code":"MX"
     },
     {  
        "name":"Jalisco",
        "code":"JA",
        "c_code":"MX"
     },
     {  
        "name":"Mexico State",
        "code":"ME",
        "c_code":"MX"
     },
     {  
        "name":"MichoacÃ¡n",
        "code":"MI",
        "c_code":"MX"
     },
     {  
        "name":"Morelos",
        "code":"MO",
        "c_code":"MX"
     },
     {  
        "name":"Nayarit",
        "code":"NA",
        "c_code":"MX"
     },
     {  
        "name":"Nuevo LeÃ³n",
        "code":"NL",
        "c_code":"MX"
     },
     {  
        "name":"Oaxaca",
        "code":"OA",
        "c_code":"MX"
     },
     {  
        "name":"Puebla",
        "code":"PB",
        "c_code":"MX"
     },
     {  
        "name":"QuerÃ©taro",
        "code":"QE",
        "c_code":"MX"
     },
     {  
        "name":"Quintana Roo",
        "code":"QR",
        "c_code":"MX"
     },
     {  
        "name":"Sinaloa",
        "code":"SI",
        "c_code":"MX"
     },
     {  
        "name":"San Luis PotosÃ­",
        "code":"SL",
        "c_code":"MX"
     },
     {  
        "name":"Sonora",
        "code":"SO",
        "c_code":"MX"
     },
     {  
        "name":"Tabasco",
        "code":"TB",
        "c_code":"MX"
     },
     {  
        "name":"Tlaxcala",
        "code":"TL",
        "c_code":"MX"
     },
     {  
        "name":"Tamaulipas",
        "code":"TM",
        "c_code":"MX"
     },
     {  
        "name":"Veracruz",
        "code":"VE",
        "c_code":"MX"
     },
     {  
        "name":"YucatÃ¡n",
        "code":"YU",
        "c_code":"MX"
     },
     {  
        "name":"Zacatecas",
        "code":"ZA",
        "c_code":"MX"
     },
     {  
        "name":"Armed Forces Americas",
        "code":"AA",
        "c_code":"US"
     },
     {  
        "name":"Armed Forces Europe",
        "code":"AE",
        "c_code":"US"
     },
     {  
        "name":"Alaska",
        "code":"AK",
        "c_code":"US"
     },
     {  
        "name":"Alabama",
        "code":"AL",
        "c_code":"US"
     },
     {  
        "name":"Armed Forces Pacific",
        "code":"AP",
        "c_code":"US"
     },
     {  
        "name":"Arkansas",
        "code":"AR",
        "c_code":"US"
     },
     {  
        "name":"American Samoa",
        "code":"AS",
        "c_code":"US"
     },
     {  
        "name":"Arizona",
        "code":"AZ",
        "c_code":"US"
     },
     {  
        "name":"California",
        "code":"CA",
        "c_code":"US"
     },
     {  
        "name":"Colorado",
        "code":"CO",
        "c_code":"US"
     },
     {  
        "name":"Connecticut",
        "code":"CT",
        "c_code":"US"
     },
     {  
        "name":"District of Columbia",
        "code":"DC",
        "c_code":"US"
     },
     {  
        "name":"Delaware",
        "code":"DE",
        "c_code":"US"
     },
     {  
        "name":"Florida",
        "code":"FL",
        "c_code":"US"
     },
     {  
        "name":"Federated Micronesia",
        "code":"FM",
        "c_code":"US"
     },
     {  
        "name":"Georgia",
        "code":"GA",
        "c_code":"US"
     },
     {  
        "name":"Guam",
        "code":"GU",
        "c_code":"US"
     },
     {  
        "name":"Hawaii",
        "code":"HI",
        "c_code":"US"
     },
     {  
        "name":"Iowa",
        "code":"IA",
        "c_code":"US"
     },
     {  
        "name":"Idaho",
        "code":"ID",
        "c_code":"US"
     },
     {  
        "name":"Illinois",
        "code":"IL",
        "c_code":"US"
     },
     {  
        "name":"Indiana",
        "code":"IN",
        "c_code":"US"
     },
     {  
        "name":"Kansas",
        "code":"KS",
        "c_code":"US"
     },
     {  
        "name":"Kentucky",
        "code":"KY",
        "c_code":"US"
     },
     {  
        "name":"Louisiana",
        "code":"LA",
        "c_code":"US"
     },
     {  
        "name":"Massachusetts",
        "code":"MA",
        "c_code":"US"
     },
     {  
        "name":"Maryland",
        "code":"MD",
        "c_code":"US"
     },
     {  
        "name":"Maine",
        "code":"ME",
        "c_code":"US"
     },
     {  
        "name":"Marshall Islands",
        "code":"MH",
        "c_code":"US"
     },
     {  
        "name":"Michigan",
        "code":"MI",
        "c_code":"US"
     },
     {  
        "name":"Minnesota",
        "code":"MN",
        "c_code":"US"
     },
     {  
        "name":"Missouri",
        "code":"MO",
        "c_code":"US"
     },
     {  
        "name":"Northern Mariana Islands",
        "code":"MP",
        "c_code":"US"
     },
     {  
        "name":"Mississippi",
        "code":"MS",
        "c_code":"US"
     },
     {  
        "name":"Montana",
        "code":"MT",
        "c_code":"US"
     },
     {  
        "name":"North Carolina",
        "code":"NC",
        "c_code":"US"
     },
     {  
        "name":"North Dakota",
        "code":"ND",
        "c_code":"US"
     },
     {  
        "name":"Nebraska",
        "code":"NE",
        "c_code":"US"
     },
     {  
        "name":"New Hampshire",
        "code":"NH",
        "c_code":"US"
     },
     {  
        "name":"New Jersey",
        "code":"NJ",
        "c_code":"US"
     },
     {  
        "name":"New Mexico",
        "code":"NM",
        "c_code":"US"
     },
     {  
        "name":"Nevada",
        "code":"NV",
        "c_code":"US"
     },
     {  
        "name":"New York",
        "code":"NY",
        "c_code":"US"
     },
     {  
        "name":"Ohio",
        "code":"OH",
        "c_code":"US"
     },
     {  
        "name":"Oklahoma",
        "code":"OK",
        "c_code":"US"
     },
     {  
        "name":"Oregon",
        "code":"OR",
        "c_code":"US"
     },
     {  
        "name":"Pennsylvania",
        "code":"PA",
        "c_code":"US"
     },
     {  
        "name":"Puerto Rico",
        "code":"PR",
        "c_code":"US"
     },
     {  
        "name":"Palau",
        "code":"PW",
        "c_code":"US"
     },
     {  
        "name":"Rhode Island",
        "code":"RI",
        "c_code":"US"
     },
     {  
        "name":"South Carolina",
        "code":"SC",
        "c_code":"US"
     },
     {  
        "name":"South Dakota",
        "code":"SD",
        "c_code":"US"
     },
     {  
        "name":"Tennessee",
        "code":"TN",
        "c_code":"US"
     },
     {  
        "name":"Texas",
        "code":"TX",
        "c_code":"US"
     },
     {  
        "name":"United States Minor Outlying Islands",
        "code":"UM",
        "c_code":"US"
     },
     {  
        "name":"Utah",
        "code":"UT",
        "c_code":"US"
     },
     {  
        "name":"Virginia",
        "code":"VA",
        "c_code":"US"
     },
     {  
        "name":"US Virgin Islands",
        "code":"VI",
        "c_code":"US"
     },
     {  
        "name":"Vermont",
        "code":"VT",
        "c_code":"US"
     },
     {  
        "name":"Washington",
        "code":"WA",
        "c_code":"US"
     },
     {  
        "name":"Wisconsin",
        "code":"WI",
        "c_code":"US"
     },
     {  
        "name":"West Virginia",
        "code":"WV",
        "c_code":"US"
     },
     {  
        "name":"Wyoming",
        "code":"WY",
        "c_code":"US"
     }
  ];
};

AS_GEO.prototype.populateCountry = function( elemCountry, elemState ) {
  var self = this;
  var countryElement = j('#'+elemCountry);
  self.generateOption( countryElement, '', 'Select Country' );

  j.each( self.countries, function( k, v ) {
    self.generateOption( countryElement, v.name, v.name, v.code );
  } );

  countryElement.on( 'change', function( e ) {
    self.populateState( elemState, j('option:selected', this).attr('code') );
  } ).change();
};

AS_GEO.prototype.populateState = function( elemState, value ) {
  var self = this;
  var stateElement = j('#'+elemState);
  var countryStates = self.findState( value );
  if( countryStates.length ) {
    var attr = self.getStateAttribute( stateElement );
    var elemS = self.selectState( elemState, attr );
    self.generateOption( elemS, '', 'Select State' );
    j.each( countryStates, function( k, v ) {
      self.generateOption( elemS, v.code, v.name );
    } );
  } else {
    var attr = self.getStateAttribute( stateElement );
    var elemS = self.textState( elemState, attr );
  }
};

AS_GEO.prototype.generateOption = function( elem, k, v, code ) {
  elem.append(j('<option>', { 
      value: k,
      text : v,
      code : code
  }));
};

AS_GEO.prototype.findState = function( value ) {
  var self = this;
  var arr = [];
  j.each( self.states, function( k, v ) {
    if( v.c_code == value ) {
      arr.push( v );
    }
  } );
  return arr;
};

AS_GEO.prototype.textState = function( elemState, attr ) {
  var self = this;
  var stateElement = j('#'+elemState);
  stateElement.after( "<input type='text' "+attr+"/>" ).remove();
  return j('#'+elemState);
};

AS_GEO.prototype.selectState = function( elemState, attr ) {
  var self = this;
  var stateElement = j('#'+elemState);
  stateElement.after( "<select "+attr+"></select>" ).remove();
  return j('#'+elemState);
};

AS_GEO.prototype.getStateAttribute = function( stateElement ) {
  var attrs = '';
  j.each(stateElement[0].attributes, function( k, v ) {
    attrs[v.name] = v.value;
    attrs += v.name +'="'+v.value+'" ';
  });
  return attrs;
};