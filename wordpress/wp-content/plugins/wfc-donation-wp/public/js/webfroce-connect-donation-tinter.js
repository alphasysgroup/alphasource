var WFC_Donation_Tinter = {};

(function($) {
    WFC_Donation_Tinter = {
        tinterClass: 'saiyan-tinter',
        tinterUI: $('<div></div>').addClass('saiyan-tinter')
            .css('cssText', 'width: 100% !important; height: 100% !important')
            .css({
                'position': 'fixed',
                'display': 'flex',
                'background-color': 'rgba(0, 0, 0, 0.8)',
                'top': '0',
                'left': '0',
                // 'width': '100vw',
                // 'height': '100vh',
                'opacity': '0.0',
                'z-index': '9980'
            }).append(
            $('<img />').attr('src', wfc_don_tinter.plugin_dir + 'css/img/loading_tail.svg').css({
                'width': '200px',
                'height': '200px',
                'margin': 'auto'
            })
        ),

        show: function() {
            var body = $('body');

            if (body.find('.' + WFC_Donation_Tinter.tinterClass).length === 0) {
                body.append(
                    this.tinterUI.animate({
                        'opacity': '1.0'
                    }, 300, 'easeOutCubic')
                )
            }
        },

        hide: function() {
            var body = $('body');
            var tinter = body.find('.' + WFC_Donation_Tinter.tinterClass);

            if (tinter.length === 0) console.log('Saiyan tinter is already closed');
            else {
                tinter.animate({
                    'opacity': '0.0'
                }, 300, 'easeOutCubic', function() {
                    tinter.remove();
                })
            }
        }
    }
})(jQuery);