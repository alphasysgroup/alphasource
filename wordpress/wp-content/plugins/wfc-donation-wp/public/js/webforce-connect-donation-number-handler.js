jQuery(document).ready(function($){

    var phone = $('#donor_phone');
    var ccn = $('#card_number');
    var ccv = $('#ccv');

    /*
    *Adding Limit to Custom Amount Field.
    */

    $('#pdwp_custom_amount').on('change keyup blur', function (event){

        var value = $(this).val();
        // $(this).attr('min', 1);

        if($(this).attr('step') == '0.01'){
            //$(this).forceNumeric().val($(this).val());
        }else{
            $(this).forceNumeric().val($(this).val().replace(/[^\d].+/, ''));
        }

        /*if ((event.which < 48 || event.which > 57  ||  event.which >= 96 && event.which <= 105 ||
            event.which === 8)) {
            event.preventDefault();
        }*/

        if ((value !== '') && (value.indexOf('.') === -1)) {
           // $(this).val(Math.max(Math.min(value, 10000)));
        }
    });

    /*
    *Phone Number Validation
    */

    phone.intlTelInput(
        defaults = {
            autoHideDialCode: true,
            preferredCountries: ['au'],
            separateDialCode: false,
            nationalMode: false,
            autoPlaceholder: 'off',
            hiddenInput: 'donor_dialcode'
        },
        keys = {
            PLUS: 43,
            SPACE: 32
        }
    );

    phone.on('change keypress keyup', function (event){

        var intlNumber = phone.intlTelInput('getNumber');
        var error = $(this).intlTelInput('getValidationError');

        if(error === intlTelInputUtils.validationError.IS_POSSIBLE) {
            $(this).css( 'border-left','green 2px solid' );
        }
        else if(error === intlTelInputUtils.validationError.TOO_SHORT ||
                error === intlTelInputUtils.validationError.TOO_LONG ||
                error === intlTelInputUtils.validationError.INVALID_COUNTRY_CODE ||
                error === intlTelInputUtils.validationError.NOT_A_NUMBER) {
            $(this).css( 'border-left','#ffcf65 2px solid' );
        }
        else{
            $(this).css( 'border-left','#ffcf65 2px solid' );
        }

        phone.forceNumeric().val(intlNumber);

        /*if ((event.which < 48 || event.which > 57  ||  event.which >= 96 && event.which <= 105 ||
            event.which === 8)) {
            event.preventDefault();
            $(this).attr( 'maxlength', 19 );
        }*/
    });

    phone.on('keyup blur', function () {

        var val = phone.val();

        if( val === '' ) {
            $(this).removeAttr( 'style' );
        }
        $('#donor_dialcode').val(val);
    });

    /*
     *Disable Auto-fill on credit card fields
     */
    if (ccn.length > 0) {
        $('#name_on_card').attr( 'autocomplete', 'cc-name' ).val('');
        ccn.attr( 'autocomplete', 'cc-number' ).val('');
        ccv.attr( 'autocomplete', 'cc-csc' ).val('');
    }

    /*
     *Validate Card Number
     */
    if (ccn.length > 0) {
        ccn.on('change blur keypress keyup', function (e) {
            $(this).forceNumeric(e);
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which < 96 || e.which > 105)) {
                var newValue = $(this).val().replace(/[^0-9]/g,'');
                $(this).val(newValue);
                return false;
            }
        });
    }

    ccn.on('change blur paste keyup focus focusout', function (e) {
        var newValue = $(this).val().replace(/[^0-9]/g,'');
        $(this).val(newValue);
    });

    if (ccn.length > 0) {
        ccn.validateCreditCard(function(result) {

            var validluhn = result.luhn_valid;

            if(result.card_type === null) {
                $(this).removeClass().removeAttr( 'style' );
            }
            else {

                var creditcard_type = result.card_type.name;
                $('#creditcard_type').val(creditcard_type.toUpperCase());

                $(this).addClass( result.card_type.name );
                $(this).attr( 'maxlength', result.card_type.valid_length );

                if( validluhn === true ) {
                    $(this).css( 'border-left','green 2px solid' );
                }
                else {
                    $(this).css( 'border-left','#ffcf65 2px solid' );
                }
            }
        });
    }

    /*
    *Validate CVC
    */
    if (ccv.length > 0) {
        ccv.on('change keypress keyup', function (event) {

            $(this).forceNumeric(event);
            $(this).attr( 'maxlength', 4 );

            if( $(this).val().length >= 3 ) {
                $(this).css( 'border-left','green 2px solid' );
            }else if( $(this).val().length === 0 ) {
                $(this).removeAttr( 'style' );
            }else {
                $(this).css( 'border-left','#ffcf65 2px solid' );
            }

        });
    }

    $.fn.forceNumeric = function (e) {
        return this.each(function (e) {
            $(this).keydown(function (e) {
                var key = e.which || e.keyCode;

                if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                    // numbers
                    key >= 48 && key <= 57 ||
                    // Numeric keypad
                    key >= 96 && key <= 105 ||
                    // comma, period and minus, . on keypad
                    key === 190 || key === 188 || key === 109 || key === 110 ||
                    // Backspace and Tab and Enter
                    key === 8 || key === 9 || key === 13 ||
                    // Home and End
                    key === 35 || key === 36 ||
                    // left and right arrows
                    key === 37 || key === 39 ||
                    // Del and Ins
                    key === 46 || key === 45)
                    return true;

                return false;
            });
        });
    }
});
