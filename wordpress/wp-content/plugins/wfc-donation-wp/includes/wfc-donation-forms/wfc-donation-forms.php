<?php

/**
* Class WFC_DonationForms
* Handles the data used by the donation form list.
*
* This Class handles all the data that are being used by and displayed on the Donation Form List Page and Donation Forms.
*
* @author Junjie Canonio
* @since  1.0.0
*
*/
class WFC_DonationForms{

	/**
	* Get donation counts using donation form ID.
	*
	* This function returns the number of donations registered in each form.
	*
	*
	* @author Junjie Canonio
	* @param string $formid
	* @return string count
	 *
	 * @LastUpdated  March 20, 2019
	*/
	public function wfc_count_donations_by_post_id( $formid ) {
		global $wpdb;
		$query = "
			SELECT COUNT(*) as count
			FROM {$wpdb->prefix}postmeta
			WHERE meta_key = 'pronto_donation_donor' AND post_id = %d
			ORDER BY meta_id DESC";
		return $wpdb->get_results( $wpdb->prepare( $query, $formid ), ARRAY_A )[0]['count'];
	}
	
	/**
	 * Get donation counts using donation meta record.
	 *
	 * This function fetches the number of donations based on the number of meta record with the same meta_value.
	 *
	 * @author Junjie Canonio
	 *
	 * @param $meta_value_search
	 *
	 * @return int|null
	 *
	 * @LastUpdated  March 20, 2019
	 */
	public function wfc_count_donations_by_meta_value( $meta_value_search ) {
		global $wpdb;

		$meta_value_search = '%' . $meta_value_search . '%';
		$query = "
			SELECT COUNT(*) as count
			FROM {$wpdb->prefix}postmeta
			WHERE meta_key = 'pronto_donation_donor' AND meta_value LIKE %s
			ORDER BY meta_id DESC";

		return $wpdb->get_results( $wpdb->prepare( $query, $meta_value_search ), ARRAY_A )[0]['count'];
	}
	
	
	/**
	 * Fetch donations using details specified on the $filter parameter.
	 *
	 * This function fetches all donations which matches the details fed on the $filter parameter.
	 *
 	 * @author Junjie Canonio
	 *
	 * @param array $filter
	 * @param bool  $count
	 *
	 * @return array
	 *
	 * @LastUpdated  March 20, 2019
	 */
	public function wfc_get_donations( $filter = array(), $count = false ) {
		global $wpdb;

		$search = null;

		if( !empty( $filter ) ) {
			foreach ($filter as $k => $v) {
				$key = sanitize_text_field( $v['key'] );
				$operator = sanitize_text_field( $v['operator'] );
				$value = sanitize_text_field( $v['value'] );
				$search .= " AND {$key} {$operator} {$value} ";
			}
		}

		$col = ( $count ) ? "count(*) as count" : "*";
		$where = "WHERE meta_key = 'pronto_donation_donor' {$search} ORDER BY meta_id DESC";
		$query = "SELECT {$col} FROM {$wpdb->prefix}postmeta {$where}";

		return $wpdb->get_results( $query, ARRAY_A );
	}


	/**
	 * Fetches all donations using WordPress meta_id.
	 *
	 * This function fetches all donations based on meta_id.
	 *
	 * @author Junjie Canonio
	 * @param integer $meta_id
	 * @return array
	 *
	 * @LastUpdated  March 20, 2019
	 */
	public function wfc_get_donation_by_meta_id($meta_id ) {
		global $wpdb;

		$query = "
			SELECT *
			FROM {$wpdb->prefix}postmeta
			WHERE meta_key = 'pronto_donation_donor' AND meta_id = %d
			ORDER BY meta_id DESC";

		return $wpdb->get_results( $wpdb->prepare( $query, $meta_id ), ARRAY_A );
	}

	/**
	 * Fetches all donations using post_id.
	 *
	 * This function fetches all donations based on post_id.
	 *
	 * @author Junjie Canonio
	 * @param integer $post_id
	 * @return array
	 *
	 * @LastUpdated  March 20, 2019
	 */
	public function wfc_get_donations_by_post_id($post_id ) {
		global $wpdb;

		$query = "
			SELECT *
			FROM {$wpdb->prefix}postmeta
			WHERE meta_key = 'pronto_donation_donor' AND post_id = %d
			ORDER BY meta_id DESC";

		return $wpdb->get_results( $wpdb->prepare( $query, $post_id ), ARRAY_A );
	}

	/**
	 * Fetches all donations by meta_value with wild cards.
	 *
	 * This function fetches all donations based on meta_value and uses wildcard as search filter.
	 *
	 * @author Junjie Canonio
	 * @param string $meta_value_search
	 * @return array
	 *
	 * @LastUpdated  March 20, 2019
	 */
	public function wfc_get_donations_by_meta_value($meta_value_search) {
		global $wpdb;

		$meta_value_search = '%' . $meta_value_search . '%';
		$query = "
			SELECT *
			FROM {$wpdb->prefix}postmeta
			WHERE meta_key = 'pronto_donation_donor' AND meta_value LIKE %s
			ORDER BY meta_id DESC";

		return $wpdb->get_results( $wpdb->prepare( $query, $meta_value_search ), ARRAY_A );
	}


	/**
	 * Get total donation amount per donation form.
	 *
	 * This function returns the total amount accumulated by a specific Donation form.
	 *
	 * @author Junjie Canonio
	 * @param $formid
	 * @return array
	 *
	 * @LastUpdated  March 20, 2019
	*/
	public function wfc_total_donations( $formid ) {

		$list = $this->wfc_get_donations_by_post_id( $formid );

		$total = 0;
		if( false !== $list && sizeof( $list ) > 0 ) {
			foreach ( $list as $data ) {
				$data_meta_value = isset($data['meta_value']) ? $data['meta_value'] : '';
				$donate = maybe_unserialize( $data_meta_value );
				if( isset( $donate['statusText'] ) 
					&& strtolower( $donate['statusText'] ) == 'approved' ) {
					if( isset( $donate['pdwp_sub_amt'] ) ) {
						$total += $donate['pdwp_sub_amt'];
					} elseif( isset( $donate['pdwp_custom_amount'] ) 
						&& !empty( $donate['pdwp_custom_amount'] ) ) {
						$total += $donate['pdwp_custom_amount'];
					} elseif( isset( $donate['pd_amount'] ) 
						&& !empty( $donate['pd_amount'] ) ) {
						$total += $donate['pd_amount'];
					} else {
						$total += $donate['pdwp_amount'];
					}
				}

			}
		}
		return $total;
	}	

	/**
	 * Get currency symbol using currency code.
	 *
	 * This function returns the currency symbol based on the Currency code provided.
	 *
	 * @author Junjie Canonio
	 * @param string $code
	 * @return array
	 *
	 * @LastUpdated  March 20, 2019
	 */
	public function wfc_get_currencySymbol($code = '')
	{
		$currency_symbols = array(
			'AED' => '&#1583;.&#1573;', // ?
			'AFN' => '&#65;&#102;',
			'ALL' => '&#76;&#101;&#107;',
			'AMD' => '',
			'ANG' => '&#402;',
			'AOA' => '&#75;&#122;', // ?
			'ARS' => '&#36;',
			'AUD' => '&#36;',
			'AWG' => '&#402;',
			'AZN' => '&#1084;&#1072;&#1085;',
			'BAM' => '&#75;&#77;',
			'BBD' => '&#36;',
			'BDT' => '&#2547;', // ?
			'BGN' => '&#1083;&#1074;',
			'BHD' => '.&#1583;.&#1576;', // ?
			'BIF' => '&#70;&#66;&#117;', // ?
			'BMD' => '&#36;',
			'BND' => '&#36;',
			'BOB' => '&#36;&#98;',
			'BRL' => '&#82;&#36;',
			'BSD' => '&#36;',
			'BTN' => '&#78;&#117;&#46;', // ?
			'BWP' => '&#80;',
			'BYR' => '&#112;&#46;',
			'BZD' => '&#66;&#90;&#36;',
			'CAD' => '&#36;',
			'CDF' => '&#70;&#67;',
			'CHF' => '&#67;&#72;&#70;',
			'CLF' => '', // ?
			'CLP' => '&#36;',
			'CNY' => '&#165;',
			'COP' => '&#36;',
			'CRC' => '&#8353;',
			'CUP' => '&#8396;',
			'CVE' => '&#36;', // ?
			'CZK' => '&#75;&#269;',
			'DJF' => '&#70;&#100;&#106;', // ?
			'DKK' => '&#107;&#114;',
			'DOP' => '&#82;&#68;&#36;',
			'DZD' => '&#1583;&#1580;', // ?
			'EGP' => '&#163;',
			'ETB' => '&#66;&#114;',
			'EUR' => '&#8364;',
			'FJD' => '&#36;',
			'FKP' => '&#163;',
			'GBP' => '&#163;',
			'GEL' => '&#4314;', // ?
			'GHS' => '&#162;',
			'GIP' => '&#163;',
			'GMD' => '&#68;', // ?
			'GNF' => '&#70;&#71;', // ?
			'GTQ' => '&#81;',
			'GYD' => '&#36;',
			'HKD' => '&#36;',
			'HNL' => '&#76;',
			'HRK' => '&#107;&#110;',
			'HTG' => '&#71;', // ?
			'HUF' => '&#70;&#116;',
			'IDR' => '&#82;&#112;',
			'ILS' => '&#8362;',
			'INR' => '&#8377;',
			'IQD' => '&#1593;.&#1583;', // ?
			'IRR' => '&#65020;',
			'ISK' => '&#107;&#114;',
			'JEP' => '&#163;',
			'JMD' => '&#74;&#36;',
			'JOD' => '&#74;&#68;', // ?
			'JPY' => '&#165;',
			'KES' => '&#75;&#83;&#104;', // ?
			'KGS' => '&#1083;&#1074;',
			'KHR' => '&#6107;',
			'KMF' => '&#67;&#70;', // ?
			'KPW' => '&#8361;',
			'KRW' => '&#8361;',
			'KWD' => '&#1583;.&#1603;', // ?
			'KYD' => '&#36;',
			'KZT' => '&#1083;&#1074;',
			'LAK' => '&#8365;',
			'LBP' => '&#163;',
			'LKR' => '&#8360;',
			'LRD' => '&#36;',
			'LSL' => '&#76;', // ?
			'LTL' => '&#76;&#116;',
			'LVL' => '&#76;&#115;',
			'LYD' => '&#1604;.&#1583;', // ?
			'MAD' => '&#1583;.&#1605;.', //?
			'MDL' => '&#76;',
			'MGA' => '&#65;&#114;', // ?
			'MKD' => '&#1076;&#1077;&#1085;',
			'MMK' => '&#75;',
			'MNT' => '&#8366;',
			'MOP' => '&#77;&#79;&#80;&#36;', // ?
			'MRO' => '&#85;&#77;', // ?
			'MUR' => '&#8360;', // ?
			'MVR' => '.&#1923;', // ?
			'MWK' => '&#77;&#75;',
			'MXN' => '&#36;',
			'MYR' => '&#82;&#77;',
			'MZN' => '&#77;&#84;',
			'NAD' => '&#36;',
			'NGN' => '&#8358;',
			'NIO' => '&#67;&#36;',
			'NOK' => '&#107;&#114;',
			'NPR' => '&#8360;',
			'NZD' => '&#36;',
			'OMR' => '&#65020;',
			'PAB' => '&#66;&#47;&#46;',
			'PEN' => '&#83;&#47;&#46;',
			'PGK' => '&#75;', // ?
			'PHP' => '&#8369;',
			'PKR' => '&#8360;',
			'PLN' => '&#122;&#322;',
			'PYG' => '&#71;&#115;',
			'QAR' => '&#65020;',
			'RON' => '&#108;&#101;&#105;',
			'RSD' => '&#1044;&#1080;&#1085;&#46;',
			'RUB' => '&#1088;&#1091;&#1073;',
			'RWF' => '&#1585;.&#1587;',
			'SAR' => '&#65020;',
			'SBD' => '&#36;',
			'SCR' => '&#8360;',
			'SDG' => '&#163;', // ?
			'SEK' => '&#107;&#114;',
			'SGD' => '&#36;',
			'SHP' => '&#163;',
			'SLL' => '&#76;&#101;', // ?
			'SOS' => '&#83;',
			'SRD' => '&#36;',
			'STD' => '&#68;&#98;', // ?
			'SVC' => '&#36;',
			'SYP' => '&#163;',
			'SZL' => '&#76;', // ?
			'THB' => '&#3647;',
			'TJS' => '&#84;&#74;&#83;', // ? TJS (guess)
			'TMT' => '&#109;',
			'TND' => '&#1583;.&#1578;',
			'TOP' => '&#84;&#36;',
			'TRY' => '&#8356;', // New Turkey Lira (old symbol used)
			'TTD' => '&#36;',
			'TWD' => '&#78;&#84;&#36;',
			'TZS' => '',
			'UAH' => '&#8372;',
			'UGX' => '&#85;&#83;&#104;',
			'USD' => '&#36;',
			'UYU' => '&#36;&#85;',
			'UZS' => '&#1083;&#1074;',
			'VEF' => '&#66;&#115;',
			'VND' => '&#8363;',
			'VUV' => '&#86;&#84;',
			'WST' => '&#87;&#83;&#36;',
			'XAF' => '&#70;&#67;&#70;&#65;',
			'XCD' => '&#36;',
			'XDR' => '',
			'XOF' => '',
			'XPF' => '&#70;',
			'YER' => '&#65020;',
			'ZAR' => '&#82;',
			'ZMK' => '&#90;&#75;', // ?
			'ZWL' => '&#90;&#36;',
		);
	
		if(!empty($code))
		{
			return $currency_symbols[strtoupper($code)];
		}
		else
		{
			return $currency_symbols;
		}
	}

}