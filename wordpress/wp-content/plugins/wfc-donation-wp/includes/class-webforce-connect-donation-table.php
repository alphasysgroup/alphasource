<?php

if( ! class_exists( 'WFC_List_Table' ) ) {
 	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	
	/**
	 * Class WFC_List_Table
	 */
	class WFC_List_Table extends WP_List_Table {

		/**
		 * The columns of the table
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      array    $columns    
		 */
		private $columns;

		/**
		 * The table list sortable
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      boolean    $sortable 
		 */
		private $sortable;

		/**
		 * The pagination item limit per page
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $per_page 
		 */
		private $per_page;

		/**
		 * The table actions
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $actions
		 */
		private $actions;

		/**
		 * The table column to be hidden
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $hidden 
		 */
		private $hidden;
		private $customcount;
		
		/**
		 * Define the core functionality of the plugin.
		 *
		 * @since    1.0.0
		 *
		 * @param array $data
		 * @param array $columns
		 * @param int   $per_page
		 * @param bool  $sortable
		 * @param array $actions
		 * @param array $hidden
		 * @param null  $customcount
		 */
		public function __construct($data=array(), $columns=array(), $per_page=5, $sortable=false, $actions=array() , $hidden=array(), $customcount = null ){
			parent::__construct();

			$this->items = $data;

			//table columns.
			$this->columns = $columns;

			//field to be hide.
			$this->hidden = array();

			//pagination item per page.
			$this->per_page = $per_page;

			//table actions.
			$this->actions = $actions;

			$this->customcount = $customcount;

			//sortable columns.
			if($sortable){
				$this->sortable = $this->get_sortable_columns();
			}
		}
		
		/**
		 *
		 */
		function prepare_items() {

			$columns  = $this->get_columns();

			$this->_column_headers = array( $columns, $this->hidden, $this->sortable );

			usort( $this->items, array( &$this, 'usort_reorder' ) );

			$current_page = $this->get_pagenum();


			if( $this->customcount == null ){
				$total_items = count( $this->items );
			}else{
				$total_items = $this->customcount; 

			}

			// only ncessary because we have sample data
			// $found_data = array_slice( $this->items, ( ( $current_page-1 )* $this->per_page ), $this->per_page );

			$this->set_pagination_args( array(
				'total_items' => $total_items,
				'per_page'    => $this->per_page 
			) );


			// print_r($this->table_data_search('title','Shadowmagic', $found_data));

			// $this->items = $found_data;

		}

		function no_items() {
			_e( 'No books found, dude.' );
		}
		
		/**
		 * @param object $item
		 * @param string $column_name
		 *
		 * @return mixed
		 */
		function column_default( $item, $column_name ) {

			if (array_key_exists($column_name, $this->items[0])){
				return $item[ $column_name ];
			}else{
				return print_r( $item, true ) ; //Show the whole array for troubleshooting purposes	
			}

		}
		
		/**
		 * Get a list of sortable columns. The format is:
		 * 'internal-name' => 'orderby'
		 * or
		 * 'internal-name' => array( 'orderby', true )
		 *
		 * The second format will make the initial sorting order be descending
		 *
		 * @since 3.1.0
		 *
		 * @return array
		 */
		function get_sortable_columns() {
			
			$sortable_columns = array();

			foreach($this->columns as $index=>$item){
				$sortable_columns[$index] =  array( $index, false);
			}

			return $sortable_columns;
		}
		
		/**
		 * Get a list of columns. The format is:
		 * 'internal-name' => 'Title'
		 *
		 * @since 3.1.0
		 * @abstract
		 *
		 * @return array
		 */
		function get_columns(){
			
			$columns = array(
		        // 'cb'        => '<input type="checkbox" />',
		    );

	    	return array_merge($columns, $this->columns);
		}
		
		/**
		 * @param $a
		 * @param $b
		 *
		 * @return int
		 */
		function usort_reorder( $a, $b ) {


			// If no sort, default to title
			$orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'ID';
			// If no order, default to asc
			$order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'desc';
			// Determine sort order
			$result = strnatcmp( $a[$orderby], $b[$orderby] );
			// Send final sort direction to usort
			return ( $order === 'asc' ) ? $result : -$result;
		}
		
		/**
		 * @param $item
		 *
		 * @return string
		 */
		function column_title($item){

			$actions = array();

			if(!is_array($this->actions))
				return $item['title'];

			foreach($this->actions as $action){
				array_push($actions, sprintf('<a href="?page=%s&action=%s&ID=%s">'.ucfirst($action).'</a>',$_REQUEST['page'],strtolower($action),$item['ID']));
			}

			return sprintf('%1$s %2$s', $item['title'], $this->row_actions($actions) );

		}
		
		/**
		 * Get an associative array ( option_name => option_title ) with the list
		 * of bulk actions available on this table.
		 *
		 * @since 3.1.0
		 *
		 * @return array
		 */
		function get_bulk_actions() {
			$actions = array(
				'delete'    => 'Delete'
			);
			return $actions;
		}
		
		/**
		 * @param object $item
		 *
		 * @return string
		 */
		function column_cb( $item) {
		    return sprintf(
		        '<input type="checkbox" name="rid[]" value="%s" />', $item['ID']
		    );    
		}
		
		/**
		 * @param $label
		 * @param $id
		 */
		public function search_form( $label, $id){

			?>
			<form method="post" style="float: right">
				<?php echo $this->search_box( $label, $id ); ?>
			</form>

			<?php
		}
		
		/**
		 * @param $id
		 * @param $array
		 *
		 * @return int|null|string
		 */
		function search_data($id, $array) {

		   foreach ($array as $key => $val) {
		       if ($val['uid'] === $id) {
		           return $key;
		       }
		   }

		   return null;
		}
		
		/**
		 * @param $key
		 * @param $value
		 * @param $array
		 *
		 * @return array
		 */
		function table_data_search($key, $value, $array) {
		  
	 		$results = array();
	         
	        if (is_array($array)) {
		        if (isset($array[$key]) && $array[$key] == $value) {
		        	$results[] = $array;
		        }
	         
		        foreach ($array as $subarray) {
		        	$results = array_merge($results, $this->table_data_search($subarray, $key, $value));
		        }
	        }
	         
	        return $results;

		}


	} //class

}

