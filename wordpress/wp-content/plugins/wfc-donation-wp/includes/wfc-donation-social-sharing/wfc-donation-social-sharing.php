<?php

defined('ABSPATH') or die('No script kiddie please!');

if (!class_exists('Social_Sharing')) {
	/**
	 *
	 */
	define('PD_SOCIAL_FACEBOOK', 'facebook');
	/**
	 *
	 */
	define('PD_SOCIAL_TWITTER', 'twitter');
	/**
	 *
	 */
	define('PD_SOCIAL_GOOGLE_PLUS', 'google_plus');
	/**
	 *
	 */
	define('PD_SOCIAL_LINKEDIN', 'linkedin');
	/**
	 *
	 */
	define('PD_SOCIAL_EMAIL', 'email');

	/**
     * Class WFC_Donation_Social_Sharing
     *
	 * Singleton class that handles social sharing buttons
	 *
	 * This is used to render social sharing buttons of specific social networking site.
	 */
	class WFC_Donation_Social_Sharing {

		/**
		 * The variable that stores the instance of its self
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      Social_Sharing     $instance
		 */
		protected static $instance = null;

		/**
		 * The variable that holds the Social Media type
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string    $social_link
		 */
		protected $social_link;
		
		/**
         * The variable that holds the URL for Social Share.
         * @access protected
		 * @var string $social_link_share_url
		 */
		protected $social_link_share_url;
		
		/**
         * The variable that holds the message for Social Share.
         *
         * @access protected
         *
		 * @var string $social_link_share_message
		 */
		protected $social_link_share_message;

		/**
		 * The variable that holds the Post Meta ID
		 *
		 * @since    1.0.0
		 * @access   protected
		 * @var      string     $post_meta_id
		 */
		protected $post_meta_id;
		
		/**
		 * WFC_Donation_Social_Sharing constructor.
		 */
		private function __construct() {
		    wp_enqueue_style(
                'wfc-donation-social-sharing',
                plugin_dir_url( __FILE__ ) . 'assets/css/social-sharing.css'
            );
		}
		
		/**
		 * Function for creating and returns a singleton class
		 * by creating its own instance if it is not instantiated yet
		 *
		 * @author   Von Sienard Vibar <von@alphasys.com.au>
         *
		 * @return Social_Sharing|WFC_Donation_Social_Sharing
		 */
		public static function get_instance() {
			if (self::$instance === null) self::$instance = new self();
			return self::$instance;
		}

		/**
		 * Function to store social link type and post meta id.
		 * Required social link:
		 *      PD_SOCIAL_FACEBOOK
		 *      PD_SOCIAL_TWITTER
		 *      PD_SOCIAL_GOOGLE_PLUS
		 *      PD_SOCIAL_LINKEDIN
		 *      PD_SOCIAL_EMAIL
		 *
		 *
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 *
		 * @param $social_link
		 * @param $post_meta_id
		 *
		 * @return $this Returns the singleton class
		 */
		public function prepare($social_link, $post_meta_id = null ) {
			$this->social_link = $social_link;
			$this->post_meta_id = $post_meta_id;

			$this->get_campaign_metas_to_share();

			return $this;
		}

		/**
		 * This function will then render the share button based from the preparation.
		 *
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		public function share() {
			ob_start();
			?>
            <div class="wfc-donation-social-sharing <?php echo $this->social_link; ?>">
            <?php
			switch ($this->social_link) {
				case PD_SOCIAL_FACEBOOK:
					$this->button_renderer_facebook_2();
					break;
				case PD_SOCIAL_TWITTER:
					$this->button_renderer_twitter_2();
					break;
				case PD_SOCIAL_GOOGLE_PLUS:
					$this->button_renderer_google_plus_2();
					break;
				case PD_SOCIAL_LINKEDIN:
					$this->button_renderer_linkedin_2();
					break;
				case PD_SOCIAL_EMAIL:
					$this->button_renderer_email();
					break;
				default:
					throw new InvalidArgumentException('Invalid social link or not included in the class');
					break;
			}
			?>
            </div>
            <?php
			return ob_get_clean();
		}
		
		/**
		 * Returns the meta_value object based from the post_meta_id.
		 *
		 * @author   Von Sienard Vibar <von@alphasys.com.au>
		 *
		 * @return mixed|null
		 */
		private function get_post_meta_result() {
			global $wpdb;
			$query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'pronto_donation_donor' AND meta_id={$this->post_meta_id} ORDER BY meta_id DESC";
			$result = $wpdb->get_results( $query, ARRAY_A );
			return !empty($result) ? unserialize($result[0]['meta_value']) : null;
		}

		/**
		 * Renders the Facebook share button.
		 *
		 * @since    1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		private function button_renderer_facebook() {
			?>
			<div id="fb-root"></div>
			<script>
				(function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0];
                    if (d.getElementById(id)) return;
                    js = d.createElement(s); js.id = id;
                    js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12';
                    fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));
			</script>
			<div class="fb-share-button" data-href="https://alphasys.com.au" data-layout="button_count" data-size="small" data-mobile-iframe="true">
				<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Falphasys.com.au%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">Share</a>
			</div>
			<?php
		}
		
		/**
		 *
		 */
		private function button_renderer_facebook_2() {
		    $get_params = array(
                'app_id' => '1739901612799358',
                'display' => 'popup',
                'href' => $this->social_link_share_url,
                'redirect_uri' => get_home_url()
            );

		    if ($this->social_link_share_message !== '')
		        $get_params['quote'] = $this->social_link_share_message;

		    $url = 'https://www.facebook.com/dialog/share?' . http_build_query($get_params);
		    ?>
            <a href="#" onclick="window.open('<?php echo $url; ?>', 'Share on Facebook', 'width=500,height=500,top=' + ((screen.height / 2) - 250) + ',left=' + ((screen.width / 2) - 250))" title="Share on Facebook">
                <i class="fa fa-facebook-f"></i><span>Share</span>
            </a>
            <?php
        }

		/**
		 * Renders the Twitter tweet button
		 *
		 * @since    1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		private function button_renderer_twitter() {
			?>
			<script>
				window.twttr = (function(d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                        t = window.twttr || {};
                    if (d.getElementById(id)) return t;
                    js = d.createElement(s);
                    js.id = id;
                    js.src = "https://platform.twitter.com/widgets.js";
                    fjs.parentNode.insertBefore(js, fjs);

                    t._e = [];
                    t.ready = function(f) {
                        t._e.push(f);
                    };

                    return t;
                }(document, "script", "twitter-wjs"));
			</script>
			<a class="twitter-share-button" href="https://twitter.com/intent/tweet?text=Hello%20world">Tweet</a>
			<?php
		}
		
		/**
		 *
		 */
		private function button_renderer_twitter_2() {
		    $get_params = array(
                'url' => $this->social_link_share_url,
            );

			if ($this->social_link_share_message !== '')
				$get_params['text'] = $this->social_link_share_message;

		    $url = 'https://twitter.com/intent/tweet?' . http_build_query($get_params);
		    ?>
            <a href="#" onclick="window.open('<?php echo $url; ?>', 'Tweet on Twitter', 'width=500,height=500,top=' + ((screen.height / 2) - 250) + ',left=' + ((screen.width / 2) - 250))" title="Tweet on Twitter">
                <i class="fa fa-twitter"></i><span>Tweet</span>
            </a>
            <?php
        }

		/**
		 * Renders the Google Plus share button
		 *
		 * @since    1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		private function button_renderer_google_plus() {
			?>
			<script src="https://apis.google.com/js/platform.js" async defer></script>
			<g:plus action="share"></g:plus>
			<?php
		}
		
		/**
		 *
		 */
		private function button_renderer_google_plus_2() {
		    $get_params = array(
                'url' => $this->social_link_share_url
            );

			if ($this->social_link_share_message !== '')
				$get_params['text'] = $this->social_link_share_message;

		    $url = 'https://plus.google.com/share?' . http_build_query($get_params);
		    ?>
            <a href="#" onclick="window.open('<?php echo $url ?>', 'Share on Google+', 'width=500,height=500,top=' + ((screen.height / 2) - 250) + ',left=' + ((screen.width / 2) - 250))" title="Share on Google+">
                <i class="fa fa-google-plus"></i><span>Share</span>
            </a>
            <?php
        }

		/**
		 * Renders the LinkedIn share button
		 *
		 * @since    1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		private function button_renderer_linkedin() {
			?>
			<script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
			<script type="IN/Share" data-url="https://alphasys.com.au"></script>
			<?php
		}
		
		/**
		 *
		 */
		private function button_renderer_linkedin_2() {
		    $get_params = array(
                'mini' => 'true',
                'url' => $this->social_link_share_url,
            );

			if ($this->social_link_share_message !== '')
				$get_params['summary'] = $this->social_link_share_message;

		    $url = 'https://www.linkedin.com/shareArticle?' . http_build_query($get_params);
		    ?>
            <a href="#" onclick="window.open('<?php echo $url; ?>', 'Share on LinkedIn', 'width=500,height=500,top=' + ((screen.height / 2) - 250) + ',left=' + ((screen.width / 2) - 250))" title="Share on LinkedIn">
                <i class="fa fa-linkedin"></i><span>Share</span>
            </a>
            <?php
        }

		/**
		 * Renders the Email link.
		 *
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		private function button_renderer_email() {
			?>
			<a href="mailto:email@email.com">Email</a>
			<?php
		}
		
		/**
		 *
		 */
		private function get_campaign_metas_to_share() {
			$form_id = isset($_GET['ty']) ? $_GET['ty'] : '';
			$pmid = isset($_GET['pmid']) ? $_GET['pmid'] : '';

			$WFC_DonationForms = new WFC_DonationForms();
			
			$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $pmid );
			$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
			$donation = maybe_unserialize( $donations );
			$donation = stripslashes_deep( $donation );

			// var_dump($donation);

			$campaign_id = isset($donation['campaignId']) ? $donation['campaignId'] : '';

			$args = array(
                'post_type' => 'wfc_fundraising',
                'meta_query' => array(
                    array(
                        'key' => 'puf_campaign_id',
                        'value' => $campaign_id,
                        'compare' => '='
                    )
                )
            );
			$query = new WP_Query($args);

			if ($query->post_count == 1) {
			    $post = $query->post;

			    // print_r(get_post_meta($post->ID));
				$this->social_link_share_url = get_permalink($post->ID);
                $this->social_link_share_message = get_post_meta($post->ID, 'puf_campaign_description', true);

				$this->social_link_share_message = $this->social_link_share_message !== ''
                    ? $this->social_link_share_message
                    : get_post_meta($form_id, 'donation_form_social_message', true);
            } else {
				$this->social_link_share_url = get_post_meta($form_id, 'donation_form_social_link', true);
				$this->social_link_share_message = get_post_meta($form_id, 'donation_form_social_message', true);

                $this->social_link_share_url =
                    $this->social_link_share_url !== null && $this->social_link_share_url !== ''
                        ? $this->social_link_share_url
                        : get_home_url();
                $this->social_link_share_message =
                    $this->social_link_share_message !== null && $this->social_link_share_message !== ''
                        ? $this->social_link_share_message
                        : '';
            }
        }
	}
}
