<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('WFC_DonationEncryptDecrypt')) {
	/**
	 * Class WFC_DonationEncryptDecrypt
	 * This class handles out the encrypt and decrypt functionalities provided on Webforce Connect Donation.
	 *
	 * LastUpdated : May 13, 2020
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 */
	class WFC_DonationEncryptDecrypt
	{
		/**
		 * Initializes values and resources needed.
		 * LastUpdated : May 10, 2020
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 * @since  1.0.0
		 */
		public function __construct() {
		}


		/**
		 * Enqueues styles and scripts
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param  array  $styles   Array of styles slug name
		 * @param  array  $scripts  Array of scripts slug name
		 *
		 * @since  1.0.0
		 *
		 * @LastUpdated  May 12, 2020
		 */
		public static function wfc_don_enqueue_styles_scripts(){

			wp_register_script(
				'wfc-don-wfc_don_cryptoJsAes-aes-js',
				plugin_dir_url( __FILE__ ) . '/js/aes.js',
				array( 'jquery' ),
				'',
				false
			);

			wp_register_script(
				'wfc-don-wfc_don_cryptoJsAes-js',
				plugin_dir_url( __FILE__ ) . '/wfc-encrypt-decrypt.js',
				array( 'jquery' ),
				'',
				false
			);

		}

		/**
		 * Decrypt data from a CryptoJS json encoding string
		 *
		 * @param mixed $passphrase
		 * @param mixed $jsonString
		 * @return mixed
		 *
		 * @LastUpdated  May 13, 2020
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 */
		public static function wfc_don_cryptoJsAesDecrypt($passphrase, $jsonString){
			$jsondata = json_decode($jsonString, true);
			$salt = hex2bin($jsondata["s"]);
			$ct = base64_decode($jsondata["ct"]);
			$iv = hex2bin($jsondata["iv"]);
			$concatedPassphrase = $passphrase . $salt;
			$md5 = array();
			$md5[0] = md5($concatedPassphrase, true);
			$result = $md5[0];
			for ($i = 1; $i < 3; $i++) {
				$md5[$i] = md5($md5[$i - 1] . $concatedPassphrase, true);
				$result .= $md5[$i];
			}
			$key = substr($result, 0, 32);
			$data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
			return json_decode($data, true);
		}


		/**
		 * Encrypt value to a cryptojs compatiable json encoding string
		 *
		 * @param mixed $passphrase
		 * @param mixed $value
		 * @return string
		 *
		 * @LastUpdated  May 13, 2020
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 */
		public static function wfc_don_cryptoJsAesEncrypt($passphrase, $value){
			$salt = openssl_random_pseudo_bytes(8);
			$salted = '';
			$dx = '';
			while (strlen($salted) < 48) {
				$dx = md5($dx . $passphrase . $salt, true);
				$salted .= $dx;
			}
			$key = substr($salted, 0, 32);
			$iv = substr($salted, 32, 16);
			$encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
			$data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
			return json_encode($data);
		}
	}
	new WFC_DonationEncryptDecrypt();
}