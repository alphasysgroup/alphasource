jQuery(document).ready(function($){

	var gateway = new WFCDONGateway();

	var noneerrorfields_list = [
		{id: 'card_number'},
		{id: 'name_on_card'},
		{id: 'expiry_month'},
		{id: 'expiry_year'},
		{id: 'ccv'},
		{id: 'pdwp_bank_code'},
		{id: 'pdwp_account_number'},
		{id: 'pdwp_account_holder_name'},
		{id: 'wfc_paymentfile_bank_code'},
		{id: 'wfc_paymentfile_account_number'},
		{id: 'wfc_paymentfile_account_holder_name'}
	];

	/** Scroll to invalid field*/
	var delay = 0;
	var offset = 150;
	document.addEventListener('invalid', function(e){
	   $(e.target).addClass("invalid");
	   $('html, body').animate({scrollTop: $($(".invalid")[0]).offset().top - offset }, delay);
	}, true);
	document.addEventListener('change', function(e){
	   $(e.target).removeClass("invalid")
	}, true);


	/**
	* toggle payment gateway
	*/
	$('input[name="payment_gateway"]').on( 'change', function() {
		/** remove stripe credit card fields*/
		$('#paypal-redirect').remove();
		/** remove stripe credit card fields*/
		$('#stripe-card-details').remove();
		/** remove nab credit card fields*/
		$('#nab-card-details').remove();

		/** remove nab credit card fields*/
		$('#paymentfile-card-details').remove();

		/** show spinner*/
		$('#wfc_donation-payment-details-toogle-container').hide();
		$('#wfc_donation-payment-details-toogle-container').before('<div id="gateway-loader-container" style="text-align: center;width: 100%;"><span id="gateway-loader"></span></div>');
		gateway.switchGateway( $(this).val() );
		HandleDonationType();	

		$('input[name="gatewayid"]').val( $(this).val() );	
	});
	$('input[name="payment_gateway"]:first').attr('checked', 'checked');
	$('input[name="payment_gateway"]:first').trigger('change');


	/**
	* toggle amount level
	*/
	HandleAmountlevel();
	$('input[type="radio"][name="pdwp_amount"]').on( 'change', function() {
		HandleAmountlevel();
		console.log('Amount Buttons Update : Paypal Button Regenerate');
	});


	/*
	* handle tribute field
	*/
	HandleTributeField();
	$('#pdwp_gift').on( 'change', function() {
		HandleTributeField();
	});


	/**
	* toggle donor type
	*/
	HandleDonorCompany();
	$('#donor_type').on( 'change', function() {
		HandleDonorCompany();
	});
	

	/**
	* set donation amount for form submit
	*/
	$('#pdwp_custom_amount').on('focusin', function() {
		$('#wfc-paypal-button-container').empty();
	});
	$('#pdwp_custom_amount').on('change', function() {
		var pdwp_custom_amount_val = $('#pdwp_custom_amount').val();
		$('#pdwp_sub_amt').val(pdwp_custom_amount_val);
		RegeneratePaypalButton();
		console.log('Amount Update : Paypal Button Regenerate');
	});



	/**
	* Regenerate paypal button if the amount has change
	*/
	function RegeneratePaypalButton(){
		$('#wfc-paypal-button-container').empty();
		if ($('#wfc-paypal-button-container').length) {
			var PaypalButton = PaypalGateway.prototype.generatePaypalButton();
			console.log('Paypal Button Regenerate');
		}
	}


	/**
	* toggle payment type show if recurring hide if one-off
	*/
	HandleDonationType();
	$('input[type="radio"][name="pdwp_donation_type"]').on( 'change', function() {
		HandleDonationType();
	});

	/**
	* toggle payment type either credit card or bank
	*/
	HandleDonationPaymentType();
	$('input[name=payment_type]').on( 'change', function() {
	    HandleDonationPaymentType();
	});

	/**
	 * toggle error message popup
	 */
	$('#alert-danger-popup-close-btn').on( 'click', function() {
		if($('#alert-danger-popup').hasClass('alert-danger-popup-hide') == true){
			$('#alert-danger-popup').removeClass('alert-danger-popup-hide');
			$('#alert-danger-popup').addClass('alert-danger-popup-show');
		}else {
			$('#alert-danger-popup').removeClass('alert-danger-popup-show');
			$('#alert-danger-popup').addClass('alert-danger-popup-hide');
			$('#alert-danger').css('display', 'block');
			$([document.documentElement, document.body]).animate({
				scrollTop: $("#alert-danger").offset().top - 200
			}, 2000);
		}
	});


	/**
	 * Mirror the amount value with the card amount value
	 */
	if(!$('#wfc_donation-amount-level-container').val()){
		if ($('#wfc_donation-amount-level-container').attr('display-type') == 'card'){
			$( "#pdwp_custom_amount" ).on('keyup focusout change',function() {
				if($(this).val() != '' && $(this).val() != '0' && $(this).val() != 0){
					$('input[name=pdwp_amount]').prop('checked', false);

					var pdwp_custom_amount_value = $(this).val();
					if(pdwp_custom_amount_value.indexOf('.') != -1) {

						var pdwp_custom_amount_value_arr = pdwp_custom_amount_value.split('.');
						var pdwp_custom_amount_decimal_value = parseInt(pdwp_custom_amount_value_arr[1]);

						if(pdwp_custom_amount_decimal_value == 0){

							pdwp_custom_amount_value = Math.trunc(pdwp_custom_amount_value);
							$('input[name=pdwp_amount][value='+pdwp_custom_amount_value+']').prop('checked', true);
						}
					}else{
						$('input[name=pdwp_amount][value='+pdwp_custom_amount_value+']').prop('checked', true);
					}

				}
			});
		}
	}
	
	/**
	* submit donation form
	*/
	$('#pdwp-form-donate').submit(function(e) {
		e.preventDefault();
		var donationForm = getDonationFormDetails();
		var template_color = $('#template_color').val();

		$('#alert-danger').css('display', 'none');

		/**
		* get gateway type and donation type
		*/
		var gatewayname = donationForm.donation.payment_gateway.split( '_' );

		var gatewayKey 		= gatewayname.length - 2;
		gatewayname 	= ( gatewayname.length > 1 ) ? gatewayname[ gatewayKey ] : gatewayname[0];

		var pdwp_donation_type 	= donationForm.donation.pdwp_donation_type;

		/**
		* get cart donation types
		*/
		var wfc_cartdonationtypes = $('#wfc_cartdonationtypes').val();
		var wfc_cartdonationtypes_bol = false;
		wfc_cartdonationtypes = wfc_cartdonationtypes.toLowerCase();
		if( wfc_cartdonationtypes.indexOf("daily") !== -1 ||
			wfc_cartdonationtypes.indexOf("weekly") !== -1 ||
			wfc_cartdonationtypes.indexOf("fortnightly") !== -1 ||
			wfc_cartdonationtypes.indexOf("monthly") !== -1 ||
			wfc_cartdonationtypes.indexOf("quarterly") !== -1 ||
			wfc_cartdonationtypes.indexOf("yearly") !== -1
		) {
			wfc_cartdonationtypes_bol = true;
		}

		/**
		* validate card details
		*/
		var validate_carddetials = ValidateCardDetials(gatewayname);

		/**
		* Fetch field error notification style
		*/
		var field_error_notice_type = $('#field_error_notice_type').val();

		/**
		 * Remove Captcha error notification
		 */
		if ($('#pdwp-recaptcha').length) {
			$('#pdwp-recaptcha .g-recaptcha > div').removeClass('wfc-don-invalid-field');
			$('#pdwp-recaptcha-error-notification').remove();
		}

		unhighlightFieldErrorValidation(validate_carddetials.unhighlight_errorfields);

		if(gatewayname == 'paymentfile' && pdwp_donation_type == 'one-off'){
			$('html,body').animate({scrollTop: $('input[name=pdwp_donation_type]').offset().top - 200}, 1000, function() {
			    $('input[name=pdwp_donation_type]').focus();
			});
			var pdwp_donation_type_label = $('input[name="pdwp_donation_type"]:checked').attr('donation-type-label');

			SaiyanToolkit.spinner.hide();
		    SaiyanToolkit.tinter.hide();
		    SaiyantistCore.snackbar.hide();
			SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+pdwp_donation_type_label+' donation type is not allowed if payment is through bank.</div>',7000,'error');
		}else if(gatewayname == 'paypal' && wfc_cartdonationtypes_bol == true){
			$('html,body').animate({scrollTop: $('input[name=pdwp_donation_type]').offset().top - 200}, 1000, function() {
			    $('input[name=pdwp_donation_type]').focus();
			});

			SaiyanToolkit.spinner.hide();
		    SaiyanToolkit.tinter.hide();
		    SaiyantistCore.snackbar.hide();
			SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Recurring payment is not supported for Paypal.</div>',7000,'error');
		}else if(validate_carddetials.status != true && gatewayname != 'nab' && gatewayname != 'stripe' && gatewayname != 'paypal'){
			$('html,body').animate({scrollTop: validate_carddetials.element.offset().top - 200}, 1000, function() {
			    validate_carddetials.element.focus();
			});



			var attr = $('#pdwp-form-donate').attr('wfc-don-advance-wizard');
			if (typeof attr !== typeof undefined && attr !== false) {
				if(field_error_notice_type.indexOf("fieldglow") >= 0) {

					highlightFieldErrorValidation(validate_carddetials.highlight_errorfields);
				}
			}

			SaiyanToolkit.spinner.hide();
		    SaiyanToolkit.tinter.hide();
		    SaiyantistCore.snackbar.hide();


		    if(field_error_notice_type.indexOf("snackbar") >= 0){
				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+validate_carddetials.msg+'</div>',7000,'error');
			}

		}else if(typeof grecaptcha !== 'undefined' && grecaptcha.getResponse().length == 0 && $('.pd-container-recaptcha').length !== 0){
			$('html,body').animate({scrollTop: $('.pd-container-recaptcha').offset().top - 200}, 1000, function() {
				$('.pd-container-recaptcha').focus();
			});

			SaiyanToolkit.spinner.hide();
			SaiyanToolkit.tinter.hide();
			SaiyantistCore.snackbar.hide();
			SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Captcha is required.</div>',7000,'error');

			var attr = $('#pdwp-form-donate').attr('wfc-don-advance-wizard');
			if (typeof attr !== typeof undefined && attr !== false) {
				if(field_error_notice_type.indexOf("fieldglow") >= 0) {
					$('#pdwp-recaptcha .g-recaptcha > div')
						.addClass('wfc-don-invalid-field')
						.after(
							$('<small id="pdwp-recaptcha-error-notification" class="wfc-don-invalid-field-message">Recaptcha is required</small>')
						);
				}
			}
		}else{
			//WFC_Donation_Tinter.show();
			$('#pdwp-btn').addClass('breath-loading');
			$('#pdwp-btn').attr('disabled', 'disabled');

			var processdonate = new DonateProcessor();

			WFC_Donation_LoadingBar.load('Validating Donor Details', 25, template_color);
			processdonate.validateDetails( donationForm.donation )
			.then(function(result) {
				console.log('validateDetails done');
				if(result.status == 'success') {
					WFC_Donation_LoadingBar.load('Processing Payment Transaction', 50, template_color);
					addElementPaymentReferrence( result.donation.pamynet_ref );
					return gateway.processPayment( result, donationForm );
				}
			})
			.then(function(payment) {
				console.log('processPayment done');
				if( payment.status == 'error' ) {
					WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
					return processdonate.resetForm( payment );
				} else {
					WFC_Donation_LoadingBar.load('Saving Payment Transaction', 70, template_color);
					return processdonate.donationData( payment, 'update' );
				}
			})
			.then(function(result) {
				if (result != undefined) {
					console.log('donationData done');
					if( result.status == 'success' ) {
						WFC_Donation_LoadingBar.load('Finalizing Payment', 80, template_color);
						return processdonate.syncdonationDataToSF( result, donationForm );
					} else {
						WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
						return processdonate.completeDonation( result.donation );
					}
				}
			})
			.then(function(result) {
				if (result != undefined) {
					console.log('syncdonationDataToSF done');
					WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
					return processdonate.completeDonation( result.donation );
				}
			})
			.catch(function(error) {
				console.error(error)
			});
		}


	});	


	/*
	* Get the data of fields on form
	*/
	function getDonationFormDetails() {
		var details = $('#pdwp-form-donate').serializeArray();

		var donation = {};
		var creditcard = {
			card_number: $('#card_number').val(),
			ccv: $('#ccv').val(),
			name_on_card: $('#name_on_card').val(),
			expiry_month: $('#expiry_month').val(),
			expiry_year: $('#expiry_year').val()
		};

		$.each( details, function( k, v ) {
			if( v.name.indexOf( '[' ) > -1 || v.name.indexOf( ']' ) > -1 ) {
				var newkey = v.name.split('[')[0];
				donation[newkey] = v.value;
			} else {
				donation[v.name] = v.value;
			}
		});

		this.creditCardDetails = creditcard;

		var gateway = donation.payment_gateway.split( '_' );
		
		var gatewayKey 		= gateway.length - 2;
		var gatewayname 	= ( gateway.length > 1 ) ? gateway[ gatewayKey ] : gateway[0];

		if( gatewayname == 'nab' ) {
			var bank = {
				bank_code : $('#bank_code').val(),
				bank_number : $('#account_number').val(),
				bank_name : $('#account_name').val()
			};
		}else if(gatewayname == 'paymentfile'){
			var bank = {
				bank_code : $('#wfc_paymentfile_bank_code').val(),
				bank_number : $('#wfc_paymentfile_account_number').val(),
				bank_name : $('#wfc_paymentfile_account_holder_name').val()
			};
		}else{
			var bank = {
				bank_code : $('#pdwp_bank_code').val(),
				bank_number : $('#pdwp_account_number').val(),
				bank_name : $('#pdwp_account_holder_name').val()
			};
		}
 
		return {
			donation : donation,
			creditcard : creditcard,
			bank : bank
		};
	}


	/*
	* Add hidden input text for pdwp_payment_referrence , reference for the process payement
	*/
	function addElementPaymentReferrence( ref ) {
		$('#system-generated-fields').append('<input type="hidden" id="pdwp_payment_referrence" name="pdwp_payment_referrence" value="'+ref+'"/>');
	}


	/*
	* toggle donor type and enable/disable 'donor_company' field
	*/
	function HandleDonorCompany(){
		var donor_type_val = $('#donor_type').val();
		if(donor_type_val == 'individual'){
			$('#donor_company').attr('disabled', 'disabled');
			$('#donor_company').val('');
		}else{
			$('#donor_company').removeAttr('disabled');
		}
		
	}

	/*
	* toggle tribute field enable/disable tribute textarea
	*/
	function HandleTributeField(){
		var pdwp_tibutefieldcheck_val = $('input[id="pdwp_gift"]:checked').val();
		if(pdwp_tibutefieldcheck_val == 'true'){
			$('#pdwp_gift_text').removeAttr('disabled');
		}else{
			$('#pdwp_gift_text').attr('disabled', 'disabled');
		}
	}
		
	/*
	* toggle amount level and populate Amount , enable/disable custom amount field
	*/
	function HandleAmountlevel(){
		var pdwp_amount_val = $('input[name="pdwp_amount"]:checked').val();
		$('#pdwp_custom_amount').val(pdwp_amount_val);
		$('#pdwp_sub_amt').val(pdwp_amount_val);

		if(pdwp_amount_val == '0'){
			var pdwp_amount_cache = $('input[name="pdwp_amount"]:checked').attr('cache');
			if (pdwp_amount_cache != null && typeof pdwp_amount_cache !== 'undefined' && pdwp_amount_cache != '') {
				$('#pdwp_custom_amount').val(pdwp_amount_cache);
				$('#pdwp_sub_amt').val(pdwp_amount_cache);					
			}else{
				var pdwp_amount_otr = $('input[name="pdwp_amount"]:checked').attr('otr');
				if (pdwp_amount_otr != null && typeof pdwp_amount_otr !== 'undefined' && pdwp_amount_otr != '') {
					$('#pdwp_custom_amount').val(pdwp_amount_otr);
					$('#pdwp_sub_amt').val(pdwp_amount_otr);	
				}
			}
			$('input[name="pdwp_amount"]:checked').removeAttr('cache');	
			$('input[name="pdwp_amount"]:checked').removeAttr('otr');							
			$('#pdwp_custom_amount').removeAttr('disabled');
		}else{
			if($('#custom-amount').length){
				$('#pdwp_custom_amount').attr('disabled', 'disabled');
			}else{
				$('#pdwp_custom_amount').removeAttr('disabled');
			}
		}
		RegeneratePaypalButton();
	}


	/*
	* hide/show payment type
	*/
	function HandleDonationType(){
		var gateway_bankmode = $('input[name="payment_gateway"]:checked').attr('wfc-don-gateway-bank');

		if(gateway_bankmode == 'true'){
			if($('input[name="pdwp_donation_type"]:checked').val() == 'one-off') {
				$("label[for=payment_type_credit_card] input").prop('checked', true);
				$('#payment-type-wrapper').hide();
				HandleDonationPaymentType();
			}else{
				$('#payment-type-wrapper').show();
				HandleDonationPaymentType();
			}
		}else{
			$("label[for=payment_type_credit_card] input").prop('checked', true);
			$('#payment-type-wrapper').hide();
			HandleDonationPaymentType();
		}
	}


	/*
	* hide/show credit card and bank details
	*/
	function HandleDonationPaymentType() {		

		var payment_type = $('input[name="payment_type"]:checked').val();
		var pdwp_donation_type = $('input[name="pdwp_donation_type"]:checked').val();
		var gateway_bankmode = $('input[name="payment_gateway"]:checked').attr('wfc-don-gateway-bank');
		if( payment_type == 'bank' && pdwp_donation_type != 'one-off' && gateway_bankmode == 'true'){
			$('#credit-card-wrapper').hide();
			$('#bank-details-wrapper').show();

			$('#card_number').val( '' );
			$('#name_on_card').val( '' );
			$('#ccv').val( '' );
		}else{
			$('#bank-details-wrapper').hide();
			$('#credit-card-wrapper').show();

			$('#pdwp_bank_code').val( '' );
			$('#pdwp_account_number').val( '' );
			$('#pdwp_account_holder_name').val( '' );
		}
	}


	/*
	* Validate credit card details
	*/
	function ValidateCardDetials(gatewayname) {
		var response = { status: true, msg: '', element : $('body') };
		var status = true;
		var msg = '';
		var element = '';
		var errorfields_list = [];

		if($('#payment_type_credit_card').is(':checked')) {
			if(!$('#card_number').val()) {
				if (status == true){
					status = false;
				}
				if (element == ''){
					element = $('#card_number');
				}
				msg = msg + 'Card Number is required. <br>';

				errorfields_list.push({
					id: 'card_number',
					err_msg: 'Card Number is required.'
				});
			}

			if(!$('#name_on_card').val()){
				if (status == true){
					status = false;
				}
				if (element == ''){
					element = $('#name_on_card');
				}
				msg = msg + 'Card Holder name is required. <br>';

				errorfields_list.push({
					id: 'name_on_card',
					err_msg: 'Card Holder name is required.'
				});
			}else{
				var name_on_card_value = $('#name_on_card').val();
				var regex = /\s+/gi;
				var name_on_card_wordCount = name_on_card_value.trim().replace(regex, ' ').split(' ').length;
				if (name_on_card_wordCount < 2) {
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#name_on_card');
					}
					msg = msg + 'Card name should contain firstname and lastname. <br>';

					errorfields_list.push({
						id: 'name_on_card',
						err_msg: 'This should contain first and last name.'
					});
				}
			}

			if(!$('#ccv').val()){
				if (status == true){
					status = false;
				}
				if (element == ''){
					element = $('#ccv');
				}
				msg = msg + 'CCV is required. <br>';

				errorfields_list.push({
					id: 'ccv',
					err_msg: 'CCV is required.'
				});
			}else {
				if($('#ccv').val().length < 3 || $('#ccv').val().length > 4){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#ccv');
					}
					msg = msg + 'CCV : ' + $('#ccv').val()+ ' Invalid number of digits. <br>';

					errorfields_list.push({
						id: 'ccv',
						err_msg: 'Invalid number of digits.'
					});
				}
			}

			if(!$('#expiry_year').val() || !$('#expiry_month').val()){
				if(!$('#expiry_year').val()){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#expiry_year');
					}
					msg = msg + 'Exp. Year is required. <br>';

					errorfields_list.push({
						id: 'expiry_year',
						err_msg: 'Exp. Year is required.'
					});
				}

				if(!$('#expiry_month').val()){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#expiry_month');
					}
					msg = msg + 'Exp. Month is required. <br>';

					errorfields_list.push({
						id: 'expiry_month',
						err_msg: 'Exp. Month is required.'
					});
				}
			}else{
				var today = new Date();
				if( parseInt($('#expiry_year').val()) < parseInt(today.getFullYear()) ) {
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#expiry_year');
					}
					msg = msg + 'Expiry Year : '+$('#expiry_year').val()+' is invalid. <br>';

					errorfields_list.push({
						id: 'expiry_year',
						err_msg: 'Exp. Year invalid.'
					});
				}else if(parseInt($('#expiry_year').val()) == parseInt(today.getFullYear())){
					if ( parseInt($('#expiry_month').val()) < parseInt(today.getMonth()) ) {
						if (status == true){
							status = false;
						}
						if (element == ''){
							element = $('#expiry_month');
						}
						msg = msg + 'Expiry Month : '+$('#expiry_month').val()+' is invalid. <br>';

						errorfields_list.push({
							id: 'expiry_month',
							err_msg: 'Exp. Month invalid.'
						});
					}
				}
			}


		}else if($('#payment_type_bank').is(':checked')) {
			if (gatewayname == 'paymentfile') {
				if(!$('#wfc_paymentfile_account_holder_name').val()) {
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#wfc_paymentfile_account_holder_name');
					}
					msg = msg + 'Account Holder name is required. <br>';

					errorfields_list.push({
						id: 'wfc_paymentfile_account_holder_name',
						err_msg: 'Account Holder name is required.'
					});
				}else{
					var name_on_card_value = $('#wfc_paymentfile_account_holder_name').val();
					var regex = /\s+/gi;
					var name_on_card_wordCount = name_on_card_value.trim().replace(regex, ' ').split(' ').length;
					if (name_on_card_wordCount < 2) {
						if (status == true){
							status = false;
						}
						if (element == ''){
							element = $('#wfc_paymentfile_account_holder_name');
						}
						msg = msg + 'Account Holder name should contain firstname and lastname. <br>';

						errorfields_list.push({
							id: 'wfc_paymentfile_account_holder_name',
							err_msg: 'This should contain first and last name.'
						});
					}
				}

				if(!$('#wfc_paymentfile_account_number').val()){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#wfc_paymentfile_account_number');
					}
					msg = msg + 'Account Number is required. <br>';

					errorfields_list.push({
						id: 'wfc_paymentfile_account_number',
						err_msg: 'Account Number is required.'
					});
				}

				if(!$('#wfc_paymentfile_bank_code').val()){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#wfc_paymentfile_bank_code');
					}
					msg = msg + 'Bank Code is required. <br>';

					errorfields_list.push({
						id: 'wfc_paymentfile_bank_code',
						err_msg: 'Bank Code is required.'
					});
				}

			}else{
				if(!$('#pdwp_account_holder_name').val()) {
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#pdwp_account_holder_name');
					}
					msg = msg + 'Account Holder name is required. <br>';

					errorfields_list.push({
						id: 'pdwp_account_holder_name',
						err_msg: 'Account Holder name is required.'
					});
				}else{
					var name_on_card_value = $('#pdwp_account_holder_name').val();
					var regex = /\s+/gi;
					var name_on_card_wordCount = name_on_card_value.trim().replace(regex, ' ').split(' ').length;
					if (name_on_card_wordCount < 2) {
						if (status == true){
							status = false;
						}
						if (element == ''){
							element = $('#pdwp_account_holder_name');
						}
						msg = msg + 'Account Holder name should contain firstname and lastname. <br>';

						errorfields_list.push({
							id: 'pdwp_account_holder_name',
							err_msg: 'This should contain first and last name.'
						});
					}
				}

				if(!$('#pdwp_account_number').val()){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#pdwp_account_number');
					}
					msg = msg + 'Account Number is required. <br>';

					errorfields_list.push({
						id: 'pdwp_account_number',
						err_msg: 'Account Number is required.'
					});
				}

				if(!$('#pdwp_bank_code').val()){
					if (status == true){
						status = false;
					}
					if (element == ''){
						element = $('#pdwp_bank_code');
					}
					msg = msg + 'Bank Code is required. <br>';

					errorfields_list.push({
						id: 'pdwp_bank_code',
						err_msg: 'Bank Code is required.'
					});
				}

			}
		}

		if (element == ''){
			element = $('body');
		}

		response = {
			status: status,
			msg: msg,
			element: element,
			highlight_errorfields: errorfields_list,
			unhighlight_errorfields: noneerrorfields_list
		};

		return response;
	}

	function unhighlightFieldErrorValidation(noneerrorfields_list){
		$('.wfc-don-invalid-field-message').remove();
		$.each(noneerrorfields_list, function(index, value) {
			$('#'+value.id).removeClass('wfc-don-invalid-field');
		});
	}

	function highlightFieldErrorValidation(errorfields_list){
		$('.wfc-don-invalid-field-message').remove();

		console.log(errorfields_list);

		$.each(errorfields_list, function(index, value) {
			var error_noti_elem = '<small style="color: red !important; font-size: 10px !important; opacity: 0.5;" class="wfc-don-invalid-field-message">'+value.err_msg+'</small>';
			$('#'+value.id).after(error_noti_elem);
			$('#'+value.id).addClass('wfc-don-invalid-field');
		});
	}
});	