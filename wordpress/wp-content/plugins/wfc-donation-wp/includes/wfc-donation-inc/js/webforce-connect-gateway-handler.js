var j = jQuery;
var WFCDONGateway = function() {

	this.capitalizeFirstLetter = function( string ) {
		return string.charAt(0).toUpperCase() + string.slice(1);
	}

	this.getName = function( gate ) {
		gate = gate.replace(j('#donation_sfinstance').val()+"_", "");
		var name = gate.split( '_' );
		return this.capitalizeFirstLetter( name[0] );
	}

	this.paymentGateway = null;
	this.gatewayConfig = null;
};

WFCDONGateway.prototype.getConfig = function( name ) {

	j('input[type=radio][name="payment_gateway"]:not(:checked)').attr('disabled', true);

	return new Promise( function( resolve, reject ) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_gateway_config',
				gatewayid : name,
				nonce: pdwp_ajax.nonce,
			},
			success: function( resp ) {

				j('input[type=radio][name="payment_gateway"]').removeAttr('disabled');

				if( resp.status == 'success' ) {
					resolve( resp.payment );
				} else {
					resolve( null );
				}

				j('#gateway-loader-container').remove();
				j('#credit-card-wrapper').show();
			},
			error: function( resp ) {

				j('input[type=radio][name="payment_gateway"]').removeAttr('disabled');

				resolve( null );
			}
		});
	});
};

WFCDONGateway.prototype.switchGateway = function( name ) {
	var self = this;
	var gateway = self.getName( name ) + 'Gateway';
	gateway = gateway.replace('Ech', 'Paydock');
	console.log(gateway);
	self.getConfig( name )
	.then(function( result ) {
		self.gatewayConfig = result;
		self.paymentGateway = new window[gateway]( result );	
	})
	.catch(function( error ){
		console.error( error );
	});	
};

WFCDONGateway.prototype.validateCreditcard = function( cc ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {
		var card = new CardValidate( cc.card_number.replace(/ /g,'') );
		var err = [];

		if( self.gatewayConfig.type.toLowerCase() != 'stripe' && self.gatewayConfig.type.toLowerCase() != 'nab' ) {
			if( typeof self.gatewayConfig.settings.PDWP_PGS_paypalredirection == 'undefined' || 
			 	 ( typeof self.gatewayConfig.settings.PDWP_PGS_paypalredirection != 'undefined' ) && 
					self.gatewayConfig.settings.PDWP_PGS_paypalredirection != 'true' ) {

				if( !card.isValid() ) {
					err.push( 'Your credit card number is not valid.' );
				}

				if( !cc.name_on_card.replace(/ /g,'') ) {
					err.push( 'Name on card is required.' );
				}

				if( !cc.ccv.replace(/ /g,'') ) {
					err.push( 'Ccv is required.' );
				}
			}
		}
		resolve(err);
	});
};


WFCDONGateway.prototype.processPayment = function( param, details ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {
		if( typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank' ) {
			if( !details.bank.bank_number ) {
				var err = [];
				err.push( 'Bank account number is required!' );
				resolve( {
					status : 'error',
					response : err,
					donation : param.donation
				} );
			} else if( !details.bank.bank_code ) {
				var err = [];
				err.push( 'Bank code is required!' );
				resolve( {
					status : 'error',
					response : err,
					donation : param.donation
				} );
			} else if( !details.bank.bank_name ) {
				var err = [];
				err.push( 'Account Holder name is required!' );
				resolve( {
					status : 'error',
					response : err,
					donation : param.donation
				} );
			} else if( details.bank.bank_number && details.bank.bank_number.length < 6 ) {
				var err = [];
				err.push( 'Bank account number must be atleast 6 characters!' );
				resolve( {
					status : 'error',
					response : err,
					donation : param.donation
				} );
			} else if( details.bank.bank_code && details.bank.bank_code.length < 6 ) {
				var err = [];
				err.push( 'Bank code must be atleast 6 characters!' );
				resolve( {
					status : 'error',
					response : err,
					donation : param.donation
				} );
			} else {
				// var details_bank_name = details.bank.bank_name.split(' ');
				// details_bank_name_0 = ( typeof details_bank_name[0] != 'undefined' ) ? details_bank_name[0] : '';
				// details_bank_name_1 = ( typeof details_bank_name[1] != 'undefined' ) ? details_bank_name[1] : '';
				// if ( !details_bank_name_0.trim() || !details_bank_name_1.trim()) {
				// 	var err = [];
				// 	err.push( 'Account Holder name should be in full name!' );
				// 	resolve( {
				// 		status : 'error',
				// 		response : err,
				// 		donation : param.donation
				// 	} );
				// }else{
					self.paymentGateway.processPayment( param, details.bank )
					.then( function( result ) {
						resolve( result );
					})
					.catch( function( error ) {
						reject( error );
					});						
				//}
			}
		} else {
			var gatewayname = param.donation.payment_gateway.split( '_' );
			var gatewayKey 	= gatewayname.length - 2;
			gatewayname 	= ( gatewayname.length > 1 ) 
							? gatewayname[gatewayKey] 
							: (( typeof gatewayname[0] != 'undefined' )
									? gatewayname[0] 
									: '');
			if(gatewayname != 'nab' && gatewayname != 'paypal' && gatewayname != 'stripe'){
				if( !details.creditcard.name_on_card ) {
					var err = [];
					err.push( 'Card Holder Name is required!' );
					resolve( {
						status : 'error',
						response : err,
						donation : param.donation
					} );
				} else if( !details.creditcard.card_number ) {
					var err = [];
					err.push( 'Card number is required!' );
					resolve( {
						status : 'error',
						response : err,
						donation : param.donation
					} );
				} else if( !details.creditcard.ccv ) {
					var err = [];
					err.push( 'CCV is required!' );
					resolve( {
						status : 'error',
						response : err,
						donation : param.donation
					} );
				} else{
					// var details_card_name = details.creditcard.name_on_card.split(' ');
					// details_card_name_0 = ( typeof details_card_name[0] != 'undefined' ) ? details_card_name[0] : '';
					// details_card_name_1 = ( typeof details_card_name[1] != 'undefined' ) ? details_card_name[1] : '';;
					// if ( !details_card_name_0.trim() || !details_card_name_1.trim()) {
					// 	var err = [];
					// 	err.push( 'Card Holder Name should be in full name!' );
					// 	resolve( {
					// 		status : 'error',
					// 		response : err,
					// 		donation : param.donation
					// 	} );
					// }else{
						self.paymentGateway.processPayment( param, details.creditcard )
						.then(function( result ) {
							resolve(result);
						})
						.catch(function( error ){
					 		reject( error );
					 	});
					//}
				}
			}else{
				self.paymentGateway.processPayment( param, details.creditcard )
				.then(function( result ) {
					resolve(result);
				})
				.catch(function( error ){
			 		reject( error );
			 	});
			}
		}
	});
};