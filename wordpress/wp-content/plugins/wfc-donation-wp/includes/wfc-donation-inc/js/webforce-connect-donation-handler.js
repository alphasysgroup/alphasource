/**
* jQuery
*/
var j = jQuery;

var DonateProcessor = function() {

};

var template_color = j('#template_color').val();
DonateProcessor.prototype.validateDetails = function( donation ) {
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'clean_validate_donor', 
				donation: donation,
				nonce: pdwp_ajax.nonce
			},
			success: function(resp) {
				console.log('DonateProcessor - validateDetails' );
				//console.log(resp);

				if (j('#pdwp_sub_amt').length) {
					if (j('#pdwp_sub_amt').attr('type') == 'text') {
						console.log(resp);
					}
				}	

				if( resp.status == 'failed' ) {
					if(resp.isPopupErrorMessage == true){

						//console.log(resp);
						//console.log(donation);

						SaiyanToolkit.spinner.hide();
						SaiyanToolkit.tinter.hide();
						SaiyantistCore.snackbar.hide();


						j('#alert-danger-popup-body').empty();
						j('#alert-danger').empty();
						if(resp.override_failure_message == 'true'){
							j('#alert-danger-popup-body').append(resp.override_message);
							j('#alert-danger').append(resp.override_message);
						}else {
							j.each(resp.form_error, function (key, value) {
								j('#alert-danger-popup-body').append(value);
								j('#alert-danger').append(value);
							});
						}

						if(j('#alert-danger-popup').hasClass('alert-danger-popup-hide') == true){
							j('#alert-danger-popup').removeClass('alert-danger-popup-hide');
							j('#alert-danger-popup').addClass('alert-danger-popup-show');
						}else {
							j('#alert-danger-popup').removeClass('alert-danger-popup-show');
							j('#alert-danger-popup').addClass('alert-danger-popup-hide');
							j('#alert-danger').css('display', 'block');
						}
						//j("#pdwp-form-donate").unbind('submit');
						j('#pdwp-btn').removeAttr('disabled');
						j('#pdwp-btn').removeClass('breath-loading');

						j('#card_number').val('');
						j('#expiry_month').val('');
						j('#expiry_year').val('');
						j('#name_on_card').val('');
						j('#ccv').val('');

						WFC_Donation_LoadingBar.load('Validation Complete', 100, template_color);
					}else {
						WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
						window.location.replace(resp.url);
					}

				} else {
					resolve( {
						status : 'success',
						donation : resp.donation
					} );
				}
			},
			error: function(resp) {
				reject( resp );
			}
		});
	});
};

DonateProcessor.prototype.resetForm = function( donation ) {
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_reset_form',
				param: donation,
				nonce: pdwp_ajax.nonce
			},
			success: function(resp) {
				if (j('#pdwp_sub_amt').length) {
					if (j('#pdwp_sub_amt').attr('type') == 'text') {
						console.log(resp);
					}
				}	
				
 				if( resp.status == 'success' ) {
					if(resp.isPopupErrorMessage == true){
						//console.log(resp);
						//console.log(donation);

						SaiyanToolkit.spinner.hide();
						SaiyanToolkit.tinter.hide();
						SaiyantistCore.snackbar.hide();


						j('#alert-danger-popup-body').empty();
						j('#alert-danger').empty();

						if(resp.override_failure_message == 'true'){
							j('#alert-danger-popup-body').append(resp.override_message);
							j('#alert-danger').append(resp.override_message);
						}else {
							j.each( resp.form_error, function( key, value ) {
								j('#alert-danger-popup-body').append(value);
								j('#alert-danger').append(value);
							});
						}


						if(j('#alert-danger-popup').hasClass('alert-danger-popup-hide') == true){
							j('#alert-danger-popup').removeClass('alert-danger-popup-hide');
							j('#alert-danger-popup').addClass('alert-danger-popup-show');
						}else {
							j('#alert-danger-popup').removeClass('alert-danger-popup-show');
							j('#alert-danger-popup').addClass('alert-danger-popup-hide');
							j('#alert-danger').css('display', 'block');
						}
						//j("#pdwp-form-donate").unbind('submit');
						j('#pdwp-btn').removeAttr('disabled');
						j('#pdwp-btn').removeClass('breath-loading');

						j('#card_number').val('');
						j('#expiry_month').val('');
						j('#expiry_year').val('');
						j('#name_on_card').val('');
						j('#ccv').val('');

						WFC_Donation_LoadingBar.load('Validation Complete', 100, template_color);
					}else {
						window.location.replace( resp.url );
					}

				}
			},
			error: function(resp) {
				reject( resp );
			}
		});
	});
};

DonateProcessor.prototype.donationData = function( donation, method ) {
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_donation_data',
				param: donation,
				method: method,
				nonce: pdwp_ajax.nonce
			},
			success: function( resp ) {
				console.log('Donation Data Saved');
				if (j('#pdwp_sub_amt').length) {
					if (j('#pdwp_sub_amt').attr('type') == 'text') {
						console.log(resp);
					}
				}	

 				resolve( resp );
			},
			error: function(resp) {
				console.log('Donation Data not Saved');
				reject( resp );
			}
		});
	});
};

DonateProcessor.prototype.syncdonationDataToSF = function( donation, donationForm ) {
	var self = this;
	return new Promise(function (resolve, reject) {	
		console.log('Syncing Donation Salesforce');
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'sync_donation_data',
				param: donation,
				getPaymentSource : self.getPaymentSource( donation, donationForm),
				nonce: pdwp_ajax.nonce
			},
			success: function( resp ) {
				if (j('#pdwp_sub_amt').length) {
					if (j('#pdwp_sub_amt').attr('type') == 'text') {
						console.log(resp);
					}
				}	
				
 				resolve( resp );
			},
			error: function(resp) {
				reject( resp );
			}
		});
	});
};

DonateProcessor.prototype.getPaymentSource = function( donation, donationForm ) {
	var donation_process = donation.donation;
	var donation_details = donationForm.donation;
	if( donation_process.hasOwnProperty( 'TokenCustomerID' ) && donation_process.TokenCustomerID != '' ) {
		return {
			ccno : ( typeof donationForm.creditcard.card_number != 'undefined' ) ? donationForm.creditcard.card_number.replace(/ /g,'') : '',
			ccname : ( typeof donationForm.creditcard.name_on_card != 'undefined' ) ? donationForm.creditcard.name_on_card : '',
			expmonth : ( typeof donationForm.creditcard.expiry_month != 'undefined' ) ? donationForm.creditcard.expiry_month : '',
			expyear : ( typeof donationForm.creditcard.expiry_year != 'undefined' ) ? donationForm.creditcard.expiry_year : '',
			ccv :  ( typeof donationForm.creditcard.ccv != 'undefined' ) ? donationForm.creditcard.ccv.replace(/ /g,'') : '',
			token : ( typeof donation_process.TokenCustomerID != 'undefined' ? donation_process.TokenCustomerID : null ),
			psType : 'token',
			type: 'cc'
		};
	} else if( donation_details.hasOwnProperty( 'payment_type' ) && donation_details.payment_type == 'bank' ) {
		return {
			bsbname : ( typeof donationForm.bank.bank_name != 'undefined' ? donationForm.bank.bank_name : null ),
			bsbno : ( typeof donationForm.bank.bank_code != 'undefined' ? donationForm.bank.bank_code : null ),
			bsbaccount : ( typeof donationForm.bank.bank_number != 'undefined' ? donationForm.bank.bank_number : null ),
			psType : 'bank',
			type: 'bank'
		};
	} else {
		return {
			ccno : ( typeof donationForm.creditcard.card_number != 'undefined' ) ? donationForm.creditcard.card_number.replace(/ /g,'') : '',
			ccname : ( typeof donationForm.creditcard.name_on_card != 'undefined' ) ? donationForm.creditcard.name_on_card : '',
			expmonth : ( typeof donationForm.creditcard.expiry_month != 'undefined' ) ? donationForm.creditcard.expiry_month : '',
			expyear : ( typeof donationForm.creditcard.expiry_year != 'undefined' ) ? donationForm.creditcard.expiry_year : '',
			ccv :  ( typeof donationForm.creditcard.ccv != 'undefined' ) ? donationForm.creditcard.ccv.replace(/ /g,'') : '',
			psType : 'cc',
			type: 'cc'
		};
	}
};

DonateProcessor.prototype.completeDonation = function( donation ) {
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_complete_donation',
				donation: donation,
				nonce: pdwp_ajax.nonce
			},
			success: function( resp ) {
				if( resp.status == 'success' ) {
					window.location.replace( resp.url );
				}
				if( resp.status == 'failed' ) {

					if(resp.isPopupErrorMessage == true){
						//console.log(resp);
						//console.log(donation);

						SaiyanToolkit.spinner.hide();
						SaiyanToolkit.tinter.hide();
						SaiyantistCore.snackbar.hide();


						j('#alert-danger-popup-body').empty();
						j('#alert-danger').empty();
						if(resp.override_failure_message == 'true'){
							j('#alert-danger-popup-body').append(resp.override_message);
							j('#alert-danger').append(resp.override_message);
						}else {
							j.each(resp.form_error, function (key, value) {
								j('#alert-danger-popup-body').append(value);
								j('#alert-danger').append(value);
							});
						}

						if(j('#alert-danger-popup').hasClass('alert-danger-popup-hide') == true){
							j('#alert-danger-popup').removeClass('alert-danger-popup-hide');
							j('#alert-danger-popup').addClass('alert-danger-popup-show');
						}else {
							j('#alert-danger-popup').removeClass('alert-danger-popup-show');
							j('#alert-danger-popup').addClass('alert-danger-popup-hide');
							j('#alert-danger').css('display', 'block');
						}
						//j("#pdwp-form-donate").unbind('submit');
						j('#pdwp-btn').removeAttr('disabled');
						j('#pdwp-btn').removeClass('breath-loading');

						j('#card_number').val('');
						j('#expiry_month').val('');
						j('#expiry_year').val('');
						j('#name_on_card').val('');
						j('#ccv').val('');

						WFC_Donation_LoadingBar.load('Validation Complete', 100, template_color);
					}else {
						window.location.replace(resp.url);
					}
				}
			},
			error: function(resp) {
				reject( resp );
			}
		});
	});
};

DonateProcessor.prototype.ShowPopupErrorMessage = function( donatio, resp ) {

};


