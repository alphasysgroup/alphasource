<?php
	
/**
 * Class WFC_DonationFormRenderer
 *
 * This class handles out the field to be renderer for donation form.
 * @author Junjie Canonio <junjie@alphasys.com.au>
 * @since 1.0.0
 *
 * @LastUpdated : April 26, 2018
 */
class WFC_DonationFormRenderer {

    /**
     * Enqueues styles and scripts for Donation Form.
     *
     *
     * @param array $styles Array of styles slug name
     * @param array $scripts Array of scripts slug name
     *
     * @return void
     *
     * @LastUpdated  August 22, 2018
     *@since  1.0.0
     *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     */
    public static function enqueue_styles_scripts($styles, $scripts) {
        if (is_array($styles) && ! empty($styles)) {
            foreach ($styles as $style) {
                wp_enqueue_style($style);
            }
        }

        if (is_array($scripts) && ! empty($scripts)) {
            foreach ($scripts as $script) {
                wp_enqueue_script($script);
            }
        }
    }

    /**
     * Return the form head tag element and end tag element.
     *
     *
     * @param $configs
     * @param $part
     *
     * @LastUpdated  May 23, 2018
     *@author Junjie Canonio <junjie@alphasys.com.au>
     * @since  1.0.0
     *
     */
    public static function render_form_tag($configs, $part){
        $formstyle_config = isset($configs['formstyle_config']) ? $configs['formstyle_config'] : array();
        $donation_form_class = isset($formstyle_config['donation_form_class']) ? $formstyle_config['donation_form_class'] : '';
        
        $pd_homeurl = isset($configs['settings']->pd_homeurl) ? $configs['settings']->pd_homeurl : '';
        $form_action = esc_url( $pd_homeurl . 'wp-admin/admin-post.php' )
        ?>

        <?php if(strtolower($part) == 'start'):?>
            <form id="pdwp-form-donate" method="post" class="<?php echo $donation_form_class;?>" action="<?php echo $form_action; ?>">
        <?php elseif(strtolower($part) == 'end'):?>
            </form>
        <?php else: ?>
            <div>Donation form tag error. Please provide second parameter either 'start' or 'end'.</div>
        <?php endif; ?>

        <?php
    }
    
    /**
	 * Renders fields for Donation form.
	 *
	 * @param $configs
     * @param null $field_options
     *
     * @LastUpdated  April 26, 2018
     *@author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
     *
     */
    public static function render_form_field($configs, $field_options = null){

        $formId = isset($configs['formId']) ? $configs['formId'] : '';
        $donation_v5_form_user_fields = get_post_meta( $formId, 'donation_v5_form_user_fields', true );
        $form_cache = isset($configs['cache']) ? $configs['cache'] : array();
        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '#1497c1';

        $geteway_settings =
        isset($configs['settings']) ? $configs['settings'] : array();

        $google_validation =
        isset($geteway_settings->enable_validation) ? $geteway_settings->enable_validation : 'false';

        $google_api_key =
        isset($geteway_settings->google_api_key) ? $geteway_settings->google_api_key : '';
        //print_r($geteway_settings);

        /*
        * Default grid
        */
        $field_grid_default = array(
            'donor_type' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_company' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_title' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
            ),
            'donor_first_name' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_last_name' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_anonymous' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
            ),
            'donor_email' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_phone' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_address' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
            ),
            'donor_country' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_state' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_postcode' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_suburb' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'donor_comment' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
            ),
        );
        if ($field_options != null && !empty($field_options) && is_array($field_options)) {
            $field_grid_default = array_merge($field_grid_default,$field_options);
        }


        /*
        * Harmony address validation special field
        */
        $donor_DPID = isset($form_cache['donor_DPID']) ? $form_cache['donor_DPID'] : '';
        $donor_barcode = isset($form_cache['donor_barcode']) ? $form_cache['donor_barcode'] : '';
        $donor_latitude = isset($form_cache['donor_latitude']) ? $form_cache['donor_latitude'] : '';
        $donor_longitude = isset($form_cache['donor_longitude']) ? $form_cache['donor_longitude'] : '';
        $address_arr = array('donor_address','donor_country','donor_state','donor_postcode','donor_suburb');
        $harmony_address_fields = array();

        ?>
        <div class="wfc_donation-donor-details-container row">
        <?php
        if(!empty($donation_v5_form_user_fields)):
            $donation_v5_form_user_fields = json_decode($donation_v5_form_user_fields,true);
            $donor_state_value = '';
        ?>
            <?php foreach ($donation_v5_form_user_fields as $key => $value_fields): ?>
                <?php
                $fieldtabindex = 101;
                foreach ($value_fields as $key => $value):
                
                    $field_label = isset($value['values']['field_label']) ? $value['values']['field_label'] : '';
                    $field_label = empty($value['values']['field_label']) ? '&nbsp;' : $field_label;
                    $field_key = isset($value['values']['field_key']) ? $value['values']['field_key'] : '';
                    $field_option = isset($value['values']['field_option']) ? $value['values']['field_option'] : '';
                    $field_ishidden = ($field_option == 'hide') ? 'display:none;' : '';
                    $field_isrequired = ($field_option == 'required') ? 'required' : '';
                    $field_type = isset($value['values']['field_type']) ? $value['values']['field_type'] : '';
                    $field_select_option = isset($value['values']['field_select_option']) ? $value['values']['field_select_option'] : '';
                    $field_checkbox_option = isset($value['values']['field_checkbox_option']) ? $value['values']['field_checkbox_option'] : '';
                    $field_select_option = explode(',', $field_select_option);

                    $gridclass = isset($field_grid_default[$field_key]['gridclass']) ? $field_grid_default[$field_key]['gridclass'] : 'col-12 col-sm-12 col-md-12 col-lg-12';
                    $field_val = isset($form_cache[$field_key]) ? $form_cache[$field_key] : '';
                    $field_checkbox_ischecked = ($field_val == 'true') ? 'checked' : '';

                    $field_default_value = isset($value['values']['field_default_value']) ? $value['values']['field_default_value'] : '';

                    // Compile radio button data
                    $field_radio_title = isset($value['values']['field_radio_title']) ? $value['values']['field_radio_title'] : '';
                    $field_radio_title = explode(',', $field_radio_title);
                    $field_radio_value = isset($value['values']['field_radio_value']) ? $value['values']['field_radio_value'] : '';
                    $field_radio_value = explode(',', $field_radio_value);
                    $field_radio_arr = array();
                    $radio_is_checked_default = false;
                    foreach ($field_radio_value as $key => $value){
                        $radio_title = isset($field_radio_title[$key]) ? $field_radio_title[$key] : $value;
                        $radio_is_checked = (!empty($field_val) && $field_val == $value) ? 'checked' : '';
                        $field_radio_arr[$key] = array(
                            'label' => $radio_title,
                            'value' => $value,
                            'is_checked' => $radio_is_checked,
                        );
                        $radio_is_checked_default = (!empty($radio_is_checked) && $radio_is_checked_default == false) ? true : false;
                    }
                    if ($radio_is_checked_default == false) {
                        $field_radio_arr[0]['is_checked'] = 'checked';
                    }
                    
                    // Set donor state cache value
                    if($field_key == 'donor_state'){
                        $donor_state_value = $field_val;
                    }
                ?>
                    <div id="<?php echo $field_key.'_container';?>" class="<?php echo $field_key.'_container'.' '.$gridclass;?>" style="<?php echo $field_ishidden;?>">
                        
                        <?php if($field_option=='required'): ?>
                            <label class="wfc-don-field-label" for="<?php echo $field_key;?>"><?php echo $field_label; ?> <span style="color:red;"> *</span></label>
                        <?php else: ?>
                            <label class="wfc-don-field-label" for="<?php echo $field_key;?>"><?php echo $field_label; ?></label>
                        <?php endif;?>
                        
                        <?php if($field_type == 'select' && $field_key == 'donor_country' && $google_validation == 'true' && !empty($google_api_key)): ?>
                            <script type="text/javascript">
                                (function($) {
                                    $(function() {

                                        var geo = new AS_GEO();
                                        geo.populateCountry( 'donor_country', 'donor_state' );
                                        <?php if(!empty($field_val)):?>
                                        $('#donor_country').val('<?php echo $field_val;?>').trigger('change');
                                        <?php else: ?>
                                        $('#donor_country').val('<?php echo $field_default_value;?>').trigger('change');
                                        <?php endif;?>

                                    });
                                })(window.jQuery);
                            </script>
                            <select tabindex="<?php echo $fieldtabindex;?>" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" <?php echo $field_isrequired;?> wfc-don-label="<?php echo $field_label; ?>">
                            </select>
                        <?php else: ?>
                        
                            <?php if($field_type == 'text'):?>
                                <input tabindex="<?php echo $fieldtabindex;?>" type="text" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" value="<?php echo $field_val;?>" <?php echo $field_isrequired;?> wfc-don-label="<?php echo $field_label; ?>"/>

                            <?php endif;?>

                            <?php if($field_type == 'email'):?>
                                <input tabindex="<?php echo $fieldtabindex;?>" type="email" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" value="<?php echo $field_val;?>" <?php echo $field_isrequired;?> wfc-don-label="<?php echo $field_label; ?>"/>
                            <?php endif;?>

                            <?php if($field_type == 'number'):?>
                                <input tabindex="<?php echo $fieldtabindex;?>" type="number" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" value="<?php echo $field_val;?>" <?php echo $field_isrequired;?> wfc-don-label="<?php echo $field_label; ?>"/>
                            <?php endif;?>

                            <?php if($field_type == 'checkbox'):?>
                                <label class="wfc-don-checkbox-container">
                                    <?php if($field_option=='required'): ?>
                                    <label class="wfc-don-field-label wfc-don-field-checkbox-label" for="<?php echo $field_key;?>"><?php echo $field_checkbox_option; ?> <span style="color:red;"> *</span></label>
                                    <?php else: ?>
                                    <label class="wfc-don-field-label wfc-don-field-checkbox-label" for="<?php echo $field_key;?>"><?php echo $field_checkbox_option; ?></label>
                                    <?php endif;?>
                                    <input tabindex="<?php echo $fieldtabindex;?>" type="checkbox" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" value="true" <?php echo $field_checkbox_ischecked;?> wfc-don-label="<?php echo $field_label; ?>"/>
                                    <span class="wfc-don-checkbox-checkmark"></span>
                                </label>
                            <?php endif;?>

                            <?php if($field_type == 'radio' && !empty($field_radio_arr)):?>
                                <div class="wfc-don-radio-<?php echo $field_key;?>-container">
                                <?php foreach ($field_radio_arr as $key => $value): ?>
                                    <label class="wfc-don-radio-container" for="<?php echo $value['value'];?>"><?php echo $value['label'];?>
                                        <input tabindex="<?php echo $fieldtabindex;?>" type="radio" id="<?php echo $value['value'];?>" name="<?php echo $field_key;?>" value="<?php echo $value['value'];?>" <?php echo $value['is_checked'];?> wfc-don-label="<?php echo $field_label; ?>"/>
                                        <span class="wfc-don-radio-checkmark"></span>
                                    </label>
                                <?php endforeach; ?>
                                </div>
                            <?php endif;?>

                            <?php if($field_type == 'select'):?>
                                <select tabindex="<?php echo $fieldtabindex;?>" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" <?php echo $field_isrequired;?> wfc-don-label="<?php echo $field_label; ?>">
                                    <?php foreach ($field_select_option as $key => $value): ?>
                                        <?php if($field_key == 'donor_type'):
                                            $lable_val = $value;
                                            $value = str_replace(' ', '', $value);
                                            $value = strtolower($value);
                                            $isselected = ($value == $field_val) ? 'selected' : '';
                                        ?>
                                            <option value="<?php echo $value; ?>" <?php echo $isselected;?>><?php echo $lable_val;?></option>
                                        <?php else:
                                            $isselected = ($value == $field_val) ? 'selected' : '';
                                        ?>
                                            <option value="<?php echo $value; ?>" <?php echo $isselected;?>><?php echo $value;?></option>
                                        <?php endif; ?>
                                    
                                    <?php endforeach; ?>
                                </select>
                            <?php endif;?>

                            <?php if($field_type == 'textarea'):?>
                                <textarea tabindex="<?php echo $fieldtabindex;?>" id="<?php echo $field_key;?>" name="<?php echo $field_key;?>" <?php echo $field_isrequired;?> wfc-don-label="<?php echo $field_label; ?>"><?php echo $field_val;?></textarea>
                            <?php endif;?>

                            <?php
                                if(in_array($field_key,$address_arr)){
                                    $harmony_address_fields[$field_key] = array(
                                        'label'    => $field_label,
                                        'value'    => $field_val,
                                        'hidden'   => $field_ishidden,
                                        'required' => $field_isrequired
                                    );
                                }

                            ?>
                            
                        <?php endif;?>

                    </div>

                    <?php $fieldtabindex = $fieldtabindex + 1; ?>
                <?php endforeach; ?>
            <?php endforeach; ?>
            <style type="text/css">
                /* When the checkbox is checked, add a blue background */
                .wfc-don-checkbox-container input:checked ~ .wfc-don-checkbox-checkmark {
                    background-color: <?php echo $template_color;?>;
                    transition: 200ms ease-out;
                }

                .wfc-don-radio-container input:checked ~ .wfc-don-radio-checkmark {
                    background-color: <?php echo $template_color;?>;
                    transition: 200ms ease-out;
                }
            </style>
            <script type="text/javascript">
                jQuery(document).ready(function($){
                    $('#donor_state').val( '<?php echo $donor_state_value;?>' );
                });
            </script>
            <div id="wfc-harmony-address-validation-main-cont" class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div id="wfc-harmony-address-validation-sub-cont">
                    <div id="wfc-harmony-address-validation-sub-cont-title">
                        <label class="wfc-don-field-label"><b>Address Validation</b></label>
                    </div>
                    <?php foreach ($harmony_address_fields as $key => $value):
                        $harmony_field_label = isset($value['label']) ? $value['label'] : 'None';
                        $harmony_field_value = isset($value['value']) ? $value['value'] : 'None';
                        $harmony_field_hidden = isset($value['hidden']) ? $value['hidden'] : 'None';
                        $harmony_field_required = isset($value['required']) ? $value['required'] : 'None';
                    ?>
                    <div id="<?php echo 'wfc-harmony-'.$key.'-cont';?>" class="wfc-harmony-field-cont">
                        <?php if($harmony_field_required=='required'): ?>
                            <label class="wfc-don-field-label"><b><?php echo $harmony_field_label; ?> :</b> <span id="<?php echo 'wfc-harmony-'.$key.'-field';?>"><?php echo $harmony_field_value; ?></span><span style="color:red;"> *</span></label>
                        <?php else: ?>
                            <label class="wfc-don-field-label"><b><?php echo $harmony_field_label; ?> :</b> <span id="<?php echo 'wfc-harmony-'.$key.'-field';?>"><?php echo $harmony_field_value; ?></span></label>
                        <?php endif;?>
                    </div>
                    <?php endforeach; ?>
                    <div id="<?php echo 'wfc-harmony-dpid-cont';?>" class="wfc-harmony-field-cont">
                        <label class="wfc-don-field-label"><b>DPID :</b> <span id="<?php echo 'wfc-harmony-dpid-field';?>"><?php echo $donor_DPID; ?></span><span style="color:red;"> *</span></label>
                    </div>
                    <div id="<?php echo 'wfc-harmony-barcode-cont';?>" class="wfc-harmony-field-cont">
                        <label class="wfc-don-field-label"><b>Barcode :</b> <span id="<?php echo 'wfc-harmony-barcode-field';?>"><?php echo $donor_barcode; ?></span><span style="color:red;"> *</span></label>
                    </div>
                    <div id="<?php echo 'wfc-harmony-latitude-cont';?>" class="wfc-harmony-field-cont">
                        <label class="wfc-don-field-label"><b>Latitude :</b> <span id="<?php echo 'wfc-harmony-latitude-field';?>"><?php echo $donor_latitude; ?></span><span style="color:red;"> *</span></label>
                    </div>
                    <div id="<?php echo 'wfc-harmony-longitude-cont';?>" class="wfc-harmony-field-cont">
                        <label class="wfc-don-field-label"><b>Longitude :</b> <span id="<?php echo 'wfc-harmony-longitude-field';?>"><?php echo $donor_longitude; ?></span><span style="color:red;"> *</span></label>
                    </div>
                </div>
            </div>
        <?php else:?>
            <h2>No fields set for this form.</h2>
        <?php endif;?>
        </div>
        <?php
    }

    /**
    * Returns and renders donation amount.
    *
    * @param $configs
    *
    * @LastUpdated  May 3, 2018
    *@since  1.0.0
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    */
    public static function render_donation_amount($configs){
        /*
         * get donation form amount configuration
         */
        $amount_config = isset($configs['amount_config']) ? $configs['amount_config'] : array();
        $donation_form_accept_cents = isset($amount_config['donation_form_accept_cents']) ? $amount_config['donation_form_accept_cents'] : 'false';
        $donation_form_custom_amount = isset($amount_config['donation_form_custom_amount']) ? $amount_config['donation_form_custom_amount'] : 'false';
        $donation_form_custom_amount_default = isset($amount_config['donation_form_custom_amount_default']) ? $amount_config['donation_form_custom_amount_default'] : 'false';
        $donation_form_separate_amtfield_from_amtbtns = isset($amount_config['donation_form_separate_amtfield_from_amtbtns']) ? $amount_config['donation_form_separate_amtfield_from_amtbtns'] : 'false';
        $donation_v5_form_amount_levels = isset($amount_config['donation_v5_form_amount_levels']) ? $amount_config['donation_v5_form_amount_levels'] : array();
        $donation_v5_form_amount_display_type = isset($amount_config['donation_v5_form_amount_display_type']) ? $amount_config['donation_v5_form_amount_display_type'] : 'default';
        $donation_v5_form_amount_card_image_position = isset($amount_config['donation_v5_form_amount_card_image_position']) ? $amount_config['donation_v5_form_amount_card_image_position'] : 'top';
        $currency_symbol = isset($amount_config['currency_symbol']) ? $amount_config['currency_symbol'] : '&#36;';
        $wfc_donation_form_min_amount = isset($amount_config['donation_min_amount']) ? $amount_config['donation_min_amount'] : 0;
        

        /*
         * get donation form amt and otr
         */
        $url_amount = isset($configs['url_amount']) ? $configs['url_amount'] : array();
        $pdwp_amount_val = '';
        $pdwp_custom_amount_val = '';
        if (isset($url_amount['otr']) && !empty($url_amount['otr'])) {
            $donation_form_custom_amount_default = 'true';
            $pdwp_custom_amount_val = $url_amount['otr'];
        }elseif (isset($url_amount['amt']) && !empty($url_amount['amt']) &&
            (isset($url_amount['otr']) || empty($url_amount['otr']))) {
            $pdwp_amount_val = $url_amount['amt'];
        }


        /*
         * get donation form cache amount
         */
        $cache = isset($configs['cache']) ? $configs['cache'] : array();
        $pdwp_custom_amount_cache_val = '';
        if (isset($cache['pdwp_custom_amount']) && !empty($cache['pdwp_custom_amount'])) {
            $donation_form_custom_amount_default = 'true';
            $pdwp_custom_amount_cache_val = $cache['pdwp_custom_amount'];
        }elseif (isset($cache['pdwp_amount']) && !empty($cache['pdwp_amount']) &&
            (isset($cache['pdwp_custom_amount']) || empty($cache['pdwp_custom_amount']))) {
            $donation_form_custom_amount_default = 'false';
            $pdwp_amount_val = $cache['pdwp_amount'];
        }

        if (!empty($pdwp_custom_amount_val) && empty($pdwp_custom_amount_cache_val) && empty($pdwp_amount_val)) {
            $pdwp_custom_amount_val = $pdwp_custom_amount_val;
        }else{
            $pdwp_custom_amount_val = '';
        }

        /*
         * get donation form template color
         */
        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '#1497c1';
        $template_btn_font_color = isset($configs['template_btn_font_color']) ? $configs['template_btn_font_color'] : '#ffffff';


        /*
         * set container Class for Card type amount
         */
        if($donation_v5_form_amount_display_type == 'card' && ($donation_v5_form_amount_card_image_position == 'top' || $donation_v5_form_amount_card_image_position == 'bottom')){
            $amount_cardtype_class = 'wfc_donation-amount-level-container-card-ver';
            $amount_cardtype_imgcont_class = 'wfc_donation-amount-level-container-card-ver-img-cont';
            $amount_cardtype_infocont_class = 'wfc_donation-amount-level-container-card-ver-info-cont';
        }elseif ($donation_v5_form_amount_display_type == 'card' && ($donation_v5_form_amount_card_image_position == 'right' || $donation_v5_form_amount_card_image_position == 'left')) {
            $amount_cardtype_class = 'wfc_donation-amount-level-container-card-hor';
            $amount_cardtype_imgcont_class = 'wfc_donation-amount-level-container-card-hor-img-cont';
            $amount_cardtype_infocont_class = 'wfc_donation-amount-level-container-card-hor-info-cont';
        }else{
            $amount_cardtype_class = '';
            $amount_cardtype_imgcont_class = '';
            $amount_cardtype_infocont_class = '';
        }

        ?>
        <div id="wfc_donation-amount-container" class="wfc_donation-amount-container row">
            <?php if($donation_form_custom_amount == 'false'): ?>
                <?php if($donation_form_separate_amtfield_from_amtbtns == 'true' || $donation_v5_form_amount_display_type == 'card'): ?>
                <div id="wfc_donation-amount-level-main-cont" class="col-12 col-sm-12 col-md-12 col-lg-12">
                <?php else: ?>
                <div id="wfc_donation-amount-level-main-cont" class="col-12 col-sm-12 col-md-6 col-lg-6">
                <?php endif; ?>
            <?php else: ?>
                <div id="wfc_donation-amount-level-main-cont" class="col-12 col-sm-12 col-md-12 col-lg-12">
            <?php endif; ?>
                <label class="wfc-don-field-label" for="wfc_donation-amount-level-container">Choose Amount</label>

                <div id="wfc_donation-amount-level-container" class="wfc_donation-amount-level-container <?php echo esc_html($amount_cardtype_class); ?>" display-type="<?php echo esc_html($donation_v5_form_amount_display_type); ?>">
                <?php if(!empty($donation_v5_form_amount_levels) && is_array($donation_v5_form_amount_levels)): ?>
                    <?php foreach ($donation_v5_form_amount_levels as $key => $value_amount_levels): ?>
                        <?php foreach ($value_amount_levels as $key_amount_level => $value):
                            $amount_level = isset($value['values']['amount_level']) ? $value['values']['amount_level'] : '';
                            $is_default = isset($value['values']['is_default']) ? $value['values']['is_default'] : 'false';
                            $is_default = ($is_default == 'true' && $donation_form_custom_amount == 'false' && $donation_form_custom_amount_default == 'false') ? 'checked' : '';
                            if (!empty($pdwp_amount_val) && $amount_level == $pdwp_amount_val) {
                                $is_default = 'checked';
                            }

                            $amount_title = isset($value['values']['title']) ? $value['values']['title'] : '';
                            $amount_image = isset($value['values']['amount_image']) ? $value['values']['amount_image'] : '';
                            $amount_image_url = !empty($amount_image) ? wp_get_attachment_image_src($amount_image, 'full') : array();
                            $amount_image_url = isset($amount_image_url[0]) ? $amount_image_url[0] : 'https://via.placeholder.com/600x400';
                            $amount_description = isset($value['values']['description']) ? $value['values']['description'] : '';
                        ?>
                        <input tabindex="11" type="radio" id="<?php echo $key_amount_level;?>" name="pdwp_amount" value="<?php echo $amount_level;?>"
                        <?php echo $is_default;?>/>
                            <?php if($donation_v5_form_amount_display_type == 'card') :?>
                                <label for="<?php echo esc_html($key_amount_level);?>" class="wfc_donation-amount-card">
                                    <?php if($donation_v5_form_amount_card_image_position == 'top' || $donation_v5_form_amount_card_image_position == 'left'): ?>
                                    <div class="<?php echo esc_html($amount_cardtype_imgcont_class); ?>">
                                        <div style="background-image: url(<?php echo $amount_image_url; ?>);"></div>
                                    </div>
                                    <?php endif; ?>
                                    <div class="<?php echo esc_html($amount_cardtype_infocont_class); ?>">
                                        <span class="wfc_donation-amount-card-amount"><?php echo esc_html($currency_symbol.' '.$amount_level); ?></span>
                                        <span class="wfc_donation-amount-card-title"><?php echo esc_html($amount_title);?></span>
                                        <span class="wfc_donation-amount-card-desc"><?php echo esc_html($amount_description);?></span>
                                    </div>
                                    <?php if($donation_v5_form_amount_card_image_position == 'bottom' || $donation_v5_form_amount_card_image_position == 'right'): ?>
                                    <div class="<?php echo esc_html($amount_cardtype_imgcont_class); ?>">
                                        <div style="background-image: url(<?php echo $amount_image_url; ?>);"></div>
                                    </div>
                                    <?php endif; ?>
                                </label>
                            <?php else : ?>
                                <label for="<?php echo esc_html($key_amount_level);?>"><?php echo esc_html($currency_symbol.' '.$amount_level);?></label>
                            <?php endif;?>
                        <?php endforeach;?>
                    <?php endforeach;?>
                <?php endif; ?>
                <?php if($donation_form_custom_amount == 'false' && $donation_v5_form_amount_display_type != 'card'):
                $custom_amount_default = ($donation_form_custom_amount == 'false' && $donation_form_custom_amount_default == 'true') ? 'checked' : '';
                ?>
                    <input
                        tabindex="12"
                        type="radio" id="custom-amount"
                        name="pdwp_amount" value="0"
                        otr="<?php echo $pdwp_custom_amount_val; ?>"
                        cache="<?php echo $pdwp_custom_amount_cache_val; ?>"
                        <?php echo $custom_amount_default;?>/>
                    <label for="custom-amount">Other</label>
                <?php endif; ?>
                </div>
            </div>


            <?php if($donation_form_custom_amount == 'false'): ?>
                <?php if($donation_form_separate_amtfield_from_amtbtns == 'true' || $donation_v5_form_amount_display_type == 'card'): ?>
                    <div id="wfc_donation-amount-other-main-cont" class="col-12 col-sm-12 col-md-12 col-lg-12">
                <?php else: ?>
                    <div id="wfc_donation-amount-other-main-cont" class="col-12 col-sm-12 col-md-6 col-lg-6">
                <?php endif; ?>
            <?php else: ?>
                <div id="wfc_donation-amount-other-main-cont" class="col-12 col-sm-12 col-md-6 col-lg-6" style="display: none !important;">
            <?php endif; ?>
                <div class="wfc_donation-amount-other-container">
                    <label for="pdwp_custom_amount" class="wfc-don-field-label">Amount</label>
                    <div style="position: relative;">
                        <span><?php echo $currency_symbol; ?></span>
                        <input
                            tabindex="13"
                            type="number"
                            id="pdwp_custom_amount"
                            name="pdwp_custom_amount"
                            value=""
                            min="<?php echo $wfc_donation_form_min_amount; ?>"
                            max="10000"
                            <?php if($donation_form_accept_cents == 'true') : ?>
                            step="0.01"
                            <?php endif; ?>
                            required/>
                    </div>
                </div>
            </div>

        </div>
        <style type="text/css">
            .wfc_donation-amount-level-container input[type="radio"]:checked + label{
                background-color: <?php echo $template_color;?>;
                color: <?php echo $template_btn_font_color;?>;
            }

            .wfc_donation-amount-level-container .wfc_donation-amount-card{
                display: inline-grid;
            }
        </style>
        <?php
    }

    /**
    * Returns and renders donation tribute field.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    * @since  1.0.0
    *
    * @param $configs
    *
    * @LastUpdated  May 3, 2018
    */
    public static function render_donation_tibutefield($configs){

        $form_cache = isset($configs['cache']) ? $configs['cache'] : array();
        $tributefield_config = isset($configs['tributefield_config']) ? $configs['tributefield_config'] : array();
        $donation_form_gift = isset($tributefield_config['donation_form_gift']) ? $tributefield_config['donation_form_gift'] : 'false';
        $donation_form_gift_caption = isset($tributefield_config['donation_form_gift_caption']) ? $tributefield_config['donation_form_gift_caption'] : 'This gift is in memory';

        $pdwp_gift = isset($form_cache['pdwp_gift']) ? $form_cache['pdwp_gift'] : '';
        $tibutefield_ischecked = ($pdwp_gift == 'true') ? 'checked' : '';
        $pdwp_gift_text_val = isset($form_cache['pdwp_gift_text']) ? $form_cache['pdwp_gift_text'] : '';

        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '#1497c1';
        if($donation_form_gift == 'true'){
        ?>
            <div class="wfc_donation-tributefield-container row">
                <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                    <label class="wfc-don-tributefield-container"><?php echo $donation_form_gift_caption; ?>
                        <input type="checkbox" id="pdwp_gift" name="pdwp_gift" value="true" <?php echo $tibutefield_ischecked;?>/>
                        <span class="wfc-don-tributefield-checkmark"></span>
                    </label>
                    <textarea id="pdwp_gift_text" name="pdwp_gift_text"><?php echo $pdwp_gift_text_val;?></textarea>
                </div>
            </div>
            <style type="text/css">
                /* When the checkbox is checked, add a blue background */
                .wfc-don-tributefield-container input:checked ~ .wfc-don-tributefield-checkmark {
                    background-color: <?php echo $template_color;?>;
                    transition: 200ms ease-out;
                }
            </style>
        <?php
        }
    }

    /**
    * Returns and renders donation type.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    * @since  1.0.0
    *
    * @param $configs
    *
    * @LastUpdated  May 16, 2018
    */
    public static function render_donation_type($configs){
        /*
         * get donation form type
         */
        $url_dontype = isset($configs['url_dontype']) ? $configs['url_dontype'] : array();
        $url_dontype_val = 'one-off';
        if (isset($url_dontype['type']) && !empty($url_dontype['type'])) {
            $url_dontype_val = $url_dontype['type'];
        }

        $form_cache = isset($configs['cache']) ? $configs['cache'] : array();
        $checked_frequency = isset($form_cache['pdwp_donation_type']) ? $form_cache['pdwp_donation_type'] : $url_dontype_val;
        $donationtypes_config = isset($configs['donationtypes_config']) ? $configs['donationtypes_config'] : array();
        $frequencies_arr = isset($donationtypes_config['frequencies_arr']) ? $donationtypes_config['frequencies_arr'] : array();
        $donation_types = isset($donationtypes_config['donation_form_donation_types']) ? $donationtypes_config['donation_form_donation_types'] : array();

        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '#1497c1';
        ?>
        <div id="wfc_donation-frequency-container" class="wfc_donation-frequency-container row">
            <div id="wfc_donation-frequency-sub-container" class="col-12 col-sm-12 col-md-12 col-lg-12">
                <?php if(!empty($donation_types) && is_array($donation_types) && !empty($frequencies_arr)): ?>
                    <?php foreach ($donation_types as $key_donation_type => $value):
                        $is_checked = ($checked_frequency == $key_donation_type) ? 'checked' : '';
                    ?>
                        <?php if($value=='true'): ?>
                        <label class="wfc-donation-frequency-radio-container" for="<?php echo $key_donation_type;?>"><?php echo $frequencies_arr[$key_donation_type];?>
                            <input type="radio" id="<?php echo $key_donation_type;?>" name="pdwp_donation_type" value="<?php echo $key_donation_type;?>" donation-type-label="<?php echo $frequencies_arr[$key_donation_type];?>" <?php echo $is_checked;?> />
                            <span class="wfc-donation-frequency-radio-checkmark"></span>
                        </label>
                        <?php endif; ?>
                    <?php endforeach;?>
                <?php endif; ?>
            </div>
        </div>
        <style>
            .wfc-donation-frequency-radio-container input:checked ~ .wfc-donation-frequency-radio-checkmark {
                background-color: <?php echo $template_color;?>;
                transition: 200ms ease-out;
            }
        </style>
        <?php
    }

    /**
    * Returns and renders activated Payment gateways.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    * @since  1.0.0
    *
    * @param $configs
    *
    * @LastUpdated  May 2, 2018
    */
    public static function render_gateway($configs){

        $hidden_config = isset($configs['hidden']) ? $configs['hidden'] : array();
        $sfinstance = isset($hidden_config['sfinstance']) ? $hidden_config['sfinstance'] : '';

        $gateway_config = isset($configs['gateway_config']) ? $configs['gateway_config'] : array();


        $donation_v5_form_payment_gateways =
        isset($gateway_config['donation_v5_form_payment_gateways']) ? $gateway_config['donation_v5_form_payment_gateways'] : array();
        $donation_v5_form_payment_gateways = json_decode($donation_v5_form_payment_gateways,true);

        $pronto_donation_gateway_list =
        isset($gateway_config['pronto_donation_gateway_list']) ? $gateway_config['pronto_donation_gateway_list'] : array();
        
        $geteway_settings =
        isset($gateway_config['geteway_settings']) ? $gateway_config['geteway_settings'] : array();

        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '#1497c1';
        ?>
        <div id="wfc_donation-payment-gateway-container" class="wfc_donation-payment-gateway-container row">
            <div class="wfc_donation-payment-gateway-container-sub col-12 col-sm-12 col-md-12 col-lg-12">
        <?php
        if(!empty($donation_v5_form_payment_gateways) && !empty($geteway_settings)):
            foreach ($donation_v5_form_payment_gateways as $form_payment_gateways_key => $form_payment_gateways_value):
                foreach ($form_payment_gateways_value as $key => $value):
                    $gateway_key = str_replace($sfinstance.'_', '', $key);
                    $gateway_name = explode('_', $gateway_key);
                    $gateway_name = ucfirst($gateway_name[0]);
                    $wfc_donation_showlogo = isset($geteway_settings[$key]['wfc_donation_showlogo']) ?
                    $geteway_settings[$key]['wfc_donation_showlogo'] : 'false';
                    $wfc_donation_logo = isset($geteway_settings[$key]['wfc_donation_logo']) ?
                    $geteway_settings[$key]['wfc_donation_logo'] : '';


                    $wfc_donation_bankmode = isset($geteway_settings[$key]['wfc_donation_bankmode']) ?
                    $geteway_settings[$key]['wfc_donation_bankmode'] : 'false';
                    $wfc_donation_gatewaylabel = isset($geteway_settings[$key]['wfc_donation_gatewaylabel']) ?
                    $geteway_settings[$key]['wfc_donation_gatewaylabel'] : $gateway_name;
                    if(isset($value['value']) && $value['value'] == 'true'):
                    ?>
                    <div class="wfc_payment_gateway-radio-img">
                        <input type="radio" id="<?php echo $key;?>" name="payment_gateway" value="<?php echo $key;?>" wfc-don-gateway="<?php echo $gateway_name;?>" wfc-don-gateway-bank="<?php echo $wfc_donation_bankmode;?>"/>
                        <label class="wfc-don-field-label" for="<?php echo $key;?>">
                            <?php if($wfc_donation_showlogo == 'true' && !empty($wfc_donation_logo)) :?>
                                <?php echo wp_get_attachment_image($wfc_donation_logo);?>
                            <?php else :?>
                                <img src="<?php echo plugin_dir_url( __FILE__ ).'../../images/'.strtolower($gateway_name).'.png';?>">
                            <?php endif;?>
                            <?php echo $wfc_donation_gatewaylabel; ?>
                        </label>
                    </div>
                    <?php
                    endif;
                endforeach;
            endforeach;
        endif;
        ?>
            </div>
        </div>
        <style type="text/css">
            .wfc_payment_gateway-radio-img input:checked+label img{
                box-shadow: 0 0 0 2px #fffeff, 0 0 0 5px <?php echo $template_color;?>;
                transition: 200ms ease-out;
            }
        </style>
        <?php
    }

    /**
    * Returns and renders payment credit and debit card fields.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    * @since  1.0.0
    *
    * @param $configs
    * @param null $field_options
    *
    * @LastUpdated  May 21, 2018
    */
    public static function render_payment_details($configs, $field_options = null){
        /*
        * Default grid
        */
        $field_grid_default = array(
            'payment_type_credit_card' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'payment_type_bank' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'card_number' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'ccv' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'name_on_card' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'expiry_month' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
            ),
            'expiry_year' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-3 col-lg-3',
            ),
            'pdwp_bank_code' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'pdwp_account_number' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-6 col-lg-6',
            ),
            'pdwp_account_holder_name' => array(
                'gridclass' => 'col-12 col-sm-12 col-md-12 col-lg-12',
            ),
        );
        if ($field_options != null && !empty($field_options)) {
            $field_grid_default = array_merge($field_grid_default,$field_options);
        }

        $paymentdetials_config = isset($configs['paymentdetials_config']) ? $configs['paymentdetials_config'] : array();
        $donation_form_credit = isset($paymentdetials_config['donation_form_credit']) ? $paymentdetials_config['donation_form_credit'] : array();
        $donation_form_debit = isset($paymentdetials_config['donation_form_debit']) ? $paymentdetials_config['donation_form_debit'] : array();

        $credit_cardgridclass = isset($field_grid_default['payment_type_credit_card']['gridclass']) ? $field_grid_default['payment_type_credit_card']['gridclass'] : '';
        $bankgridclass = isset($field_grid_default['payment_type_bank']['gridclass']) ? $field_grid_default['payment_type_bank']['gridclass'] : '';

        $expiry_month = self::_get_expiry_month();
        $expiry_year = self::_get_expiry_year();

        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '#1497c1';
        ?>
        <div id="wfc_donation-payment-details-container" class="wfc_donation-payment-details-container row">
            <div id="wfc_donation-payment-details-toogle-container" class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div id="payment-type-wrapper" class="payment-type-wrapper row">
                    <div id="<?php echo 'payment_type_credit_card_container_cc';?>" class="<?php echo 'payment_type_credit_card_container'.' '.$credit_cardgridclass;?>">
                        <label class="wfc-donation-paymenttype-radio-container" for="payment_type_credit_card">Credit Card
                            <input type="radio" id="payment_type_credit_card" class="" name="payment_type" checked="checked" value="credit_card"/>
                            <span class="wfc-donation-paymenttype-radio-checkmark"></span>
                        </label>
                    </div>
                    <div id="<?php echo 'payment_type_credit_card_container_dc';?>" class="<?php echo 'payment_type_credit_card_container'.' '.$bankgridclass;?>">
                        <label class="wfc-donation-paymenttype-radio-container wfc-don-field-label" for="payment_type_bank">Direct Debit
                            <input type="radio" id="payment_type_bank" class="" name="payment_type" value="bank"/>
                            <span class="wfc-donation-paymenttype-radio-checkmark"></span>
                        </label>
                    </div>
                </div>
                <div id="credit-card-wrapper" class="wfc-donation-credit-card-wrapper row">
                    <?php
                    $fieldtabindex = 201;
                    foreach ($donation_form_credit as $key => $value):
                        $gridclass = isset($field_grid_default[$key]['gridclass']) ? $field_grid_default[$key]['gridclass'] : '';
                        $fieldlabel = isset($value['label']) ? $value['label'] : '';
                        $fieldplaceholder = isset($value['placeholder']) ? $value['placeholder'] : '';
                    ?>
                        <?php if($key != 'expiry_month' && $key != 'expiry_year'): ?>
                        <div id="<?php echo $key.'_container';?>" class="<?php echo $key.'_container'.' '.$gridclass;?>">
                            <label class="wfc-don-field-label" for="<?php echo $key;?>"><?php echo $fieldlabel;?> <span style="color:red;"> *</span></label>
                            <input tabindex="<?php echo $fieldtabindex; ?>" type="text" id="<?php echo $key;?>" name="" value="" placeholder="<?php echo $fieldplaceholder;?>" wfc-don-label="<?php echo $fieldlabel;?>">
                        </div>
                        <?php else: ?>
                            <?php if($key == 'expiry_month'): ?>
                                <div id="<?php echo $key.'_container';?>" class="<?php echo $key.'_container'.' '.$gridclass;?>">
                                    <label class="wfc-don-field-label" for="<?php echo $key;?>"><?php echo $fieldlabel;?> <span style="color:red;"> *</span></label>
                                    <select tabindex="<?php echo $fieldtabindex; ?>" id="<?php echo $key;?>" name="" wfc-don-label="<?php echo $fieldlabel;?>">
                                         <option></option>
                                        <?php foreach ($expiry_month as $expiry_month_key => $expiry_month_value): ?>
                                        <option value="<?php echo $expiry_month_key;?>"><?php echo $expiry_month_value;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>
                            <?php if($key == 'expiry_year'): ?>
                                <div id="<?php echo $key.'_container';?>" class="<?php echo $key.'_container'.' '.$gridclass;?>">
                                    <label class="wfc-don-field-label" for="<?php echo $key;?>"><?php echo $fieldlabel;?> <span style="color:red;"> *</span></label>
                                    <select tabindex="<?php echo $fieldtabindex; ?>" id="<?php echo $key;?>" name="" wfc-don-label="<?php echo $fieldlabel;?>">
                                        <option></option>
                                        <?php foreach ($expiry_year as $expiry_year_key => $expiry_year_value): ?>
                                        <option value="<?php echo $expiry_year_key;?>"><?php echo $expiry_year_value;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php $fieldtabindex = $fieldtabindex + 1; ?>
                    <?php endforeach; ?>
                </div>
                <div id="bank-details-wrapper" class="wfc-donation-credit-bank-details-wrapper row">
                    <?php
                    $fieldtabindex_b = 211;
                    foreach ($donation_form_debit as $key => $value):
                        $gridclass = isset($field_grid_default[$key]['gridclass']) ? $field_grid_default[$key]['gridclass'] : '';
                        $fieldlabel = isset($value['label']) ? $value['label'] : '';
                        $fieldplaceholder = isset($value['placeholder']) ? $value['placeholder'] : '';
                    ?>
                        <div id="<?php echo $key.'_container';?>" class="<?php echo $key.'_container'.' '.$gridclass;?>">
                            <label class="wfc-don-field-label" for="<?php echo $key;?>"><?php echo $fieldlabel;?> <span style="color:red;"> *</span></label>
                            <input tabindex="<?php echo $fieldtabindex_b; ?>" type="text" id="<?php echo $key;?>" name="" value="" placeholder="<?php echo $fieldplaceholder;?>" wfc-don-label="<?php echo $fieldlabel;?>">
                        </div>
                        <?php $fieldtabindex_b = $fieldtabindex_b + 1; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <style type="text/css">
            .wfc-donation-paymenttype-radio-container input:checked ~ .wfc-donation-paymenttype-radio-checkmark {
                background-color: <?php echo $template_color;?>;
                transition: 200ms ease-out;
            }

            #gateway-loader-container{
                margin-bottom: 30px;
                transition: 200ms ease-out;
            }
            <?php
                /**
                  * @ignore
                  */
            ?>
            #gateway-loader {
                margin-top: 30px;
                border: 16px solid #f3f3f3;
                border-top: 16px solid <?php echo $template_color; ?> !important;
                border-radius: 50%;
                width: 150px;
                height: 150px;
                animation: spin 2s linear infinite;

                display: inline-block;
                vertical-align: middle;
                opacity: .7;
                filter: alpha(opacity=70);
                margin-left: 10px;
                transition: 200ms ease-out;
            }

            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
        <?php
    }

    /**
    * Returns and renders google recaptcha.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    * @since  1.0.0
    *
    * @param $configs
    *
    * @LastUpdated  May 23, 2018
    */
    public static function render_google_captcha($configs){
        $settings = isset($configs['settings']) ? $configs['settings'] : array();
        $enable_recaptcha = isset( $settings->enable_recaptcha ) ? $settings->enable_recaptcha : false;
        if( $enable_recaptcha == 'true' ){
            $google_site_key = isset( $settings->google_site_key ) ? $settings->google_site_key : '';
            wp_enqueue_script( 'wfc-donation-recaptcha', 'https://www.google.com/recaptcha/api.js', array(), null, true );

            if( !empty( $google_site_key ) ) {
                ?>
                <div id= "pd-container-recaptcha" class="pd-container-recaptcha clearfix">
                    <div class="g-recaptcha" data-sitekey="<?php echo $google_site_key;?>" data-callback="wfcEnableDonateBtn"></div></div>
                <?php
            }
        }
    }

    /**
    * Returns style for Donate button.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    *
    * @param $configs
    *
    * @return string
    *
    * @LastUpdated : May 23, 2018
    */
    public static function btn_donate_style($configs) {
        /*
         * btn_donate button CSS
         */
        $template_btn_padding = isset($configs['template_btn_padding']) ? $configs['template_btn_padding'] : '';
        $template_btn_padding = !empty($template_btn_padding) ? 'padding:'.$template_btn_padding.'px;' : '';
        $template_btn_font_size = isset($configs['template_btn_font_size']) ? $configs['template_btn_font_size'] : '';
        $template_btn_font_size = !empty($template_btn_font_size) ? 'font-size:'.$template_btn_font_size.'px;' : '';
        $template_btn_width = isset($configs['template_btn_width']) ? $configs['template_btn_width'] : '';
        $template_btn_width = !empty($template_btn_width) ? 'width:'.$template_btn_width.'px;' : '';
        $template_btn_font_color = isset($configs['template_btn_font_color']) ? $configs['template_btn_font_color'] : '';
        $template_btn_font_color = !empty($template_btn_font_color) ? 'color:'.$template_btn_font_color.';' : '';
        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '';
        $template_color = !empty($template_color) ? 'background-color:'.$template_color.';' : '';

        return $template_btn_padding.$template_btn_font_size.$template_btn_width.$template_btn_font_color.$template_color;
    }

    /**
    * Returns and renders submit button.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    * @since  1.0.0
    *
    * @param $configs
    *
    * @LastUpdated : May 2, 2018
    */
    public static function render_submitbtn($configs){
        $formstyle_config = isset($configs['formstyle_config']) ? $configs['formstyle_config'] : array();
        $donation_form_btn_class = isset($formstyle_config['donation_form_btn_class']) ? $formstyle_config['donation_form_btn_class'] : '';
        $donation_form_btn_caption = isset($formstyle_config['donation_form_btn_caption']) ? $formstyle_config['donation_form_btn_caption'] : 'submit';

        /*
        * btn_donate button CSS
        */
        $template_btn_padding = isset($configs['template_btn_padding']) ? $configs['template_btn_padding'] : '';
        $template_btn_padding = !empty($template_btn_padding) ? 'padding:'.$template_btn_padding.'px;' : '';
        $template_btn_font_size = isset($configs['template_btn_font_size']) ? $configs['template_btn_font_size'] : '';
        $template_btn_font_size = !empty($template_btn_font_size) ? 'font-size:'.$template_btn_font_size.'px;' : '';
        $template_btn_width = isset($configs['template_btn_width']) ? $configs['template_btn_width'] : '';
        $template_btn_width = !empty($template_btn_width) ? 'width:'.$template_btn_width.'px;' : '';
        $template_btn_font_color = isset($configs['template_btn_font_color']) ? $configs['template_btn_font_color'] : '';
        $template_btn_font_color = !empty($template_btn_font_color) ? 'color:'.$template_btn_font_color.';' : '';
        $template_color = isset($configs['template_color']) ? $configs['template_color'] : '';
        $template_color = !empty($template_color) ? 'background-color:'.$template_color.';' : '';
        $btn_donate_style = $template_btn_padding.$template_btn_font_size.$template_btn_width.$template_btn_font_color.$template_color;
        
        /*
        * call Saiyan_Toolkit
        */
        // Saiyan_Toolkit::get_instance()->load_toolkits(array(
        //     'tinter', 'spinner'
        // ));

        /*
        * fetch doantion settings
        */
        $wfc_don_settings = isset($configs['settings']) ? $configs['settings'] : array();
        $loading_enable = isset($wfc_don_settings->loading_enable) ? $wfc_don_settings->loading_enable : 'true';
        $loading_spinner = isset($wfc_don_settings->loading_spinner) ? $wfc_don_settings->loading_spinner : 'tail';
        $loading_tinter_color = isset($wfc_don_settings->loading_tinter_color) ? $wfc_don_settings->loading_tinter_color : '#000000';
        ?>
        <div class="wfc_donation-submitbtn-container">
            <input type="submit" id="pdwp-btn" name="pdwp-btn" class="<?php echo $donation_form_btn_class;?>" value="<?php echo $donation_form_btn_caption;?>" style="<?php echo self::btn_donate_style($configs);?>"/>
            <input type="submit" id="pdwp-btn-ezidebit" name="pdwp-btn" style="display: none;"/>
        </div>
        <?php if ($loading_enable === 'true') : ?>
        <script>
            jQuery(document).ready(function($){
                $('#pdwp-form-donate').submit(function(e) {
                   SaiyanToolkit.spinner.show(null, '<?php echo $loading_spinner; ?>');
                   SaiyanToolkit.tinter.show('<?php echo $loading_tinter_color; ?>');
                });
            });
        </script>
    <?php endif; ?>
        <?php
    }

    /**
     * This will return valid expiry year.
     *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     * @return array
     * @since    3.0.0
     *
     * @LastUpdated  May 3, 2018
     */
    public static function _get_expiry_year() {
        $i = date('Y');
        $j = $i+11;
        $years = array();
        for ($i; $i <= $j; $i++) {
            $years[$i] = $i;
        }
        return $years;
    }

    /**
     * This will return a list of month
     *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     *
     * @return array
     * @since    3.0.0
     *
     * @LastUpdated  May 3, 2018
     */
    public static function _get_expiry_month() {
        for( $i = 1; $i <= 12; $i++ ) {
            $key = sprintf('%02d', $i);
            $month[$key] = $key;
        }
            return $month;
    }

    /**
     * Returns and render hidden fields for system campaign form.
     *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     * @since  1.0.0
     *
     * @param $configs
     *
     * @LastUpdated  April 30, 2018
     */
    public static function system_hidden_fields($configs){
        $country = WFC_DonationFormSettings::getCountryList();

        $data = isset($configs['hidden']) ? $configs['hidden'] : array();
        
        $form_cache = isset($configs['cache']) ? $configs['cache'] : array();

        $donation_sfinstance = isset($data['sfinstance']) ? $data['sfinstance'] : '';

        $field_type = isset($data['field_type']) ? $data['field_type'] : 'hidden';
        $nonce = isset($data['nonce']) ? $data['nonce'] : '';
        $donation_campaign = isset($data['donation_campaign']) ? $data['donation_campaign'] : '';
        $donation_formtitle = isset($data['donation_formtitle']) ? $data['donation_formtitle'] : '';
        $donation_formfilter = isset($data['donation_formfilter']) ? $data['donation_formfilter'] : '';
        $currency = isset($data['currency']) ? $data['currency'] : '';
        $campaignSourceMode = (isset($_GET['campaign_source']) && !empty($_GET['campaign_source'])) ? 'CampaignSourceMode' : '';
        $campaignId = isset($data['campaignId']) ? $data['campaignId'] : '';
        $gauId = isset($data['gauId']) ? $data['gauId'] : '';
        $campaignSessionId = isset($data['campaignSessionId']) ? $data['campaignSessionId'] : '';
        $p = isset($data['p']) ? $data['p'] : '';
        $unique_token = isset($data['unique_token']) ? $data['unique_token'] : md5( uniqid( "", true ) );
        $thankyou_url = isset($data['thankyou_url']) ? $data['thankyou_url'] : '';

        $enable_validation = isset($data['enable_validation']) ? $data['enable_validation'] : '';
        $google_api_key = isset($data['google_api_key']) ? $data['google_api_key'] : '';
        $google_prioritized_country = isset($data['google_prioritized_country']) ? $data['google_prioritized_country'] : '';
        $google_prioritized_country_code = '';
        if(!empty($google_prioritized_country)){
            foreach ($country as $key => $value){
                if (isset($value['title']) && $google_prioritized_country == $value['title']){
                    $google_prioritized_country_code = isset($value['value']) ? $value['value'] : '';
                }
            }
        }


        $harmony_rapid_displaymode = isset($data['harmony_rapid_displaymode']) ? $data['harmony_rapid_displaymode'] : 'detailcardmode';
        $harmony_overridablefields = isset($data['harmony_overridablefields']) ? $data['harmony_overridablefields'] : '';
        $harmony_rapid_sourceoftruth = isset($data['harmony_rapid_sourceoftruth']) ? $data['harmony_rapid_sourceoftruth'] : 'AUPAF';
        $harmony_rapid_username = isset($data['harmony_rapid_username']) ? $data['harmony_rapid_username'] : '';
        $harmony_rapid_password = isset($data['harmony_rapid_password']) ? $data['harmony_rapid_password'] : '';
        $donor_DPID = isset($form_cache['donor_DPID']) ? $form_cache['donor_DPID'] : '';
        $donor_barcode = isset($form_cache['donor_barcode']) ? $form_cache['donor_barcode'] : '';
        $donor_latitude = isset($form_cache['donor_latitude']) ? $form_cache['donor_latitude'] : '';
        $donor_longitude = isset($form_cache['donor_longitude']) ? $form_cache['donor_longitude'] : '';
 
        $donor_dialcode = isset($form_cache['donor_dialcode']) ? $form_cache['donor_dialcode'] : '';

        $spinnerstyle = isset($data['spinnerstyle']) ? $data['spinnerstyle'] : '';
        $template_color = isset($data['template_color']) ? $data['template_color'] : '';
        
        $wfc_cartdonationtypes = isset($data['wfc_cartdonationtypes']) ? $data['wfc_cartdonationtypes'] : '';
        $wfc_carttype = isset($data['wfc_carttype']) ? $data['wfc_carttype'] : 'not_cart';
        $pdwp_sub_amt = isset($data['pdwp_sub_amt']) ? $data['pdwp_sub_amt'] : '';
        ?>
        <div id="system-generated-fields" class="system-generated-fields">
            <input type="<?php echo $field_type; ?>" name="nonce" value="<?php echo $nonce; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="donation_sfinstance" id="donation_sfinstance" value="<?php echo $donation_sfinstance; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="donation_campaign" value="<?php echo $donation_campaign; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="donation_formtitle" value="<?php echo $donation_formtitle; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="donation_formfilter" value="<?php echo $donation_formfilter; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="currency" value="<?php echo $currency; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="creditcard_type" value="" id="creditcard_type" readonly/>
            <input type="<?php echo $field_type; ?>" name="campaign_sourcemode" value="<?php echo $campaignSourceMode; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="campaignId" value="<?php echo $campaignId; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="gauId" value="<?php echo $gauId; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="campaignSessionId" value="<?php echo $campaignSessionId; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="action" value="process_donate" readonly/>
            <input type="<?php echo $field_type; ?>" name="gatewayid" value="" readonly/>
            <input type="<?php echo $field_type; ?>" name="p" value="<?php echo $p; ?>" id="redirectPostId" readonly/>
            <input type="<?php echo $field_type; ?>" name="unique_token" value="<?php echo $unique_token; ?>" readonly/>
            <input type="<?php echo $field_type; ?>" name="thankyou_url" value="<?php echo $thankyou_url; ?>" id="thankyou_url" readonly/>

            <input type="<?php echo $field_type; ?>" name="enable_validation" value="<?php echo $enable_validation; ?>" id="enable_validation" readonly/>
            <input type="<?php echo $field_type; ?>" name="google_api_key" value="<?php echo $google_api_key; ?>" id="google_api_key" readonly/>
            <input type="<?php echo $field_type; ?>" name="google_prioritized_country" value="<?php echo $google_prioritized_country; ?>" id="google_prioritized_country" readonly/>
            <input type="<?php echo $field_type; ?>" name="google_prioritized_country_code" value="<?php echo $google_prioritized_country_code; ?>" id="google_prioritized_country_code" readonly/>

            <input type="<?php echo $field_type; ?>" name="harmony_rapid_displaymode" value="<?php echo $harmony_rapid_displaymode; ?>" id="harmony_rapid_displaymode" readonly/>
            <input type="<?php echo $field_type; ?>" name="harmony_overridablefields" value="<?php echo $harmony_overridablefields; ?>" id="harmony_overridablefields" readonly/>
            <input type="<?php echo $field_type; ?>" name="harmony_rapid_sourceoftruth" value="<?php echo $harmony_rapid_sourceoftruth; ?>" id="harmony_rapid_sourceoftruth" readonly/>
            <input type="<?php echo $field_type; ?>" name="harmony_rapid_username" value="<?php echo $harmony_rapid_username; ?>" id="harmony_rapid_username" readonly/>
            <input type="<?php echo $field_type; ?>" name="harmony_rapid_password" value="<?php echo $harmony_rapid_password; ?>" id="harmony_rapid_password" readonly/>
            <input type="<?php echo $field_type; ?>" name="donor_DPID" value="<?php echo $donor_DPID;?>" id="donor_DPID" readonly/>
            <input type="<?php echo $field_type; ?>" name="donor_barcode" value="<?php echo $donor_barcode;?>" id="donor_barcode" readonly/>
            <input type="<?php echo $field_type; ?>" name="donor_latitude" value="<?php echo $donor_latitude;?>" id="donor_latitude" readonly/>
            <input type="<?php echo $field_type; ?>" name="donor_longitude" value="<?php echo $donor_longitude;?>" id="donor_longitude" readonly/>

            <input type="<?php echo $field_type; ?>" name="donor_dialcode" value="<?php echo $donor_dialcode;?>" id="donor_dialcode" readonly/>

            <input type="<?php echo $field_type; ?>" name="spinnerstyle" value="<?php echo $spinnerstyle; ?>" id="spinnerstyle" readonly/>
            <input type="<?php echo $field_type; ?>" name="template_color" value="<?php echo $template_color; ?>" id="template_color" readonly/>
            <input type="<?php echo $field_type; ?>" name="wfc_cartdonationtypes" value="<?php echo $wfc_cartdonationtypes; ?>" id="wfc_cartdonationtypes" readonly/>
            <input type="<?php echo $field_type; ?>" name="wfc_carttype" value='<?php echo $wfc_carttype; ?>' id="wfc_carttype" readonly/>
            <input type="<?php echo $field_type; ?>" name="pdwp_sub_amt" value='<?php echo $pdwp_sub_amt; ?>' id="pdwp_sub_amt" readonly/>
            <input type="<?php echo $field_type; ?>" name="donation_version" value='v5' id="donation_version" readonly/>
            <input type="<?php echo $field_type; ?>" name="advance_cart_ver" value='2.0' id="advance_cart_ver" readonly/>
            <input type="<?php echo $field_type; ?>" name="field_error_notice_type" value='fieldglow,snackbar' id="field_error_notice_type" readonly/>
        </div>
        <?php
    }

    /**
    * Fetches Form Error.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    *
    * @param $configs
    *
    * @LastUpdated  April 30, 2018
    */
    public static function get_error($configs) {
        $campaignSessionId = isset($_GET['e']) ? $_GET['e'] : '';
        $form_error = get_transient('pdwp_errors' . $campaignSessionId);
        $form_error = isset($form_error['form_error']) ? $form_error['form_error'] : array();

        $failuremessage_config = isset($configs['failuremessage_config']) ? $configs['failuremessage_config'] : array();
        $passed_form_validation  = isset($_GET['piv']) ? $_GET['piv'] : '';

        $donation_form_error_message_behavior =
        isset($failuremessage_config['donation_form_error_message_behavior']) ?
        $failuremessage_config['donation_form_error_message_behavior'] : 'standard';

        $donation_form_override_failure_message =
        isset($failuremessage_config['donation_form_override_failure_message']) ?
        $failuremessage_config['donation_form_override_failure_message'] : 'false';

        $donation_form_override_message_type =
        isset($failuremessage_config['donation_form_override_message_type']) ?
        $failuremessage_config['donation_form_override_message_type'] : 'plain_text';

        $donation_form_override_message =
        isset($failuremessage_config['donation_form_override_message']) ?
        $failuremessage_config['donation_form_override_message'] :'There has been an issue with processing your donation. Please contact our administrator.';


        if(!empty($form_error) && !empty($campaignSessionId)){
            if($passed_form_validation == 'true' && $donation_form_override_failure_message == 'true') {
                if($donation_form_override_message_type == 'plain_text') {
                    if($donation_form_error_message_behavior == 'popup'){
                        ?>
                        <div id="alert-danger-popup" class="alert-danger-popup-show">
                            <div id="alert-danger-popup-tint" style=""></div>
                            <div id="alert-danger" class="alert alert-danger alert-danger-popup" style="">
                                <div id="alert-danger-popup-header" style="">
                                    <span id="alert-danger-popup-close-btn" style="">X</span>
                                </div>
                                <div id="alert-danger-popup-body" style="">
                                <?php echo $donation_form_override_message;?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }else {
                        ?>
                        <div id="alert-danger" class="alert alert-danger"><?php echo $donation_form_override_message;?></div>
                        <?php
                    }

                }else{
                    if($donation_form_error_message_behavior == 'popup'){
                        ?>

                        <div id="alert-danger-popup" class="alert-danger-popup-show">
                            <div id="alert-danger-popup-tint" style=""></div>
                            <div id="alert-danger" class="alert alert-danger alert-danger-popup" style="">
                                <div id="alert-danger-popup-header" style="">
                                    <span id="alert-danger-popup-close-btn" style="">X</span>
                                </div>
                                <div id="alert-danger-popup-body" style="">
                                <?php echo html_entity_decode($donation_form_override_message); ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }else {
                        echo html_entity_decode($donation_form_override_message);
                    }
                }
            }else{
                if($donation_form_error_message_behavior == 'popup'){
                    ?>
                    <div id="alert-danger-popup" class="alert-danger-popup-show">
                        <div id="alert-danger-popup-tint" style=""></div>
                        <div id="alert-danger" class="alert alert-danger alert-danger-popup" style="">
                            <div id="alert-danger-popup-header" style="">
                                <span id="alert-danger-popup-close-btn" style="">X</span>
                            </div>
                            <div id="alert-danger-popup-body" style="">
                            <?php foreach ($form_error as $key => $value):?>
                            <?php  print_r($value); ?>
                            <?php endforeach;?>
                            </div>
                        </div>
                    </div>
                    <?php
                }else {
                    ?>
                    <div id="alert-danger" class="alert alert-danger">
                        <?php foreach ($form_error as $key => $value):?>
                        <?php  print_r($value); ?>
                        <?php endforeach;?>
                    </div>
                    <?php
                }

            }
        }else {
            ?>
            <div id="alert-danger-popup" class="alert-danger-popup-hide">
                <div id="alert-danger-popup-tint" style=""></div>
                <div class="alert alert-danger alert-danger-popup" style="">
                    <div id="alert-danger-popup-header" style="">
                        <span id="alert-danger-popup-close-btn" style="">X</span>
                    </div>
                    <div id="alert-danger-popup-body" style="">
                    </div>
                </div>
            </div>
            <div id="alert-danger" class="alert alert-danger" style="display: none">
            </div>
            <?php
        }
    }

    /**
    * Sets error to Donation Form.
    *
    * @author Junjie Canonio <junjie@alphasys.com.au>
    *
    * @param $errArr
    * @param $post
    *
    * @LastUpdated April 30, 2018
    */
    public static function set_error($errArr, $post) {
        /*
        * for form cache
        */
        $_SESSION['pdwp_rand'] = $post['campaignSessionId'];

        $error = new WP_Error();
        foreach ( $errArr as $err ) {
            $error->add( 'form_error', __( "<div><span style='color:red;'>*</span>{$err}</div>") );
        }
        set_transient( 'pdwp_errors' . $post['campaignSessionId'], $error->errors, HOUR_IN_SECONDS );
        set_transient( 'pdwp_form_cache_' . $_SESSION['pdwp_rand'], $post, HOUR_IN_SECONDS );
    }
}