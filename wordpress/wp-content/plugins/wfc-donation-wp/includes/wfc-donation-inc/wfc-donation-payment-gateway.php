<?php
/**
 * Class WFC_DonationPaymentGateway
 * This class handles out the payment gateways use on Webforce Connect Donation.
 *
 * @author Junjie Canonio <junjie@alphasys.com.au>
 * @since  1.0.0
 */
class WFC_DonationPaymentGateway {
	/**
	 * Loads all payment gateway modules.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param  array $gateways An Array of payment Gateways.
	 * @param $mainclass
	 *
	 * @LastUpdated  May 2, 2018
	 */
    
    public function wfc_donation_load_gatewaymodules($gateways, $mainclass) {

        $path = trailingslashit( plugin_dir_path( __FILE__ ) ) . '../../payment-gateway/';
        $payments = scandir($path);

        foreach ( $gateways as $gateway ) {
            if( !is_file( $gateway ) && !is_dir( $gateway ) ) {
                require_once( $path . $gateway . '/index.php' );
                $gate = $this->wfc_donation_get_gatewaymodule( $gateway );
                if( !empty( $gate ) ) {
                    $gate->loadWpCore( $mainclass );
                }
            }
        }
    }
	
	/**
	 * Fetches all Payment Gateway module.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $module
	 *
	 * @return mixed
	 *
	 * @LastUpdated  May 2, 2018
	 */
    public function wfc_donation_get_gatewaymodule( $module ) {
        $gateway = ucfirst( $module );
        if( class_exists( $gateway ) )
            return new $gateway;
    }
	
	/**
	 * Loads and Enqueues all payment gateway module scripts.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param int $post_id
	 *
	 * @LastUpdated  April 28, 2020
	 */
    public static function wfc_donation_load_gatewayscripts($post_id,$sfinstance){
        $paypal_redirectID = '';

        $donation_v5_form_payment_gateways = get_post_meta( $post_id, 'donation_v5_form_payment_gateways_'.$sfinstance, true );
        $donation_v5_form_payment_gateways = json_decode($donation_v5_form_payment_gateways,true);
        
        $donation_cc_country = get_post_meta( $post_id, 'donation_cc_country', true );
        $currency = $donation_cc_country;
        $donation_cc_currency = get_post_meta( $post_id, 'donation_cc_currency', true );
        $currency = empty($currency) ? $donation_cc_currency : $currency;
        $donation_cc_currency_symbol = get_post_meta( $post_id, 'donation_cc_currency_symbol', true );
        $currency = empty($currency) ? $donation_cc_currency_symbol : $currency;
        $currency = '&currency='.$currency;

        $gateway_lib_arr = array(); 
        if(is_array($donation_v5_form_payment_gateways)) {
            foreach ($donation_v5_form_payment_gateways as $payment_gateways_key => $payment_gateways_value) {
                foreach ($payment_gateways_value as $key => $value) {
                    $gateway = get_option($key);
                    if (isset($gateway['wfc_donation_paypayredirect']) && 
                        $gateway['wfc_donation_paypayredirect'] == 'true') {

                        $paypal_redirectID = 
                        isset($gateway['wfc_donation_paypalclientid']) ? 
                        $gateway['wfc_donation_paypalclientid'] : '';
                    }
                    //print_r($gateway);
                    if($value['value'] == 'true'){
	                    $key_option = str_replace($sfinstance.'_',"",$key);
                        $payment_gateway = explode("_", $key_option);
                        $gateway_lib_arr[] = isset($payment_gateway[0]) ? $payment_gateway[0] : '';
                    }
                }
            }
        }
        $gateway_lib_arr = array_unique($gateway_lib_arr);

        if(in_array('eway', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-eway_encrypt_js');
            wp_enqueue_script('webforce-connect-donation-eway_js');
        }

        if(in_array('ezidebit', $gateway_lib_arr)){

	        wp_enqueue_script('wfc-don-wfc_don_cryptoJsAes-aes-js');
	        wp_enqueue_script('wfc-don-wfc_don_cryptoJsAes-js');
            wp_enqueue_script('webforce-connect-donation-ezidebit_lib_js');
            wp_enqueue_script('webforce-connect-donation-ezidebit_js');

        }

        if(in_array('ech', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-paydock_token_js');
            wp_enqueue_script('webforce-connect-donation-paydock_js');            
        }

        if(in_array('stripe', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-stripe_library_js');
            wp_enqueue_script('webforce-connect-donation-stripe_js');            
        }

        if(in_array('nab', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-nab_js');
        }

        if(in_array('paypal', $gateway_lib_arr)){
            //wp_enqueue_script('webforce-connect-donation-paypal_redirect_js');
            //wp_enqueue_script('webforce-connect-donation-paypal_spb_js');

            /** Smart Payment Buttons features */
            wp_enqueue_script( 
                'webforce-connect-donation-paypal_spb_js',
                'https://www.paypal.com/sdk/js?client-id='.$paypal_redirectID.'&commit=true&intent=capture'.$currency,
                array( 'jquery' ), 
                null, 
                false 
            );

            wp_enqueue_script('webforce-connect-donation-paypal_js');
        }

        if(in_array('pin', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-pin_library_js');
            wp_enqueue_script('webforce-connect-donation-pinpayments_js');
        }

        if(in_array('securepay', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-securepay_js');
        }

        if(in_array('paymentfile', $gateway_lib_arr)){
            wp_enqueue_script('webforce-connect-donation-paymentfile_js');
        }

        // WESTPAC PayWay => includes js file
        if( in_array('westpac', $gateway_lib_arr) ) {
            wp_enqueue_script('webforce-connect-donation-westpac_js');
            wp_enqueue_script('webforce-connect-donation-moment_with_loacale_js');
            wp_enqueue_script('webforce-connect-donation-moment_tz_with_data_js');
        }
        
    }
	
	/**
	 * Filters and returns proper message response.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $response
	 * @param $gateway
	 *
	 * @return array
	 *
	 * @LastUpdated  May 9, 2018
	 */
    public static function wfc_gateway_parse_resp($response, $gateway){
        $error = array();
        if(strtolower($gateway) == 'eway'){
            $error[] = '<strong>Payment Gateway : </strong>eWay';
            $eway_ResponseCode = isset($response['ResponseCode']) ? $response['ResponseCode'] : '';
            $error[] = '<strong>Code : </strong>'.$eway_ResponseCode;
            $eway_ResponseMessage = isset($response['ResponseMessage']) ? $response['ResponseMessage'] : '';
            $error[] = '<strong>eWay Message : </strong>'.$eway_ResponseMessage;
            $eway_ResponseCode = (string)$eway_ResponseCode; 
            $eway_status_detail = (new self)->get_responseDetails($eway_ResponseCode); 
            $error[] = '<strong>Status Detail : </strong>'.$eway_status_detail;
            $error[] = '<strong>Message : </strong>  Please try again or contact your administrator.';
        }elseif (strtolower($gateway) == 'ezidebit'){
            $error[] = '<strong>Payment Gateway : </strong>Ezidebit';

            if(isset($response['PaymentResultText'])){
                $ezidebit_PaymentResultText = isset($response['PaymentResultText']) ? $response['PaymentResultText'] : '';
                $error[] = '<strong>Credit Card Status : </strong>'.$ezidebit_PaymentResultText;
                $ezidebit_PaymentResultCode = isset($response['PaymentResultCode']) ? $response['PaymentResultCode'] : '';
                $ezidebit_PaymentResultCode = (string)$ezidebit_PaymentResultCode; 
                $ezidebit_status_detail = (new self)->get_responseDetails($ezidebit_PaymentResultCode); 
                $error[] = '<strong>Status Detail : </strong>'.$ezidebit_status_detail; 
            }else{
                $ezidebit_PaymentResultText = isset($response['ErrorMessage']) ? $response['ErrorMessage'] : '';
                $error[] = '<strong>Credit Card Status : </strong>'.$ezidebit_PaymentResultText;
            }
            $error[] = '<strong>Message : </strong>  Please try again or contact your administrator.';

        }elseif (strtolower($gateway) == 'westpac'){
            $error[] = '<strong>Payment Gateway : </strong>westpac';

            if(isset($response['responseText'])){
                $westpac_responseText = isset($response['responseText']) ? $response['responseText'] : '';
                $error[] = '<strong>Credit Card Status : </strong>'.$westpac_responseText;
                $westpac_responseCode = isset($response['responseCode']) ? $response['responseCode'] : '';
                $westpac_responseCode = (string)$westpac_responseCode; 
                $westpac_status_detail = (new self)->get_responseDetails($westpac_responseCode); 
                
                $error[] = '<strong>Status Detail : </strong>'. $westpac_status_detail; 
            }else{
                $westpac_responseText = isset($response['ErrorMessage']) ? $response['ErrorMessage'] : '';
                $error[] = '<strong>Credit Card Status : </strong>'.$westpac_responseText;
            }
            $error[] = '<strong>Message : </strong>  Please try again or contact your administrator.';

        }else{
            $error[] = '<strong>Payment Gateway : </strong>'.$gateway;
            $error[] = '<strong>Message : </strong> Something went wrong during the donation, Please contact your administrator.';
        }

        return $error;
    }
	
	/**
	 * Returns Bank Response Codes Details by code.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $code
	 *
	 * @return mixed|string
	 *
	 * @LastUpdated  April 28, 2020
	 */
    public function get_responseDetails( $code )
    {
        $details = array(
            /*
            *  Bank Response Codes - 00 to 38
            */
            "00" => "Transaction Approved Successfully.",
            "01" => "The customer’s bank (Card Issuer) has indicated there is a problem with the card number. The customer should contact their bank or the customer should use an alternate card.",
            "02" => "The customer’s bank (Card Issuer) has indicated there is a problem with the card number. The customer should contact their bank. The customer should use an alternate card.",
            "03" => "This Error indicates that either your merchant facility is non-functional or the details entered into eWAY are incorrect.",
            "04" => "The card has been reported lost or stolen. Please have the customer use a different card, or have them contact their card issuer directly regarding the error.",
            "05" => "The '05 Do Not Honour' error is a generic bank response code that has several possible causes. However it does generally indicate a card error, not an error with your merchant facility. The '05' error indicates your bank declining the customer's card for an unspecified reason.",
            "06" => "The customer’s card issuer has declined the transaction as there is a problem with the card number. The customer should contact their card issuer and/or use an alternate card.",
            "08" => "Transaction processed successfully - identification NOT required. This code is returned by some banks in place of 00 response.",
            "09" => "The card issuer has indicated there is a problem with the card number. The customer should contact their bank and/or use an alternate credit card.",
            "10" => "Amount The transaction was successful.",
            "12" => "The bank has declined the transaction because of an invalid format or field. This indicates the card details were incorrect. Check card data entered and try again.",
            "13" => "An invalid character (e.g. $, & @) may be being passed to the gateway. Check your website’s code.",
            "14" => "The card issuing bank has declined the transaction as the credit card number is incorrectly entered, or does not exist. Check card details and try processing again.",
            "15" => "The customer’s card issuer does not exist. Check the card information and try processing the transaction again.",
            "19" => "The transaction has not been processed and the customer should attempt to process the transaction again. No further information is provided from the bank as to the reason why this was not processed.",
            "21" => "The customer’s card issuer has indicated there is a problem with the credit card number. The customer should contact their card issuer and/or use an alternate credit card.",
            "22" => "The customer’s card issuer could not be contacted during the transaction. The customer should check the card information and try processing the transaction again.",
            "23" => "An unspecified bank error has occurred. No further information is available from eWAY or the bank. The customer should attempt to process the transaction again.",
            "25" => "The customer’s card issuer does not recognise the credit card details. The customer should check the card information and try processing the transaction again.",
            "30" => "The customer’s card issuer does not recognise the transaction details being entered. This is due to a format error. The customer should check the transaction information and try processing the transaction again.",
            "31" => "The customer’s card issuer has declined the transaction as it does not allow transactions originating through mail /telephone, fax, email or Internet orders. This error is associated with customers attempting to use a Discover Card.",
            "33" => "The customer’s card issuer has declined the transaction as Card has expired or the date is incorrect. Check the expiry date in the transaction and try processing the transaction again. Sometimes, this error code will be provided if the expiry date is entered incorrectly, NOT only for expired cards.",
            "34" => "The customer’s card issuer has declined the transaction as there is a suspected fraud on this Card number.",
            "35" => "The customer’s bank card issuer has declined the transaction and requested that the customer’s card be retained as the card was reported as lost or stolen.",
            "36" => "The customer’s bank (Card Issuer) has declined the transaction and requested that the customer’s card be retained.",
            "37" => "The customer’s bank (Card Issuer) has declined the transaction and requested that your customer’s card be retained.",
            "38" => "The customer’s bank (Card Issuer) has declined the transaction as the customer has entered the incorrect PIN three times.",
            /*
            *  Bank Response Codes - 39 to 96
            */
            "39" => "Bank has declined the transaction as the Credit Card number used is not a credit account.",
            "40" => "Bank (Card Issuer) has declined the transaction as it does not allow this type of transaction.",
            "41" => "Bank (Card Issuer) has declined the transaction as the card has been reported lost.",
            "42" => "Bank (Card Issuer) has declined the transaction as the account type selected is not valid for this credit card number.",
            "43" => "Bank (Card Issuer) has declined the transaction as the card has been reported stolen.",
            "44" => "Bank (Card Issuer) has declined the transaction as the account type selected is not valid for this credit card number.",
            "51" => "Bank (Card Issuer) has declined the transaction as the credit card does not have sufficient funds.",
            "52" => "Bank (Card Issuer) has declined the transaction as the credit card number is associated to a cheque account that does not exist.",
            "53" => "Bank (Card Issuer) has declined the transaction as the credit card number is associated to a savings account that does not exist.",
            "54" => "Bank (Card Issuer) has declined the transaction as the credit card appears to have expired. The customer should check the expiry date entered and try again.",
            "55" => "Bank (Card Issuer) has declined the transaction as the customer has entered an incorrect PIN. The customer should re-enter their PIN.",
            "56" => "Bank has declined the transaction as the credit card number does not exist.",
            "57" => "Bank has declined the transaction as this credit card cannot be used for this type of transaction.",
            "58" => "Bank has declined the transaction as this credit card cannot be used for this type of transaction. This may be associated with a test credit card number. The customer should contact their bank for further info.",
            "59" => "Bank has declined this transaction as the credit card appears to be fraudulent.",
            "60" => "Bank (card issuer) has declined the transaction. The customer should contact their bank and retry the transaction.",
            "61" => "Bank has declined the transaction as it will exceed the customer’s card limit.",
            "62" => "Bank has declined the transaction as the credit card has some restrictions. The customer should contact their bank for further information.",
            "63" => "Bank has declined the transaction. The customer should use an alternate credit card and contact their card issuer if the problem persists.",
            "64" => "The customer’s card issuer has declined the transaction due to the amount attempting to be processed. The customer should check the transaction amount and try again, contacting their card issuer should the problem persist.",
            "65" => "The customer’s card issuer has declined the transaction as the customer has exceeded the withdrawal frequency limit.",
            "66" => "The customer should use an alternative credit card. Neither eWAY nor the bank can provide more details.",
            "67" => "The customer’s card issuer has declined the transaction as the card is suspected to be counterfeit. The card issuer has requested that your customer’s credit card be retained.",
            "75" => "The customer’s card issuer has declined the transaction as the customer has entered the incorrect PIN more than three times.",
            "82" => "The customer’s card issuer has declined the transaction as the CVV is incorrect. The customer should check the CVV details (the 3 numbers on the back for Visa/MC, or 4 numbers on the front for AMEX) and try again. If not successful, the customer should use an alternate credit card.",
            "90" => "The customer’s card issuer is temporarily not able to process this customer’s credit card. The customer should attempt to process this transaction again.",
            "91" => "The customer’s card issuer is unable to be contacted to authorise the transaction. The customer should attempt to process this transaction again.",
            "92" => "The customer’s card issuer cannot be found for routing. This response code is often returned when the customer is using a test credit card number. The customer should attempt to process this transaction again.",
            "93" => "The customer’s card issuer has declined the transaction and request the customer to contact their bank.",
            "94" => "The customer’s card issuer has declined the transaction as this transaction appears to be a duplicate transmission. Check this is the case and process the transaction again if needed.",
            "96" => "The customer’s card issuer was not able to process the transaction. The customer should attempt to process this transaction again.",
            /*
            *  Bank Response Codes - 17, 68, QA to QY => WESTPAC PayWay
            */
            "17" => "This indicates that the transaction was authorised and subsequently voided. Voided transactions do not appear on the customer's statement or form part of your settlement total.",
            "68" => "The card issuer (your customer's bank) did not respond in time. The credit card transaction has not been processed. You can retry the transaction after waiting for 15 minutes.",
            "QA" => "Invalid parameters passed to API, for example, missing credit card number or customer number. The status text will contain more detail about which parameters are invalid.",
            "QI" => "This status code indicates that a request message was sent to the PayWay server but no response was received within the timeout period.",
            "QQ" => "This error code indicates that the credit card details (card number, expiry date or CVN) are invalid. This could be because the card number does not meet check digit validation, an invalid expiry date was entered, or an invalid CVN was entered.",
            "QY" => "The Merchant is not enabled for the particular Card Scheme (normally returned for American Express® and Diners Club cards).
                    To register for American Express® or Diners Club, click the Register to accept Amex or Diners through PayWay link on the Merchants page. 
                    Note: There are no response codes specific to card verification number mismatches. 
                    This is because no financial institutions in Australia currently return any such information if declining a transaction.",
        );
        return isset( $details[$code] ) ? $details[$code] : 'None';
    }

}