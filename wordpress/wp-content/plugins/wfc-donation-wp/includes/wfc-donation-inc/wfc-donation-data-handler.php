<?php
/**
* Class WFC_DonationDataHandler
* This class handles out the data for use on donation templates.
*
* @author Junjie Canonio <junjie@alphasys.com.au>
* @since  1.0.0
 *
*/
class WFC_DonationDataHandler {

	public function __construct() {


	}
	
	/**
	 * Cleans data from form.
	 *
	 * This function cleans data from specific donation form. This is used to wipe clean donation form data after every
	 * donation process.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 *
	 * @param $form_data
	 *
	 * @return array
	 *
	 * @LastUpdated  April 30, 2018
	 */
	public function clean_form_data($form_data){
        $donationData = array();
        $post = stripslashes_deep( isset($form_data['donation']) ? $form_data['donation'] : array() );

        foreach ($post as $index => $data) {
            if (gettype($data) == 'string') {
                $donationData[$index] = sanitize_text_field($data);
            } elseif (is_array($data)) {
                if ($index == 'pdwp_gift') {
                    $donationData['pdwp_gift'] = ($post['pdwp_gift']['gift'] == 'true') ? true : false;
                } elseif ($index == 'donor_newsletter') {
                    $donationData['donor_newsletter'] = ($post['donor_newsletter']['dailynewsletter'] == 'true') ? true : false;
                } else {
                    $donationData[$index] = wp_unslash( $data );
                }
            } else {
                $donationData[$index] = $data;
            }
        }
        return $donationData;
	}
	
	/**
	 * Validate data from form.
	 *
	 * This function validates all data inputted to the donation form.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 *
	 * @param $form_data
	 *
	 * @return array
	 *
	 * @LastUpdated  April 30, 2018
	 */
	public function validate_form_data($form_data){	
        $field_error = array();
        $focuselemname = '';

		$donation_sfinstance = isset($form_data['donation_sfinstance']) ? $form_data['donation_sfinstance'] : '';

        $settings = get_option( 'pronto_donation_settings', 0 );
        $pdwp_ASSFAPI_gau = get_option( 'pdwp_ASSFAPI/gau_'.$donation_sfinstance, 0 );


        /*required donor fields*/
        $required_fields = array();
        $form_id = isset($form_data['donation_campaign']) ? $form_data['donation_campaign'] : '';
        $donation_v5_form_user_fields = get_post_meta( $form_id, 'donation_v5_form_user_fields', true );
        $donation_v5_form_user_fields = json_decode($donation_v5_form_user_fields,true);
        foreach ($donation_v5_form_user_fields as $key => $value_fields) {
            foreach ($value_fields as $key => $value) {
                $field_key = isset($value['values']['field_key']) ? $value['values']['field_key'] : '';
                $field_label = isset($value['values']['field_label']) ? $value['values']['field_label'] : '';
                $field_option = isset($value['values']['field_option']) ? $value['values']['field_option'] : '';
                if($field_option == 'required'){
                    $required_fields[$field_key] = $field_label;
                }
            }
        }
        foreach ($required_fields as $key => $value) {
            if($key == 'donor_company' && (isset($form_data['donor_type']) && $form_data['donor_type'] == 'individual')){           
                /* bypass validation for donor_company*/
            }else{
                if(empty($form_data[$key])){
                    $field_error[] = $value." field is required.";
                    if (empty($focuselemname)) {
                        $focuselemname = $key;
                    }
                }
            }
        }


        /* gau validation*/
        if (isset($form_data['gauId']) && !empty($form_data['gauId']) && !isset($form_data['cartItems'])) {
            $gauId = isset($form_data['gauId']) ? $form_data['gauId'] : '';
            if(!isset($pdwp_ASSFAPI_gau[$gauId])){
                $field_error[] = "GAU set for this form is not available or inactive.";
            }
        }


        /*DPID validation*/
        if (isset($settings->enable_validation) && $settings->enable_validation != 'true') {
            if( (isset($settings->harmony_rapid_username) && !empty($settings->harmony_rapid_username)) && 
                (isset($settings->harmony_rapid_password) && !empty($settings->harmony_rapid_password)) ){
                $donor_DPID = isset( $form_data['donor_DPID'] ) ? $form_data['donor_DPID'] : '';
                if(empty($donor_DPID)) {
                    $field_error[] = "Make sure to select/click from the suggested addresses displayed below the address field.";
                    if (empty($focuselemname)) {
                        $focuselemname = 'donor_address';
                    }
                }
            }
        }

         
        /*bank payment validation*/
        if (isset( $form_data['payment_type'] ) && $form_data['payment_type'] == 'bank') {
            if(isset( $form_data['pdwp_donation_type'] ) && $form_data['pdwp_donation_type'] == 'one-off') {
                $field_error[] = "One-off donation type does not accept BANK payment.";
                if (empty($focuselemname)) {
                    $focuselemname = 'pdwp_donation_type';
                }
            }

            if(isset($form_data['wfc_carttype']) && $form_data['wfc_carttype'] == 'advance_cart') {
                $field_error[] = "Bank payment is not allowed for multiple donation.";
            }
        }


        /*Paypal Button payment validation*/
        if (isset( $form_data['paypal_redirect'] ) && $form_data['paypal_redirect'] == 'true') {
            if (isset( $form_data['pdwp_donation_type'] ) && $form_data['pdwp_donation_type'] != 'one-off') {
                $field_error[] = "PayPal does not accept recurring donation type.";
                if (empty($focuselemname)) {
                    $focuselemname = 'pdwp_donation_type';
                }
            }
        }
        

        /*Multi cart validation*/
        if(isset($form_data['wfc_carttype']) && $form_data['wfc_carttype'] == 'advance_cart') {
            $wfc_cartdonationtypes = isset($form_data['wfc_cartdonationtypes']) ? $form_data['wfc_cartdonationtypes'] : ''; 
            if (strpos($wfc_cartdonationtypes, 'daily') !== false ||
                strpos($wfc_cartdonationtypes, 'weekly') !== false ||
                strpos($wfc_cartdonationtypes, 'fortnightly') !== false ||
                strpos($wfc_cartdonationtypes, 'monthly') !== false ||
                strpos($wfc_cartdonationtypes, 'quarterly') !== false ||
                strpos($wfc_cartdonationtypes, 'yearly') !== false) {

                if( isset( $form_data['gatewayid'] ) && !empty( $form_data['gatewayid'] ) ) {
                    $gateway = explode( '_' , $form_data['gatewayid'] );
                    $gateway_name = isset($gateway[0]) ? $gateway[0] : '';
                    
                    $gateway_key	= count( $gateway ) - 2;
                    $gateway_name 	= isset( $gateway[ $gateway_key ] ) ? $gateway[ $gateway_key ] : $gateway_name;


                    if ( strtolower($gateway_name) == 'paypal') {
                        $field_error[] = "Recurring payment is not supported for Paypal.";
                        if (empty($focuselemname)) {
                            $focuselemname = 'pdwp_donation_type';
                        }
                    }
                }  
            }    

            if(isset($form_data['cartItems']) && empty($form_data['cartItems'])) {
                $field_error[] = "There are no items on cart.";
            }

            if(!isset($form_data['cartItems'])) {
                $field_error[] = "Cart is missing.";
            }

            if(isset($form_data['cartItems']) && !empty($form_data['cartItems'])) {
                if(is_array($form_data['cartItems'])) {
                    foreach($form_data['cartItems'] as $key => $value) {
                        $cartitem_number = $key+1;
                        if(!isset($value['gau_id'])) {
                           $field_error[] = "Item ".$cartitem_number." has no GAU.";
                        }

                        if(isset($value['gau_id']) && empty($value['gau_id'])) {
                           $field_error[] = "Item ".$cartitem_number." GAU is not set.";
                        }

                        if(!isset($value['amount'])) {
                           $field_error[] = "Item ".$cartitem_number." has no amount.";
                        }

                        if(isset($value['amount']) && empty($value['amount'])) {
                           $field_error[] = "Item ".$cartitem_number." amount is not set.";
                        }

                        if(!isset($value['wfc_donation_type'])) {
                           $field_error[] = "Item ".$cartitem_number." has no Donation Type.";
                        }

                        if(isset($value['wfc_donation_type']) && empty($value['wfc_donation_type'])) {
                           $field_error[] = "Item ".$cartitem_number." Donation Type is not set.";
                        }
                    }
                }else{
                    $field_error[] = "Cart must contain be an array items.";
                }
            }
        }      
         
        /*Google Captcha Validation*/
        $enable_recaptcha = isset($settings->enable_recaptcha) ? $settings->enable_recaptcha : '';
        if ( $enable_recaptcha == 'true') {
            $captchaResponce = isset($form_data['g-recaptcha-response']) ? $form_data['g-recaptcha-response'] : '';
            if (empty($captchaResponce) || $captchaResponce = '') {
                $field_error[] = "Captcha is required.";
                if (empty($focuselemname)) {
                    $focuselemname = 'g-recaptcha-response';
                }
            }
        }

        /*amt validation*/
        $amt = isset( $form_data['pdwp_sub_amt'] ) ? $form_data['pdwp_sub_amt'] : 0;
        $min_amt = get_post_meta( $form_data['donation_campaign'], 'donation_min_amount', true );
        $min_amt_amt_format = number_format( $min_amt, 2, ".", "" );
        $amt_format = number_format( $amt, 2, ".", "" );
        if( (int)$amt < (int)$min_amt || (int)$amt > 10000 ) {

            $field_error[] = "Donation amount should be greater than or equal {$min_amt_amt_format} and less than or equal 10,000.00";
            if (empty($focuselemname)) {
                $focuselemname = 'pdwp_custom_amount';
            }
        }

		/*amt validation*/
		if ( strpos( $min_amt_amt_format, "." ) !== false ) {
			if (preg_match('/^[0-9]+\.[0-9]{2}$/', $min_amt_amt_format)) {}
			else {
				$field_error[] = "Donation amount ".$min_amt_amt_format." is not valid.";
				if (empty($focuselemname)) {
					$focuselemname = 'pdwp_custom_amount';
				}
			}
		}

        /*paypal module validation*/
        $paymentObject_type = isset($form_data['paymentObject']['type']) ? $form_data['paymentObject']['type'] : '';
        if (strtolower($paymentObject_type) == 'paypal' && $form_data['pdwp_donation_type'] != 'one-off') {
            $donation_type = isset($form_data['pdwp_donation_type']) ? $form_data['pdwp_donation_type'] : 'Recurring donation';
            $donation_type = ucfirst($donation_type);
            $field_error[] = "{$donation_type} is not available for Paypal Payment Gateway.";
            if (empty($focuselemname)) {
                $focuselemname = 'pdwp_donation_type';
            }
        }


        /* call all custom validation filter*/
        if (has_filter('wfc_donation_form_validation') == true) {
            $custom_validation_field_error = apply_filters('wfc_donation_form_validation', $form_data);

            $custom_validation_field_error = 
            isset($custom_validation_field_error['errors']) ? $custom_validation_field_error['errors'] : array();

            if(is_array($custom_validation_field_error) && !empty($custom_validation_field_error)){
                foreach ($custom_validation_field_error as $key => $value_field_error) {
                    $field_error[] = $value_field_error;
                }            
            }
            
            if (empty($focuselemname)) {
                $focuselemname = 
                isset($custom_validation_field_error['focuselemname']) ? 
                $custom_validation_field_error['focuselemname'] : array();
            }
        }

        return array(
            'errors' => $field_error,
            'focuselemname' => $focuselemname,
        );
	}
	
	/**
	 * Returns all configurations regarding with amount.
	 *
	 * This function returns all the amount-related configurations from a specific Donation Form Settings.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 *
	 * @param $post_id
	 *
	 * @return array
	 *
	 * @LastUpdated  May 11, 2018
	 */
    public static function get_amount_config($post_id){
	    $donation_form_accept_cents = get_post_meta( $post_id, 'donation_form_accept_cents', true );
        $donation_target = get_post_meta( $post_id, 'donation_target', true );
        $donation_min_amount = get_post_meta( $post_id, 'donation_min_amount', true );
        $donation_form_custom_amount = get_post_meta( $post_id, 'donation_form_custom_amount', true );
        $donation_form_custom_amount_default = get_post_meta( $post_id, 'donation_form_custom_amount_default', true );
        $donation_form_separate_amtfield_from_amtbtns = get_post_meta( $post_id, 'donation_form_separate_amtfield_from_amtbtns', true );
        $donation_v5_form_amount_levels = get_post_meta( $post_id, 'donation_v5_form_amount_levels', true );
        if(!empty($donation_v5_form_amount_levels)) {
            $donation_v5_form_amount_levels = json_decode($donation_v5_form_amount_levels,true);
        }
	    $donation_v5_form_amount_display_type = get_post_meta( $post_id, 'donation_v5_form_amount_display_type', true );
	    $donation_v5_form_amount_card_image_position = get_post_meta( $post_id, 'donation_v5_form_amount_card_image_position', true );
        $donation_cc_country = get_post_meta( $post_id, 'donation_cc_country', true );
        $donation_cc_currency = get_post_meta( $post_id, 'donation_cc_currency', true );
        $donation_cc_currency_symbol = get_post_meta( $post_id, 'donation_cc_currency_symbol', true );
        $WFC_DonationForms = new WFC_DonationForms();
        $currency_symbol = $WFC_DonationForms->wfc_get_currencySymbol($donation_cc_currency_symbol );
        $amount_config = array(
	        'donation_form_accept_cents' => $donation_form_accept_cents,
            'donation_target' => $donation_target,
            'donation_min_amount' => $donation_min_amount,
            'donation_form_custom_amount' => $donation_form_custom_amount,
            'donation_form_custom_amount_default' => $donation_form_custom_amount_default,
            'donation_form_separate_amtfield_from_amtbtns' => $donation_form_separate_amtfield_from_amtbtns,
            'donation_v5_form_amount_levels' => $donation_v5_form_amount_levels,
	        'donation_v5_form_amount_display_type' => $donation_v5_form_amount_display_type,
	        'donation_v5_form_amount_card_image_position' => $donation_v5_form_amount_card_image_position,
            'donation_cc_country' => $donation_cc_country,
            'donation_cc_currency' => $donation_cc_currency,
            'donation_cc_currency_symbol' => $donation_cc_currency_symbol,
            'currency_symbol' =>  $currency_symbol, 
        );
        return $amount_config;
    }
	
	/**
	 * Returns all configurations regarding with donation types.
	 *
	 * This function returns all the Donation Type-related configurations from a specific Donation Form Settings.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 *
	 * @param $post_id
	 *
	 * @return array
	 *
	 * @LastUpdated  May 16, 2018
	 */
    public static function get_donationtypes($post_id){ 
        $wfc_don_settings = get_option( 'pronto_donation_settings', 0 );
        $frequencies = isset($wfc_don_settings->frequencies) ? $wfc_don_settings->frequencies : '';
        $frequencies = json_decode($frequencies,true);

        $frequencies_arr = array();
        if (is_array($frequencies)) {
            foreach ($frequencies as $key => $value_frequencies) {
                foreach ($value_frequencies as $key => $value) {
                    $frequency_name = isset($value['values']['frequency-name']) ? $value['values']['frequency-name'] : '';
                    $frequency_value = isset($value['values']['frequency-value']) ? $value['values']['frequency-value'] : '';
                    $frequencies_arr[$frequency_value] = $frequency_name;
                }
            }
        }

        $donation_form_donation_types = get_post_meta( $post_id, 'donation_form_donation_types', true );
        $donationtypes_config = array(
            'frequencies' => $frequencies,
            'frequencies_arr' => array_unique($frequencies_arr),
            'donation_form_donation_types' => $donation_form_donation_types,
        );
        return $donationtypes_config;
    }

	/**
	 * Returns all payment gateway and gateway settings.
	 *
	 * This function returns all the gateway-related configurations from a specific Donation Form Settings.
	 *
	 * @param $post_id
	 *
	 * @param $sfinstance
	 * @return array
	 *
	 * @LastUpdated  May 1, 2018
	 * @since  1.0.0
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 */
    public function get_gateway_config($post_id,$sfinstance){
        $donation_v5_form_payment_gateways = get_post_meta( $post_id, 'donation_v5_form_payment_gateways_'.$sfinstance, true );
        $pronto_donation_gateway_list = get_option('WFCDON_gateway_list_'.$sfinstance);
        $geteway_settings = array();
        if(!empty($pronto_donation_gateway_list)){
            foreach ($pronto_donation_gateway_list as $key => $value) {
               $DeveloperName = isset($value->RecordType->DeveloperName) ? strtolower($value->RecordType->DeveloperName) : '';
               $DeveloperName = str_replace('_', '', $DeveloperName);
               $DeveloperName = strtolower($DeveloperName);

               $Id = isset($value->Id) ? $value->Id : '';
               $geteway_settings[$sfinstance.'_'.$DeveloperName.'_'.$Id] = get_option($sfinstance.'_'.$DeveloperName.'_'.$Id);
               
            }
        }

        $gateway = array(
            'donation_v5_form_payment_gateways' => $donation_v5_form_payment_gateways,
            'pronto_donation_gateway_list' => $pronto_donation_gateway_list,
            'geteway_settings' => $geteway_settings,
        );
        return $gateway;
    }

    /**
     * Returns all Payment Details settings
     *
     *  This function returns all the Payment Details-related configurations from a specific Donation Form Settings.
     *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     * @since  1.0.0
     *
     * @param $post_id
     *
     * @return array
     *
     * @LastUpdated  May 1, 2018
    */
    public function get_paymentdetials_config($post_id){ 

        $form_credit = array();
        $donation_form_credit = get_post_meta( $post_id, 'donation_form_credit', true );
        $donation_form_credit = json_decode($donation_form_credit,true);
        if(is_array($donation_form_credit)) {
            foreach ($donation_form_credit as $key1 => $value_form_credit) {
                foreach ($value_form_credit as $key2 => $value) {
                    $cclabel = isset($value['values']['label']) ? $value['values']['label'] : '';
                    $ccoriginal_label = isset($value['values']['original_label']) ? $value['values']['original_label'] : '';
                    $ccplaceholder = isset($value['values']['placeholder']) ? $value['values']['placeholder'] : '';
                    $form_credit[$ccoriginal_label] = array(
                        'label' => $cclabel,
                        'placeholder' => $ccplaceholder
                    );
                }
            }
        }

        $form_debit = array();
        $donation_form_debit = get_post_meta( $post_id, 'donation_form_debit', true );
        $donation_form_debit = json_decode($donation_form_debit,true);
        if(is_array($donation_form_debit)) {
            foreach ($donation_form_debit as $key1 => $value_form_debit) {
                foreach ($value_form_debit as $key2 => $value) {
                    $ddlabel = isset($value['values']['label']) ? $value['values']['label'] : '';
                    $ddoriginal_label = isset($value['values']['original_label']) ? $value['values']['original_label'] : '';
                    $ddplaceholder = isset($value['values']['placeholder']) ? $value['values']['placeholder'] : '';
                    $form_debit[$ddoriginal_label] = array(
                        'label' => $ddlabel,
                        'placeholder' => $ddplaceholder
                    );
                }
            }
        }

        $paymentdetials = array(
            'donation_form_credit' => $form_credit,
            'donation_form_debit' => $form_debit,
        );
        return $paymentdetials;
    }

    /**
     * Returns all form style settings.
     *
	 * This function returns all the Form Style configurations from a specific Donation Form Settings.
	 *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     *
     * @since  1.0.0
     *
     * @param $post_id
     *
     * @return array
     *
     * @LastUpdated May 23, 2018
     */
    public function get_formstyle_config($post_id){
        $donation_form_class = get_post_meta( $post_id, 'donation_form_class', true );
        $donation_form_input_class = get_post_meta( $post_id, 'donation_form_input_class', true );
        $donation_form_btn_class = get_post_meta( $post_id, 'donation_form_btn_class', true );
        $donation_form_btn_caption = get_post_meta( $post_id, 'donation_form_btn_caption', true );

        $formstyle = array(
            'donation_form_class' => $donation_form_class,
            'donation_form_input_class' => $donation_form_input_class,
            'donation_form_btn_class' => $donation_form_btn_class,
            'donation_form_btn_caption' => $donation_form_btn_caption,
        );
        return $formstyle;
    }
	
	/**
	 * Returns Tribute Field settings.
	 *
	 * This function returns all the Tribute Field-related configurations from a specific Donation Form Settings.
	 *
	 * LastUpdated : May 28, 2018
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since  1.0.0
	 *
	 * @param $post_id
	 *
	 * @return array
	 *
	 * @LastUpdated  May 28, 2018
	 */
    public function get_tributefield_config($post_id){
        $donation_form_gift = get_post_meta( $post_id, 'donation_form_gift', true );
        $donation_form_gift_caption = get_post_meta( $post_id, 'donation_form_gift_caption', true );

        $tributefield = array(
            'donation_form_gift' => $donation_form_gift,
            'donation_form_gift_caption' => $donation_form_gift_caption,
        );
        return $tributefield;
    }
    
    /**
     * Returns Failure Message Settings.
     *
     * This function returns all the Failure Message configurations from a specific Donation Form Settings.
     *
     * @author Junjie Canonio <junjie@alphasys.com.au>
     *
     * @since  1.0.0
     *
     * @param $post_id
     *
     * @return array
     *
     * @LastUpdated  September 3, 2018
     */
    public function get_failuremessage_config($post_id){
	    $donation_form_error_message_behavior = get_post_meta( $post_id, 'donation_form_error_message_behavior', true );
        $donation_form_override_failure_message = get_post_meta( $post_id, 'donation_form_override_failure_message', true );
        $donation_form_override_message_type = get_post_meta( $post_id, 'donation_form_override_message_type', true );
        $donation_form_override_message = get_post_meta( $post_id, 'donation_form_override_message', true );

        $failuremessage = array(
	        'donation_form_error_message_behavior' => $donation_form_error_message_behavior,
            'donation_form_override_failure_message' => $donation_form_override_failure_message,
            'donation_form_override_message_type' => $donation_form_override_message_type,
            'donation_form_override_message' => $donation_form_override_message,
        );
        return $failuremessage;
    }
	
	/**
	 * Get country code by country name.
	 *
	 * This function returns the country code based on the provided country name.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param string $country
	 *
	 * @return false|int|string
	 *
	 * @LastUpdated  May 3, 2018
	 */
    public static function wfc_country_code( $country ) {
        $countries = array(
            'AF' => 'Afghanistan',
            'AX' => 'Aland Islands',
            'AL' => 'Albania',
            'DZ' => 'Algeria',
            'AS' => 'American Samoa',
            'AD' => 'Andorra',
            'AO' => 'Angola',
            'AI' => 'Anguilla',
            'AQ' => 'Antarctica',
            'AG' => 'Antigua And Barbuda',
            'AR' => 'Argentina',
            'AM' => 'Armenia',
            'AW' => 'Aruba',
            'AU' => 'Australia',
            'AT' => 'Austria',
            'AZ' => 'Azerbaijan',
            'BS' => 'Bahamas',
            'BH' => 'Bahrain',
            'BD' => 'Bangladesh',
            'BB' => 'Barbados',
            'BY' => 'Belarus',
            'BE' => 'Belgium',
            'BZ' => 'Belize',
            'BJ' => 'Benin',
            'BM' => 'Bermuda',
            'BT' => 'Bhutan',
            'BO' => 'Bolivia',
            'BA' => 'Bosnia And Herzegovina',
            'BW' => 'Botswana',
            'BV' => 'Bouvet Island',
            'BR' => 'Brazil',
            'IO' => 'British Indian Ocean Territory',
            'BN' => 'Brunei Darussalam',
            'BG' => 'Bulgaria',
            'BF' => 'Burkina Faso',
            'BI' => 'Burundi',
            'KH' => 'Cambodia',
            'CM' => 'Cameroon',
            'CA' => 'Canada',
            'CV' => 'Cape Verde',
            'KY' => 'Cayman Islands',
            'CF' => 'Central African Republic',
            'TD' => 'Chad',
            'CL' => 'Chile',
            'CN' => 'China',
            'CX' => 'Christmas Island',
            'CC' => 'Cocos (Keeling) Islands',
            'CO' => 'Colombia',
            'KM' => 'Comoros',
            'CG' => 'Congo',
            'CD' => 'Congo, Democratic Republic',
            'CK' => 'Cook Islands',
            'CR' => 'Costa Rica',
            'CI' => 'Cote D\'Ivoire',
            'HR' => 'Croatia',
            'CU' => 'Cuba',
            'CY' => 'Cyprus',
            'CZ' => 'Czech Republic',
            'DK' => 'Denmark',
            'DJ' => 'Djibouti',
            'DM' => 'Dominica',
            'DO' => 'Dominican Republic',
            'EC' => 'Ecuador',
            'EG' => 'Egypt',
            'SV' => 'El Salvador',
            'GQ' => 'Equatorial Guinea',
            'ER' => 'Eritrea',
            'EE' => 'Estonia',
            'ET' => 'Ethiopia',
            'FK' => 'Falkland Islands (Malvinas)',
            'FO' => 'Faroe Islands',
            'FJ' => 'Fiji',
            'FI' => 'Finland',
            'FR' => 'France',
            'GF' => 'French Guiana',
            'PF' => 'French Polynesia',
            'TF' => 'French Southern Territories',
            'GA' => 'Gabon',
            'GM' => 'Gambia',
            'GE' => 'Georgia',
            'DE' => 'Germany',
            'GH' => 'Ghana',
            'GI' => 'Gibraltar',
            'GR' => 'Greece',
            'GL' => 'Greenland',
            'GD' => 'Grenada',
            'GP' => 'Guadeloupe',
            'GU' => 'Guam',
            'GT' => 'Guatemala',
            'GG' => 'Guernsey',
            'GN' => 'Guinea',
            'GW' => 'Guinea-Bissau',
            'GY' => 'Guyana',
            'HT' => 'Haiti',
            'HM' => 'Heard Island & Mcdonald Islands',
            'VA' => 'Holy See (Vatican City State)',
            'HN' => 'Honduras',
            'HK' => 'Hong Kong',
            'HU' => 'Hungary',
            'IS' => 'Iceland',
            'IN' => 'India',
            'ID' => 'Indonesia',
            'IR' => 'Iran, Islamic Republic Of',
            'IQ' => 'Iraq',
            'IE' => 'Ireland',
            'IM' => 'Isle Of Man',
            'IL' => 'Israel',
            'IT' => 'Italy',
            'JM' => 'Jamaica',
            'JP' => 'Japan',
            'JE' => 'Jersey',
            'JO' => 'Jordan',
            'KZ' => 'Kazakhstan',
            'KE' => 'Kenya',
            'KI' => 'Kiribati',
            'KR' => 'Korea',
            'KW' => 'Kuwait',
            'KG' => 'Kyrgyzstan',
            'LA' => 'Lao People\'s Democratic Republic',
            'LV' => 'Latvia',
            'LB' => 'Lebanon',
            'LS' => 'Lesotho',
            'LR' => 'Liberia',
            'LY' => 'Libyan Arab Jamahiriya',
            'LI' => 'Liechtenstein',
            'LT' => 'Lithuania',
            'LU' => 'Luxembourg',
            'MO' => 'Macao',
            'MK' => 'Macedonia',
            'MG' => 'Madagascar',
            'MW' => 'Malawi',
            'MY' => 'Malaysia',
            'MV' => 'Maldives',
            'ML' => 'Mali',
            'MT' => 'Malta',
            'MH' => 'Marshall Islands',
            'MQ' => 'Martinique',
            'MR' => 'Mauritania',
            'MU' => 'Mauritius',
            'YT' => 'Mayotte',
            'MX' => 'Mexico',
            'FM' => 'Micronesia, Federated States Of',
            'MD' => 'Moldova',
            'MC' => 'Monaco',
            'MN' => 'Mongolia',
            'ME' => 'Montenegro',
            'MS' => 'Montserrat',
            'MA' => 'Morocco',
            'MZ' => 'Mozambique',
            'MM' => 'Myanmar',
            'NA' => 'Namibia',
            'NR' => 'Nauru',
            'NP' => 'Nepal',
            'NL' => 'Netherlands',
            'AN' => 'Netherlands Antilles',
            'NC' => 'New Caledonia',
            'NZ' => 'New Zealand',
            'NI' => 'Nicaragua',
            'NE' => 'Niger',
            'NG' => 'Nigeria',
            'NU' => 'Niue',
            'NF' => 'Norfolk Island',
            'MP' => 'Northern Mariana Islands',
            'NO' => 'Norway',
            'OM' => 'Oman',
            'PK' => 'Pakistan',
            'PW' => 'Palau',
            'PS' => 'Palestinian Territory, Occupied',
            'PA' => 'Panama',
            'PG' => 'Papua New Guinea',
            'PY' => 'Paraguay',
            'PE' => 'Peru',
            'PH' => 'Philippines',
            'PN' => 'Pitcairn',
            'PL' => 'Poland',
            'PT' => 'Portugal',
            'PR' => 'Puerto Rico',
            'QA' => 'Qatar',
            'RE' => 'Reunion',
            'RO' => 'Romania',
            'RU' => 'Russian Federation',
            'RW' => 'Rwanda',
            'BL' => 'Saint Barthelemy',
            'SH' => 'Saint Helena',
            'KN' => 'Saint Kitts And Nevis',
            'LC' => 'Saint Lucia',
            'MF' => 'Saint Martin',
            'PM' => 'Saint Pierre And Miquelon',
            'VC' => 'Saint Vincent And Grenadines',
            'WS' => 'Samoa',
            'SM' => 'San Marino',
            'ST' => 'Sao Tome And Principe',
            'SA' => 'Saudi Arabia',
            'SN' => 'Senegal',
            'RS' => 'Serbia',
            'SC' => 'Seychelles',
            'SL' => 'Sierra Leone',
            'SG' => 'Singapore',
            'SK' => 'Slovakia',
            'SI' => 'Slovenia',
            'SB' => 'Solomon Islands',
            'SO' => 'Somalia',
            'ZA' => 'South Africa',
            'GS' => 'South Georgia And Sandwich Isl.',
            'ES' => 'Spain',
            'LK' => 'Sri Lanka',
            'SD' => 'Sudan',
            'SR' => 'Suriname',
            'SJ' => 'Svalbard And Jan Mayen',
            'SZ' => 'Swaziland',
            'SE' => 'Sweden',
            'CH' => 'Switzerland',
            'SY' => 'Syrian Arab Republic',
            'TW' => 'Taiwan',
            'TJ' => 'Tajikistan',
            'TZ' => 'Tanzania',
            'TH' => 'Thailand',
            'TL' => 'Timor-Leste',
            'TG' => 'Togo',
            'TK' => 'Tokelau',
            'TO' => 'Tonga',
            'TT' => 'Trinidad And Tobago',
            'TN' => 'Tunisia',
            'TR' => 'Turkey',
            'TM' => 'Turkmenistan',
            'TC' => 'Turks And Caicos Islands',
            'TV' => 'Tuvalu',
            'UG' => 'Uganda',
            'UA' => 'Ukraine',
            'AE' => 'United Arab Emirates',
            'GB' => 'United Kingdom',
            'US' => 'United States',
            'UM' => 'United States Outlying Islands',
            'UY' => 'Uruguay',
            'UZ' => 'Uzbekistan',
            'VU' => 'Vanuatu',
            'VE' => 'Venezuela',
            'VN' => 'Viet Nam',
            'VG' => 'Virgin Islands, British',
            'VI' => 'Virgin Islands, U.S.',
            'WF' => 'Wallis And Futuna',
            'EH' => 'Western Sahara',
            'YE' => 'Yemen',
            'ZM' => 'Zambia',
            'ZW' => 'Zimbabwe',
        );
        return array_search( strtolower($country), array_map('strtolower', $countries));
    }

	/**
	 * Converts array to object.
	 *
	 * This function returns an object-converted version of the array that is provided.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $data
	 *
	 * @return object
	 *
	 * @LastUpdated  May 3, 2018
	 */
    public static function wfc_array_to_object( $data ){
        $options = new stdClass();
        if( !empty( $data ) ) {
            foreach ( $data as $key => $value )
            {
                $options->$key = $value;
            }
        }
        return $options;
    }
	
	/**
	 * Cleans payment source.
	 *
	 * This function clears all payment source data.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param null $key
	 *
	 * @return array
	 *
	 * @LastUpdated  May 7, 2018
	 */
    public function wfc_clean_payment_source( $post, $key = null ) {
        if( $key == null ) {
            foreach( array(
                'card_number',
                'expiry_month',
                'expiry_year',
                'name_on_card',
                'ccv',
            ) as $details ) {
                unset( $post[$details] );
            }
            
            return $post;

        } else {
            /*
            *  check arrays contained in this array
            */
            foreach ( $post['data'] as $K => $element ) {
                $post['data'][$K]['strDonation'] = $this->wfc_creditcard( $element['strDonation'], true );
            }

            return $post;
        }
    }
	
	/**
	 * Construct credit card details.
	 *
	 * This function reconstructs and returns an encrypted version of payment source details.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param bool $is_PS
	 *
	 * @return mixed
	 *
	 * @LastUpdated  May 7, 2018
	 */
    public function wfc_creditcard( $post, $is_PS = false ) {
        if( !isset( $post['SalesforceResponse']['errorCode'] ) ) {
            if (isset($post['PaymentSource'])) {
                if( $is_PS && $post['donationType'] != 'one' ) {
                    $PaymentSource_ccno = isset($post['PaymentSource']['ccno']) ? $post['PaymentSource']['ccno'] : '';
                    $last_3digit = substr( $PaymentSource_ccno, -3 );
                    $post['PaymentSource']['ccno'] = "****************{$last_3digit}";
                    $post['PaymentSource']['ccv'] = '***';
                } elseif( ! $is_PS ) {
                    if( $post['pdwp_donation_type'] != 'one-off' ) {
                        $ccno = $this->pdwp_encrypt_decrypt( 'decrypt', trim( $post['card_details']['card_number'] ) );
                        $last_3digit = substr( $ccno, -3 );
                        $post['card_details']['card_number'] = "****************{$last_3digit}";
                        $post['card_details']['ccv'] = '***';
                    }
                }               
            }
        }
        return $post;
    }
	
	/**
	 * Syncs single donation to Saleforce.
	 *
	 * This function syncs single donation to Saleforce and save PPAPI response and sync history.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $donation
	 * @param      $main_class
	 * @param null $recurringid
	 *
	 * @return mixed
	 *
	 * @LastUpdated  May 7, 2018
	 */
    public function wfc_sync_singledonation_tosf($donation,$main_class, $recurringid = NULL) {
        $WFC_DonationList = new WFC_DonationList();
        $WFC_DonationEmailHandler = new WFC_DonationEmailHandler();
        $WFC_SalesforcePPAPI = new WFC_SalesforcePPAPI();

        if(!empty($recurringid)){

            $WFC_DonationForms = new WFC_DonationForms();
            $post_meta_id = isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '';
            $donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $post_meta_id );
            $rec_donations = maybe_unserialize( $donations[0]['meta_value'] );  

            if(isset($rec_donations['payment_type']) && $rec_donations['payment_type'] == 'bank') {
                /*
                * parse  ppapi response
                */             
                $donation['SalesforceResponse'] = isset($rec_donations['SalesforceResponse']) ? $rec_donations['SalesforceResponse'] : array();
                $donation['SalesforceResponse']['OpportunityId'] = ''; 

                /*
                * log sf sync activity
                */      
                $donation['SalesforceLogs']['data']['single'] = '';
                $donation['SalesforceLogs']['result']['single'] = '';
                
                $recurring_dontion_sfres = isset($rec_donations['SalesforceLogs']) ? $rec_donations['SalesforceLogs'] : array();

                $donation['SalesforceLogs']['data']['recurring'] = 
                isset($recurring_dontion_sfres['data']['recurring']) ? $recurring_dontion_sfres['data']['recurring'] : array();

                $donation['SalesforceLogs']['result']['recurring'] = 
                isset($recurring_dontion_sfres['result']['recurring']) ? $recurring_dontion_sfres['result']['recurring'] : array();


            }else{
                /*
                * sync single donation
                */  
                $result = $WFC_SalesforcePPAPI->wfc_sync_singledonation($donation, $recurringid);

                /*
                * parse  ppapi response
                */             
                $donation['SalesforceResponse'] = isset($rec_donations['SalesforceResponse']) ? $rec_donations['SalesforceResponse'] : array();
                $donation['SalesforceResponse']['OpportunityId'] = isset($result['result']['single']['sf_response']['oppResult']['Id']) ? $result['result']['single']['sf_response']['oppResult']['Id'] : ''; 

                /*
                * log sf sync activity
                */      
                $recurring_dontion_sfres = isset($rec_donations['SalesforceLogs']) ? $rec_donations['SalesforceLogs'] : array();
                
                $donation['SalesforceLogs']['data']['recurring'] = 
                isset($recurring_dontion_sfres['data']['recurring']) ? $recurring_dontion_sfres['data']['recurring'] : array();

                $donation['SalesforceLogs']['result']['recurring'] = 
                isset($recurring_dontion_sfres['result']['recurring']) ? $recurring_dontion_sfres['result']['recurring'] : array();

                $single_dontion_sfres = $this->wfc_clean_payment_source( $result, 'PaymentSource' );  

                $donation['SalesforceLogs']['data']['single'] = 
                isset($single_dontion_sfres['data']['single']) ? $single_dontion_sfres['data']['single'] : array();

                if(isset($result['result']['single']['sf_response'])){
                    $donation['SalesforceLogs']['result']['single'] = isset($result['result']['single']['sf_response']) ?
                    $result['result']['single']['sf_response'] : $result;  
                }else{
                    $donation['SalesforceLogs']['result']['single'] = 
                    isset($single_dontion_sfres['result']['single']) ? $single_dontion_sfres['result']['single'] : array();
                }                 
            }

            /*
            *  send success notification
            */
            $WFC_DonationEmailHandler->wfc_donation_admin_success_notification($donation);

        }else{
            /*
            * sync single donation
            */  
            $result = $WFC_SalesforcePPAPI->wfc_sync_singledonation($donation, $recurringid);

            /*
            * parse  ppapi response
            */
            $donation['SalesforceResponse'] = $WFC_SalesforcePPAPI->wfc_ppapi_parse_response($donation, $result);

            /*
            * log sf sync activity
            */      
            $donation['SalesforceLogs'] = $this->wfc_clean_payment_source( $result, 'PaymentSource' ); 
            if(isset($result['result']['single']['sf_response'])){
                $donation['SalesforceLogs']['result']['single'] = isset($result['result']['single']['sf_response']) ? $result['result']['single']['sf_response'] : $result;      
            }    
              
        }

        /*
        * add resync attemp
        */  
        $donation['resync_attemp'] = isset( $donation['resync_attemp'] ) ? (int)$donation['resync_attemp'] + 1 : 1;

        /*
        * ppapi sync history
        */      
        $resync_attemp = isset($donation['resync_attemp']) ? $donation['resync_attemp'] : 1;
        $SalesforceSyncDetails['SalesforceResponse'] = $donation['SalesforceResponse'];
        $SalesforceSyncDetails['SalesforceLogs'] = $donation['SalesforceLogs'];
        $SalesforceSyncHistory = isset($donation['SalesforceSyncHistory']) ? $donation['SalesforceSyncHistory'] : array();
        $SalesforceSyncHistory['attemp-'.$resync_attemp] = $SalesforceSyncDetails;
        $donation['SalesforceSyncHistory'] = $SalesforceSyncHistory;
  
        /*
        * update donation records with payment response
        */
        $donation_post_meta_id = isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '';
        $main_class->wfc_donation_data( array( serialize( $donation ), $donation_post_meta_id  ), 'update' );

        /*
        *  send success notification
        */
        $WFC_DonationEmailHandler->wfc_donation_donor_thankyou_email($donation);
        
        /*
         * hook that run before completing the whole donation
         */
        do_action( 'pdwp_donation_complete', array(
            'donation' => $donation,
            'main_class' => $main_class
        ));

        /*
         * forward DPID TO salesforce
         */
        if( isset($donation['donor_DPID'] ) && !empty($donation['donor_DPID'] ) ){
	        $donation_sfinstance = isset($donation['donation_sfinstance']) ? $donation['donation_sfinstance'] : '';

            $donor_barcode = isset($donation['donor_barcode']) ? $donation['donor_barcode'] : '';
            $donor_barcode = !empty($donor_barcode) ? $donor_barcode : 'Empty';

            $donor_latitude = isset($donation['donor_latitude']) ? $donation['donor_latitude'] : '';
            $donor_latitude = !empty($donor_latitude) ? $donor_latitude : '';

            $donor_longitude = isset($donation['donor_longitude']) ? $donation['donor_longitude'] : '';
            $donor_longitude = !empty($donor_longitude) ? $donor_longitude : '';

            $oauth = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$donation_sfinstance);

            if( isset( $donation['SalesforceLogs']['result']['single']['oppResult']['AccountId'] ) ){
                $account_id = $donation['SalesforceLogs']['result']['single']['oppResult']['AccountId'];
                $data = array(
                    'ASPU__DPID__c' => $donation['donor_DPID'],
                    'ASPU__Barcode__c' =>  $donor_barcode,
                    'BillingLatitude' =>  $donor_latitude,
                    'BillingLongitude' =>  $donor_longitude,
                );
                $result = $oauth->api_request( 'data/v39.0/sobjects/Account/'.$account_id, $data, 'update' );
            }

            if( isset( $donation['SalesforceLogs']['result']['single']['oppResult']['ASPHPP__Contact__c'] ) ){
                $contact_id = $donation['SalesforceLogs']['result']['single']['oppResult']['ASPHPP__Contact__c'];
                $data = array(
                    'ASPU__DPID__c' => $donation['donor_DPID'],
                    'ASPU__Barcode__c' =>  $donor_barcode,
                    'MailingLatitude' =>  $donor_latitude,
                    'MailingLongitude' =>  $donor_longitude,
                );
                $result = $oauth->api_request( 'data/v39.0/sobjects/Contact/'.$contact_id, $data, 'update' );
            }else{
                if( isset( $donation['SalesforceLogs']['result']['recurring']['recResult']['ASPHPPADDON__Contact__c'] ) ){
                    $contact_id = $donation['SalesforceLogs']['result']['recurring']['recResult']['ASPHPPADDON__Contact__c'];
                    $data = array(
                        'ASPU__DPID__c' => $donation['donor_DPID'],
                        'ASPU__Barcode__c' =>  $donor_barcode,
                        'MailingLatitude' =>  $donor_latitude,
                        'MailingLongitude' =>  $donor_longitude,
                    );
                    $result = $oauth->api_request( 'data/v39.0/sobjects/Contact/'.$contact_id, $data, 'update' );
                }
            } 
        }

        return $donation;  
    }
	
	/**
	 * Syncs recurring donation to Saleforce.
	 *
	 * This function syncs recurring donation to Saleforce and saves both the PPAPI response and the sync history.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $donation
	 * @param $main_class
	 * @param $payment_source
	 *
	 * @return mixed
	 *
	 * @LastUpdated  May 7, 2018
	 */
    public function wfc_sync_recurringdonation_tosf($donation,$main_class, $payment_source) {  
        $WFC_SalesforcePPAPI = new WFC_SalesforcePPAPI();
        $result = $WFC_SalesforcePPAPI->wfc_sync_recurringdonation($donation, $payment_source);

        /*
        * parse  ppapi response
        */             
        $donation['SalesforceResponse'] = $WFC_SalesforcePPAPI->wfc_ppapi_parse_response($donation, $result);

        /*
        * log sf sync activity
        */
        if(!empty($payment_source)) {
            $donation['SalesforceLogs'] = $this->wfc_clean_payment_source( $result, 'PaymentSource' );
        }else{
            $donation['SalesforceLogs'] = $this->wfc_clean_payment_source( $result, 'PaymentSource' );
            unset($donation['SalesforceLogs']['data']['recurring']['strDonation']['PaymentSource']);
        }      

        if(isset($result['result']['recurring']['sf_response'])){
            $donation['SalesforceLogs']['result']['recurring'] = isset($result['result']['recurring']['sf_response']) ? $result['result']['recurring']['sf_response'] : $result;      
        }    

        /*
        * add resync attemp
        */  
        $donation['resync_attemp'] = isset( $donation['resync_attemp'] ) ? (int)$donation['resync_attemp'] + 1 : 1;

        /*
        * ppapi sync history
        */      
        $resync_attemp = isset($donation['resync_attemp']) ? $donation['resync_attemp'] : 1;
        $SalesforceSyncDetails['SalesforceResponse'] = $donation['SalesforceResponse'];
        $SalesforceSyncDetails['SalesforceLogs'] = $donation['SalesforceLogs'];
        $SalesforceSyncHistory = isset($donation['SalesforceSyncHistory']) ? $donation['SalesforceSyncHistory'] : array();
        $SalesforceSyncHistory['attemp-'.$resync_attemp] = $SalesforceSyncDetails;
        $donation['SalesforceSyncHistory'] = $SalesforceSyncHistory;
        
        /*
        * update donation records with payment response
        */
        $post_meta_id = isset( $donation['post_meta_id'] ) ? $donation['post_meta_id'] : '';
        $main_class->wfc_donation_data( array( serialize( $donation ), $post_meta_id ), 'update' );


        if (isset($donation['donation']['payment_type']) && $donation['donation']['payment_type'] == 'bank'){
			/*
			* hook that run before completing the whole donation
			*/
	        do_action( 'pdwp_donation_complete', array(
		        'donation' => $donation,
		        'main_class' => $main_class
	        ));
        }
        return $donation;              
    }
	
	/**
	 * Syncs advance cart donation to Saleforce.
	 *
	 * This function syncs advance cart donation to Saleforce and saves both the PPAPI response and the sync history.
	 *
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $donation
	 * @param      $main_class
	 * @param null $recurringid
	 *
	 * @return mixed
	 *
	 * @LastUpdated  July 23, 2018
	 */
    public function wfc_sync_advancecartdonation_tosf($donation,$main_class, $recurringid = NULL) { 
        $WFC_SalesforcePPAPI = new WFC_SalesforcePPAPI();
        /*
        * parse  ppapi response
        */             
        $wfc_advancecart_suffix = isset($donation['wfc_advancecart_suffix']) ? $donation['wfc_advancecart_suffix'] : '';

        if(isset($donation['pdwp_donation_type']) && $donation['pdwp_donation_type'] == 'one-off' && empty($recurringid)) {
            $result = $WFC_SalesforcePPAPI->wfc_sync_singledonation($donation);     
            $donation['SalesforceResponse'][$wfc_advancecart_suffix] = $WFC_SalesforcePPAPI->wfc_ppapi_parse_response($donation, $result);
            $donation['SalesforceLogs'][$wfc_advancecart_suffix] = $this->wfc_clean_payment_source( $result, 'PaymentSource' );   
            $donation['SalesforceLogs'][$wfc_advancecart_suffix]['result']['single'] = isset($result['result']['single']['sf_response']) ? $result['result']['single']['sf_response'] : $result;     

            /*
             * hook that run before completing the whole donation
             */
            do_action( 'pdwp_donation_complete', array(
                'donation' => $donation,
                'main_class' => $this->main_class
            ));
        }elseif(!empty($recurringid)){

            $result = $WFC_SalesforcePPAPI->wfc_sync_singledonation($donation, $recurringid);
            
            $cartarg = array(
                'wfc_carttype' => 'advance_cart',
                'wfc_donation_type' => 'one-off',
                'recurringid' => $recurringid,
                'suffix' => $wfc_advancecart_suffix,
            );
            $donation['SalesforceResponse'][$wfc_advancecart_suffix] = 
            $WFC_SalesforcePPAPI->wfc_ppapi_parse_response(
                $donation, 
                $result,
                $cartarg
            );

            /*
            * log sf sync activity
            */      
            $recurring_dontion_sfres = isset($donation['SalesforceLogs'][$wfc_advancecart_suffix]) ? $donation['SalesforceLogs'][$wfc_advancecart_suffix] : array();
            
            $donation['SalesforceLogs'][$wfc_advancecart_suffix]['data']['recurring'] = 
            isset($recurring_dontion_sfres['data']['recurring']) ? $recurring_dontion_sfres['data']['recurring'] : array();

            $donation['SalesforceLogs'][$wfc_advancecart_suffix]['result']['recurring'] = 
            isset($recurring_dontion_sfres['result']['recurring']) ? $recurring_dontion_sfres['result']['recurring'] : array();

            $single_dontion_sfres = $this->wfc_clean_payment_source( $result, 'PaymentSource' );  

            $donation['SalesforceLogs'][$wfc_advancecart_suffix]['data']['single'] = 
            isset($single_dontion_sfres['data']['single']) ? $single_dontion_sfres['data']['single'] : array();

            if(isset($result['result']['single']['sf_response'])){
                $donation['SalesforceLogs'][$wfc_advancecart_suffix]['result']['single'] = isset($result['result']['single']['sf_response']) ? $result['result']['single']['sf_response'] : $result;  
            }else{
                $donation['SalesforceLogs'][$wfc_advancecart_suffix]['result']['single'] = 
                isset($single_dontion_sfres['result']['single']) ? $single_dontion_sfres['result']['single'] : $result;
            }

            /*
             * hook that run before completing the whole donation
             */
            do_action( 'pdwp_donation_complete', array(
                'donation' => $donation,
                'main_class' => $main_class
            ));               
        } elseif(isset($donation['pdwp_donation_type']) && ($donation['pdwp_donation_type'] != NULL && $donation['pdwp_donation_type'] != 'one-off')){
            $result = $WFC_SalesforcePPAPI->wfc_sync_recurringdonation($donation,''); 
            $donation['SalesforceResponse'][$wfc_advancecart_suffix] = $WFC_SalesforcePPAPI->wfc_ppapi_parse_response($donation, $result);
            $donation['SalesforceLogs'][$wfc_advancecart_suffix] = $this->wfc_clean_payment_source( $result, 'PaymentSource' );
            $donation['SalesforceLogs'][$wfc_advancecart_suffix]['result']['recurring'] = isset($result['result']['recurring']['sf_response']) ? $result['result']['recurring']['sf_response'] : $result;  
        }else{
            
        }

        /*
        * update donation records with payment response
        */
        $post_meta_id = isset( $donation['post_meta_id'] ) ? $donation['post_meta_id'] : '';
        $main_class->wfc_donation_data( array( serialize( $donation ), $post_meta_id ), 'update' );

        return $donation;      
    }

}