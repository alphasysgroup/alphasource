<?php

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('WFC_DonationFormSettings')) {
	/**
	 * Class WFC_DonationFormSettings
	 *
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	class WFC_DonationFormSettings {

		/**
		 * Fetches all normal donation form template.
		 *
		 * This Function scans for template folders from the current theme or a currently active add-on and fetches
		 * Normal Donation Form templates.
		 *
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 *
		 * @return array
		 */
		
		public static function _get_pdwp_templates() {
			
			/*Fetch templates forms on WFC - Donation plugin */
			$dir = plugin_dir_path( __FILE__ ) . '../../public/templates/normal/';
			$dirItems = scandir( $dir );
			$templates = array();
			foreach ( $dirItems as $file ) {
				if( !is_file( $file ) && !is_dir( $file ) ) {
					$data = get_file_data( $dir . $file, array( 'Title' ) );
					$temp_path = $dir . $file;
					$temp_name = $data[0];
					$templates[$temp_path] = $temp_name;
				}
			}
				
			/*Fetch template forms on WFC - Donation addon plugin */
			$plugin_list = get_plugins();
			$plugins_as_addon = array();
			foreach ($plugin_list as $plugin_list_key => $plugin_list_value) {
				$plugins_as_addon_val = $pieces = explode("/", $plugin_list_key);
				$plugins_as_addon_val = isset($plugins_as_addon_val[0]) ? $plugins_as_addon_val[0] : $plugin_list_key;
				$plugins_as_addon[$plugins_as_addon_val] = isset($plugin_list_value['Name']) ? $plugin_list_value['Name'] : 'None';
			}	
			foreach ($plugins_as_addon as $plugins_as_addon_key => $value) {
				$addon_dir = ABSPATH . 'wp-content/plugins/'.$plugins_as_addon_key.'/wfc-donation-templates/normal/';
				if( is_dir($addon_dir) === true ){
					$addon_dirItems = scandir( $addon_dir );
					foreach ( $addon_dirItems as $file ) {
						if( $file !== '.' && $file !== '..'){
							if( !is_file( $file ) && !is_dir( $file ) ) {
								$data = get_file_data( $addon_dir . $file, array( 'Title' ) );
								$temp_path = $addon_dir . $file;
								$temp_name = $data[0];
								$templates[$temp_path] = $temp_name;
							}
						}
					}
				}
			}
			
			$theme_dir = get_template_directory() . '/wfc-donation-templates/normal/';
			if( is_dir($theme_dir) === true ){
				$theme_dirItems = scandir( $theme_dir );
				foreach ( $theme_dirItems as $file ) {
					if( $file !== '.' && $file !== '..'){
						if( !is_file( $file ) && !is_dir( $file ) ) {
							$data = get_file_data( $theme_dir . $file, array( 'Title' ) );
							$temp_path = $theme_dir . $file;
							$temp_name = $data[0];
							$templates[$temp_path] = $temp_name;
						}
					}
				}
			}

			$childtheme_dir = get_stylesheet_directory() . '/wfc-donation-templates/normal/';
			if( is_dir($childtheme_dir) === true ){
				$childtheme_dirItems = scandir( $childtheme_dir );
				foreach ( $childtheme_dirItems as $file ) {
					if( $file !== '.' && $file !== '..'){
						if( !is_file( $file ) && !is_dir( $file ) ) {
							$data = get_file_data( $childtheme_dir . $file, array( 'Title' ) );
							$temp_path = $childtheme_dir . $file;
							$temp_name = $data[0];
							$templates[$temp_path] = $temp_name;
						}
					}
				}
			}
			
			return $templates;
		}

		/**
		 * Fetches all Wizard Donation Form template.
		 *
		 * This Function scans for template folders from the current theme or a currently active add-on and fetches
		 * Wizard Donation Form templates.
		 *
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 *
		 * @return array
		 */
		public static function _get_pdwp_templates2() {
			
			$dir = plugin_dir_path( __FILE__ ) . '../../public/templates/wizard/';
			$dirItems = scandir( $dir );
			$templates = array();
			foreach ( $dirItems as $file ) {
				if( !is_file( $file ) && !is_dir( $file ) ) {
					$data = get_file_data( $dir . $file, array( 'Title' ) );
					$temp_path = $dir . $file;
					$temp_name = $data[0];
					$templates[$temp_path] = $temp_name;
				}
			}

			/*Fetch template forms on WFC - Donation addon plugin */
			$plugin_list = get_plugins();
			$plugins_as_addon = array();
			foreach ($plugin_list as $plugin_list_key => $plugin_list_value) {
				$plugins_as_addon_val = $pieces = explode("/", $plugin_list_key);
				$plugins_as_addon_val = isset($plugins_as_addon_val[0]) ? $plugins_as_addon_val[0] : $plugin_list_key;
				$plugins_as_addon[$plugins_as_addon_val] = isset($plugin_list_value['Name']) ? $plugin_list_value['Name'] : 'None';
			}
			foreach ($plugins_as_addon as $plugins_as_addon_key => $value) {
				$addon_dir = ABSPATH . 'wp-content/plugins/'.$plugins_as_addon_key.'/wfc-donation-templates/wizard/';
				if( is_dir($addon_dir) === true ){
					$addon_dirItems = scandir( $addon_dir );
					foreach ( $addon_dirItems as $file ) {
						if( $file !== '.' && $file !== '..'){
							if( !is_file( $file ) && !is_dir( $file ) ) {
								$data = get_file_data( $addon_dir . $file, array( 'Title' ) );
								$temp_path = $addon_dir . $file;
								$temp_name = $data[0];
								$templates[$temp_path] = $temp_name;
							}
						}
					}
				}
			}

			$theme_dir = get_template_directory() . '/wfc-donation-templates/wizard/';
			if( is_dir($theme_dir) === true ){
				$theme_dirItems = scandir( $theme_dir );
				foreach ( $theme_dirItems as $file ) {
					if( $file !== '.' && $file !== '..'){
						if( !is_file( $file ) && !is_dir( $file ) ) {
							$data = get_file_data( $theme_dir . $file, array( 'Title' ) );
							$temp_path = $theme_dir . $file;
							$temp_name = $data[0];
							$templates[$temp_path] = $temp_name;
						}
					}
				}
			}

			$childtheme_dir = get_stylesheet_directory() . '/wfc-donation-templates/wizard/';
			if( is_dir($childtheme_dir) === true ){
				$childtheme_dirItems = scandir( $childtheme_dir );
				foreach ( $childtheme_dirItems as $file ) {
					if( $file !== '.' && $file !== '..'){
						if( !is_file( $file ) && !is_dir( $file ) ) {
							$data = get_file_data( $childtheme_dir . $file, array( 'Title' ) );
							$temp_path = $childtheme_dir . $file;
							$temp_name = $data[0];
							$templates[$temp_path] = $temp_name;
						}
					}
				}
			}

			return $templates;
		}
		
		/**
		 * Returns the details of all published payment gateways.
		 *
		 * This function returns the details of all the payment gateways published on the 'Payment Gateways' Settings
		 * Page. This will appear as checkbox select on the donation form settings.
		 *
		 * @return array
		 */

		public static function pd_get_donation_gateway_list($instance_slug) {
			$gateway = get_option('WFCDON_gateway_list_'.$instance_slug, array());

			if (count($gateway) > 0) {
			
				$payment_gateways = array();
				$index = 0;

				foreach ($gateway as $key => $value) {

					$gateway_type = strtolower($value->RecordType->DeveloperName);
					$gateway_type = str_replace('_', '', $gateway_type);
					$gateway_type = strtolower($gateway_type);
					$option = $instance_slug.'_'.esc_html(strtolower($gateway_type)) . '_' . esc_html($value->Id);

					$gateway_settings = get_option($option, array());

					if (isset($gateway_settings['wfc_donation_publish']) && $gateway_settings['wfc_donation_publish'] === 'true') {
						$payment_gateways["item-{$index}"] = array(
							'payment_gateway_field' => $gateway_type,
							'payment_gateway_name' => $value->Name,
							'payment_gateway_key' => $option
						);

						$index++;
					}
				}

				return $payment_gateways;

			}

			return array();
		}


		/**
		 * Fetches list of country with country code.
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @return array
		 */
		public static function getCountryList(){
			return $country = array (
				0 =>
					array (
						'value' => 'AF',
						'title' => 'Afghanistan',
					),
				1 =>
					array (
						'value' => 'AX',
						'title' => 'Aland Islands',
					),
				2 =>
					array (
						'value' => 'AL',
						'title' => 'Albania',
					),
				3 =>
					array (
						'value' => 'DZ',
						'title' => 'Algeria',
					),
				4 =>
					array (
						'value' => 'AD',
						'title' => 'Andorra',
					),
				5 =>
					array (
						'value' => 'AO',
						'title' => 'Angola',
					),
				6 =>
					array (
						'value' => 'AI',
						'title' => 'Anguilla',
					),
				7 =>
					array (
						'value' => 'AQ',
						'title' => 'Antarctica',
					),
				8 =>
					array (
						'value' => 'AG',
						'title' => 'Antigua and Barbuda',
					),
				9 =>
					array (
						'value' => 'AR',
						'title' => 'Argentina',
					),
				10 =>
					array (
						'value' => 'AM',
						'title' => 'Armenia',
					),
				11 =>
					array (
						'value' => 'AW',
						'title' => 'Aruba',
					),
				12 =>
					array (
						'value' => 'AU',
						'title' => 'Australia',
					),
				13 =>
					array (
						'value' => 'AT',
						'title' => 'Austria',
					),
				14 =>
					array (
						'value' => 'AZ',
						'title' => 'Azerbaijan',
					),
				15 =>
					array (
						'value' => 'BS',
						'title' => 'Bahamas',
					),
				16 =>
					array (
						'value' => 'BH',
						'title' => 'Bahrain',
					),
				17 =>
					array (
						'value' => 'BD',
						'title' => 'Bangladesh',
					),
				18 =>
					array (
						'value' => 'BB',
						'title' => 'Barbados',
					),
				19 =>
					array (
						'value' => 'BY',
						'title' => 'Belarus',
					),
				20 =>
					array (
						'value' => 'BE',
						'title' => 'Belgium',
					),
				21 =>
					array (
						'value' => 'BZ',
						'title' => 'Belize',
					),
				22 =>
					array (
						'value' => 'BJ',
						'title' => 'Benin',
					),
				23 =>
					array (
						'value' => 'BM',
						'title' => 'Bermuda',
					),
				24 =>
					array (
						'value' => 'BT',
						'title' => 'Bhutan',
					),
				25 =>
					array (
						'value' => 'BO',
						'title' => 'Bolivia, Plurinational State of',
					),
				26 =>
					array (
						'value' => 'BQ',
						'title' => 'Bonaire, Sint Eustatius and Saba',
					),
				27 =>
					array (
						'value' => 'BA',
						'title' => 'Bosnia and Herzegovina',
					),
				28 =>
					array (
						'value' => 'BW',
						'title' => 'Botswana',
					),
				29 =>
					array (
						'value' => 'BV',
						'title' => 'Bouvet Island',
					),
				30 =>
					array (
						'value' => 'BR',
						'title' => 'Brazil',
					),
				31 =>
					array (
						'value' => 'IO',
						'title' => 'British Indian Ocean Territory',
					),
				32 =>
					array (
						'value' => 'BN',
						'title' => 'Brunei Darussalam',
					),
				33 =>
					array (
						'value' => 'BG',
						'title' => 'Bulgaria',
					),
				34 =>
					array (
						'value' => 'BF',
						'title' => 'Burkina Faso',
					),
				35 =>
					array (
						'value' => 'BI',
						'title' => 'Burundi',
					),
				36 =>
					array (
						'value' => 'KH',
						'title' => 'Cambodia',
					),
				37 =>
					array (
						'value' => 'CM',
						'title' => 'Cameroon',
					),
				38 =>
					array (
						'value' => 'CA',
						'title' => 'Canada',
					),
				39 =>
					array (
						'value' => 'CV',
						'title' => 'Cape Verde',
					),
				40 =>
					array (
						'value' => 'KY',
						'title' => 'Cayman Islands',
					),
				41 =>
					array (
						'value' => 'CF',
						'title' => 'Central African Republic',
					),
				42 =>
					array (
						'value' => 'TD',
						'title' => 'Chad',
					),
				43 =>
					array (
						'value' => 'CL',
						'title' => 'Chile',
					),
				44 =>
					array (
						'value' => 'CN',
						'title' => 'China',
					),
				45 =>
					array (
						'value' => 'CX',
						'title' => 'Christmas Island',
					),
				46 =>
					array (
						'value' => 'CC',
						'title' => 'Cocos (Keeling) Islands',
					),
				47 =>
					array (
						'value' => 'CO',
						'title' => 'Colombia',
					),
				48 =>
					array (
						'value' => 'KM',
						'title' => 'Comoros',
					),
				49 =>
					array (
						'value' => 'CG',
						'title' => 'Congo',
					),
				50 =>
					array (
						'value' => 'CD',
						'title' => 'Congo, the Democratic Republic of the',
					),
				51 =>
					array (
						'value' => 'CK',
						'title' => 'Cook Islands',
					),
				52 =>
					array (
						'value' => 'CR',
						'title' => 'Costa Rica',
					),
				53 =>
					array (
						'value' => 'CI',
						'title' => 'Cote d\'Ivoire',
					),
				54 =>
					array (
						'value' => 'HR',
						'title' => 'Croatia',
					),
				55 =>
					array (
						'value' => 'CU',
						'title' => 'Cuba',
					),
				56 =>
					array (
						'value' => 'CW',
						'title' => 'Cura&ccedil;ao',
					),
				57 =>
					array (
						'value' => 'CY',
						'title' => 'Cyprus',
					),
				58 =>
					array (
						'value' => 'CZ',
						'title' => 'Czech Republic',
					),
				59 =>
					array (
						'value' => 'DK',
						'title' => 'Denmark',
					),
				60 =>
					array (
						'value' => 'DJ',
						'title' => 'Djibouti',
					),
				61 =>
					array (
						'value' => 'DM',
						'title' => 'Dominica',
					),
				62 =>
					array (
						'value' => 'DO',
						'title' => 'Dominican Republic',
					),
				63 =>
					array (
						'value' => 'EC',
						'title' => 'Ecuador',
					),
				64 =>
					array (
						'value' => 'EG',
						'title' => 'Egypt',
					),
				65 =>
					array (
						'value' => 'SV',
						'title' => 'El Salvador',
					),
				66 =>
					array (
						'value' => 'GQ',
						'title' => 'Equatorial Guinea',
					),
				67 =>
					array (
						'value' => 'ER',
						'title' => 'Eritrea',
					),
				68 =>
					array (
						'value' => 'EE',
						'title' => 'Estonia',
					),
				69 =>
					array (
						'value' => 'ET',
						'title' => 'Ethiopia',
					),
				70 =>
					array (
						'value' => 'FK',
						'title' => 'Falkland Islands (Malvinas)',
					),
				71 =>
					array (
						'value' => 'FO',
						'title' => 'Faroe Islands',
					),
				72 =>
					array (
						'value' => 'FJ',
						'title' => 'Fiji',
					),
				73 =>
					array (
						'value' => 'FI',
						'title' => 'Finland',
					),
				74 =>
					array (
						'value' => 'FR',
						'title' => 'France',
					),
				75 =>
					array (
						'value' => 'GF',
						'title' => 'French Guiana',
					),
				76 =>
					array (
						'value' => 'PF',
						'title' => 'French Polynesia',
					),
				77 =>
					array (
						'value' => 'TF',
						'title' => 'French Southern Territories',
					),
				78 =>
					array (
						'value' => 'GA',
						'title' => 'Gabon',
					),
				79 =>
					array (
						'value' => 'GM',
						'title' => 'Gambia',
					),
				80 =>
					array (
						'value' => 'GE',
						'title' => 'Georgia',
					),
				81 =>
					array (
						'value' => 'DE',
						'title' => 'Germany',
					),
				82 =>
					array (
						'value' => 'GH',
						'title' => 'Ghana',
					),
				83 =>
					array (
						'value' => 'GI',
						'title' => 'Gibraltar',
					),
				84 =>
					array (
						'value' => 'GR',
						'title' => 'Greece',
					),
				85 =>
					array (
						'value' => 'GL',
						'title' => 'Greenland',
					),
				86 =>
					array (
						'value' => 'GD',
						'title' => 'Grenada',
					),
				87 =>
					array (
						'value' => 'GP',
						'title' => 'Guadeloupe',
					),
				88 =>
					array (
						'value' => 'GT',
						'title' => 'Guatemala',
					),
				89 =>
					array (
						'value' => 'GG',
						'title' => 'Guernsey',
					),
				90 =>
					array (
						'value' => 'GN',
						'title' => 'Guinea',
					),
				91 =>
					array (
						'value' => 'GW',
						'title' => 'Guinea-Bissau',
					),
				92 =>
					array (
						'value' => 'GY',
						'title' => 'Guyana',
					),
				93 =>
					array (
						'value' => 'HT',
						'title' => 'Haiti',
					),
				94 =>
					array (
						'value' => 'HM',
						'title' => 'Heard Island and McDonald Islands',
					),
				95 =>
					array (
						'value' => 'VA',
						'title' => 'Holy See (Vatican City State)',
					),
				96 =>
					array (
						'value' => 'HN',
						'title' => 'Honduras',
					),
				97 =>
					array (
						'value' => 'HK',
						'title' => 'Hong Kong',
					),
				98 =>
					array (
						'value' => 'HU',
						'title' => 'Hungary',
					),
				99 =>
					array (
						'value' => 'IS',
						'title' => 'Iceland',
					),
				100 =>
					array (
						'value' => 'IN',
						'title' => 'India',
					),
				101 =>
					array (
						'value' => 'ID',
						'title' => 'Indonesia',
					),
				102 =>
					array (
						'value' => 'IR',
						'title' => 'Iran, Islamic Republic of',
					),
				103 =>
					array (
						'value' => 'IQ',
						'title' => 'Iraq',
					),
				104 =>
					array (
						'value' => 'IE',
						'title' => 'Ireland',
					),
				105 =>
					array (
						'value' => 'IM',
						'title' => 'Isle of Man',
					),
				106 =>
					array (
						'value' => 'IL',
						'title' => 'Israel',
					),
				107 =>
					array (
						'value' => 'IT',
						'title' => 'Italy',
					),
				108 =>
					array (
						'value' => 'JM',
						'title' => 'Jamaica',
					),
				109 =>
					array (
						'value' => 'JP',
						'title' => 'Japan',
					),
				110 =>
					array (
						'value' => 'JE',
						'title' => 'Jersey',
					),
				111 =>
					array (
						'value' => 'JO',
						'title' => 'Jordan',
					),
				112 =>
					array (
						'value' => 'KZ',
						'title' => 'Kazakhstan',
					),
				113 =>
					array (
						'value' => 'KE',
						'title' => 'Kenya',
					),
				114 =>
					array (
						'value' => 'KI',
						'title' => 'Kiribati',
					),
				115 =>
					array (
						'value' => 'KP',
						'title' => 'Korea, Democratic People\'s Republic of',
					),
				116 =>
					array (
						'value' => 'KR',
						'title' => 'Korea, Republic of',
					),
				117 =>
					array (
						'value' => 'KW',
						'title' => 'Kuwait',
					),
				118 =>
					array (
						'value' => 'KG',
						'title' => 'Kyrgyzstan',
					),
				119 =>
					array (
						'value' => 'LA',
						'title' => 'Lao People\'s Democratic Republic',
					),
				120 =>
					array (
						'value' => 'LV',
						'title' => 'Latvia',
					),
				121 =>
					array (
						'value' => 'LB',
						'title' => 'Lebanon',
					),
				122 =>
					array (
						'value' => 'LS',
						'title' => 'Lesotho',
					),
				123 =>
					array (
						'value' => 'LR',
						'title' => 'Liberia',
					),
				124 =>
					array (
						'value' => 'LY',
						'title' => 'Libya',
					),
				125 =>
					array (
						'value' => 'LI',
						'title' => 'Liechtenstein',
					),
				126 =>
					array (
						'value' => 'LT',
						'title' => 'Lithuania',
					),
				127 =>
					array (
						'value' => 'LU',
						'title' => 'Luxembourg',
					),
				128 =>
					array (
						'value' => 'MO',
						'title' => 'Macao',
					),
				129 =>
					array (
						'value' => 'MK',
						'title' => 'Macedonia, the former Yugoslav Republic of',
					),
				130 =>
					array (
						'value' => 'MG',
						'title' => 'Madagascar',
					),
				131 =>
					array (
						'value' => 'MW',
						'title' => 'Malawi',
					),
				132 =>
					array (
						'value' => 'MY',
						'title' => 'Malaysia',
					),
				133 =>
					array (
						'value' => 'MV',
						'title' => 'Maldives',
					),
				134 =>
					array (
						'value' => 'ML',
						'title' => 'Mali',
					),
				135 =>
					array (
						'value' => 'MT',
						'title' => 'Malta',
					),
				136 =>
					array (
						'value' => 'MQ',
						'title' => 'Martinique',
					),
				137 =>
					array (
						'value' => 'MR',
						'title' => 'Mauritania',
					),
				138 =>
					array (
						'value' => 'MU',
						'title' => 'Mauritius',
					),
				139 =>
					array (
						'value' => 'YT',
						'title' => 'Mayotte',
					),
				140 =>
					array (
						'value' => 'MX',
						'title' => 'Mexico',
					),
				141 =>
					array (
						'value' => 'MD',
						'title' => 'Moldova, Republic of',
					),
				142 =>
					array (
						'value' => 'MC',
						'title' => 'Monaco',
					),
				143 =>
					array (
						'value' => 'MN',
						'title' => 'Mongolia',
					),
				144 =>
					array (
						'value' => 'ME',
						'title' => 'Montenegro',
					),
				145 =>
					array (
						'value' => 'MS',
						'title' => 'Montserrat',
					),
				146 =>
					array (
						'value' => 'MA',
						'title' => 'Morocco',
					),
				147 =>
					array (
						'value' => 'MZ',
						'title' => 'Mozambique',
					),
				148 =>
					array (
						'value' => 'MM',
						'title' => 'Myanmar',
					),
				149 =>
					array (
						'value' => 'NA',
						'title' => 'Namibia',
					),
				150 =>
					array (
						'value' => 'NR',
						'title' => 'Nauru',
					),
				151 =>
					array (
						'value' => 'NP',
						'title' => 'Nepal',
					),
				152 =>
					array (
						'value' => 'NL',
						'title' => 'Netherlands',
					),
				153 =>
					array (
						'value' => 'NC',
						'title' => 'New Caledonia',
					),
				154 =>
					array (
						'value' => 'NZ',
						'title' => 'New Zealand',
					),
				155 =>
					array (
						'value' => 'NI',
						'title' => 'Nicaragua',
					),
				156 =>
					array (
						'value' => 'NE',
						'title' => 'Niger',
					),
				157 =>
					array (
						'value' => 'NG',
						'title' => 'Nigeria',
					),
				158 =>
					array (
						'value' => 'NU',
						'title' => 'Niue',
					),
				159 =>
					array (
						'value' => 'NF',
						'title' => 'Norfolk Island',
					),
				160 =>
					array (
						'value' => 'NO',
						'title' => 'Norway',
					),
				161 =>
					array (
						'value' => 'OM',
						'title' => 'Oman',
					),
				162 =>
					array (
						'value' => 'PK',
						'title' => 'Pakistan',
					),
				163 =>
					array (
						'value' => 'PS',
						'title' => 'Palestine',
					),
				164 =>
					array (
						'value' => 'PA',
						'title' => 'Panama',
					),
				165 =>
					array (
						'value' => 'PG',
						'title' => 'Papua New Guinea',
					),
				166 =>
					array (
						'value' => 'PY',
						'title' => 'Paraguay',
					),
				167 =>
					array (
						'value' => 'PE',
						'title' => 'Peru',
					),
				168 =>
					array (
						'value' => 'PH',
						'title' => 'Philippines',
					),
				169 =>
					array (
						'value' => 'PN',
						'title' => 'Pitcairn',
					),
				170 =>
					array (
						'value' => 'PL',
						'title' => 'Poland',
					),
				171 =>
					array (
						'value' => 'PT',
						'title' => 'Portugal',
					),
				172 =>
					array (
						'value' => 'QA',
						'title' => 'Qatar',
					),
				173 =>
					array (
						'value' => 'RE',
						'title' => 'Reunion',
					),
				174 =>
					array (
						'value' => 'RO',
						'title' => 'Romania',
					),
				175 =>
					array (
						'value' => 'RU',
						'title' => 'Russian Federation',
					),
				176 =>
					array (
						'value' => 'RW',
						'title' => 'Rwanda',
					),
				177 =>
					array (
						'value' => 'BL',
						'title' => 'Saint Barth&eacute;lemy',
					),
				178 =>
					array (
						'value' => 'SH',
						'title' => 'Saint Helena, Ascension and Tristan da Cunha',
					),
				179 =>
					array (
						'value' => 'KN',
						'title' => 'Saint Kitts and Nevis',
					),
				180 =>
					array (
						'value' => 'LC',
						'title' => 'Saint Lucia',
					),
				181 =>
					array (
						'value' => 'MF',
						'title' => 'Saint Martin (French part)',
					),
				182 =>
					array (
						'value' => 'PM',
						'title' => 'Saint Pierre and Miquelon',
					),
				183 =>
					array (
						'value' => 'VC',
						'title' => 'Saint Vincent and the Grenadines',
					),
				184 =>
					array (
						'value' => 'WS',
						'title' => 'Samoa',
					),
				185 =>
					array (
						'value' => 'SM',
						'title' => 'San Marino',
					),
				186 =>
					array (
						'value' => 'ST',
						'title' => 'Sao Tome and Principe',
					),
				187 =>
					array (
						'value' => 'SA',
						'title' => 'Saudi Arabia',
					),
				188 =>
					array (
						'value' => 'SN',
						'title' => 'Senegal',
					),
				189 =>
					array (
						'value' => 'RS',
						'title' => 'Serbia',
					),
				190 =>
					array (
						'value' => 'SC',
						'title' => 'Seychelles',
					),
				191 =>
					array (
						'value' => 'SL',
						'title' => 'Sierra Leone',
					),
				192 =>
					array (
						'value' => 'SG',
						'title' => 'Singapore',
					),
				193 =>
					array (
						'value' => 'SX',
						'title' => 'Sint Maarten (Dutch part)',
					),
				194 =>
					array (
						'value' => 'SK',
						'title' => 'Slovakia',
					),
				195 =>
					array (
						'value' => 'SI',
						'title' => 'Slovenia',
					),
				196 =>
					array (
						'value' => 'SB',
						'title' => 'Solomon Islands',
					),
				197 =>
					array (
						'value' => 'SO',
						'title' => 'Somalia',
					),
				198 =>
					array (
						'value' => 'ZA',
						'title' => 'South Africa',
					),
				199 =>
					array (
						'value' => 'GS',
						'title' => 'South Georgia and the South Sandwich Islands',
					),
				200 =>
					array (
						'value' => 'SS',
						'title' => 'South Sudan',
					),
				201 =>
					array (
						'value' => 'ES',
						'title' => 'Spain',
					),
				202 =>
					array (
						'value' => 'LK',
						'title' => 'Sri Lanka',
					),
				203 =>
					array (
						'value' => 'SD',
						'title' => 'Sudan',
					),
				204 =>
					array (
						'value' => 'SR',
						'title' => 'Suriname',
					),
				205 =>
					array (
						'value' => 'SJ',
						'title' => 'Svalbard and Jan Mayen',
					),
				206 =>
					array (
						'value' => 'SZ',
						'title' => 'Swaziland',
					),
				207 =>
					array (
						'value' => 'SE',
						'title' => 'Sweden',
					),
				208 =>
					array (
						'value' => 'CH',
						'title' => 'Switzerland',
					),
				209 =>
					array (
						'value' => 'SY',
						'title' => 'Syrian Arab Republic',
					),
				210 =>
					array (
						'value' => 'TW',
						'title' => 'Taiwan',
					),
				211 =>
					array (
						'value' => 'TJ',
						'title' => 'Tajikistan',
					),
				212 =>
					array (
						'value' => 'TZ',
						'title' => 'Tanzania, United Republic of',
					),
				213 =>
					array (
						'value' => 'TH',
						'title' => 'Thailand',
					),
				214 =>
					array (
						'value' => 'TL',
						'title' => 'Timor-Leste',
					),
				215 =>
					array (
						'value' => 'TG',
						'title' => 'Togo',
					),
				216 =>
					array (
						'value' => 'TK',
						'title' => 'Tokelau',
					),
				217 =>
					array (
						'value' => 'TO',
						'title' => 'Tonga',
					),
				218 =>
					array (
						'value' => 'TT',
						'title' => 'Trinidad and Tobago',
					),
				219 =>
					array (
						'value' => 'TN',
						'title' => 'Tunisia',
					),
				220 =>
					array (
						'value' => 'TR',
						'title' => 'Turkey',
					),
				221 =>
					array (
						'value' => 'TM',
						'title' => 'Turkmenistan',
					),
				222 =>
					array (
						'value' => 'TC',
						'title' => 'Turks and Caicos Islands',
					),
				223 =>
					array (
						'value' => 'TV',
						'title' => 'Tuvalu',
					),
				224 =>
					array (
						'value' => 'UG',
						'title' => 'Uganda',
					),
				225 =>
					array (
						'value' => 'UA',
						'title' => 'Ukraine',
					),
				226 =>
					array (
						'value' => 'AE',
						'title' => 'United Arab Emirates',
					),
				227 =>
					array (
						'value' => 'GB',
						'title' => 'United Kingdom',
					),
				228 =>
					array (
						'value' => 'US',
						'title' => 'United States',
					),
				229 =>
					array (
						'value' => 'UY',
						'title' => 'Uruguay',
					),
				230 =>
					array (
						'value' => 'UZ',
						'title' => 'Uzbekistan',
					),
				231 =>
					array (
						'value' => 'VU',
						'title' => 'Vanuatu',
					),
				232 =>
					array (
						'value' => 'VE',
						'title' => 'Venezuela, Bolivarian Republic of',
					),
				233 =>
					array (
						'value' => 'VN',
						'title' => 'Viet Nam',
					),
				234 =>
					array (
						'value' => 'VG',
						'title' => 'Virgin Islands, British',
					),
				235 =>
					array (
						'value' => 'WF',
						'title' => 'Wallis and Futuna',
					),
				236 =>
					array (
						'value' => 'EH',
						'title' => 'Western Sahara',
					),
				237 =>
					array (
						'value' => 'YE',
						'title' => 'Yemen',
					),
				238 =>
					array (
						'value' => 'ZM',
						'title' => 'Zambia',
					),
				239 =>
					array (
						'value' => 'ZW',
						'title' => 'Zimbabwe',
					),
			);
		}

		/**
		 * Fetches all Salesforce instances.
		 *
		 * This function will fetch and returns the list of Salesforce instances created on WFC Base for option select
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @return array
		 */
		public static function get_sfinstances_selectoption(){
			$instances_option = array();
			array_push($instances_option, array('title' => '--Select--', 'value' => ''));

			$WFCBASE_MultiInstanceSettings = get_option('WFCBASE_MultiInstanceSettings');
			$instances = isset($WFCBASE_MultiInstanceSettings['instances']) ? $WFCBASE_MultiInstanceSettings['instances'] : array();

			foreach($instances as $key => $value){
				$instance = get_option($value);
				$wfc_base_salesforceinstance_name = isset($instance['wfc_base_salesforceinstance_name']) ? $instance['wfc_base_salesforceinstance_name'] : '';
				$wfc_base_salesforceinstance_slug = isset($instance['wfc_base_salesforceinstance_slug']) ? $instance['wfc_base_salesforceinstance_slug'] : '';
				array_push($instances_option, array('title' => $wfc_base_salesforceinstance_name, 'value' => $wfc_base_salesforceinstance_slug));
			}
			return $instances_option;
		}

		/**
		 * Fetches all Salesforce instances.
		 *
		 * This function will fetch and returns the list of Salesforce instances created on WFC Base
		 *
		 * @author Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @return array
		 */
		public static function get_sfinstances(){
			$instances_option = array();
			$WFCBASE_MultiInstanceSettings = get_option('WFCBASE_MultiInstanceSettings');
			$instances = isset($WFCBASE_MultiInstanceSettings['instances']) ? $WFCBASE_MultiInstanceSettings['instances'] : array();

			foreach($instances as $key => $value){
				$instance = get_option($value);
				$wfc_base_salesforceinstance_name = isset($instance['wfc_base_salesforceinstance_name']) ? $instance['wfc_base_salesforceinstance_name'] : '';
				$wfc_base_salesforceinstance_slug = isset($instance['wfc_base_salesforceinstance_slug']) ? $instance['wfc_base_salesforceinstance_slug'] : '';
				array_push(
					$instances_option,
					array(
						'name' => $wfc_base_salesforceinstance_name,
						'slug' => $wfc_base_salesforceinstance_slug
					)
				);
			}
			return $instances_option;
		}

	}
}