<?php

/**
 * Class Webforce_Connect_Donation_i18n
 *
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 *
 * @since      1.0.0
 * @package    Webforce_Connect_Donation
 * @author     AlphaSys Pty. Ltd. <https://alphasys.com.au>
 */
class Webforce_Connect_Donation_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'webforce-connect-donation',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
