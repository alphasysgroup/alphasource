<?php

/**
 * Class Webforce_Connect_Donation
 *
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 *
 * @since      1.0.0
 * @package    Webforce_Connect_Donation
 * @author     AlphaSys Pty. Ltd. <https://alphasys.com.au>
 */
class Webforce_Connect_Donation {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Webforce_Connect_Donation_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'WFC_DONATION_VERSION' ) ) {
			$this->version = WFC_DONATION_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'webforce-connect-donation';

		/*
		 * Set constant path to the plugin directory.
		 *
		 * @since 1.0.0
		 */
		if (!defined('WFC_DONATION_DIR')) {
			define( 'WFC_DONATION_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Webforce_Connect_Donation_Loader. Orchestrates the hooks of the plugin.
	 * - Webforce_Connect_Donation_i18n. Defines internationalization functionality.
	 * - Webforce_Connect_Donation_Admin. Defines all hooks for the admin area.
	 * - Webforce_Connect_Donation_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies()
	{

		/*
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webforce-connect-donation-loader.php';

		/*
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webforce-connect-donation-i18n.php';
		
		/*
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-webforce-connect-donation-admin.php';
		
		/*
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-webforce-connect-donation-public.php';


		/*
		* Load table list
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-webforce-connect-donation-table.php';

		/*
		* Load donation form
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-forms/wfc-donation-forms.php';

		/*
		* Load donation list
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-list/wfc-donation-list.php';

		/*
		* Load WFC_DonationEncryptDecrypt
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-encrypt-decrypt/wfc-encrypt-decrypt.php';

		/*
		* Load donation data handler
		*/			
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-inc/wfc-donation-data-handler.php';
		
		/*
		* Load donation field renderer
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-inc/wfc-donation-form-field-renderer.php';

		/*
		* Load donation payment gateway modules
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-inc/wfc-donation-payment-gateway.php';

		/*
		* Load donation payment gateway modules
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-form-settings/wfc-donation-form-settings.php';

		/*
		* Load Salesforce PPAPI Class
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-salesforce/wfc-donation-salesforce-PPAPI.php';

		/*
		* Load Social buttons class
		*/	
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-social-sharing/wfc-donation-social-sharing.php';
		
		$this->loader = new Webforce_Connect_Donation_Loader();

		/*
		 * Load PayPal payment gateway class
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-email/wfc-donation-email-handler.php';

		/*
		 * Load Custom Address Set (Address Autocomplete) class
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/wfc-donation-cas-address-autocomplete/wfc-donation-cas-address-autocomplete.php';


	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Webforce_Connect_Donation_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Webforce_Connect_Donation_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Registers all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Webforce_Connect_Donation_Admin( $this->get_plugin_name(), $this->get_version(), $this );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		/*
		* --------------- Add Donation Submenu----------------------------
		*/
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'admin_menu', 97 );

		/*
		* register post types
		*/	
		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'register' );
		$this->loader->add_filter( 'manage_edit-pdwp_donation_columns', $plugin_admin, 'wfc_donation_forms_headers' );
		$this->loader->add_action( 'manage_pdwp_donation_posts_custom_column', $plugin_admin, 'wfc_donation_forms_data', 10, 2 );

		/*
		* register metaboxes
		*/	
		$this->loader->add_action( 'add_meta_boxes', $plugin_admin, 'wfc_register_formsettings' );

		/*
		* register wfc_register_donationlogs
		*/	
		$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'wfc_don_register_donationlogs' );
		$this->loader->add_action( 'admin_init', $plugin_admin, 'wfc_don_register_donationlogs_meta_boxes' );

		/*
		* Fetch GAU and Campiagn ajax
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_fetchgaucamp_action',$plugin_admin, 'wfc_don_fetchgaucamp_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_fetchgaucamp_action',$plugin_admin, 'wfc_don_fetchgaucamp_callback');

		/*
		* Save settings ajax
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_savesettings_action',$plugin_admin, 'wfc_don_savesettings_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_savesettings_action',$plugin_admin, 'wfc_don_savesettings_callback');

		/*
		* Reset settings ajax
		*/
		$this->loader->add_action( 'wp_ajax_wfc_don_resetsettings_action',$plugin_admin, 'wfc_don_resetsettings_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_resetsettings_action',$plugin_admin, 'wfc_don_resetsettings_callback');

		/*
		* get salesforce instance ajax
		*/
		$this->loader->add_action( 'wp_ajax_wfc_don_getsfinstance_action',$plugin_admin, 'wfc_don_getsfinstance_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_getsfinstance_action',$plugin_admin, 'wfc_don_getsfinstance_callback');


		/*
		* get payment gateway ajax
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_getpaymntgate_action',$plugin_admin, 'wfc_don_getpaymntgate_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_getpaymntgate_action',$plugin_admin, 'wfc_don_getpaymntgate_callback');

		/*
		* Fetch payment gateway ajax
		*/
		$this->loader->add_action( 'wp_ajax_wfc_don_syncpaymntgate_action',$plugin_admin, 'wfc_don_syncpaymntgate_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_syncpaymntgate_action',$plugin_admin, 'wfc_don_syncpaymntgate_callback');

		$this->loader->add_action( 'admin_post_custom_action_hook',$plugin_admin,'the_action_hook_callback');

		/*
		* hook for saving form settings
		*/
		$this->loader->add_action( 'save_post',$plugin_admin,'wfc_save_form_settings');

		/*
		* resync campaign
		*/
		$this->loader->add_action( 'wp_ajax_wfc_resync_campaign', $plugin_admin, 'wfc_resync_campaign' );
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_resync_campaign', $plugin_admin, 'wfc_resync_campaign' );

		/*
		* resync gau
		*/
		$this->loader->add_action( 'wp_ajax_wfc_resync_gau', $plugin_admin, 'wfc_resync_gau' );
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_resync_gau', $plugin_admin, 'wfc_resync_gau' );
		
		/*
		* cron sync of gau and campaign
		*/
		$this->loader->add_action( 'wfc_don_cron_resyncgaucamp', $plugin_admin, 'wfc_don_cronresyncgaucamp' );

		/*
		* donation form fields reset
		*/
		$this->loader->add_action( 'wp_ajax_donation_form_fields_reset', $plugin_admin, 'donation_form_fields_reset' );
		$this->loader->add_action( 'wp_ajax_nopriv_donation_form_fields_reset', $plugin_admin, 'donation_form_fields_reset' );

		/*
		* resync failed donation ajax
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_resyncdonation_action',$plugin_admin, 'wfc_don_resyncdonation_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_resyncdonation_action',$plugin_admin, 'wfc_don_resyncdonation_callback');

		/*
		* resync advance cart donation ajax
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_resyncadvancecart_action',$plugin_admin, 'wfc_don_resyncadvancecartdonation_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_resyncadvancecart_action',$plugin_admin, 'wfc_don_resyncadvancecartdonation_callback');

		/*
		* set payment transaction failed donation ajax
		*/
		$this->loader->add_action( 'wp_ajax_wfc_don_setpaymenttransaction_action',$plugin_admin, 'wfc_don_setpaymenttransaction_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_setpaymenttransaction_action',$plugin_admin, 'wfc_don_setpaymenttransaction_callback');


		/*
		* link failed donation ajax
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_linkdonation_action',$plugin_admin, 'wfc_don_linkdonation_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_linkdonation_action',$plugin_admin, 'wfc_don_linkdonation_callback');

		/*
		* exclude from cron sync
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_don_excludefromcronsync_action',$plugin_admin, 'wfc_don_excludefromcronsync_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_don_excludefromcronsync_action',$plugin_admin, 'wfc_don_excludefromcronsync_callback');

		/*
		* ajax callback for testing thankyou email
		*/
		$this->loader->add_action( 'wp_ajax_wfc_donation_donor_thankyou_email',$plugin_admin, 'wfc_donation_donor_thankyou_email');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_donation_donor_thankyou_email',$plugin_admin, 'wfc_donation_donor_thankyou_email');

		/*
		* ajax callback for testing admin notification
		*/
		$this->loader->add_action( 'wp_ajax_wfc_donation_admin_notification_test',$plugin_admin, 'wfc_donation_admin_notification_test');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_donation_admin_notification_test',$plugin_admin, 'wfc_donation_admin_notification_test');

		/*
		* cron sync donation function
		*/
		//$this->loader->add_action( 'cron_schedules', $plugin_admin, 'wfc_don_10min_cron' );
		$this->loader->add_action( 'wfc_don_cron_resyncdonation', $plugin_admin, 'wfc_don_cronresyncdonation' );
		$this->loader->add_action( 'wfc_don_event_resyncdonation', $plugin_admin, 'wfc_don_eventresyncdonation' );
		$this->loader->add_action( 'wfc_don_event_purgedonation', $plugin_admin, 'wfc_don_purgedonationrecord' );

		/*
		* cron sync advance cart donation function
		*/
		$this->loader->add_action( 'wfc_don_event_syncadvancecartdonation', $plugin_admin, 'wfc_don_syncadvancecartdonation' );
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Webforce_Connect_Donation_Public( $this->get_plugin_name(), $this->get_version(), $this );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$this->loader->add_shortcode( 'pronto-donation-form', $plugin_public, 'wfc_donation_form' );

		/*
		* clean and validation data ajax callback 
		*/	
		$this->loader->add_action( 'wp_ajax_clean_validate_donor', $plugin_public, 'wfc_clean_and_validate' );
		$this->loader->add_action( 'wp_ajax_nopriv_clean_validate_donor', $plugin_public, 'wfc_clean_and_validate' );

		/*
		* update donation fdata ajax after payment transaction
		*/			
		$this->loader->add_action( 'wp_ajax_wfc_donation_data', $plugin_public, 'wfc_donation_data' );
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_donation_data', $plugin_public, 'wfc_donation_data' );

		/*
		* reload page and display payment transaction error
		*/			
		$this->loader->add_action( 'wp_ajax_wfc_reset_form', $plugin_public, 'wfc_reset_form' );
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_reset_form', $plugin_public, 'wfc_reset_form' );

		/*
		* sync donation data to salesforce after payment transaction
		*/			
		$this->loader->add_action( 'wp_ajax_sync_donation_data', $plugin_public, 'wfc_sync_donation_data' );
		$this->loader->add_action( 'wp_ajax_nopriv_sync_donation_data', $plugin_public, 'wfc_sync_donation_data' );

		/*
		* get gateway config
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_gateway_config', $plugin_public, 'wfc_donation_get_paymentconfig' );
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_gateway_config', $plugin_public, 'wfc_donation_get_paymentconfig' );

		/**
		* sync event single donation to salesforce (Background process)
		*/	
		$this->loader->add_action( 'wfc_donation_syncsingledonation',$plugin_public,'wfc_sync_event_singledonation', 10, 3 );

		/*
		* sync event recurring donation to salesforce (Background process)
		*/	
		$this->loader->add_action( 'wfc_donation_syncrecurringdonation',$plugin_public,'wfc_sync_event_recurringdonation', 10, 3 );
		
		/*
		* complete donation
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_complete_donation', $plugin_public, 'wfc_complete_donation' );
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_complete_donation', $plugin_public, 'wfc_complete_donation' );

		/*
		* thank you page
		*/	
		$this->loader->add_filter( 'the_title', $plugin_public, 'wfc_donation_thankyou_override_title', 99 );
		$this->loader->add_filter( 'the_content', $plugin_public, 'wfc_donation_thankyoucontent_override', 99 );

		$this->loader->add_action( 'admin_head', $plugin_public, 'wfc_donation_inserttothankyoupageheader' );
		$this->loader->add_action( 'wp_head', $plugin_public, 'wfc_donation_inserttothankyoupageheader' );
	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Webforce_Connect_Donation_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
	
	/**
	 * Updates/Deletes donation data after gateway payment transaction.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $param
	 * @param string $action Accepted values:
	 *                       -update
	 *                       -delete
	 *
	 * @return bool|int
	 *
	 * @LastUpdated  May 3, 2018
	 */
    public function wfc_donation_data( $param, $action ) {
    	global $wpdb;

		switch ( $action ) {
			case 'delete':
				$query = $wpdb->prepare(
					"DELETE FROM {$wpdb->prefix}postmeta WHERE meta_id = %d AND meta_key = 'pronto_donation_donor' ", 
					$param
				);
			break;

			case 'update':
				$query = $wpdb->prepare(
					"UPDATE {$wpdb->prefix}postmeta SET meta_value = %s WHERE meta_id = %d AND meta_key = 'pronto_donation_donor' ", 
					$param
				);
			break;
		}
		
 		return $wpdb->query( $query );
	}
	
	/**
	 * Deletes donation data base on post meta id.
	 *
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $meta_id
	 *
	 * @return bool|int
	 *
	 * @LastUpdated  May 2, 2019
	 */
    public function wfc_delete_donation_data( $meta_id ) {
    	$meta_id = (int)$meta_id;

    	global $wpdb;

	    $query = $wpdb->prepare(
		    "DELETE FROM {$wpdb->prefix}postmeta WHERE meta_id = %d AND meta_key = 'pronto_donation_donor' ",
		    $meta_id
	    );
    	return $wpdb->query( $query );
    }
	
	/**
	 * This function will get the field template return data.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param       $template
	 * @param array $params
	 *
	 * @return bool|false|string
	 *
	 * @LastUpdated  March 28, 2018
	 */
	public function pdwp_view_tmpl($template, $params = array()) {
		if (is_file($template)) {
			ob_start();
			extract($params);
			require( $template );
			$var = ob_get_contents();
			ob_end_clean();
			return $var;
		}else{
			return false;
		}
	}
}