<?php

if ( !class_exists('Webforce_Connect_Plugin_Extend')) {
	/**
	 * Class Webforce_Connect_Plugin_Extend
	 */
	class Webforce_Connect_Plugin_Extend {
		
		private $base;
		private $plugin_prereq;
		private $plugin_prereq_to_notice = array();
		private $plugin_prereq_message;
		private $is_plugins_active = false;
		
		/**
		 * WFC_SyncerACFAddon_PluginPrereq constructor.
		 * @param string            $base           Plugin base path
		 * @param array             $plugin_prereq  Plugin dependencies
		 */
		public function __construct($base, $plugin_prereq) {
			$this->base = $base;
			$this->plugin_prereq = $plugin_prereq;
			
			add_action('admin_init', array($this, 'check_plugin_prereq'));
		}
		
		/**
		 * Hook that checks plugin dependencies
		 */
		public function check_plugin_prereq() {
			
			$this->is_plugins_active = false;
			
			foreach ($this->plugin_prereq as $path => $plugin) {
				$plugin_path = plugin_dir_path(dirname(__FILE__)) . '../' . $path;
				$plugin_version = get_plugin_data($plugin_path)['Version'];
				
				if (! is_plugin_active($path) || version_compare($plugin_version, $plugin['version']) < 0) {
					array_push($this->plugin_prereq_to_notice, $plugin);
				}
				
				$this->is_plugins_active |= ! is_plugin_active($path);
			}
			
			if (is_admin() && current_user_can('activate_plugins') && $this->is_plugins_active) {
				
				$this->plugin_prereq_message = $this->generate_plugin_prereq_notice();
				
				add_action('admin_notices', array($this, 'plugin_prereq_notice'));
				add_action('after_plugin_row_' . $this->base, array($this, 'display_row_message'));
				
				deactivate_plugins($this->base);
				
				if (isset($_GET['activate'])) {
					unset($_GET['activate']);
				}
			}
		}
		
		/**
		 * Hook that displays Admin notice
		 */
		public function plugin_prereq_notice() {
			?>
			<div class="error"><p>This plugin requires <strong><?php echo $this->plugin_prereq_message; ?></strong> to make it work.</p></div>
			<?php
		}
		
		/**
		 * Hook that displays message below the admin in the row
		 */
		public function display_row_message() {
			if ($this->is_plugins_active) {
				printf(
					'</tr><tr class="plugin-update-tr"><td colspan="3" class="plugin-update colspanchange"><div class="update-message notice inline notice-alt updated-message notice-error"><p  class="update-message" style="background-color: #ffebe8;"><strong>%s</strong></p></div></td>',
					'This plugin requires ' . $this->plugin_prereq_message . ' to make it work properly.'
				);
			}
		}
		
		/**
		 * Generates plugin prerequisites notice
		 *
		 * @return string Plugin prerequisites notice
		 */
		private function generate_plugin_prereq_notice() {
			$notice = array();
			
			foreach ($this->plugin_prereq_to_notice as $plugin) {
				array_push($notice, "{$plugin['name']} <small>( ver. {$plugin['version']} )</small>");
			}
			
			$notice[count($notice) - 1] =
				(count($notice) > 1)
					? 'and ' . $notice[count($notice) - 1]
					: $notice[count($notice) - 1];
			
			return implode(count($notice) > 2 ? ', ' : ' ', $notice);
		}
	}
}