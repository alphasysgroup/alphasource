<?php
/**
 * Class WFC_DonationList
 * This class handles out the field to be renderer for donation form.
 *
 * @author Junjie Canonio <junjie@alphasys.com.au>
 *
 * @since  1.0.0
 */
class WFC_DonationList{


	public function __construct() {


	}
	
	/**
	 * Fetches all donations on WordPress based on search filter.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param array $filter
	 * @param array $exta_con
	 *
	 * @return array|null|object
	 *
	 * @LastUpdated  June 14, 2018
	 */
	public function wfc_get_donations( $filter = array() , $exta_con = array() ) {
		global $wpdb;

		$search = null;
		if( !empty( $filter ) ) {
			foreach ($filter as $k => $v) {
				$search .= " AND {$v['key']} {$v['operator']} {$v['value']} ORDER BY meta_id DESC";
			}
		}

		$query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'pronto_donation_donor' {$search}";

		return $wpdb->get_results( $query, ARRAY_A );
	}

	/**
	 * Fetches all donation list on WordPress based on filters.
	 *
	 * @LastUpdated  April 27, 2018
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param array, array
	 *
	 * @return array
	 */
	public function wfc_get_donations_list( $filter = array() , $exta_con = array() ) {
		global $wpdb;

		$search = null;
		$limit_per_page = isset( $exta_con['limit'] ) ? 'LIMIT '.$exta_con['limit'] : '';
		$paged			= isset( $exta_con['paged'] ) ? $exta_con['paged'] : 1;
		$offset_page    = isset( $exta_con['offset'] ) ? $exta_con['offset'] : 10 ;

		$offset         = ($paged - 1) * $offset_page;
		$offset 		= 'OFFSET '.$offset;
 

		if( !empty( $filter ) ) {
			foreach ($filter as $k => $v) {
				$search .= " AND {$v['key']} {$v['operator']} {$v['value']} ORDER BY meta_id DESC {$limit_per_page} {$offset}";
			}
		}

		$query = "SELECT * FROM {$wpdb->prefix}postmeta WHERE meta_key = 'pronto_donation_donor' {$search}";

		return $wpdb->get_results( $query, ARRAY_A );
	}
	
	/**
	 * Get donation list count on wordpress.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param array $filter
	 *
	 * @return null|string
	 *
	 * @LastUpdated  April 27, 2018
	 */
	public function wfc_get_donations_list_count( $filter = array() ){
		global  $wpdb;

		$search = null;
		if( !empty( $filter ) ) {
			foreach ($filter as $k => $v) {
				$search .= " AND {$v['key']} {$v['operator']} {$v['value']} ORDER BY meta_id DESC ";
			}
		}

    	$rowcount = $wpdb->get_var("SELECT COUNT(*) FROM {$wpdb->prefix}postmeta WHERE meta_key = 'pronto_donation_donor' {$search}");
    	return $rowcount;
	}
	
	/**
	 * Fetches Donations donor list.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $donations
	 *
	 * @return array
	 *
	 * @LastUpdated  April 27, 2018
	 */
	public function wfc_donation_donorList( $donations ) {
		
		if( $donations !== false ) {
			$donation_data = array();
			foreach ( $donations as $k => $donors ) {

				$donor = unserialize( $donors['meta_value'] );

				/*
				* identify payment gateway
				*/
				if( isset( $donor['payment_gateway'] ) ) {
					$pdwp_payment = explode('_', $donor['payment_gateway']);
					// $payment = ucfirst( $pdwp_payment[0] );
					$gateway_key	= count( $pdwp_payment ) - 2;
					$payment 		= isset( $pdwp_payment[ $gateway_key ] ) ? $pdwp_payment[ $gateway_key ] : ucfirst( $pdwp_payment[0] );
                 
				}elseif( isset( $donor['payment'] ) ) {
					$payment = isset($donor['payment']) ? $donor['payment'] : '';
				} else {
					$payment = '';
				}


				/*
				* Donation colomn
				*/
				if(isset($donor['donor_first_name']) && 
					isset($donor['donor_last_name']) && 
					isset($donor['donor_email'])){
					if(isset($donor['donation_version']) && $donor['donation_version'] == 'v5') {
						$data_version = 'v5';
					}else{
						if(isset($donor['SalesforceSyncHistory'])) {
							$data_version = 'v5';
						}else{
							$data_version = 'v4';
						}
					}
					$donation = '<div class="pdwp-donation-title"><a href="'.admin_url( 'admin.php' ).'?page=donation-list-page&action=view&ID='.$donors['meta_id'].'">#'.$donors['meta_id'].'</a> by <a href="#">'.$donor['donor_first_name'].' '.$donor['donor_last_name'].'</a><br><a href="#">'.$donor['donor_email'].'</a></div>';					
				}elseif(isset($donor['user_firstname_option']) && 
					isset($donor['user_lastname_option']) && 
					isset($donor['user_email_option'])){
					$data_version = 'v2';
					$donation = '<div class="pdwp-donation-title"><a href="'.admin_url( 'admin.php' ).'?page=donation-list-page&action=view&ID='.$donors['meta_id'].'">#'.$donors['meta_id'].'</a> by <a href="#">'.$donor['user_firstname_option'].' '.$donor['user_lastname_option'].'</a><br><a href="#">'.$donor['user_email_option'].'</a></div>';
				}elseif(isset($donor['first_name']) && 
					isset($donor['last_name']) && 
					isset($donor['email'])){
					$data_version = 'v1';
					$donation = '<div class="pdwp-donation-title"><a href="'.admin_url( 'admin.php' ).'?page=donation-list-page&action=view&ID='.$donors['meta_id'].'">#'.$donors['meta_id'].'</a> by <a href="#">'.$donor['first_name'].' '.$donor['last_name'].'</a><br><a href="#">'.$donor['email'].'</a></div>';
				}else{
					$donation = '';
				}


				/*
				* Donation form colomn
				*/
				$donation_campaign = isset($donor['donation_campaign']) ? $donor['donation_campaign'] : '';
				$form_title = get_the_title($donation_campaign);
				$form_title = !empty($form_title) ? $form_title : 'Donation Form ID : '.$donation_campaign;
				$donation_form = '<div class="pdwp-donation-form"><a href="'.admin_url( 'post.php' ).'?post='.$donor['donation_campaign'].'&action=edit">'.$form_title.'</a></div>';


				/*
				* Transaction Status colomn
				*/
				if(isset($donor['wfc_carttype']) && $donor['wfc_carttype'] == 'advance_cart') {
					$cartItems = isset($donor['cartItems']) ? $donor['cartItems'] : array();
					$singlePaymentsItem = array();
					$recurringPaymentsItem = array();
					if(!empty($cartItems)) {
						foreach ($cartItems as $key => $value) {
							if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
								array_push($singlePaymentsItem,$value);
							}else{
								array_push($recurringPaymentsItem,$value);
							}
						}
					}

					if (sizeof($singlePaymentsItem) > 1) {
						$cartItems = $recurringPaymentsItem;
						array_push($cartItems,$singlePaymentsItem);
					}

					$donation_type = '';
					if (!empty($cartItems)) {
						foreach ($cartItems as $key => $value) {

							if (isset($value['wfc_donation_type'])) {
								$donation_type .= isset($value['wfc_donation_type']) ? '<div>'.$value['wfc_donation_type'].'</div>' : '-';							
							}else{
								if (is_array($value) && sizeof($value) > 1) {
									$donation_type .= 'one-off';
								}else{
									$donation_type .= '-';
								}
							}
						}
					}else{
						$donation_type = '-';
					}
				}else{
					if(isset($donor['pdwp_donation_type'])){
						$donation_type = isset( $donor['pdwp_donation_type'] ) ? ucwords( $donor['pdwp_donation_type'] ) : '-';
					}elseif(isset($donor['donation_type'])){
						$donation_type = isset( $donor['donation_type'] ) ? ucwords( $donor['donation_type'] ) : '-';
					}else{
						$donation_type = '-';
					}
				}


				/*
				* Donation type colomn
				*/
				if(isset($donor['statusText'])) {
					if(isset($donor['payment_type']) && $donor['payment_type'] == 'bank') {
						$transaction_status = '<div class="transaction-status-wrap"><span class="transaction-status-bank"></span> BANK</div>';
					}else{
						if( strtolower( $donor['statusText'] ) == 'transaction approved'){
							$transaction_status = '<div class="transaction-status-wrap"><span class="transaction-status-approved"></span> APPROVED</div>';
						}else{
							$transaction_status = '<div class="transaction-status-wrap"><span class="transaction-status-'.strtolower( $donor['statusText'] ).'"></span> '.$donor['statusText'] .'</div>';	
						}
					}
				} else {
					if(isset($donor['payment_type']) && $donor['payment_type'] == 'bank') {
						$transaction_status = '<div class="transaction-status-wrap"><span class="transaction-status-bank"></span> Bank</div>';
					}else{
						$transaction_status = '<div class="transaction-status-wrap"><span class="transaction-status-pending"></span> VOID</div>';
					}
				}
				

				/*
				* Sync Status colomn
				*/
				if($data_version == 'v3' || $data_version == 'v4' || $data_version == 'v5' ){

					if (isset($donor['wfc_carttype']) && $donor['wfc_carttype'] == 'advance_cart') {
						$donation_sync_status = '';
						$cartItems = isset($donor['cartItems']) ? $donor['cartItems'] : array(); 
						$singlePaymentsItem = array();
						$recurringPaymentsItem = array();
						if(!empty($cartItems)) {
							foreach ($cartItems as $key => $value) {
								if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
									array_push($singlePaymentsItem,$value);
								}else{
									array_push($recurringPaymentsItem,$value);
								}
							}
						}

						if (sizeof($singlePaymentsItem) > 1) {
							$cartItems = $recurringPaymentsItem;
							array_push($cartItems,$singlePaymentsItem);
						}


						foreach ($cartItems as $key => $value) {
							$sync_status = $this->wfc_get_sync_status_advancecart($donor,NULL,$key);
							$style = $this->wfc_get_sync_status_advancecart($donor,'style',$key);

							if( $sync_status['status'] == 'success_sync' ) {
								$sf_sync_status = "SUCCESS";
							}elseif( $sync_status['status'] == 'failed_sync' ) {
								$sf_sync_status = "FAILED"; 
							}elseif( $sync_status['status'] == 'recurring_failed_sync' ) {
								$sf_sync_status = "Recurring Sync Failed";
							}elseif( $sync_status['status'] == 'partial_sync' ) {
								$sf_sync_status = "Recurring Partial Sync";
							} elseif( $sync_status['status'] == 'attemp_reach' ) {
								$sf_sync_status = "LIMIT REACHED";
							} else {
								if(isset($value['wfc_donation_type']) && $value['wfc_donation_type'] != 'one-off'){
									$sf_sync_status = "Recurring Failed Sync";
								}else{
									$sf_sync_status = "UN-SYNCED";			
								}	
							}

							$donation_sync_status .= '<div class="wfc-advance-cart donation-'.$style.'">'.$sf_sync_status.'</div>';
						}
					}else{
						$sync_status = $this->wfc_get_sync_status( $donor );
						$style = $this->wfc_get_sync_status( $donor, 'style' );

						if( $sync_status['status'] == 'success_sync' ) {
							$sf_sync_status = "SUCCESS";
						}elseif( $sync_status['status'] == 'failed_sync' ) {
							$sf_sync_status = "FAILED"; 
						}elseif( $sync_status['status'] == 'recurring_failed_sync' ) {
							$sf_sync_status = "Recurring Sync Failed";
						}elseif( $sync_status['status'] == 'partial_sync' ) {
							$sf_sync_status = "Recurring Partial Sync";
						} elseif( $sync_status['status'] == 'attemp_reach' ) {
							$sf_sync_status = "LIMIT REACHED";
						} else {
							if(isset($donor['pdwp_donation_type']) && $donor['pdwp_donation_type'] != 'one-off'){
								$sf_sync_status = "Recurring Failed Sync";
							}else{
								$sf_sync_status = "UN-SYNCED";			
							}	
						}

						$donation_sync_status = '<div class="donation-'.$style.'">'.$sf_sync_status.'</div>';
					}
				}elseif($data_version == 'v2'){
					$sync_status = $this->wfc_get_sync_statusv2( $donor );
					$style = $this->wfc_get_sync_statusv2( $donor, 'style' );

					if( $sync_status['status'] == 'success_sync' ) {
						$sf_sync_status = "SUCCESS";
					}elseif( $sync_status['status'] == 'old_cannot_sync' ) {
						$sf_sync_status = "Old Data Failed Sync";
					}else{
						$sf_sync_status = "UN-SYNC";			
					}

					$donation_sync_status = '<div class="donation-'.$style.'">'.$sf_sync_status.'</div>';
				}elseif($data_version == 'v1'){
					$sync_status = $this->wfc_get_sync_statusv1( $donor );
					$style = $this->wfc_get_sync_statusv1( $donor, 'style' );

					if( $sync_status['status'] == 'success_sync' ) {
						$sf_sync_status = "SUCCESS";
					}elseif( $sync_status['status'] == 'old_cannot_sync' ) {
						$sf_sync_status = "Old Data Failed Sync";
					}else{
						$sf_sync_status = "UN-SYNC";			
					}

					$donation_sync_status = '<div class="donation-'.$style.'">'.$sf_sync_status.'</div>';
				}else{
					$sync_status['status'] = '';
					$donation_sync_status = '<div class="donation-'.$style.'">'.$sf_sync_status.'</div>';
				}

				


				/*
				* Amount colomn
				*/
				if( isset( $donor['currency'] ) ) {
					$donor_currency = explode(',', $donor['currency']);
					$currency = $donor_currency[0];
				} else {
					$currency = '';
				}

				if(isset($donor['pdwp_custom_amount']) || isset($donor['pdwp_sub_amt'])){
					if( isset( $donor['pdwp_sub_amt'] ) && (int) $donor['pdwp_sub_amt'] > 0 ) {
						$amt = $currency . number_format( $donor['pdwp_sub_amt'], 2, '.', ',');
					} else {
						$custom_amt = isset( $donor['pdwp_custom_amount'] ) ? number_format( $donor['pdwp_custom_amount'], 2, '.', ',') : '0.00';
						$amt = $currency . $custom_amt;
					}
				}elseif(isset($donor['pd_amount'])){
					$custom_amt = isset( $donor['pd_amount'] ) ? number_format( $donor['pd_amount'], 2, '.', ',') : '0.00';
					if (!empty($currency)) {
						$amt = $currency . $custom_amt;
					}else{
						$CurrencyCode = isset($donor['CurrencyCode']) ? $donor['CurrencyCode'] : 'AUD';
						$amt = $CurrencyCode.' - '.$custom_amt;
					}				
				}else{
					$amt = '';
				}

				$payment = (strtolower($payment) == 'ech') ? 'Paydock': $payment;
				$amount = '<div class="donation-amt">'.$amt.'<br><small>via</small> <span class="payment-type pt-'.strtolower( $payment ).'">'.strtolower($payment).'</span></div>';


				/*
				* Date colomn
				*/
				//$date = ( isset( $donor['timestamp'] ) && !empty( $donor['timestamp'] ) ) ? date('M d, Y h:m:s', $donor['timestamp'] ) : '';
				$date = ( isset( $donor['timestamp'] ) && !empty( $donor['timestamp'] ) ) ? get_date_from_gmt( date( 'Y-m-d H:i:s', $donor['timestamp'] ), 'F j, Y H:i:s' ) : '';

				
				/*
				* version logo colomn
				*/			
				if($data_version == 'v1'){
					$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_1.png'.'"/>';				
				}elseif($data_version == 'v2'){	
					$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_2.png'.'"/>';		
				}elseif($data_version == 'v3'){	
					$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_3.png'.'"/>';		
				}elseif($data_version == 'v4'){	
					$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_4.png'.'"/>';		
				}else{
					if(isset($donor['wfc_carttype']) && $donor['wfc_carttype'] == 'advance_cart') {
						if(isset($donor['advance_cart_ver']) && $donor['advance_cart_ver'] == '2.0') {
							if (isset($donor['purge_donation']['purge']) && $donor['purge_donation']['purge'] == 'include') {
								$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_5_cart_2.0-purge.png'.'"/>';	
							}else {
								$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_5_cart_2.0.png'.'"/>';		
							}
						}else{
							if (isset($donor['purge_donation']['purge']) && $donor['purge_donation']['purge'] == 'include') {
								$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_5_cart-purge.png'.'"/>';
							}else {
								$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_5_cart.png'.'"/>';
							}	
						}
						
					}else{
						if (isset($donor['purge_donation']['purge']) && $donor['purge_donation']['purge'] == 'include') {
							$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_5-purge.png'.'"/>';	
						}else {
							$version_logo = '<img id="wfc_version_logo" style="width: 50px;" src="'.plugin_dir_url( __FILE__ ).'../../images/version_indicator/ribbon_5.png'.'"/>';	
						}								
					}
				}		

					
				$data = array(
					'ID' => $donors['meta_id'],
					'data_version' => $version_logo,
					'title' => str_replace( '\\','',$donation ),
					'donation_form' => $donation_form,
					'donation_type' => $donation_type,
					'transaction_status' => $transaction_status,
					'sync_status' => $donation_sync_status,
					'amount' => $amount,
					'date' => $date
				);
				$donation_data[] = $data;
			}
			return  $donation_data;
		} else {
			return array();
		}
	}
	
	/**
	 * Returns the Salesforce sync status.
	 *
	 * @author       Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param null $is_style
	 *
	 * @return array|string
	 *
	 * @LastUpdated  April 27, 2018
	 */
	public function wfc_get_sync_status( $post, $is_style = null ) {
        /*
        *  get salesforce sync status
        */
        if((isset($post['resync_attemp']) && $post['resync_attemp'] >= 5) && 
        	!isset($post['SalesforceResponse']['OpportunityId'])) {
            if(empty($is_style )) {
                return array(
                    'status' => 'attemp_reach',
                    'message' => 'Unable to resync donation, Re-sync attemp limit is already reach.'
                );
            } else {
                return 'attemp-reach';
            }
        }elseif(isset($post['SalesforceResponse']) && !empty($post['SalesforceResponse'])){

        	if(isset($post['pdwp_donation_type']) && $post['pdwp_donation_type'] == 'one-off'){

        		if((isset($post['SalesforceResponse']['OpportunityId']) 
        			&& !empty($post['SalesforceResponse']['OpportunityId'])) &&
        			(isset($post['SalesforceResponse']['status']) 
        			&& $post['SalesforceResponse']['status'] == 200)){

	                if( empty( $is_style ) ) {
	                    return array(
	                        'status' => 'success_sync',
	                        'message' => 'Donation successfully synced to salesforce.'
	                    );
	                } else {
	                    return 'success-sync';
	                }        				
        			
        		}else{
        			if( empty( $is_style ) ) {
	                    $err_msg = null;
	                    if( isset( $post['SalesforceResponse']['ErrorMessage'] ) ) {
	                        $err_msg = $post['SalesforceResponse']['ErrorMessage'];
	                    } else {
	                        $err_msg = isset( $post['SalesforceResponse']['message'] ) ? 
	                                            $post['SalesforceResponse']['message'] : null;
	                    }

	                    return array(
	                        'status' => 'failed_sync',
	                        'message' => $err_msg
	                    );
	                } else {
	                    return 'failed-sync';
	                }
        		}

        	}else{
        		if((isset($post['SalesforceResponse']['RecurringId']) 
        			&& !empty($post['SalesforceResponse']['RecurringId'])) &&
        			(isset($post['SalesforceResponse']['status']) 
        			&& $post['SalesforceResponse']['status'] == 200)){

        			if(isset($post['SalesforceLogs']['result']['single']['status']) &&
        				$post['SalesforceLogs']['result']['single']['status'] == 'Success'){

	        			if( empty( $is_style ) ) {
		                    return array(
		                        'status' => 'success_sync',
		                        'message' => 'Recurring Donation successfully synced to salesforce.'
		                    );
		                } else {
		                    return 'success-sync';
		                }
        			}else{
        				if(isset($post['payment_type']) && $post['payment_type'] == 'bank') {
        					if( empty( $is_style ) ) {
			                    return array(
			                        'status' => 'success_sync',
			                        'message' => 'Recurring Donation successfully synced to salesforce.'
			                    );
			                } else {
			                    return 'success-sync';
			                }
        				}else{
	        				if( empty( $is_style ) ) {
			                    return array(
			                        'status' => 'partial_sync',
			                        'message' => 'Recurring Donation successfully synced to salesforce but One-off Donation sync failed.'
			                    );
			                } else {
			                    return 'partial-sync';
			                }
        				}
        			}
        		}else{
        			if( empty( $is_style ) ) {
	                    $err_msg = null;
	                    if( isset( $post['SalesforceResponse']['ErrorMessage'] ) ) {
	                        $err_msg = $post['SalesforceResponse']['ErrorMessage'];
	                    } else {
	                        $err_msg = isset( $post['SalesforceResponse']['message'] ) ? 
	                                            $post['SalesforceResponse']['message'] : null;
	                    }

	                    return array(
	                        'status' => 'recurring_failed_sync',
	                        'message' => $err_msg
	                    );
	                } else {
	                    return 'recurring-failed-sync';
	                }
        		}
        	}

        } else {
            if( empty( $is_style ) ) {
                return array(
                    'status' => 'not_sync',
                    'message' => 'Donation is not sync to salesforce yet.'
                );
            } else {
	            return 'not-sync';           		
            }
        }
    }
	
	
	/**
	 * Fetches Salesforce sync status for advance cart.
	 *
	 * @author       Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param      $suffix
	 *
	 * @param null $is_style
	 *
	 * @return array|string
	 *
	 * @LastUpdated  July 23, 2018
	 */
	public function wfc_get_sync_status_advancecart( $post, $suffix, $is_style = null ) {
        /*
        *  get salesforce sync status
        */
        if((isset($post['resync_attemp']) && $post['resync_attemp'] >= 5) && 
        	!isset($post['SalesforceResponse'][$suffix]['OpportunityId'])) {
            if(empty($is_style )) {
                return array(
                    'status' => 'attemp_reach',
                    'message' => 'Unable to resync donation, Re-sync attemp limit is already reach.'
                );
            } else {
                return 'attemp-reach';
            }
        }elseif(isset($post['SalesforceResponse'][$suffix]) && !empty($post['SalesforceResponse'][$suffix])){
        	$cartItems = isset($post['cartItems']) ? $post['cartItems'] : array(); 
        	$singlePaymentsItem = array();
			$recurringPaymentsItem = array();
			if(!empty($cartItems)) {
				foreach ($cartItems as $key => $value) {
					if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
						array_push($singlePaymentsItem,$value);
					}else{
						array_push($recurringPaymentsItem,$value);
					}
				}
			}

			
			if (sizeof($singlePaymentsItem) > 1) {
				$cartItems = $recurringPaymentsItem;
				array_push($cartItems,$singlePaymentsItem);
			}
			foreach ($cartItems as $key => $value) {
				if (empty($value)) {
					unset($cartItems[$key]);
				}
			}


        	if(isset($cartItems[$suffix]['wfc_donation_type']) && $cartItems[$suffix]['wfc_donation_type'] == 'one-off'){

        		if((isset($post['SalesforceResponse'][$suffix]['OpportunityId']) 
        			&& !empty($post['SalesforceResponse'][$suffix]['OpportunityId'])) &&
        			(isset($post['SalesforceResponse'][$suffix]['status']) 
        			&& $post['SalesforceResponse'][$suffix]['status'] == 200)){

	                if( empty( $is_style ) ) {
	                    return array(
	                        'status' => 'success_sync',
	                        'message' => 'Donation successfully synced to salesforce.'
	                    );
	                } else {
	                    return 'success-sync';
	                }        				
        			
        		}else{
        			if( empty( $is_style ) ) {
	                    $err_msg = null;
	                    if( isset( $post['SalesforceResponse'][$suffix]['ErrorMessage'] ) ) {
	                        $err_msg = $post['SalesforceResponse'][$suffix]['ErrorMessage'];
	                    } else {
	                        $err_msg = isset( $post['SalesforceResponse'][$suffix]['message'] ) ? 
	                                            $post['SalesforceResponse'][$suffix]['message'] : null;
	                    }

	                    return array(
	                        'status' => 'failed_sync',
	                        'message' => $err_msg
	                    );
	                } else {
	                    return 'failed-sync';
	                }
        		}

        	}elseif (sizeof($singlePaymentsItem) > 1 && !isset($cartItems[$suffix]['wfc_donation_type'])){
        		if((isset($post['SalesforceResponse'][$suffix]['OpportunityId']) 
        			&& !empty($post['SalesforceResponse'][$suffix]['OpportunityId'])) &&
        			(isset($post['SalesforceResponse'][$suffix]['status']) 
        			&& $post['SalesforceResponse'][$suffix]['status'] == 200)){

	                if( empty( $is_style ) ) {
	                    return array(
	                        'status' => 'success_sync',
	                        'message' => 'Donation successfully synced to salesforce.'
	                    );
	                } else {
	                    return 'success-sync';
	                }        				
        			
        		}else{
        			if( empty( $is_style ) ) {
	                    $err_msg = null;
	                    if( isset( $post['SalesforceResponse'][$suffix]['ErrorMessage'] ) ) {
	                        $err_msg = $post['SalesforceResponse'][$suffix]['ErrorMessage'];
	                    } else {
	                        $err_msg = isset( $post['SalesforceResponse'][$suffix]['message'] ) ? 
	                                            $post['SalesforceResponse'][$suffix]['message'] : null;
	                    }

	                    return array(
	                        'status' => 'failed_sync',
	                        'message' => $err_msg
	                    );
	                } else {
	                    return 'failed-sync';
	                }
        		}
        	}else{
        		if((isset($post['SalesforceResponse'][$suffix]['RecurringId']) 
        			&& !empty($post['SalesforceResponse'][$suffix]['RecurringId'])) &&
        			(isset($post['SalesforceResponse'][$suffix]['status']) 
        			&& $post['SalesforceResponse'][$suffix]['status'] == 200)){

        			if(isset($post['SalesforceLogs'][$suffix]['result']['single']['status']) &&
        				$post['SalesforceLogs'][$suffix]['result']['single']['status'] == 'Success'){

	        			if( empty( $is_style ) ) {
		                    return array(
		                        'status' => 'success_sync',
		                        'message' => 'Recurring Donation successfully synced to salesforce.'
		                    );
		                } else {
		                    return 'success-sync';
		                }
        			}else{
        				if(isset($post['payment_type']) && $post['payment_type'] == 'bank') {
        					if( empty( $is_style ) ) {
			                    return array(
			                        'status' => 'success_sync',
			                        'message' => 'Recurring Donation successfully synced to salesforce.'
			                    );
			                } else {
			                    return 'success-sync';
			                }
        				}else{
	        				if( empty( $is_style ) ) {
			                    return array(
			                        'status' => 'partial_sync',
			                        'RecurringId' => $post['SalesforceResponse'][$suffix]['RecurringId'],
			                        'message' => 'Recurring Donation successfully synced to salesforce but One-off Donation sync failed.'
			                    );
			                } else {
			                    return 'partial-sync';
			                }
        				}
        			}
        		}else{
        			if( empty( $is_style ) ) {
	                    $err_msg = null;
	                    if( isset( $post['SalesforceResponse'][$suffix]['ErrorMessage'] ) ) {
	                        $err_msg = $post['SalesforceResponse'][$suffix]['ErrorMessage'];
	                    } else {
	                        $err_msg = isset( $post['SalesforceResponse'][$suffix]['message'] ) ? 
	                                            $post['SalesforceResponse'][$suffix]['message'] : null;
	                    }

	                    return array(
	                        'status' => 'recurring_failed_sync',
	                        'message' => $err_msg,
	                        'RecurringId' => isset($post['SalesforceResponse'][$suffix]['RecurringId']) ? $post['SalesforceResponse'][$suffix]['RecurringId'] : ''
	                    );
	                } else {
	                    return 'recurring-failed-sync';
	                }
        		}
        	}

        } else {
            if( empty( $is_style ) ) {
                return array(
                    'status' => 'not_sync',
                    'message' => 'Donation is not sync to salesforce yet.'
                );
            } else {
	            return 'not-sync';           		
            }
        }
    }
	
	
	/**
	 * Fetches Salesforce sync overall status for Advance Cart.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $donation
	 *
	 * @return string
	 *
	 * @LastUpdated  May 7, 2019
	 */
	public function wfc_get_sync_overallstatus_advancecart($donation) {
		$overallstatus_arr = array();
		$overallstatus = 'incomplete';
		$cartItems = isset($donation['cartItems']) ? $donation['cartItems'] : array();

		$singlePaymentsItem = array();
		$recurringPaymentsItem = array();
		if(!empty($cartItems)) {
			foreach ($cartItems as $key => $value) {
				if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
					array_push($singlePaymentsItem,$value);
				}else{
					array_push($recurringPaymentsItem,$value);
				}
			}
		}

		if (sizeof($singlePaymentsItem) > 1) {
			$cartItems = $recurringPaymentsItem;
			array_push($cartItems,$singlePaymentsItem);
		}
		foreach ($cartItems as $key => $value) {
			if (empty($value)) {
				unset($cartItems[$key]);
			}
		}

		if (!empty($cartItems)) {
			$donor_detials_syncbtn = array();
			foreach ($cartItems as $key => $value) {
				$wfc_get_sync_status = $this->wfc_get_sync_status_advancecart($donation,NULL,$key);
				$advancecart_status = isset($wfc_get_sync_status['status']) ? $wfc_get_sync_status['status'] : '';
				
				if( $advancecart_status == 'success_sync' ) {

					array_push($overallstatus_arr, 'complete');

				}elseif( $advancecart_status == 'link_sync' ) {

					array_push($overallstatus_arr, 'incomplete');

				}elseif( $advancecart_status == 'failed_sync' ) {

					array_push($overallstatus_arr, 'incomplete');

				}elseif( $advancecart_status == 'recurring_failed_sync' ) {

					array_push($overallstatus_arr, 'incomplete');

				}elseif( $advancecart_status == 'partial_sync' ) {

					array_push($overallstatus_arr, 'incomplete');

				} elseif( $advancecart_status == 'attemp_reach' ) {

					array_push($overallstatus_arr, 'incomplete');

				} else {

					array_push($overallstatus_arr, 'incomplete');

				}
			}
		}

		if (in_array("incomplete", $overallstatus_arr)) { 
			$overallstatus = 'incomplete';
		} else{ 
			$overallstatus = 'complete';
		} 
		
		return $overallstatus;
	}
	
	
	/**
	 * Fetches Salesforce sync status for donations made in v2.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param null $is_style
	 *
	 * @return array|string
	 *
	 * @LastUpdated  April 27, 2018
	 *
	 */
	public function wfc_get_sync_statusv2( $post, $is_style = null ) {
		/*
        *  get salesforce sync status
        */
        if((isset($post['opportunityStatus']) && $post['opportunityStatus'] == 200) &&
		(isset($post['opportunityId']) && !empty($post['opportunityId']))){
        	if( empty( $is_style ) ) {
                return array(
                    'status' => 'success_sync',
                    'message' => 'Recurring Donation successfully synced to salesforce.'
                );
            } else {
                return 'success-sync';
            }
        }else{
			$err_msg = isset($post['opportunityMessage']) ? $post['opportunityMessage'] : '';
			if( empty( $is_style ) ) {
				if(!empty($err_msg)){
	                return array(
	                    'status' => 'old_cannot_sync',
	                    'message' => $err_msg
	                );
				}else{
					return array(
	                    'status' => 'old_cannot_sync',
	                    'message' => 'Donation is not sync to salesforce yet.'
	                );
				}
            } else {
                return 'old-cannot-sync';            		      	
            }
        }
	}
	
	
	/**
	 * Fetches Salesforce sync status for Donations made in v1.
	 *
	 
	 * @author       Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param null $is_style
	 *
	 * @return array|string
	 *
	 * @LastUpdated  April 27, 2018
	 */
	public function wfc_get_sync_statusv1( $post, $is_style = null ) {
		/*
        *  get salesforce sync status
        */
        if((isset($post['opportunityStatus']) && $post['opportunityStatus'] == 200) &&
		(isset($post['opportunityId']) && !empty($post['opportunityId']))){
        	if( empty( $is_style ) ) {
                return array(
                    'status' => 'success_sync',
                    'message' => 'Recurring Donation successfully synced to salesforce.'
                );
            } else {
                return 'success-sync';
            }
        }else{
			$err_msg = isset($post['opportunityMessage']) ? $post['opportunityMessage'] : '';
			if( empty( $is_style ) ) {
				if(!empty($err_msg)){
	                return array(
	                    'status' => 'old_cannot_sync',
	                    'message' => $err_msg
	                );
				}else{
					return array(
	                    'status' => 'old_cannot_sync',
	                    'message' => 'Donation is not sync to salesforce yet.'
	                );
				}
            } else {
                return 'old-cannot-sync';            		      	
            }
        }
	}


}