<?php
/**
 * Class WFC_SalesforcePPAPI
 * This class handles functions related to Salesforce PPAPI package.
 *
 * @author Junjie Canonio
 * @since  1.0.0
 *
 * @LastUpdated  May 4, 2018
 */
class WFC_SalesforcePPAPI{
	
	/**
	 * Get donation counts for form.
	 *
	 * @author      Junjie Canonio
	 *
	 * @LastUpdated  May 4, 2018
	 *
	 * @param $data
	 *
	 * @return mixed
	 */
	public function wfc_sync_donation($data) {
		
		return $data;
	}
	
	/**
	 * Fetch GAU from Salesforce and save as option named 'pdwp_ASSFAPI/gau' .
	 *
	 * @author       Junjie Canonio
	 *
	 * @return array
	 * @LastUpdated  May 15, 2018
	 */
	public static function wfc_donation_fetchgau($sfinstanceid) {
		$gau_arr = array();
		$result = WFC_SalesforcePPAPI::wfc_donation_fetchgaucamp('GAU',$sfinstanceid);
		if((isset($result['status']) && $result['status'] == 'Success') && isset($result['data'])){
			foreach ($result['data'] as $key => $value) {
				if(isset($value['npsp__Active__c']) && $value['npsp__Active__c'] == 1) {
					$gau_arr[$value['Id']] = isset($value['Name']) ? $value['Name'] : '';
				}
			}
			update_option( "pdwp_ASSFAPI/gau_".$sfinstanceid, $gau_arr );
			$result = array(
				'status' => 'Success',
				'message' => 'GAU Fetched',
			);
		}else{
			$result = array(
				'status' => 'Failed',
				'message' => isset($result['message']) ? $result['message'] : 'error',
			);
		}
		
		return $result;
	}
	
	/**
	 * Fetch Campaign from Salesforce and save as option named 'pdwp_ASSFAPI/campaign'
	 *
	 * @author       Junjie Canonio
	 * @return array
	 *
	 * @LastUpdated  May 15, 2018
	 */
	public static function wfc_donation_fetchcamp($sfinstanceid) {
		$camp_arr = array();
		$result = WFC_SalesforcePPAPI::wfc_donation_fetchgaucamp('Campaign',$sfinstanceid);
		if((isset($result['status']) && $result['status'] == 'Success') && isset($result['data'])){
			foreach ($result['data'] as $key => $value) {
				$camp_arr[$value['Id']] = isset($value['Name']) ? $value['Name'] : '';
			}
			update_option( "pdwp_ASSFAPI/campaign_".$sfinstanceid, $camp_arr );
			$result = array(
				'status' => 'Success',
				'message' => 'Campaign Fetched',
			);
		}else{
			$result = array(
				'status' => 'Failed',
				'message' => isset($result['message']) ? $result['message'] : 'error',
			);
		}
		return $result;
	}
	
	/**
	 * Fetch GAU or Campaign from Salesforce
	 *
	 * @author       Junjie Canonio
	 *
	 * @param $type
	 *
	 * @return array|object
	 *
	 * @LastUpdated  May 15, 2018
	 */
	public static function wfc_donation_fetchgaucamp($type,$sfinstanceid) {

		if(strtolower($type) == 'gau'){
			$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$sfinstanceid);
			$result = $oauth->api_request( 'apexrest/ASSFAPI/gau','', 'retrieve', true, false );
			if((isset($result['status_code']) && $result['status_code'] == 200) &&
				(isset($result['sf_response']['status']) && $result['sf_response']['status'] == 'Success')){
				$result = array(
					'status' => 'Success',
					'message' => isset($result['sf_response']['message']) ? $result['sf_response']['message'] : '',
					'data' => isset($result['sf_response']['result']) ? $result['sf_response']['result'] : array(),
				);
			}else{
				if(isset($result['sf_error'])) {
					$result = array(
						'status' => 'Failed',
						'message' => isset($result['sf_error']['error_description']) ? $result['sf_error']['error_description'] : '',
						'data' => isset($result['sf_error']) ? $result['sf_error'] : array(),
					);
				}else{
					$result = array(
						'status' => 'Failed',
						'message' => isset($result['sf_response']['message']) ? $result['sf_response']['message'] : '',
						'data' => isset($result['sf_response']['result']) ? $result['sf_response']['result'] : array(),
					);
				}

			}			
		}elseif(strtolower($type) == 'campaign'){
			$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$sfinstanceid);
			$result = $oauth->api_request( 'apexrest/ASSFAPI/campaign','', 'retrieve', true, false );
			if((isset($result['status_code']) && $result['status_code'] == 200) &&
				(isset($result['sf_response']['status']) && $result['sf_response']['status'] == 'Success')){
				$result = array(
					'status' => 'Success',
					'message' => isset($result['sf_response']['message']) ? $result['sf_response']['message'] : '',
					'data' => isset($result['sf_response']['result']) ? $result['sf_response']['result'] : array(),
				);
			}else{
				if(isset($result['sf_error'])) {
					$result = array(
						'status' => 'Failed',
						'message' => isset($result['sf_error']['error_description']) ? $result['sf_error']['error_description'] : '',
						'data' => isset($result['sf_error']) ? $result['sf_error'] : array(),
					);
				}else{
					$result = array(
						'status' => 'Failed',
						'message' => isset($result['sf_response']['message']) ? $result['sf_response']['message'] : '',
						'data' => isset($result['sf_response']['result']) ? $result['sf_response']['result'] : array(),
					);
				}
			}			
		}else{
			return array(
				'status' => 'Failed',
				'message' => "Invalid parameter value. Choose either 'GAU' or 'CAMPAIGN'",
				'data' => array(),
			);
		}
		return $result;
	}
	
	/**
	 * Processes PPAPI single payment transaction.
	 *
	 * @author      Junjie Canonio
	 *
	 * @LastUpdated  May 4, 2018
	 *
	 * @param      $post
	 * @param null $recurringid
	 *
	 * @return array
	 */
	public function wfc_sync_singledonation( $post, $recurringid = NULL ) {
		$donation_sfinstance = isset($post['donation_sfinstance']) ? $post['donation_sfinstance'] : '';

		/*
         * call all custom wfc_donation_before_sfsync_flt filter
         */
        if (has_filter('wfc_donation_before_sfsync_flt') == true) {
            $wfc_donation_before_sfsync_flt = apply_filters('wfc_donation_before_sfsync_flt', $post);
            $post = $wfc_donation_before_sfsync_flt;
        }
        
		/*
		* check payment method type
		*/			
		if( isset( $post['payment_type'] ) && $post['payment_type'] == 'bank' ) {
			$PaymentMethodType = 'bank';
		} else {
			// if(isset($post['TokenCustomerID']) && !empty($post['TokenCustomerID'])){
			// 	$PaymentMethodType = 'token';
			// }else{
				$PaymentMethodType = 'cc';
			//}
		}

		if( isset( $post['gatewayid'] ) && !empty( $post['gatewayid'] ) ) {
			$post_gatewayid = str_replace($donation_sfinstance.'_',"",$post['gatewayid']);
			$gate = explode( '_' , $post_gatewayid );
			$gatewayid = $gate[1];
		} else {
			$gatewayid = NULL;
		}
		/*
		* construct data
		*/
		$salesforceinstance_settings = get_option('wfc_donation_'.$donation_sfinstance.'_settings');

		if(!empty($recurringid) && $recurringid != NULL){
			$donation_dnsr_single = ( isset( $salesforceinstance_settings['donation_dnsr_recurring'] )
			&& $salesforceinstance_settings['donation_dnsr_recurring'] == 'true' ) ? true : false;
		}else {
			$donation_dnsr_single = ( isset( $salesforceinstance_settings['donation_dnsr_single'] )
			&& $salesforceinstance_settings['donation_dnsr_single'] == 'true' ) ? true : false;
		}
		
		/*
		* Manage donation amount
		*/		
		$pdwp_amount = isset( $post['pdwp_amount'] ) ? $post['pdwp_amount']  : '';
		$pdwp_custom_amount = isset( $post['pdwp_custom_amount'] ) ? $post['pdwp_custom_amount']  : '';
		if (!empty($pdwp_amount)) {
			$pdwp_sub_amt = isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : $pdwp_custom_amount;
		}elseif(!empty($pdwp_custom_amount)) {
			$pdwp_sub_amt = isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : $pdwp_custom_amount;
		}else{
			$pdwp_sub_amt = isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : '';
		}
		$wfc_donation_amount = $pdwp_sub_amt;
		

		/*
		* GET currency 
		*/
		$pdwp_currency = isset( $post['currency'] ) ? $post['currency'] : '';
		$currency_arr = explode(',', $pdwp_currency);
		$currency = isset( $currency_arr[1] ) ? $currency_arr[1] : '';


		/*
		* advance cart suffix
		*/	
		$wfc_advancecart_suffix = isset($post['wfc_advancecart_suffix']) ? $post['wfc_advancecart_suffix'] : '';
		$wfc_advancecart_amount = isset($post['wfc_advancecart_amount']) ? $post['wfc_advancecart_amount'] : '';
		if(isset($post['wfc_carttype']) && $post['wfc_carttype'] == 'advance_cart') {
			$donation_original_amount = $wfc_advancecart_amount; 
		}else{
			$donation_original_amount = $wfc_donation_amount; 
		}

		$pamynet_ref = isset( $post['pamynet_ref'] ) ? $post['pamynet_ref'] : '';
		if(isset($post['paypal_redirect']) && $post['paypal_redirect'] == 'true'){
			$pamynet_ref = isset($post['PaymentTransaction']['PaymentCaptureId']) ? $post['PaymentTransaction']['PaymentCaptureId'] : $pamynet_ref;
		}
		

		$data = array(
			'strDonation' => array(
				'emailReceipt' => $donation_dnsr_single,
				'donationType' => 'one',
				'PaymentMethodType' => $PaymentMethodType,
				'donorType' => ( isset( $post['donor_type'] ) && trim( $post['donor_type'] ) == 'business' ) ? 'company' : 'individual',
				'companyName' => isset( $post['donor_company'] ) ? $post['donor_company'] : '',
				'honoreeName' => isset( $post['pdwp_gift_text'] ) ? $post['pdwp_gift_text'] : '',
				'Salutation' => isset( $post['donor_title'] ) ? ucfirst( $post['donor_title'] ) : '',
				'FirstName' => isset( $post['donor_first_name'] ) ? $post['donor_first_name'] : '',
				'LastName' => isset( $post['donor_last_name'] ) ? $post['donor_last_name'] : '',
				'Email' => isset( $post['donor_email'] ) ? $post['donor_email'] : '',
				'phone' => isset( $post['donor_phone'] ) ? $post['donor_phone'] : '',
				'address' => $this->wfc_donor_address( $post ),
				'city' => ( isset( $post['donor_suburb'] ) && !empty( $post['donor_suburb'] ) ) ? $post['donor_suburb'] : '',
				'state' => ( isset( $post['donor_state'] ) && !empty( $post['donor_state'] ) ) ? $post['donor_state'] : '',
				'postcode' => ( isset( $post['donor_postcode'] ) && !empty( $post['donor_postcode'] ) ) ? $post['donor_postcode'] : '',
				'country' => ( isset( $post['donor_country'] ) && !empty( $post['donor_country'] ) ) ? $post['donor_country'] : '',
				'Amount' => $donation_original_amount,
				'currencyIsoCode' => $currency,
				'comments' => isset( $post['donor_comment'] ) ?  $post['donor_comment'] : '',
				'newsletter' => ( isset( $post['donor_newsletter'] ) && !empty( $post['donor_newsletter'] ) ) ? true : false,
				'CampaignId' => ( isset( $post['campaignId'] ) && !empty( $post['campaignId'] ) ) ? trim( $post['campaignId'] ) : NULL,
				'GAUAlloc' => $this->wfc_gau_allocation( $post ),
				'GatewayId' => $gatewayid,
				'TxnStatus' => isset( $post['PaymentTransaction']['TxnStatus'] ) ? $post['PaymentTransaction']['TxnStatus'] : '',
				'TxnId' => isset( $post['PaymentTransaction']['TxnId'] ) ? $post['PaymentTransaction']['TxnId'] : '',
				'TxnDate' => isset( $post['PaymentTransaction']['TxnDate'] ) ? $post['PaymentTransaction']['TxnDate'] : '',
				'TxnMessage' => isset( $post['PaymentTransaction']['TxnMessage'] ) ? $post['PaymentTransaction']['TxnMessage'] : '',
				'TxnPaymentRef' => $pamynet_ref,
				'SysRefId' => isset( $post['SysRefId'] ) ? $post['SysRefId'] : '',
				'RecurringId' => !empty( $recurringid ) ? $recurringid : NULL,
				'ptSuffix' => $wfc_advancecart_suffix,
       			'origAmount' => $wfc_donation_amount,
			)
		);
		
		/*
		* sync single donation to ppapi 
		*/			
		$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$donation_sfinstance);
		$result = $oauth->api_request( 'apexrest/ASSFAPI/donation' , stripslashes_deep($data), 'create' );	

		/*
		* return data and sync result
		*/	
		return array(
			'data' => array(
				'single' => stripslashes_deep( $data )
			),
			'result' => array(
				'single' => $result
			)
		);
	}
	
	/**
	 * Processes PPAPI recurring payment transactions.
	 *
	 * @author       Junjie Canonio
	 *
	 * @LastUpdated  May 7, 2018
	 *
	 * @param $post
	 * @param $payment_source
	 *
	 * @return array
	 */
	public function wfc_sync_recurringdonation( $post, $payment_source) {
		$donation_sfinstance = isset($post['donation_sfinstance']) ? $post['donation_sfinstance'] : '';

		/*
         * call all custom wfc_donation_before_sfsync_flt filter
         */
        if (has_filter('wfc_donation_before_sfsync_flt') == true) {
            $wfc_donation_before_sfsync_flt = apply_filters('wfc_donation_before_sfsync_flt', $post);
            $post = $wfc_donation_before_sfsync_flt;
        }

		/*
		* check payment method type
		*/			
		if( isset( $post['payment_type'] ) && $post['payment_type'] == 'bank' ) {
			$PaymentMethodType = 'bank';
		} else {
			// if(isset($post['TokenCustomerID']) && !empty($post['TokenCustomerID'])){
			// 	$PaymentMethodType = 'token';
			// }else{
				$PaymentMethodType = 'cc';
			//}
		}

		if( isset( $post['gatewayid'] ) && !empty( $post['gatewayid'] ) ) {
			$post_gatewayid = str_replace($donation_sfinstance.'_',"",$post['gatewayid']);
			$gate = explode( '_' , $post_gatewayid );
			$gatewayid = $gate[1];
		} else {
			$gatewayid = NULL;
		}

		$salesforceinstance_settings = get_option('wfc_donation_'.$donation_sfinstance.'_settings');
		$donation_dnsr_recurring = ( isset( $salesforceinstance_settings['donation_dnsr_recurring'] )
		&& $salesforceinstance_settings['donation_dnsr_recurring'] == 'true' ) ? true : false;


		/*
		* Manage donation amount
		*/		
		$pdwp_amount = isset( $post['pdwp_amount'] ) ? $post['pdwp_amount']  : '';
		$pdwp_custom_amount = isset( $post['pdwp_custom_amount'] ) ? $post['pdwp_custom_amount']  : '';
		if (!empty($pdwp_amount)) {
			$pdwp_sub_amt = isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : $pdwp_custom_amount;
		}elseif(!empty($pdwp_custom_amount)) {
			$pdwp_sub_amt = isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : $pdwp_custom_amount;
		}else{
			$pdwp_sub_amt = isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : '';
		}
		$wfc_donation_amount = $pdwp_sub_amt;


		/*
		* GET currency 
		*/
		$pdwp_currency = isset( $post['currency'] ) ? $post['currency'] : '';
		$currency_arr = explode(',', $pdwp_currency);
		$currency = isset( $currency_arr[1] ) ? $currency_arr[1] : '';


		/*
		* advance cart suffix
		*/	
		$wfc_advancecart_amount = isset($post['wfc_advancecart_amount']) ? $post['wfc_advancecart_amount'] : '';
		if(isset($post['wfc_carttype']) && $post['wfc_carttype'] == 'advance_cart') {
			$donation_original_amount = $wfc_advancecart_amount; 
		}else{
			$donation_original_amount = $wfc_donation_amount; 
		}


		$data = array(
			'strDonation' => array(
				'emailReceipt' => $donation_dnsr_recurring,
				'donationType' => 'monthly',
				'PaymentMethodType' => $PaymentMethodType,
				'donorType' => ( isset( $post['donor_type'] ) && trim( $post['donor_type'] ) == 'business' ) ? 'company' : 'individual',
				'companyName' => isset( $post['donor_company'] ) ? $post['donor_company'] : '',
				'honoreeName' => isset( $post['pdwp_gift_text'] ) ? $post['pdwp_gift_text'] : '',
				'Salutation' => isset( $post['donor_title'] ) ? ucfirst( $post['donor_title'] ) : '',
				'FirstName' => isset( $post['donor_first_name'] ) ? $post['donor_first_name'] : '',
				'LastName' => isset( $post['donor_last_name'] ) ? $post['donor_last_name'] : '',
				'Email' => isset( $post['donor_email'] ) ? $post['donor_email'] : '',
				'phone' => isset( $post['donor_phone'] ) ? $post['donor_phone'] : '',
				'address' => $this->wfc_donor_address( $post ),
				'city' => ( isset( $post['donor_suburb'] ) && !empty( $post['donor_suburb'] ) ) ? $post['donor_suburb'] : '',
				'state' => ( isset( $post['donor_state'] ) && !empty( $post['donor_state'] ) ) ? $post['donor_state'] : '',
				'postcode' => ( isset( $post['donor_postcode'] ) && !empty( $post['donor_postcode'] ) ) ? $post['donor_postcode'] : '',
				'country' => ( isset( $post['donor_country'] ) && !empty( $post['donor_country'] ) ) ? $post['donor_country'] : '',
				'Amount' => $donation_original_amount,
				'currencyIsoCode' => $currency,
				// 'frequency' => isset( $post['pdwp_donation_type'] ) ? ucfirst( $post['pdwp_donation_type'] ) : NULL,
				'frequency' => isset( $post['pdwp_donation_type'] ) ? strtolower( $post['pdwp_donation_type'] ) : NULL,
				'GatewayId' => $gatewayid,
				'CampaignId' => ( isset( $post['campaignId'] ) && !empty( $post['campaignId'] ) ) ?  trim( $post['campaignId'] ) : NULL,
				'comments' => isset( $post['donor_comment'] ) ?  $post['donor_comment'] : '',
				'newsletter' => ( isset( $post['donor_newsletter'] ) && !empty( $post['donor_newsletter'] ) ) ? true : false,
				'GAUAlloc' => $this->wfc_gau_allocation( $post ),
				'SysRefId' => isset( $post['SysRefId'] ) ? $post['SysRefId'] : '',
			)
		);

		/*
		* check if payment source is empty 
		*/	
		if(!empty($payment_source)) {
			$data['strDonation']['PaymentSource'] = $payment_source;
		}

		/*
		* check if payment source ID empty 
		*/	
		if (isset($post['SF_PS_paymentSourceId']) && !empty($post['SF_PS_paymentSourceId'])) {
			$data['strDonation']['PSId'] = $post['SF_PS_paymentSourceId'];
			unset($data['strDonation']['PaymentSource']);
		}

		/*
		* check if payment source contact ID empty 
		*/	
		if (isset($post['SF_PS_contactId']) && !empty($post['SF_PS_contactId'])) {
			$data['strDonation']['ContactId'] = $post['SF_PS_contactId'];
		}


		if( isset( $post['pdwp_donor_date'] ) && !empty( $post['pdwp_donor_date'] ) ) {
			$data['strDonation']['donorDate'] = $post['pdwp_donor_date'];
		}


		/*
		* sync recurring donation to ppapi 
		*/			
		$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$donation_sfinstance);
		$result = $oauth->api_request( 'apexrest/ASSFAPI/donation' , stripslashes_deep($data), 'create' );	

		if( isset( $post['payment_type'] ) && $post['payment_type'] == 'bank' ) {
			$data_bsbno = isset($payment_source['bsbno']) ? $payment_source['bsbno'] : '';
			$data_bsbno = '**'.substr($data_bsbno, 2);
			$data_bsbaccount = isset($payment_source['bsbaccount']) ? $payment_source['bsbaccount'] : '';
			$data_bsbaccount = '*****'.substr($data_bsbaccount, 5);
			$bsbname = isset($payment_source['bsbname']) ? $payment_source['bsbname'] : '';

			$data['strDonation']['PaymentSource'] = array(
				'bsbno' => $data_bsbno,
				'bsbaccount' => $data_bsbaccount,
				'bsbname' => $bsbname,
				'psType' => 'bank',
			);
		}

		/*
		* return data and sync result
		*/	
		return array(
			'data' => array(
				'recurring' => stripslashes_deep( $data )
			),
			'result' => array(
				'recurring' => $result
			)
		);
	}
	
	/**
	 * Processes donor address from input.
	 *
	 * @author       Junjie Canonio
	 *
	 * @LastUpdated  May 4, 2018
	 *
	 * @param $post
	 *
	 * @return mixed|string
	 */
	private function wfc_donor_address( $post ) {
		if( isset( $post['unit_number'] ) && !empty( $post['unit_number'] ) ) {
			$the_address = isset( $post['donor_address'] ) ? $post['donor_address'] : '' ;
			return $post['unit_number'] . ' ' . $the_address;
		} else {
			return isset( $post['donor_address'] ) ? $post['donor_address'] : '' ;
		}
	}
	
	/**
	 * Processes transaction's GAU allocation.
	 *
	 * @author      Junjie Canonio
	 *
	 * @LastUpdated  May 4, 2018
	 *
	 * @param $post
	 *
	 * @return array
	 */
	public function wfc_gau_allocation( $post ) {
		if (isset($post['wfc_carttype']) && $post['wfc_carttype'] == 'advance_cart') {
			$wfc_advancecart_suffix = isset($post['wfc_advancecart_suffix']) ? $post['wfc_advancecart_suffix'] : '';
			
			$cartItems = isset($post['cartItems']) ? $post['cartItems'] : array(); 
			$singlePaymentsItem = array();
			$recurringPaymentsItem = array();
			if(!empty($cartItems)) {
				foreach ($cartItems as $key => $value) {
					if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
						array_push($singlePaymentsItem,$value);
					}else{
						array_push($recurringPaymentsItem,$value);
					}
				}
			}
			
			if (sizeof($singlePaymentsItem) > 1) {
				$cartItems = $recurringPaymentsItem;
				array_push($cartItems,$singlePaymentsItem);
			}
			foreach ($cartItems as $key => $value) {
				if (empty($value)) {
					unset($cartItems[$key]);
				}
			}
			
			if (sizeof($cartItems[$wfc_advancecart_suffix]) > 1 && !isset($cartItems[$wfc_advancecart_suffix]['wfc_donation_type'])) {
				foreach ( $cartItems[$wfc_advancecart_suffix] as $item ) {
					if( isset( $item['gau_id'] ) ) {
						$gauid = trim( $item['gau_id'] );
						if( !empty( $gauid ) ) {
							$gau[] = array(
								'gauId' => trim( $item['gau_id'] ),
								'amount' => $item['amount']
							);
						}
					}
				}
				return $gau;
			}else{
				return array( 
					array(
						'gauId' => trim($post['cartItems'][$wfc_advancecart_suffix]['gau_id']),
						'amount' => isset($post['cartItems'][$wfc_advancecart_suffix]['amount']) ?
						$post['cartItems'][$wfc_advancecart_suffix]['amount'] : 0,
					) 
				);
			}


		}else{
			if( !isset( $post['cartItems'] ) ) {
				if( !empty( $post['gauId'] ) ) {
					return array( array(
						'gauId' => trim( $post['gauId'] ),
						'amount' => isset( $post['pdwp_sub_amt'] ) ? $post['pdwp_sub_amt']  : $post['pdwp_custom_amount']
					) );
				} else {
					return array();
				}
			} else {
				if( is_array( $post['cartItems'] ) && sizeof( $post['cartItems'] ) > 0 ) {
					foreach ( $post['cartItems'] as $item ) {
						if( isset( $item['gau_id'] ) ) {
							$gauid = trim( $item['gau_id'] );
							if( !empty( $gauid ) ) {
								$gau[] = array(
									'gauId' => trim( $item['gau_id'] ),
									'amount' => $item['amount']
								);
							}
						}
					}
				}
				return $gau;
			}
		}
	}
	
	/**
	 * Parses response from Salesforce PPAPI.
	 *
	 * @author      Junjie Canonio
	 *
	 * @param       $post
	 * @param       $result
	 * @param array $cartarg
	 *
	 * @return array|mixed|string
	 *
	 * @LastUpdated  May 7, 2018
	 */
	public function wfc_ppapi_parse_response( $post, $result, $cartarg = array()  ) {
		$WFC_DonationEmailHandler = new WFC_DonationEmailHandler();
		if( (isset($post['pdwp_donation_type']) && $post['pdwp_donation_type'] == 'one-off') ||
		 (isset($cartarg['wfc_donation_type']) && $cartarg['wfc_donation_type'] == 'one-off') ) {

			if( isset( $result['result']['single']['sf_response']['oppResult']['Id']) ){
				/*
				*  Notify Admin Sync Successful
				*/
				$post['SalesforceResponse'] = array(
					'OpportunityId' => $result['result']['single']['sf_response']['oppResult']['Id'],
					'status' => $result['result']['single']['status_code'],
					'Message' => $result['result']['single']['sf_response']['message']
				);

				if(isset($cartarg['recurringid']) && !empty($cartarg['recurringid'])){
					$post['SalesforceResponse']['RecurringId'] = $cartarg['recurringid'];
				}

				/*
				*  send success notification
				*/
				$WFC_DonationEmailHandler->wfc_donation_admin_success_notification($post);

				/*
				*  sucess
				*/
				return $post['SalesforceResponse'];
			}else{
				/*
				* Notify Admin Sync Fail
				*/		
				if(isset($result['result']['single']['sf_response'])){
								
					$post['SalesforceResponse'] = $result['result']['single']['sf_response'] ;

					/*
					* send fail notification
					*/					
					$WFC_DonationEmailHandler->wfc_donation_admin_fail_notification($post);

					return $result['result']['single']['sf_response'];
				}elseif(isset($result['result']['single']['sf_error'])) {
					
					if(isset($result['result']['single']['sf_error']['error'])){
						$post['SalesforceResponse'] = 
						isset($result['result']['single']['sf_error']) ? $result['result']['single']['sf_error'] : array();

						$result['result']['single']['sf_error']['status_code'] = 
						isset($result['result']['single']['status_code']) ? $result['result']['single']['status_code'] : '';

						/*
						* send fail notification
						*/					
						$WFC_DonationEmailHandler->wfc_donation_admin_fail_notification($post);

						return $result['result']['single']['sf_error'];
					}elseif(!isset($result['result']['single']['sf_error']['error'])){
						$post['SalesforceResponse'] = 
						isset($result['result']['single']['sf_error']) ? $result['result']['single']['sf_error'] : array();

						$error_arr = array();						
						foreach ($post['SalesforceResponse'] as $key => $value) {
							$errorCode = isset($value['errorCode']) ? $value['errorCode'] : '';
							$message = isset($value['message']) ? $value['message'] : '';
							$status_code = isset($result['result']['single']['status_code']) ? $result['result']['single']['status_code'] : '';

							$suffix = isset($cartarg['suffix']) ? $cartarg['suffix'] : '';
							$RecurringId = 
							isset($post['SalesforceLogs'][$suffix]['result']['recurring']['recResult']['Id']) ?
							$post['SalesforceLogs'][$suffix]['result']['recurring']['recResult']['Id'] : '';

							array_push(
								$error_arr,
								array(
									'errorCode' => $errorCode,
									'message' => $message,
									'status_code' => $status_code
								)
							);
						}

						$result['result']['single']['sf_error']['error'] = $error_arr;
						if (!empty($RecurringId)) {
							$result['result']['single']['sf_error']['RecurringId'] = $RecurringId;
						}

						/*
						* send fail notification
						*/					
						$WFC_DonationEmailHandler->wfc_donation_admin_fail_notification($post);

						return $result['result']['single']['sf_error'];
					}else{

					}
				}else{
					return 'Unknown Error';
				}

			}

		} else {
			if( isset( $result['result']['recurring']['sf_response']['recResult']['Id']) ){

				$response['OpportunityId'] = $result['result']['recurring']['sf_response']['recResult']['Id'];
				$response['RecurringId'] = $result['result']['recurring']['sf_response']['recResult']['Id'];
				$response['status'] = $result['result']['recurring']['status_code'];
				$response['Message'] = $result['result']['recurring']['sf_response']['message'];

				/*
				*  Notify Admin Sync Successful
				*/
				$post['SalesforceResponse'] = array(
					'OpportunityId' => $response['OpportunityId'],
					'RecurringId' => $response['RecurringId'],
					'status' => $response['status'],
					'Message' => $response['Message']
				);

				return array(
					'OpportunityId' => isset( $response['OpportunityId'] ) ? $response['OpportunityId'] : 
						array( 
							'errorCode' 	=>	$response['errorCode'],
							'ErrorMessage' 	=>	$response['ErrorMessage'],
						),
					'RecurringId'	=> $response['RecurringId'],
					'status' 		=> $response['status'],
					'Message' 		=> $response['Message']
				);				

				
			}else{
				/*
				* Notify Admin Sync Fail
				*/		
				if(isset($result['result']['recurring']['sf_response'])){
								
					$post['SalesforceResponse'] = $result['result']['recurring']['sf_response'] ;

					/*
					* send fail notification
					*/					
					$WFC_DonationEmailHandler->wfc_donation_admin_fail_notification($post);

					return $result['result']['recurring']['sf_response'];
				}elseif(isset($result['result']['recurring']['sf_error'])) {
					
					if(isset($result['result']['recurring']['sf_error']['error'])){
						$post['SalesforceResponse'] = 
						isset($result['result']['recurring']['sf_error']) ? $result['result']['recurring']['sf_error'] : array();

						$result['result']['recurring']['sf_error']['status_code'] = 
						isset($result['result']['recurring']['status_code']) ? $result['result']['recurring']['status_code'] : '';

						/*
						* send fail notification
						*/					
						$WFC_DonationEmailHandler->wfc_donation_admin_fail_notification($post);

						return $result['result']['recurring']['sf_error'];
					}elseif(!isset($result['result']['recurring']['sf_error']['error'])){
						$post['SalesforceResponse'] = 
						isset($result['result']['recurring']['sf_error']) ? $result['result']['recurring']['sf_error'] : array();

						foreach ($post['SalesforceResponse'] as $key => $value) {
							$errorCode = isset($value['errorCode']) ? $value['errorCode'] : '';
							$message = isset($value['message']) ? $value['message'] : '';
							$status_code = isset($result['result']['recurring']['status_code']) ? $result['result']['recurring']['status_code'] : '';

							$result['result']['recurring']['sf_error'][$key] = array(
								'errorCode' => $errorCode,
								'message' => $message,
								'status_code' => $status_code,
							);
						}

						/*
						* send fail notification
						*/					
						$WFC_DonationEmailHandler->wfc_donation_admin_fail_notification($post);

						return $result['result']['recurring']['sf_error'];
					}else{

					}
				}else{
					return 'Unknown Error';
				}
			}
		}
	}


	/**
	 * Validates GAU if it is active on Salesforce.
	 *
	 * @param $sf_id
	 *
	 * @param $donation_sfinstance
	 * @return array
	 *
	 * @LastUpdated  June 22, 2018
	 * @author       Junjie Canonio
	 *
	 */
    public static function wfc_gau_validator($sf_id,$donation_sfinstance) {
        
        if(!empty($sf_id)){
            $oauth  = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$donation_sfinstance);

            $result = $oauth->api_request( 'data/v39.0/sobjects/npsp__General_Accounting_Unit__c/'.$sf_id, '', 'get' );

            if(isset($result['sf_response']) && isset($result['sf_response']['Id'])) {
            	$npsp__Active__c = isset($result['sf_response']['npsp__Active__c']) ? $result['sf_response']['npsp__Active__c'] : '';
            	$Name = isset($result['sf_response']['Name']) ? $result['sf_response']['Name'] : '';
            	if($npsp__Active__c == true) {
            		return array(
            			'status' => 'success',
            			'message' => 'GAU: '.$Name.' is active.'
            		);
            	}else{
            		return array(
            			'status' => 'fail',
            			'message' => 'Unable to proceed, GAU: '.$Name.' is inactive, Please contact your administrator.'
            		);
            	}
            }elseif(isset($result['sf_error'][0]['errorCode']) && $result['sf_error'][0]['errorCode'] == 'NOT_FOUND') {
            	return array(
        			'status' => 'fail',
        			'message' => 'Unable to process donation, GAU does not exist, Please contact your administrator.'
        		);
            }elseif(isset($result['sf_error']['error_description'])){
            	return array(
        			'status' => 'fail',
        			'message' => $result['sf_error']['error_description']
        		);
            }else{
            	return array(
        			'status' => 'fail',
        			'message' => 'Unknown error, Please contact your administrator.'
        		);
            }
        }
    }

	/**
	 * Sync payment source to Saleforce.
	 *
	 * @param array $data
	 *
	 * @param $donation_sfinstance
	 * @return array|object
	 *
	 * @LastUpdated  April 17, 2019
	 * @author Junjie Canonio
	 *
	 */
    public static function sync_paymentsource($data = array(),$donation_sfinstance) {

    	if (empty($data)) {
    		
    		return array(
    			'status' => 'error',
    			'msg' => 'Data is empty.'
    		);

    	}else {

			$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ),$donation_sfinstance);
			$result = $oauth->api_request( 'apexrest/ASSFAPI/setPS' , stripslashes_deep($data), 'create' );	
			return $result;

    	}		

    }

}