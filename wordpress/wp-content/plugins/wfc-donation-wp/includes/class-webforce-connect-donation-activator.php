<?php

/**
 * Class Webforce_Connect_Donation_Activator
 *
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 *
 * @since      1.0.0
 * @package    Webforce_Connect_Donation
 * @author     AlphaSys Pty. Ltd. <https://alphasys.com.au>
 */
class Webforce_Connect_Donation_Activator {
	
	
	/**
	 * @since    1.0.0
	 */
	public static function activate() {

	}
	
	/**
	 * Initialize default Donation Settings configurations.
	 *
	 * This function loads the necessary presets for the donation settings and generates the 'Thank you' and 'Failure' pages on activation.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @since    1.0.0
	 */
	public static function init_wfc_donation_settings() {
		global $wpdb;
		$pd_settings = get_option('pronto_donation_settings', 0);

		// Data to be saved
		$donation_debug_mode 	 = isset($pd_settings->donation_debug_mode) ? $pd_settings->donation_debug_mode : 'false';
		$cron_schedule 			 = isset($pd_settings->cron_schedule) ? $pd_settings->cron_schedule : 'hourly';
		$donation_dnsr_single  	 = isset($pd_settings->donation_dnsr_single) ? $pd_settings->donation_dnsr_single : 'true';
		$donation_dnsr_recurring = isset($pd_settings->donation_dnsr_recurring) ? $pd_settings->donation_dnsr_recurring : 'true';

		$frequencies_def 		 = '[{"item-0":{"values":{"frequency-name":"One-off","frequency-switch":"true","frequency-value":"one-off"},"non_deletable":"true"}},{"item-1":{"values":{"frequency-name":"Daily","frequency-switch":"true","frequency-value":"daily"},"non_deletable":"true"}},{"item-2":{"values":{"frequency-name":"Weekly","frequency-switch":"true","frequency-value":"weekly"},"non_deletable":"true"}},{"item-3":{"values":{"frequency-name":"Four-weekly","frequency-switch":"true","frequency-value":"fourweekly"},"non_deletable":"true"}},{"item-4":{"values":{"frequency-name":"Fortnightly","frequency-switch":"true","frequency-value":"fortnightly"},"non_deletable":"true"}},{"item-5":{"values":{"frequency-name":"Monthly","frequency-switch":"true","frequency-value":"monthly"},"non_deletable":"true"}},{"item-6":{"values":{"frequency-name":"Quarterly","frequency-switch":"true","frequency-value":"quarterly"},"non_deletable":"true"}},{"item-7":{"values":{"frequency-name":"Yearly","frequency-switch":"true","frequency-value":"yearly"},"non_deletable":"true"}}]';
		$frequencies 			 = isset($pd_settings->frequencies) ? $pd_settings->frequencies : $frequencies_def ;
		
		$enable_validation 		 = isset($pd_settings->enable_validation) ? $pd_settings->enable_validation : 'false';
		$google_api_key 		 = isset($pd_settings->google_api_key) ? $pd_settings->google_api_key : '';
		$harmony_rapid_displaymode  = isset($pd_settings->harmony_rapid_displaymode) ? $pd_settings->harmony_rapid_displaymode : 'fieldmode';
		$harmony_rapid_sourceoftruth  = isset($pd_settings->harmony_rapid_sourceoftruth) ? $pd_settings->harmony_rapid_sourceoftruth : 'AUSOTS';
		$harmony_rapid_username 	  = isset($pd_settings->harmony_rapid_username) ? $pd_settings->harmony_rapid_username : '';
		$harmony_rapid_password 	  = isset($pd_settings->harmony_rapid_password) ? $pd_settings->harmony_rapid_password : '';
		
		$enable_recaptcha 		 = isset($pd_settings->enable_recaptcha) ? $pd_settings->enable_recaptcha : 'false';
		$google_site_key 		 = isset($pd_settings->google_site_key) ? $pd_settings->google_site_key : '';
		$google_secret_key 		 = isset($pd_settings->google_secret_key) ? $pd_settings->google_secret_key : '';

		$social_facebook 		 = isset($pd_settings->social_facebook) ? $pd_settings->social_facebook : 'false';
		$social_twitter 		 = isset($pd_settings->social_twitter) ? $pd_settings->social_twitter : 'false';
		$social_google_plus 	 = isset($pd_settings->social_google_plus) ? $pd_settings->social_google_plus : 'false';
		$social_linkedin 		 = isset($pd_settings->social_linkedin) ? $pd_settings->social_linkedin : 'false';

		$loading_enable = isset($pd_settings->loading_enable) ? $pd_settings->loading_enable : 'true';
		$loading_spinner = isset($pd_settings->loading_spinner) ? $pd_settings->loading_spinner : 'tail';
		$loading_tinter_color = isset($pd_settings->loading_tinter_color) ? $pd_settings->loading_tinter_color : '#000000';

		$pd_homeurl = home_url( '/', 'http' );
		if(is_ssl()){ $pd_homeurl = home_url( '/', 'https' ); }

		// Create Thank you page
		$pd_thank_you_page_id = null;
		$new_post_thank_result = $wpdb->get_results("SELECT count(*) as detect FROM  $wpdb->posts WHERE post_type = 'page' AND post_title = 'Thank You - Pronto Donation WPPlugin'");

		if( $new_post_thank_result[0]->detect == 0 ) {
			$new_post_thank_you = array(
	            'post_title' 	=> 'Thank You - Pronto Donation WPPlugin',
	            'post_content'  => 'Thank you for your donation.',
	            'post_status' 	=> 'publish',
	            'post_date' 	=> date('Y-m-d H:i:s'),
	            'post_author' 	=> '',
	            'post_type' 	=> 'page',
	            'post_category' => array(0)
	        );
	        $pd_thank_you_page_id = wp_insert_post( $new_post_thank_you );
		}

		$pd_settings_args = array(
            'donation_debug_mode' 	 => $donation_debug_mode,
            'cron_schedule' 		 => $cron_schedule,
            'donation_dnsr_single' 	  => $donation_dnsr_single,
            'donation_dnsr_recurring' => $donation_dnsr_recurring,

            'frequencies' 			 => $frequencies,

            'enable_validation' 	 => $enable_validation,
            'google_api_key' 		 => $google_api_key,
            'harmony_rapid_displaymode'   => $harmony_rapid_displaymode,
            'harmony_rapid_sourceoftruth' => $harmony_rapid_sourceoftruth,
            'harmony_rapid_username' 	  => $harmony_rapid_username,
            'harmony_rapid_password' 	  => $harmony_rapid_password,

            'enable_recaptcha' 		 => $enable_recaptcha,
            'google_site_key' 		 => $google_site_key,
            'google_secret_key' 	 => $google_secret_key,

            'social_facebook' 		 => $social_facebook,
            'social_twitter' 		 => $social_twitter,
            'social_google_plus' 	 => $social_google_plus,
            'social_linkedin' 		 => $social_linkedin,

            'loading_enable' 		 => $loading_enable,
            'loading_spinner' 		 => $loading_spinner,
			'loading_tinter_color' 	 => $loading_tinter_color,

            'pd_thank_you_page_id'   => $pd_thank_you_page_id,
            'pd_homeurl'			 => $pd_homeurl,

		);

		update_option('pronto_donation_settings' , (object)$pd_settings_args);
		wp_clear_scheduled_hook( 'wfc_don_cron_resyncdonation' );
		wp_schedule_event( time(), trim($cron_schedule), 'wfc_don_cron_resyncdonation' );
		//wp_schedule_event( time(), 'hourly', 'wfc_don_cron_resyncgaucamp' );
	}

}
