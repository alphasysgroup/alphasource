<?php
	
	/**
	 * Class WFC_DonationEmailHandler
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @since 1.0.0.0
	 *
	 * @LastUpdated  May 24, 2018
	 */
	class WFC_DonationEmailHandler{
	
	/**
	 * Sends success sync Notification Email to admin.
	 *
	 * This function sends an Email notification to Administrators everytime a donation syncing succeeds. This only works
	 * when "Admin Notification" is set to either "Both" or "Success".
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $post
	 *
	 * @return bool
	 *
	 * @LastUpdated  May 24, 2018
	 */
	public function wfc_donation_admin_success_notification( $post ) {
		require_once(WFC_DONATION_DIR . '../../../../wp-includes/pluggable.php');
		$the_link = '';
		$linkurl = admin_url('admin.php') . '?page=donation-list-page&action=view&ID=';
		if( isset( $post['post_meta_id'] ) ) {
			$the_link = $linkurl . $post['post_meta_id'];
		}

	   	$companyName_value 	= (isset($post['donor_company'])) ? $post['donor_company'] : 'None';
	    $email_value		= (isset($post['donor_email'])) ? $post['donor_email'] : '';
		$donor_phone		= (isset($post['donor_phone'])) ? $post['donor_phone'] : '';
		$donor_title 	    = (isset($post['donor_title'])) ? $post['donor_title'] : '';
	    $first_name_value 	= (isset($post['donor_first_name'])) ? $post['donor_first_name'] : '';
	    $last_name_value 	= (isset($post['donor_last_name'])) ? $post['donor_last_name'] : '';
	    $address_value 	 	= (isset($post['donor_address'])) ? $post['donor_address'] : '';
	    $country_value 		= (isset($post['donor_country'])) ? $post['donor_country'] : '';
	    $state_value 		= (isset($post['donor_state'])) ? $post['donor_state'] : '';
	    $post_code_value 	= (isset($post['donor_postcode'])) ? $post['donor_postcode'] : '';
	    $suburb_value 		= (isset($post['donor_suburb'])) ? $post['donor_suburb'] : '';
	    $currency_value 	= (isset($post['currency'])) ? $post['currency'] : '';
	    $payment_method     = (isset($post['payment_type'])) ? $post['payment_type'] : 'credit_card';
	    $payment_method 	= ($payment_method == 'credit_card') ? 'Credit Card' : 'Direct Debit';

	    if(isset($post['paypal_redirect']) && ($post['paypal_redirect'] == 'true' || $post['paypal_redirect'] == true)){
		    $payment_method = 'PayPal';
		}

		$creditcard_type	= (isset($post['creditcard_type'])) ? $post['creditcard_type'] : '';
		$pdwp_custom_amount = isset($post['pdwp_custom_amount']) ? $post['pdwp_custom_amount'] : '';
	    $donate_amount 	 	= (isset($post['pdwp_sub_amt']) && !empty($post['pdwp_sub_amt'])) ? $post['pdwp_sub_amt'] : $pdwp_custom_amount;
	    $payment_trans_sts 	= (isset($post['PaymentTransaction']['TxnStatus'])) ? $post['PaymentTransaction']['TxnStatus'] : '';
	    $payment_trans_id 	= (isset($post['PaymentTransaction']['TxnId'])) ? $post['PaymentTransaction']['TxnId'] : '';
	  	$payment_trans_date	= (isset($post['PaymentTransaction']['TxnDate'])) ? $post['PaymentTransaction']['TxnDate'] : '';
	    $payment_trans_msg	= (isset($post['PaymentTransaction']['TxnMessage'])) ? $post['PaymentTransaction']['TxnMessage'] : '';
	    $payment_trans_paymentRef = (isset($post['PaymentTransaction']['TxnPaymentRef'])) ? $post['PaymentTransaction']['TxnPaymentRef'] : '';

	    $SFOpportunityId	= (isset($post['SalesforceResponse']['OpportunityId'])) ? $post['SalesforceResponse']['OpportunityId'] : '';
	    $SFRecurringId		= (isset($post['SalesforceResponse']['RecurringId'])) ? $post['SalesforceResponse']['RecurringId'] : '';
	    $SFStatus	  		= (isset($post['SalesforceResponse']['status'])) ? $post['SalesforceResponse']['status'] : '';
	    $SFMessage	    	= (isset($post['SalesforceResponse']['Message'])) ? $post['SalesforceResponse']['Message'] : '';


	  	$site_name 	= "{$first_name_value} {$last_name_value}";

	    $message = "<p><strong>{$site_name} Donation</strong></p></br>";
	    $message .= "<p>Company Name: {$companyName_value}</p>";
	    $message .= "<p>Email: {$email_value}</p>";
		$message .= "<p>Salutation: {$donor_title}</p>";
	    $message .= "<p>First Name: {$first_name_value}</p>";
	    $message .= "<p>Last Name: {$last_name_value}</p>";
		$message .= "<p>Contact Number: {$donor_phone}</p>";
	    $message .= "<p>Address: {$address_value}</p>";
	    $message .= "<p>Country: {$country_value}</p>";
	    $message .= "<p>State: {$state_value}</p>";
	    $message .= "<p>Post Code: {$post_code_value}</p>";
	    $message .= "<p>Suburb: {$suburb_value}</p>";
	    $message .= "<p>Payment Method: {$payment_method}</p>";
		$message .= "<p>Card Type: {$creditcard_type}</p>";
	    $message .= "<p>Currency: {$currency_value}</p>";
	    $message .= "<p>Donation Amount: {$donate_amount}</p>";
	    $message .= "<p>Transaction Status: {$payment_trans_sts}</p>";
	    $message .= "<p>Transaction ID: {$payment_trans_id}</p>";
	    $message .= "<p>Transaction Date: {$payment_trans_date}</p>";
	    $message .= "<p>Transaction Message: {$payment_trans_msg}</p>";
	    $message .= "<p>Transaction Reference: {$payment_trans_paymentRef}</p>";

	   	if( is_array( $SFOpportunityId )){
	   		$errorCode 	  = isset( $SFOpportunityId['errorCode'] ) ? $SFOpportunityId['errorCode'] : '';	
	    	$ErrorMessage = isset( $SFOpportunityId['ErrorMessage'] ) ? $SFOpportunityId['ErrorMessage'] : '';
	    	$message .= "<p>Salesforce Error Code: {$errorCode}</p>";
	    	$message .= "<p>Salesforce Error Message: {$ErrorMessage}</p>";  	
	    }else{
	    	$message .= "<p>Opportunity ID: {$SFOpportunityId}</p>";	    	
	    }

	    if( !empty( $SFRecurringId ) && $SFRecurringId != '' ){
	    	$message .= "<p>Recurring ID: {$SFRecurringId}</p>";
	    }
	    $message .= "<p>Salesforce Status: {$SFStatus}</p>";
	    $message .= "<p>Salesforce Message: {$SFMessage}</p>";


	    $message .= "</br></br><a href='".$the_link."'>Click For More Donation Details</a>";

	    $formid = isset( $post['donation_campaign'] ) ? $post['donation_campaign']  : '';
	    $notification_option = get_post_meta( $formid, 'donation_form_notification_option', true );

	    if( $notification_option == 'both' || $notification_option == 'success' ){

		    $donation_form_from_email 		= get_post_meta( $formid, 'donation_form_from_email', true );
		    $donation_form_from_name  		= get_post_meta( $formid, 'donation_form_from_name', true );
		    $donation_form_recipient_email  = get_post_meta( $formid, 'donation_form_recipient_email', true );

		   	$wppd_from_SD 		= isset( $donation_form_from_email ) ? $donation_form_from_email : '';
			$wppd_from_SD 		= !empty( $wppd_from_SD  ) ? $wppd_from_SD  : 'donation@alphasys.com.au';
			$wppd_from_name_SD  = isset( $donation_form_from_name ) ? $donation_form_from_name : '';
			$wppd_from_name_SD  = !empty( $wppd_from_name_SD  ) ? $wppd_from_name_SD  : 'Donation Success';

	    	$headers  = 'MIME-Version: 1.0' . "\r\n";
		    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		    //$headers .= 'To: ' . $donation_form_recipient_email . "\r\n";
		    $headers .= 'From: ' . $donation_form_from_name . "\r\n" .
		    $headers .= 'Reply-To: ' . $donation_form_from_email . "\r\n" .
		    $headers .= 'X-Mailer: PHP/' . phpversion();


			add_filter('phpmailer_init', function(&$mailer)use($wppd_from_name_SD,$wppd_from_SD) {
				$mailer->From 		= (string)$wppd_from_SD;
				$mailer->FromName 	= (string)$wppd_from_name_SD;
			}, 99999, 1);


			(string)$mail_header_title = 'Donation ( Sync - Successful , Payment - '. $payment_trans_sts .' )';
			return wp_mail($donation_form_recipient_email, $mail_header_title, $message, $headers);
		}
	}
	
	/**
	 * Sends fail sync Notification Email to admin.
	 *
	 * This function sends an Email notification to Administrators everytime a donation syncing fails. This only works
	 * when "Admin Notification" is set to either "Both" or "Failed".
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $post
	 *
	 * @return bool
	 *
	 * @LastUpdated May 24, 2018
	 */
	public function wfc_donation_admin_fail_notification( $post ) {
		require_once(WFC_DONATION_DIR . '../../../../wp-includes/pluggable.php');
		$the_link = '';
		$linkurl = admin_url('admin.php') . '?page=donation-list-page&action=view&ID=';
		if( isset( $post['post_meta_id'] ) ) {
			$the_link = $linkurl . $post['post_meta_id'];
		}

	   	$companyName_value 	= (isset($post['donor_company'])) ? $post['donor_company'] : 'None';
	    $email_value		= (isset($post['donor_email'])) ? $post['donor_email'] : '';
		$donor_phone		= (isset($post['donor_phone'])) ? $post['donor_phone'] : '';
		$donor_title 	    = (isset($post['donor_title'])) ? $post['donor_title'] : '';
	    $first_name_value 	= (isset($post['donor_first_name'])) ? $post['donor_first_name'] : '';
	    $last_name_value 	= (isset($post['donor_last_name'])) ? $post['donor_last_name'] : '';
	    $address_value 	 	= (isset($post['donor_address'])) ? $post['donor_address'] : '';
	    $country_value 		= (isset($post['donor_country'])) ? $post['donor_country'] : '';
	    $state_value 		= (isset($post['donor_state'])) ? $post['donor_state'] : '';
	    $post_code_value 	= (isset($post['donor_postcode'])) ? $post['donor_postcode'] : '';
	    $suburb_value 		= (isset($post['donor_suburb'])) ? $post['donor_suburb'] : '';
	    $currency_value 	= (isset($post['currency'])) ? $post['currency'] : '';
	    $payment_method     = (isset($post['payment_type'])) ? $post['payment_type'] : 'credit_card';
	    $payment_method 	= ($payment_method == 'credit_card') ? 'Credit Card' : 'Direct Debit';

		if(isset($post['paypal_redirect']) && ($post['paypal_redirect'] == 'true' || $post['paypal_redirect'] == true)){
			$payment_method = 'PayPal';
		}
		
		$creditcard_type	= (isset($post['creditcard_type'])) ? $post['creditcard_type'] : '';
		$pdwp_custom_amount = isset($post['pdwp_custom_amount']) ? $post['pdwp_custom_amount'] : '';
		$donate_amount 	 	= (isset($post['pdwp_sub_amt']) && !empty($post['pdwp_sub_amt'])) ? $post['pdwp_sub_amt'] : $pdwp_custom_amount;
	    $payment_trans_sts 	= (isset($post['PaymentTransaction']['TxnStatus'])) ? $post['PaymentTransaction']['TxnStatus'] : '';
	    $payment_trans_id 	= (isset($post['PaymentTransaction']['TxnId'])) ? $post['PaymentTransaction']['TxnId'] : '';
	  	$payment_trans_date	= (isset($post['PaymentTransaction']['TxnDate'])) ? $post['PaymentTransaction']['TxnDate'] : '';
	    $payment_trans_msg	= (isset($post['PaymentTransaction']['TxnMessage'])) ? $post['PaymentTransaction']['TxnMessage'] : '';
	    $payment_trans_paymentRef = (isset($post['PaymentTransaction']['TxnPaymentRef'])) ? $post['PaymentTransaction']['TxnPaymentRef'] : '';

	    $SFStatus	  		= (isset($post['SalesforceResponse']['status'])) ? $post['SalesforceResponse']['status'] : '';
	    $SFMessage	    	= (isset($post['SalesforceResponse']['message'])) ? $post['SalesforceResponse']['message'] : '';

	    $site_name 	= "{$first_name_value} {$last_name_value}";

	    $message = "<p><strong>{$site_name} Donation</strong></p></br>";
	    $message .= "<p>Company Name: {$companyName_value}</p>";
	    $message .= "<p>Email: {$email_value}</p>";
		$message .= "<p>Salutation: {$donor_title}</p>";
	    $message .= "<p>First Name: {$first_name_value}</p>";
	    $message .= "<p>Last Name: {$last_name_value}</p>";
		$message .= "<p>Contact Number: {$donor_phone}</p>";
	    $message .= "<p>Address: {$address_value}</p>";
	    $message .= "<p>Country: {$country_value}</p>";
	    $message .= "<p>State: {$state_value}</p>";
	    $message .= "<p>Post Code: {$post_code_value}</p>";
	    $message .= "<p>Suburb: {$suburb_value}</p>";
	    $message .= "<p>Payment Method: {$payment_method}</p>";
		$message .= "<p>Card Type: {$creditcard_type}</p>";
	    $message .= "<p>Currency: {$currency_value}</p>";
	    $message .= "<p>Donation Amount: {$donate_amount}</p>";
	    $message .= "<p>Transaction Status: {$payment_trans_sts}</p>";
	    $message .= "<p>Transaction ID: {$payment_trans_id}</p>";
	    $message .= "<p>Transaction Date: {$payment_trans_date}</p>";
	    $message .= "<p>Transaction Message: {$payment_trans_msg}</p>";
	    $message .= "<p>Transaction Reference: {$payment_trans_paymentRef}</p>";
	    
	    $message .= "<p>Salesforce Status: {$SFStatus}</p>";
	    $message .= "<p>Salesforce Message: {$SFMessage}</p>";
	    

	    $message .= "</br></br><a href='".$the_link."'>Click For More Donation Details</a>";

	    $formid = isset( $post['donation_campaign'] ) ? $post['donation_campaign']  : '';
	    $notification_option = get_post_meta( $formid, 'donation_form_notification_option', true );

	    if( $notification_option == 'both' || $notification_option == 'failed' ){

	    	$donation_form_from_email 		= get_post_meta( $formid, 'donation_form_from_email', true );
		    $donation_form_from_name  		= get_post_meta( $formid, 'donation_form_from_name', true );
		    $donation_form_recipient_email  = get_post_meta( $formid, 'donation_form_recipient_email', true );

		   	$wppd_from_SD 		= isset( $donation_form_from_email ) ? $donation_form_from_email : '';
			$wppd_from_SD 		= !empty( $wppd_from_SD  ) ? $wppd_from_SD  : 'donation@alphasys.com.au';
			$wppd_from_name_SD  = isset( $donation_form_from_name ) ? $donation_form_from_name : '';
			$wppd_from_name_SD  = !empty( $wppd_from_name_SD  ) ? $wppd_from_name_SD  : 'Donation Success';

	    	$headers  = 'MIME-Version: 1.0' . "\r\n";
		    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		    //$headers .= 'To: ' . $donation_form_recipient_email . "\r\n";
		    $headers .= 'From: ' . $donation_form_from_name . "\r\n" .
		    $headers .= 'Reply-To: ' . $donation_form_from_email . "\r\n" .
		    $headers .= 'X-Mailer: PHP/' . phpversion();


		    add_filter('phpmailer_init', function(&$mailer)use($wppd_from_name_SD,$wppd_from_SD) {
				$mailer->From 		= (string)$wppd_from_SD;
				$mailer->FromName 	= (string)$wppd_from_name_SD;
			}, 99999, 1);


			(string)$mail_header_title = 'Donation ( Sync - Failed , Payment - '.$payment_trans_sts.' )';
			return wp_mail($donation_form_recipient_email, $mail_header_title, $message, $headers);
	    }
	}
	
	/**
	 * Sends Thank you Email to donor.
	 *
	 * This function sends an Email notification to donor after every donation. This will only work if "Thank You Email"
	 * option is enabled on the specified donation form.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param      $post
	 * @param null $type
	 *
	 * @return mixed
	 *
	 * @LastUpdated  May 24, 2018
	 */
	public function wfc_donation_donor_thankyou_email( $post, $type = null ) {

		global $wpdb;

		$response = array();
		
		/*
		*  BUILD USER NOTIFICATION EMAIL
		*/
	    $formid = isset( $post['donation_campaign'] ) ? $post['donation_campaign']  : '';
		$email_subject = get_post_meta( $formid, 'donation_form_thankyou_email_subject', true );
		$email_subject = (!empty( $email_subject )) ? $email_subject  : 'Thank you for your donation.';

	    $message_template = get_post_meta( $formid, 'donation_form_thankyou_email_msg', true );
	    $message_template = (!empty( $message_template )) ? $message_template  : '';

	    $email_value =  (isset($post['donor_email'])) ? $post['donor_email'] : '';
	    $message = str_ireplace('[email]', $email_value, $message_template);

	    $title_value =  (isset($post['donor_title'])) ? $post['donor_title'] : '';
	    $message = str_ireplace('[donor-title]', $title_value, $message_template);
	    
	    $first_name_value =  (isset($post['donor_first_name'])) ? $post['donor_first_name'] : '';
	    $message = str_ireplace('[first-name]', $first_name_value, $message);

	    $last_name_value =  (isset($post['donor_last_name'])) ? $post['donor_last_name'] : '';
	    $message = str_ireplace('[last-name]', $last_name_value, $message);

	    $address_value =  (isset($post['donor_address'])) ? $post['donor_address'] : '';
	    $message = str_ireplace('[address]', $address_value, $message);

	    $country_value =  (isset($post['donor_country'])) ? $post['donor_country'] : '';
	    $message = str_ireplace('[country]', $country_value, $message);

	    $state_value =  (isset($post['donor_state'])) ? $post['donor_state'] : '';
	    $message = str_ireplace('[state]', $state_value, $message);

	    $post_code_value =  (isset($post['donor_postcode'])) ? $post['donor_postcode'] : '';
	    $message = str_ireplace('[post-code]', $post_code_value, $message);

	    $suburb_value =  (isset($post['donor_suburb'])) ? $post['donor_suburb'] : '';
	    $message = str_ireplace('[city]', $suburb_value, $message);
	    $message = str_ireplace('[suburb]', $suburb_value, $message);

	    $SetCurrencySymbol_value = (isset($post['currency'])) ? $post['currency'] : array();
	    $SetCurrencyCode_value 	 = ''; 
	    if( is_array($SetCurrencySymbol_value) ){	    	
		    $currencyArray = explode(",",$SetCurrencySymbol_value);
		    foreach ($currencyArray as $key => $value) {
		    	if( $key == 0 ){
		    		$SetCurrencySymbol_value = $value;
		    	}

		    	if( $key == 1 ){
		    		$SetCurrencyCode_value = $value;
		    	}
		    }
	    }

	    $pdwp_custom_amount = (isset($post['pdwp_custom_amount'])) ? $post['pdwp_custom_amount'] : '';
	    $pd_amount_value =  (isset($post['pdwp_sub_amt'])) ? $post['pdwp_sub_amt'] : $pdwp_custom_amount ;
	    $message = str_ireplace('[amount]', $SetCurrencySymbol_value . $pd_amount_value, $message);
	    $message = str_ireplace('[currency]', $SetCurrencyCode_value, $message);
	    $message = str_ireplace('[campaign-name]', get_the_title($formid), $message);

    	$donation_form_from_email = get_post_meta( $formid, 'donation_form_from_email', true );
	    $donation_form_from_name  = get_post_meta( $formid, 'donation_form_from_name', true );
	    $thankyou_email_enable 	  = get_post_meta( $formid, 'donation_form_thankyou_email_enable', true );

	    $notification_config_notification_name 	= (!empty( $donation_form_from_email )) ? $donation_form_from_email  : '';
	    $notification_config_notification_email = (!empty( $donation_form_from_name )) ? $donation_form_from_name  : '';
	    $notification_thankyou_email_enable 	= (!empty( $thankyou_email_enable )) ? $thankyou_email_enable  : 'false';

	    $message = str_ireplace('[from-name]', $notification_config_notification_name , $message);

		$wppd_from_SD 		= isset( $donation_form_from_email ) ? $donation_form_from_email : '';
		$wppd_from_SD 		= !empty( $wppd_from_SD  ) ? $wppd_from_SD  : 'donation@alphasys.com.au';
		$wppd_from_name_SD  = isset( $donation_form_from_name ) ? $donation_form_from_name : '';
		$wppd_from_name_SD  = !empty( $wppd_from_name_SD  ) ? $wppd_from_name_SD  : 'Donation Success';

    	$headers  = 'MIME-Version: 1.0' . "\r\n";
	    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	    //$headers .= 'To: ' . $email_value . "\r\n";
	    $headers .= 'From: '.$notification_config_notification_name.' <'.$notification_config_notification_email.'>' . "\r\n" .
	    $headers .= 'Reply-To: ' . $donation_form_from_email . "\r\n" .
	    $headers .= 'X-Mailer: PHP/' . phpversion();

		add_filter('phpmailer_init', function(&$mailer)use($wppd_from_name_SD,$wppd_from_SD) {
			$mailer->From 		= (string)$wppd_from_SD;
			$mailer->FromName 	= (string)$wppd_from_name_SD;
		}, 99999, 1);

		/*
		*  Send user notification email
		*/
		if($notification_thankyou_email_enable  == 'true' || $type === 'test'){
			$clean = str_replace(chr(194)," ",$message);
			$form_title = get_the_title($formid);
			(string)$donationTitle = !empty( $form_title ) ? $form_title : 'your';
			(string)$mail_header_title = $email_subject;
			$response['success'] = wp_mail($email_value, $mail_header_title, $clean, $headers);
		}

		return $response;
	}
}	