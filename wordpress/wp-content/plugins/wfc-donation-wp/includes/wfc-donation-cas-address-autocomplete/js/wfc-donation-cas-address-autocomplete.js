console.log('WFC - Donation CAS Google Address Rest API Status : OK');

var casaddressvalidation_param = wfc_don_casaddressvalidation_param;
var enable_validation = ( typeof casaddressvalidation_param.enable_validation != 'undefined' ? casaddressvalidation_param.enable_validation : 'false' );
var google_api_key = ( typeof casaddressvalidation_param.google_api_key != 'undefined' ? casaddressvalidation_param.google_api_key : '' );

var register_custom_set_address = casaddressvalidation_param.register_custom_set_address;
register_custom_set_address = JSON.parse(register_custom_set_address)

/**
 * Google Maps Autocomplete
 */
var autocomplete_element_dv  = [];
var street_number_element_dv = '';

var placeSearch_dv = '';
var autocomplete_dv = [];
var component_dv = '';
var val_dv = '';
var place_dv = '';
var addressType_dv = '';
var hasRoute_dv = '';

var componentForm = {
    subpremise: 'short_name',
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
    postal_code: 'short_name'
};

if (Object.keys(register_custom_set_address).length === 0 && register_custom_set_address.constructor === Object){
    console.log('WFC - Donation CAS Google Address Rest Field Sets Status : No field sets registered.');
}else {
    setupGooogleAutocomplete(register_custom_set_address);
}


function setupGooogleAutocomplete(set_of_address){

    set_of_address.forEach(function(item, index) {
        var itemnum = index + 1;
        var itemkey = 'item-' + itemnum.toString();
        var include_set = ( typeof item[itemkey]['values']['include_set'] != 'undefined' ? item[itemkey]['values']['include_set'] : 'false' );
        var cas_address_field = ( typeof item[itemkey]['values']['cas_address_field'] != 'undefined' ? item[itemkey]['values']['cas_address_field'] : '' );
        var cas_name = ( typeof item[itemkey]['values']['cas_name'] != 'undefined' ? item[itemkey]['values']['cas_name'] : '' );
        var cas_address_field_element =  document.getElementById(cas_address_field);

        if (typeof(cas_address_field_element) != 'undefined' && cas_address_field_element != null) {

            if (include_set == 'true' && cas_address_field.length != 0 && cas_name.length != 0) {
                autocomplete_element_dv['autocomplete_element-' + cas_name] = "";
                autocomplete_dv['autocomplete-' + cas_name] = "";
                cas_address_field_element.setAttribute('wfc-cas-google-autocomplete', cas_name);
            }
        }
    });

}


function initCASAutocomplete(cas_address_field,cas_name) {
    autocomplete_element_dv['autocomplete_element-' + cas_name]  = document.getElementById(cas_address_field);

    /**
     * Create the autocomplete object, restricting the search to geographical
     * location types.
     */
    autocomplete_dv['autocomplete-' + cas_name] = new google.maps.places.Autocomplete(
        document.getElementById(cas_address_field), {types: ['geocode']});

}


/**
 * Bias the autocomplete object to the user's geographical location,
 * as supplied by the browser's 'navigator.geolocation' object.
 */
function CASgeolocate(cas_name) {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
                center: geolocation,
                radius: position.coords.accuracy
            });
            //autocomplete_dv['autocomplete-' + cas_name].setBounds(circle.getBounds());
        });
    }
}

jQuery(document).ready(function($){

    setTimeout(function(){

        if(enable_validation == 'true' && google_api_key.length != 0 ) {
            register_custom_set_address.forEach(function(item, index) {
                var itemnum = index + 1;
                var itemkey = 'item-' + itemnum.toString();
                //console.log(item[itemkey]['values']);
                var include_set = ( typeof item[itemkey]['values']['include_set'] != 'undefined' ? item[itemkey]['values']['include_set'] : 'false' );
                var cas_address_field = ( typeof item[itemkey]['values']['cas_address_field'] != 'undefined' ? item[itemkey]['values']['cas_address_field'] : '' );
                var cas_name = ( typeof item[itemkey]['values']['cas_name'] != 'undefined' ? item[itemkey]['values']['cas_name'] : '' );
                var cas_address_field_element =  document.getElementById(cas_address_field);

                if (typeof(cas_address_field_element) != 'undefined' && cas_address_field_element != null) {

                    if (include_set == 'true' && cas_address_field.length != 0 && cas_name.length != 0) {
                        CASEnableAutoComplete(item,itemkey);
                    }
                }
            });

            //
        }
    }, 1000);

    function CASEnableAutoComplete(set_address_item,itemkey){
        var cas_address_field = ( typeof set_address_item[itemkey]['values']['cas_address_field'] != 'undefined' ? set_address_item[itemkey]['values']['cas_address_field'] : '' );
        var cas_name = ( typeof set_address_item[itemkey]['values']['cas_name'] != 'undefined' ? set_address_item[itemkey]['values']['cas_name'] : '' );

        var autocompleteElement = $('[wfc-cas-google-autocomplete="'+cas_name+'"]');

        if( autocompleteElement.length != 0 ){
            autocompleteElement.attr( 'placeholder', '' );

            if(enable_validation == 'true' ) {

                autocompleteElement.attr('autocomplete', 'new-password');

                autocompleteElement.attr('onFocus', "CASgeolocate('"+cas_name+"')");
                setTimeout(function(){
                    initCASAutocomplete(cas_address_field,cas_name);
                }, 2000);

                autocompleteElement.on('focusout', function(){
                    setTimeout(function(){
                        if($.trim(autocompleteElement.val()) ==''){
                            $('[pdwp-address="street_number"]').val('');
                            $('[pdwp-address="route"]').val('');
                            $('[pdwp-address="country"]').val('');
                            $('[pdwp-address="administrative_area_level_1"]').val('');
                            $('[pdwp-address="postal_code"]').val('');
                            $('[pdwp-address="locality"]').val('');
                        }
                        else{

                            var address_value = autocompleteElement.val();
                            var specify_country ='';

                            if($('[pdwp-address="country"]').val()==''||$('[pdwp-address="country"]').val().length === 0){
                                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address_value+'&key='+google_api_key, function (data) {
                                    if(data['status']=='ZERO_RESULTS'){
                                        console.log('ZERO_RESULTS');
                                    }else{
                                        CASfillInAddress(set_address_item,itemkey,data.results[0]['address_components']);
                                    }
                                });
                            }
                            else{
                                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+$('[pdwp-address="country"]').val()+'&key='+google_api_key, function (data) {
                                    $.each( data['results'][0]['address_components'], function( key, value ) {
                                        if(value['types'][0]=='country'){
                                            var specify_country = '&components=country:'+value['short_name'];

                                        }
                                    });
                                });
                                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+address_value+specify_country+'&key='+google_api_key, function (data) {
                                    if(data['status']=='ZERO_RESULTS'){
                                        $('#adress_validation').text('* Invalid address');
                                    }else{
                                        $('#adress_validation').text('');

                                    }
                                });
                            }
                        }
                    }, 1000);
                });

                autocompleteElement.on('click', function(){
                   // $('[pdwp-address="route"]').val('');
                });
            }
        }
    }

    function CASfillInAddress(set_address_item,itemkey,result) {
        var street_number = '';
        var route = '';
        var country = '';
        var administrative_area_level_1 = '';
        var postal_code = '';
        var locality = '';

        result.forEach(function(item, index) {
            var types = ( typeof item['types'] != 'undefined' ? item['types'] : '' );

            if ($.inArray( "street_number", types ) >  -1){
                if (street_number == ''){
                    street_number = ( typeof item['long_name'] != 'undefined' ? item['long_name'] : '' );
                    //console.log(street_number);
                }
            }else if ($.inArray( "route", types ) >  -1){
                if (route == ''){
                    route = ( typeof item['long_name'] != 'undefined' ? item['long_name'] : '' );
                    //console.log(route);
                }
            }else if ($.inArray( "country", types ) >  -1){
                if (country == '') {
                    country = (typeof item['long_name'] != 'undefined' ? item['long_name'] : '');
                    //console.log(country);
                }
            }else if ($.inArray( "administrative_area_level_1", types ) >  -1){
                if (administrative_area_level_1 == '') {
                    administrative_area_level_1 = (typeof item['long_name'] != 'undefined' ? item['long_name'] : '');
                    //console.log(administrative_area_level_1);
                }
            }else if ($.inArray( "postal_code", types ) >  -1){
                if (postal_code == '') {
                    postal_code = (typeof item['long_name'] != 'undefined' ? item['long_name'] : '');
                    //console.log(postal_code);
                }
            }else if ($.inArray( "locality", types ) >  -1){
                if (locality == '') {
                    locality = (typeof item['long_name'] != 'undefined' ? item['long_name'] : '');
                    //console.log(locality);
                }
            }

        });

        // console.log(street_number);
        // console.log(route);
        // console.log(country);
        // console.log(administrative_area_level_1);
        // console.log(postal_code);
        // console.log(locality);

        var field_val = {
            'street_number' : street_number,
            'route' : route,
            'country' : country,
            'administrative_area_level_1' : administrative_area_level_1,
            'postal_code' : postal_code,
            'locality' : locality,
        };
        CASPopulateField(set_address_item,itemkey,field_val);
    }

    function CASPopulateField(set_address_item,itemkey,field_val) {

        var cas_address_field = ( typeof set_address_item[itemkey]['values']['cas_address_field'] != 'undefined' ? set_address_item[itemkey]['values']['cas_address_field'] : '' );
        var cas_country_field = ( typeof set_address_item[itemkey]['values']['cas_country_field'] != 'undefined' ? set_address_item[itemkey]['values']['cas_country_field'] : '' );
        var cas_state_field = ( typeof set_address_item[itemkey]['values']['cas_state_field'] != 'undefined' ? set_address_item[itemkey]['values']['cas_state_field'] : '' );
        var cas_postcode_field = ( typeof set_address_item[itemkey]['values']['cas_postcode_field'] != 'undefined' ? set_address_item[itemkey]['values']['cas_postcode_field'] : '' );
        var cas_suburb_field = ( typeof set_address_item[itemkey]['values']['cas_suburb_field'] != 'undefined' ? set_address_item[itemkey]['values']['cas_suburb_field'] : '' );
        var include_set = ( typeof set_address_item[itemkey]['values']['include_set'] != 'undefined' ? set_address_item[itemkey]['values']['include_set'] : '' );
        var cas_name = ( typeof set_address_item[itemkey]['values']['cas_name'] != 'undefined' ? set_address_item[itemkey]['values']['cas_name'] : '' );


        if (include_set == 'true'){
            if( $('#'+cas_address_field).length != 0 ){
                if(field_val['street_number'] == '') {
                    $('#'+cas_address_field).val(field_val['route']);
                }else {
                    $('#'+cas_address_field).val(field_val['street_number'] + ' ' + field_val['route'] );
                }
            }
            if( $('#'+cas_country_field).length != 0 ) {
                $('#'+cas_country_field).val(field_val['country']);
            }
            if( $('#'+cas_state_field).length != 0 ) {
                $('#'+cas_state_field).val(field_val['administrative_area_level_1']);
            }
            if( $('#'+cas_postcode_field).length != 0 ) {
                $('#'+cas_postcode_field).val(field_val['postal_code']);
            }
            if( $('#'+cas_suburb_field).length != 0 ) {
                $('#'+cas_suburb_field).val(field_val['locality']);
            }
        }

        //console.log(set_address_item);
    }
});
