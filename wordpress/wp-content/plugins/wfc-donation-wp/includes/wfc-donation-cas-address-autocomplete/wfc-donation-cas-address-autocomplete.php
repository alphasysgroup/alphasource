<?php

/**
 * Class WFC_DonationCASAddressAutocomplete
 *
 * This class handles out the custom address set "Address Autocomplete".
 * @author Junjie Canonio <junjie@alphasys.com.au>
 * @since 1.0.0
 *
 * @LastUpdated : April 26, 2018
 */
class WFC_DonationCASAddressAutocomplete {

	/**
	* Name : __constructrender_recent_posts_list
	* Description: intialize values and resources needed.
	* LastUpdated : April 10, 2019
	* @author Junjie Canonio
	* @since  1.0.0
	* @param
	* @return
	*/
	public function __construct() {
	}


	/**
	 * Enqueues styles and scripts for Donation Form.
	 *
	 *
	 * @param array $styles Array of styles slug name
	 * @param array $scripts Array of scripts slug name
	 *
	 * @return void
	 *
	 * @LastUpdated  August 22, 2018
	 * @since  1.0.0
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 */
	public function enqueue_styles_scripts() {

	}

	/**
	* Enqueues styles and scripts for Donation Form.
	*
	* @LastUpdated  June 28, 2019
	* @since  1.0.0
	*
	* @author Junjie Canonio <junjie@alphasys.com.au>
	*/
	public static function setup_googleaddressautocomplete() {
		$setting = get_option( 'pronto_donation_settings', 0 );

		$enable_validation 	= isset( $setting->enable_validation ) ? $setting->enable_validation : 'false';
		$google_api_key 	= isset( $setting->google_api_key ) ? $setting->google_api_key : '';
		$register_custom_set_address = isset( $setting->register_custom_set_address ) ? $setting->register_custom_set_address : '[]';

		if($enable_validation == 'true'){
			if ($google_api_key != '') {
				wp_enqueue_script(
					'webforce-connect-donation-google-address-validation-lib',
					'https://maps.googleapis.com/maps/api/js?key=' . $google_api_key . '&language=en&libraries=places',
					true
				);

				/*
				* Register the JS SCRIPT for custom address set google address validation.
				*/
				wp_enqueue_script(
					'wfc-google-cas-address-validation-js',
					plugin_dir_url( __FILE__ ) . 'js/wfc-donation-cas-address-autocomplete.js',
					array( 'jquery' ),
					'',
					false
				);

				/*
				* Localize "wfc_don_casaddressvalidation_param" on 'webforce-connect-donation-google-cas-address-validation-js' script
				*/
				wp_localize_script(
					'wfc-google-cas-address-validation-js',
					'wfc_don_casaddressvalidation_param',
					array(
						'enable_validation' => $enable_validation,
						'google_api_key' => $google_api_key,
						'register_custom_set_address' => $register_custom_set_address,
					)
				);
			}
		}
	}
}
new WFC_DonationCASAddressAutocomplete();