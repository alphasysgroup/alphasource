<?php

/**
 * Class Webforce_Connect_Donation_Deactivator
 *
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 *
 * @since      1.0.0
 * @package    Webforce_Connect_Donation
 * @author     AlphaSys Pty. Ltd. <https://alphasys.com.au>
 */
class Webforce_Connect_Donation_Deactivator {
	
	
	/**
	 * Responsible for deactivating the plugin.
	 *
	 * This function will also delete the generated 'Thank you' and 'Failure' pages upon deactivation.
	 */
	public static function deactivate() {
		global $wpdb;
		// Get Post Page for Messages 
		$wpdb->get_results("DELETE FROM $wpdb->posts WHERE post_type = 'page' AND post_title = 'Failure - Pronto Donation WPPlugin'");
		$wpdb->get_results("DELETE FROM $wpdb->posts WHERE post_type = 'page' AND post_title = 'Thank You - Pronto Donation WPPlugin'");
	}

}
