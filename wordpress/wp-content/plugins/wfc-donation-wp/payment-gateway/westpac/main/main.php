<?php

	/**
	* Westpac Wordpress Hooks
	* this class and its methods are cloned from the original classes of ezidebit payment gateway
	*/
	class Westpac_WP_Hooks
	{
		/**
		 * The Westpac main class
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $parent    The Westpac main class
		 */
		public $parent = null;

		/**
		 * The main class of the plugin
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $includes    The main class of the plugin
		 */
		public $includes = null;
		
		/**
		 * Westpac_WP_Hooks constructor.
		 *
		 * @param $a
		 * @param $b
		 */
		function __construct( $a, $b )
		{

			/*
			 * The Westpac main class
			 */
			$this->parent = $a;

			/*
			 * The main class of the plugin
			 */
			$this->includes = $b;

			/*
			 * Westpac scripts
			 */
			add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_westpacscripts' ) );



			add_action( 'wp_ajax_wfc_westpac_generate_single_use_token', array( $this, 'wfc_westpac_generate_single_use_token' ) );
			add_action( 'wp_ajax_nopriv_wfc_westpac_generate_single_use_token', array( $this, 'wfc_westpac_generate_single_use_token' ) );

			add_action( 'wp_ajax_wfc_westpac_create_customer', array( $this, 'wfc_westpac_create_customer' ) );
			add_action( 'wp_ajax_nopriv_wfc_westpac_create_customer', array( $this, 'wfc_westpac_create_customer' ) );

			add_action( 'wp_ajax_wfc_westpac_create_transaction', array( $this, 'wfc_westpac_create_transaction' ) );
			add_action( 'wp_ajax_nopriv_wfc_westpac_create_transaction', array( $this, 'wfc_westpac_create_transaction' ) );
		}

		/**
		 * Fetch payment gateway settings.
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param $donate
		 *
		 * @return array
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 1, 2020
		 * @LastUpdatedBy Vincent Liong
		 */
		public function wfc_get_sp_config( $donate ) {

			/*
			* get gateway setting option
			*/
			$gateway = get_option( $donate['gatewayid'] );

			return array(
				'publishable_api_key' 	=> isset($gateway['wfc_donation_publishable_api_key']) ? $gateway['wfc_donation_publishable_api_key'] : '',
				'secret_api_key' 		=> isset($gateway['wfc_donation_secret_api_key']) ? $gateway['wfc_donation_secret_api_key'] : '',
				'merchant_id'  			=> isset($gateway['wfc_donation_merchant_id']) ? $gateway['wfc_donation_merchant_id'] : '',
				'api_base_url'  		=> isset($gateway['wfc_donation_api_base_url']) ? $gateway['wfc_donation_api_base_url'] : ''
			);
		}


		/**
		 * Function Name: get_basic_auth_headers
		 * Generate the auth headers required for basic auth callouts
		 * 
		 * @author	Vincent Liong <edil@alphasys.com.au>
		 * @return	String		the base64 encoded credentials (aready includes the header key) 	
		 * @since   5.0.0
		 */
		public function get_basic_auth_headers( $username = false, $password = false ) {
			if ( $username === false && $password === false ) {
				return '';
			} 

			return 'Authorization: Basic ' . base64_encode( "{$username}:{$password}" );
		}
		

		/**
		 * Function Name: wfc_westpac_generate_single_use_token
		 * The callout method for creating the single use token for creating customers
		 * 
		 * @author	Vincent Liong <edil@alphasys.com.au>
		 * @return	String		JSON String of the Westpac Payway singleUseToken 	
		 * @since   5.0.0
		 *
		 * @LastUpdated  May 1, 2020
		 */
		public function wfc_westpac_generate_single_use_token() {
			if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_westpac_generate_single_use_token' ) {
				
				/*
				 * prevent processing on direct access endpoint
				 */
				if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
					die( 'busted' );
				}

				$gateway_settings 	= $this->wfc_get_sp_config( $_POST['param']['donation'] );
				$payload 			= http_build_query( $_POST['ccd'] );
				$curl 				= curl_init();

				curl_setopt_array( $curl, array(
					CURLOPT_URL 				=> 'https://api.payway.com.au/rest/v1/single-use-tokens',
					// CURLOPT_URL 				=> $gateway_settings['api_base_url'] . '/single-use-tokens',
					CURLOPT_RETURNTRANSFER 		=> true,
					CURLOPT_ENCODING 			=> "",
					CURLOPT_MAXREDIRS 			=> 10,
					CURLOPT_TIMEOUT 			=> 0,
					CURLOPT_FOLLOWLOCATION 		=> true,
					CURLOPT_HTTP_VERSION 		=> CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST 		=> "POST",
					CURLOPT_POSTFIELDS 			=> $payload,
					CURLOPT_HTTPHEADER 			=> array(
						$this->get_basic_auth_headers( $gateway_settings['publishable_api_key'], '' ),
						"Content-Type: application/x-www-form-urlencoded"
					),
				) );

				$response 		= curl_exec( $curl );
				$info 			= curl_getinfo( $curl );
				$status_code 	= curl_getinfo( $curl, CURLINFO_HTTP_CODE );
				if ($response === false) {
					$response 	= curl_error( $curl) ;
				}
					
				curl_close( $curl );

				if ( ! headers_sent() ) {
					header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
					if ( null !== $status_code ) {
						status_header( $status_code );
					}
				}

				echo $response;
				
				wp_die();
			}
		}


		/**
		 * Function Name: wfc_westpac_create_customer
		 * The callout method for creating the customer 
		 * 
		 * @author	Vincent Liong <edil@alphasys.com.au>
		 * @return	String		JSON String of the Westpac Payway customer model 	
		 * @since   5.0.0
		 *
		 * @LastUpdated  May 1, 2020
		 */
		public function wfc_westpac_create_customer() {
			if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_westpac_create_customer' ) {
				
				/*
				 * prevent processing on direct access endpoint
				 */
				if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
					die( 'busted' );
				}

				$gateway_settings 	= $this->wfc_get_sp_config( $_POST['param']['donation'] );
				$customer_data		= http_build_query( array(
					'singleUseTokenId'	=> $_POST['token'],
					'merchantId'		=> $gateway_settings['merchant_id'],
					'customerName'		=> trim( "{$_POST['param']['donation']['donor_first_name']} {$_POST['param']['donation']['donor_last_name']}" ),
					'emailAddress'		=> $_POST['param']['donation']['donor_email'],
					'phoneNumber'		=> $_POST['param']['donation']['donor_phone'],
					'street1'			=> $_POST['param']['donation']['donor_address'],
					'cityName'			=> $_POST['param']['donation']['donor_suburb'],
					'state'				=> $_POST['param']['donation']['donor_state'],
					'postalCode'		=> $_POST['param']['donation']['donor_postcode']
				) );
				$curl 				= curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL 			=> 'https://api.payway.com.au/rest/v1/customers',
					CURLOPT_RETURNTRANSFER 	=> true,
					CURLOPT_ENCODING 		=> "",
					CURLOPT_MAXREDIRS 		=> 10,
					CURLOPT_TIMEOUT 		=> 0,
					CURLOPT_FOLLOWLOCATION 	=> true,
					CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST 	=> "POST",
					CURLOPT_POSTFIELDS 		=> $customer_data,
					CURLOPT_HTTPHEADER 		=> array(
						$this->get_basic_auth_headers( $gateway_settings['secret_api_key'], '' ),
						"Content-Type: application/x-www-form-urlencoded"
					),
				));

				$response 		= curl_exec( $curl );
				$info 			= curl_getinfo( $curl );
				$status_code 	= curl_getinfo( $curl, CURLINFO_HTTP_CODE );
				if ($response === false) {
					$response 	= curl_error( $curl) ;
				}
					
				curl_close( $curl );

				if ( ! headers_sent() ) {
					header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
					if ( null !== $status_code ) {
						status_header( $status_code );
					}
				}

				echo $response;
				
				wp_die();
			}
		}


		/**
		 * Function Name: wfc_westpac_create_transaction
		 * The callout method for creating the transactions
		 * 
		 * @author	Vincent Liong <edil@alphasys.com.au>
		 * @return	String		JSON String of the Westpac Payway transaction model 	
		 * @since   5.0.0
		 *
		 * @LastUpdated  May 1, 2020
		 */
		public function wfc_westpac_create_transaction() {
			if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_westpac_create_transaction' ) {
				
				/*
				 * prevent processing on direct access endpoint
				 */
				if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
					die( 'busted' );
				}

				$currency_obj		= explode(",", $_POST['param']['donation']['currency']);
				$gateway_settings 	= $this->wfc_get_sp_config( $_POST['param']['donation'] );
				$customer_data		= http_build_query( array(
					'merchantId'		=> $gateway_settings['merchant_id'],
					'customerNumber'	=> $_POST['customerNumber'],
					'principalAmount'	=> $_POST['param']['donation']['pdwp_sub_amt'],
					'currency'			=> strtolower( $currency_obj[1] ),
					'transactionType'	=> 'payment'
				) );
				$curl 				= curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL 			=> 'https://api.payway.com.au/rest/v1/transactions',
					CURLOPT_RETURNTRANSFER 	=> true,
					CURLOPT_ENCODING 		=> "",
					CURLOPT_MAXREDIRS 		=> 10,
					CURLOPT_TIMEOUT 		=> 0,
					CURLOPT_FOLLOWLOCATION 	=> true,
					CURLOPT_HTTP_VERSION 	=> CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST 	=> "POST",
					CURLOPT_POSTFIELDS 		=> $customer_data,
					CURLOPT_HTTPHEADER 		=> array(
						$this->get_basic_auth_headers( $gateway_settings['secret_api_key'], '' ),
						"Content-Type: application/x-www-form-urlencoded"
					),
				));

				$response 		= curl_exec( $curl );
				$info 			= curl_getinfo( $curl );
				$status_code 	= curl_getinfo( $curl, CURLINFO_HTTP_CODE );
				if ($response === false) {
					$response 	= curl_error( $curl) ;
				}
					
				curl_close( $curl );

				if ( ! headers_sent() ) {
					header( 'Content-Type: application/json; charset=' . get_option( 'blog_charset' ) );
					if ( null !== $status_code ) {
						status_header( $status_code );
					}
				}

				echo $response;
				
				wp_die();
			}
		}
		

		/**
		 * Loads script for westpac.
		 * 
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 * @return void
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 1, 2020
		 * @LastUpdatedBy Vincent Liong
		 */
		public function wfc_load_westpacscripts() {
			global $post;
			
			/*
			* westpac js library
			*/
			wp_register_script(
				'webforce-connect-donation-westpac_js',
				plugin_dir_url( __FILE__ ) . '../js/westpac.js',
				array(),
				1.0,
				true
			);

			/**
			 * Register scripts for parsing date and time
			 */
			wp_register_script(
				'webforce-connect-donation-moment_with_loacale_js',
				'//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment-with-locales.min.js',
				array(),
				'2.24.0',
				true
			);
			
			wp_register_script(
				'webforce-connect-donation-moment_tz_with_data_js',
				'//cdnjs.cloudflare.com/ajax/libs/moment-timezone/0.5.28/moment-timezone-with-data.min.js',
				array('webforce-connect-donation-moment_with_loacale_js'),
				'0.5.28',
				true
			);
		}
	}