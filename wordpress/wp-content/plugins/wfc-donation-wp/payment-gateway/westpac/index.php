<?php 
	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'Westpac' ) ) {
		
		/**
		 * Class Westpac
		 */
		class Westpac
		{

			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-westpac_lib_js',
				'webforce-connect-donation-westpac_js'
			);
			
			function __construct()
			{

			}
			
			/**
			 * Load westpac settings template.
			 *
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 3, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Loads Westpac WordPress core hooks.
			 * LastUpdated : May 3, 2018
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param string|null $class  plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 3, 2018
			 */
			public function loadWpCore( $class ) {
				require_once plugin_dir_path(__FILE__) . 'main/main.php';
				new Westpac_WP_Hooks( $this, $class );
			}
		}
	}