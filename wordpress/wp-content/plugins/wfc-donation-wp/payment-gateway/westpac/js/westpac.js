var j = jQuery;
var WestpacGateway = function (config) {
	this.config = config;
	j('#pdwp-btn').show();

	j('#wfc_donation-payment-details-toogle-container').show();
	j("label[for=payment_type_credit_card]").click();

};

/**
 * formatDate
 * Format date from local to UTC
 * 
 * @param date String 	date to be formatted in the following format: `DD MMM YYYY HH:mm z` 
 * @return String		the formatted date
 */
WestpacGateway.prototype.formatDate = function (date) {

	//convert date from AEST to UTC
	var dateInUTC = moment(date, 'DD MMM YYYY HH:mm AEST').format('YYYY-MM-DD');

	if (date.includes("AEST")) {
		dateInUTC = moment.tz(date, "DD MMM YYYY HH:mm AEST", "Australia/Sydney").utc().format('YYYY-MM-DD');
	}
	
	return dateInUTC.toString();
};


/**
 * update card details and mask some of the characters
 * 
 * @param cc	Object	the credit card data 
 * @return 		Object	the formatted cc object
 */
WestpacGateway.prototype.clean = function (cc) {

	var creditcarddetails = {
		card_number: cc.card_number,
		name_on_card: cc.name_on_card,
		expiry_month: cc.expiry_month,
		expiry_year: cc.expiry_year,
		ccv: cc.ccv
	};

	var regex	= /\d(?=\d{4})/mg;
	var str 	= creditcarddetails.card_number.replace(/ /g, '');
	var subst 	= '*';

	creditcarddetails.card_number 	= str.replace(regex, subst);
	creditcarddetails.ccv 			= creditcarddetails.ccv.replace(/\S/gi, '*');

	return creditcarddetails;
};

/**
 * update bank details and mask some of the characters
 * 
 * @param bank	Object	the bank data 
 * @return 		Object	the formatted bank object
 */
WestpacGateway.prototype.cleanBank = function (bank) {
	var bank_details = {
		bank_code: (typeof bank.bank_code != 'undefined' ? bank.bank_code : ''),
		bank_number: (typeof bank.bank_number != 'undefined' ? bank.bank_number : ''),
		bank_name: (typeof bank.bank_name != 'undefined' ? bank.bank_name : '')
	};

	var regex = /\d(?=\d{4})/mg;
	var strbank = bank_details.bank_number.replace(/ /g, '');
	var strcode = bank_details.bank_code.replace(/ /g, '');
	var subst = 'x';

	bank_details.bank_code = strcode.replace(regex, subst);
	bank_details.bank_number = strbank.replace(regex, subst);
	return bank_details;
};

/**
 * perform the tokenize transaction
 * 
 * @param param	Object	the gateway details 
 * @param cc	Object	the credit card data 
 */
WestpacGateway.prototype.tokenizePayment = function (param, cc) {
	var self = this;
	return new Promise(function (resolve, reject) {
		var donation = param.donation;
		try {
			var donate = param.donation;
			var successCallback = function (data) {

				j('#pdwp-btn').attr('disabled', 'disabled');

				//var paymentDate = new Date(donate.timestamp * 1000);

				donate.payment_response 	= data;
				donate.PaymentTransaction 	= {};

				if (data.status == 'approved') {
					donate.statusCode = 1;
					donate.statusText = 'APPROVED';
					donate.PaymentTransaction.TxnStatus = 'Success';
				} else {
					donate.statusCode = 0;
					donate.statusText = 'FAILED';
					donate.PaymentTransaction.TxnStatus = 'Failed';
				}
				
				donate.PaymentTransaction.TxnId				= data.receiptNumber;
				donate.PaymentTransaction.TxnDate			= self.formatDate( data.transactionDateTime );
				donate.PaymentTransaction.TxnMessage		= data.status + ' - ' + data.responseText;
				donate.PaymentTransaction.TxnPaymentRef		= `https://api.payway.com.au/rest/v1/transactions/${data.transactionId}`;

				donate.TokenCustomerID 	= data.customerNumber;
				donate.card_details 	= self.clean(cc);

				resolve({
					status: 'success',
					response: data,
					donation: donate
				});
			};

			var errorCallback = function( data ) {
				resolve( {
					status : 'error',
					response: data,
					donation
				} );
			};

			self.generateToken(param, cc)
				.then(function (gtresponse) {
					console.log('generateToken: ', gtresponse);

					if ( gtresponse.status == 'success' ) {
						return self.createCustomer(param, gtresponse.token.singleUseTokenId)
					} else { 
						errorCallback(gtresponse.response); 
					}
				})
				.then(function (ccResponse) {
					console.log('createCustomer: ', ccResponse);

					if ( ccResponse.status == 'success' ) {
						return self.createTransaction(param, ccResponse.customer.customerNumber)
					} else { 
						errorCallback(ccResponse.response); 
					}
				})
				.then(function (ctResponse) {
					console.log('createTransaction: ', ctResponse);

					if ( ctResponse.status == 'success' ) {
						successCallback(ctResponse.transaction);
					} else { 
						errorCallback(ctResponse.response); 
					}
				})
				.catch( errorCallback );

			j('#pdwp-btn').click();
		} catch(e) {
			reject( e );
		}
	});
};

/**
 * Re-map the error object/array returned by the api calls to be readable by the base error display logic
 * 
 * @param response	Object	the error object 
 * @return 			Mixed	the formatted error Array/object
 */
WestpacGateway.prototype.formatErrorMessages = function(response) {
	try{ 
		var formattedErrors = [];
		if (typeof response == 'object' && response!=null && 'data' in response && Array.isArray(response.data)) {
			j.each(response.data, function(k, v){
				if ( ! 'fieldName' in v || ! 'message' in v ) {
					return;
				}

				formattedErrors.push(`<strong>[${v.fieldName}]</strong> ${v.message}`);
			});
		} else if (typeof response == 'object' && response!=null && 'message' in response ) {
			formattedErrors.push(`${response.message}`);
		}
		
		return formattedErrors;
	} catch (e) {
		console.log(e)
		return response;
	}
}

/**
 * Perform the transaction callout
 * 
 * @param param				Object	the gateway details 
 * @param customerNumber	Object	the credit card data 
 * @return 					Promise	the callout response of create transaction API
 */
WestpacGateway.prototype.createTransaction = function (param, customerNumber) {
	var self = this;
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				action:'wfc_westpac_create_transaction',
				param,
				customerNumber,
				nonce: pdwp_ajax.nonce,
			},
			dataType: 'json',
			success: function( resp ) {
				if ( resp != null && 'transactionId' in resp ) {
					resolve( {
						status: 'success',
						transaction: resp,
						donation: param.donation
					} );
				} else {
					resolve( {
						status: 'error',
						response: self.formatErrorMessages(resp),
						donation: param.donation
					} );
				}
			},
			error: function( e ) {
				console.log('err');
				resolve( {
					status: 'error',
					response: e,
					donation: param.donation
				} );
			}
		});
	});
};

/**
 * Perform the create customer callout
 * 
 * @param param		Object	the gateway details 
 * @param token		string	the single use token 
 * @return 			Promise	the callout response of create customer API
 */
WestpacGateway.prototype.createCustomer = function (param, token) {
	var self = this;
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				action:'wfc_westpac_create_customer',
				param,
				token,
				nonce: pdwp_ajax.nonce,
			},
			dataType: 'json',
			success: function( resp ) {
				if ( resp != null && 'customerNumber' in resp ) {
					resolve( {
						status : 'success',
						customer : resp,
						donation : param.donation
					} );
				} else {
					resolve( {
						status : 'error',
						response : self.formatErrorMessages(resp),
						donation : param.donation
					} );
				}
			},
			error: function( e ) {
				resolve( {
					status : 'error',
					response : e,
					donation : param.donation
				} );
			}
		});
	});
};

/**
 * Perform the single token generation callout
 * 
 * @param param		Object	the gateway details 
 * @param cc		Object	the credit card details 
 * @return 			Promise	the callout response of generate single use token API
 */
WestpacGateway.prototype.generateToken = function (param, cc) {
	var self = this;
	return new Promise(function (resolve, reject) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				action:'wfc_westpac_generate_single_use_token',
				nonce: pdwp_ajax.nonce,
				ccd : {
					cardNumber: cc.card_number,
					cardholderName: cc.name_on_card,
					cvn: cc.ccv,
					expiryDateMonth: cc.expiry_month,
					expiryDateYear: cc.expiry_year,
					paymentMethod: 'creditCard'
				},
				param,
			},
			dataType: 'json',
			success: function( resp ) {
				if ( resp != null && 'singleUseTokenId' in resp ) {
					resolve( {
						status : 'success',
						token: resp,
						donation : param.donation
					} );
				} else {
					resolve( {
						status : 'error',
						response : self.formatErrorMessages(resp),
						donation : param.donation
					} );
				}
			},
			error: function( e ) {
				resolve( {
					status : 'error',
					response : e,
					donation : param.donation
				} );
			}
		});
	});
};


/**
 * Process the payment and format response to the standard object required on the base logic/method to record transactions
 * 
 * @param param		Object	the gateway details 
 * @param details	Object	the payment details 
 * @return 			Promise	the formatted response of process payment
 */
WestpacGateway.prototype.processPayment = function (param, details) {
	var self = this;
	return new Promise(function (resolve, reject) {

		if (typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank') {

			var donate = param.donation;
			var paymentDate = new Date(donate.timestamp * 1000);

			donate.payment_response = {};
			donate.statusCode = 1;
			donate.statusText = 'PENDING';

			donate.PaymentTransaction = {
				TxnStatus: 'Success',
				TxnId: '',
				TxnDate: self.formatDate(paymentDate),
				TxnMessage: '',
				TxnPaymentRef: donate.pamynet_ref
			};

			donate.card_details = self.cleanBank(details);

			resolve({
				status: 'success',
				response: {},
				donation: donate
			});

		} else {
			console.log('Westpac Tokenize Payment');
			
			self.tokenizePayment(param, details)
				.then(function (result) {
					resolve(result);
				})
				.catch(function (error) {
					reject(error);
				});
		}
	});
};

