var j = jQuery;

var PaymentfileGateway = function( config ) {
  this.config = config;
  j('#pdwp-btn').show();

  j('#wfc_donation-payment-details-toogle-container')
  .after('<div id="paymentfile-card-details" class="col-12 col-sm-12 col-md-12 col-lg-12"></div>')
  .hide();

  var bankfields = j('#bank-details-wrapper').clone();  
  bankfields.addClass('paymentfile-bank-details-wrapper');
  j('#paymentfile-card-details').prepend(bankfields);
  j('#paymentfile-card-details').find('input#pdwp_account_holder_name').attr('id' , 'wfc_paymentfile_account_holder_name');
  j('#paymentfile-card-details').find('input#pdwp_account_number').attr('id' , 'wfc_paymentfile_account_number');
  j('#paymentfile-card-details').find('input#pdwp_bank_code').attr('id' , 'wfc_paymentfile_bank_code');
  j("label[for=payment_type_bank]").click();
  j('.paymentfile-bank-details-wrapper').show();

};

PaymentfileGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

PaymentfileGateway.prototype.clean = function( cc ) {

  var creditcarddetails = {
    card_number: cc.card_number,
    name_on_card: cc.name_on_card,
    expiry_month: cc.expiry_month,
    expiry_year: cc.expiry_year,
    ccv: cc.ccv
  };

  var regex = /\d(?=\d{4})/mg;
  var str = creditcarddetails.card_number.replace(/ /g,'');
  var subst = '*';

  creditcarddetails.card_number = str.replace(regex, subst);
  creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

  return creditcarddetails;
};

PaymentfileGateway.prototype.cleanBank = function( bank ) {
  var bank_details = {
      bank_code: ( typeof bank.bank_code != 'undefined' ? bank.bank_code : '' ),
      bank_number: ( typeof bank.bank_number != 'undefined' ? bank.bank_number : '' ),
      bank_name: ( typeof bank.bank_name != 'undefined' ? bank.bank_name : '' )
  };

  var regex = /\d(?=\d{4})/mg;
    var strbank = bank_details.bank_number.replace(/ /g,'');
    var strcode = bank_details.bank_code.replace(/ /g,'');
    var subst = 'x';

    bank_details.bank_code = strcode.replace(regex, subst);
    bank_details.bank_number = strbank.replace(regex, subst);
    return bank_details;
};

PaymentfileGateway.prototype.processPayment = function( param, details ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {

    if( typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank' ) {

      var donate = param.donation;
      var paymentDate = new Date( donate.timestamp * 1000 );

      donate.payment_response = {};
      donate.statusCode = 1;
      donate.statusText = 'PENDING';

      donate.PaymentTransaction = {
        TxnStatus : 'Success',
        TxnId : '',
        TxnDate : self.formatDate( paymentDate ),
        TxnMessage : '',
        TxnPaymentRef : donate.pamynet_ref
      };

      donate.card_details = self.cleanBank( details );

      resolve( {
        status : 'success',
        response : {},
        donation : donate
      } );

    } else {  

    }
 	});
};

