<?php

	/**
	* Paymentfile Wordpress Hooks
	*/
	class Paymentfile_WP_Hooks
	{
		/**
		 * The Paymentfile main class
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $parent    The Paymentfile main class
		 */
		public $parent = null;

		/**
		 * The main class of the plugin
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $includes    The main class of the plugin
		 */
		public $includes = null;
		
		/**
		 * Paymentfile_WP_Hooks constructor.
		 *
		 * @param $a
		 * @param $b
		 */
		function __construct( $a, $b )
		{

			/*
			 * The Paymentfile main class
			 */
			$this->parent = $a;

			/*
			 * The main class of the plugin
			 */
			$this->includes = $b;

			/*
			 * Paymentfile scripts
			 */
			add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_paymentfilescripts' ) );
		}
		
		/**
		 * Loads script for Paymentfile.
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 * @return void
		 * @since    5.0.0
		 *
		 * @LastUpdated   June 28, 2018
		 */
		public function wfc_load_paymentfilescripts() {
			global $post;
				
				/*
				 * paymentfile js library
				 */
				wp_register_script(
					'webforce-connect-donation-paymentfile_js',
					plugin_dir_url( __FILE__ ) . '../js/paymentfile.js',
					array(),
					1.0,
					true
				);

		}
	}