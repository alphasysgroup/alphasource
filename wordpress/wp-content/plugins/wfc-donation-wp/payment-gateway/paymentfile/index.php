<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'Paymentfile' ) ) {
		
		/**
		 * Class Paymentfile
		 */
		class Paymentfile
		{

			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-paymentfile_lib_js',
				'webforce-connect-donation-paymentfile_js'
			);
			
			function __construct()
			{

			}
			
			/**
			 * Loads Paymentfile settings template.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 28, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Loads Paymentfile WordPress core hooks.
			 * LastUpdated : June 28, 2018
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param string $class  plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 */
			public function loadWpCore( $class ) {
				require_once plugin_dir_path(__FILE__) . 'main/main.php';
				new Paymentfile_WP_Hooks( $this, $class );
			}

		}
	}