<?php 

/**
* Pin Wordpress Hooks
*/
class Pin_WP_Hooks
{
	/**
	 * The Pin main class
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $parent    The Pin main class
	 */
	public $parent = null;

	/**
	 * The main class of the plugin
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $includes    The main class of the plugin
	 */
	public $includes = null;
	
	/**
	 * Pin_WP_Hooks constructor.
	 *
	 * @param $a
	 * @param $b
	 */
	function __construct( $a, $b )
	{
		/*
		 * The Pin main class
		 */
		$this->parent = $a;

		/*
		 * The main class of the plugin
		 */
		$this->includes = $b;

		/*
		 * Pin scripts
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_pinscripts' ) );

		/*
		 * Pin oauth
		 */
		add_action( 'wp_ajax_wfc_pin_payment', array( $this, 'wfc_pin_payment' ) );
		add_action( 'wp_ajax_nopriv_wfc_pin_payment', array( $this, 'wfc_pin_payment' ) );

	}
	
	/**
	 * Loads Pin javascript libraries.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 5, 2018
	 */
	public function wfc_load_pinscripts() {
		global $post;

		// Pin.js load
		wp_register_script(
			'webforce-connect-donation-pin_library_js',
			'https://cdn.pin.net.au/pin.v2.js',
			array(),
			4.0,
			true
		);

		//pdwp pin js
		wp_register_script(
			'webforce-connect-donation-pinpayments_js',
			plugin_dir_url( __FILE__ ) . '../js/pinpayments.js',
			array(),
			4.0,
			true
		);

	}
	
	/**
	 * Registers ajax endpoint for Pin payment
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 5, 2018
	 */
	public function wfc_pin_payment() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_pin_payment' ) {

 			/*
			 * break if direct call enpoint
			 */
 			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}
	
			/*
			 * donation data
			 */
			$donate = $_POST['param']['donation'];

			/*
			 * pin gateway
			 */
			$gateway = get_option($donate['gatewayid']);

			/*
			 * process pin token payment
			 */
			$payment = $this->parent->wfc_TokenPayment( $donate, $gateway, $_POST['token'] );

			if( is_array( $payment ) ) {
				
				/*
				 * return to form if error occured
				 */
				wp_send_json( $payment );

			} else {

				/*
				 * success transaction
				 */
				wp_send_json( array(
					'status' => 'success',
					'response' => $payment
				) );
			}

		}
	}
}