/**
* jQuery
*/
var j = jQuery;

var PinGateway = function( config ) {
	this.config = config;
	j('#pdwp-btn').show();
	
	j('#wfc_donation-payment-details-toogle-container').show();
	j("label[for=payment_type_credit_card]").click();
	
};

PinGateway.prototype.clean = function( cc ) {

	var creditcarddetails = {
		card_number: cc.card_number,
		name_on_card: cc.name_on_card,
		expiry_month: cc.expiry_month,
		expiry_year: cc.expiry_year,
		ccv: cc.ccv
	};

	var regex = /\d(?=\d{4})/mg;
	var str = creditcarddetails.card_number.replace(/ /g,'');
	var subst = '*';

	creditcarddetails.card_number = str.replace(regex, subst);
	creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

	return creditcarddetails;
};

PinGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

PinGateway.prototype.oneTimeToken = function( param, cc ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {
		var PinPaymentsmode = 'live';
		/* 
		Create an API object with your publishable api key, and specifying 'test' or 'live'.
		Be sure to use your live publishable key with the live api, and your test publishable key with the test api.
		*/
		if( self.config.settings.wfc_donation_sandboxmode == "true" ){
			PinPaymentsmode = 'test';
		}
		var pinApi = new Pin.Api(self.config.settings.wfc_donation_pinpublishablekey,PinPaymentsmode);
		/*
		Fetch details required for the createToken call to Pin Payments
		*/		
		var card = {
			number:           cc.card_number.replace(/ /g,''),
			name:             cc.name_on_card,
			expiry_month:     cc.expiry_month,
			expiry_year:      cc.expiry_year,
			cvc:              cc.ccv.replace(/ /g,''),
			address_line1:    param.donation.donor_address,
			address_line2:    '',
			address_city:     param.donation.donor_suburb,
			address_state:    param.donation.donor_state,
			address_postcode: param.donation.donor_postcode,
			address_country:  param.donation.donor_country
		};

      // Request a token for the card from Pin Payments
		pinApi.createCardToken(card)
		.then( function( card ) {
			resolve({
				status : 'success',
				token : card.token,
			});
		}, function(response){
			var err = [];
			if (response.messages) {
			  	j.each(response.messages, function(index, paramError) {
				  	var errorPram    = paramError.param;
				  	var errorMessage = paramError.message;
				  	var errorDesc    = response.error_description;
				  	err.push( errorMessage );
			  	});
			}else{
				if(response.unauthenticated){
					err.push( response.unauthenticated );
				}
				if(response.error_description){
					err.push( response.error_description );
				}				
			}
			resolve( {
				status : 'error',
				response : err,
				donation : param.donation
			} );
		}).done();

	});


};

PinGateway.prototype.tokenPayment = function( param, cc, token ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_pin_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				token: token
			},
			success: function( resp ) {
				
				if( resp.status == 'success' ) {
				 	
					var donate = param.donation;
 					var paymentDate = new Date( donate.timestamp * 1000 );

 					donate.payment_response = resp.response.response;
 					
 					if( resp.response.response.status_message == 'Success' ) {
 						donate.statusCode = 1;
 						donate.statusText = 'APPROVED';
 					} else if( resp.response.response.status_message == 'Pending' ) {
	 					donate.statusCode = 1;
	 					donate.statusText = 'PENDING';
 					}else {
 						donate.statusCode = 0;
 						donate.statusText = 'FAILED';
 					}

 					donate.PaymentTransaction = {
 						TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
 						TxnId : resp.response.response.token,
 						TxnDate : self.formatDate( paymentDate ),
 						TxnMessage : resp.response.response.status_message + '-' + donate.pamynet_ref,
 						TxnPaymentRef : donate.pamynet_ref
 					};

					if( self.config.settings.wfc_donation_tokenize == "true" ){
						donate.TokenCustomerID = resp.response.response.card.customer_token;
					}else{
						donate.TokenCustomerID = '';
					}
 					
 					donate.card_details = self.clean( cc );

 					resolve( {
 						status : 'success',
 						response : resp.response.response,
 						donation : donate
 					} );

				} else {
					resolve( {
						status : 'error',
						response : resp.errors,
						donation : param.donation
					} );
				}
			},
			error: function( resp ) {
				reject( resp );
			}
		});
 	});
};

PinGateway.prototype.processPayment = function( param, cc ) {
	var self = this;
 	return new Promise( function( resolve, reject ) {
 		self.oneTimeToken( param, cc )
 		.then(function(result){
 			if( result.status == 'error' ) {
 				resolve( result );
 			} else {
 				return self.tokenPayment( param, cc, result.token );
 			}
 		})
 		.then( function( result ) {
 			resolve( result );
 		})
 		.catch(function(error){
 			reject( error );
 		});
 	});
};
