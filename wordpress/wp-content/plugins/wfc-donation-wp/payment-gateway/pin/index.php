<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'PIN' ) ) {
		
		/**
		 * Class PIN
		 */
		class PIN
		{
			/**
			 * @var array
			 */
			public $restricted = array(
				'wfc_donation_pinsecretkey'
			);

			/**
			 * The registered scripts id.
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-pin_library_js',
				'webforce-connect-donation-pinpayments_js'
			);
			
			/**
			 * PIN constructor.
			 */
			function __construct()
			{

			}
			
			/**
			 * Load stripe settings template
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 5, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Load stripe wp core hooks
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $class
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 5, 2018
			 */
			public function loadWpCore( $class ) {
				require_once( plugin_dir_path( __FILE__ ) . 'main/main.php' );
				new PIN_WP_Hooks( $this, $class );
			}
			
			/**
			 * Stripe tokenize payment and direct payment.
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $post    , donation data object
			 * @param $gateway , gateway object
			 * @param $token   , pin card token
			 *
			 * @return array|mixed|object $charge
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 5, 2018
			 */
			public function wfc_TokenPayment( $post, $gateway, $token ) {

				/*
				 * pin payments mode if sandbox/live
				 */
				$mode = (isset($gateway['wfc_donation_sandboxmode']) && $gateway['wfc_donation_sandboxmode'] == 'true') ? 'test-' : '';

				/*
				 * pin payments endpoint
				 */
				$endpoint = "https://{$mode}api.pin.net.au/1/charges";

				/*
				 * get pin payments api key
				 */
				$apiKey = (isset( $gateway['wfc_donation_pinsecretkey']) && !empty($gateway['wfc_donation_pinsecretkey'])) ? $gateway['wfc_donation_pinsecretkey'] : null;

				 /*
				 * pin payments mode if tokinized
				 */
				$tokenmode = (isset( $gateway['wfc_donation_tokenize']) && $gateway['wfc_donation_tokenize'] == 'true') ? true : false;											

				/*
				 * donation form currency
				 */
				$currency = explode( ',', $post['currency'] );
				$data_amount = str_replace(
	                    '.',
	                    '',
	                    number_format(
	                        $post['pdwp_sub_amt'],
	                        2,
	                        ".",
	                        ""
	                    )
	                );
				
				/*
				 * construct data for charge
				 */
				$proceed_payment = false;
				$customer_error_res = array();
				if( $tokenmode == true ){

					$customer_res = $this->wfc_TokenCustomer( $post, $gateway, $token );	

					if( isset($customer_res->response->token) ){

						$data = json_encode( array(
							'customer_token' => $customer_res->response->token,
							'amount' 	     => $data_amount ,
							'currency'       => $currency[1],
							"email" 	     => $post['donor_email'],
							'description'    => $post['pamynet_ref']
						) );
						$proceed_payment = true;
						
					}else{
						$customer_error_res = $customer_res;
					}
                    
				}else{

					$data = json_encode( array(
						'card_token'  => $token,
						'amount' 	  => $data_amount ,
						'currency'    => $currency[1],
						"email" 	  => $post['donor_email'],
						'description' => $post['pamynet_ref']
					) );
                    					
				}

                                                                                                                                                                               
				if( $proceed_payment == true ){

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
					curl_setopt($ch, CURLOPT_URL, $endpoint);
					curl_setopt($ch, CURLOPT_ENCODING, "gzip");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
					curl_setopt($ch, CURLOPT_USERPWD, $apiKey.':'.'');
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(   
					    'Accept: application/json',
					    'Content-Type: application/json')                                                           
					);          

					// Send the request & save response to $resp
					$res = json_decode(curl_exec($ch));
					if( isset( $res->response->success ) && $res->response->success == true ) {
						return $res;
					}else{
						$errors = '';
						$DefaultErrorMsg = "Something went wrong, Please try again or contact your administrator.";
						if( isset( $res->error ) && !empty( $res->error ) ) {
							$error_description =  isset( $res->error_description ) ? $res->error_description : $DefaultErrorMsg;
							$errors[] = $error_description;
						}else{
							$errors[] = $DefaultErrorMsg;
						}
						return array(
							'res'    => $res,
							'status' => 'error',
							'errors' => $errors,
							'endpoint' => $endpoint
						);
					}

				}else{
					return $customer_error_res;								
				}
			}
			
			/**
			 * Stripe tokenized payment.
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $post    , donation data object
			 * @param $gateway , gateway object
			 * @param $token   , pin card token
			 *
			 * @return array|mixed|object $charge , if success object
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 5, 2018
			 */
			public function wfc_TokenCustomer( $post, $gateway, $token ) {
				/*
				 * pin payments mode if sandbox/live
				 */
				$mode = (isset( $gateway['wfc_donation_sandboxmode']) && $gateway['wfc_donation_sandboxmode'] == 'true') ? 'test-' : '';

				/*
				 * pin payments endpoint
				 */
				$endpoint = "https://{$mode}api.pin.net.au/1/customers";

				/*
				 * get pin payments api key
				 */
				$apiKey = (isset( $gateway['wfc_donation_pinsecretkey']) && !empty( $gateway['wfc_donation_pinsecretkey'])) ? $gateway['wfc_donation_pinsecretkey']: null;

				/*
				 * construct data for charge
				 */
				$data = json_encode( array(
					'card_token'  => $token,
					'email'  	  => $post['donor_email']
				) );
                                                                                                                                                                                                   

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_URL, $endpoint);
				curl_setopt($ch, CURLOPT_ENCODING, "gzip");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);     
				curl_setopt($ch, CURLOPT_USERPWD, $apiKey.':'.'');
				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(   
				    'Accept: application/json',
				    'Content-Type: application/json')                                                           
				);  

				// Send the request & save response to $resp
				$res = json_decode(curl_exec($ch));

				if( isset( $res->response->token ) ) {
					return $res;
				}else{
					$errors = '';
					$DefaultErrorMsg = "Something wen't wrong, Please try again or contact your administrator.";
					if( isset( $res->error ) && !empty( $res->error ) ) {
						$error_description =  isset( $res->error_description ) ? $res->error_description : $DefaultErrorMsg;
						$errors[] = $error_description;
					}else{
						$errors[] = $DefaultErrorMsg;
					}
					return array(
						'res'    => $res,
						'status' => 'error',
						'errors' => $errors,
						'endpoint' => $endpoint
					);
				}

			}


		}
	}