<?php 

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

if ( ! class_exists( 'Paypal' ) ) {
	
	/**
	 * Class Paypal
	 */
	class Paypal
	{
		public $restricted = array(
			'wfc_donation_paypalclientsecret'
		);

		/**
		 * The registered scripts id
		 *
		 * @since    5.0.0
		 * @access   public
		 * @var      string    $registerd_scripts    The main class of the plugin
		 */
		public $registerd_scripts = array(
			'webforce-connect-donation-paypal_js'
		);

		function __construct()
		{

		}
		
		/**
		 * Name : loadSettingsTemplate
		 * Description: load paypal settings template
		 * LastUpdated : sept 02, 2017
		 *
		 * @author   Danryl Carpio <danryl@alphasys.com.au>
		 *
		 * @param $data , settings data
		 *
		 * @return mixed
		 * @since    5.0.0
		 */
		public function loadSettingsTemplate( $data ) {
			return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
		}
		
		/**
		 * Name : loadWpCore
		 * Description: load paypal wp core hooks
		 * LastUpdated : June 7, 2018
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param $class , plugin main file
		 *
		 * @return void
		 * @since    5.0.0
		 */
		public function loadWpCore( $class ) {
			require_once( plugin_dir_path( __FILE__ ) . 'main/main.php' );
			new PayPal_WP_Hooks( $this, $class );
		}
		
		/**
		 * Name : paypal_ipn_handler
		 * Description: register paypal ipn callback
		 * LastUpdated : June 7, 2018
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *           Danryl Carpio <danryl@alphasys.com.au>
		 *
		 * @param $post    , donation data array
		 * @param $meta_id , donation data meta id
		 * @param $class   , donation plugin main class
		 *
		 * @return bool
		 * @since    5.0.0
		 */
		public function paypal_ipn_handler( $post, $meta_id, $class ) {

			/*
			 * load paypal ipn listener class
			 */
			require_once( 'ipn/ipnlistener.php' );

			/*
			 * get donation
			 */
			$WFC_DonationForms = new WFC_DonationForms();
			$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $meta_id );

			/*
			 * check if getting donation is successful
			 */
			if( false !== $donations ) {

				/*
				 * convert json string to json
				 */
				$data = unserialize( $donations[0]['meta_value'] );

				/*
				 * get paypal payment gateway
				 */
				$gateway = get_option($donate['gatewayid']);

				/*
				 * check if paypal payment is sandbox/live
				 */
				$is_sandbox = ( $gateway['wfc_donation_sandboxmode'] == 'true' ) ? true : false;

				try {

					/*
					 * initialize class
					 */
					$ipn = new PaypalIPN();

					/*
					 * checking paypal if sandbox
					 */
					if( $is_sandbox === true ) {
						$ipn->useSandbox();
					}

					/*
					 * checking post data 
					 */
					if( $post ) {

						/*
						 * checking if ipn data verified
						 */
						$verified = $ipn->verifyIPN();
						if( $verified ) {

							/*
							 * process ipn 
							 */
							$this->paypal_process_ipn( $post, $data, $meta_id, $class );

						} else {
							return false;
						}
					} else {
						return false;
					}
				} catch( Exception $e ) {
					return false;
				}
			}
		}
		
		/**
		 * Name : paypal_process_ipn
		 * Description: process paypal ipn
		 * LastUpdated : June 7, 2018
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *         Danryl Carpio <danryl@alphasys.com.au>
		 *
		 * @param $post     , ipn data/paypal transaction response
		 * @param $donation , donation data array
		 * @param $meta_id  , donation data meta id
		 * @param $class    , donation plugin main class
		 *
		 * @return void
		 * @since    5.0.0
		 */
		private function paypal_process_ipn( $post, $donation, $meta_id, $class ) {

			/*
			 * set post meta id for donation records
			 */
			$donation['post_meta_id'] = $meta_id;

			/*
			 * set payment response
			 */
			$donation['payment_response'] = array(
				'PayerStatus' => $post['payer_status'],
				'ReceiverID' => $post['receiver_id'],
				'TxnType' => $post['txn_type'],
				'Business' => $post['business'],
				'McFee' => $post['mc_fee'],
				'McGross' => $post['mc_gross'],
				'Tax' => isset( $post['tax'] ) ? $post['tax'] : '',
				'ProtectionEligibility' => $post['protection_eligibility'],
				'PaymentStatus' => $post['payment_status']
			);

			/*
			 * check if payment transaction is successful
			 */
			if( in_array( strtolower( $post['payment_status'] ), array( 'completed', 'created', 'processed', 'approved' ) ) ) {
               
                $donation['statusCode']  = 1;
                $donation['statusText']  = 'APPROVED';

            } elseif( strtolower( $pp_rep['payment_status'] ) == 'pending' ) {

                /*
				 * pending transaction
				 */
                $donation['statusCode']  = 1;
                $donation['statusText']  = 'PENDING';

            } else {

            	/*
				 * failed transaction
				 */
                $donation['statusCode']  = 0;
                $donation['statusText']  = 'FAILED';
            }

            /*
			 * set PPAPI payment transaction status
			 */
            $donation['PaymentTransaction'] = array(
                "TxnStatus" => ( $donation['statusCode'] === 1 ) ? 'Success' : 'Failed',
                "TxnId" => $post['txn_id'],
                "TxnDate" => date( 'Y-m-d', $donation['timestamp'] ),
                "TxnMessage" => $meta_id. '-' .ucfirst( $post['payment_status'] ),
                "TxnPaymentRef" => $donation['pamynet_ref']
            );

            /*
			 * update after payment 
			 */
            $class->wfc_donation_data( array( serialize( $donation ), $meta_id ), 'update' ); 
           
           // $class->pdwp_donation_data( array( serialize( $donation ), $meta_id ), 'update' );
            //update_post_meta( $meta_id, 'pronto_donation_donor', stripslashes_deep( $donation ) );

            /*
			 * sync to sf ppai
			 */
            wp_schedule_single_event( time() + 5, 'wfc_donation_syncsingledonation', array( $donation ) );
            //$ppai = $class->pdwp_ppapi( $donation );

			 /*
			 * get sf response
			 */
            //$donation['SalesforceResponse'] = $class->pdwp_ppapi_parse_response( $donation, $ppai );

             /*
			 * set ppapi logs
			 */
           // $donation['SalesforceLogs'] = $ppai;

            /*
			 * update donation records with payment response
			 */
            //$class->pdwp_donation_data( array( serialize( $donation ), $meta_id ), 'update' );

            //update_post_meta( $meta_id, 'pronto_donation_donor', stripslashes_deep( $donation ) );

            /*
			 * delete user donation form session
			 */
            unset( $_SESSION['pdwp_rand'] );

             /*
			 * terminate process
			 */
            exit();
		}
	}
}