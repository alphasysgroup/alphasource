<?php

/**
* Paypal Wordpress Hooks
*/
class PayPal_WP_Hooks
{
	/**
	 * The paypal main class
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $parent    The paypal main class
	 */
	public $parent = null;

	/**
	 * The main class of the plugin
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $includes    The main class of the plugin
	 */
	public $includes = null;
	
	/**
	 * PayPal_WP_Hooks constructor.
	 *
	 * @param $a
	 * @param $b
	 */
	function __construct( $a, $b )
	{
		/*
		 * The paypal main class
		 */
		$this->parent = $a;

		/*
		 * The main class of the plugin
		 */
		$this->includes = $b;

		/*
		 * create paypal IPN endpoint
		 */
		add_action( 'init', array( $this, 'pdwp_paypal_add_endpoint' ) );
		add_action( 'parse_request', array( $this, 'pdwp_ipn_handler' ) );

		/*
		 * paypal scripts
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_paypalscripts' ) );

		/*
		 * paypal oauth
		 */
		add_action( 'wp_ajax_wfc_paypal_auth', array( $this, 'wfc_paypal_auth' ) );
		add_action( 'wp_ajax_nopriv_wfc_paypal_auth', array( $this, 'wfc_paypal_auth' ) );

		/*
		 * paypal redirect
		 */
		add_action( 'wp_ajax_wfc_prepare_paypal_redirect', array( $this, 'wfc_paypal_prepare_redirect' ) );
		add_action( 'wp_ajax_nopriv_wfc_prepare_paypal_redirect', array( $this, 'wfc_paypal_prepare_redirect' ) );
	}
	
	/**
	 * Loads scripts required by Paypal.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 1, 2018
	 */
	public function wfc_load_paypalscripts() {
		global $post;

		/*
		 * paypal js redirect payment
		 */
		// wp_register_script( 
		// 	'webforce-connect-donation-paypal_redirect_js',
		// 	'https://www.paypalobjects.com/api/checkout.js',
		// 	array( 'jquery' ), 
		// 	'', 
		// 	false 
		// );

		/* Smart Payment Buttons features */
		// wp_register_script( 
		// 	'webforce-connect-donation-paypal_spb_js',
		// 	'https://www.paypal.com/sdk/js?client-id=AcF2fVknqRgneqh2Rhd2OmpIv9p2l23ugfROwjpHqeuXcMS9fSyFTl2Xs1-uSwRSqMTd2mqLgE2ADqng&commit=true&intent=capture&currency=CAD',
		// 	array( 'jquery' ), 
		// 	null, 
		// 	false 
		// );

	 	/*
		 * pdwp paypal js
		 */
		wp_register_script(
			'webforce-connect-donation-paypal_js',
			plugin_dir_url( __FILE__ ) . '../js/paypal.js',
			array(),
			1.0,
			true
		);

		wp_localize_script(
			'webforce-connect-donation-paypal_js',
			'wfc_pp',
			array(
				'plugin_url' => plugin_dir_url(__DIR__) . '../../'
			)
		);
	}
	
	/**
	 * AJAX Callback function for Paypal Authentication.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @return void object
	 * @throws Exception
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 1, 2018
	 */
	public function wfc_paypal_auth() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_paypal_auth' ) {

			/*
			 * terminate if direct access for paypal ajax endpoint
			 */
			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
			 * load paypal auth library
			 */
			require_once( plugin_dir_path( __FILE__ ) . '../lib/paypal-auth.php' );

			/*
			 * grab donation data
			 */
 			$donate = $_POST['donate'];

 			/*
			 * get paypal gateway object class
			 */
			$gateway = get_option($donate['gatewayid']);

			/*
			 * get paypal client id
			 */
			$client_id = !empty($gateway['wfc_donation_paypalclientid']) ? $gateway['wfc_donation_paypalclientid']: '';

			/*
			 * get paypal client secret
			 */
			$client_secret = !empty($gateway['wfc_donation_paypalclientsecret']) ? $gateway['wfc_donation_paypalclientsecret']: '';

			/*
			 * check paypal environment if sandbox/live
			 */
			$sandbox = ( $gateway['wfc_donation_sandboxmode'] == 'true' ) ? true : false;

			/*
			 * paypal auth class initialization
			 */
			$pp = new PayPalAuth( $client_id, $client_secret, $sandbox );

			/*
			 * get paypal access token
			 */
			$accesstoken = $pp->getAccessToken();

			if( !empty( $accesstoken ) ) {

				/*
				 * return access token if success
				 */
				wp_send_json( array(
					'status' => 'success',
					'accesstoken' => $accesstoken
				) );
			} else {

				/*
				 * return to form if error occured
				 */
				wp_send_json( array(
					'status' => 'failed',
					'accesstoken' => null
				) );
			}
		}
	}
	
	/**
	 * Callback for Paypal redirection.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @since    5.0.0
	 * @LastUpdated  June 1, 2018
	 */
	public function wfc_paypal_prepare_redirect() {

		
		// if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_prepare_paypal_redirect' ) {

		// 	/*
		// 	 * terminate if direct access for paypal ajax endpoint
		// 	 */
		// 	if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
		// 		die('busted');
		// 	}

		// 	/*
		// 	 * grab donation data
		// 	 */
		// 	$donate = $_POST['donate'];

		// 	/*
		// 	 * get paypal gateway object class
		// 	 */
		// 	$gateway = get_option($donate['gatewayid']);

		// 	/*
		// 	 * get paypal business username
		// 	 */
		// 	$business = isset($gateway['wfc_donation_paypalbusinessusername']) ? $gateway['wfc_donation_paypalbusinessusername'] : '';

		// 	/*
		// 	 * get pronto donation global settings
		// 	 */
		// 	//$pdwpSettings = $this->includes->pdwp_array_to_object( get_option( 'pronto_donation_settings', 0 ) );

		// 	if( isset( $donate['currency'] ) ) {

		// 		/*
		// 		 * get donation currency
		// 		 */
		// 		$cur = explode( ',', $donate['currency'] );
		// 		$currency = $cur[1];

		// 	} else {

		// 		/*
		// 		 * set currency to null
		// 		 */
		// 		$currency = null;
		// 	}

		// 	/*
		// 	 * thankyou url
		// 	 */
		// 	$setting = get_option( 'pronto_donation_settings', 0 );
		// 	$ty_url = isset($setting->pd_thank_you_page_id) ? $setting->pd_thank_you_page_id : '';
		// 	//$ty_url = isset( $pdwpSettings->pd_thank_you_page_id ) ? $pdwpSettings->pd_thank_you_page_id : '';

		// 	/*
		// 	 * construct url
		// 	 */
		// 	$url = add_query_arg(
		// 		array(
		// 			'p'	=>	$ty_url, //page ID of thankyou
		// 			'ty' => $donate['donation_campaign'], //campaign ID
  //                   'pmid' => $donate['post_meta_id'],
		// 		),
		// 		get_home_url()
		// 	);

		// 	*
		// 	 * thankyou url
			 
		// 	//$data_url = ( isset( $post['thankyou_url'] ) && !empty( $post['thankyou_url'] ) ) ? $post['thankyou_url'] : $url;

		// 	/*
		// 	 * construct paypal form redirection
		// 	 */
		// 	$data = array(
		// 		'cmd' => '_cart',
		// 		'upload' => '1',
		// 		'business' => $business,
		// 		'notify_url' => get_site_url() . '/?PDWP_PayPal_IPN&action=ipn_handler',
		// 		// 'notify_url' => 'http://a0ea070d.ngrok.io/PRONTODONATION/?PDWP_PayPal_IPN&action=ipn_handler',
		// 		'return' => $url,
		// 		'currency_code' => $currency,
		// 		'custom' => isset( $donate['post_meta_id'] ) ? $donate['post_meta_id'] : '',
		// 		'first_name' => isset( $donate['donor_first_name'] ) ? $donate['donor_first_name'] : '',
		// 		'last_name' => isset( $donate['donor_last_name'] ) ? $donate['donor_last_name'] : '',
		// 		'address1' => isset( $donate['donor_address'] ) ? $donate['donor_address'] : '',
		// 		'country' => isset( $donate['donor_country'] ) ? $donate['donor_country'] : '',
		// 		'zip' => isset( $donate['donor_postcode'] ) ? $donate['donor_postcode'] : '',
		// 		'city' => isset( $donate['donor_suburb'] ) ? $donate['donor_suburb'] : '',
		// 		'email' => isset( $donate['donor_email'] ) ? $donate['donor_email'] : '',
		// 		'state' => isset( $donate['donor_state'] ) ? $donate['donor_state'] : '',
		// 		'item_name_1' => get_the_title( $donate['donation_campaign'] ),
		// 		'amount_1' => isset( $donate['pdwp_sub_amt'] ) ? $donate['pdwp_sub_amt'] : 0
		// 	);

		// 	/*
		// 	 * return data for redirection
		// 	 */
		// 	wp_send_json( array(
		// 		'status' => 'success',
		// 		'redirect_data' => $data,
		// 		'return_url' => $url
		// 	) );
		// }



		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_prepare_paypal_redirect' ) {

			/*
			 * terminate if direct access for paypal ajax endpoint
			 */
			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
	         * call all custom begin filter
	         */
	        if (has_filter('wfc_donation_begin') == true) {
	            $custom_donation_begin_filter = apply_filters('wfc_donation_begin', $_POST);
	            $_POST = $custom_donation_begin_filter;
	        }

			$post = isset($_POST['donation']) ? $_POST['donation'] : array();
			$post['paypal_redirect'] = 'true';
			set_transient( 'pdwp_form_cache_' . $post['campaignSessionId'], $post, HOUR_IN_SECONDS );

			/*
             * set up return page
             */
            $url = add_query_arg(
            	array(
	                'e' => $post['campaignSessionId'],
	                'p' => $post['p'],
	            ), 
	            get_the_permalink($post['p'])
            );

			/*
			 * return data for redirection
			 */
			wp_send_json( array(
				'status' => 'success',
				'cancel_url' => $url,
				'donate' => $post
			) );
		}
	}
	
	/**
	 * Adds Paypal ipn endpoint.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 1, 2018
	 */
	public function pdwp_paypal_add_endpoint() {
		add_rewrite_endpoint('PDWP_PayPal_IPN', EP_ALL);
	}
	
	/**
	 * IPN handler function for Paypal.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param object|null $query wordpress query
	 *
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 1, 2018
	 */
	public function pdwp_ipn_handler( $query ) {
		if( isset( $query->query_vars['PDWP_PayPal_IPN'] ) && 
			( isset( $_GET['action'] ) && $_GET['action'] == 'ipn_handler' ) ) 
		{
			/*
			 * check if paypal ipn post response is not an empty
			 */
			if( !empty( $_POST ) ) {

				/*
				 * process ipn
				 */
				$this->parent->paypal_ipn_handler( $_POST, $_POST['custom'], $this->includes );
			}
		}
	}
}