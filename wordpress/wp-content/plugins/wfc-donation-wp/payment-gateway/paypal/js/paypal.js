var j = jQuery;

var PaypalGateway = function( config ) {
	this.config = config;
	j('#pdwp-btn').show();

	j( '.pp-msg' ).remove();
	if( config.settings.wfc_donation_paypayredirect == 'true' ) {
		j('#wfc_donation-payment-details-toogle-container').hide();
		var paypalredirect_msg = '<div id="paypal-redirect-message" class="col-12 col-sm-12 col-md-12 col-lg-12"><p class="wfc-don-field-label pp-msg">Please click below to finalise your PayPal payment.</p><p class="pp-msg" style="font-size: 12px;"><span style="color:red;">Important note</span> : While paypal is processing your payment please dont close the window until you are redirected back to this website. Thank you</p><div id="wfc-paypal-button-container"></div></div>';
		paypalredirect_msg = '<div id="paypal-redirect" class="col-12 col-sm-12 col-md-12 col-lg-12"><div id="paypal-redirect-wrapper" class="row">'+paypalredirect_msg+'</div></div>';
		j('#wfc_donation-payment-details-toogle-container').after(paypalredirect_msg);
		PaypalGateway.prototype.generatePaypalButton();
		j('#pdwp-btn').hide();
	} else {
		j('#wfc_donation-payment-details-toogle-container').show();
		j("label[for=payment_type_credit_card]").click();
	}

};

PaypalGateway.prototype.post = function( path, params, method ) {

	/**
	* Set method to post by default if not specified.
	*/
	method = method || "post";

    /**
	* The rest of this code assumes you are not using a library.
	* It can be made less wordy if you use one.
	*/
    var form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);

    for(var key in params) {
        if(params.hasOwnProperty(key)) {
            var hiddenField = document.createElement("input");
            hiddenField.setAttribute("type", "hidden");
            hiddenField.setAttribute("name", key);
            hiddenField.setAttribute("value", params[key]);

            form.appendChild(hiddenField);
         }
    }

    document.body.appendChild(form);
    form.submit();
};

PaypalGateway.prototype.clean = function( cc ) {
	var creditcarddetails = {
		card_number: cc.card_number,
		name_on_card: cc.name_on_card,
		expiry_month: cc.expiry_month,
		expiry_year: cc.expiry_year,
		ccv: cc.ccv
	};

	var regex = /\d(?=\d{4})/mg;
	var str = creditcarddetails.card_number.replace(/ /g,'');
	var subst = '*';

	creditcarddetails.card_number = str.replace(regex, subst);
	creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

	return creditcarddetails;
};

PaypalGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

PaypalGateway.prototype.getSafe = function(fn) {
    try {
        return fn();
    } catch (e) {
        return undefined;
    }
};

PaypalGateway.prototype.getAccessToken = function( param, cc ) {
	var self = this;
	return new Promise( function( resolve, reject ) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_paypal_auth',
				donate : param.donation,
				nonce: pdwp_ajax.nonce,
			},
			success: function( resp ) {
				resolve( resp );
			},
			error: function( resp ) {
				reject( resp );
			}
		});
	});
};

PaypalGateway.prototype.directCreditCardPayment = function( param, cc, accesstoken ) {
	var self = this;
	return new Promise( function( resolve, reject ) {

		var cnumber = cc.card_number.replace(/ /g,'');
		var donate = param.donation;
		var currency = donate.currency.split(',');
		var card = new CardValidate( cnumber );
		var mode = ( self.config.settings.wfc_donation_sandboxmode == 'true' ) ? '.sandbox' : '';
		var countries = new PdwpCountries();

		j.ajax({
			type: 'POST',
			url:  'https://api'+mode+'.paypal.com/v1/payments/payment',
			headers: {
				'Authorization': "Bearer " + accesstoken,
				'Content-Type': 'application/json',
			},
			crossDomain: true,
			data: JSON.stringify({
				"intent":"sale",
				"payer":{
					"payment_method":"paypal",
					"funding_instruments":[
					{
						"payment_card":{
							"type": card.getType(),
							"number": cnumber,
							"expire_month": cc.expiry_month,
							"expire_year": cc.expiry_year,
							"cvv2": cc.ccv,
							"first_name": donate.donor_first_name,
							"billing_country": countries.getCode( donate.donor_country ),
							"last_name": donate.donor_last_name,
						}
					}
					]
				},
				"transactions":[
				{
					"amount":{
						"currency": currency[1],
						"total": donate.pdwp_sub_amt,
					},
					"description":"Pronto Donation PayPal Payment",
					"invoice_number": donate.pamynet_ref
				}
				]
			}),
			success: function( resp, status, xhr ) {
				if( xhr.status == 201 ) {

					var paymentDate = new Date( donate.timestamp * 1000 );

					donate.payment_response = resp;

					if( j.inArray( resp.state, ['completed', 'created', 'processed', 'approved'] ) != -1 ) {
						donate.statusCode = 1;
						donate.statusText = 'APPROVED';
					} else {
						donate.statusCode = 0;
						donate.statusText = 'FAILED';
					}

					donate.PaymentTransaction = {
						TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
						TxnId : resp.transactions[0].related_resources[0].sale.id,
						TxnDate : self.formatDate( paymentDate ),
						TxnMessage : resp.state + '-' + donate.pamynet_ref,
						TxnPaymentRef : donate.pamynet_ref
					}

					donate.card_details = self.clean( cc );

					resolve( {
						status : 'success',
						response : resp,
						donation : donate
					} );

				} else {
					resolve( {
						status : 'error',
						response : xhr.responseJSON.details.length === 0 ? xhr.responseJSON.message : xhr.responseJSON.details[0].issue,
						donation : donate
					} );
				}
			},
			error: function( resp, status, error ) {
				var errorMsg = self.getSafe(() => resp.responseJSON.details.length === 0 ? resp.responseJSON.message : resp.responseJSON.details[0].issue );
				resolve( {
					status : 'error',
					response : ( typeof errorMsg == 'undefined' ) ? "Something wen't wrong during Authentication to PayPal, Please try again or contact your administrator." : errorMsg,
					donation : donate
				} );
			}
		});
	});
};

PaypalGateway.prototype.saveCreditCard = function( param, cc, accesstoken ) {
	var self = this;
	return new Promise( function( resolve, reject ) {

		var cnumber = cc.card_number.replace(/ /g,'');
		var card = new CardValidate( cnumber );
		var donate = param.donation;
		var mode = ( self.config.settings.wfc_donation_sandboxmode == 'true' ) ? '.sandbox' : '';

		j.ajax({
			type: 'POST',
			url:  'https://api'+mode+'.paypal.com/v1/vault/credit-cards',
			headers: {
				'Authorization': "Bearer " + accesstoken,
				'Content-Type': 'application/json',
			},
			crossDomain: true,
			data: JSON.stringify({
				"type": card.getType(),
				"number": cnumber,
				"expire_month": cc.expiry_month,
				"expire_year": cc.expiry_year,
				"cvv2": cc.ccv,
				"first_name": donate.donor_first_name,
				"last_name": donate.donor_last_name,
				"external_customer_id": donate.pamynet_ref
			}),
			success: function( resp, status, xhr ) {
				if( xhr.status == 201 ) {
					resolve({
						status : 'success',
						card_id : resp.id,
						external_customer_id : resp.external_customer_id
					});
				} else {
					resolve( {
						status : 'error',
						response : xhr.responseJSON.details.length === 0 ? xhr.responseJSON.message : xhr.responseJSON.details[0].issue,
						donation : donate
					});
				}
			},
			error: function( resp, status, error ) {
				var errorMsg = self.getSafe(() => resp.responseJSON.details.length === 0 ? resp.responseJSON.message : resp.responseJSON.details[0].issue );
				resolve( {
					status : 'error',
					response : ( typeof errorMsg == 'undefined' ) ? "Something wen't wrong during Authentication to PayPal, Please try again or contact your administrator." : errorMsg,
					donation : donate
				} );
			}
		});
	});
};

PaypalGateway.prototype.saveCardProcessPayment = function( param, cc, accesstoken, card_id, external_customer_id ) {
	var self = this;
 	return new Promise( function( resolve, reject ) {

 		var donate = param.donation;
 		var mode = ( self.config.settings.wfc_donation_sandboxmode == 'true' ) ? '.sandbox' : '';
 		var currency = donate.currency.split(',');

 		j.ajax({
			type: 'POST',
			url:  'https://api'+mode+'.paypal.com/v1/payments/payment',
			headers: {
				'Authorization': "Bearer " + accesstoken,
				'Content-Type': 'application/json',
			},
			crossDomain: true,
			data: JSON.stringify({
				"id" : "CPPAY-"+external_customer_id,
				"intent":"sale",
				"payer":{
					"payment_method":"credit_card",
					"funding_instruments":[
						{
							"credit_card_token":{
	 							"credit_card_id" : card_id,
	 							"external_customer_id": external_customer_id
							}
						}
					]
				},
				"transactions":[
					{
						"amount":{
							"currency": currency[1],
							"total": donate.pdwp_sub_amt,
						},
						"description":"Pronto Donation PayPal Payment",
					}
				]
			}),
			success: function( resp, status, xhr ) {
				if( xhr.status == 201 ) {

					var paymentDate = new Date( donate.timestamp * 1000 );

					donate.payment_response = resp;

					if( j.inArray( resp.state, ['completed', 'created', 'processed', 'approved'] ) != -1 ) {
						donate.statusCode = 1;
						donate.statusText = 'APPROVED';
					} else {
						donate.statusCode = 0;
						donate.statusText = 'FAILED';
					}

					donate.PaymentTransaction = {
						TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
						TxnId : resp.transactions[0].related_resources[0].sale.id,
						TxnDate : self.formatDate( paymentDate ),
						TxnMessage : resp.state + '-' + donate.pamynet_ref,
						TxnPaymentRef : donate.pamynet_ref
					};

                    donate.TokenCustomerID = xhr.responseJSON.id;
					donate.card_details = self.clean( cc );

					resolve( {
						status : 'success',
						response : resp,
						donation : donate
					} );

				} else {
					if (xhr.responseJSON.name === 'UNKNOWN_ERROR') {
						resolve({
							status: 'error',
							response: 'PayPal Sandbox is not available for a moment',
							donation: donate
						})
					}
					else {
                        resolve( {
                            status : 'error',
                            response : xhr.responseJSON.details.length === 0 ? xhr.responseJSON.message : xhr.responseJSON.details[0].issue,
                            donation : donate
                        } );
					}
				}
			},
			error: function( resp, status, error ) {
				// var errorMsg = self.getSafe(() => resp.responseJSON.details.length === 0 ? resp.responseJSON.message : resp.responseJSON.details[0].issue );
                if (resp.responseJSON.name === 'UNKNOWN_ERROR') {
                    resolve({
                        status: 'error',
                        response: 'PayPal Sandbox is not available for a moment',
                        donation: donate
                    })
                }
                else {
                    resolve( {
                        status : 'error',
                        response : resp.responseJSON.details.length === 0 ? resp.responseJSON.message : resp.responseJSON.details[0].issue,
                        donation : donate
                    } );
                }
			}
		});
 	});
};

PaypalGateway.prototype.preparePaypalRedirect = function( param ) {
	var self = this;
    // j('#wfc-paypal-button-container').hide();
	return new Promise( function( resolve, reject ) {
		// ------------------ OLD REDIRECT PAYMENT ------------------
		// j.ajax({
		// 	type: 'POST',
		// 	url:  pdwp_ajax.url,
		// 	data: {
		// 		'action':'wfc_prepare_paypal_redirect',
		// 		donate : param.donation,
		// 		nonce: pdwp_ajax.nonce,
		// 	},
		// 	success: function( resp ) {
		// 		resolve( resp );
		// 	},
		// 	error: function( resp ) {
		// 		reject( resp );
		// 	}
		// });
		
		// ------------------ NEW REDIRECT PAYMENT ------------------

		var donate = param.donation;
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_prepare_paypal_redirect',
				donate : param.donation,
				nonce: pdwp_ajax.nonce,
			},
			success: function( resp ) {
				var cancel_url = resp.cancel_url;

				var payment_mode = ( self.config.settings.wfc_donation_sandboxmode == 'true' ) ? 'sandbox' : 'production';
				var amount = donate.pdwp_sub_amt;
				var currency = j('input[name=currency]').val().split(',');
				currency = currency[1];

				paypal.Button.render({

		            env: payment_mode, // sandbox | production

		            // Specify the style of the button
			        style: {
			            label: 'pay',  // checkout | credit | pay | buynow | generic
			            size:  'responsive', // small | medium | large | responsive
			            shape: 'rect',   // pill | rect
			            color: 'blue'   // gold | blue | silver | black
			        },

		            // PayPal Client IDs - replace with your own
		            // Create a PayPal app: https://developer.paypal.com/developer/applications/create
		            client: {
		                sandbox:    self.config.settings.wfc_donation_paypalclientid,
		                production: self.config.settings.wfc_donation_paypalclientid
		            },

		            // Show the buyer a 'Pay Now' button in the checkout flow
		            commit: true,

		            // payment() is called when the button is clicked
		            payment: function(data, actions) {

		                // Make a call to the REST api to create the payment
		                return actions.payment.create({
		                    payment: {
		                        transactions: [
		                            {
		                                amount: { 
		                                	total: amount,
		                                	currency: currency 
		                                }
		                            }
		                        ]
		                    }
		                });
		            },

		            // onAuthorize() is called when the buyer approves the payment
		            onAuthorize: function(data, actions) {

		                // Make a call to the REST api to execute the payment
		                return actions.payment.execute()
		                .then(function(resp) {
							var paymentDate = new Date( donate.timestamp * 1000 );

							donate.payment_response = resp;

							if( j.inArray( resp.state, ['completed', 'created', 'processed', 'approved'] ) != -1 ) {
								donate.statusCode = 1;
								donate.statusText = 'APPROVED';
							} else {
								donate.statusCode = 0;
								donate.statusText = 'FAILED';
							}

							donate.PaymentTransaction = {
								TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
								TxnId : resp.transactions[0].related_resources[0].sale.id,
								TxnDate : self.formatDate( paymentDate ),
								TxnMessage : resp.state + '-' + donate.pamynet_ref,
								TxnPaymentRef : donate.pamynet_ref
							}

							donate.TokenCustomerID = ( typeof resp.id != 'undefined' ) ? resp.id : null;

							resolve( {
								status : 'success',
								response : resp,
								donation : donate
							} );

		                });
		            },

		            onCancel: function(data, actions) {
			            window.location.replace(cancel_url);
			        },

					onError: function(err) {
		            	var errMessage = err.message;
		            	errMessage = errMessage.replace(/^Error.*\s*/gm, '');
		            	errMessage = errMessage.replace(/\s+at.*\)/gm, '');

		            	var json = JSON.parse(errMessage);

		            	resolve({
							status: 'error',
							response: json.details.length === 0 ? json.message : json.details[0].issue,
							donation: donate
						});
					}

		        }, '#wfc-paypal-button-container')
					.then(function() {
						//WFC_Donation_Tinter.hide();
						SaiyanToolkit.spinner.hide();
		    			SaiyanToolkit.tinter.hide();

						j('.wfc_donation-amount-main-container, ' +
							'.wfc_donation-frequency-main-container, ' +
							'.wfc_donation-donor-details-main-container, ' +
							'.wfc_donation-payment-gateway-main-container').css('pointer-events', 'none');

						var ppFocus = j('<div id="pp-focus"></div>').css({
							'position': 'absolute',
							'width': '100%',
							'height': '100%',
							'top': '0',
							'pointer-events': 'none',
							'z-index': '9999'
						}).append(
							j('<img src="' + wfc_pp.plugin_url + '/images/click.png" width="50px" height="50px" />')
								.css({
									'position': 'absolute',
									'right': '-50px',
									'bottom': '-25px',
									'opacity': '0'
								})
						);
						j('#wfc-paypal-button-container').find('.paypal-button-context-iframe')
							.css('position', 'relative')
							.append(ppFocus);

						j('#pp-focus img').animate({
                            'right': '20px',
                            'bottom': '10px',
							'width': '60px',
							'opacity': '1'
						}, 800, 'easeOutCubic', function() {
							setTimeout(function() {
                                j('#pp-focus img').addClass('pp-focus-breathing-pointer');
                                j('#wfc-paypal-button-container').find('iframe').addClass('pp-focus-breathing-btn');
							}, 500);
						});

						/*
						setTimeout(function() {
                            $('#pp-focus').css({
                                'opacity': '1',
                                'transform': 'scale(1)'
                            });
						}, 300);
						*/
					});
				
			},
			error: function( resp ) {
				reject( resp );
			}
		});

		
	});
};

/*
* Get the data of fields on form
*/
PaypalGateway.prototype.getDonationFormDetails = function() {
	var details = j('#pdwp-form-donate').serializeArray();
	
	var donation = {};
	var creditcard = {
		card_number: j('#card_number').val(),
		ccv: j('#ccv').val(),
		name_on_card: j('#name_on_card').val(),
		expiry_month: j('#expiry_month').val(),
		expiry_year: j('#expiry_year').val()
	};

	j.each( details, function( k, v ) {
		if( v.name.indexOf( '[' ) > -1 || v.name.indexOf( ']' ) > -1 ) {
			var newkey = v.name.split('[')[0];
			donation[newkey] = v.value;
		} else {
			donation[v.name] = v.value;
		}
	});

	this.creditCardDetails = creditcard;

	var gateway = donation.payment_gateway.split( '_' );
	var gatewayKey 		= gateway.length - 2;
	var gatewayname 	= ( gateway.length > 1 ) ? gateway[ gatewayKey ] : gateway[0];

	if( gatewayname == 'nab' ) {
		var bank = {
			bank_code : j('#bank_code').val(),
			bank_number : j('#account_number').val(),
			bank_name : j('#account_name').val()
		};
	}else if(gatewayname == 'paymentfile'){
		var bank = {
			bank_code : j('#wfc_paymentfile_bank_code').val(),
			bank_number : j('#wfc_paymentfile_account_number').val(),
			bank_name : j('#wfc_paymentfile_account_holder_name').val()
		};
	}else{
		var bank = {
			bank_code : j('#pdwp_bank_code').val(),
			bank_number : j('#pdwp_account_number').val(),
			bank_name : j('#pdwp_account_holder_name').val()
		};
	}

	return {
		donation : donation,
		creditcard : creditcard,
		bank : bank
	};
}


PaypalGateway.prototype.processPayment = function( param, cc ) {
	var self = this;
 	return new Promise( function( resolve, reject ) {
 		if( param.donation.pdwp_donation_type != 'one-off' ) {
 			resolve({
 				status : 'error',
 				response : param.donation.pdwp_donation_type + ' is not supported for Paypal.',
 				donation : param.donation
 			});
 		}else {
 			var wfc_cartdonationtypes = param.donation.wfc_cartdonationtypes;
 			wfc_cartdonationtypes = wfc_cartdonationtypes.toLowerCase();
 			if( wfc_cartdonationtypes.includes("daily") == true ||
 				wfc_cartdonationtypes.includes("weekly") == true ||
 				wfc_cartdonationtypes.includes("fortnightly") == true ||
 				wfc_cartdonationtypes.includes("monthly") == true ||
 				wfc_cartdonationtypes.includes("quarterly") == true ||
 				wfc_cartdonationtypes.includes("yearly") == true
 			) {	
 				resolve({
	 				status : 'error',
	 				response : 'Recurring payment is not supported for Paypal.',
	 				donation : param.donation
	 			});
 			}else{
		 		if( self.config.settings.wfc_donation_paypayredirect == 'true' ) {
		 			self.preparePaypalRedirect( param )
		 			.then(function( result ) {
		 				resolve( result );
		 				// if( result.status == 'success' ) {
		 				// 	var url = ( self.this.config.settings.wfc_donation_sandboxmode == 'true' ) ? 'https://www.sandbox.paypal.com' : 'https://www.paypal.com';
		 				// 	self.post( url + '/cgi-bin/webscr', result.redirect_data );
		 				// }
		 			})
		 			.catch(function( error ) {
		 				reject( error );
		 			});
		 		} else {
		 			if( self.config.settings.wfc_donation_tokenize == 'true' ) {
		 				var accesstoken = null;
		 				self.getAccessToken( param, cc )
		 				.then(function( result ) {
		 					if( result.status == 'success' ) {
		 						accesstoken = result.accesstoken;
		 						return self.saveCreditCard( param, cc, result.accesstoken );
		 					} else {
		 						resolve({
		 							status : 'error',
		 							response : "Something wen't wrong during Authentication to PayPal or PayPal sandbox is not available at this moment, Please try again or contact your administrator.",
		 							donation : param.donation
		 						});
		 					}
		 				})
		 				.then(function( result ) {
		 					if( result.status == 'success' ) {
		 						return self.saveCardProcessPayment(param, cc, accesstoken, result.card_id, result.external_customer_id);
		 					} else {
		 						resolve( result );
		 					}
		 				})
		 				.then(function( result ) {
		 					resolve( result );
		 				})
		 				.catch(function(error){
		 					reject(error);
		 				});
		 			} else {
		 				self.getAccessToken( param, cc )
		 				.then(function( result ) {
		 					if( result.status == 'success' ) {
		 						return self.directCreditCardPayment( param, cc, result.accesstoken );
		 					} else {
		 						resolve({
		 							status : 'error',
		 							response : "Something wen't wrong during Authentication to PayPal, Please try again or contact your administrator.",
		 							donation : param.donation
		 						});
		 					}
		 				})
		 				.then(function( result ) {
		 					resolve( result );
		 				})
		 				.catch(function(error){
		 					reject(error);
		 				});
		 			}
		 		}
 			}
 		}
 	});
};







PaypalGateway.prototype.generatePaypalButton = function() {
	var self = this;
	var donationForm = PaypalGateway.prototype.getDonationFormDetails();
	var donateProcessResp = [];

	var template_color = j('#template_color').val();
    j.ajax({
		type: 'POST',
		url:  pdwp_ajax.url,
		data: {
			'action':'wfc_prepare_paypal_redirect',
			donation : donationForm.donation,
			nonce: pdwp_ajax.nonce,
		},
		success: function( resp ) {
			console.log('PayPal Generate Button');
			donationForm = resp.donate;
			var cancel_url = resp.cancel_url;

			if (j('#pdwp_sub_amt').length) {
				if (j('#pdwp_sub_amt').attr('type') == 'text') {
					console.log(resp);
				}
			}

		    paypal.Buttons({

		        style: {
		            layout:  'horizontal',
		            label: 'pay',  // checkout | credit | pay | buynow | generic
		            size:  'responsive', // small | medium | large | responsive
		            shape: 'rect',   // pill | rect
		            color: 'blue',   // gold | blue | silver | black
		            tagline: false
		        },

		        createOrder: function(data, actions) {
		            console.log('PayPal createOrder');
		            if (j('#pdwp_sub_amt').length) {
						if (j('#pdwp_sub_amt').attr('type') == 'text') {
							console.log(data);
						}
					}	

		           	if (!j('form#pdwp-form-donate')[0].checkValidity()) {
			    		j('#wfc-paypal-button-container').empty();
		            	PaypalGateway.prototype.generatePaypalButton();
			    		j('form#pdwp-form-donate')[0].reportValidity();
		           	}else{
		           		var donationFormSecond = PaypalGateway.prototype.getDonationFormDetails();
		           		donationFormSecond.donation.paypal_redirect = 'true';
		           		WFC_Donation_LoadingBar.load('Validating Donor Details', 25, template_color);
		           		j.ajax({
							type: 'POST',
							url:  pdwp_ajax.url,
							data: {
								'action':'clean_validate_donor', 
								donation: donationFormSecond.donation,
								nonce: pdwp_ajax.nonce
							},
							success: function(resp) {
								var noneerrorfields_list = [
									{id: 'card_number'},
									{id: 'name_on_card'},
									{id: 'expiry_month'},
									{id: 'expiry_year'},
									{id: 'ccv'},
									{id: 'pdwp_bank_code'},
									{id: 'pdwp_account_number'},
									{id: 'pdwp_account_holder_name'},
									{id: 'wfc_paymentfile_bank_code'},
									{id: 'wfc_paymentfile_account_number'},
									{id: 'wfc_paymentfile_account_holder_name'}
								];

								PaypalGateway.prototype.unhighlightFieldErrorValidation(noneerrorfields_list);

								if (j('#pdwp_sub_amt').length) {
									if (j('#pdwp_sub_amt').attr('type') == 'text') {
										console.log(resp);
									}
								}	

								if( resp.status == 'failed' ) {
									j('#wfc-paypal-button-container').empty();
		            				var PaypalButton = PaypalGateway.prototype.generatePaypalButton();

		            				var errMSG = '';
		            				var field_error = resp.field_error.errors;
		            				var focuselemname = resp.field_error.focuselemname;

		            				if (field_error.length === 0) {
		            					errMSG = 'Something went wrong, please contact the administrator.';
		            				}else{
		            					j.each(field_error, function( index, value ) {
		            						errMSG = errMSG+' '+value;
										});
		            				}

		            				setTimeout(function(){
		            					WFC_Donation_LoadingBar.load('', 0, template_color);
			            				SaiyanToolkit.spinner.hide();
									    SaiyanToolkit.tinter.hide();
									    SaiyantistCore.snackbar.hide();
										SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+errMSG+'</div>',7000,'error');

										var attr = $('#pdwp-form-donate').attr('wfc-don-advance-wizard');
										if (typeof attr !== typeof undefined && attr !== false) {
											var field_error_notice_type = $('#field_error_notice_type').val();
											if(field_error_notice_type.indexOf("fieldglow") >= 0) {
												if(focuselemname == 'g-recaptcha-response'){
													$('#pdwp-recaptcha .g-recaptcha > div')
														.addClass('wfc-don-invalid-field')
														.after(
															$('<small id="pdwp-recaptcha-error-notification" class="wfc-don-invalid-field-message">Recaptcha is required</small>')
														);
												}

											}
										}else{
											let focusObj = j('[name='+focuselemname+']');
											j('html,body').animate({scrollTop: j(focusObj).get(0).offsetTop - 200}, 1000, function() {
												j('[name='+focuselemname+']').get(0).focus();
											});
										}

		            				}, 2000);

								} else {
									WFC_Donation_LoadingBar.load('Processing Payment Transaction', 50, template_color);
									donateProcessResp = {
										status : 'success',
										donation : resp.donation
									};
								}
							},
							error: function(resp) {
								if (j('#pdwp_sub_amt').length) {
									if (j('#pdwp_sub_amt').attr('type') == 'text') {
										console.log(resp);
									}
								}	
								reject( resp );
							}
						});
		            }

		            var amount = donationForm.pdwp_sub_amt;
		            return actions.order.create({
		                purchase_units: [{
		                    amount: {
		                        value: amount
		                    }
		                }]
		            })

		        },

		        onApprove: function(data, actions) {
		        	console.log('PayPal onApprove');
		            return actions.order.capture().then(function(resp) {
		            	if (j('#pdwp_sub_amt').length) {
							if (j('#pdwp_sub_amt').attr('type') == 'text') {
								console.log(resp);
							}
						}

		                var onApproveDonate = donateProcessResp.donation;
		                var paymentDate = new Date( onApproveDonate.timestamp * 1000 );

						onApproveDonate.payment_response = resp;

						if( j.inArray( resp.status, ['completed', 'COMPLETED', 'created', 'CREATED', 'processed', 'PROCESSED', 'approved', 'APPROVED'] ) != -1 ) {
							onApproveDonate.statusCode = 1;
							onApproveDonate.statusText = 'APPROVED';
						} else {
							onApproveDonate.statusCode = 0;
							onApproveDonate.statusText = 'FAILED';
						}

						onApproveDonate.PaymentTransaction = {
							TxnStatus : ( onApproveDonate.statusCode == 1 ) ? 'Success' : 'Failed',
							TxnId : resp.id,
							TxnDate : self.formatDate( paymentDate ),
							TxnMessage : resp.status + '-' + onApproveDonate.pamynet_ref,
							TxnPaymentRef : onApproveDonate.pamynet_ref,
							PaymentCaptureId : resp.purchase_units[0].payments.captures[0].id
						}

						onApproveDonate.TokenCustomerID = ( typeof resp.id != 'undefined' ) ? resp.id : null;

						console.log('Update Doantion Data');
						WFC_Donation_LoadingBar.load('Saving Payment Transaction', 70, template_color);
						PaypalGateway.prototype.donationData( {
							status : 'success',
							response : resp,
							donation : onApproveDonate
						}, 'update');
		            });
		        },
		        onCancel: function(data, actions) {
		        	console.log('PayPal onCancel');
		        	WFC_Donation_LoadingBar.load('Payment has been cancelled. Redirecting Page Now', 100, template_color);
		            window.location.replace(cancel_url);
		        },

		        onError: function(err) {
		        	console.log('PayPal onError');
		        	console.log(err);
		        	var errMessage = err.message;
	            	errMessage = errMessage.replace(/^Error.*\s*/gm, '');
	            	errMessage = errMessage.replace(/\s+at.*\)/gm, '');

	            	var json = JSON.parse(errMessage);

	            	WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
					PaypalGateway.prototype.resetForm( {
						status: 'error',
						response: err,
						donation: donateProcessResp.donation
					} );

				}
		    }).render('#wfc-paypal-button-container')
				.then(function() {
					//WFC_Donation_Tinter.hide();
					SaiyanToolkit.spinner.hide();
	    			SaiyanToolkit.tinter.hide();

					var ppFocus = j('<div id="pp-focus"></div>').css({
						'position': 'absolute',
						'width': '100%',
						'height': '100%',
						'top': '0',
						'pointer-events': 'none',
						'z-index': '9999'
					}).append(
						j('<img src="' + wfc_pp.plugin_url + '/images/click.png" width="50px" height="50px" />')
							.css({
								'position': 'absolute',
								'right': '-50px',
								'bottom': '-25px',
								'opacity': '0'
							})
					);
					j('#wfc-paypal-button-container').find('.paypal-buttons-context-iframe')
						.css('position', 'relative')
						.append(ppFocus);

					j('#pp-focus img').animate({
	                    'right': '20px',
	                    'bottom': '-20px',
						'width': '60px',
						'opacity': '1'
					}, 800, 'easeOutCubic', function() {
						setTimeout(function() {
	                        j('#pp-focus img').addClass('pp-focus-breathing-pointer');
	                        j('#wfc-paypal-button-container').find('iframe').addClass('pp-focus-breathing-btn');
						}, 500);
					});

					j('#pdwp-btn').hide();
				});

			},
		error: function( resp ) {
			reject( resp );
		}
	});
};

DonateProcessor.prototype.resetForm = function( donation ) {
	console.log('resetForm');
	j.ajax({
		type: 'POST',
		url:  pdwp_ajax.url,
		data: {
			'action':'wfc_reset_form',
			param: donation,
			nonce: pdwp_ajax.nonce
		},
		success: function(resp) {
			if (j('#pdwp_sub_amt').length) {
				if (j('#pdwp_sub_amt').attr('type') == 'text') {
					console.log(resp);
				}
			}	

			if( resp.status == 'success' ) {
				window.location.replace( resp.url );
			}
		},
		error: function(resp) {
			console.log(resp);
			return resp;
		}
	});
};

PaypalGateway.prototype.donationData = function( donation, method ) {
	console.log('donationData');
	var donationDataFdonationForm = PaypalGateway.prototype.getDonationFormDetails();

	j.ajax({
		type: 'POST',
		url:  pdwp_ajax.url,
		data: {
			'action':'wfc_donation_data',
			param: donation,
			method: method,
			nonce: pdwp_ajax.nonce
		},
		success: function( resp ) {
			console.log('Donation Data Saved');
			PaypalGateway.prototype.syncdonationDataToSF(resp, donationDataFdonationForm);

			if (j('#pdwp_sub_amt').length) {
				if (j('#pdwp_sub_amt').attr('type') == 'text') {
					console.log(resp);
				}
			}	

			return resp;
		},
		error: function(resp) {
			console.log('Donation Data not Saved');
			console.log(resp);
			return resp;
		}
	});
};

PaypalGateway.prototype.syncdonationDataToSF = function( donation, donationForm ) {
	console.log('syncdonationDataToSF');
	WFC_Donation_LoadingBar.load('Finalizing Payment', 80, template_color);
	j.ajax({
		type: 'POST',
		url:  pdwp_ajax.url,
		data: {
			'action':'sync_donation_data',
			param: donation,
			getPaymentSource : PaypalGateway.prototype.getPaymentSource(donation, donationForm),
			nonce: pdwp_ajax.nonce
		},
		success: function( resp ) {
			if (j('#pdwp_sub_amt').length) {
				if (j('#pdwp_sub_amt').attr('type') == 'text') {
					console.log(resp);
				}
			}	

			PaypalGateway.prototype.completeDonation(resp.donation);
			return resp;
		},
		error: function(resp) {
			console.log(resp);
			return resp;
		}
	});
};

PaypalGateway.prototype.completeDonation = function( donation ) {
	console.log('completeDonation');
	WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
	j.ajax({
		type: 'POST',
		url:  pdwp_ajax.url,
		data: {
			'action':'wfc_complete_donation',
			donation: donation,
			nonce: pdwp_ajax.nonce
		},
		success: function( resp ) {
			if (j('#pdwp_sub_amt').length) {
				if (j('#pdwp_sub_amt').attr('type') == 'text') {
					console.log(resp);
				}
			}	
			
			if( resp.status == 'success' ) {
				window.location.replace( resp.url );
			}
			if( resp.status == 'failed' ) {
				window.location.replace( resp.url );
			}
		},
		error: function(resp) {
			console.log(resp);
			return resp;
		}
	});
};

PaypalGateway.prototype.getPaymentSource = function( donation, donationForm ) {
	var donation_process = donation.donation;
	var donation_details = donationForm.donation;
	if( donation_process.hasOwnProperty( 'TokenCustomerID' ) && donation_process.TokenCustomerID != '' ) {
		return {
			ccno : ( typeof donationForm.creditcard.card_number != 'undefined' ) ? donationForm.creditcard.card_number.replace(/ /g,'') : '',
			ccname : ( typeof donationForm.creditcard.name_on_card != 'undefined' ) ? donationForm.creditcard.name_on_card : '',
			expmonth : ( typeof donationForm.creditcard.expiry_month != 'undefined' ) ? donationForm.creditcard.expiry_month : '',
			expyear : ( typeof donationForm.creditcard.expiry_year != 'undefined' ) ? donationForm.creditcard.expiry_year : '',
			ccv :  ( typeof donationForm.creditcard.ccv != 'undefined' ) ? donationForm.creditcard.ccv.replace(/ /g,'') : '',
			token : ( typeof donation_process.TokenCustomerID != 'undefined' ? donation_process.TokenCustomerID : null ),
			psType : 'token'
		};
	} else if( donation_details.hasOwnProperty( 'payment_type' ) && donation_details.payment_type == 'bank' ) {
		return {
			bsbname : ( typeof donationForm.bank.bank_name != 'undefined' ? donationForm.bank.bank_name : null ),
			bsbno : ( typeof donationForm.bank.bank_code != 'undefined' ? donationForm.bank.bank_code : null ),
			bsbaccount : ( typeof donationForm.bank.bank_number != 'undefined' ? donationForm.bank.bank_number : null ),
			psType : 'bank'
		};
	} else {
		return {
			ccno : ( typeof donationForm.creditcard.card_number != 'undefined' ) ? donationForm.creditcard.card_number.replace(/ /g,'') : '',
			ccname : ( typeof donationForm.creditcard.name_on_card != 'undefined' ) ? donationForm.creditcard.name_on_card : '',
			expmonth : ( typeof donationForm.creditcard.expiry_month != 'undefined' ) ? donationForm.creditcard.expiry_month : '',
			expyear : ( typeof donationForm.creditcard.expiry_year != 'undefined' ) ? donationForm.creditcard.expiry_year : '',
			ccv :  ( typeof donationForm.creditcard.ccv != 'undefined' ) ? donationForm.creditcard.ccv.replace(/ /g,'') : '',
			psType : 'cc'
		};
	}
};

PaypalGateway.prototype.unhighlightFieldErrorValidation = function(noneerrorfields_list){
	j('.wfc-don-invalid-field-message').remove();
	j.each(noneerrorfields_list, function(index, value) {
		j('#'+value.id).removeClass('wfc-don-invalid-field');
	});
}
