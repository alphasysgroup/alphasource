<?php

require_once 'paypal_sdk/autoload.php';

use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;

defined('ABSPATH') or die('No script kiddie please!');

if (! class_exists('PayPal_WFC_Donation')) {

	define('WFC_DONATION_PP_CREDIT_CARD', 'credit_card');
	define('WFC_DONATION_PP_PAYPAL', 'paypal');

	define('WFC_DONATION_PP_CUR_AUD', 'AUD');
	define('WFC_DONATION_PP_CUR_BRL', 'BRL');
	define('WFC_DONATION_PP_CUR_CAD', 'CAD');
	define('WFC_DONATION_PP_CUR_CZK', 'CZK');
	define('WFC_DONATION_PP_CUR_DKK', 'DKK');
	define('WFC_DONATION_PP_CUR_EUR', 'EUR');
	define('WFC_DONATION_PP_CUR_HKD', 'HKD');
	define('WFC_DONATION_PP_CUR_HUF', 'HUF');
	define('WFC_DONATION_PP_CUR_ILS', 'ILS');
	define('WFC_DONATION_PP_CUR_JPY', 'JPY');
	define('WFC_DONATION_PP_CUR_MYR', 'MYR');
	define('WFC_DONATION_PP_CUR_MXN', 'MXN');
	define('WFC_DONATION_PP_CUR_NZD', 'NZD');
	define('WFC_DONATION_PP_CUR_NOK', 'NOK');
	define('WFC_DONATION_PP_CUR_PHP', 'PHP');
	define('WFC_DONATION_PP_CUR_PLN', 'PLN');
	define('WFC_DONATION_PP_CUR_GBP', 'GBP');
	define('WFC_DONATION_PP_CUR_RUB', 'RUB');
	define('WFC_DONATION_PP_CUR_SGD', 'SGD');
	define('WFC_DONATION_PP_CUR_SEK', 'SEK');
	define('WFC_DONATION_PP_CUR_CHF', 'CHF');
	define('WFC_DONATION_PP_CUR_THB', 'THB');
	define('WFC_DONATION_PP_CUR_USD', 'USD');

	define('WFC_DONATION_PP_INTENT_SALE', 'sale');
	define('WFC_DONATION_PP_INTENT_AUTHORIZE', 'authorize');
	define('WFC_DONATION_PP_INTENT_ORDER', 'order');

	/**
	 * Class PayPal_WFC_Donation
	 *
	 * PayPal payment gateway class for creating and executing payments
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	class PayPal_WFC_Donation {

		private $credentials;
		private $transaction_details;

		private $payer;
		private $item_list;
		private $details;
		private $amount;
		private $transaction;
		private $redirect_urls;
		private $payment;

		/**
		 * PayPal_WFC_Donation constructor.
		 *
		 * Initialize required objects for creating payments
		 * including credentials and details for payment transaction
		 *
		 * @param array $credentials Contains client_id and client_secret
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		public function __construct($credentials) {
			$this->credentials = $credentials;

			$this->payer = new Payer();
			$this->item_list = new ItemList();
			$this->details = new Details();
			$this->amount = new Amount();
			$this->transaction = new Transaction();
			$this->redirect_urls = new RedirectUrls();
			$this->payment = new Payment();
		}

		/**
		 * Returns ApiContext after authenticating to the PayPal services using the provided
		 * client_id and client_secret that is supplied from the $credentials
		 *
		 * @return ApiContext
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		private function get_api_context() {
			if (! is_array($this->credentials))
				throw new InvalidArgumentException('credentials supplied is not valid');
			if (! isset($this->credentials['client_id']))
				throw new InvalidArgumentException('client_id is needed');
			if (! isset($this->credentials['client_secret']))
				throw new InvalidArgumentException('client_secret is needed');
			$api_context = new ApiContext(
				new OAuthTokenCredential(
					$this->credentials['client_id'],
					$this->credentials['client_secret']
				)
			);
			$api_context->setConfig(
				array(
					'mode' => 'sandbox',
					'log.LogEnabled' => true,
					'log.FileName' => '../PayPal.log',
					'log.LogLevel' => 'DEBUG',
					'cache.enabled' => false
				)
			);

			return $api_context;
		}

		/**
		 * Creates payment transaction with the provided details:
		 *      - payment_method (default: WFC_DONATION_PP_PAYPAL)
		 *      - items (array: contains name, currency: e.g. WFC_DONATION_PP_CUR_USD, quantity, sku, price)
		 *      - details (contains: shipping, tax)
		 *      - amount (contains: currency)
		 *      - transaction_description
		 *      - redirection_urls (contains: return_url, cancel_url)
		 *      - payment_intent (default: WFC_DONATION_PP_INTENT_SALE)
		 *
		 * @param array $transaction_details Contains transaction details needed to create payment transactions
		 *
		 * @return null|Payment Payment object after the payment is successfully created
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		public function create_payment($transaction_details) {
			// setting payment_method
			if (isset($transaction_details['payment_method'])) {
				$this->payer->setPaymentMethod($transaction_details['payment_method']);
			}
			else {
				$this->payer->setPaymentMethod(WFC_DONATION_PP_PAYPAL);
			}

			// setting for items
			$sub_total = 0;
			if (isset($transaction_details['items'])) {
				if (! is_array($transaction_details['items']))
					throw new InvalidArgumentException('Invalid items mapping');

				foreach ($transaction_details['items'] as $item) {
					if (! isset($item['name']))
						throw new InvalidArgumentException('name is needed for item');

					if (isset($item['currency'])) $currency = $item['currency'];
					else $currency = WFC_DONATION_PP_CUR_USD;

					if (! isset($item['quantity']))
						throw new InvalidArgumentException('quantity is needed for item');

					if (! isset($item['sku']))
						throw new InvalidArgumentException('sku is needed for item');

					if (! isset($item['price']))
						throw new InvalidArgumentException('price is needed for item');

					$new_item = new Item();
					$new_item
						->setName($item['name'])
						->setCurrency($currency)
						->setQuantity($item['quantity'])
						->setSku($item['sku'])
						->setPrice($item['price']);

					$this->item_list->addItem($new_item);

					$sub_total += $item['quantity'] * $item['price'];
				}
			}
			else throw new InvalidArgumentException('items is needed for the transaction');

			// setting for details
			if (isset($transaction_details['details'])) {
				if (! is_array($transaction_details['details']))
					throw new InvalidArgumentException('Invalid details mapping');

				$details = $transaction_details['details'];

				if (! isset($details['shipping']))
					throw new InvalidArgumentException('shipping is needed for details');

				if (! isset($details['tax']))
					throw new InvalidArgumentException('tax is needed for details');

				/*
				if (! isset($details['sub_total']))
					throw new InvalidArgumentException('sub_total is needed for details');
				*/

				$this->details
					->setShipping($details['shipping'])
					->setTax($details['tax'])
					->setSubtotal($sub_total);

				$amount_total = $details['shipping'] + $details['tax'] + $sub_total;
			}
			else throw new InvalidArgumentException('details is needed for the transaction');

			// setting for amount
			if (isset($transaction_details['amount'])) {
				if (! is_array($transaction_details['amount']))
					throw new InvalidArgumentException('Invalid amount mapping');

				$amount = $transaction_details['amount'];

				if (! isset($amount['currency']))
					throw new InvalidArgumentException('currency is needed for amount');

				/*
				if (! isset($amount['total']))
					throw new InvalidArgumentException('total is needed for amount');
				*/

				$this->amount
					->setCurrency($amount['currency'])
					->setTotal($amount_total)
					->setDetails($this->details);
			}
			else throw new InvalidArgumentException('amount is needed for the transaction');

			// setting for transaction
			if (isset($transaction_details['transaction_description'])) {
				$this->transaction
					->setAmount($this->amount)
					->setItemList($this->item_list)
					->setDescription($transaction_details['transaction_description'])
					->setInvoiceNumber(uniqid());
			}
			else throw new InvalidArgumentException('transaction_description is needed for the transaction');

			// setting for redirection_urls
			if (isset($transaction_details['redirection_urls'])) {
				if (! is_array($transaction_details['redirection_urls']))
					throw new InvalidArgumentException('Invalid redirection_urls mapping');

				$redirection_urls = $transaction_details['redirection_urls'];

				if (! isset($redirection_urls['return_url']))
					throw new InvalidArgumentException('return_url is needed for redirection_urls');

				if (! isset($redirection_urls['cancel_url']))
					throw new InvalidArgumentException('cancel_url is needed for redirection_urls');

				$this->redirect_urls
					->setReturnUrl($redirection_urls['return_url'])
					->setCancelUrl($redirection_urls['cancel_url']);
			}
			else throw new InvalidArgumentException('redirection_urls is needed for the transaction');

			// setting for payment_intent
			$this->payment
				->setPayer($this->payer)
				->setRedirectUrls($this->redirect_urls)
				->setTransactions(array($this->transaction));
			if (isset($transaction_details['payment_intent']))
				$this->payment
					->setIntent($transaction_details['payment_intent']);
			else
				$this->payment
					->setIntent(WFC_DONATION_PP_INTENT_SALE);


			// creating payment
			try {
				$this->payment->create($this->get_api_context());
			} catch (\PayPal\Exception\PayPalConnectionException $exception) {
				echo $exception->getData();
				return null;
			}

			return $this->payment;
		}

		/**
		 * Prints out the Checkout Link
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		public function checkout_link() {
			echo '<a href="' . $this->payment->getApprovalLink() . '" target="_blank">Checkout</a>';
		}

		/**
		 * Returns the Checkout Link
		 *
		 * @return null|string Checkout Link
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		public function get_checkout_link() {
			return $this->payment->getApprovalLink();
		}

		/**
		 * Executes the payment after successfully performing checkout
		 *
		 * @param string $payment_id    $_GET['paymentId'] provided after success checkout
		 * @param string $payer_id      $_GET['PayerID'] provided after success checkout
		 *
		 * @return null|Payment         Payment object after success checkout
		 *
		 * @since 1.0.0
		 * @author Von Sienard Vibar <von@alphasys.com.au>
		 */
		public function execute_link($payment_id, $payer_id) {
			$payment = Payment::get($payment_id, $this->get_api_context());

			$execution = new PaymentExecution();
			$execution->setPayerId($payer_id);

			$result = null;

			try {
				$result = $payment->execute($execution, $this->get_api_context());
			} catch (Exception $exception) {
				echo $exception->getMessage();
			}

			return $result;
		}
	}
}