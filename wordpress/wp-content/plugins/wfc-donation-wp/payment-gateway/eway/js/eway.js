var j = jQuery;

var EwayGateway = function( config ) {
	this.payment = config;
	j('#pdwp-btn').show();
	
	j('#wfc_donation-payment-details-toogle-container').show();
	j("label[for=payment_type_credit_card]").click();
	this.config = config;
	
};

EwayGateway.prototype.clean = function( cc ) {

	var creditcarddetails = {
		card_number: cc.card_number,
		name_on_card: cc.name_on_card,
		expiry_month: cc.expiry_month,
		expiry_year: cc.expiry_year,
		ccv: cc.ccv
	};

	var regex = /\d(?=\d{4})/mg;
	var str = creditcarddetails.card_number.replace(/ /g,'');
	var subst = '*';

	creditcarddetails.card_number = str.replace(regex, subst);
	creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

	return creditcarddetails;
};

EwayGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

EwayGateway.prototype.encrpt = function( param, cc ) {

	var creditcarddetails = {
		cn: cc.card_number.replace(/ /g,''),
		noc: cc.name_on_card,
		em: cc.expiry_month,
		ey: cc.expiry_year,
		cvn: cc.ccv
	};

	creditcarddetails.cn = eCrypt.encryptValue( creditcarddetails.cn, this.payment.settings.wfc_donation_ewayencryptionkey );
	creditcarddetails.cvn = eCrypt.encryptValue( creditcarddetails.cvn, this.payment.settings.wfc_donation_ewayencryptionkey );

	return creditcarddetails;
};

EwayGateway.prototype.creditCardPayment = function( param, cc ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_eway_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				process: 'creditcard',
				cc: self.encrpt( param, cc )
			},
			success: function( resp ) {
 				if( resp.status == 'success' && resp.status != undefined) {

 					var donate = param.donation;
 					var paymentDate = new Date( donate.timestamp * 1000 );

 					donate.payment_response = {
 						TransactionID: resp.response.TransactionID,
 						TransactionStatus: resp.response.TransactionStatus,
 						TransactionType: resp.response.TransactionType,
 						AuthorisationCode: resp.response.AuthorisationCode,
 						ResponseCode: resp.response.ResponseCode,
 						ResponseMessage: resp.response.ResponseMessage + ' - ' + resp.response.StatusMessage,
 						TotalAmount: resp.response.Payment.TotalAmount,
 						InvoiceNumber: resp.response.Payment.InvoiceNumber,
 						InvoiceReference: resp.response.Payment.InvoiceReference,
 						CurrencyCode: resp.response.Payment.CurrencyCode,
 					};

 					if( typeof resp.response.TransactionStatus != 'undefined' && resp.response.TransactionStatus == true ) {
 						donate.statusCode = 1;
 						donate.statusText = 'APPROVED';
 					} else {
 						donate.statusCode = 0;
 						donate.statusText = 'FAILED';
 					}

 					donate.PaymentTransaction = {
 						TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
 						TxnId : resp.response.TransactionID,
 						TxnDate : self.formatDate( paymentDate ),
 						TxnMessage : resp.response.StatusMessage + '-' + donate.pamynet_ref,
 						TxnPaymentRef : donate.pamynet_ref
 					};

 					donate.card_details = self.clean( cc );

 					resolve( {
 						status : 'success',
 						response : resp.response,
 						donation : donate
 					} );

 				} else {
 					if( resp.errors != undefined ){
	 					resolve( {
	 						status : 'error',
	 						response : resp.errors,
	 						donation : param.donation
	 					} );
 					}else{
 						var resp_extra = resp;
 						resp_extra = resp_extra.replace('<h2>','');
 						resp_extra = resp_extra.replace('</h2>','');
 						resp_extra = resp_extra.replace('<pre>','');
 						resolve( {
	 						status : 'error',
	 						response : resp_extra,
	 						donation : param.donation
	 					} );
 					}
 				}
			},
			error: function( resp ) {
				reject( resp );
			}
		});

 	});
};

EwayGateway.prototype.tokenizePayment = function( param, cc ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_eway_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				process: 'token',
				cc: self.encrpt( param, cc )
			},
			success: function( resp ) {
				if( resp.status == 'success' && resp.status != undefined) {
					var donate = param.donation;
					var paymentDate = new Date( donate.timestamp * 1000 );

					donate.payment_response = {
						TokenCustomerID: resp.response.Customer.TokenCustomerID,
						TransactionID: resp.response.TransactionID,
						TransactionStatus: resp.response.TransactionStatus,
						TransactionType: resp.response.TransactionType,
						AuthorisationCode: resp.response.AuthorisationCode,
						ResponseCode: resp.response.ResponseCode,
						ResponseMessage: resp.response.ResponseMessage + ' - ' + resp.response.StatusMessage,
						TotalAmount: resp.response.Payment.TotalAmount,
						InvoiceNumber: resp.response.Payment.InvoiceNumber,
						InvoiceReference: resp.response.Payment.InvoiceReference,
						CurrencyCode: resp.response.Payment.CurrencyCode,
					};

					if( typeof resp.response.TransactionStatus != 'undefined' && resp.response.TransactionStatus == true ) {
						donate.statusCode = 1;
						donate.statusText = 'APPROVED';
					} else {
						donate.statusCode = 0;
						donate.statusText = 'FAILED';
					}

					donate.PaymentTransaction = {
						TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
						TxnId : resp.response.TransactionID,
						TxnDate : self.formatDate( paymentDate ),
						TxnMessage : resp.response.StatusMessage + '-' + donate.pamynet_ref,
						TxnPaymentRef : donate.pamynet_ref
					};

					donate.TokenCustomerID = ( typeof resp.response.Customer.TokenCustomerID != 'undefiend' ) ? resp.response.Customer.TokenCustomerID : null;
					donate.card_details = self.clean( cc );

					resolve( {
						status : 'success',
						response : resp.response,
						donation : donate
					} );
				} else {
 					if( resp.errors != undefined ){
	 					resolve( {
	 						status : 'error',
	 						response : resp.errors,
	 						donation : param.donation
	 					} );
 					}else{
 						var resp_extra = resp;
 						resp_extra = resp_extra.replace('<h2>','');
 						resp_extra = resp_extra.replace('</h2>','');
 						resp_extra = resp_extra.replace('<pre>','');
 						resolve( {
	 						status : 'error',
	 						response : resp_extra,
	 						donation : param.donation
	 					} );
 					}
				}
			},
			error: function( resp ) {
				reject( resp );
			}
		});
 	});
};

EwayGateway.prototype.processPayment = function( param, cc ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
		if( param.donation.wfc_donation_tokenize == 'true' ) {
			console.log('Eway Tokenize Payment');
			self.tokenizePayment( param, cc )
			.then(function(result){
				resolve( result );
			})
			.catch(function(error){
				reject(error);
			});
		} else {
			console.log('Eway Non-Tokenize Payment');
			self.creditCardPayment( param, cc )
			.then(function(result){
				resolve( result );
			})
			.catch(function(error){
				reject( error );
			});
		}
 	});
};