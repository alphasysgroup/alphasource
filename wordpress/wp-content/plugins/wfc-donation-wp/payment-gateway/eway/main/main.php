<?php

	/**
	* Eway Wordpress Hooks
	*/
	class Eway_WP_Hooks
	{
		/**
		 * The Eway main class
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $parent    The Eway main class
		 */
		public $parent = null;

		/**
		 * The main class of the plugin
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $includes    The main class of the plugin
		 */
		public $includes = null;
		
		/**
		 * Eway_WP_Hooks constructor.
		 *
		 * @param $a
		 * @param $b
		 */
		function __construct( $a, $b )
		{

			/*
			 * The Eway main class
			 */
			$this->parent = $a;

			/*
			 * The main class of the plugin
			 */
			$this->includes = $b;

			/*
			 * Eway scripts
			 */
			add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_ewayscripts' ) );

			/*
			 * Eway payment ajax endpoint
			 */
			add_action( 'wp_ajax_wfc_eway_payment', array( $this, 'wfc_EwayCreditCardProcess' ) );
			add_action( 'wp_ajax_nopriv_wfc_eway_payment', array( $this, 'wfc_EwayCreditCardProcess' ) );

		}
		
		/**
		 * Loads script for EWay
		 *
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 * @return void
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 3, 2018
		 */
		public function wfc_load_ewayscripts() {
			global $post;

			/*
			* Eway scripts
			*/
			wp_register_script( 
				'webforce-connect-donation-eway_encrypt_js',
				'https://secure.ewaypayments.com/scripts/eCrypt.min.js',
				array( 'jquery' ), 
				'',
				true
			);

			/*
			* wfc dontation eway js gateway
			*/
			wp_register_script(
				'webforce-connect-donation-eway_js',
				plugin_dir_url( __FILE__ ) . '../js/eway.js',
				array(),
				'',
				true
			);

		}
		
		/**
		 * Ajax-callback Credit Card encryption process for EWay
		 * LastUpdated : May 3, 2018
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 * @return void array
		 * @since    5.0.0
		 */
		public function wfc_EwayCreditCardProcess() {
			if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_eway_payment' ) {
				
				/*
				 * prevent processing on direct access endpoint
				 */
				if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
					die('busted');
				}

				/*
				 * load eway rapid api library
				 */
				require_once( plugin_dir_path( __FILE__ ) . '../lib/RapidAPI.php' );
				
				/*
				 * grab donation details
				 */
				$donate = $_POST['param']['donation'];

				if( $_POST['process'] == 'creditcard' ) {

					/*
					 * grab direct customer credit card charge
					 */
					$result = $this->parent->wfc_EwayChargeCustomer( $donate, $this->includes, $_POST['cc'] );

				} else {

					/*
					 * token payment for eway
					 */
					$result = $this->parent->wfc_TokenCustomer( $donate, $this->includes, $_POST['cc'] );
				}

				if( is_array( $result ) ) {

					/*
					 * return to form and display error
					 */
					wp_send_json( array(
						'status' => 'error',
						'errors' => $result
					) );

				} else {

					/*
					 * success response
					 */
					wp_send_json( array(
						'status' => 'success',
						'response' => $result
					) );
				}
			}
		}
	}

