<?php

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

if (!class_exists('Eway')) {
	
	/**
	 * Class Eway
	 */
	class Eway
    {

        public $restricted = array(
            'wfc_donation_ewayapikey',
            'wfc_donation_ewayapipassword',
        );

        /**
         * The registered scripts id
         *
         * @since    5.0.0
         * @access   public
         * @var      string    $registerd_scripts    The main class of the plugin
         */
        public $registerd_scripts = array(
            'webforce-connect-donation-eway_encrypt_js',
            'webforce-connect-donation-eway_js'
        );

        public function __construct()
        {

        }
		
		/**
		 * Load EWay WordPress core hooks
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param $class , plugin main file
		 *
		 * @return void
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 3, 2018
		 */
        public function loadWpCore($class){
            require_once plugin_dir_path(__FILE__) . 'main/main.php';
            new Eway_WP_Hooks($this, $class);
        }
		
		/**
		 * Processes Direct customer credit card payment or create customer.
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param array  $donate donation form post
		 * @param string $class  plugin main class
		 * @param array  $cc     creditcard payment
		 * @param bool   $direct or tokenize
		 *
		 * @return json array
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 3, 2018
		 */
        public function wfc_EwayChargeCustomer($donate, $class, $cc, $direct = true){

            /*
             * get gateway
             */
            $gateway = get_option($donate['gatewayid']);


            /*
             * customer personal details
             */
            $request = array(
                'Customer' => array(
                    'Reference'   => (string) $donate['pamynet_ref'],
                    'CompanyName' => (isset($donate['donor_company']) && !empty($donate['donor_company'])) ? $donate['donor_company'] : '',
                    'Email'       => (isset($donate['donor_email']) && !empty($donate['donor_email'])) ? $donate['donor_email'] : '',
                    'Title'       => (isset($donate['donor_title']) && !empty($donate['donor_title'])) ? $donate['donor_title'] : '',
                    'FirstName'   => (isset($donate['donor_first_name']) && !empty($donate['donor_first_name'])) ? $donate['donor_first_name'] : '',
                    'LastName'    => (isset($donate['donor_last_name']) && !empty($donate['donor_last_name'])) ? $donate['donor_last_name'] : '',
                    'Phone'       => (isset($donate['donor_phone']) && !empty($donate['donor_phone'])) ? $donate['donor_phone'] : '',
                    'Street1'     => (isset($donate['donor_address']) && !empty($donate['donor_address'])) ? $donate['donor_address'] : '',
                    'Country'     => (isset($donate['donor_country']) && !empty($donate['donor_country'])) ? WFC_DonationDataHandler::wfc_country_code($donate['donor_country']) : '',
                    'City'        => (isset($donate['donor_suburb']) && !empty($donate['donor_suburb'])) ? $donate['donor_suburb'] : '',
                    'State'       => (isset($donate['donor_state']) && !empty($donate['donor_state'])) ? $donate['donor_state'] : '',
                    'PostalCode'  => (isset($donate['donor_postcode']) && !empty($donate['donor_postcode'])) ? $donate['donor_postcode'] : '',
                    'CardDetails' => array(
                        'Name'        => (isset($cc['noc']) && !empty($cc['noc'])) ? $cc['noc'] : '',
                        'Number'      => (isset($cc['cn']) && !empty($cc['cn'])) ? $cc['cn'] : '',
                        'ExpiryMonth' => (isset($cc['em']) && !empty($cc['em'])) ? $cc['em'] : '',
                        'ExpiryYear'  => (isset($cc['ey']) && !empty($cc['ey'])) ? $cc['ey'] : '',
                        'CVN'         => (isset($cc['cvn']) && !empty($cc['cvn'])) ? $cc['cvn'] : '',
                    ),
                ),
            );

            if ($direct === true) {

                /*
                 * direct credit card payment charge
                 */
                $request['Method'] = 'ProcessPayment';
                $currencycode      = explode(',', $donate['currency']);
                $request['Payment'] = array(
                    'CurrencyCode' => $currencycode[1],
                    'InvoiceReference' => (string) $donate['pamynet_ref'],
                    'TotalAmount' => str_replace(
	                    '.',
	                    '',
	                    number_format(
	                        $donate['pdwp_sub_amt'],
	                        2,
	                        ".",
	                        ""
	                    )
	                )
                );
                
            } else {

                /*
                 * create eway token customer
                 */
                $request['Method'] = 'CreateTokenCustomer';
            }

            /*
             * eway TransactionType important
             */  
            $request['TransactionType'] = 'Purchase';

            $eway_params = array();
            if ($gateway['wfc_donation_sandboxmode'] == 'on') {
                $eway_params['sandbox'] = true;
            }

            $service = new eWAY\RapidAPI(
                $gateway['wfc_donation_ewayapikey'],
                $gateway['wfc_donation_ewayapipassword'],
                $eway_params
            );

            //$eway_request = $class->pdwp_array_to_object( $request );
            $result = $service->DirectPayment($request);

            /*
             * Check if any error returns
             */
            if (isset($result->Errors) && !empty($result->Errors)) {

                /*
                 * Get Error Messages from Error Code.
                 */
                $ErrorArray = explode(",", $result->Errors);
                foreach ($ErrorArray as $error) {
                    $errors[] = $service->getMessage($error);
                }
                return $errors;

            } else {

                /*
                 * return success transaction response for eway
                 */
                $ResponseMessage       = isset($result->ResponseMessage) ? $result->ResponseMessage : '';
                $result->StatusMessage = $service->getMessage($ResponseMessage);
                return $result;
            }
        }

        /**
         * Processes tokenized direct customer credit card payment or create customer.
         *
         * @author Junjie Canonio <junjie@alphasys.com.au>
         * @param array $donate donation form post
         * @param string $class plugin main class
         * @param array $cc creditcard payment
         * @return json array
         * @since    5.0.0
         *
         * @LastUpdated  May 3, 2018
         */
        public function wfc_TokenCustomer($donate, $class, $cc)
        {

            /*
             * create eway customer
             */
            $result = $this->wfc_EwayChargeCustomer($donate, $class, $cc, false);

            if (is_array($result)) {

                /*
                 * return to form if error occured
                 */
                return $result;

            } else {

                /*
                 * get gateway
                 */
                $gateway = get_option($donate['gatewayid']);

                /*
                 * get created token custom id
                 */
                $TokenCustomerID = (isset($result->Customer->TokenCustomerID)
                    && !empty($result->Customer->TokenCustomerID)) ? $result->Customer->TokenCustomerID : '';

                /*
                 * get country code
                 */
                $currencycode = explode(',', $donate['currency']);

                $request = array(
                    'Customer'        => array(
                        'TokenCustomerID' => $TokenCustomerID,
                    ),
                    'Payment'         => array(
                        'CurrencyCode'     => $currencycode[1],
                        'TotalAmount'      => str_replace(
                            '.',
                            '',
                            number_format(
                                $donate['pdwp_sub_amt'],
                                2,
                                ".",
                                ""
                            )
                        ),
                        'InvoiceReference' => (string) $donate['pamynet_ref'],
                    ),
                    'Method'          => 'TokenPayment',
                    'TransactionType' => 'Recurring',
                );

                $eway_request = WFC_DonationDataHandler::wfc_array_to_object($request);
                $eway_params  = array();
                if ($gateway['wfc_donation_sandboxmode'] == 'on') {
                    $eway_params['sandbox'] = true;
                }

                $service = new eWAY\RapidAPI(
                    $gateway['wfc_donation_ewayapikey'],
                    $gateway['wfc_donation_ewayapipassword'],
                    $eway_params
                );

                $result = $service->DirectPayment($eway_request);

                /*
                 * Check if any error returns
                 */
                if (isset($result->Errors) && !empty($result->Errors)) {

                    /*
                     * Get Error Messages from Error Code.
                     */
                    $ErrorArray = explode(",", $result->Errors);
                    foreach ($ErrorArray as $error) {
                        $errors[] = $service->getMessage($error);
                    }
                    return $errors;

                } else {

                    /*
                     * return success transaction response for eway
                     */
                    $ResponseMessage       = isset($result->ResponseMessage) ? $result->ResponseMessage : '';
                    $result->StatusMessage = $service->getMessage($ResponseMessage);
                    return $result;
                }
            }
        }
		
		/**
		 * Get gateway transaction error/s.
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 *
		 * @param array $donate donation form post
		 *
		 * @return array array
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 3, 2018
		 */
        public function getGatewayTransactionError($donate)
        {

            $code = isset($donate['payment_response']['ResponseCode']) ?
            $donate['payment_response']['ResponseCode'] : null;

            $message = isset($donate['payment_response']['ResponseMessage']) ?
            $donate['payment_response']['ResponseMessage'] : null;

            return array(
                'responseCode'    => $code,
                'responseMessage' => $message,
            );
        }
    }
}
