<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'Ech' ) ) {
		
		/**
		 * Class Ech
		 */
		class Ech
		{
			
			/**
			 * @var array $restricted
			 */
			public $restricted = array(
				'wfc_donation_paydocksecretkey',
			);

			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation_paydock_token_js',
				'webforce-connect-donation_paydock_js'
			);
			
			/**
			 * Ech constructor.
			 */
			function __construct()
			{

			}
			
			/**
			 * Name : loadSettingsTemplate
			 * Description: load paydock settings template
			 * LastUpdated : May 30, 2018
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Name : loadWpCore
			 * Description: load stripe wp core hooks
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $class , plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 30, 2018
			 */
			public function loadWpCore( $class ) {
				require_once plugin_dir_path(__FILE__) . 'main/main.php';
				new Ech_WP_Hooks( $this, $class );
			}
			
			/**
			 * Paydock process for tokenize payment.
			 *
			 * @author   Junjie Canonio
			 *
			 * @param      $post     ,donation data array
			 * @param      $gateway , gateway object
			 * @param      $token   , one-time token created by stripe.js
			 * @param bool $customer
			 *
			 * @return array|mixed|object $res if success object
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 30, 2018
			 */
			public function wfc_TokenPayment( $post, $gateway, $token, $customer = false ) {

				/*
				 * paydock mode if sandbox/live
				 */
				$mode = (isset($gateway['wfc_donation_sandboxmode']) && $gateway['wfc_donation_sandboxmode'] == 'true') ? '-sandbox.' : '.';

				/*
				 * paydock endpoint
				 */
				$endpoint = "https://api{$mode}paydock.com/v1/charges";

				/*
				 * get paydock api key
				 */
				$apiKey = (isset($gateway['wfc_donation_paydocksecretkey'] ) && 
							 	!empty($gateway['wfc_donation_paydocksecretkey'])) ?
									$gateway['wfc_donation_paydocksecretkey'] : null;

				/*
				 * donation form currency
				 */
				$currency = explode( ',', $post['currency'] );

				/*
				 * construct data for charge
				 */
				$request_data = array(
					'amount' => number_format( $post['pdwp_sub_amt'], 2, ".", "" ),
					'currency' => $currency[1],
					'reference' => $post['pamynet_ref'],
					'description' => "Charge for {$post['donor_email']}",
					'first_name' => $post['donor_first_name'],
					'last_name' => $post['donor_last_name'],
					'email' => $post['donor_email']
				);

				if( $customer ) {

					/*
					 * charge customer id
					 */
					$request_data['customer_id'] = $token;

				} else {

					/*
					 * charge one time token
					 */
					$request_data['token'] = $token;
				}

				/*
				 * json format for request
				 */
				$data = json_encode( $request_data );

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_URL, $endpoint);
				curl_setopt($ch, CURLOPT_ENCODING, "gzip");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				if( $gateway['wfc_donation_paydockverifyssl'] == 'true' ){
					$paydock_ssl_on = 2;
				} else {
					$paydock_ssl_on = 0;
				}

				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $paydock_ssl_on );
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $paydock_ssl_on );

				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					"Content-Type: application/json",
					"Content-Length: ". strlen($data),
					"x-user-token:". $apiKey,
				));

				$res = json_decode(curl_exec($ch));

				if( isset( $res->status ) && $res->status == 201 ) {
					return $res;
				} else {
					if( isset( $res->error ) && !empty( $res->error ) ) {
						$res_error_details = isset( $res->error->details ) ? $res->error->details : '';
						if( is_array( $res_error_details ) || is_object( $res_error_details ) ) {
							foreach ( $res_error_details as $error ) {
								if( is_object( $error ) ) {
									foreach ( $error as $r ) {
										$_r_trim = trim( $r );
										if( !empty( $_r_trim ) ) {
											$errors[] = $r;
										}
									}
								} else {
									$errors[] = $error;
								}
							}
						} else {
							$errors[] = isset( $res->error->message ) ? $res->error->message : '';
						}

					} else {
						$errors[] = "Something went wrong, Please try again or contact your administrator.";
					}

					return array(
						'status' => 'error',
						'errors' => $errors
					);
				}
			}
			
			/**
			 * Create token customer for Paydock.
			 *
			 * @author   Junjie Canonio
			 *
			 * @param $post    , donation data array
			 * @param $gateway , gateway object
			 * @param $token   , one-time token create by stripe.js
			 *
			 * @return array|mixed|object $res if success object
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 30, 2018
			 */
			public function wfc_CreateTokenCustomer( $post, $gateway, $token ) {

				/*
				 * paydock mode if sandbox/live
				 */
				$mode = (isset($gateway['wfc_donation_sandboxmode']) && 
						 	$gateway['wfc_donation_sandboxmode'] == 'true') ? '-sandbox.' : '.';

				/*
				 * paydock endpoint
				 */
				$endpoint = "https://api{$mode}paydock.com/v1/customers";

				/*
				 * get paydock api key
				 */
				$apiKey = ( isset( $gateway['wfc_donation_paydocksecretkey']) && 
							 	!empty( $gateway['wfc_donation_paydocksecretkey']) ) ?
									$gateway['wfc_donation_paydocksecretkey'] : null;

				/*
				 * donation form currency
				 */
				$currency = explode( ',', $post['currency'] );

				/*
				 * construct data for charge
				 */
				$data = json_encode( array(
					'token' => $token,
					'first_name' => $post['donor_first_name'],
					'last_name' => $post['donor_last_name'],
					'email' => $post['donor_email']
				) );

				$ch = curl_init();
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_URL, $endpoint);
				curl_setopt($ch, CURLOPT_ENCODING, "gzip");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				if( $gateway['wfc_donation_paydockverifyssl'] == 'true' ){
					$paydock_ssl_on = 2;
				} else {
					$paydock_ssl_on = 0;
				}

				curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, $paydock_ssl_on );
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, $paydock_ssl_on );

				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					"Content-Type: application/json",
					"Content-Length: ". strlen($data),
					"x-user-token:". $apiKey,
				));

				$res = json_decode(curl_exec($ch));

				if( isset( $res->status ) && $res->status == 201 ) {
					return $res;
				} else {
					if( isset( $res->error ) && !empty( $res->error ) ) {
						$res_error_details = isset( $res->error->details ) ? $res->error->details : '';
						if( is_array( $res_error_details ) || is_object( $res_error_details ) ) {
							foreach ( $res_error_details as $error ) {
								if( is_object( $error ) ) {
									foreach ( $error as $r ) {
										$_r_trim = trim( $r );
										if( !empty( $_r_trim ) ) {
											$errors[] = $r;
										}
									}
								} else {
									$errors[] = $error;
								}
							}
						} else {
							$errors[] = isset( $res->error->message ) ? $res->error->message : '';
						}

					} else {
						$errors[] = "Something went wrong, Please try again or contact your administrator.";
					}

					return array(
						'status' => 'error',
						'errors' => $errors
					);
				}
			}
		}
	}