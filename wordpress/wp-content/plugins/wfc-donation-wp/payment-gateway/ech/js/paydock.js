/**
* jQuery
*/
var j = jQuery;

var PaydockGateway = function( config ) {
	this.config = config;
	j('#pdwp-btn').show();
	
	j('#wfc_donation-payment-details-toogle-container').show();
	j("label[for=payment_type_credit_card]").click();
	
};

PaydockGateway.prototype.clean = function( cc ) {

	var creditcarddetails = {
		card_number: cc.card_number,
		name_on_card: cc.name_on_card,
		expiry_month: cc.expiry_month,
		expiry_year: cc.expiry_year,
		ccv: cc.ccv
	};

	var regex = /\d(?=\d{4})/mg;
	var str = creditcarddetails.card_number.replace(/ /g,'');
	var subst = '*';

	creditcarddetails.card_number = str.replace(regex, subst);
	creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

	return creditcarddetails;
};

PaydockGateway.prototype.cleanBank = function( bank ) {
  var bank_details = {
      bank_code: ( typeof bank.bank_code != 'undefined' ? bank.bank_code : '' ),
      bank_number: ( typeof bank.bank_number != 'undefined' ? bank.bank_number : '' ),
      bank_name: ( typeof bank.bank_name != 'undefined' ? bank.bank_name : '' )
  };

  var regex = /\d(?=\d{4})/mg;
    var strbank = bank_details.bank_number.replace(/ /g,'');
    var strcode = bank_details.bank_code.replace(/ /g,'');
    var subst = 'x';

    bank_details.bank_code = strcode.replace(regex, subst);
    bank_details.bank_number = strbank.replace(regex, subst);
    return bank_details;
};

PaydockGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

PaydockGateway.prototype.oneTimeToken = function( param, cc ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {

		Paydock.setPublicKey(self.config.settings.wfc_donation_paydockpublickey);
		if( self.config.settings.wfc_donation_sandboxmode == 'true' ) {
			Paydock.setSandbox( true );
		}

		var formData = {
			first_name : param.donation.donor_first_name,
			last_name: param.donation.donor_last_name,
			email: param.donation.donor_email,
			gateway_id: self.config.settings.wfc_donation_paydockgatewayid,
			card_name: cc.name_on_card,
			card_number: cc.card_number.replace(/ /g,''),
			expire_month: cc.expiry_month,
			expire_year: cc.expiry_year,
			card_ccv: cc.ccv.replace(/ /g,'')
		};

		Paydock.createToken(
			formData,
			function (token, status) {
				if( status == 201 ) {
					resolve({
						status : 'success',
						token : token,
					});
				} else {
					resolve( {
						status : 'error',
						response : token,
						donation : param.donation
					} );
				}
			},
			function (res, status) {
				if( status != 201 ) {
					if( typeof res.details == 'array' || typeof res.details == 'object' ) {
						var err = [];
						j.each( res.details, function(k, v){
							if( typeof v == 'object' ) {
								j.each( v, function(k, v){
									var e = j.trim( v );
									if( e ) {
										err.push( e );
									}
								});
							} else {
								err.push( v );
							}
						});
						var error = err;
					} else {
 						var error = (typeof res.message != 'undefined' ) ? res.message : '';
					}
					resolve( {
						status : 'error',
						response : error,
						donation : param.donation
					} );
				}
			}
		);
	});
};

PaydockGateway.prototype.paydockPayment = function( param, cc, token, type ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_paydock_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				token: token,
				paymenttype: type
			},
			success: function( resp ) {

				if( resp.status == 'success' ) {
				 	
					var donate = param.donation;
 					var paymentDate = new Date( donate.timestamp * 1000 );

 					donate.payment_response = resp.response;

 					var response_status = ( typeof resp.response.resource.data.status != 'undefined' ) ? resp.response.resource.data.status : '';

 					if( j.inArray( response_status, ['complete', 'success'] ) != -1 ) {
 						donate.statusCode = 1;
 						donate.statusText = 'APPROVED';
 					} else {
 						donate.statusCode = 0;
 						donate.statusText = 'FAILED';
 					}

 					donate.PaymentTransaction = {
 						TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
 						TxnId : resp.response.resource.data._id,
 						TxnDate : self.formatDate( paymentDate ),
 						TxnMessage : response_status + '-' + donate.pamynet_ref,
 						TxnPaymentRef : donate.pamynet_ref
 					};

 					donate.TokenCustomerID = resp.response.resource.data.user_id;
 					donate.card_details = self.clean( cc );

 					resolve( {
 						status : 'success',
 						response : resp.response,
 						donation : donate
 					} );

				} else {
					resolve( {
						status : 'error',
						response : resp.errors,
						donation : param.donation
					} );
				}
			},
			error: function( resp ) {
				reject( resp );
			}
		});
 	});
};

PaydockGateway.prototype.processPayment = function( param, cc ) {
	var self = this;
 	return new Promise( function( resolve, reject ) {
 		if( typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank' ) {

	      var donate = param.donation;
	      var paymentDate = new Date( donate.timestamp * 1000 );

	      donate.payment_response = {};
	      donate.statusCode = 1;
	      donate.statusText = 'PENDING';

	      donate.PaymentTransaction = {
	        TxnStatus : 'Success',
	        TxnId : '',
	        TxnDate : self.formatDate( paymentDate ),
	        TxnMessage : '',
	        TxnPaymentRef : donate.pamynet_ref
	      };

	      donate.card_details = self.cleanBank( cc );

	      resolve( {
	        status : 'success',
	        response : {},
	        donation : donate
	      } );

	    } else {  
	 		self.oneTimeToken( param, cc )
	 		.then(function(result){
	 			if( result.status == 'error' ) {
	 				resolve( result );
	 			} else {
	 				if( self.config.settings.wfc_donation_tokenize == 'true' ) {
	 					console.log('Paydock Tokenize Payment');
	 					return self.paydockPayment( param, cc, result.token, 'tokenpayent' );
	 				} else {
	 					console.log('Paydock Non-Tokenize Payment');
	 					return self.paydockPayment( param, cc, result.token, 'cardpayent' );
	 				}
	 			}
	 		})
	 		.then( function( result ) {
	 			resolve( result );
	 		})
	 		.catch(function(error){
	 			reject( error );
	 		});
	 	}
 	});
};