<?php 

/**
* Paydock Wordpress Hooks
*/
class Ech_WP_Hooks
{
	/**
	 * The paypal main class
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $parent    The paypal main class
	 */
	public $parent = null;

	/**
	 * The main class of the plugin
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $includes    The main class of the plugin
	 */
	public $includes = null;
	
	/**
	 * Ech_WP_Hooks constructor.
	 *
	 * @param $a
	 * @param $b
	 */
	function __construct( $a, $b )
	{

		/*
		 * The paypal main class
		 */
		$this->parent = $a;

		/*
		 * The main class of the plugin
		 */
		$this->includes = $b;

		/*
		 * paypal scripts
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_paydockscripts' ) );

		/*
		 * paydock payment ajax endpoint
		 */
		add_action( 'wp_ajax_wfc_paydock_payment', array( $this, 'wfc_paydock_payment' ) );
		add_action( 'wp_ajax_nopriv_wfc_paydock_payment', array( $this, 'wfc_paydock_payment' ) );

	}
	
	/**
	 * Loads script for paydock.
	 *
	 *
	 * @author   Junjie Canonio
	 * @return void
	 * @since    5.0.0
	 *
	 * @LastUpdated  May 30, 2018
	 */
	public function wfc_load_paydockscripts() {
		global $post;

		/*
		 * paydock js one time token
		 */
		wp_register_script( 
			'webforce-connect-donation-paydock_token_js',
			'https://app.paydock.com/v1/paydock.min.js',
			array( 'jquery' ), 
			'',
			true
		);

		/*
		 * pdwp paydock js
		 */
		wp_register_script(
			'webforce-connect-donation-paydock_js',
			plugin_dir_url( __FILE__ ) . '../js/paydock.js',
			array(),
			'',
			true
		);

		// if( ( isset( $post->post_type ) && $post->post_type == 'pdwp_donation' ) || 
		// 		( isset( $post->post_content ) && has_shortcode( $post->post_content, 'pronto-donation-form' ) ) ) {
			
		// 	/**
		// 	 * pdwp paydock js
		// 	*/
 	// 		wp_enqueue_script(PRONTO_DONATION_SLUG . 'paydock_onetime_token');
		// 	wp_enqueue_script(PRONTO_DONATION_SLUG . 'pdwp_paydock_gateway');
 	// 	}
	}
	
	/**
	 * Paydock ajax callback that processes payment.
	 *
	 * @author   Junjie Canonio
	 * @return void
	 * @since    5.0.0
	 *
	 * @LastUpdated  May 30, 2018
	 */
 	public function wfc_paydock_payment() {
 		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_paydock_payment' ) {

 			/*
			 * break if direct call enpoint
			 */
 			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
			 * donation data
			 */
			$donate = $_POST['param']['donation'];

			/*
			 * paydock gateway
			 */
			$gateway = get_option($donate['gatewayid']);

			/*
			 * paydock gateway
			 */
			if( $_POST['paymenttype']  == 'cardpayent' ) {

				/*
				 * process paydock direct payment
				 */
				$payment = $this->parent->wfc_TokenPayment( $donate, $gateway, $_POST['token'], false );

				if( is_array( $payment ) ) {
				
					/*
					 * return to form if error occured
					 */
					wp_send_json( $payment );

				} else {

					/*
					 * success transaction
					 */
					wp_send_json( array(
						'status' => 'success',
						'response' => $payment
					) );
				}

			} else {

				/*
				 * create token customer
				 */
				$customer = $this->parent->wfc_CreateTokenCustomer( $donate, $gateway, $_POST['token'] );

				if( is_array( $customer ) ) {

					/*
					 * return to form if error occured
					 */
					wp_send_json( $customer );

				} else {

					/*
					 * process paydock direct payment
					 */
					$payment = $this->parent->wfc_TokenPayment( $donate, $gateway, $customer->resource->data->_id, true );

					if( is_array( $payment ) ) {
					
						/*
						 * return to form if error occured
						 */
						wp_send_json( $payment );

					} else {
						
						/*
						 * success transaction
						 */
						wp_send_json( array(
							'status' => 'success',
							'response' => $payment
						) );
					}
				}
			}
		}
 	}
}