/**
* jQuery
*/
var j = jQuery;

var StripeGateway = function( config ) {
	this.config = config;
	j('#pdwp-btn').show();

	j('#wfc_donation-payment-details-toogle-container').after('<div id="stripe-card-details" class="col-12 col-sm-12 col-md-12 col-lg-12"><div id="stripe-credit-card-wrapper" class="wfc-donation-credit-card-wrapper row"></div></div>').hide();
	this.instance = this.ccmount( this.config );
};

StripeGateway.prototype.ccmount = function( config ) {
	var stripe = Stripe( config.settings.wfc_donation_stripepublickey );
	var elements = stripe.elements();

	var str_cardNumber = '<div id="card-number-wrapper" class="col-12 col-sm-12 col-md-12 col-lg-12">';
	str_cardNumber += '<label class="wfc-don-field-label" for="card_number">Card Number<span style="color:red;"> *</span></label>';
	str_cardNumber += '<div id="stripe-card-number" style="background-color: #f1f1f1;border-radius: 2px;border: 1px solid #f1f1f1;box-shadow: none;color: #999 !important;padding: 0.7em;"></div>';
	str_cardNumber += '</div>';

	j('#stripe-credit-card-wrapper').append( str_cardNumber );
	var cardNumber = elements.create( 'cardNumber', {
		style: {
			base: {
				color: '#666',
				lineHeight: '20px',
				fontWeight: 300,
				fontFamily: '"Libre Franklin", "Helvetica Neue", helvetica, arial, sans-serif',
				fontSize: '14px'
			}
		},
		placeholder : ''
	});
	cardNumber.mount('#stripe-card-number');

	var str_cardExpiry = '<div id="stripe-card-expiry-wrapper" class="col-12 col-sm-12 col-md-6 col-lg-6">';
	str_cardExpiry += '<label class="wfc-don-field-label">Card Expiry<span style="color:red;"> *</span></label>';
	str_cardExpiry += '<div id="stripe-card-expiry" style="background-color: #f1f1f1;border-radius: 2px;border: 1px solid #f1f1f1;box-shadow: none;color: #999 !important;padding: 0.7em;"></div>';
	str_cardExpiry += '</div>';

	j('#stripe-credit-card-wrapper').append( str_cardExpiry );
	var cardExpiry = elements.create( 'cardExpiry', {
		style: {
			base: {
				color: '#666',
				lineHeight: '20px',
				fontWeight: 300,
				fontFamily: '"Libre Franklin", "Helvetica Neue", helvetica, arial, sans-serif',
				fontSize: '14px'
			}
		},
		placeholder : ''
	});
	cardExpiry.mount('#stripe-card-expiry');

	var str_cardCvc = '<div id="CVC-wrapper" class="col-12 col-sm-12 col-md-6 col-lg-6">';
	str_cardCvc += '<label class="wfc-don-field-label">CVC<span style="color:red;"> *</span></label>';
	str_cardCvc += '<div id="stripe-card-cvc" style="background-color: #f1f1f1;border-radius: 2px;border: 1px solid #f1f1f1;box-shadow: none;color: #999 !important;padding: 0.7em;"></div>';
	str_cardCvc += '</div>';

	j('#stripe-credit-card-wrapper').append( str_cardCvc );
	var cardCvc = elements.create( 'cardCvc', {
		style: {
			base: {
				color: '#666',
				lineHeight: '20px',
				fontWeight: 300,
				fontFamily: '"Libre Franklin", "Helvetica Neue", helvetica, arial, sans-serif',
				fontSize: '14px'
			}
		},
		placeholder : ''
	});
	cardCvc.mount('#stripe-card-cvc');

	return {
		stripe : stripe,
		elements: elements,
		card : cardNumber
	};
};

StripeGateway.prototype.toTitleCase = function(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

StripeGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

StripeGateway.prototype.generateToken = function( param ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {
		var donate = param.donation;
		self.instance.stripe.createToken( self.instance.card, {
			address_line1 : donate.donor_address,
			address_city : donate.donor_suburb,
			address_state : donate.donor_state,
			address_zip : donate.donor_postcode,
			address_country : donate.donor_country,
		}).then(function( result ){
			if( typeof result.error == 'undefined' ) {
				resolve({
					status : 'success',
					token : result.token.id,
				});
			} else {
				resolve( {
					status : 'error',
					response : result.error,
					donation : donate
				} );
			}
		});
	});
};

StripeGateway.prototype.tokenizePayment = function( param, token ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_stripe_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				token: token
			},
			success: function( resp ) {
 				resolve( resp );
			},
			error: function( resp ) {
				reject( resp );
			}
		});
	});
};

StripeGateway.prototype.processPayment = function( param, cc ) {
	var self = this;
 	return new Promise( function( resolve, reject ) {
 		self.generateToken( param )
 		.then(function( result ) {
 			if( result.status == 'error' ) {
 				resolve( result );
 			} else {
 				return self.tokenizePayment( param, result.token );
 			}
 		})
 		.then(function( result ) {
 			if( result.status == 'success' ) {

 				var donate = param.donation;
 				var paymentDate = new Date( donate.timestamp * 1000 );

 				donate.payment_response = result.response;

 				if( result.response.status == 'succeeded' ) {

 					donate.statusCode = 1;
 					donate.statusText = 'APPROVED';

 				} else if( result.response.status == 'pending' ) {

 					donate.statusCode = 1;
 					donate.statusText = 'PENDING';

 				} else if( result.response.status == 'failed' ) {

 					donate.statusCode = 0;
 					donate.statusText = 'FAILED';

 				}

 				donate.PaymentTransaction = {
					TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
					TxnId : result.response.id,
					TxnDate : self.formatDate( paymentDate ),
					TxnMessage : result.response.status + '-' + donate.pamynet_ref,
					TxnPaymentRef : donate.pamynet_ref
				};

				donate.TokenCustomerID = result.response.customer;
				donate.card_details = {};

				resolve( {
					status : 'success',
					response : result.response,
					donation : donate
				} );
			
 			} else {
 				resolve( {	
					status : 'error',
					response : result.errors,
					donation : param.donation
				} );
 			}
 		})
 		.catch(function( error ){
 			reject( error );
 		});
 	});
};