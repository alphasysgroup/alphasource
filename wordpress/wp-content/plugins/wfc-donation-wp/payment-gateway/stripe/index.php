<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'Stripe' ) ) {
		
		/**
		 * Class Stripe
		 *
		 * The data handler for Stripe Payment Gateway.
		 */
		class Stripe
		{

			public $restricted = array(
				'wfc_donation_stripesecretkey'
			);
			
			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var array
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-stripe_library_js',
				'webforce-connect-donation-stripe_js'
			);
			
			/**
			 * Stripe constructor.
			 */
			function __construct()
			{

			}
			
			/**
			 * Loads stripe settings template.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 31, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Loads Stripe WordPress core hooks.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param string $class  plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 31, 2018
			 */
			public function loadWpCore( $class ) {
				require_once( plugin_dir_path( __FILE__ ) . 'main/main.php' );
				new Stripe_WP_Hooks( $this, $class );
			}
			
			/**
			 * Processes tokenization of Stripe payment
			 *
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $donate      , donation data object
			 * @param $gateway     , gateway object
			 * @param $customer_id , stripe customer id
			 *
			 * @return array|mixed $charge if success object
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 31, 2018
			 */
			public function wfc_TokenPayment( $donate, $gateway, $customer_id ) {

				/*
				 * load stripe library
				 */
				require_once( plugin_dir_path( __FILE__ ) . 'lib/init.php' );

				/*
				 * get security token for stripe
				 */
				$sk = !empty($gateway['wfc_donation_stripesecretkey']) ? $gateway['wfc_donation_stripesecretkey'] : '';

				/*
				 * set api key for stripe library
				 */
				\Stripe\Stripe::setApiKey( $sk );

				/*
				 * get donation form currency
				 */
				$currency = explode( ',', $donate['currency'] );

				try {

					/*
					 * charge token customer for stripe
					 */
					$charge = \Stripe\Charge::create( array(
						"amount" => $donate['pdwp_sub_amt'] * 100,
						"currency" => $currency[1],
						"customer" => $customer_id,
						"description" => "Charge for {$donate['donor_email']}"
					) );

					/*
					 * serialize and return json response
					 */
					return $charge->jsonSerialize();

				} catch( Exception $e ) {

					/*
					 * return if an error occured
					 */
					return array(
						'status' => 'error',
						'errCode' => $e->getCode(),
						'errorMessage' => $e->getMessage()
					);
				}
			}
			
			/**
			 * Create Stripe token customer.
			 *
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param array $donate   donation data object
			 * @param array $gateway  gateway object
			 * @param mixed $token    one-time token create by stripe.js
			 *
			 * @return array $source if success object
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 31, 2018
			 */
			public function wfc_CreateCustomer( $donate, $gateway, $token ) {

				/*
				 * load stripe library
				 */
				require_once( plugin_dir_path( __FILE__ ) . 'lib/init.php' );

				/*
				 * get security token for stripe
				 */
				$sk = !empty($gateway['wfc_donation_stripesecretkey']) ? $gateway['wfc_donation_stripesecretkey'] : '';

				/*
				 * set api key for stripe library
				 */
				\Stripe\Stripe::setApiKey( $sk );

				try {

					/*
					 * create stripe customer
					 */
					$source = \Stripe\Customer::create( array(
						"description" => "Customer for {$donate['donor_email']}",
						"source" => $token
					) );

					/*
					 * serialize and return json response
					 */
					return $source->jsonSerialize();

				} catch( Exception $e ) {

					/*
					 * return if an error occured
					 */
					return array(
						'status' => 'error',
						'errCode' => $e->getCode(),
						'errorMessage' => $e->getMessage()
					);
				}
			}
		}
	}