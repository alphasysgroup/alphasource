<?php 

/**
* Stripe Wordpress Hooks
*/
class Stripe_WP_Hooks
{
	/**
	 * The stripe main class
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      null    $parent    The stripe main class
	 */
	public $parent = null;

	/**
	 * The main class of the plugin
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      null    $includes    The main class of the plugin
	 */
	public $includes = null;
	
	/**
	 * Stripe_WP_Hooks constructor.
	 *
	 * @param $a
	 * @param $b
	 */
	function __construct( $a, $b )
	{
		/*
		 * The stripe main class
		 */
		$this->parent = $a;

		/*
		 * The main class of the plugin
		 */
		$this->includes = $b;

		/*
		 * stripe scripts
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_stripescripts' ) );
		
		/*
		 * stripe oauth
		 */
		add_action( 'wp_ajax_wfc_stripe_payment', array( $this, 'wfc_StripeTokenPayment' ) );
		add_action( 'wp_ajax_nopriv_wfc_stripe_payment', array( $this, 'wfc_StripeTokenPayment' ) );
	}

	/**
	 * Stripe load js libraries.
	 *
     * @author Junjie Canonio <junjie@alphasys.com.au>
	 * @since    5.0.0
	 * @return void
	 *
	 * @LastUpdated  May 31, 2018
	 *
	 */
	public function wfc_load_stripescripts() {
		global $post;
		
		// stripe.js load
		wp_register_script(
			'webforce-connect-donation-stripe_library_js',
			'https://js.stripe.com/v3/',
			array(),
			4.0,
			true
		);

		//pdwp paydock js
		wp_register_script(
			'webforce-connect-donation-stripe_js',
			plugin_dir_url( __FILE__ ) . '../js/stripe.js',
			array(),
			4.0,
			true
		);
	}
	
	/**
	 * Stripe register ajax endpoint for payment.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @return void
	 * @since    5.0.0
	 *
	 * @LastUpdated  May 31, 2018
	 */
	public function wfc_StripeTokenPayment() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_stripe_payment' ) {

			/*
			 * break if direct call enpoint
			 */
 			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
			 * donate
			 */
			$donate = $_POST['param']['donation'];

			/*
			 * token
			 */
			$token = $_POST['token'];

			/*
			 * get gateway
			 */
			$gateway = get_option($donate['gatewayid']);

			/*
			 * create stripe customer
			 */
			$customer = $this->parent->wfc_CreateCustomer( $donate, $gateway, $token );

			if( isset( $customer['status'] ) && $customer['status'] == 'error' ) {

				/*
				 * return to form if error occured
				 */
				$errors[] = $customer['errCode'];
				$errors[] = $customer['errorMessage'];

				wp_send_json( array(
					'status' => 'error',
					'errors' => $errors
				) );

			} else {

				/*
				 * if creating customer success, process payment
				 */
				$charge = $this->parent->wfc_TokenPayment( $donate, $gateway, $customer['id'] );

				if( isset( $charge['status'] ) && $charge['status'] == 'error' ) {

					/*
					 * return to form if error occured
					 */
					$errors[] = $customer['errCode'];
					$errors[] = $customer['errorMessage'];

					wp_send_json( array(
						'status' => 'error',
						'errors' => $errors
					) );

				} else {

					/*
					 * success transaction
					 */
					wp_send_json( array(
						'status' => 'success',
						'response' => $charge
					) );
				}
			}
		}
	}
}