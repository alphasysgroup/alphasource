/**
* jQuery
*/
var j = jQuery;

var NabGateway = function( config ) {
	this.config = config;
	j('#pdwp-btn').show();

	j('#wfc_donation-payment-details-toogle-container').after('<div id="nab-card-details" class="col-12 col-sm-12 col-md-12 col-lg-12"><div id="nab-credit-card-wrapper" class="wfc-donation-credit-card-wrapper row"></div></div>').hide();

	// j('#payment-details-wrapper').before('<div id="gateway-loader-container" style="text-align: center;"><span id="gateway-loader"></span></div>');
	// j('.payment-details-wrapper').after('<div id="nab-card-detals"></div>').hide();
	this.donationData = null;
 
	var self = this;
	j.getScript( "https://gateway.nab.com.au/form/version/45/merchant/"+ this.config.settings.wfc_donation_nabmerchantid +"/session.js", function() {
 		console.log( 'NAB Gateway Session Loaded!!' );

 		self.generateForm();

	} );
};

NabGateway.prototype.generateForm = function() {

	var formdata = ''

	+   '<div id="card-number-wrapper" class="col-12 col-sm-12 col-md-6 col-lg-6 card-number-wrapper-nab">'
	+    	'<label class="wfc-don-field-label" for="nab_card_number">Card Number <span style="color:red;"> *</span></label> '
	+    	'<input type="text" id="nab_card_number" class="widefat" name="" value="" placeholder="" readonly="readonly">'
	+   '</div>'

	+   '<div id="expiry-month-wrapper" class="col-12 col-sm-12 col-md-3 col-lg-3">'
	+   	'<label class="wfc-don-field-label" for="nab_expiry_month">Exp. Month <span style="color:red;"> *</span></label> '
	+   	'<select id="nab_expiry_month" class="" name="" size="1" data-filter="false" placeholder="" style="width: 100%">'
	+      		'<option value="01">01</option>'
	+      		'<option value="02">02</option>'
	+      		'<option value="03">03</option>'
	+      		'<option value="04">04</option>'
	+      		'<option value="05">05</option>'
	+      		'<option value="06">06</option>'
	+      		'<option value="07">07</option>'
	+      		'<option value="08">08</option>'
	+      		'<option value="09">09</option>'
	+      		'<option value="10">10</option>'
	+      		'<option value="11">11</option>'
	+      		'<option value="12">12</option>'
	+   	'</select>'
	+   '</div>'
	+   '<div id="expiry-year-wrapper" class="col-12 col-sm-12 col-md-3 col-lg-3">'
	+   	'<label class="wfc-don-field-label" for="nab_expiry_year">Exp. Year <span style="color:red;"> *</span></label> '
	+       '<select id="nab_expiry_year" class="" name="" size="1" data-filter="false" placeholder="" style="width: 100%">'

				j.each( this.CreditCardYears(), function( k, v ) {
					formdata += '<option value="'+v+'">'+v+'</option>';
				});

	formdata += ''
	+       '</select>'
	+   '</div>'
	+   '<div id="card-name-on-card-wrapper" class="col-12 col-sm-12 col-md-6 col-lg-6">'
	+      	'<label class="wfc-don-field-label" for="nab_name_on_card">Name on Card <span style="color:red;"> *</span></label>'
	+	   	'<input type="text" id="nab_name_on_card" class="widefat" name="" value="" placeholder="">'
	+   '</div>'
	+   '<div id="card-name-ccv-wrapper" class="col-12 col-sm-12 col-md-6 col-lg-6">'
	+      	'<label class="wfc-don-field-label" for="nab_ccv">CCV <span style="color:red;"> *</span></label>'
	+		'<input type="text" id="nab_ccv" class="widefat" name="" value="" placeholder="" readonly="readonly">'
	+   '</div>';




	j('#nab-credit-card-wrapper').append( formdata );

	// TogglePaymentType();
	//            j('#gateway-loader-container').remove();
 
 	PaymentSession.configure({
	    fields: {
	        // Attach hosted fields to your payment page
            card: {
                number: "#nab_card_number",
                expiryMonth: "#nab_expiry_month",
                expiryYear: "#nab_expiry_year",
                securityCode: "#nab_ccv",
            },
            // ach: {
            //     accountType: "#nab_account_type",
            // 	bankAccountHolder: "#nab_account_name",
            // 	bankAccountNumber: "#nab_account_number",
            // 	routingNumber: "#nab_bank_code"
            // }
	    },
	    frameEmbeddingMitigation: ["javascript"],
	    callbacks: {
	        initialized: function(response) {
	           TogglePaymentType();
	           j('#gateway-loader-container').remove();
	           setTimeout( function() {
		           j('.card-number-wrapper-nab').addClass('hgt_ovrd');
	           }, 0 )

	        },
	        formSessionUpdate: this.nabSessionUpdate.bind(this)
	    }
	});
};

NabGateway.prototype.nabSessionUpdate = function( response ) {
	var processdonate = new DonateProcessor();
	var nab_session_resp = null;
	var donate = this.donationData;
	var ccdetails = {
		card_number : '',
		name_on_card : '',
		expiry_month : '',
		expiry_year : '',
		ccv : ''
	};

	var self = this;

	if ( response.status ) {

		if ( "ok" == response.status ) {

			nab_session_resp = {
				status : 'success',
				session : response.session.id
			};

		} else if ( "fields_in_error" == response.status ) {

			var sess_err = [];

			if ( response.errors.cardNumber ) {
				sess_err.push( "Card number invalid or missing." );
			}

			if (response.errors.expiryYear) {
				sess_err.push( "Expiry year invalid or missing." );
			}

			if (response.errors.expiryMonth) {
				sess_err.push( "Expiry month invalid or missing." );
			}

			if (response.errors.securityCode) {
				sess_err.push( "Security code invalid." );
			}

			if (response.errors.number) {
				sess_err.push( "Gift card number invalid or missing." );
			}

			if (response.errors.pin) {
				sess_err.push( "Pin invalid or missing." );
			}

			if (response.errors.bankAccountHolder) {
				sess_err.push( "Bank account holder invalid." );
			}

			if (response.errors.bankAccountNumber) {
				sess_err.push( "Bank account number invalid." );
			}

			if (response.errors.routingNumber) {
				sess_err.push( "Routing number invalid." );
			}

			nab_session_resp = {
				status : 'error',
				session : sess_err
			};

		} else if ("request_timeout" == response.status) {
			nab_session_resp = {
				status : 'error',
				session : "Session update failed with request timeout: " + response.errors.message
			};
		} else if ("system_error" == response.status) {
			nab_session_resp = {
				status : 'error',
				session : "Session update failed with system error: " + response.errors.message
			};
		}
	} else {
		nab_session_resp = {
			status : 'error',
			session : "Session update failed: " + response
		};
	}

	var donationForm = getDonationFormDetails();

	self.nabCreateToken( nab_session_resp, donate )
	.then( function( token ) {
		if( token.status == 'error' ) {
			return processdonate.resetForm( token );
		} else {
			return self.nabProcessPayment( token );
		}
	})
	.then( function( payment ) {
		if( payment.status == 'error' ) {
			WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
			return processdonate.resetForm( payment );
		} else {
			WFC_Donation_LoadingBar.load('Saving Payment Transaction', 70, template_color);
			return processdonate.donationData( payment, 'update' );
		}
	})
	.then(function( result ) {
		if( result.status == 'success' ) {
			WFC_Donation_LoadingBar.load('Finalizing Payment', 80, template_color);
			return processdonate.syncdonationDataToSF( result, donationForm );
		} else {
			WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
			return processdonate.completeDonation( result.donation );
		}
	})
	.then(function( result ) {
		WFC_Donation_LoadingBar.load('Redirecting Page', 100, template_color);
		return processdonate.completeDonation( result.donation );
	})
	.catch( function( error ) {
		console.error( error )
	});
};

NabGateway.prototype.toTitleCase = function(str) {
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};

NabGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

NabGateway.prototype.processUdpateDonation = function( donation, tokenData, cc ) {
	var self = this;
	var donationData = donation;
	var token = tokenData;
	var processdonate = new DonateProcessor();
 	return new Promise(function ( resolve, reject ) {
 		var paymentDate = new Date( donationData.timestamp * 1000 );
 		j.each( donationData.cartItems, function( k, v ) {

 			self.getDonationData( v.post_meta_id ).done( function( data ) {

 				var donationData = data.donation;

 				donationData.payment_response = v.payment_response;

 				if( v.payment_response.result == 'SUCCESS' ) {
 					donationData.statusCode = 1;
 					donationData.statusText = 'APPROVED';
 				} else {
 					donationData.statusCode = 0;
 					donationData.statusText = 'FAILED';
 				}

 				donationData.PaymentTransaction = {
 					TxnStatus : ( donationData.statusCode == 1 ) ? 'Success' : 'Failed',
 					TxnId : v.payment_response.transaction.acquirer.transactionId,
 					TxnDate : self.formatDate( paymentDate ),
 					TxnMessage : v.payment_response.response.gatewayCode + '-' + donationData.pamynet_ref,
 					TxnPaymentRef : donationData.pamynet_ref
 				};

 				donationData.TokenCustomerID = token;

 				cc.card_number = v.payment_response.sourceOfFunds.provided.card.number

 				donationData.card_details = cc;
 				donationData.post_meta_id = v.post_meta_id;

 				processdonate.donationData( {
 					status : 'success',
 					donation : donationData
 				}, 'update' );

 			} );

 		} );

 		resolve( donationData );
 	});
};

NabGateway.prototype.nabCreateToken = function( nab_session_resp, donate ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		if( nab_session_resp.status == 'error' ) {
 			resolve( {
				status : 'error',
				response : nab_session_resp.session,
				donation : donate
			} );
 		} else {
 			j.ajax({
 				type: 'POST',
 				url:  pdwp_ajax.url,
 				data: {
 					'action':'wfc_nab_create_token',
 					nonce: pdwp_ajax.nonce,
 					session : nab_session_resp.session,
 					donate : donate
 				},
 				success: function( resp ) {
 					resolve( resp );
 				},
 				error: function( resp ) {
 					reject( resp );
 				}
 			});
 		}
 	});
};

NabGateway.prototype.nabProcessPayment = function( resp ) {
	var self = this;
	var token = resp.response.token;
	var cc = {
		card_number : resp.response.sourceOfFunds.provided.card.number,
		name_on_card : j('#nab_name_on_card').val(),
		expiry_month : j('#nab_expiry_month').val(),
		expiry_year : j('#nab_expiry_year').val(),
		ccv : 'xxx'
	};

	// var details_card_name = cc.name_on_card.split(' ');
	// details_card_name_0 = ( typeof details_card_name[0] != 'undefined' ) ? details_card_name[0] : '';
	// details_card_name_1 = ( typeof details_card_name[1] != 'undefined' ) ? details_card_name[1] : '';;
	// if ( !details_card_name_0.trim() || !details_card_name_1.trim()) {
	// 	return new Promise(function ( resolve, reject ) {
	// 		var err = [];
	// 		err.push( 'Card Holder Name should be in full name!' );
	// 		resolve( {
	// 			status : 'error',
	// 			response : err,
	// 			donation : resp.donation
	// 		} );
	// 	});
	// }else{
	 	return new Promise(function ( resolve, reject ) {
	 		j.ajax({
				type: 'POST',
				url:  pdwp_ajax.url,
				data: {
					'action':'wfc_nab_payment',
					nonce: pdwp_ajax.nonce,
					token : resp.response.token,
					donate : resp.donation
				},
				success: function( resp ) {
					if( resp.status == 'error' ) {
						resolve( resp );
					} else {

						var donate = resp.donation;
						var paymentDate = new Date( donate.timestamp * 1000 );

						donate.payment_response = resp.response;

						if( resp.response.result == 'SUCCESS' ) {
							donate.statusCode = 1;
							donate.statusText = 'APPROVED';
						} else {
							donate.statusCode = 0;
							donate.statusText = 'FAILED';
						}

						donate.PaymentTransaction = {
							TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
							TxnId : resp.response.transaction.acquirer.transactionId,
							TxnDate : self.formatDate( paymentDate ),
							TxnMessage : resp.response.response.gatewayCode + '-' + donate.pamynet_ref,
							TxnPaymentRef : donate.pamynet_ref
						};

						donate.TokenCustomerID = token;
						donate.card_details = cc;

						resolve( {
							status : 'success',
							response : resp.response,
							donation : donate
						} );
					}
				},
				error: function( resp ) {
					reject( resp );
				}
			});
	 	});
	//}

};

NabGateway.prototype.generateSession = function( param, details ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {

 		self.donationData = param.donation;

 		j('#pdwp-btn').attr( 'disabled', false ).on( 'click', function( e ) {
			e.preventDefault();

			// var mode = ( j('input[name="payment_type"]:checked').val() == 'credit_card' ? 'card' : 'ach' );

			if( typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank' ) {

				var donate = param.donation;
				var paymentDate = new Date( donate.timestamp * 1000 );
 
				donate.payment_response = {};
				donate.statusCode = 1;
				donate.statusText = 'PENDING';

				donate.PaymentTransaction = {
 					TxnStatus : 'Success',
 					TxnId : '',
 					TxnDate : self.formatDate( paymentDate ),
 					TxnMessage : '',
 					TxnPaymentRef : donate.pamynet_ref
 				};

 				donate.card_details = self.cleanBank( details );

				resolve( {
 					status : 'success',
 					response : {},
 					donation : donate
 				} );

			} else {

				/**
				* card payment
				*/
				PaymentSession.updateSessionFromForm( 'card' );
			}


		} ).trigger( 'click' ).attr( 'disabled', true );

 	} );
};

NabGateway.prototype.getDonationData = function( post_meta_id ) {
	return j.ajax({
		type: 'POST',
		url:  pdwp_ajax.url,
		data: {
			'action':'nab_get_donation_data',
			nonce: pdwp_ajax.nonce,
			post_meta_id : post_meta_id,
		}
	});
};

NabGateway.prototype.processPayment = function( param, cc ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {

 		if( param.donation.hasOwnProperty( 'cartItems' ) ) {
 			var cart_amnt = 0;
 			var cart_donation_type = '';
 			j.each( param.donation.cartItems, function( k, v ) {
 				cart_amnt += parseInt( v.amount );
 				cart_donation_type = v.gift_type;
 			} );
 			param.donation.pdwp_sub_amt = cart_amnt;
 			param.donation.pdwp_custom_amount = cart_amnt;
 			param.donation.pdwp_donation_type = cart_donation_type;
 		}

		self.generateSession( param, cc )
		.then(function(result) {
			resolve( result );
		})
		.catch(function(error){
			reject(error);
		});

 	} );
};

NabGateway.prototype.CreditCardYears = function() {
	var d = new Date();
	var currentYear = d.getFullYear(), years = [];
	var lastYear = new Date( d.setFullYear( d.getFullYear() + 10 ) ).getFullYear();
 	for( currentYear; currentYear <= lastYear; currentYear ++ ) {
 		years.push( currentYear );
 	}
	return years;
};

NabGateway.prototype.isValidIBANNumber = function(input) {
    var CODE_LENGTHS = {
        AD: 24, AE: 23, AT: 20, AZ: 28, BA: 20, BE: 16, BG: 22, BH: 22, BR: 29,
        CH: 21, CR: 21, CY: 28, CZ: 24, DE: 22, DK: 18, DO: 28, EE: 20, ES: 24,
        FI: 18, FO: 18, FR: 27, GB: 22, GI: 23, GL: 18, GR: 27, GT: 28, HR: 21,
        HU: 28, IE: 22, IL: 23, IS: 26, IT: 27, JO: 30, KW: 30, KZ: 20, LB: 28,
        LI: 21, LT: 20, LU: 20, LV: 21, MC: 27, MD: 24, ME: 22, MK: 19, MR: 27,
        MT: 31, MU: 30, NL: 18, NO: 15, PK: 24, PL: 28, PS: 29, PT: 25, QA: 29,
        RO: 24, RS: 22, SA: 24, SE: 24, SI: 19, SK: 24, SM: 27, TN: 24, TR: 26
    };
    var iban = String(input).toUpperCase().replace(/[^A-Z0-9]/g, ''), // keep only alphanumeric characters
            code = iban.match(/^([A-Z]{2})(\d{2})([A-Z\d]+)$/), // match and capture (1) the country code, (2) the check digits, and (3) the rest
            digits;
    // check syntax and length
    if (!code || iban.length !== CODE_LENGTHS[code[1]]) {
        return false;
    }
    // rearrange country code and check digits, and convert chars to ints
    digits = (code[3] + code[1] + code[2]).replace(/[A-Z]/g, function (letter) {
        return letter.charCodeAt(0) - 55;
    });
    // final check
    return this.mod97(digits);
};

NabGateway.prototype.mod97 = function(string) {
    var checksum = string.slice(0, 2), fragment;
    for (var offset = 2; offset < string.length; offset += 7) {
        fragment = String(checksum) + string.substring(offset, offset + 7);
        checksum = parseInt(fragment, 10) % 97;
    }
    return checksum;
};

NabGateway.prototype.cleanBank = function( bank ) {
	var bank_details = {
	    bank_code: ( typeof bank.bank_code != 'undefined' ? bank.bank_code : '' ),
	    bank_number: ( typeof bank.bank_number != 'undefined' ? bank.bank_number : '' ),
	    bank_name: ( typeof bank.bank_name != 'undefined' ? bank.bank_name : '' )
	};

 	var regex = /\d(?=\d{4})/mg;
  	var strbank = bank_details.bank_number.replace(/ /g,'');
  	var strcode = bank_details.bank_code.replace(/ /g,'');
  	var subst = 'x';

  	bank_details.bank_code = strcode.replace(regex, subst);
  	bank_details.bank_number = strbank.replace(regex, subst);
  	return bank_details;
};


function TogglePaymentType() {
 	j('#bank-wrapper').removeClass( "pronto-donation-show" ).addClass( "pronto-donation-hide" );
	j('input[name=nab_payment_type]').click(function(){
		if( j(this).val() == 'bank' ){
			j('div[name=credit-card-wrapper]').removeClass( "pronto-donation-show" ).addClass( "pronto-donation-hide" );
			j('#bank-wrapper').removeClass( "pronto-donation-hide" ).addClass( "pronto-donation-show" );
		}else{
			j('#bank-wrapper').removeClass( "pronto-donation-show" ).addClass( "pronto-donation-hide" );
			j('div[name=credit-card-wrapper]').removeClass( "pronto-donation-hide" ).addClass( "pronto-donation-show" );
		}
    });  	
}

/*
* Get the data of fields on form
*/
function getDonationFormDetails() {
	var details = j('#pdwp-form-donate').serializeArray();

	var donation = {};
	var creditcard = {
		card_number: j('#card_number').val(),
		ccv: j('#ccv').val(),
		name_on_card: j('#name_on_card').val(),
		expiry_month: j('#expiry_month').val(),
		expiry_year: j('#expiry_year').val()
	};

	j.each( details, function( k, v ) {
		if( v.name.indexOf( '[' ) > -1 || v.name.indexOf( ']' ) > -1 ) {
			var newkey = v.name.split('[')[0];
			donation[newkey] = v.value;
		} else {
			donation[v.name] = v.value;
		}
	});

	this.creditCardDetails = creditcard;

	var gateway = donation.payment_gateway.split( '_' );
	var gatewayKey 		= gateway.length - 2;
	var gatewayname 	= ( gateway.length > 1 ) ? gateway[ gatewayKey ] : gateway[0];
	
	if( gatewayname == 'nab' ) {
		var bank = {
			bank_code : j('#bank_code').val(),
			bank_number : j('#account_number').val(),
			bank_name : j('#account_name').val()
		};
	} else {
		var bank = {
			bank_code : j('#pdwp_bank_code').val(),
			bank_number : j('#pdwp_account_number').val(),
			bank_name : j('#pdwp_account_holder_name').val()
		};
	}

	return {
		donation : donation,
		creditcard : creditcard,
		bank : bank
	};
}

