<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'NabGateway' ) ) {
		
		/**
		 * Class Nab
		 */
		class Nab
		{
			public $restricted = array(
				'wfc_donation_nabmerchantpass'
			);

			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-nab_js'
			);


			function __construct()
			{

			}
			
			/**
			 * Load NAB settings template.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 31, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Load NAB Wordpress core hooks.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $class , plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 31, 2018
			 */
			public function loadWpCore( $class ) {
				require_once( plugin_dir_path( __FILE__ ) . 'main/main.php' );
				new Nab_Gateway_WP_Hooks( $this, $class );
			}
			
			/**
			 * Merchant initialization which loads configurations from API Library.
			 *
			 * @author      Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $config
			 *
			 * @return array
			 *
			 * @since       5.0.0
			 *
			 * @LastUpdated : May 31, 2018
			 */
			public function merchantInit( $config ) {

				/*
				 * load nab config api library
				 */
				require_once( plugin_dir_path( __FILE__ ) . 'lib/configuration.php' );

				/*
				 * load nab config api library
				 */
				require_once( plugin_dir_path( __FILE__ ) . 'lib/connection.php' );

				// nab merchant
				$merchantObj = new Merchant( $config );

				return array(
					'merchantObj' => $merchantObj,
					'parserObj' => new Parser( $merchantObj )
				);
			}
			
			/**
			 * Fetches NAB gateway settings.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @since    5.0.0
			 *
			 * @param $gateway
			 *
			 * @return array
			 *
			 * @LastUpdated   May 31, 2018
			 */
			public function getNabSettings( $gateway ) {
				return array(
					'merchant_id' => isset($gateway['wfc_donation_nabmerchantid']) ? $gateway['wfc_donation_nabmerchantid'] : '',
					'merchant_password' => isset($gateway['wfc_donation_nabmerchantpass']) ? $gateway['wfc_donation_nabmerchantpass'] : ''
				);	
			}
			
			/**
			 * Fetch gateway configurations.
			 *
			 * @author   Junjie Canonio
			 *
			 * @param array $gateway
			 * @param string $action
			 * @param array $donate
			 *
			 * @return array
			 * @since    5.0.0
			 *
			 * @LastUpdated   May 31, 2018
			 */
			public function getConfig( $gateway, $action, $donate ) {

				$action = ($action == 'pay') ? "order/{$donate['pamynet_ref']}/transaction/{$donate['pamynet_ref']}" : $action;

				$merchant_id = isset($gateway['wfc_donation_nabmerchantid']) ? 
				trim($gateway['wfc_donation_nabmerchantid']) : '';

				$merchant_password = isset($gateway['wfc_donation_nabmerchantpass']) ? 
				trim($gateway['wfc_donation_nabmerchantpass']) : '';

				return array(
					"gatewayUrl" => "https://gateway.nab.com.au/api/rest/version/45/merchant/{$merchant_id}/{$action}",
					"merchantId" => $merchant_id,
					"apiUsername" => "merchant.{$merchant_id}",
					"password" => $merchant_password,
					"version" => "45"
				);
			}
		}
	}