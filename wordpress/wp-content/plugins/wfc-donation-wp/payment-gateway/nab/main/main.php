<?php 

/**
* Nab Wordpress Hooks
*/
class Nab_Gateway_WP_Hooks
{
	/**
	 * The Nab main class
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $parent    The Nab main class
	 */
	public $parent = null;

	/**
	 * The main class of the plugin
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $includes    The main class of the plugin
	 */
	public $includes = null;
	
	/**
	 * Nab_Gateway_WP_Hooks constructor.
	 *
	 * @param $a
	 * @param $b
	 */
	function __construct( $a, $b )
	{

		/**
		 * The nab main class
		 */
		$this->parent = $a;

		/**
		 * The main class of the plugin
		 */
		$this->includes = $b;

		/**
		 * nab scripts
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_nabscripts' ) );

		/**
		 * nab payment ajax endpoint
		 */
		add_action( 'wp_ajax_wfc_nab_create_token', array( $this, 'wfc_NabCreateToken' ) );
		add_action( 'wp_ajax_nopriv_wfc_nab_create_token', array( $this, 'wfc_NabCreateToken' ) );

		/**
		 * nab payment ajax endpoint
		 */
		add_action( 'wp_ajax_wfc_nab_payment', array( $this, 'wfc_NabProcessPayment' ) );
		add_action( 'wp_ajax_nopriv_wfc_nab_payment', array( $this, 'wfc_NabProcessPayment' ) );
	}
	
	/**
	 * Loads Nab Javascript libraries.
	 * LastUpdated : May 31, 2018
	 *
	 * @author   Junjie Canonio
	 * @return void
	 * @since    5.0.0
	 */
	public function wfc_load_nabscripts() {
		global $post;

		 wp_register_script(
			'webforce-connect-donation-nab_js',
			plugin_dir_url( __FILE__ ) . '../js/nab.js',
			array(),
			4.0,
			 true
		);

	}
	
	/**
	 * Ajax Callback function that creates NAB Token.
	 *
	 * @author   Junjie Canonio
	 * @return void
	 * @since    5.0.0
	 *
	 * @LastUpdated  May 31, 2018
	 */
	public function wfc_NabCreateToken() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_nab_create_token' ) {

			/*
			 * prevent processing on direct access endpoint
			 */
			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
             * get donation inputs
             */
			$donate = isset( $_POST['donate'] ) ? $_POST['donate'] : array();

			$sessionid = isset( $_POST['session'] ) ? $_POST['session'] : '';
			if( empty( $sessionid ) ) {
				/*
				 * return to form and display error
				 */
				wp_send_json( array(
					'status' => 'error',
					'response' => "session is not provided",
					'donation' => $donate
				) );
			}

			/*
             * get gateway
             */
            $gateway = get_option($donate['gatewayid']);

            /*
             * get config
             */
			$nab_config = $this->parent->getConfig( $gateway, 'token', $donate );

            /*
             * nab merchant
             */
			$init = $this->parent->merchantInit( $nab_config );

			/*
             * do create token
             */
			$response = $init['parserObj']->SendTransaction( 
				$init['merchantObj'], 
				$init['parserObj']->ParseRequest( array( 
					"session" => array( 
						"id" => $sessionid 
					) 
				) ), 
				'POST'
			);

			$nabresponse = json_decode( $response );

			if( isset( $nabresponse->result ) && $nabresponse->result == 'ERROR' ) {
				wp_send_json( array(
					'status' => 'error',
					'response' => $nabresponse->error->explanation,
					'donation' => $donate
				) );
			} elseif( isset( $nabresponse->status ) && $nabresponse->status != 'VALID' ) {
				wp_send_json( array(
					'status' => 'error',
					'response' => 'Card is not Valid!',
					'donation' => $donate
				) );
			} else {
				wp_send_json( array(
					'status' => 'success',
					'response' => $nabresponse,
					'donation' => $donate
				) );
			}
		}
	}
	
	/**
	 * AJAX Callback function that processes Payment in NAB Gateway.
	 *
	 * @author   Junjie Canonio
	 *
	 * @return void
	 *
	 * @since    5.0.0
	 *
	 * @LastUpdated  May 31, 2018
	 */
	public function wfc_NabProcessPayment() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_nab_payment' ) {

			/*
			 * prevent processing on direct access endpoint
			 */
			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
             * get donation inputs
             */
			$donate = isset( $_POST['donate'] ) ? $_POST['donate'] : array();

			$token = isset( $_POST['token'] ) ? $_POST['token'] : '';
			if( empty( $token ) ) {
				/*
				 * return to form and display error
				 */
				wp_send_json( array(
					'status' => 'error',
					'response' => "token is not provided",
					'donation' => $donate
				) );
			}

			/*
	         * get gateway
	         */
	        $gateway = get_option($donate['gatewayid']);

	        /*
	         * get config
	         */
			$nab_config = $this->parent->getConfig( $gateway, 'pay', $donate );

	        /*
	         * nab merchant
	         */
			$init = $this->parent->merchantInit( $nab_config );

			/*
	         * do create token
	         */
			$response = $init['parserObj']->SendTransaction(
				$init['merchantObj'], 
				$init['parserObj']->ParseRequest( array(
				    "apiOperation" => "PAY",
				    "sourceOfFunds" => array(
				    	"token" => $token,
				    ),
				    "order" => array(
			            "amount" => number_format(
	                        $donate['pdwp_sub_amt'],
	                        2,
	                        ".",
	                        ""
	                    ),
			            "currency" => explode( ',', $donate['currency'] )[1]
				    ),
				    "transaction" => array(
				    	"frequency" => "RECURRING"
				    )
				)),
				'PUT'
			);
			
			$nabresponse = json_decode( $response );

			if( isset( $nabresponse->result ) && $nabresponse->result === 'ERROR' ) {
				wp_send_json( array(
					'status' => 'error',
					'response' => $nabresponse->error->explanation,
					'donation' => $donate
				) );
			} else {
				wp_send_json( array(
					'status' => 'success',
					'response' => $nabresponse,
					'donation' => $donate
				) );
			}
		}
	}
}