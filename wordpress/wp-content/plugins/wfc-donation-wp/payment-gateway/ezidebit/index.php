<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'Ezidebit' ) ) {
		
		/**
		 * Class Ezidebit
		 */
		class Ezidebit
		{

			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-ezidebit_lib_js',
				'webforce-connect-donation-ezidebit_js'
			);
			
			function __construct()
			{

			}
			
			/**
			 * Load Ezidebit settings template.
			 *
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 3, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Loads Ezidebit WordPress core hooks.
			 * LastUpdated : May 3, 2018
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param string|null $class  plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 3, 2018
			 */
			public function loadWpCore( $class ) {
				require_once plugin_dir_path(__FILE__) . 'main/main.php';
				new Ezidebit_WP_Hooks( $this, $class );
			}
			
			/**
			 * Get gateway transaction error/s.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param array $donate donation form post
			 *
			 * @return array array
			 * @since    5.0.0
			 *
			 * @LastUpdated  May 3, 2018
			 */
			public function getGatewayTransactionError( $donate ) {

				$code = null;
				$message = null;

				if( isset( $donate['TokenCustomerID'] ) 
					&& $donate['TokenCustomerID'] != null ) {

					$res = isset( $donate['payment_response']['ErrorMessage'] ) ? 
									$donate['payment_response']['ErrorMessage'] : null;

					$response = ( $res ) ? explode( '-' , $res ) : '';

					$code = isset( $response[0] ) ? $response[0] : null;
					$message = isset( $response[1] ) ? $response[1] : null;

					return array(
						'responseCode' => $code,
						'responseMessage' => $message
					);

				} else {

					$code = isset( $donate['payment_response']['PaymentResultCode'] ) ? 
										$donate['payment_response']['PaymentResultCode'] : null;

					$message = isset( $donate['payment_response']['PaymentResultText'] ) ? 
										$donate['payment_response']['PaymentResultText'] : null;

					return array(
						'responseCode' => $code,
						'responseMessage' => $message
					);
				}
			}
		}
	}