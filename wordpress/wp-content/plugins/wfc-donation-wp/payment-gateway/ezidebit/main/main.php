<?php

	/**
	* Ezidebit Wordpress Hooks
	*/
	class Ezidebit_WP_Hooks
	{
		/**
		 * The Ezidebit main class
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $parent    The Ezidebit main class
		 */
		public $parent = null;

		/**
		 * The main class of the plugin
		 *
		 * @since    5.0.0
		 * @access   private
		 * @var      string    $includes    The main class of the plugin
		 */
		public $includes = null;
		
		/**
		 * Ezidebit_WP_Hooks constructor.
		 *
		 * @param $a
		 * @param $b
		 */
		function __construct( $a, $b )
		{

			/*
			 * The Ezidebit main class
			 */
			$this->parent = $a;

			/*
			 * The main class of the plugin
			 */
			$this->includes = $b;

			/*
			 * Ezidebit scripts
			 */
			add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_ezidebitscripts' ) );

			/*
			 * Eway payment ajax endpoint
			 */
			add_action( 'wp_ajax_wfc_ezidebit_payment', array( $this, 'wfc_EzidebitCreditCardProcess' ) );
			add_action( 'wp_ajax_nopriv_wfc_ezidebit_payment', array( $this, 'wfc_EzidebitCreditCardProcess' ) );

		}
		
		/**
		 * Loads script for ezidebit.
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 * @return void
		 * @since    5.0.0
		 *
		 * @LastUpdated  May 3, 2018
		 */
		public function wfc_load_ezidebitscripts() {
			global $post;
			
				/*
				 * ezidebit js library
				 */
				wp_register_script(
					'webforce-connect-donation-ezidebit_lib_js',
					'https://static.ezidebit.com.au/javascriptapi/js/ezidebit_2_0_0.min.js',
					array(),
					1.0,
					true
				);
				
				/*
				 * ezidebit js library
				 */
				wp_register_script(
					'webforce-connect-donation-ezidebit_js',
					plugin_dir_url( __FILE__ ) . '../js/ezidebit.js',
					array(),
					1.0,
					true
				);


			// if( ( isset( $post->post_type ) && $post->post_type == 'pdwp_donation' ) || 
			// 	( isset( $post->post_content ) && has_shortcode( $post->post_content, 'pronto-donation-form' ) ) ) {
	 	// 		/*
			// 	 * ezidebit js library
			// 	 */
			// 	wp_enqueue_script(PRONTO_DONATION_SLUG . 'ezidebit_js');
			// 	wp_enqueue_script(PRONTO_DONATION_SLUG . 'pdwp_ezidebit_gateway');
	 	// 	}
		}

		/**
		 * Ajax-callback Credit Card encryption process for Ezidebit
		 * LastUpdated : May 11, 2020
		 *
		 * @author   Junjie Canonio <junjie@alphasys.com.au>
		 * @return void array
		 * @since    5.0.0
		 */
		public function wfc_EzidebitCreditCardProcess() {
			if (isset($_POST['action']) && $_POST['action'] == 'wfc_ezidebit_payment') {
				/*
				 * prevent processing on direct access endpoint
				 */
				if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
					die('busted');
				}

				$donation = $_POST['param'];

				$oneTimeKeyResult = '';
				$oneTimeKey = '';
				if(isset($donation['gatewayid'])){
					$gateway = get_option($donation['gatewayid']);
					if(isset($gateway['wfc_donation_ezidebitpublickey']) && !empty($gateway['wfc_donation_ezidebitpublickey'])) {

						if(isset($gateway['wfc_donation_sandboxmode']) && $gateway['wfc_donation_sandboxmode'] == 'true'){
							$endpoint = "https://apix.demo.ezidebit.com.au/public-rest/v3-5/TestPublicKey";
						}else{
							$endpoint = "https://apix.ezidebit.com.au/public-rest/v3-5/TestPublicKey";
						}

						$querystring = "?PublicKey=";
						$myPublicKey = $gateway['wfc_donation_ezidebitpublickey']; // Replace with your key
						$url = $endpoint . $querystring . $myPublicKey;
						$oneTimeKeyResult = json_decode(file_get_contents($url));
						$oneTimeKey = json_decode(file_get_contents($url))->OneTimeKey;

						unset($gateway['wfc_donation_ezidebitpublickey']);
					}

				}



				if( !is_object( $oneTimeKeyResult ) && !empty($oneTimeKey)) {

					/*
					 * return to form and display error
					 */
					wp_send_json( array(
						'status' => 'error',
						'errors' => 'oneTimeKey not generated',
						'POST' => $_POST,
						'donation' => $donation
					) );

				} else {

					if(isset($oneTimeKeyResult->OneTimeKey)){
						unset($oneTimeKeyResult->OneTimeKey);
					}

					/*
					 * encrypt one time key
					 */
					$oneTimeKeyOriginal = $oneTimeKey;
					$sessionId = isset($donation['campaignSessionId']) ? $donation['campaignSessionId'] : '';
					$oneTimeKey = WFC_DonationEncryptDecrypt::wfc_don_cryptoJsAesEncrypt($sessionId, $oneTimeKey);


					/*
					 * success response
					 */
					wp_send_json( array(
						'ResponseFrom' => 'wfc_EzidebitCreditCardProcess',
						'status' => 'success',
						'oneTimeKeyResult' => $oneTimeKeyResult,
						//'oneTimeKeyOriginal' => $oneTimeKeyOriginal,
						'oneTimeKey' => $oneTimeKey,
						'isEncrypted' => true,
						'POST' => $_POST,
						'donation' => $donation,
						'gateway' => $gateway
					) );
				}
			}
		}


		


	}