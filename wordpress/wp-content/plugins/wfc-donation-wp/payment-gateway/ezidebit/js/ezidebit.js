var j = jQuery;

var EzidebitGateway = function( config ) {
  this.config = config;
  j('#pdwp-btn').show();

  j('#wfc_donation-payment-details-toogle-container').show();
  j("label[for=payment_type_credit_card]").click();

};

EzidebitGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

EzidebitGateway.prototype.clean = function( cc ) {

  var creditcarddetails = {
    card_number: cc.card_number,
    name_on_card: cc.name_on_card,
    expiry_month: cc.expiry_month,
    expiry_year: cc.expiry_year,
    ccv: cc.ccv
  };

  var regex = /\d(?=\d{4})/mg;
  var str = creditcarddetails.card_number.replace(/ /g,'');
  var subst = '*';

  creditcarddetails.card_number = str.replace(regex, subst);
  creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

  return creditcarddetails;
};

EzidebitGateway.prototype.cleanBank = function( bank ) {
  var bank_details = {
      bank_code: ( typeof bank.bank_code != 'undefined' ? bank.bank_code : '' ),
      bank_number: ( typeof bank.bank_number != 'undefined' ? bank.bank_number : '' ),
      bank_name: ( typeof bank.bank_name != 'undefined' ? bank.bank_name : '' )
  };

  var regex = /\d(?=\d{4})/mg;
    var strbank = bank_details.bank_number.replace(/ /g,'');
    var strcode = bank_details.bank_code.replace(/ /g,'');
    var subst = 'x';

    bank_details.bank_code = strcode.replace(regex, subst);
    bank_details.bank_number = strbank.replace(regex, subst);
    return bank_details;
};

EzidebitGateway.prototype.creditCardPayment = function( param, cc ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		try {
 			var donate = param.donation;
 			var successCallback = function( data ) {

 				var success_response = ['00', '08', '10', '11', '16', '77', '000', '003'];
 				var paymentDate = new Date( donate.timestamp * 1000 );

 				donate.payment_response = data;

 				if( j.inArray( data.PaymentResultCode, success_response ) != -1 ) {
 					donate.statusCode = 1;
 					donate.statusText = 'APPROVED';
 				} else {
 					donate.statusCode = 0;
 					donate.statusText = 'FAILED';
 				}

 				donate.PaymentTransaction = {
 					TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
 					TxnId : data.ExchangePaymentID,
 					TxnDate : self.formatDate( paymentDate ),
 					TxnMessage : data.PaymentResultText + '-' + donate.pamynet_ref,
          TxnPaymentRef : donate.pamynet_ref
 				}

 				donate.card_details = self.clean( cc );

 				resolve( {
 					status : 'success',
 					response : data,
 					donation : donate
 				} );
 			};

 			var errorCallback = function( data ) {
 				resolve( {
 					status : 'error',
 					response : data,
 					donation : donate
 				} );
 			};

 			var endpoint = ( self.config.settings.wfc_donation_sandboxmode == 'true' ) ? 'demo.' : '';

 			eziDebit.init( self.config.settings.wfc_donation_ezidebitpublickey, {
 				submitAction: "ChargeCard",
 				submitButton: "pdwp-btn",
 				submitCallback: successCallback,
 				submitError: errorCallback,
 				nameOnCard: "name_on_card",
 				cardNumber: "card_number",
 				cardExpiryMonth: "expiry_month",
 				cardExpiryYear: "expiry_year",
 				cardCCV: "ccv",
 				paymentAmount: "pdwp_sub_amt",
 				paymentReference: "pdwp_payment_referrence"
 			}, 'https://api.'+ endpoint +'ezidebit.com.au/V3-5/public-rest' );

 			j('#pdwp-btn').click();

 		} catch( error ) {
 			reject( error );
 		}
 	});
};

EzidebitGateway.prototype.tokenizePayment = function( param, cc ) {
	console.log('tokenizePayment');

	var self = this;
	return new Promise(function ( resolve, reject ) {

		var donate = param.donation;

		var sessionId = donate.campaignSessionId;
		//console.log('sessionId : ' +sessionId);

		var successCallback = function( data ) {

			console.log('successCallback');

			j('#pdwp-btn').attr( 'disabled', 'disabled' );

			var paymentDate = new Date( donate.timestamp * 1000 );

			donate.payment_response = data;

			if( !data.ErrorMessage ) {

				donate.statusCode = 1;
				donate.statusText = 'APPROVED';

				donate.PaymentTransaction = {
					TxnStatus : 'Success',
					TxnId : data.Data.BankReceiptID,
					TxnDate : self.formatDate( paymentDate ),
					TxnMessage : null + '-' + donate.pamynet_ref,
					TxnPaymentRef : donate.pamynet_ref
				}

			} else {

				donate.statusCode = 0;
				donate.statusText = 'FAILED';

				donate.PaymentTransaction = {
					TxnStatus : 'Failed',
					TxnId : null,
					TxnDate : self.formatDate( paymentDate ),
					TxnMessage : null + '-' + donate.pamynet_ref,
					TxnPaymentRef : donate.pamynet_ref
				}
			}

			donate.TokenCustomerID = ( typeof data.Data.CustomerRef != 'undefiend' ) ? data.Data.CustomerRef : null;
			donate.card_details = self.clean( cc );

			resolve( {
				status : 'success',
				response : data,
				donation : donate
			} );

		};

		var errorCallback = function( data ) {

			console.log('errorCallback');
			console.log(data);
			resolve( {
				status : 'error',
				response : data,
				donation : donate
			} );
		};

		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_ezidebit_payment',
				param : param.donation,
				nonce: pdwp_ajax.nonce,
				process: 'creditcard',
			},
			success: function( resp ) {
				//console.log(resp);
				if( resp.status == 'success' && resp.status != undefined) {

					console.log('A');

					if( resp.oneTimeKeyResult.Data == 'success' && resp.oneTimeKeyResult.Data != undefined &&
						resp.oneTimeKeyResult.Error == 0 && resp.oneTimeKeyResult.Error != undefined) {
						console.log('A.A');
						//console.log(resp);

						var OneTimeKey = resp.oneTimeKey;
						var OneTimeKeyStatus = resp.isEncrypted;

						var endpoint = ( self.config.settings.wfc_donation_sandboxmode == 'true' ) ? 'demo.' : '';


						if(OneTimeKeyStatus == true && OneTimeKeyStatus != undefined && sessionId != undefined){
							var key = sessionId;
							var encrypted = OneTimeKey;
							var decrypted = JSON.parse(CryptoJS.AES.decrypt(encrypted, key, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
							OneTimeKey = decrypted;
							//console.log('decrypted : ' + decrypted);
							//console.log('oneTimeKeyOriginal : ' + resp.oneTimeKeyOriginal);
						}


						eziDebit.init( OneTimeKey, {
							submitAction: "SaveCustomer",
							submitButton: "pdwp-btn-ezidebit",
							submitCallback: successCallback,
							submitError: errorCallback,
							customerReference: "pdwp_payment_referrence",
							customerLastName: "donor_last_name",
							customerFirstName: "donor_first_name",
							customerAddress1: "donor_address",
							customerAddress2: "",
							customerSuburb: "donor_country",
							customerState: "donor_state",
							customerPostcode: "donor_postcode",
							customerEmail: "donor_email",
							customerMobile: "",
							nameOnCard: "name_on_card",
							cardNumber: "card_number",
							cardExpiryMonth: "expiry_month",
							cardExpiryYear: "expiry_year",
							cardCCV: "ccv",
							paymentAmount: "pdwp_sub_amt",
							paymentReference: "pdwp_payment_referrence"
						}, 'https://apix.'+ endpoint +'ezidebit.com.au/public-rest/v3-5' );

						j('#pdwp-btn-ezidebit').click();


					}else if(resp.oneTimeKeyResult.Error != 0 && resp.oneTimeKeyResult.Error != undefined){

						var error = resp.oneTimeKeyResult.ErrorMessage;


						resolve( {
							status : 'error',
							response : error,
							donation : param.donation
						} );
					}else {

						resolve( {
							status : 'error',
							response : 'oneTimeKey not generated',
							donation : param.donation
						} );
					}


				} else {
					if( resp.errors != undefined ){
						resolve( {
							status : 'error',
							response : resp.errors,
							donation : param.donation
						} );
					}else{
						var resp_extra = resp;
						resp_extra = resp_extra.replace('<h2>','');
						resp_extra = resp_extra.replace('</h2>','');
						resp_extra = resp_extra.replace('<pre>','');
						resolve( {
							status : 'error',
							response : resp_extra,
							donation : param.donation
						} );
					}
				}
			},
			error: function( resp ) {
				reject( resp );
			}
		});

	});

};


EzidebitGateway.prototype.processPayment = function( param, details ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {

    if( typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank' ) {

      var donate = param.donation;
      var paymentDate = new Date( donate.timestamp * 1000 );

      donate.payment_response = {};
      donate.statusCode = 1;
      donate.statusText = 'PENDING';

      donate.PaymentTransaction = {
        TxnStatus : 'Success',
        TxnId : '',
        TxnDate : self.formatDate( paymentDate ),
        TxnMessage : '',
        TxnPaymentRef : donate.pamynet_ref
      };

      donate.card_details = self.cleanBank( details );

      resolve( {
        status : 'success',
        response : {},
        donation : donate
      } );

    } else {

   		if( param.donation.wfc_donation_tokenize == 'true' ) {
        console.log('Ezidebit Tokenize Payment');
   			self.tokenizePayment( param, details )
   			.then(function( result ){
   				resolve( result );
   			})
   			.catch(function( error ){
   				reject( error );
   			});

   		} else {
   			console.log('Ezidebit Non-Tokenize Payment');
   			self.creditCardPayment( param, details )
   			.then(function( result ){
   				resolve( result );
   			})
   			.catch(function( error ){
   				reject( error );
   			});
   		}

    }
 	});
};

