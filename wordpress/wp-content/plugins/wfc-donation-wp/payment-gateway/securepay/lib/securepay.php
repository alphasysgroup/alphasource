<?php 

/**
* Secure Pay Payment Process
*/
class SecurePay_Api
{

    private $merchantId;
    private $txnPassword;
    private $isSandbox;
    private $verifySSL = true;
    private $timeOutValue = 60;
    private $amount;
    private $currency;
    private $PurchaseOrderNo;
    private $creditCardNumber;
    private $expiryMonth;
    private $expiryYear;
    private $ccv;

    private $cards = array(
        // Debit cards must come first, since they have more specific patterns than their credit-card equivalents.

        'visaelectron' => array(
            'type' => 'visaelectron',
            'pattern' => '/^4(026|17500|405|508|844|91[37])/',
            'length' => array(16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'maestro' => array(
            'type' => 'maestro',
            'pattern' => '/^(5(018|0[23]|[68])|6(39|7))/',
            'length' => array(12, 13, 14, 15, 16, 17, 18, 19),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'forbrugsforeningen' => array(
            'type' => 'forbrugsforeningen',
            'pattern' => '/^600/',
            'length' => array(16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'dankort' => array(
            'type' => 'dankort',
            'pattern' => '/^5019/',
            'length' => array(16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        // Credit cards
        'visa' => array(
            'type' => 'visa',
            'pattern' => '/^4/',
            'length' => array(13, 16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'mastercard' => array(
            'type' => 'mastercard',
            'pattern' => '/^(5[0-5]|2[2-7])/',
            'length' => array(16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'amex' => array(
            'type' => 'amex',
            'pattern' => '/^3[47]/',
            'format' => '/(\d{1,4})(\d{1,6})?(\d{1,5})?/',
            'length' => array(15),
            'cvcLength' => array(3, 4),
            'luhn' => true,
        ),
        'dinersclub' => array(
            'type' => 'dinersclub',
            'pattern' => '/^3[0689]/',
            'length' => array(14),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'discover' => array(
            'type' => 'discover',
            'pattern' => '/^6([045]|22)/',
            'length' => array(16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
        'unionpay' => array(
            'type' => 'unionpay',
            'pattern' => '/^(62|88)/',
            'length' => array(16, 17, 18, 19),
            'cvcLength' => array(3),
            'luhn' => false,
        ),
        'jcb' => array(
            'type' => 'jcb',
            'pattern' => '/^35/',
            'length' => array(16),
            'cvcLength' => array(3),
            'luhn' => true,
        ),
    );
    
    function __construct( $merchantId, $txnPassword, $isSandbox = true )
    {
        $this->merchantId = $merchantId;

        $this->txnPassword = $txnPassword;

        $this->isSandbox = $isSandbox;
    }

    public function verifySSL( $verifySSL = true ) {
        $this->verifySSL = $verifySSL;
    }

    public function setTimeoutValue( $timeout = 60 ) {
        $this->timeOutValue = $timeout;
    }
    
    public function setAmount( $amt ) {
        $this->amount = $amt;
    }
    
    public function setCurrency( $currency ) {
        $this->currency = $currency;
    }

    public function setPurchaseOrderNo( $ref ) {
        $this->PurchaseOrderNo = $ref;
    }

    public function setCardNumber( $creditcard ) {
        $this->creditCardNumber = $creditcard;
    }

    public function setExpiryMonth( $month ) {
        $this->expiryMonth = $month;
    }

    public function setExpiryYear( $year ) {
        $this->expiryYear = $year;
    }

    public function setCCV( $ccv ) {
        $this->ccv = $ccv;
    }

    private function getMessageId() {
        return time();
    }

    private function getMessageTimeStamp() {
        return date("YdmHisB" . "000+660");
    }

    private function getTimeoutValue() {
        return $this->timeOutValue;
    }

    private function getAmount() {

		$amount = $this->amount;

		if (strpos($amount, '.') == true){
			$amount = str_replace('.', '', $amount);
		}else{
			$amount = $amount . '00';
		}
//
//        if (!is_numeric($amount)) {
//            throw new \InvalidArgumentException("Invalid amount");
//        }
//        $amount = strval($amount);
//        if (strpos($amount, "-") !== false) {
//            throw new \InvalidArgumentException("Amount cannot be negative");
//        }
//        if (strpos($amount, ".") !== false) {
//            $amount = bcmul($amount, 100);
//        }
//
//        $this->amount = $amount;
//
//        return $this->amount;

	    return $amount;
    }

    private function getCurrency() {
        return $this->currency;
    }

    private function getPurchaseOrderNo() {
        return $this->PurchaseOrderNo;
    }

    private function getCardNumber() {

        $creditCard = $this->validCreditCard( $this->creditCardNumber );
        if ( !$creditCard["valid"] ) {
            throw new \InvalidArgumentException("Invalid credit card number");
        }

        return $this->creditCardNumber;
    }

    private function getExpiryMonth() {

        $expiryMonth = $this->expiryMonth;

        if (strlen($expiryMonth) > 2) {
            $expiryMonth = substr($expiryMonth, -2);
        } else if (strlen($expiryMonth) == 1) {
            $expiryMonth = "0" . $expiryMonth;
        }
        
        if (! preg_match('/^(0[1-9]|1[0-2])$/', $expiryMonth )) {
            throw new \InvalidArgumentException("Invalid expiry month");
        }
        
        $this->expiryMonth = $expiryMonth;

        return $this->expiryMonth;
    }

    private function getExpiryYear() {
        
        if (! preg_match('/^20\d\d$/', $this->expiryYear ) ) {
            throw new \InvalidArgumentException("Invalid expiry year");
        }

        $this->expiryYear = substr( $this->expiryYear, 2, 2);

        return $this->expiryYear;
    }

    private function getCCV() {

        $cardtype = $this->creditCardType( $this->creditCardNumber );
        if( ! $this->validCvc( $this->ccv, $cardtype ) ) {
            throw new \InvalidArgumentException("Invalid cvc");
        }

        return $this->ccv;
    }

    private function apiUrl() {
        if( $this->isSandbox ) {
            return 'https://test.api.securepay.com.au/xmlapi/';
        } else {
            return 'https://api.securepay.com.au/xmlapi/';
        }
    }

    private function creditCardType( $number ) {
        foreach ( $this->cards as $type => $card) {
            if (preg_match($card['pattern'], $number)) {
                return $type;
            }
        }
        return '';
    }

    public function validCreditCard($number, $type = null) {
        $ret = array(
            'valid' => false,
            'number' => '',
            'type' => '',
        );

        // Strip non-numeric characters
        $number = preg_replace('/[^0-9]/', '', $number);

        if (empty($type)) {
            $type = $this->creditCardType($number);
        }

        if (array_key_exists($type, $this->cards) && $this->validCard($number, $type)) {
            return array(
                'valid' => true,
                'number' => $number,
                'type' => $type,
            );
        }

        return $ret;
    }

    private function validCard( $number, $type ) {
        return ($this->validPattern($number, $type) && $this->validLength($number, $type) && $this->validLuhn($number, $type));
    }

    private function validPattern($number, $type) {
        return preg_match($this->cards[$type]['pattern'], $number);
    }

    public function validLength($number, $type) {
        foreach ($this->cards[$type]['length'] as $length) {
            if (strlen($number) == $length) {
                return true;
            }
        }

        return false;
    }

    public function validCvc($cvc, $type) {
        return (ctype_digit($cvc) && array_key_exists($type, $this->cards) && $this->validCvcLength($cvc, $type));
    }

    public function validCvcLength($cvc, $type) {
        foreach ($this->cards[$type]['cvcLength'] as $length) {
            if (strlen($cvc) == $length) {
                return true;
            }
        }

        return false;
    }

    public function validLuhn($number, $type) {
        if (! $this->cards[$type]['luhn']) {
            return true;
        } else {
            return $this->luhnCheck($number);
        }
    }

    public function luhnCheck($number) {
        $checksum = 0;
        for ($i=(2-(strlen($number) % 2)); $i<=strlen($number); $i+=2) {
            $checksum += (int) ($number{$i-1});
        }

        // Analyze odd digits in even length strings or even digits in odd length strings.
        for ($i=(strlen($number)% 2) + 1; $i<strlen($number); $i+=2) {
            $digit = (int) ($number{$i-1}) * 2;
            if ($digit < 10) {
                $checksum += $digit;
            } else {
                $checksum += ($digit-9);
            }
        }

        if (($checksum % 10) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function directCreditCardPayment() {

        $xmlRequest = '<?xml version="1.0" encoding="UTF-8"?>
        <SecurePayMessage>
            <MessageInfo>
                <messageID>'. $this->getMessageId() .'</messageID>
                <messageTimestamp>'. $this->getMessageId() .'</messageTimestamp>
                <timeoutValue>'. $this->getTimeoutValue() .'</timeoutValue>
                <apiVersion>xml-4.2</apiVersion>
            </MessageInfo>
            <MerchantInfo>
                <merchantID>'. $this->merchantId .'</merchantID>
                <password>'. $this->txnPassword .'</password>
            </MerchantInfo>
            <RequestType>Payment</RequestType>
            <Payment>
                <TxnList count="1">
                    <Txn ID="1">
                        <txnType>0</txnType>
                        <txnSource>23</txnSource>
                        <amount>'. $this->getAmount() .'</amount>
                        <recurring>no</recurring>
                        <currency>'. $this->getCurrency() .'</currency>
                        <purchaseOrderNo>'. $this->getPurchaseOrderNo() .'</purchaseOrderNo>
                        <CreditCardInfo>
                            <cardNumber>'. $this->getCardNumber() .'</cardNumber>
                            <expiryDate>'. $this->getExpiryMonth() .'/'. $this->getExpiryYear() .'</expiryDate>
                            <cvv>'. $this->getCCV() .'</cvv>
                        </CreditCardInfo>
                    </Txn>
                </TxnList>
            </Payment>
        </SecurePayMessage>';
        return $this->sendRequest( 'payment', $xmlRequest );
    }

    public function createCreditCardToken() {
        $xmlRequest = '<?xml version="1.0" encoding="UTF-8"?>
        <SecurePayMessage>
            <MessageInfo>
                <messageID>'. $this->getMessageId() .'</messageID>
                <messageTimestamp>'. $this->getMessageId() .'</messageTimestamp>
                <timeoutValue>'. $this->getTimeoutValue() .'</timeoutValue>
                <apiVersion>xml-4.2</apiVersion>
            </MessageInfo>
            <MerchantInfo>
                <merchantID>'. $this->merchantId .'</merchantID>
                <password>'. $this->txnPassword .'</password>
            </MerchantInfo>
            <RequestType>addToken</RequestType>
            <Token>
                <TokenList count="1">
                    <TokenItem ID="1">
                        <cardNumber>'. $this->getCardNumber() .'</cardNumber>
                        <expiryDate>'. $this->getExpiryMonth() .'/'. $this->getExpiryYear() .'</expiryDate>
                        <tokenType>1</tokenType>
                        <amount>'. $this->getAmount() .'</amount>
                        <transactionReference>'. $this->getPurchaseOrderNo() .'</transactionReference>
                    </TokenItem>
                </TokenList>
            </Token>
        </SecurePayMessage>';
        return $this->sendRequest( 'token', $xmlRequest );
    }

    /**
     * Sends a request to a URL
     *
     * @param string $url The url which the request will be sent to
     * @param null|string|array $postdata The post data sent to the server. Can either be empty, a string (post body) or an array for a form post.
     * @return string The response from the server
     * @throws \Exception When a cURL exception occurs.
     */
    public function sendRequest( $method, $postdata = null ) {

        $url = $this->apiUrl() . $method;
        $ch = curl_init();
        $response = null;
        try {
            curl_setopt( $ch, CURLOPT_URL, $url );
            curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
            curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
            if ( $postdata != null ) {
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt( $ch, CURLOPT_POSTFIELDS, $postdata );
            }

            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, $this->verifySSL ? 2 : 0 );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, $this->verifySSL ? true : false );
            curl_setopt( $ch, CURLOPT_CAINFO, plugin_dir_path( __FILE__ ) . 'cacert.pem' );
            $result = curl_exec( $ch );
            $curl = array(
               "curl_error"    =>  curl_error($ch),
               "curl_errno"    =>   curl_errno($ch)
            );
            $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            $response = array(
                'result' => $result,
                'curl' => $curl,
                'status' => $status
            );

        } catch (Exception $e) {
        	echo 'sendRequest error';
            print_r($e);
        }
        curl_close($ch);

        return $response;
    }
}


?>