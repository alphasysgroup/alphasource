/**
* jQuery
*/
var j = jQuery;

var SecurepayGateway = function( config ) {
	this.config = config;
	j('#pdwp-btn').show();

	j('#wfc_donation-payment-details-toogle-container').show();
	j("label[for=payment_type_credit_card]").click();

};

SecurepayGateway.prototype.clean = function( cc ) {

	var creditcarddetails = {
		card_number: cc.card_number,
		name_on_card: cc.name_on_card,
		expiry_month: cc.expiry_month,
		expiry_year: cc.expiry_year,
		ccv: cc.ccv
	};

	var regex = /\d(?=\d{4})/mg;
	var str = creditcarddetails.card_number.replace(/ /g,'');
	var subst = '*';

	creditcarddetails.card_number = str.replace(regex, subst);
	creditcarddetails.ccv = creditcarddetails.ccv.replace(/\S/gi, '*');

	return creditcarddetails;
};

SecurepayGateway.prototype.formatDate = function( date ) {
	var d = new Date(date),
	month = '' + (d.getMonth() + 1),
	day = '' + d.getDate(),
	year = d.getFullYear();

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [year, month, day].join('-');
};

SecurepayGateway.prototype.cleanBank = function( bank ) {
  var bank_details = {
      bank_code: ( typeof bank.bank_code != 'undefined' ? bank.bank_code : '' ),
      bank_number: ( typeof bank.bank_number != 'undefined' ? bank.bank_number : '' ),
      bank_name: ( typeof bank.bank_name != 'undefined' ? bank.bank_name : '' )
  };

  var regex = /\d(?=\d{4})/mg;
    var strbank = bank_details.bank_number.replace(/ /g,'');
    var strcode = bank_details.bank_code.replace(/ /g,'');
    var subst = 'x';

    bank_details.bank_code = strcode.replace(regex, subst);
    bank_details.bank_number = strbank.replace(regex, subst);
    return bank_details;
};

SecurepayGateway.prototype.creditCardPayment = function( param, cc ) {
	var self = this;
	return new Promise(function ( resolve, reject ) {
		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'wfc_securepay_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				process: 'creditcard',
				creditcard: cc
			},
			success: function( resp ) {
				if( resp.status == 'success' ) {

					if( resp.response.Status.statusCode == '000' ) {

						var donate = param.donation;
						var paymentDate = new Date( donate.timestamp * 1000 );

						donate.payment_response = resp.response;

						if( resp.response.Payment.TxnList.Txn.approved == 'Yes' ) {
							donate.statusCode = 1;
							donate.statusText = 'APPROVED';
						} else {
							if( resp.response.Payment.TxnList.Txn.responseText == 'Pending' ) {
								donate.statusCode = 1;
								donate.statusText = 'PENDING';
							} else {
								donate.statusCode = 0;
								donate.statusText = 'FAILED';
							}
						}

						donate.PaymentTransaction = {
							TxnStatus : ( donate.statusCode == 1 ) ? 'Success' : 'Failed',
							TxnId : resp.response.Payment.TxnList.Txn.txnID,
							TxnDate : self.formatDate( paymentDate ),
							TxnMessage : resp.response.Payment.TxnList.Txn.responseText + '-' + donate.pamynet_ref,
							TxnPaymentRef : donate.pamynet_ref
						};

						donate.card_details = self.clean( cc );

						resolve( {
							status : 'success',
							response : resp.response,
							donation : donate
						} );

					} else {

						resolve( {
							status : 'error',
							response : self.TranslateServerCode( resp.response.Status ),
							donation : param.donation
						} );
					}

				} else {
					resolve( {
						status : 'error',
						response : resp.errors,
						donation : param.donation
					} );
				}
			},
			error: function( resp ) {
				reject( resp );
			}
		});
	});
};

SecurepayGateway.prototype.tokenCustomerPayment = function( param, cc, token ) {
	var self = this;
 	return new Promise(function ( resolve, reject ) {
 		j.ajax({
			type: 'POST',
			url:  pdwp_ajax.url,
			data: {
				'action':'pdwp_securepay_payment',
				param : param,
				nonce: pdwp_ajax.nonce,
				process: 'token',
				creditcard: cc
			},
			success: function( resp ) {
				console.log( resp )
			},
			error: function( resp ) {
				reject( resp );
			}
		});
 	});
};

SecurepayGateway.prototype.TranslateServerCode = function( spResp ) {

	var desc = spResp.statusDescription;
	var code = spResp.statusCode;

	switch( code ) {
		case 000:
			return '';
		case 504:
			return 'We are currently experiencing technical difficulties (Error: Credential failure with merchant ID). Please try again later';
		case 505:
			return 'We are currently experiencing technical difficulties (Error: Invalid SecurePay URL). Please try again later';
		case 510:
			return 'The credit card processor is currently experiencing difficulties. Please try again later';
		case 512:
			return 'The credit card processor is currently experiencing difficulties. Please try again later';
		case 513:
		case 514:
		case 515:
		case 545:
			return 'The credit card processor is currently experiencing difficulties. Please try again later';
		case 516:
		case 517:
		case 518:
		case 575:
		case 577:
		case 580:
			return 'We are currently experiencing technical difficulties (Error: XML Processing Fault). Please try again later';
		case 524:
			return 'We are currently experiencing technical difficulties (Error: Connection fault). Please try again later';
		case 550:
			return 'We are currently experiencing technical difficulties (Error: Credential failure with password). Please try again later';
		case 595:
			return 'Credit card declined';
		default:
			return desc;
	}
};

SecurepayGateway.prototype.processPayment = function( param, details ) {
	var self = this;
 	return new Promise( function( resolve, reject ) {

 		if( typeof param.donation.payment_type != 'undefined' && param.donation.payment_type == 'bank' ) {

	      var donate = param.donation;
	      var paymentDate = new Date( donate.timestamp * 1000 );

	      donate.payment_response = {};
	      donate.statusCode = 1;
	      donate.statusText = 'PENDING';

	      donate.PaymentTransaction = {
	        TxnStatus : 'Success',
	        TxnId : '',
	        TxnDate : self.formatDate( paymentDate ),
	        TxnMessage : '',
	        TxnPaymentRef : donate.pamynet_ref
	      };

	      donate.card_details = self.cleanBank( details );

	      resolve( {
	        status : 'success',
	        response : {},
	        donation : donate
	      } );

	    } else {  

			self.creditCardPayment( param, details )
			.then(function( result ){
				resolve( result );
			})
			.catch(function( error ){
				reject( error );
			});
		}
 	});
};
