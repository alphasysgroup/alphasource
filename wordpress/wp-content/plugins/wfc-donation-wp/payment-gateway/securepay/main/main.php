<?php 

use SecurePay\XMLAPI\Requests\Payment\PaymentRequest;
use SecurePay\XMLAPI\Requests\Periodic\PeriodicRequest;

/**
* Securepay Wordpress Hooks
*/


class Securepay_WP_Hooks
{
	/**
	 * The Securepay main class
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $parent    The Securepay main class
	 */
	public $parent = null;

	/**
	 * The main class of the plugin
	 *
	 * @since    5.0.0
	 * @access   private
	 * @var      string    $includes    The main class of the plugin
	 */
	public $includes = null;
	
	/**
	 * Securepay_WP_Hooks constructor.
	 *
	 * @param $a
	 * @param $b
	 */
	function __construct( $a, $b )
	{
		/*
		 * The Securepay main class
		 */
		$this->parent = $a;

		/*
		 * The main class of the plugin
		 */
		$this->includes = $b;

		/*
		 * Securepay scripts
		 */
		add_action( 'wp_enqueue_scripts', array( $this, 'wfc_load_securepayscripts' ) );

		/*
		 * Securepay oauth
		 */
		add_action( 'wp_ajax_wfc_securepay_payment', array( $this, 'wfc_securepay_payment' ) );
		add_action( 'wp_ajax_nopriv_wfc_securepay_payment', array( $this, 'wfc_securepay_payment' ) );
	}
	
	
	/**
	 * Pin load js libraries
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @return void
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 6, 2018
	 */
	public function wfc_load_securepayscripts() {
		global $post;

		//pdwp pin js
		wp_register_script(
			'webforce-connect-donation-securepay_js',
			plugin_dir_url( __FILE__ ) . '../js/securepay.js',
			array(),
			4.0,
			true
		);	
			
	}
	
	/**
	 * Securepay register ajax endpoint for payment
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 * @return void
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 6, 2018
	 */
	public function wfc_securepay_payment() {
		if( isset( $_POST['action'] ) && $_POST['action'] == 'wfc_securepay_payment' ) {

 			/*
			 * break if direct call enpoint
			 */
 			if( ! wp_verify_nonce( $_POST['nonce'], 'ajax-nonce' ) ) {
				die('busted');
			}

			/*
			 * load secure pay class library
			 */
			require_once( plugin_dir_path( __FILE__ ) . '../lib/securepay.php' );

			/*
			 * donate
			 */
			$donate = $_POST['param']['donation'];

			/*
			 * creditcard details
			 */
			$creditcard = $_POST['creditcard'];

			/*
			 * creditcard details
			 */
			$gateway = $this->wfc_get_sp_config( $donate );

			if( $_POST['process']  == 'creditcard' ) {

				$response = $this->wfc_creditcard_payment( $donate, $gateway, $creditcard );

			} else {

				$response = $this->pdwp_add_sp_payor( $donate, $gateway, $creditcard );
			}

			/*
			 * send securepay response
			 */
			wp_send_json( $response );
		}
	}
	
	/**
	 * Fetch payment gateway settings.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $donate
	 *
	 * @return array
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 7, 2018
	 */
	public function wfc_get_sp_config( $donate ) {

		/*
		 * get gateway
		 */
		$gateway = get_option($donate['gatewayid']);

		return array(
			'sp_merchat_id' => isset($gateway['wfc_donation_securepaymerchantid']) ? $gateway['wfc_donation_securepaymerchantid'] : '',

			'sp_txn_password' => isset($gateway['wfc_donation_securepaytranspass']) ? $gateway['wfc_donation_securepaytranspass']: '',

			'sp_mode' => (isset($gateway['wfc_donation_sandboxmode']) && $gateway['wfc_donation_sandboxmode'] == 'true') ? true : false
		);
	}
	
	/**
	 * Process credit card payment.
	 *
	 * @author   Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $donate
	 * @param $gateway
	 * @param $creditcard
	 *
	 * @return array
	 * @since    5.0.0
	 *
	 * @LastUpdated  June 7, 2018
	 */
	public function wfc_creditcard_payment( $donate, $gateway, $creditcard ) {

		/*
         * ref number
         */
        $ref_no = isset( $donate['pamynet_ref'] ) ? $donate['pamynet_ref'] : '';

        /*
         * donation form currency
         */
        $currency = explode( ',', $donate['currency'] );

        /*
         * donation amt
         */
        $amount = number_format( $donate['pdwp_sub_amt'], 2, ".", "" );

        try {

	        /*
			 * initialize securepay payment class
			 */
			$securepay = new SecurePay_Api(
				$gateway['sp_merchat_id'],
				$gateway['sp_txn_password'],
				$gateway['sp_mode']
			);

			$securepay->setAmount( $amount );
			$securepay->setCurrency( $currency[1] );
			$securepay->setPurchaseOrderNo( $ref_no );
			$securepay->setCardNumber( str_replace(' ', '', $creditcard['card_number'] ) );
			$securepay->setExpiryMonth( $creditcard['expiry_month'] );
			$securepay->setExpiryYear( $creditcard['expiry_year'] );
			$securepay->setCCV( $creditcard['ccv'] );

			$response = $securepay->directCreditCardPayment();

			if( isset( $response['status'] ) && $response['status'] == 200 ) {
				return array(
					'status' => 'success',
					'response' => simplexml_load_string( $response['result'] )
				);
			} else {
				return array(
					'status' => 'error',
					'errors' => $response['curl']['curl_error']
				);
			}

        } catch( Exception $e ) {

        	return array(
        		'status' => 'error',
        		'errors' => $e->getMessage()
        	);
        }
	}
	
	/**
	 * Sets the Payer detail of the donor.
	 *
	 * @param $donate
	 * @param $gateway
	 * @param $creditcard
	 *
	 * @return array
	 */
	public function pdwp_add_sp_payor( $donate, $gateway, $creditcard ) {

		/*
         * ref number
         */
        $ref_no = isset( $donate['pamynet_ref'] ) ? $donate['pamynet_ref'] : '';

        /*
         * donation amt
         */
        $amount = number_format( $donate['pdwp_sub_amt'], 2, ".", "" );

        try {

	        /*
			 * initialize securepay payment class
			 */
			$securepay = new SecurePay_Api(
				$gateway['sp_merchat_id'],
				$gateway['sp_txn_password'],
				$gateway['sp_mode']
			);

			$securepay->setAmount( $amount );
			// $securepay->setCurrency( $currency[1] );
			$securepay->setPurchaseOrderNo( $ref_no );
			$securepay->setCardNumber( str_replace(' ', '', $creditcard['card_number'] ) );
			$securepay->setExpiryMonth( $creditcard['expiry_month'] );
			$securepay->setExpiryYear( $creditcard['expiry_year'] );
			$securepay->setCCV( $creditcard['ccv'] );

			$response = $securepay->createCreditCardToken();

			if( isset( $response['status'] ) && $response['status'] == 200 ) {
				return array(
					'status' => 'success',
					'response' => simplexml_load_string( $response['result'] )
				);
			} else {
				return array(
					'status' => 'error',
					'errors' => $response['curl']['curl_error']
				);
			}

        } catch( Exception $e ) {
        	
        	return array(
        		'status' => 'error',
        		'errors' => $e->getMessage()
        	);
        }
	}
}