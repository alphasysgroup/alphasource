<?php 

	// If this file is called directly, abort.
	if ( ! defined( 'WPINC' ) ) {
		die;
	}

	if ( ! class_exists( 'Securepay' ) ) {
		
		/**
		 * Class Securepay
		 */
		class Securepay
		{
			public $restricted = array(
				'wfc_donation_securepaytranspass'
			);

			/**
			 * The registered scripts id
			 *
			 * @since    5.0.0
			 * @access   public
			 * @var      string    $registerd_scripts    The main class of the plugin
			 */
			public $registerd_scripts = array(
				'webforce-connect-donation-securepay_js'
			);
			
			/**
			 * Securepay constructor.
			 */
			function __construct()
			{

			}
			
			/**
			 * Load securepay settings template.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $data , settings data
			 *
			 * @return mixed
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 7, 2018
			 */
			public function loadSettingsTemplate( $data ) {
				return $data['class']->pdwp_view_tmpl( plugin_dir_path( __FILE__ ) . 'tmpl/settings.php', $data );
			}
			
			/**
			 * Load securepay wp core hooks.
			 *
			 * @author   Junjie Canonio <junjie@alphasys.com.au>
			 *
			 * @param $class , plugin main file
			 *
			 * @return void
			 * @since    5.0.0
			 *
			 * @LastUpdated  June 7, 2018
			 */
			public function loadWpCore( $class ) {
				require_once( plugin_dir_path( __FILE__ ) . 'main/main.php' );
				new Securepay_WP_Hooks( $this, $class );
			}
		}
	}