<?php
wp_enqueue_style( 'wfc-donation-payment-gateway-css' );

$gatewayID = isset($_GET['gatewayID']) ? $_GET['gatewayID'] : '';
$gateway = isset($_GET['gateway']) ? strtolower($_GET['gateway']) : '';
$gatewaysfinstanceid = isset($_GET['gatewaysfinstanceid']) ? $_GET['gatewaysfinstanceid'] : '';
$backurl = admin_url( 'admin.php?page=wfc-donation&sfinstanceID='.$gatewaysfinstanceid);
$pronto_donation_gateway_list = get_option('WFCDON_gateway_list_'.$gatewaysfinstanceid);

$gateway_name = '';

$wfc_donation_sandboxmode = 'off';
$wfc_donation_bankmode = 'off';
$wfc_donation_tokenize = 'off';

foreach ($pronto_donation_gateway_list as $key => $value) {
	if(isset($value->Id) && $value->Id == $gatewayID){
		$gateway_name = isset($value->Name) ? $value->Name : $gateway.'_'.$gatewayID;

		$wfc_donation_sandboxmode =
			(isset($value->ASPHPP__isSandbox__c) && $value->ASPHPP__isSandbox__c == true) ? 'on' : 'off';

		$wfc_donation_bankmode =
			(isset($value->ASPHPP__Bank__c) && $value->ASPHPP__Bank__c == true) ? 'on' : 'off';

		$wfc_donation_tokenize =
			(isset($value->ASPHPP__Tokenization_Payment__c) && $value->ASPHPP__Tokenization_Payment__c == true) ? 'on' : 'off';

		$Name = isset($value->Name) ? $value->Name : 'eWay';
		$DeveloperName = isset($value->RecordType->DeveloperName) ? $value->RecordType->DeveloperName : 'eWay';
	}
}

if(isset($_GET['reset']) && $_GET['reset'] == '1'){
	delete_option($gatewaysfinstanceid.'_'.$gateway.'_'.$gatewayID);
}
$gateway_settings = get_option($gatewaysfinstanceid.'_'.$gateway.'_'.$gatewayID);

$wfc_donation_publish = isset($gateway_settings['wfc_donation_publish']) ? $gateway_settings['wfc_donation_publish'] : 'false';
$wfc_donation_publish = ($wfc_donation_publish == 'true') ? 'on' : 'off';
$wfc_donation_showlogo = isset($gateway_settings['wfc_donation_showlogo']) ? $gateway_settings['wfc_donation_showlogo'] : 'false';
$wfc_donation_showlogo = ($wfc_donation_showlogo == 'true') ? 'on' : 'off';
$wfc_donation_logo = isset($gateway_settings['wfc_donation_logo']) ? $gateway_settings['wfc_donation_logo'] : '';

$wfc_donation_gatewaylabel = isset($gateway_settings['wfc_donation_gatewaylabel']) ? $gateway_settings['wfc_donation_gatewaylabel'] : $gateway_name;
$wfc_donation_salesforceID = isset($gateway_settings['wfc_donation_salesforceID']) ? $gateway_settings['wfc_donation_salesforceID'] : '';
$wfc_donation_securepaymerchantid = isset($gateway_settings['wfc_donation_securepaymerchantid']) ? $gateway_settings['wfc_donation_securepaymerchantid'] : '';
$wfc_donation_securepaytranspass = isset($gateway_settings['wfc_donation_securepaytranspass']) ? $gateway_settings['wfc_donation_securepaytranspass'] : '';

$children = array(
	'wfc_donation_sandboxmode' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'wfc_donation_sandboxmode',
			'class'    => 'wfc_donation_sandboxmode',
			'name'     => 'wfc_donation_sandboxmode',
			'switch'   => $wfc_donation_sandboxmode,
			'gridclass'=> 'col-6',
			'required' => false,
			'readonly' => true,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Sandbox Mode',
				'position'  => 'top',
			),
		)
	),
	'wfc_donation_publish' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'wfc_donation_publish',
			'class'    => 'wfc_donation_publish',
			'name'     => 'wfc_donation_publish',
			'switch'   => $wfc_donation_publish,
			'gridclass'=> 'col-6',
			'required' => false,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Publish',
				'position'  => 'top',
			),
		)
	),
	'wfc_donation_showlogo' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'wfc_donation_showlogo',
			'class'    => 'wfc_donation_showlogo',
			'name'     => 'wfc_donation_showlogo',
			'switch'   => $wfc_donation_showlogo,
			'gridclass'=> 'col-6',
			'required' => false,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Show Logo',
				'position'  => 'top',
			),
		)
	),
	'wfc_donation_logo' => array(
		'type' => 'media',
		'settings' => array(
			'id' => 'wfc_donation_logo',
			'class' => 'wfc_donation_logo',
			'name' => 'wfc_donation_logo',
            'gridclass'=> 'col-6',
			'label' => array(
				'value' => 'Logo'
			),
			'multiple' => false,
			'value' => $wfc_donation_logo,
			'dependency' => array(
				'id' => 'wfc_donation_showlogo',
				'trigger_value' => 'on',
				'trigger_action' => 'access'
			)
		)
	),
	'wfc_donation_bankmode' => array(
		'type' => 'switch',
		'settings' => array(
			'id'        => 'wfc_donation_bankmode',
			'class'     => 'wfc_donation_bankmode',
			'name'      => 'wfc_donation_bankmode',
			'switch' => $wfc_donation_bankmode,
			'gridclass'=> 'col-6',
			'required' => false,
			'readonly' => true,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Bank Mode',
				'position'  => 'top',
			),
		)
	),
	'wfc_donation_tokenize' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'wfc_donation_tokenize',
			'class'    => 'wfc_donation_tokenize',
			'name'     => 'wfc_donation_tokenize',
			'switch'   => $wfc_donation_tokenize,
			'gridclass'=> 'col-6',
			'required' => false,
			'readonly' => true,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Token Payment',
				'position'  => 'top',
			),
		)
	),
	'wfc_donation_gatewaylabel' => array(
        'type'    => 'input',
		'settings'  => array(
			'type'  => 'text',
			'id'    => 'wfc_donation_gatewaylabel',
			'class' => 'wfc_donation_gatewaylabel',
			'name'  => 'wfc_donation_gatewaylabel',
			'style' => '',
			'value' => $wfc_donation_gatewaylabel,
			'gridclass'=> 'col-6',
			'placeholder' => '',
			'required' => true,
			'label' => array(
				'hidden'       => false,
				'value' => 'Gateway Label',
				'position' => 'top',
			),
		),
    ),	
	'wfc_donation_salesforceID' => array(
		'type'    => 'input',
		'settings'  => array(
			'type'  => 'text',
			'id'    => 'wfc_donation_salesforceID',
			'class' => 'wfc_donation_salesforceID',
			'name'  => 'wfc_donation_salesforceID',
			'style' => '',
			'value' => $gatewayID,
			'gridclass'=> 'col-6',
			'placeholder' => '',
			'required' => true,
			'label' => array(
				'hidden'       => false,
				'value'    => 'Salesforce Gateway ID *',
				'position' => 'top',
			),
		),
	),
	'wfc_donation_securepaymerchantid' => array(
		'type'    => 'input',
		'settings'  => array(
			'type'  => 'text',
			'id'    => 'wfc_donation_securepaymerchantid',
			'class' => 'wfc_donation_securepaymerchantid',
			'name'  => 'wfc_donation_securepaymerchantid',
			'style' => '',
			'value' => $wfc_donation_securepaymerchantid,
			'gridclass'=> 'col-6',
			'placeholder' => '',
			'required' => true,
			'label' => array(
				'hidden'       => false,
				'value'    => 'Secure Pay Merchant ID *',
				'position' => 'top',
			),
		),
	),
	'wfc_donation_securepaytranspass' => array(
		'type'    => 'input',
		'settings'  => array(
			'type'  => 'password',
			'id'    => 'wfc_donation_securepaytranspass',
			'class' => 'wfc_donation_securepaytranspass',
			'name'  => 'wfc_donation_securepaytranspass',
			'style' => '',
			'value' => $wfc_donation_securepaytranspass,
			'gridclass'=> 'col-6',
			'placeholder' => '',
			'required' => true,
			'label' => array(
				'hidden'       => false,
				'value'    => 'Secure Pay Trans Pass *',
				'position' => 'top',
			),
		),
	),
	'wfc_donation_separator' => array(
		'type' => 'separator',
		'settings' => array(
			'id' => 'wfc_donation_separator',
			'gridclass' => 'col-12',
			'visibility' => 'hidden'
		)
	),
	'wfc_donation_gatewaysettings_btn' => array(
		'type' => 'button',
		'settings' => array(
			'id'    => 'wfc_donation_gatewaysettings_btn',
			'class' => 'wfc_donation_gatewaysettings_btn',
			'name'  => 'wfc_donation_gatewaysettings_btn',
			'type'  => 'button',
			'style' => 'float:left;margin-top:20px;',
			'gridclass'=> 'col-12',
			'label'     => array(
				'hidden'    => true,
				'value'     => '',
				'position'  => 'top',
			),
			'content' => 'SAVE'
		)
	),
	'wfc_donation_back_btn' => array(
		'type' => 'button',
		'settings' => array(
			'id' => 'wfc_donation_back_btn',
			'name' => 'wfc_donation_back_btn',
			'style' => 'float:left;margin-top:20px;',
			'label' => array(
				'hidden' => true
			),
			'content' => 'BACK'
		)
	)
);

Saiyan::tist()->render_module_mapping(array(
	'wfc_donation_gatewaysettings' => array(
		'type' => 'settingsform',
		'settings' => array(
			'id'        => 'wfc_donation_gatewaysettings',
			'class'     => '',
			'action'    => '',
			'name'      => 'wfc_donation_gatewaysettings',
			'style'     => 'width: 100%;',
			'title'     => $gateway_name,
			'action'    => '',
			'method'    => 'post',
			'wp_option' => array(
				'enable'     => true,
				'slug_name'  => $gatewaysfinstanceid.'_'.$gateway.'_'.$gatewayID,
			),
			'submit_id' => 'wfc_donation_gatewaysettings_btn',
		),
		'children' => $children,
	),
));
?>
<input type="hidden" id="wfc-don-back-url" value="<?php echo esc_url($backurl); ?>">
<script>
    /**
     * @author: Junjie Canonio
     */
    jQuery(document).ready(function($){
        // go back to payment gateway lists
        $('#wfc_donation_back_btn').on('click', function() {
            var backbuttonurl = $('#wfc-don-back-url').val();
            window.location.replace(backbuttonurl);
        });
    });
</script>
<?php if ($wfc_donation_tokenize != 'off') : ?>
	<div class="wfc-warn-container">
		<div class="wfc-modal">
			<h5 class="wfc-modal-title">
				<div class="wfc-warn-icon">
					<?php require_once WFC_PLUGIN_BASEDIR . 'svg/webforce-connect-donation-donation-warn-exclamation.php'; ?>
				</div>
				<div class="wfc-warn-title">Warning</div>
			</h5>
			<div class="wfc-modal-body">
				<span>This payment gateway requires Token Payment disabled.</span>
			</div>
		</div>

	</div>
<?php endif; ?>