<?php
/**
* @author: Junjie Canonio
*/

wp_enqueue_style( 'wfc-donation-payment-gateway-css' );
wp_enqueue_script( 'wfc-donation-payment-gateway-js' );
?>
<?php if(isset($_GET['sfinstanceID']) && !empty($_GET['sfinstanceID'])):?>
<div id="wfc-don-gateway-list-container" class="wfc-don-gateway-list-container" sfinstanceID="<?php echo $_GET['sfinstanceID']; ?>">
  <div class="container-fluid wfc-don-gateway-list-item-title">
    <div class="row">
      <div class="col-6">
        <span><?php echo __('Name', 'webforce-connect-donation'); ?></span>
      </div>
      <div class="col-1">
        <span><?php echo __('Live', 'webforce-connect-donation'); ?></span>
      </div>
      <div class="col-2">
        <span><?php echo __('Tokenize', 'webforce-connect-donation'); ?></span>
      </div>
      <div class="col-2">
        <span><?php echo __('Logo', 'webforce-connect-donation'); ?></span>
      </div>
    </div>
  </div>
  <ul id="wfc-don-gateways-easyPaginate" class="list-group">

  </ul>
</div>
<div>
	<?php
	Saiyan::tist()->render_module_mapping(array(
		'wfc-don-resync-gateway-btn' => array(
			'type' => 'button',
			'settings' => array(
				'id'    => 'wfc-don-resync-gateway-btn',
				'class' => '',
				'name'  => 'wfc-don-resync-gateway-btn',
				'style' => 'margin:0px 20px 10px 20px;',
				'label'     => array(
					'hidden'    => true,
					'value'     => '',
					'position'  => 'top',
				),
				'content' => '<i id="wfc-don-resync-gateway-icon" class="fa fa-refresh" style="margin-right: 8px;"></i>Resync Gateways'
			)
		),
	));
	?>
</div>
<?php else: ?>

<?php endif; ?>




