<?php
//author Junjie Canonio
?>
<?php if( !isset( $_GET['action'] ) ): ?>
	<div id="checkSFConnection-container"></div>
	<button type="button" id="PD-sfconnection-button" name="PD-sfconnection-button" hidden></button>
	<h2><?php echo __('Donation List', 'webforce-connect-donation'); ?></h2>
	<style type="text/css">
        th, th a{
            color: #ffffff !important;
            background-color: #1497c1 !important;
            font-weight: 600 !important;
        }

        th .sorting-indicator:before{
            color: #ffffff !important;
        }

        tfoot{
            display: none !important;
        }

        .column-data_version {
            width: 5%;
        }

        .column-data_version .sorting-indicator:before{
            display: none;
        }

		.column-title {
			width: 18%;
		}

		.column-donation_form {
			width: 20%;
		}

		.column-donation_type {
			width: 12%;
		}

		.column-transaction_status {
			width: 15%;
		}

		.column-sync_status {
			width: 15%;
		}

		.column-amount {
			width:100px; 
		}

		.column-date {
			width:100px; 
		}

		.bulkactions {
			display: none;
		}
	</style>
	<script type="text/javascript">
		jQuery(document).ready(function($){
			if($('.no-items').length) {
				$('.no-items td').text('No Donations found.');
			}
		});
	</script>
	<div class="wrap">
        <form method="get">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>" />
            <?php $tableList->search_box('Search Table', 'your-element-id'); ?>
        </form>
		<?php echo $tableList->prepare_items(); ?>
		<?php echo $tableList->display(); ?>
	</div>

	<div id="wfc-donation-list-info" class="wfc-donation-list-info">
        <div class="pins">
            <div class="info-loading-icon">
		        <?php require_once( WFC_PLUGIN_BASEDIR . 'svg/webforce-connect-donation-donation-list-info.php' ); ?>
            </div>
        </div>
        <div class="notes">
            <div class="info-list-container show">
                <h4 class="info-header"><?php echo __('Status Information', 'webforce-connect-donation'); ?></h4>
                <div class="info-content">
                    <div class="row">
                        <div class="wfc-donation-trans-status-cont col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="wfc-donation-trans-status-title"><h4>Transaction Status</h4></div>
                            <div>
			        			<div class="transaction-status-wrap"><span class="transaction-status-approved"></span> <?php echo __('APPROVED', 'webforce-connect-donation'); ?></div>
			        			<span style="font-size: 12px;"> - <?php echo __('Payment transaction is a success payment. This donation will be synced to Salesforce as close won.', 'webforce-connect-donation'); ?></span>
			        			<div class="transaction-status-wrap"><span class="transaction-status-bank"></span> <?php echo __('BANK', 'webforce-connect-donation'); ?></div>
			        			<span style="font-size: 12px;"> - <?php echo __('Payment transaction is a bank type payment. This type of donation is directly synced to Salesforce.', 'webforce-connect-donation'); ?></span>
			        			<div class="transaction-status-wrap"><span class="transaction-status-failed"></span> <?php echo __('FAILED', 'webforce-connect-donation'); ?></div>
			        			<span style="font-size: 12px;"> - <?php echo __('Payment transaction is a failed payment. This donation will synced to Salesforce as close lost.', 'webforce-connect-donation'); ?></span>
			        			<div class="transaction-status-wrap"><span class="transaction-status-pending"></span> <?php echo __('VOID', 'webforce-connect-donation'); ?></div>
			        			<span style="font-size: 12px;"> - <?php echo __('Payment transaction is still a pending payment or the payment is completely empty. No transaction has been made through the whole donation process. This is usually what happens when a certain donor reloads the donation page while transaction is still in process. This donation is not allowed to be synced to Salesforce because there is no transaction made.', 'webforce-connect-donation'); ?></span>
			        		</div>
                        </div>
                        <div class="wfc-donation-trans-status-cont col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="wfc-donation-sync-status-title"><h4><?php echo __('Sync Status', 'webforce-connect-donation'); ?></h4></div>
                            <div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-success-sync"><?php echo __('SUCCESS', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('Donation has been synced successfully to Salesforce via PPAPI.', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> <?php echo __('<b>Note:</b> All donations that has been synced successfully cannot be re-synced.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-partial-sync"><?php echo __('Recurring Partial Sync', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('Donation is a recurring donation and has two parts of syncing. The initial syncing of the recurring donation and the syncing of initial single donation.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-attemp-reach"><?php echo __('LIMIT REACHED', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('Donation re-sync limitation has been reached.', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> <?php echo __('<b>Note:</b> This needs to be reported to the developers in order to be synced successfully. The donation plugin has an automated synchronous syncing of failed sync and not sync donations. This sync limit functionality is being used to avoid hitting the Salesforce API call limit.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-failed-sync"><?php echo __('FAILED', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('Donation has not been synced successfully to Salesforce via PPAPI. This donation can be re-synced manually or can be re-synced automatically by the synchronous syncing functionality of the donation plugin.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-recurring-failed-sync"><?php echo __('Recurring Failed Sync', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('This donation is a recurring donation that has not been synced successfully to Salesforce. This donation can be re-synced manually or can be re-synced automatically by the synchronous syncing functionality of the donation plugin.', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> <?php echo __('<b>Note:</b> Syncing a recurring donation has two parts,  first is sync as recurring donation and then sync as initial single donation.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-not-sync"><?php echo __('Recurring Failed Sync', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('Donation has not been synced to Salesforce yet.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-old-cannot-sync"><?php echo __('Old Data Failed Sync', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('This donation record is made vai older version of the Webforce Connect - Donation. This donation is not allowed to be re-synced to Salesforce in order to avoid data lost and Salesforce data error.', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> <?php echo __('<b>Note:</b> This donation needs to be reported to the developers and manually link this donation to a Salesforce record.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="donation-not-sync"><?php echo __('UN-SYNCED', 'webforce-connect-donation'); ?></div>
                                    <div class="wfc-donation-sync-status-desc"> - <?php echo __('Donation has not been synced to Salesforce yet.', 'webforce-connect-donation'); ?></div>
                                </div>
                            </div>
                        </div>
                        <div class="wfc-donation-trans-status-cont col-12 col-sm-12 col-md-12 col-lg-12">
                            <div class="wfc-donation-data-version-title"><h4><?php echo __('Data Version', 'webforce-connect-donation'); ?></h4></div>
                            <div>
                                <div style="padding-bottom: 20px;">
                                    <div class="wfc-donation-data-version-desc"> <img class="wfc-donation-data-version-img" src="<?php echo plugin_dir_url( __FILE__ ) . '../../images/version_indicator/ribbon_5.png'?>"/> - <?php echo __('Donation Data is taken from WFC - Donation Version 5.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="wfc-donation-data-version-desc"> <img class="wfc-donation-data-version-img" src="<?php echo plugin_dir_url( __FILE__ ) . '../../images/version_indicator/ribbon_4.png'?>"/> - <?php echo __('Donation Data is taken from WFC - Donation Version 4.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="wfc-donation-data-version-desc"> <img class="wfc-donation-data-version-img" src="<?php echo plugin_dir_url( __FILE__ ) . '../../images/version_indicator/ribbon_3.png'?>"/> - <?php echo __('Donation Data is taken from WFC - Donation Version 3.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="wfc-donation-data-version-desc"> <img class="wfc-donation-data-version-img" src="<?php echo plugin_dir_url( __FILE__ ) . '../../images/version_indicator/ribbon_2.png'?>"/> - <?php echo __('Donation Data is taken from WFC - Donation Version 2.', 'webforce-connect-donation'); ?></div>
                                </div>
                                <div style="padding-bottom: 20px;">
                                    <div class="wfc-donation-data-version-desc"> <img class="wfc-donation-data-version-img" src="<?php echo plugin_dir_url( __FILE__ ) . '../../images/version_indicator/ribbon_1.png'?>"/> - <?php echo __('Donation Data is taken from WFC - Donation Version 1.', 'webforce-connect-donation'); ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>	
<?php elseif( $_GET['action'] === 'view' ): ?>
	<?php require_once( plugin_dir_path( __FILE__ ) . '/webforce-connect-donation-donation-list-edit.php' ); ?>
<?php endif; ?>