<?php 
/**
* @author: Junjie Canonio
*/
//echo '<pre>'; print_r( _get_cron_array() ); echo '</pre>';
// wp_clear_scheduled_hook( 'wfc_don_cron_resyncdonation' );
// $schedule = wp_get_schedule( 'wfc_don_cron_resyncdonation' );
// print_r($schedule);
?>
<?php if(!isset($_GET['settings'])): ?>
<a class="wfc-don-menu-btn-link" href="<?php echo admin_url( 'admin.php?page=wfc-donation&settings=1' );?>">
  <div id="wfc-don-menu-btn" class="wfc-don-menu-btn">
    <div class="container-fluid">
      <div class="row">
        <div class="col-2 wfc-don-menu-btn-icon"><i class="fa fa-cogs"></i></div>
        <div class="col-10 wfc-don-menu-btn-desc">
          <h3><?php echo __('Settings', 'webforce-connect-donation'); ?></h3>
          <em><?php echo __('Configure donation settings and set all resource credentials.', 'webforce-connect-donation'); ?></em>
        </div>
      </div>
    </div>
  </div>
</a>  	
<?php else:
if(isset($_GET['errorpopup'])){
    if($_GET['errorpopup'] == 'on'){
	    update_option('pronto_donation_errorpopup', 'on');
    }else {
	    update_option('pronto_donation_errorpopup', 'off');
    }
}

if(isset($_GET['reset']) && $_GET['reset'] == 1){
	delete_option('pronto_donation_settings');
}

$country = WFC_DonationFormSettings::getCountryList();
array_unshift($country, Array(
    'value' => '',
    'title' => ''
));

foreach ($country as &$ctry) {
    $ctry['value'] = $ctry['title'];
}

$wfc_don_settings = get_option( 'pronto_donation_settings', 0 );

$donation_debug_mode = isset($wfc_don_settings->donation_debug_mode) ? $wfc_don_settings->donation_debug_mode : 'false';
$donation_debug_mode = ($donation_debug_mode == 'true') ? 'on' : 'off';
$cron_schedule = isset($wfc_don_settings->cron_schedule) ? $wfc_don_settings->cron_schedule : 'hourly';
$donation_dnsr_single = isset($wfc_don_settings->donation_dnsr_single) ? $wfc_don_settings->donation_dnsr_single : 'true';
$donation_dnsr_single = ($donation_dnsr_single == 'true') ? 'on' : 'off';
$donation_dnsr_recurring = isset($wfc_don_settings->donation_dnsr_recurring) ? $wfc_don_settings->donation_dnsr_recurring : 'true';
$donation_dnsr_recurring = ($donation_dnsr_recurring == 'true') ? 'on' : 'off';


$purge_donation = isset($wfc_don_settings->purge_donation) ? $wfc_don_settings->purge_donation : 'false';
$purge_donation = ($purge_donation == 'true') ? 'on' : 'off';
$purge_donation_datefrom = isset($wfc_don_settings->purge_donation_datefrom) ? $wfc_don_settings->purge_donation_datefrom : '';
$purge_donation_datefrom_timestamp = (isset($wfc_don_settings->purge_donation_datefrom_timestamp) && $wfc_don_settings->purge_donation_datefrom_timestamp != 0) ? (Int)$wfc_don_settings->purge_donation_datefrom_timestamp : '';
$purge_donation_dateto = isset($wfc_don_settings->purge_donation_dateto) ? $wfc_don_settings->purge_donation_dateto : '';
$purge_donation_dateto_timestamp = (isset($wfc_don_settings->purge_donation_dateto_timestamp) && $wfc_don_settings->purge_donation_dateto_timestamp != 0)? (Int)$wfc_don_settings->purge_donation_dateto_timestamp : '';

$frequencies_def = array(
	array('index' => 0,
        'values' => array(
	        'frequency-name' => 'One-off',
	        'frequency-switch' => 'on',
	        'frequency-value' => 'one-off'
        ),
        'non_deletable' => true
    ),
    array('index' => 1,
          'values' => array(
	          'frequency-name' => 'Daily',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'daily'
          ),
        'non_deletable' => true
    ),
    array('index' => 2,
          'values' => array(
	          'frequency-name' => 'Weekly',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'weekly'
          ),
        'non_deletable' => true
    ),
    array('index' => 3,
          'values' => array(
	          'frequency-name' => 'Four-weekly',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'fourweekly'
          ),
        'non_deletable' => true
    ),
    array('index' => 4,
          'values' => array(
	          'frequency-name' => 'Fortnightly',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'fortnightly'
          ),
        'non_deletable' => true
    ),
    array('index' => 5,
          'values' => array(
	          'frequency-name' => 'Monthly',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'monthly'
          ),
        'non_deletable' => true
    ),
    array('index' => 6,
          'values' => array(
	          'frequency-name' => 'Quarterly',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'quarterly'
          ),
        'non_deletable' => true
    ),
    array('index' => 7,
          'values' => array(
	          'frequency-name' => 'Yearly',
	          'frequency-switch' => 'on',
	          'frequency-value' => 'yearly'
          ),
        'non_deletable' => true
    ),
);
$frequencies = isset($wfc_don_settings->frequencies) ? $wfc_don_settings->frequencies : null ;


$enable_validation = isset($wfc_don_settings->enable_validation) ? $wfc_don_settings->enable_validation : 'false';
$enable_validation = ($enable_validation == 'true') ? 'on' : 'off';
$google_api_key = isset($wfc_don_settings->google_api_key) ? $wfc_don_settings->google_api_key : '';
$google_prioritized_country = isset($wfc_don_settings->google_prioritized_country) ? $wfc_don_settings->google_prioritized_country : '';

$register_custom_set_address_def = array();
$register_custom_set_address = isset($wfc_don_settings->register_custom_set_address) ? $wfc_don_settings->register_custom_set_address : null ;

$harmony_rapid_displaymode  = 
isset($wfc_don_settings->harmony_rapid_displaymode) ? 
$wfc_don_settings->harmony_rapid_displaymode : 'fieldmode';

$harmony_overridablefields = 
isset($wfc_don_settings->harmony_overridablefields) ? 
$wfc_don_settings->harmony_overridablefields : 'false';
$harmony_overridablefields = ($harmony_overridablefields == 'true') ? 'on' : 'off';

$harmony_rapid_sourceoftruth  = 
isset($wfc_don_settings->harmony_rapid_sourceoftruth) ? 
$wfc_don_settings->harmony_rapid_sourceoftruth : 'AUSOTS';

$harmony_rapid_username = 
isset($wfc_don_settings->harmony_rapid_username) ? 
$wfc_don_settings->harmony_rapid_username : '';

$harmony_rapid_password = 
isset($wfc_don_settings->harmony_rapid_password) ? 
$wfc_don_settings->harmony_rapid_password : '';

$enable_recaptcha = isset($wfc_don_settings->enable_recaptcha) ? $wfc_don_settings->enable_recaptcha : 'false';
$enable_recaptcha = ($enable_recaptcha == 'true') ? 'on' : 'off';
$google_site_key = isset($wfc_don_settings->google_site_key) ? $wfc_don_settings->google_site_key : '';
$google_secret_key = isset($wfc_don_settings->google_secret_key) ? $wfc_don_settings->google_secret_key : '';

$social_facebook = isset($wfc_don_settings->social_facebook) ? $wfc_don_settings->social_facebook : 'false';
$social_facebook = ($social_facebook == 'true') ? 'on' : 'off';
$social_twitter = isset($wfc_don_settings->social_twitter) ? $wfc_don_settings->social_twitter : 'false';
$social_twitter = ($social_twitter == 'true') ? 'on' : 'off';
$social_google_plus = isset($wfc_don_settings->social_google_plus) ? $wfc_don_settings->social_google_plus : 'false';
$social_google_plus = ($social_google_plus == 'true') ? 'on' : 'off';
$social_linkedin = isset($wfc_don_settings->social_linkedin) ? $wfc_don_settings->social_linkedin : 'false';
$social_linkedin = ($social_linkedin == 'true') ? 'on' : 'off';

$loading_enable = isset($wfc_don_settings->loading_enable) ? $wfc_don_settings->loading_enable : 'true';
$loading_spinner = isset($wfc_don_settings->loading_spinner) ? $wfc_don_settings->loading_spinner : 'tail';
$loading_tinter_color = isset($wfc_don_settings->loading_tinter_color) ? $wfc_don_settings->loading_tinter_color : '#000000';

$general_tab = array(
	'donation_debug_mode' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'donation_debug_mode',
			'class'    => 'donation_debug_mode',
			'name'     => 'donation_debug_mode',
            'switch'   => $donation_debug_mode,
            'gridclass' => 'col-md-12 col-lg-12',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Donation Debug Mode',
				'position'  => 'top',
			),
			'description' => 'This will be enable and make developer debug tools available.'
		)
	),	
	'cron_schedule' => array(
		'type' => 'select',
        'settings' => array(
        	'id' => 'cron_schedule',
	        'name' => 'cron_schedule',
	        'gridclass' => 'col-md-12 col-lg-12',
	        'style'   => 'width:100%;',
	        'readonly' => false,
	        'options' => array(
	        	array(
	        		'title' => 'Hourly',
			        'value' => 'hourly'
		        ),
		        array(
			        'title' => 'Twice Daily',
			        'value' => 'twicedaily'
		        ),
		        array(
			        'title' => 'Daily',
			        'value' => 'daily'
		        ),
	        ),
	        'label'     => array(
				'hidden'    => false,
				'value'     => 'Cron Sync Schedule',
				'position'  => 'top',
			),
	        'description' => 'This will be the status of data during Salesforce synchronization.',
	        'selected' => $cron_schedule,
        )
    ),
);

$donation_record_tab = array(
	'purge_donation' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'purge_donation',
			'class'    => 'purge_donation',
			'name'     => 'purge_donation',
            'switch'   => $purge_donation,
            'gridclass' => 'col-md-12 col-lg-12',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Purge Donation Synced Successfully',
				'position'  => 'top',
			),
			'description' => '<b>Note : </b>This will regularly delete the donation record that have been successfully synced to Salesforce. The deleted record cannot be retrieve which means that enabling the purge donation functionality must be carefully thought out. This functionality refers to the purge date range.'
		)
	),

	'purge_donation_datefrom' => array(
        'type' => 'date-picker',
        'settings' => array(
            'id' => 'purge_donation_datefrom',
            'class' => 'purge_donation_datefrom',
            'name' => 'purge_donation_datefrom',
            'gridclass' => 'col-md-6 col-lg-6',
            'label' => array(
                'hidden' => false,
                'value' => 'Purge donation dated on date set and beyond',
                'position' => 'top'
            ),
            'value' => is_integer($purge_donation_datefrom_timestamp) ? gmdate("Y-m-d", $purge_donation_datefrom_timestamp) : '',
            'description' => '<b>Note : </b> Set the date of the purge where all the successfully synced donation on that date and beyond will be purge.',
            'dependency' => array(
                'id' => 'purge_donation',
                'trigger_value' => 'on',
                'trigger_action' => 'show_hide',
            )
        )
    ),
    'purge_donation_dateto' => array(
        'type' => 'date-picker',
        'settings' => array(
            'id' => 'purge_donation_dateto',
            'class' => 'purge_donation_dateto',
            'name' => 'purge_donation_dateto',
            'gridclass' => 'col-md-6 col-lg-6',
            'label' => array(
                'hidden' => false,
                'value' => 'Purge donation dated on date set and before',
                'position' => 'top'
            ),
            'value' => is_integer($purge_donation_dateto_timestamp) ? gmdate("Y-m-d", $purge_donation_dateto_timestamp) : '',
            'description' => '<b>Note : </b> Set the date of the purge where all the successfully synced donation on that date and before will be purge.',
            'dependency' => array(
                'id' => 'purge_donation',
                'trigger_value' => 'on',
                'trigger_action' => 'show_hide',
            )
        )
    )
);


$frequency_tab = array(

    'frequencies' => array(
        'type' => 'sortable-item',
        'settings' => array(
            'id' => 'frequencies',
            'class' => 'frequencies',
            'name' => 'frequencies',
            'gridclass' => 'col-md-12 col-lg-12',
            'mode' => 'advance',
            'base_title_id' => 'frequency-name',
            'item_deletable' => false,
            'add_item' => false,
            'label' => array(
                'value' => 'Your frequency settings here:',
                'position' => 'top'
            )
        ),
        'item_struct' => array(
	        'frequency-name' => array(
                'type' => 'input',
                'settings' => array(
                    'id' => 'frequency-name',
                    'name' => 'frequency-name',
                    'label' => array(
                        'hidden' => false,
                        'value' => 'Label',
                        'position' => 'top'
                    )
                )
            ),
	        'frequency-switch' => array(
                'type' => 'switch',
                'settings' => array(
                    'id' => 'frequency-switch',
                    'name' => 'frequency-switch',
                    'switch' => 'on',
                    'label' => array(
                        'hidden' => false,
                        'value' => 'Enable Frequency?',
                        'position' => 'top'
                    )
                )
            ),
	        'frequency-value' => array(
                'type' => 'input',
                'settings' => array(
                    'id' => 'frequency-value',
                    'name' => 'frequency-value',
                    'type' => 'hidden',
                    'label' => array(
                        'hidden' => true
                    )
                )
            )
        ),
        'item_values' => Saiyan_Module_SortableItem::get_value($frequencies,$frequencies_def),
        //'item_values' => $frequencies_def,
    )


);

$address_validation_tab = array(
	'enable_validation' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'enable_validation',
			'class'    => 'enable_validation',
			'name'     => 'enable_validation',
            'switch'   => $enable_validation,
            'gridclass' => 'col-md-12 col-lg-12',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Use Google Validation',
				'position'  => 'top',
			),
			'description' => 'Toggle whether to use Google or Harmony Rapid(Mastersoft) Address Validation.',
		)
	),	
	'google_api_key' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'google_api_key',
	        'name' => 'google_api_key',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Google Api Key',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $google_api_key,
	        'description' => 'Google Api Key.',
	        'dependency' => array(
	        	'id' => 'enable_validation',
	        	'trigger_value' => 'on',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
	'google_prioritized_country' => array(
		'type' => 'select',
		'settings' => array(
			'id' => 'google_prioritized_country',
			'name' => 'google_prioritized_country',
			'gridclass' => 'col-md-6 col-lg-6',
			'style'   => 'width:100%;',
			'readonly' => false,
			'options' => $country,
			'label'         => array(
				'hidden'    => false,
				'value'     => 'Prioritized Country',
				'position'  => 'top',
			),
			'description' => 'Select a country to prioritize on the Google address autocomplete.',
			'dependency' => array(
				'id' => 'enable_validation',
				'trigger_value' => 'on',
				'trigger_action' => 'show_hide',
			),
			'selected' => $google_prioritized_country,
		)
	),
	'register_custom_set_address' => array(
		'type' => 'sortable-item',
		'settings' => array(
			'id' => 'register_custom_set_address',
			'class' => 'register_custom_set_address',
			'name' => 'register_custom_set_address',
			'gridclass' => 'col-md-12 col-lg-12',
			'mode' => 'advance',
			'base_title_id' => 'cas_name',
			'item_deletable' => true,
			'add_item' => true,
			'label' => array(
				'value' => 'Register set of custom address:',
				'position' => 'top'
			),
			'dependency' => array(
				'id' => 'enable_validation',
				'trigger_value' => 'on',
				'trigger_action' => 'show_hide',
			)
		),
		'item_struct' => array(
			'cas_name' => array(
				'type' => 'input',
				'settings' => array(
					'id' => 'cas_name',
					'name' => 'cas_name',
					'class' => 'col-md-6 col-lg-6',
					'label' => array(
						'hidden' => false,
						'value' => 'Name of the set of address. *',
						'position' => 'top'
					)
				)
			),
			'include_set' => array(
				'type' => 'switch',
				'settings' => array(
					'id' => 'include_set',
					'name' => 'include_set',
					'class' => 'col-md-6 col-lg-6',
					'switch' => 'on',
					'label' => array(
						'hidden' => false,
						'value' => 'Include this set.',
						'position' => 'top'
					)
				)
			),'cas_address_field' => array(
				'type' => 'input',
				'settings' => array(
					'id' => 'cas_address_field',
					'name' => 'cas_address_field',
					'class' => 'col-md-12 col-lg-12',
					'label' => array(
						'hidden' => false,
						'value' => 'Address Field *',
						'position' => 'top'
					)
				)
			),
			'cas_country_field' => array(
				'type' => 'input',
				'settings' => array(
					'id' => 'cas_country_field',
					'name' => 'cas_country_field',
					'class' => 'col-md-6 col-lg-6',
					'label' => array(
						'hidden' => false,
						'value' => 'Country Field *',
						'position' => 'top'
					)
				)
			),
			'cas_state_field' => array(
				'type' => 'input',
				'settings' => array(
					'id' => 'cas_state_field',
					'name' => 'cas_state_field',
					'class' => 'col-md-6 col-lg-6',
					'label' => array(
						'hidden' => false,
						'value' => 'State Field *',
						'position' => 'top'
					)
				)
			),
			'cas_postcode_field' => array(
				'type' => 'input',
				'settings' => array(
					'id' => 'cas_postcode_field',
					'name' => 'cas_postcode_field',
					'class' => 'col-md-6 col-lg-6',
					'label' => array(
						'hidden' => false,
						'value' => 'Post Code Field *',
						'position' => 'top'
					)
				)
			),
			'cas_suburb_field' => array(
				'type' => 'input',
				'settings' => array(
					'id' => 'cas_suburb_field',
					'name' => 'cas_suburb_field',
					'class' => 'col-md-6 col-lg-6',
					'label' => array(
						'hidden' => false,
						'value' => 'Suburb Field *',
						'position' => 'top'
					)
				)
			),
		),
		'item_values' => Saiyan_Module_SortableItem::get_value($register_custom_set_address,$register_custom_set_address_def),
		//'item_values' => $frequencies_def,
	),
    'harmony_rapid_displaymode' => array(
		'type' => 'select',
        'settings' => array(
        	'id' => 'harmony_rapid_displaymode',
	        'name' => 'harmony_rapid_displaymode',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'style'   => 'width:100%;',
	        'readonly' => false,
	        'options' => array(
		        array(
	        		'title' => "Display as Readonly Fields",
			        'value' => 'fieldmode'
		        ),
	        	array(
	        		'title' => "Display as Address Detail Card",
			        'value' => 'detailcardmode'
		        ),
	        ),
	        'label'     => array(
				'hidden'    => false,
				'value'     => 'Display Mode',
				'position'  => 'top',
			),
	        'description' => 'Display address field as detail view or readonly fields.',
	        'dependency' => array(
	        	'id' => 'enable_validation',
	        	'trigger_value' => 'off',
	        	'trigger_action' => 'show_hide',
	        ),
	        'selected' => $harmony_rapid_displaymode,
        )
    ),
    'harmony_overridablefields' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'harmony_overridablefields',
			'class'    => 'harmony_overridablefields',
			'name'     => 'harmony_overridablefields',
            'switch'   => $harmony_overridablefields,
            'gridclass' => 'col-md-6 col-lg-6',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Make fields overridable',
				'position'  => 'top',
			),
			'description' => '<b>Note:</b> This will allow the donor to edit the address fields. If the address used is not from Harmony Rapid this means that there is no DPID generated thus making the address as not certified by Harmony Rapid.',
			'dependency' => array(
	        	'id' => 'enable_validation',
	        	'trigger_value' => 'off',
	        	'trigger_action' => 'show_hide',
	        ),
		)
	),
    'harmony_rapid_sourceoftruth' => array(
		'type' => 'select',
        'settings' => array(
        	'id' => 'harmony_rapid_sourceoftruth',
	        'name' => 'harmony_rapid_sourceoftruth',
	        'gridclass' => 'col-md-12 col-lg-12',
	        'style'   => 'width:100%;',
	        'readonly' => false,
	        'options' => array(
		        array(
	        		'title' => "Combination of Australian Sources of Truth (Australia) - AUSOTs",
			        'value' => 'AUSOTS'
		        ),
	        	array(
	        		'title' => "Australia Post's Postal Address File (Australia) - AUPAF",
			        'value' => 'AUPAF'
		        ),
		        array(
	        		'title' => "PSMA's Geocoded National Address File (Australia) - GNAF",
			        'value' => 'GNAF'
		        ),
		        array(
	        		'title' => "New Zealand Post's Postal Address File (New Zealand) - NZPAF",
			        'value' => 'NZPAF'
		        ),
	        ),
	        'label'     => array(
				'hidden'    => false,
				'value'     => 'Source of Truth',
				'position'  => 'top',
			),
	        'description' => 'Source of truth for harmony rapid address Validation.',
	        'dependency' => array(
	        	'id' => 'enable_validation',
	        	'trigger_value' => 'off',
	        	'trigger_action' => 'show_hide',
	        ),
	        'selected' => $harmony_rapid_sourceoftruth,
        )
    ),
    'harmony_rapid_username' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'harmony_rapid_username',
	        'name' => 'harmony_rapid_username',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Harmony Rapid Username',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $harmony_rapid_username,
	        'description' => 'Harmony Rapid Username for harmony rapid address Validation.',
	        'dependency' => array(
	        	'id' => 'enable_validation',
	        	'trigger_value' => 'off',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
    'harmony_rapid_password' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'harmony_rapid_password',
	        'name' => 'harmony_rapid_password',
	        'type' => 'password', 
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Harmony Rapid Password',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $harmony_rapid_password,
	        'description' => 'Harmony Rapid Password for harmony rapid address Validation.',
	        'dependency' => array(
	        	'id' => 'enable_validation',
	        	'trigger_value' => 'off',
	        	'trigger_action' => 'show_hide',
	        )
        )
    ),
	
);

$google_recaptcha_tab = array(
	'enable_recaptcha' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'enable_recaptcha',
			'class'    => 'enable_recaptcha',
			'name'     => 'enable_recaptcha',
            'switch'   => $enable_recaptcha,
            'gridclass' => 'col-md-12 col-lg-12',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Enable reCaptcha',
				'position'  => 'top',
			),
			'description' => 'Enable/Disable google recaptcha.',
		)
	),	
    'google_site_key' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'google_site_key',
	        'name' => 'google_site_key',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Site Key',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $google_site_key,
	        'description' => 'Site key needed for google reCaptcha.',
        )
    ),
    'google_secret_key' => array(
        'type' => 'input',
        'settings' => array(
	        'id' => 'google_secret_key',
	        'name' => 'google_secret_key',
	        'gridclass' => 'col-md-6 col-lg-6',
	        'label'         => array(
		        'hidden'    => false,
		        'value'     => 'Secret Key',
		        'position'  => 'top',
	        ),
	        'readonly' => false,
	        'value' => $google_secret_key,
	        'description' => 'Secret key needed for google reCaptcha.',
        )
    ),

);

$social_tab = array(
	'social_facebook' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'social_facebook',
			'class'    => 'social_facebook',
			'name'     => 'social_facebook',
            'switch'   => $social_facebook,
            'gridclass' => 'col-md-6 col-lg-6',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Facebook',
				'position'  => 'top',
			),
			'description' => '<span class="fa fa-facebook-square" style="font-size: 1.5em;"></span> Enable Facebook link button.',
		)
	),	
	'social_twitter' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'social_twitter',
			'class'    => 'social_twitter',
			'name'     => 'social_twitter',
            'switch'   => $social_twitter,
            'gridclass' => 'col-md-6 col-lg-6',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Twitter',
				'position'  => 'top',
			),
			'description' => '<span class="fa fa-twitter" style="font-size: 1.5em;"></span> Enable Twitter link button.',
		)
	),	
	'social_google_plus' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'social_google_plus',
			'class'    => 'social_google_plus',
			'name'     => 'social_google_plus',
            'switch'   => $social_google_plus,
            'gridclass' => 'col-md-6 col-lg-6',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'Google Plus',
				'position'  => 'top',
			),
			'description' => '<span class="fa fa-google-plus" style="font-size: 1.5em;"></span> Enable Google Plus link button.',
		)
	),
	'social_linkedin' => array(
		'type' => 'switch',
		'settings' => array(
			'id'       => 'social_linkedin',
			'class'    => 'social_linkedin',
			'name'     => 'social_linkedin',
            'switch'   => $social_linkedin,
            'gridclass' => 'col-md-6 col-lg-6',
            'required' => false,
            'readonly' => false,
            'label'         => array(
				'hidden'    => false,
				'value'     => 'LInkedIn',
				'position'  => 'top',
			),
			'description' => '<span class="fa fa-linkedin-square" style="font-size: 1.5em;"></span> Enable LInkedIn link button.',
		)
	),	
);

$toolkit_instance = Saiyan_Toolkit::get_instance();
$toolkits = scandir($toolkit_instance->get_spinner_svg_dir());
$select_toolkits = array();
foreach ($toolkits as $toolkit) {
    if ($toolkit !== '.' && $toolkit !== '..') {
        $svg_type = preg_replace('/^(loading_)/', '', $toolkit);
        $svg_type = preg_replace('/(\.svg)?/', '', $svg_type);

        array_push($select_toolkits, array(
            'title' => ucwords(preg_replace('/_/', ' ', $svg_type)),
            'value' => $svg_type
        ));
    }
}

$loading_options = array(
    'loading_enable' => array(
        'type' => 'switch',
        'settings' => array(
            'id' => 'loading_enable',
            'name' => 'loading_enable',
            'class' => 'loading_enable',
            'gridclass' => 'col-md-12',
            'label' => array(
                'value' => 'Enable Loader'
            ),
            'description' => 'Enable loader during page redirection',
            'switch' => $loading_enable === 'true' ? 'on' : 'off'
        )
    ),
	'loading_spinner' => array(
        'type' => 'select',
        'settings' => array(
            'id' => 'loading_spinner',
            'name' => 'loading_spinner',
            'gridclass' => 'col-md-6',
            'label' => array(
                'value' => 'Loading Spinner'
            ),
            'description' => 'This will be displayed on any page that has a loading feature',
            'options' => $select_toolkits,
            'selected' => $loading_spinner
        )
    ),
    'loading_spinner_preview' => array(
        'type' => 'html',
        'settings' => array(
            'id' => 'loading_spinner_preview',
            'gridclass' => 'col-md-6',
            'label' => array(
                'value' => 'Spinner Preview'
            )
        ),
        'content' =>
            '<img id="puf-spinner-preview" height="70px" style="background-color: rgba(0, 0, 0, 0.15);" src="' . $toolkit_instance->get_spinner_svg_url($loading_spinner) . '" />'
    ),
    'loading_tinter_color' => array(
        'type' => 'color-picker',
        'settings' => array(
            'id' => 'loading_tinter_color',
            'name' => 'loading_tinter_color',
            'class' => 'loading_tinter_color',
            'gridclass' => 'col-md-6',
            'label' => array(
                'value' => 'Tinter Color'
            ),
            'description' => 'This will be applied for the loading page with tinter. Opacity will be added automatically',
            'color' => $loading_tinter_color
        )
    )
);

$accordion = array(
	'donation_form' => array(
	    'type' => 'accordion',
	    'settings' => array(
	        'id' => 'donation_form',
	        'gridclass' => 'col-md-12 col-lg-12',
	    ),
	    'sections' => array(
	        array(
	            'title' => 'General',
	            'description' => 'Developer tools and general settings for donation.',
	            'content' => $general_tab,
	        ),
	        array(
	            'title' => 'Donation Record',
	            'description' => 'Data records that have created through donation process.',
	            'content' => $donation_record_tab,
	        ),
	        array(
	            'title' => 'Frequency',
	            'description' => 'Donation frequency available in donation form settings.',
	            'content' => $frequency_tab,
	        ),
	       	array(
	            'title' => 'Address Validation',
	            'description' => 'Address auto-suggest and validation for donation form.',
	            'content' => $address_validation_tab,
	        ),
	        array(
	            'title' => 'Google reCaptcha',
	            'description' => 'Google reCaptcha for donation form.',
	            'content' => $google_recaptcha_tab,
	        ),
	        array(
	            'title' => 'Social',
	            'description' => 'Social link buttons.',
	            'content' => $social_tab,
	        ),
	        array(
	            'title' => 'Loading Options',
	            'content' => $loading_options,
	        )
	    )
	),
	'wfc_donation_savesettings_btn' => array(
	  	'type' => 'button',
	  	'settings' => array(
	        'id'    => 'wfc_donation_savesettings_btn',
	        'class' => 'wfc_donation_savesettings_btn',
	        'name'  => 'wfc_donation_savesettings_btn',
	        'type'  => 'button',
	        'style' => 'float:left;margin-top:20px;',
	        // 'gridclass'=> 'col-1',
	        'label'     => array(
		        'hidden'    => true,
		        'value'     => '',
		        'position'  => 'top',
		    ),
		    'content' => 'SAVE'
	    )
	),
	'wfc_donation_resetsettings_btn' => array(
		'type' => 'button',
		'settings' => array(
			'id'    => 'wfc_donation_resetsettings_btn',
			'class' => 'wfc_donation_resetsettings_btn',
			'name'  => 'wfc_donation_resetsettings_btn',
			'type'  => 'button',
			'style' => 'float:left;margin-top:20px;',
			'gridclass'=> 'col-1',
			'label'     => array(
				'hidden'    => true,
				'value'     => '',
				'position'  => 'top',
			),
			'content' => 'RESET'
		)
	),
); 
?>
<?php if(isset($_GET['cron']) && $_GET['cron'] == 1): 
/*
* Display wordpress schedule events
*/
$currenttimestamp = date("Y-m-d H:i:s");
$crontimestamp = wp_next_scheduled( 'wfc_don_cron_resyncdonation' );
?>
<div><?php echo " Current time - ".$currenttimestamp;?></div>
<?php echo " Cron (wfc_don_cron_resyncdonation) next schedule - ".date('m/d/Y H:i:s', $crontimestamp);?></div>
<div id="list-of-corn"><?php echo '<pre>'; print_r( _get_cron_array() ); echo '</pre>';?></div>	
<?php endif; ?>
<div class="wfc_donation_settings_main_container">
	<h2><?php echo __('Donation Settings', 'webforce-connect-donation'); ?></h2>
<?php
Saiyan::tist()->render_module_mapping(array(
	'wfc_donation_settings' => array(
		'type' => 'settingsform',
		'settings' => array(
			'id'        => 'wfc_donation_settings',
			'class'     => 'wfc_donation_settings',
			'action'    => '',
			'name'      => 'wfc_donation_settings',
			'style'     => '',
			'title'     => '',
			'method'    => 'post',
			'wp_option' => array(
				'enable'     => true,
				'slug_name'  => '',
			),
			'submit_id' => 'wfc_donation_savesettings_btn',
		),
		'children' => $accordion,
    ),
	'navigator' => array(
		'type' => 'navigator',
		'settings' => array(
			'id' => 'navigator',
			'class' => 'navigator',
			'link_to_accordion' => 'donation_form'
		),
		'way_points' => array(
			'donation_form' => 'Donation Details'
		)
	)
));
?>
</div>
<script>
	jQuery(document).ready(function($){
	    $('#loading_spinner').on('change', function() {
	        let svgPath = '<?php echo $toolkit_instance->get_spinner_svg_url(); ?>';
	        $('#puf-spinner-preview').attr('src', svgPath + 'loading_' + $(this).val() + '.svg');
	    });
	});
</script>
<?php endif; ?>