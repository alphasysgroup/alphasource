<?php 
/**
* @author: Junjie Canonio
*/
?>
<?php if(!isset($_GET['scheduleevent'])): ?>
<a class="wfc-don-menu-btn-link" href="<?php echo home_url().'/wp-admin/admin.php?page=wfc-donation&scheduleevent=1' ;?>">
  <div id="wfc-don-menu-btn" class="wfc-don-menu-btn">
    <div class="container-fluid">
      <div class="row">
        <div class="col-2 wfc-don-menu-btn-icon"><i class="fa fa-cogs"></i></div>
        <div class="col-10 wfc-don-menu-btn-desc">
          <h3><?php echo __('Schedule Events', 'webforce-connect-donation'); ?></h3>
          <em><?php echo __('Monitor the cron functionality of Webforce Connect - Donation.', 'webforce-connect-donation'); ?></em>
        </div>
      </div>
    </div>
  </div>
</a>  	
<?php else: ?>
<div class="wfc_don_settings_main_cont">
  <h2><?php echo __('Schedule Events', 'webforce-connect-donation'); ?></h2>
  <div class="wfc-menu-container">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div id="date-part"></div>
          <div id="time-part"></div>
        </div>
      </div>
    </div>
  </div>  
</div>
<script type="text/javascript">
  jQuery(document).ready(function($){
    // var interval = setInterval(function() {
    //     var momentNow = moment();
    //     $('#date-part').html(momentNow.format('YYYY MMMM DD') + ' '
    //                         + momentNow.format('dddd')
    //                          .substring(0,3).toUpperCase());
    //     $('#time-part').html(momentNow.format('A hh:mm:ss'));
    // }, 100);    
  });
</script>        
<?php endif; ?>