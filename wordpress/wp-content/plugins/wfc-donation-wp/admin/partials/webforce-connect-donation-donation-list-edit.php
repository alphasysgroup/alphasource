<style type="text/css">
	pre {outline: 1px solid #ccc; padding: 5px; margin: 5px; overflow: auto;}
	.string { color: green; }
	.number { color: darkorange; }
	.boolean { color: blue; }
	.null { color: magenta; }
	.key { color: red; }

	.donation_cartItems{
		width: 100%;
	}

	.donation_cartItems input{
		padding: 5px 10px;
	    font-size: 14px;
	    line-height: 100%;
	    height: 36px;
	    width: auto;
	    outline: 0;
	    margin: 0 10px 10px 0;
	    background-color: #fff;
	}
</style>
<script type="text/javascript">

	jQuery(document).ready(function($){

		// beautify textarea
		if ($('#SF_PaymentSource').length) { beautifyJSONTextarea(['SF_PaymentSource']); }
		if ($('#SalesforceLogs').length) { beautifyJSONTextarea(['SalesforceLogs']); }
		if ($('#SalesforceSyncHistory').length) { beautifyJSONTextarea(['SalesforceSyncHistory']); }
		if ($('#card_details').length) { beautifyJSONTextarea(['card_details']); }
		
		if ($('#PaymentTransaction').length) { beautifyJSONTextarea(['PaymentTransaction']); }
		if ($('#payment_response').length) { beautifyJSONTextarea(['payment_response']); }
		if ($('#paymentObject').length) { beautifyJSONTextarea(['paymentObject']); }
		if ($('#donation_olddata').length) { beautifyJSONTextarea(['donation_olddata']); }
		
		if ($('#payment_response_debug').length) { beautifyJSONTextarea(['payment_response_debug']); }
		if ($('#PaymentTransaction_debug').length) { beautifyJSONTextarea(['PaymentTransaction_debug']); }
		if ($('#card_details_debug').length) { beautifyJSONTextarea(['card_details_debug']); }
		
		if ($('#SalesforceLogs_debug').length) { beautifyJSONTextarea(['SalesforceLogs_debug']); }
		if ($('#SalesforceSyncHistory_debug').length) { beautifyJSONTextarea(['SalesforceSyncHistory_debug']); }
		if ($('#cartItems_debug').length) { beautifyJSONTextarea(['cartItems_debug']); }
		if ($('#SF_PaymentSource_debug').length) { beautifyJSONTextarea(['SF_PaymentSource_debug']); }
		if ($('#purge_donation_debug').length) { beautifyJSONTextarea(['purge_donation_debug']); }
		
		setTimeout(function(){
			if($('#SalesforceResponse_debug').length == 1){
				beautifyJSONTextarea(['SalesforceResponse_debug']);	
			}
		},3000);

		function beautifyJSONTextarea( elems ) {
			$.each(elems, function(v, k){
				var obj = $( '#' + k );
				var str = syntaxHighlight( $.parseJSON( obj.val() ) );
				$( '#' + k ).after("<pre>"+str+"</pre>");
				$( '#' + k ).hide();
			});
		}

		function syntaxHighlight(json) {
		    if (typeof json != 'string') {
		         json = JSON.stringify(json, undefined, 2);
		    }
		    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
		    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		        var cls = 'number';
		        if (/^"/.test(match)) {
		            if (/:$/.test(match)) {
		                cls = 'key';
		            } else {
		                cls = 'string';
		            }
		        } else if (/true|false/.test(match)) {
		            cls = 'boolean';
		        } else if (/null/.test(match)) {
		            cls = 'null';
		        }
		        return '<span class="' + cls + '">' + match + '</span>';
		    });
		}
	});

</script>
<div id="wfc-donation-edit-cont" class="wrap" wfc-donation-id="<?php echo $_GET['ID'];?>">
	<?php if( $_GET['ID'] ): ?>
	<?php
	/*
	* Donotion settings
	*/
	$setting = get_option( 'pronto_donation_settings', 0 );
	$donation_debug_mode = isset($setting->donation_debug_mode) ? $setting->donation_debug_mode : 'false';

	$WFC_DonationForms = new WFC_DonationForms();

	$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $_GET['ID'] );
	$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
	$donation = maybe_unserialize( $donations );
	$donation = stripslashes_deep( $donation );
	//print_r($donation);
	$donation_olddata = str_replace( '\\','', json_encode( $donation ) );

	if(isset($donation['donor_first_name']) && isset($donation['donor_last_name']) && isset($donation['donor_email'])){
	

	/*
	* Exlude from cron syncing
	*/
	$exclude_on_cronsyncing = isset($donation['exclude_on_cronsyncing']) ? $donation['exclude_on_cronsyncing'] : 'false';
	$exclude_on_cronsyncing = ($exclude_on_cronsyncing == 'true') ? 'on' : 'off';


	/*
	* Donor Details
	*/
	$pdwp_donation_type = isset($donation['pdwp_donation_type']) ? $donation['pdwp_donation_type'] : '';
	$pdwp_sub_amt = isset($donation['pdwp_sub_amt']) ? $donation['pdwp_sub_amt'] : '';

	$payment_gateway = isset($donation['payment_gateway']) ? $donation['payment_gateway'] : '';
	$payment_gateway = explode("_", $payment_gateway);
	// $payment_gateway = isset($payment_gateway[0]) ? $payment_gateway[0] : '';
	$gateway_key		= count( $payment_gateway ) - 2;
	$payment_gateway 	= isset( $payment_gateway[ $gateway_key ] ) 
						? $payment_gateway[ $gateway_key ] 
						: ( isset($payment_gateway[0]) 
							? $payment_gateway[0] 
							: '' );


	$timestamp = isset($donation['timestamp']) ? $donation['timestamp'] : '';
	$timestamp = date( 'Y-m-d h:m:s', $timestamp );

	$donation_campaign = isset($donation['donation_campaign']) ? $donation['donation_campaign'] : '';

	$donor_type = isset($donation['donor_type']) ? $donation['donor_type'] : '';
	$donor_first_name = isset($donation['donor_first_name']) ? $donation['donor_first_name'] : '';
	$donor_last_name = isset($donation['donor_last_name']) ? $donation['donor_last_name'] : '';
	$donor_email = isset($donation['donor_email']) ? $donation['donor_email'] : '';
	$donor_phone = isset($donation['donor_phone']) ? $donation['donor_phone'] : '';
	$donor_address = isset($donation['donor_address']) ? $donation['donor_address'] : '';
	$donor_suburb = isset($donation['donor_suburb']) ? $donation['donor_suburb'] : '';
	$donor_state = isset($donation['donor_state']) ? $donation['donor_state'] : '';
	$donor_postcode = isset($donation['donor_postcode']) ? $donation['donor_postcode'] : '';
	$donor_country = isset($donation['donor_country']) ? $donation['donor_country'] : '';


	/*
	* Payment Transaction
	*/
	$statusCode = isset($donation['statusCode']) ? $donation['statusCode'] : '';
	$statusText = isset($donation['statusText']) ? $donation['statusText'] : '';
	$TokenCustomerID = isset($donation['TokenCustomerID']) ? $donation['TokenCustomerID'] : '';

	$card_details = isset($donation['card_details']) ? $donation['card_details'] : '';
	$card_details = str_replace( '\\','', json_encode( $card_details ) );

	$PaymentTransaction = isset($donation['PaymentTransaction']) ? $donation['PaymentTransaction'] : '';
	$PaymentTransaction = str_replace( '\\','', json_encode( $PaymentTransaction ) );

	$payment_response = isset($donation['payment_response']) ? $donation['payment_response'] : '';
	$payment_response = str_replace( '\\','', json_encode( $payment_response ) );

	/*
	* Campaign and GAU ID
	*/
	$campaignId = isset($donation['campaignId']) ? $donation['campaignId'] : '';


	/*
	* Fetch payment type
	*/
	$payment_type = isset($donation['payment_type']) ? $donation['payment_type'] : '';
	
	/*
	* Sync status
	*/
	$WFC_DonationList = new WFC_DonationList();

	/*
	* Fetch cart type and cart item
	*/
	$wfc_carttype = isset($donation['wfc_carttype']) ? $donation['wfc_carttype'] : '';
	$donation_cartItems = isset($donation['cartItems']) ? $donation['cartItems'] : array();
	$donation_singlePaymentsItem = array();
	$donation_recurringPaymentsItem = array();
	if(!empty($donation_cartItems)) {
		foreach ($donation_cartItems as $key => $value) {
			if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
				array_push($donation_singlePaymentsItem,$value);
			}else{
				array_push($donation_recurringPaymentsItem,$value);
			}
		}
	}


	if (sizeof($donation_singlePaymentsItem) > 1) {
		$donation_cartItems = $donation_recurringPaymentsItem;
		array_push($donation_cartItems,$donation_singlePaymentsItem);
	}
	foreach ($donation_cartItems as $key => $value) {
		if (empty($value)) {
			unset($donation_cartItems[$key]);
		}
	}
	

	if (!empty($donation_cartItems)) {
		$donation_cartItems_arr = array();
		foreach ($donation_cartItems as $key => $value) {
			$sync_status = $WFC_DonationList->wfc_get_sync_status_advancecart($donation,NULL,$key);
			$value['sync_status'] = $sync_status;
			$donation_cartItems_arr[$key] = $value;
		}
		$donation_cartItems = str_replace( '\\','', json_encode( $donation_cartItems_arr ) );
	}else{
		$donation_cartItems = '';
	}
	?><input type="hidden" id="wfc_carttype" name="wfc_carttype" value="<?php echo $wfc_carttype ;?>"><?php
	?><textarea hidden id="donation_cartItems" name="donation_cartItems"><?php echo $donation_cartItems;?></textarea><?php


	/*
	* Salesforce transaction
	*/
	$RecurringId = isset($donation['SalesforceResponse']['RecurringId']) ? $donation['SalesforceResponse']['RecurringId'] : '';
	$OpportunityId = isset($donation['SalesforceResponse']['OpportunityId']) ? $donation['SalesforceResponse']['OpportunityId'] : '';
	$Message = isset($donation['SalesforceResponse']['Message']) ? $donation['SalesforceResponse']['Message'] : '';

	$SalesforceLogs = isset($donation['SalesforceLogs']) ? $donation['SalesforceLogs'] : '';
	$SalesforceLogs = str_replace( '\\','', json_encode( $SalesforceLogs ) );

    $sync_statuses = '';
	?>
		<?php if(!empty($donation)): ?>
			<?php 
			$donation_actions = array();	
		    $donor_detials = array(
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'pdwp_donation_type',
				        'name' => 'pdwp_donation_type',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Donation Type',
					        'position'  => 'top',
				        ),
				        'value' => $pdwp_donation_type,
				        'readonly' => true,
				        'description' => 'Donation type of the donation form.'
			        )
		        ),
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'pdwp_amount',
				        'name' => 'pdwp_amount',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Amount',
					        'position'  => 'top',
				        ),
				        'value' => $pdwp_sub_amt,
				        'readonly' => true,
				        'description' => 'Donation amount of the transaction.'
			        )
		        ),
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'payment_gateway',
				        'name' => 'payment_gateway',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Payment Gateway',
					        'position'  => 'top',
				        ),
				        'value' => $payment_gateway,
				        'readonly' => true,
				        'description' => 'Type of payment gateway used.'
			        )
		        ),
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'timestamp',
				        'name' => 'timestamp',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Date & Time',
					        'position'  => 'top',
				        ),
				        'value' => $timestamp,
				        'readonly' => true,
				        'description' => 'Date and time the donation made.'
			        )
		        ),             
		    );


		    if($statusCode != '') {
		    	
				if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart') {
					$cartItems = isset($donation['cartItems']) ? $donation['cartItems'] : array();

					$singlePaymentsItem = array();
					$recurringPaymentsItem = array();
					if(!empty($cartItems)) {
						foreach ($cartItems as $key => $value) {
							if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
								array_push($singlePaymentsItem,$value);
							}else{
								array_push($recurringPaymentsItem,$value);
							}
						}
					}

					if (sizeof($singlePaymentsItem) > 1) {
						$cartItems = $recurringPaymentsItem;
						array_push($cartItems,$singlePaymentsItem);
					}
					foreach ($cartItems as $key => $value) {
						if (empty($value)) {
							unset($cartItems[$key]);
						}
					}

					if (!empty($cartItems)) {
						$donor_detials_syncbtn = array();
						foreach ($cartItems as $key => $value) {
							$sync_status = $WFC_DonationList->wfc_get_sync_status_advancecart($donation,NULL,$key);
							$sync_statuses = isset($sync_status['status']) ? $sync_status['status'] : 'not_sync';
							if( $sync_statuses == 'success_sync' ) {
								$sync_btncapt = "Sync Successful";
								$btn_style = 'background-color: #5BC75F;pointer-events:none;';
								//$btn_style = 'background-color: #5BC75F;';
							}elseif( $sync_statuses == 'link_sync' ) {
								$sync_btncapt = "Linked"; 
							}elseif( $sync_statuses == 'failed_sync' ) {
								$sync_btncapt = "Sync"; 
								$btn_style = 'background-color: #1497c1;';
							}elseif( $sync_statuses == 'recurring_failed_sync' ) {
								$sync_btncapt = "Sync Recurring";
								$btn_style = 'background-color: #1497c1;';
							}elseif( $sync_statuses == 'partial_sync' ) {
								$sync_btncapt = "Resync";
								$btn_style = 'background-color: #1497c1;';
							} elseif( $sync_statuses == 'attemp_reach' ) {
								$sync_btncapt = "LIMIT REACHED";
							} else {
								if($statusCode == '0'){
									$sync_btncapt = "Resync";	
									$btn_style = 'background-color: #C6C6C6;';												
								} elseif(empty($statusCode)) {
									$sync_btncapt = "Sync Not Allowed";	
									$btn_style = 'background-color: #C6C6C6;pointer-events:none;';
								}else{
									$sync_btncapt = "Resync";	
									$btn_style = 'background-color: #C6C6C6;';
								}	
							}

							$donor_detials_syncbtn[] = array(
							    'type' => 'button',
							    'settings' => array(
									'id'    => 'btn_resync_direct_advancecart'.$key,
									'class' => 'btn_resync_direct_advancecart',
									'name'  => 'btn_resync_direct_advancecart'.$key,
									'gridclass' => 'col-md-6 col-lg-6',
									'style' => $btn_style,
									'label'     => array(
										'hidden'    => false,
										'value'     => 'Re-Sync Donation',
										'position'  => 'top',
									),
								    'content' => '<i id="wfc-don-resync-donation-icon" class="fa fa-refresh" style="margin-right: 8px;"></i>'.$sync_btncapt,
								    'description' => 'By clicking the re-sync button, donation details will be re-synchronize to salesforce.',
							    )
							);	


							$donor_detials_syncbtn[] = array(
					    		'type' => 'select',
						        'settings' => array(
						        	'id' => 'sync_statuses_advancecart'.$key,
							        'name' => 'sync_statuses_advancecart'.$key,
							        'gridclass' => 'col-md-6 col-lg-6',
							        'style'   => 'width:100%;',
							        'readonly' => true,
							        'options' => array(
							        	array(
							        		'title' => 'Not Sync',
									        'value' => 'not_sync'
								        ),
								        array(
									        'title' => 'Success Sync',
									        'value' => 'success_sync'
								        ),
								        array(
									        'title' => 'Linked',
									        'value' => 'linked_sync'
								        ),
								        array(
									        'title' => 'Failed Sync',
									        'value' => 'failed_sync'
								        ),
								        array(
									        'title' => 'Attempt Reach',
									        'value' => 'attemp_reach'
								        ),
								        array(
									        'title' => 'Recurring Sync Failed ',
									        'value' => 'recurring_failed_sync'
								        ),
								        array(
									        'title' => 'Recurring Partial Sync',
									        'value' => 'partial_sync'
								        )
							        ),
							        'label'     => array(
										'hidden'    => false,
										'value'     => 'Sync Status',
										'position'  => 'top',
									),
							        'description' => 'This will be the status of data during salesforce synchronization.',
							        'selected' => $sync_statuses,
						        )
					        );				
						}
						$donation_actions = array_merge($donor_detials_syncbtn, $donation_actions);
					}
				}else{
					$sync_status = $WFC_DonationList->wfc_get_sync_status( $donation );
					$sync_statuses = isset($sync_status['status']) ? $sync_status['status'] : 'not-sync';
					if( $sync_statuses == 'success_sync' ) {
						$sync_btncapt = "Sync Successful";
						$btn_style = 'background-color: #5BC75F;pointer-events:none;';
					}elseif( $sync_statuses == 'link_sync' ) {
						$sync_btncapt = "Linked"; 
					}elseif( $sync_statuses == 'failed_sync' ) {
						$sync_btncapt = "Sync"; 
						$btn_style = 'background-color: #1497c1;';
					}elseif( $sync_statuses == 'recurring_failed_sync' ) {
						$sync_btncapt = "Sync Recurring";
						$btn_style = 'background-color: #1497c1;';
					}elseif( $sync_statuses == 'partial_sync' ) {
						$sync_btncapt = "Resync";
						$btn_style = 'background-color: #1497c1;';
					} elseif( $sync_statuses == 'attemp_reach' ) {
						$sync_btncapt = "LIMIT REACHED";
						$btn_style = 'background-color: #9e54a2;pointer-events:none;';
					} else {
						if(empty($statusCode) && $statusCode != '0' && $statusCode != '1'){
							if(strtolower($payment_type) == 'bank'){
								$sync_btncapt = "Resync";	
								$btn_style = 'background-color: #C6C6C6;';
							}else{
								$sync_btncapt = "Sync Not Allowed";
								$btn_style = 'background-color: #C6C6C6;pointer-events:none;';
							}
						}else{
							$sync_btncapt = "Resync";	
							$btn_style = 'background-color: #C6C6C6;';	
						}	
					}

				    $donor_detials_syncbtn = array(
						array(
						    'type' => 'button',
						    'settings' => array(
								'id'    => 'btn_resync_direct',
								'class' => '',
								'name'  => 'btn_resync_direct',
								'gridclass' => 'col-md-6 col-lg-6',
								'style' => $btn_style,
								'label'     => array(
									'hidden'    => false,
									'value'     => 'Re-Sync Donation',
									'position'  => 'top',
								),
							    'content' => '<i id="wfc-don-resync-donation-icon" class="fa fa-refresh" style="margin-right: 8px;"></i>'.$sync_btncapt,
							    'description' => 'By clicking the re-sync button, donation details will be re-synchronize to salesforce.',
						    )
						),
					    array(
				    		'type' => 'select',
					        'settings' => array(
					        	'id' => 'sync_statuses',
						        'name' => 'sync_statuses',
						        'gridclass' => 'col-md-6 col-lg-6',
						        'style'   => 'width:100%;',
						        'readonly' => true,
						        'options' => array(
						        	array(
						        		'title' => 'Not Sync',
								        'value' => 'not_sync'
							        ),
							        array(
								        'title' => 'Success Sync',
								        'value' => 'success_sync'
							        ),
							        array(
								        'title' => 'Linked',
								        'value' => 'linked_sync'
							        ),
							        array(
								        'title' => 'Failed Sync',
								        'value' => 'failed_sync'
							        ),
							        array(
								        'title' => 'Attempt Reach',
								        'value' => 'attemp_reach'
							        ),
							        array(
								        'title' => 'Recurring Sync Failed ',
								        'value' => 'recurring_failed_sync'
							        ),
							        array(
								        'title' => 'Recurring Partial Sync',
								        'value' => 'partial_sync'
							        )
						        ),
						        'label'     => array(
									'hidden'    => false,
									'value'     => 'Sync Status',
									'position'  => 'top',
								),
						        'description' => 'This will be the status of data during salesforce synchronization.',
						        'selected' => $sync_statuses,
					        )
				        ),
				    );
					$donation_actions = array_merge($donor_detials_syncbtn, $donation_actions);

					if( $sync_statuses != 'success_sync' && $sync_statuses != 'attemp_reach' ) {
						$donation_actions_excludebtn = array(
							array(
								'type' => 'switch',
								'settings' => array(
									'id'       => 'exclude_on_cronsyncing',
									'class'    => 'exclude_on_cronsyncing',
									'name'     => 'exclude_on_cronsyncing',
						            'switch'   => $exclude_on_cronsyncing,
						            'gridclass' => 'col-md-12 col-lg-12',
						            'required' => false,
						            'readonly' => false,
						            'label'         => array(
										'hidden'    => false,
										'value'     => 'Exclude in cron syncing',
										'position'  => 'top',
									),
									'description' => 'This donation record will be excluded on the scheduled syncing.'
								)
							)
						);
						$donation_actions = array_merge($donation_actions_excludebtn, $donation_actions);
					}


				}
		    }


			$donation_v5_form_user_fields = get_post_meta($donation_campaign, 'donation_v5_form_user_fields', true);
			$donor_detials_extend = array();
			if(!empty($donation_v5_form_user_fields)){
				$donation_v5_form_user_fields = json_decode($donation_v5_form_user_fields,true);	

				foreach ($donation_v5_form_user_fields as $key => $value_fields){
					foreach ($value_fields as $key => $value){
						$field_label = isset($value['values']['field_label']) ? $value['values']['field_label'] : '';
						$field_key = isset($value['values']['field_key']) ? $value['values']['field_key'] : '';
						$donation_value = isset($donation[$field_key]) ? $donation[$field_key] : '';

						$donor_detials_extend[] = array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => $field_key,
						        'name' => $field_key,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => $field_label,
							        'position'  => 'top',
						        ),
						        'readonly' => true,
						        'value' => $donation_value,
						        'description' => $field_label.' of the donor.'
					        )
				        );
					}
				}
			}else{
				$donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_type',
				        'name' => 'donor_type',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Donor Type',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_type,
				        'description' => 'Type of donor.'
			        )
		        );    
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_first_name',
				        'name' => 'donor_first_name',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'First Name',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_first_name,
				        'description' => 'First name of the donor.'
			        )
		        );
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_last_name',
				        'name' => 'donor_last_name',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Last Name',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_last_name,
				        'description' => 'Last name of the donor.'
			        )
		        );
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_email',
				        'name' => 'donor_email',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Email Address',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_email,
				        'description' => 'Email of the donor.'
			        )
		        );
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_phone',
				        'name' => 'donor_phone',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Phone',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_phone,
				        'description' => 'Phone/mobile number of the donor.'
			        )
		        );  
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_address',
				        'name' => 'donor_address',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Street Address',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_address,
				        'description' => 'Street Address of the donor.'
			        )
		        ); 
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_suburb',
				        'name' => 'donor_suburb',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Suburb Address',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_suburb,
				        'description' => 'Suburb Address of the donor.'
			        )
		        ); 
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_state',
				        'name' => 'donor_state',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'State Address',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_state,
				        'description' => 'State Address of the donor.'
			        )
		        ); 
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_postcode',
				        'name' => 'donor_postcode',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Postal Code',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_postcode,
				        'description' => 'Postal Code Address of the donor.'
			        )
		        ); 
		        $donor_detials_extend[] = array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'donor_country',
				        'name' => 'donor_country',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Country',
					        'position'  => 'top',
				        ),
				        'readonly' => true,
				        'value' => $donor_country,
				        'description' => 'Country of the donor.'
			        )
		        ); 
			}
			$donor_detials = array_merge($donor_detials, $donor_detials_extend);


			/* ========================================= Note ===============================================
			* payment_transaction - data ragarding the donation payment transaction are displayed here
			==============================================================================================*/
			$payment_transaction = array(
				array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'statusCode',
				        'name' => 'statusCode',
				        'readonly' => true,
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Status Code',
					        'position'  => 'top',
				        ),
				        'description' => 'Status code of the payment transaction.',
				        'value' => $statusCode
			        )
		        ), 
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'statusText',
				        'name' => 'statusText',
				        'readonly' => true,
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Status Text',
					        'position'  => 'top',
				        ),
				        'description' => 'Status text of the payment transaction.',
				        'value' => $statusText
			        )
		        ), 
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'statusText',
				        'name' => 'statusText',
				        'readonly' => true,
				        'gridclass' => 'col-md-12 col-lg-12',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Token Customer ID',
					        'position'  => 'top',
				        ),
				        'description' => 'Customer ID for tokenize payment transaction.',
				        'value' => $TokenCustomerID
			        )
		        ), 
		        array(
			        'type' => 'html',
			        'settings' => array(
				        'id' => 'html1',
				        'name' => '',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Payment Source',
					        'position'  => 'top',
				        ),

				        'description' => 'Payment Source of the donation.'
			        ),
			        'content' => '<textarea id="card_details" name="card_details">'.$card_details.'</textarea>',
		        ), 
		        array(
			        'type' => 'html',
			        'settings' => array(
				        'id' => 'html1',
				        'name' => '',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Payment Transaction',
					        'position'  => 'top',
				        ),

				        'description' => 'Payment Transaction of the donation.'
			        ),
			        'content' => '<textarea id="PaymentTransaction" name="PaymentTransaction">'.$PaymentTransaction.'</textarea>',
		        ),
		        array(
			        'type' => 'separator',
			        'settings' => array(
			        	'id' => 'separator1',
			        	'gridclass' => 'col-md-12 col-lg-12',
			        	'visibility' => 'hidden'
			       	),
		        ), 
		        array(
			        'type' => 'html',
			        'settings' => array(
				        'id' => 'html3',
				        'name' => '',
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Payment Response',
					        'position'  => 'top',
				        ),

				        'description' => 'Payment Response of the donation.'
			        ),
			        'content' => '<textarea id="payment_response" name="payment_response">'.$payment_response.'</textarea>',
		        ), 
			);


			if((!isset($donation['PaymentTransaction']) && empty($donation['PaymentTransaction'])) || (($sync_btncapt == 'Resync' || $sync_btncapt == 'Sync' || $sync_btncapt == 'Sync Recurring')&& isset($donation['paymenttransactionsetmanually']) && $donation['paymenttransactionsetmanually'] == 'true')) {
			    $wfc_don_paymenttransactionform_txnstatus = isset($donation['PaymentTransaction']['TxnStatus']) ? $donation['PaymentTransaction']['TxnStatus'] : '';
				$wfc_don_paymenttransactionform_txnid = isset($donation['PaymentTransaction']['TxnId']) ? $donation['PaymentTransaction']['TxnId'] : '';
				$wfc_don_paymenttransactionform_txndate = isset($donation['PaymentTransaction']['TxnDate']) ? $donation['PaymentTransaction']['TxnDate'] : '';
				$wfc_don_paymenttransactionform_txnpaymentref = isset($donation['PaymentTransaction']['TxnPaymentRef']) ? $donation['PaymentTransaction']['TxnPaymentRef'] : '';

				$payment_transaction_form_str = '<div id="wfc-donation-list-payment-transaction-form" class="wfc-donation-list-payment-transaction-form">';
                $payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-header row"><span> Set Payment Transaction </span></div>';

				$payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-body row">';

				$payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-fields-cont saiyan-select col-md-6 col-lg-6">';
				$payment_transaction_form_str .= '<label >Transaction Status (TxnStatus)</label >';
				$payment_transaction_form_str .= '<select id="wfc_don_paymenttransactionform_txnstatus">';
				$payment_transaction_form_str .= '<option value=""></option >';
				if ($wfc_don_paymenttransactionform_txnstatus == 'Success'){
					$payment_transaction_form_str .= '<option value="Success" selected>Success (Close Won)</option >';
				}else{
					$payment_transaction_form_str .= '<option value="Success">Success (Close Won)</option >';
				}

				if ($wfc_don_paymenttransactionform_txnstatus == 'Failed'){
					$payment_transaction_form_str .= '<option value="Failed" selected>Failed (Close Lost)</option >';
				}else{
					$payment_transaction_form_str .= '<option value="Failed">Failed (Close Lost)</option >';
                }
				$payment_transaction_form_str .= '</select>';
				$payment_transaction_form_str .= '<small><b>Note: </b> Please make sure and confirm if the transaction is Close Won or Close Lost.</small>';
				$payment_transaction_form_str .= '</div>';

				$payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-fields-cont saiyan-input col-md-6 col-lg-6">';
				$payment_transaction_form_str .= '<label >Transaction ID (TxnId)</label >';
				$payment_transaction_form_str .= '<input id="wfc_don_paymenttransactionform_txnid" type="text" value="'.$wfc_don_paymenttransactionform_txnid.'">';
				$payment_transaction_form_str .= '<small><b>Note: </b> Please make sure to provide the right and exact transaction ID that matches with the other transaction data.</small>';
				$payment_transaction_form_str .= '</div>';

				$payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-fields-cont saiyan-input col-md-6 col-lg-6">';
				$payment_transaction_form_str .= '<label >Transaction Date (TxnDate)</label >';
				$payment_transaction_form_str .= '<input id="wfc_don_paymenttransactionform_txndate" type="text" value="'.$wfc_don_paymenttransactionform_txndate.'">';
				$payment_transaction_form_str .= '<small><b>Note: </b> Please make sure to provide the right and exact transaction date that matches with the other transaction data.</small>';
				$payment_transaction_form_str .= '<small>Also make sure that the date string format is <kbd>(yyyy-mm-dd)</kbd> example <kbd>(2019-06-25)</kbd>.</small>';
				$payment_transaction_form_str .= '</div>';

				$payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-fields-cont saiyan-input col-md-6 col-lg-6">';
				$payment_transaction_form_str .= '<label >Transaction Reference (TxnPaymentRef)</label >';
				$payment_transaction_form_str .= '<input id="wfc_don_paymenttransactionform_txnpaymentref" type="text" value="'.$wfc_don_paymenttransactionform_txnpaymentref.'">';
				$payment_transaction_form_str .= '<small><b>Note: </b> Please make sure to provide the right and exact transaction reference that matches with the other transaction data. Example of a WFC Donation transaction reference looks like this <kbd>(1561453012-729657793-1197)</kbd>.</small>';

				$payment_transaction_form_str .= '</div>';

				$payment_transaction_form_str .= '<div class="wfc-donation-list-payment-transaction-form-fields-cont saiyan-input col-md-4 col-lg-4" style="padding: 15px; margin-top: 20px;height: 100px !important;">';
				$payment_transaction_form_str .= '<small style="margin-bottom: 10px;"><b>Warning: </b> Before saving this payment transaction details, please make sure and confirm all the payment transaction details are correct. The payment transaction of this <kbd>Void</kbd> donation record can only be set once and connot be updated again if synced to <kbd>Salesforce</kbd> so please make sure that every thing is validated and a hundred percent correct.</small>';
				$payment_transaction_form_str .= '<span id="wfc-donation-list-payment-transaction-form-save-btn">Save Payment Transaction</span >';
                $payment_transaction_form_str .= '</div>';

				$payment_transaction_form_str .= '</div>';

                $payment_transaction_form_str .= '</div>';

				$payment_transaction_form = array(
					'type' => 'html',
					'settings' => array(
						'id' => 'payment_transaction_form',
						'name' => '',
						'gridclass' => 'col-md-12 col-lg-12',
						'label' => array(
							'hidden' => false,
							'value' => '',
							'position' => 'top',
						),
						'description' => $payment_transaction_form_str
					),
					'content' => '',
				);
                array_unshift($payment_transaction, $payment_transaction_form);
            }

			/* ========================================= Note ===============================================
			* SalesforceResponse - data ragarding the donation Salesforce sync is displayed here
			==============================================================================================*/
			$campaignandgau = array();
			$campaignandgau[] = array(
		        'type' => 'input',
		        'settings' => array(
			        'id' => 'wfc_donation_campaignid',
			        'name' => 'wfc_donation_campaignid',
			        'readonly' => true,
			        'gridclass' => 'col-md-6 col-lg-6',
			        'label'         => array(
				        'hidden'    => false,
				        'value'     => 'Campaign ID',
				        'position'  => 'top',
			        ),
			        'description' => 'Salesforce Campaign ID link with this donation.',
			        'value' => $campaignId
		        )
			);
			$donation_cartItem = '<div class="saiyan-input">';
			if(isset($donation['cartItems']) && !empty($donation['cartItems'])) {
				foreach ($donation['cartItems'] as $key => $value) {
					$value_gau_id = isset($value['gau_id']) ? $value['gau_id'] : '';
					$value_amount = isset($value['amount']) ? $value['amount'] : '';
					$value_wfc_donation_type = isset($value['wfc_donation_type']) ? $value['wfc_donation_type'] : '';

					$donation_cartItem .= '<div class="donation_cartItems">';
					$donation_cartItem .= '<input type="text" id="donation_cartItems_gau_id'.$key.'" value="'.$value_gau_id.'" readonly/>';	
					$donation_cartItem .= '<input type="text" id="donation_cartItems_amount'.$key.'" value="'.$value_amount.'" readonly/>';	
					if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart') {
						$donation_cartItem .= '<input type="text" id="donation_cartItems_wfc_donation_type'.$key.'" value="'.$value_wfc_donation_type.'" readonly/>';	
					}
					$donation_cartItem .= '</div>';

				}
			}else{
				if(isset($donation['gauId']) && !empty($donation['gauId'])) {
					$value_gau_id = isset($donation['gauId']) ? $donation['gauId'] : '';
					$value_amount = isset($donation['pdwp_sub_amt']) ? $donation['pdwp_sub_amt'] : '';
					$donation_cartItem .= '<div class="donation_cartItems">';
					$donation_cartItem .= '<input type="text" id="gauId'.$value_gau_id.'" value="'.$value_gau_id.'" readonly/>';	
					$donation_cartItem .= '<input type="text" id="amount'.$value_gau_id.'" value="'.$value_amount.'" readonly/>';	
					$donation_cartItem .= '</div>';
				}else{
					$donation_cartItem .= '<span>No GAU set.</span>';
				}
			}
			$donation_cartItem .= '</div>';

			$key_val = isset($key) ? $key : '';
			$campaignandgau[] = array(
		        'type' => 'html',
		        'settings' => array(
			        'id' => 'donation_cartItems'.$key_val,
			        'name' => '',
			        'gridclass' => 'col-md-12 col-lg-12',
			        'label'         => array(
				        'hidden'    => false,
				        'value'     => 'GAU ID',
				        'position'  => 'top',
			        ),

			        'description' => 'List of Salesforce GAU ID link with this donation.',
		        ),
		        'content' => $donation_cartItem,
		    );

			/* ========================================= Note ===============================================
			* SalesforcePaymentSourceResponse - data ragarding the donation Salesforce payment sync are displayed here
			==============================================================================================*/
			$SF_PS_paymentSourceId = 
			isset($donation['SF_PS_paymentSourceId']) ? 
			$donation['SF_PS_paymentSourceId'] : '';

			$SF_PS_contactId = 
			isset($donation['SF_PS_contactId']) ? 
			$donation['SF_PS_contactId'] : '';

			$SF_PaymentSource = 
			isset($donation['SF_PaymentSource']) ? 
			$donation['SF_PaymentSource'] : array();
			$SF_PaymentSource = str_replace( '\\','', json_encode( $SF_PaymentSource ) );

		    $salesforce_paymentsource_response = array(
		        array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'statusText',
				        'name' => 'statusText',
				        'readonly' => true,
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Salesforce Payment Source Id',
					        'position'  => 'top',
				        ),
				        'description' => 'This is the payment source Salesforce Id generated via PPAPI.',
				        'value' => $SF_PS_paymentSourceId
			        )
		        ), 
				array(
			        'type' => 'input',
			        'settings' => array(
				        'id' => 'statusCode',
				        'name' => 'statusCode',
				        'readonly' => true,
				        'gridclass' => 'col-md-6 col-lg-6',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Salesforce Payment Source Contact Id',
					        'position'  => 'top',
				        ),
				        'description' => 'This is the payment source contact Salesforce Id generated via PPAPI.',
				        'value' => $SF_PS_contactId
			        )
		        ), 
		        array(

			        'type' => 'html',
			        'settings' => array(
				        'id' => 'html111',
				        'name' => '',
				        'gridclass' => 'col-md-12 col-lg-12',
				        'label'         => array(
					        'hidden'    => false,
					        'value'     => 'Payment Source Salesforce Response',
					        'position'  => 'top',
				        ),
				        'description' => 'PPAPI Payment Source Request and Response.'
			        ),
			        'content' => '<textarea id="SF_PaymentSource" name="SF_PaymentSource">'.$SF_PaymentSource.'</textarea>',
		        )
			);


			/* ========================================= Note ===============================================
			* SalesforceResponse - data ragarding the donation Salesforce sync are displayed here
			==============================================================================================*/
			if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart') {
				if(isset($donation['SalesforceResponse']) && 
					is_array($donation['SalesforceResponse']) && 
					!empty($donation['SalesforceResponse'])) {
					$salesforce_response = array();
					foreach ($donation['SalesforceResponse'] as $key => $value) {

						if(isset($donation['SalesforceResponse'][$key]['error']) && 
							isset($donation['SalesforceResponse'][$key]['error_description']) && 
							isset($donation['SalesforceResponse'][$key]['status_code'])){

							$error_A = 
							isset($donation['SalesforceResponse'][$key]['error']) ? 
							$donation['SalesforceResponse'][$key]['error'] : '';

							$error_description_A = 
							isset($donation['SalesforceResponse'][$key]['error_description']) ? 
							$donation['SalesforceResponse'][$key]['error_description'] : '';

							$status_code_A = 
							isset($donation['SalesforceResponse'][$key]['status_code']) ? 
							$donation['SalesforceResponse'][$key]['status_code'] : '';

							$salesforce_response[] = array(		
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'error_A',
							        'name' => 'error_A',
							        'readonly' => true,
							        'gridclass' => 'col-md-6 col-lg-6',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Error',
								        'position'  => 'top',
							        ),
							        'description' => 'Error response form Salesforce.',
							        'value' => $error_A
						        )
						    );
							$salesforce_response[] = array(	
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'error_description_A',
							        'name' => 'error_description_A',
							        'readonly' => true,
							        'gridclass' => 'col-md-6 col-lg-6',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Error Description',
								        'position'  => 'top',
							        ),
							        'description' => 'Error description of the error response.',
							        'value' => $error_description_A
						        )
							); 
							$salesforce_response[] = array(	
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'status_code_A',
							        'name' => 'status_code_A',
							        'readonly' => true,
							        'gridclass' => 'col-md-12 col-lg-12',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Status Code',
								        'position'  => 'top',
							        ),
							        'description' => 'Status Code of the error response.',
							        'value' => $status_code_A
						        )			   
							);	
							$salesforce_response[] = array(
					            'type' => 'separator',
					            'settings' => array(
					                'id' =>  'wfc_fundraising_p2p_separator',
					                'class' => 'saiyan-separator',
					                'gridclass' => 'col-md-12',
					                'visibility' => 'visible',
					                'size' => 10,
					                'align' => 'center',
					                'width' => '100%',
					                'style' => 'border: 1px #cccccc solid; margin-bottom: 25px;'
					            )
						    ); 
						}elseif(isset($donation['SalesforceResponse'][$key]['errorCode']) && 
							(isset($donation['SalesforceResponse'][$key]['status']) && $donation['SalesforceResponse'][$key]['status'] == 'error')){

							$status_B = 
							isset($donation['SalesforceResponse'][$key]['status']) ? 
							$donation['SalesforceResponse'][$key]['status'] : '';

							$errorCode_B = 
							isset($donation['SalesforceResponse'][$key]['errorCode']) ? 
							$donation['SalesforceResponse'][$key]['errorCode'] : '';

							$Message_B = 
							isset($donation['SalesforceResponse'][$key]['Message']) ? 
							$donation['SalesforceResponse'][$key]['Message'] : '';

							$salesforce_response[] = array(	
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'status_B',
							        'name' => 'status_B',
							        'readonly' => true,
							        'gridclass' => 'col-md-6 col-lg-6',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Status',
								        'position'  => 'top',
							        ),
							        'description' => 'Status response.',
							        'value' => $status_B
						        )
						    );
							$salesforce_response[] = array(	
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'errorCode_B',
							        'name' => 'errorCode_B',
							        'readonly' => true,
							        'gridclass' => 'col-md-6 col-lg-6',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Error Code',
								        'position'  => 'top',
							        ),
							        'description' => 'Error code of response.',
							        'value' => $errorCode_B
						        )
						    ); 
							$salesforce_response[] = array(	
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'Message_B',
							        'name' => 'Message_B',
							        'readonly' => true,
							        'gridclass' => 'col-md-12 col-lg-12',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Message',
								        'position'  => 'top',
							        ),
							        'description' => 'Repsonse message.',
							        'value' => $Message_B
						        )
							);	
							$salesforce_response[] = array(
					            'type' => 'separator',
					            'settings' => array(
					                'id' =>  'wfc_fundraising_p2p_separator',
					                'class' => 'saiyan-separator',
					                'gridclass' => 'col-md-12',
					                'visibility' => 'visible',
					                'size' => 10,
					                'align' => 'center',
					                'width' => '100%',
					                'style' => 'border: 1px #cccccc solid; margin-bottom: 25px;'
					            )
						    ); 				
						}elseif(isset($donation['SalesforceResponse'][$key]['Message'])) {

							if (isset($value['OpportunityId'])) {
								$salesforce_response[] = array(								
							        'type' => 'input',
							        'settings' => array(
								        'id' => 'OpportunityId',
								        'name' => 'OpportunityId',
								        'readonly' => true,
								        'gridclass' => 'col-md-6 col-lg-6',
								        'label'         => array(
									        'hidden'    => false,
									        'value'     => 'Opportunity Salesforce ID',
									        'position'  => 'top',
								        ),
								        'description' => 'Opportunity ID on PPAPI response.',
								        'value' => $value['OpportunityId']
							        )						         
							    );
							}

						    if (isset($value['RecurringId'])) {
							    $salesforce_response[] = array(								
							        'type' => 'input',
							        'settings' => array(
								        'id' => 'RecurringId',
								        'name' => 'RecurringId',
								        'readonly' => true,
								        'gridclass' => 'col-md-6 col-lg-6',
								        'label'         => array(
									        'hidden'    => false,
									        'value'     => 'Recurring Salesforce ID',
									        'position'  => 'top',
								        ),
								        'description' => 'Recurring ID on PPAPI response.',
								        'value' => $value['RecurringId']
							        )						         
							    );
						    }
						    $salesforce_response[] = array(
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'Message',
							        'name' => 'Message',
							        'readonly' => true,
							        'gridclass' => 'col-md-12 col-lg-12',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Message',
								        'position'  => 'top',
							        ),
							        'description' => 'PPAPI response message.',
							        'value' => $value['Message']
						        )					        
							);
							$salesforce_response[] = array(
					            'type' => 'separator',
					            'settings' => array(
					                'id' =>  'wfc_fundraising_p2p_separator',
					                'class' => 'saiyan-separator',
					                'gridclass' => 'col-md-12',
					                'visibility' => 'visible',
					                'size' => 10,
					                'align' => 'center',
					                'width' => '100%',
					                'style' => 'border: 1px #cccccc solid; margin-bottom: 25px;'
					            )
						    ); 
					    }else{

					    }							
					}
				}else{
					$salesforce_response = array(
				        array(
					        'type' => 'html',
					        'settings' => array(
						        'id' => 'html999',
						        'name' => '',
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Payment Response',
							        'position'  => 'top',
						        ),

						        'description' => 'Payment Response of the donation.'
					        ),
					        'content' => '<h2>Not Sync</h2>',
				        ), 
				    );
				}
			
			}else{
				if(isset($donation['SalesforceResponse']['error']) && 
					isset($donation['SalesforceResponse']['error_description']) && 
					isset($donation['SalesforceResponse']['status_code'])){
					$error_A = 
					isset($donation['SalesforceResponse']['error']) ? $donation['SalesforceResponse']['error'] : '';
					$error_description_A = 
					isset($donation['SalesforceResponse']['error_description']) ? $donation['SalesforceResponse']['error_description'] : '';
					$status_code_A = 
					isset($donation['SalesforceResponse']['status_code']) ? $donation['SalesforceResponse']['status_code'] : '';

					$salesforce_response = array(
						array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'error_A',
						        'name' => 'error_A',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Error',
							        'position'  => 'top',
						        ),
						        'description' => 'Error response form Salesforce.',
						        'value' => $error_A
					        )
					    ), 
					    array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'error_description_A',
						        'name' => 'error_description_A',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Error Description',
							        'position'  => 'top',
						        ),
						        'description' => 'Error description of the error response.',
						        'value' => $error_description_A
					        )
					    ), 
					    array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'status_code_A',
						        'name' => 'status_code_A',
						        'readonly' => true,
						        'gridclass' => 'col-md-12 col-lg-12',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Status Code',
							        'position'  => 'top',
						        ),
						        'description' => 'Status Code of the error response.',
						        'value' => $status_code_A
					        )
					    ), 
					);	
				}elseif(isset($donation['SalesforceResponse']['errorCode']) && 
					(isset($donation['SalesforceResponse']['status']) && $donation['SalesforceResponse']['status'] == 'error')){
					$status_B = 
					isset($donation['SalesforceResponse']['status']) ? $donation['SalesforceResponse']['status'] : '';
					$errorCode_B = 
					isset($donation['SalesforceResponse']['errorCode']) ? $donation['SalesforceResponse']['errorCode'] : '';
					$Message_B = 
					isset($donation['SalesforceResponse']['Message']) ? $donation['SalesforceResponse']['Message'] : '';

					$salesforce_response = array(
						array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'status_B',
						        'name' => 'status_B',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Status',
							        'position'  => 'top',
						        ),
						        'description' => 'Status response.',
						        'value' => $status_B
					        )
					    ), 
					    array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'errorCode_B',
						        'name' => 'errorCode_B',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Error Code',
							        'position'  => 'top',
						        ),
						        'description' => 'Error code of response.',
						        'value' => $errorCode_B
					        )
					    ), 
					    array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'Message_B',
						        'name' => 'Message_B',
						        'readonly' => true,
						        'gridclass' => 'col-md-12 col-lg-12',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Message',
							        'position'  => 'top',
						        ),
						        'description' => 'Repsonse message.',
						        'value' => $Message_B
					        )
					    ), 
					);					
				}elseif(isset($donation['SalesforceResponse']['OpportunityId']) &&
					isset($donation['SalesforceResponse']['RecurringId']) &&
					isset($donation['SalesforceResponse']['Message'])) {
					$salesforce_response = array(
						array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'OpportunityId',
						        'name' => 'OpportunityId',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Opportunity Salesforce ID',
							        'position'  => 'top',
						        ),
						        'description' => 'Opportunity ID on PPAPI response.',
						        'value' => $OpportunityId
					        )
				        ), 
				        array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'RecurringId',
						        'name' => 'RecurringId',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Recurring Salesforce ID',
							        'position'  => 'top',
						        ),
						        'description' => 'Recurring ID on PPAPI response.',
						        'value' => $RecurringId
					        )
				        ), 
				        array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'Message',
						        'name' => 'Message',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Message',
							        'position'  => 'top',
						        ),
						        'description' => 'PPAPI response message.',
						        'value' => $Message
					        )
				        ), 
					);	
				}elseif((isset($donation['SalesforceResponse']['OpportunityId']) &&
					isset($donation['SalesforceResponse']['Message'])) && 
					(isset($donation['pdwp_donation_type']) && $donation['pdwp_donation_type'] == 'one-off')){
					$salesforce_response = array(
						array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'OpportunityId',
						        'name' => 'OpportunityId',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Opportunity Salesforce ID',
							        'position'  => 'top',
						        ),
						        'description' => 'Opportunity ID on PPAPI response.',
						        'value' => $OpportunityId
					        )
				        ), 
				        array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'Message',
						        'name' => 'Message',
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Message',
							        'position'  => 'top',
						        ),
						        'description' => 'PPAPI response message.',
						        'value' => $Message
					        )
				        ), 
					);	
				}elseif(isset($donation['SalesforceResponse']['type']) && $donation['SalesforceResponse']['type'] == 'link'){	
					
				}elseif((isset($donation['SalesforceResponse']) && !empty($donation['SalesforceResponse'])) &&
					is_array($donation['SalesforceResponse'])){
					$salesforce_response = array();

					foreach ($donation['SalesforceResponse'] as $key => $value) {
						if(is_numeric($key)) {
							$index = $key + 1;
						}else{
							$index = $key;
						}
						$salesforce_response[] = array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'errorCode_arr_'.$key,
						        'name' => 'errorCode_arr_'.$key,
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => $index.') Error',
							        'position'  => 'top',
						        ),
						        'description' => 'Error response form Salesforce.',
						        'value' => isset($value['errorCode']) ? $value['errorCode'] : '',
					        )
					    ); 
					    $salesforce_response[] = array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'message_arr_'.$key,
						        'name' => 'message_arr_'.$key,
						        'readonly' => true,
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => $index.') Error Description',
							        'position'  => 'top',
						        ),
						        'description' => 'Error description of the error response.',
						        'value' => isset($value['message']) ? $value['message'] : '',
					        )
					    );
					    $salesforce_response[] = array(
					        'type' => 'input',
					        'settings' => array(
						        'id' => 'status_code_arr_'.$key,
						        'name' => 'status_code_arr_'.$key,
						        'readonly' => true,
						        'gridclass' => 'col-md-12 col-lg-12',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => $index.') Status Code',
							        'position'  => 'top',
						        ),
						        'description' => 'Status Code of the error response.',
						        'value' => isset($value['status_code']) ? $value['status_code'] : '',
					        )
					    );
					}
				}else{
					$salesforce_response = array(
				        array(
					        'type' => 'html',
					        'settings' => array(
						        'id' => 'html3',
						        'name' => '',
						        'gridclass' => 'col-md-6 col-lg-6',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => 'Payment Response',
							        'position'  => 'top',
						        ),

						        'description' => 'Payment Response of the donation.'
					        ),
					        'content' => '<h2>Not Sync</h2>',
				        ), 
				    );
				}
			}

			$salesforce_response[] = array(
		        'type' => 'html',
		        'settings' => array(
			        'id' => 'html4',
			        'name' => '',
			        'gridclass' => 'col-md-12 col-lg-12',
			        'label'         => array(
				        'hidden'    => false,
				        'value'     => 'PPAPI Request & Response',
				        'position'  => 'top',
			        ),

			        'description' => 'PPAPI request and response made on upon donation.'
		        ),
		        'content' => '<textarea id="SalesforceLogs" name="SalesforceLogs">'.$SalesforceLogs.'</textarea>',
		    );

			$debug_details = array();
			$debug_details_notarray = array();
			$debug_details_array = array();
			foreach ($donation as $debug_field_key => $debug_field_value) {
				if (!is_array($debug_field_value)) {
					$debug_details_notarray[] = array(
						'type' => 'input',
				        'settings' => array(
					        'id' => $debug_field_key.'_debug',
					        'name' => $debug_field_key.'_debug',
					        'gridclass' => 'col-md-6 col-lg-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => $debug_field_key,
						        'position'  => 'top',
					        ),
					        'readonly' => true,
					        'value' => $debug_field_value,
					        'description' => $debug_field_key.' debug field.'
				        )
					);					
				}

				if(is_array($debug_field_value)){
					$debug_field_value = str_replace( '\\','', json_encode( $debug_field_value ) );
					$debug_details_array[] = array(
				        'type' => 'html',
				        'settings' => array(
					        'id' => 'html5',
					        'name' => '',
					        'gridclass' => 'col-md-6 col-lg-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => $debug_field_key,
						        'position'  => 'top',
					        ),

					        'description' => $debug_field_key.' debug field.'
				        ),
				        'content' => '<textarea id="'.$debug_field_key.'_debug" name="'.$debug_field_key.'_debug">'.$debug_field_value.'</textarea>',
				    );
				}
			}
			$debug_details = array_merge($debug_details_notarray, $debug_details_array);

			$donationlist_sections = array(
				array(
	                'title' => 'Record Actions',
	                'description' => '',
	                'content' => $donation_actions,
	            ),
	            array(
	                'title' => 'Donor Details',
	                'description' => 'Donor personal details and donation form details.',
	                'content' => $donor_detials,
	            ),
	            array(
	                'title' => 'Payment Transaction',
	                'description' => 'Payment transaction details of the donation.',
	                'content' => $payment_transaction,
	            ),
	            array(
	                'title' => 'Campaign and GAU',
	                'description' => 'Salesforce Campaign and GAU ID link with this donation.',
	                'content' => $campaignandgau,
	            ),
	            array(
	                'title' => 'Salesforce Payment Source Response',
	                'description' => 'PPAPI Request and Response for payment source.',
	                'content' => $salesforce_paymentsource_response,
	            ),
	           	array(
	                'title' => 'Salesforce Response',
	                'description' => 'PPAPI Request and Response.',
	                'content' => $salesforce_response,
	            ),
	        );


			/**
	         * call all custom wfc_donation_metaboxes filter
	         */
	        if (has_filter('wfc_donation_metaboxes') == true) {
	            $wfc_donation_metaboxes = apply_filters('wfc_donation_metaboxes', $donation);

	            if (is_array($wfc_donation_metaboxes) && !empty($wfc_donation_metaboxes)) {
	            	
	            	foreach ($wfc_donation_metaboxes as $key => $metaboxvalue) {
	            		$donation_metabox = array();	

			            $donation_metabox_title = 
			            isset($metaboxvalue['title']) ? 
			            $metaboxvalue['title'] : 'Donation Meta Box'.$key;

			            $donation_metabox_description = 
			            isset($metaboxvalue['description']) ? 
			            $metaboxvalue['description'] : 'Donation Meta Box'.$key;

			            $donation_metabox_slug = 
			            isset($metaboxvalue['slug']) ? 
			            $metaboxvalue['slug'] : 'donation-meta-box'.rand(1, 1000000);

			            $donation_metabox_content = 
			            isset($metaboxvalue['content']) ? 
			            $metaboxvalue['content'] : '<h1>Empty</h1>';

			            $donation_metabox[] = array(
					        'type' => 'html',
					        'settings' => array(
						        'id' => $donation_metabox_slug,
						        'name' => '',
						        'gridclass' => 'col-md-12 col-lg-12',
						        'label'         => array(
							        'hidden'    => false,
							        'value'     => '',
							        'position'  => 'top',
						        ),

						        'description' => ''
					        ),
					        'content' => $donation_metabox_content,
					    );
					    $donationlist_sections[] = array(
			                'title' => $donation_metabox_title,
			                'description' => $donation_metabox_description,
			                'content' => $donation_metabox,
			            );

	            	}
	            }

	        }



	        if($donation_debug_mode == 'true'){
	        	$donationlist_sections[] = array(
	                'title' => 'Debug Details',
	                'description' => 'Donation data.',
	                'content' => $debug_details,
	            );
	        }

			$setting = get_option( 'pronto_donation_settings', 0 );
			Saiyan::tist()->render_module_mapping(array(
				'donation_form' => array(
			        'type' => 'accordion',
			        'settings' => array(
			            'id' => 'donation_form'
			        ),
			        'sections' => $donationlist_sections
			    ),
				'navigator' => array(
					'type' => 'navigator',
					'settings' => array(
						'id' => 'navigator',
						'class' => 'navigator',
						'link_to_accordion' => 'donation_form'
					),
					'way_points' => array(
						'donation_form' => 'Donation Details'
					)
				)
			));
			?>
		<?php else: ?>
			<div><h2>No donation records.</h2></div>
		<?php endif;?>
	<?php 
	}else{
		?><textarea id="donation_olddata" name="donation_olddata"><?php echo $donation_olddata ;?> </textarea><?php
	} ?>
	<?php else: ?>
		<div><h2><?php echo __('Please provide meta ID.', 'webforce-connect-donation'); ?></h2></div>
	<?php endif; ?>
</div>
<?php if(is_admin()): ?>
	<?php if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart'):?>
	<?php else: ?>

        <?php if($sync_statuses == 'attemp_reach'|| $sync_statuses == 'failed_sync' || $sync_statuses == 'recurring_failed_sync' || $sync_statuses == 'not_sync'):?>
        <div id="wfc-donation-list-info" class="wfc-donation-list-info">
            <div class="pins">
                <div class="info-loading-icon" style="margin: 5px;">
                    <?php require_once( WFC_PLUGIN_BASEDIR . 'svg/webforce-connect-donation-admin-config.php' ); ?>
                </div>
            </div>
            <div class="notes">
                <div class="info-list-container show" style="width: 500px;">
                    <h4 class="info-header"><?php echo __('Admin Tool', 'webforce-connect-donation'); ?></h4>
                    <div class="info-content">
                        <small><?php echo __('By clicking the Re-sync button, this donation will be sync to Salesforce and updates this donation to "Success" sync status. This button will bypass the sync limit restriction functionality of the Donation plugin. This button is only available if the current user is an administrator.', 'webforce-connect-donation'); ?></small>
                        <div id="wfc_forcesyncdonation-container" class="saiyan-module-wrapper saiyan-button wordpress" data-saiyan-module="button" style="margin-top: 10px;">
                            <div class="saiyan-module">
                                <button id="wfc_forcesyncdonation" class="saiyan-button " name="wfc_forcesyncdonation" type="button"><i id="wfc-don-forcesyncdonation-icon" class="fa fa-refresh" style="margin-right: 8px;"></i>Re-sync</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>

	<?php endif; ?>
<?php endif; ?>