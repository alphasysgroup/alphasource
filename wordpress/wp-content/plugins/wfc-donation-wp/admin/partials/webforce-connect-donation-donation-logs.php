<?php 
/**
* @author: Junjie Canonio <junjie@alphasys.com.au>
*/
?>
<?php if(!isset($_GET['donationlogs'])): ?>
<a class="wfc-don-menu-btn-link" href="<?php echo admin_url( 'admin.php?page=wfc-donation&donationlogs=1' ); ?>">
  <div id="wfc-don-menu-btn" class="wfc-don-menu-btn">
    <div class="container-fluid">
      <div class="row">
        <div class="col-2 wfc-don-menu-btn-icon"><i class="fa fa-file-archive-o"></i></div>
        <div class="col-10 wfc-don-menu-btn-desc">
          <h3><?php echo __('Donation logs', 'webforce-connect-donation'); ?></h3>
          <em><?php echo __('Monitor the donor donation activities.', 'webforce-connect-donation'); ?></em>
        </div>
      </div>
    </div>
  </div>
</a>  	
<?php else: ?>
<?php
  $donation_logs_page = home_url().'/wp-admin/edit.php?post_type=wfc_donation_logs';
  wp_redirect( $donation_logs_page );
  exit;
?>   
<?php endif; ?>