<?php
/**
 * @author: Junjie Canonio
 */
wp_enqueue_style( 'wfc-donation-sfinstance-css' );
wp_enqueue_script( 'wfc-donation-sfinstance-js' );

?>
<?php if(isset($_GET['sfinstancelist']) && $_GET['sfinstancelist'] == 1): ?>
<div class="wfc-don-sfinstance-main-cont">
    <h2><?php echo __('Salesforce Instances', 'webforce-connect-donation'); ?></h2>
    <div class="wfc-don-sfinstance-body-cont">
        <div class="wfc-don-gateway-list-container" style="padding: 20px;">
            <div class="container-fluid wfc-don-gateway-list-item-title">
                <div class="row">
                    <div class="col-3">
                        <span><?php echo __('Name', 'webforce-connect-donation'); ?></span>
                    </div>
                    <div class="col-4">
                        <span><?php echo __('Slug', 'webforce-connect-donation'); ?></span>
                    </div>
                    <div class="col-3">
                        <span><?php echo __('URL', 'webforce-connect-donation'); ?></span>
                    </div>
                    <div class="col-1">
                        <span><?php echo __('Status', 'webforce-connect-donation'); ?></span>
                    </div>
                    <div class="col-1">
                        <span><?php echo __('', 'webforce-connect-donation'); ?></span>
                    </div>
                </div>
            </div>
            <ul id="wfc-don-easyPaginate-sfinstance" class="list-group">
                <li class="list-group-item"><div>Fetching Salesforce Instances ...... </div></li>
            </ul>
        </div>
	    <?php
	    Saiyan::tist()->render_module_mapping(array(
		    'wfc_donation_back_btn' => array(
			    'type' => 'button',
			    'settings' => array(
				    'id'    => 'wfc_donation_back_btn',
				    'class' => '',
				    'name'  => 'wfc_donation_back_btn',
				    'style' => 'margin:0px 20px 10px 20px;',
				    'label'     => array(
					    'hidden'    => true,
					    'value'     => '',
					    'position'  => 'top',
				    ),
				    'content' => 'Back'
			    )
		    ),
	    ));
	    ?>
    </div>
</div>
<input type="hidden" id="wfc-don-back-url" value="<?php echo esc_url(admin_url( 'admin.php?page=wfc-donation')); ?>">
<script>
    /**
     * @author: Junjie Canonio
     */
    jQuery(document).ready(function($){
        // go back to payment gateway lists
        $('#wfc_donation_back_btn').on('click', function() {
            var backbuttonurl = $('#wfc-don-back-url').val();
            window.location.replace(backbuttonurl);
        });
    });
</script>
<?php else: ?>
	<a class="wfc-don-menu-btn-link" href="<?php echo admin_url( 'admin.php?page=wfc-donation&sfinstancelist=1' );?>">
		<div id="wfc-don-menu-btn" class="wfc-don-menu-btn">
			<div class="container-fluid">
				<div class="row">
					<div class="col-2 wfc-don-menu-btn-icon"><i class="fa fa-cloud"></i></div>
					<div class="col-10 wfc-don-menu-btn-desc">
						<h3><?php echo __('Salesforce Instance', 'webforce-connect-donation'); ?></h3>
						<em><?php echo __('Handle and configure the available Salesforce Instances.', 'webforce-connect-donation'); ?></em>
					</div>
				</div>
			</div>
		</div>
	</a>
<?php endif; ?>
