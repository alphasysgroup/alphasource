<?php
/**
 * @author: Junjie Canonio
 */
wp_enqueue_style( 'wfc-donation-sfinstance-css' );
wp_enqueue_script( 'wfc-donation-sfinstance-js' );



$sfinstanceID = isset($_GET['sfinstanceID']) ? $_GET['sfinstanceID'] : '';
$instances_option = get_option($sfinstanceID);
$salesforceinstance_slug = isset($instances_option['wfc_base_salesforceinstance_slug']) ? $instances_option['wfc_base_salesforceinstance_slug'] : '';
$salesforceinstance_name = isset($instances_option['wfc_base_salesforceinstance_name']) ? $instances_option['wfc_base_salesforceinstance_name'] : '';
$instancetype = isset($instances_option['wfc_base_instancetype']) ? strtoupper($instances_option['wfc_base_instancetype']) : '';

$instance_connection = WFC_Base_MultiSFInstanceOauth::test_connection($salesforceinstance_slug);
$instance_connection_status = isset($instance_connection['status']) ? $instance_connection['status'] : 'failed';
$instance_connection_message = isset($instance_connection['message']) ? $instance_connection['message'] : '';

?>
<input type="hidden" id="wfc_don_salesforceinstance" value="<?php echo $salesforceinstance_slug; ?>">
<div class="wfc-don-sfinstance-main-cont">
	<h2><?php echo __('Salesforce Instance', 'webforce-connect-donation'); ?></h2>
	<div class="wfc-don-sfinstance-body-cont">
		<div class="wfc-don-sfinstance-info-cont">
			<div class="wfc-don-sfinstance-info-head"><span>Instance Information</span></div>
			<div class="wfc-don-sfinstance-info-body container-fluid">
				<div class="row">
					<div class="col-6">
						<div>
							<span class="wfc-don-sfinstance-label"><?php echo __('ID : ', 'webforce-connect-donation'); ?></span>
							<span class="wfc-don-sfinstance-value"><?php echo __($salesforceinstance_slug, 'webforce-connect-donation'); ?></span>
						</div>
						<div>
							<span class="wfc-don-sfinstance-label"><?php echo __('Name : ', 'webforce-connect-donation'); ?></span>
							<span class="wfc-don-sfinstance-value"><?php echo __($salesforceinstance_name, 'webforce-connect-donation'); ?></span>
						</div>
						<div>
							<span class="wfc-don-sfinstance-label"><?php echo __('Type : ', 'webforce-connect-donation'); ?></span>
							<span class="wfc-don-sfinstance-value"><?php echo __($instancetype, 'webforce-connect-donation'); ?></span>
						</div>
					</div>
                    <div class="col-6">
                        <div>
                            <span class="wfc-don-sfinstance-label"><?php echo __('Connection Status : ', 'webforce-connect-donation'); ?></span>
                            <?php if($instance_connection_status == 'success'): ?>
                                <span class="wfc-don-sfinstance-value" style="font-weight: 600; color: green;"><?php echo __('Success', 'webforce-connect-donation'); ?></span>
                            <?php else: ?>
                                <span class="wfc-don-sfinstance-value" style="font-weight: 600; color: red;"><?php echo __('Failed', 'webforce-connect-donation'); ?></span>
                            <?php endif; ?>
                        </div>
                        <div>
                            <span class="wfc-don-sfinstance-value"><?php echo __($instance_connection_message, 'webforce-connect-donation'); ?></span>
                        </div>
                    </div>
				</div>
			</div>
		</div>
		<div class="wfc-don-sfinstance-settings-cont">
			<div class="wfc-don-sfinstance-settings-head"><span>Settings</span></div>
			<div class="wfc-don-sfinstance-settings-body container-fluid">
                <div class="wfc_donation_settings_main_container">
                    <?php
                    $settings = get_option('wfc_donation_'.$salesforceinstance_slug.'_settings');

                    $donation_dnsr_single = isset($settings['donation_dnsr_single']) ? $settings['donation_dnsr_single'] : 'true';
                    $donation_dnsr_single = ($donation_dnsr_single == 'true') ? 'on' : 'off';
                    $donation_dnsr_recurring = isset($settings['donation_dnsr_recurring']) ? $settings['donation_dnsr_recurring'] : 'true';
                    $donation_dnsr_recurring = ($donation_dnsr_recurring == 'true') ? 'on' : 'off';
                    $children = array(
	                    'donation_dnsr_single' => array(
		                    'type' => 'switch',
		                    'settings' => array(
			                    'id'       => 'donation_dnsr_single',
			                    'class'    => 'donation_dnsr_single',
			                    'name'     => 'donation_dnsr_single',
			                    'switch'   => $donation_dnsr_single,
			                    'gridclass' => 'col-md-6 col-lg-6',
			                    'required' => false,
			                    'readonly' => false,
			                    'label'         => array(
				                    'hidden'    => false,
				                    'value'     => 'Send receipt - One-off Donations',
				                    'position'  => 'top',
			                    ),
			                    'description' => 'This will enable/disable sending receipt for one-off donations.'
		                    )
	                    ),
	                    'donation_dnsr_recurring' => array(
		                    'type' => 'switch',
		                    'settings' => array(
			                    'id'       => 'donation_dnsr_recurring',
			                    'class'    => 'donation_dnsr_recurring',
			                    'name'     => 'donation_dnsr_recurring',
			                    'switch'   => $donation_dnsr_recurring,
			                    'gridclass' => 'col-md-6 col-lg-6',
			                    'required' => false,
			                    'readonly' => false,
			                    'label'         => array(
				                    'hidden'    => false,
				                    'value'     => 'Send receipt - Recurring Donations',
				                    'position'  => 'top',
			                    ),
			                    'description' => 'This will enable/disable sending receipt for recurring donations.'
		                    )
	                    ),
	                    'wfc_resync_campaign_gau' => array(
		                    'type' => 'button',
		                    'settings' => array(
			                    'id'    => 'wfc_resync_campaign_gau',
			                    'class' => '',
			                    'name'  => 'wfc_resync_campaign_gau',
			                    'gridclass' => 'col-md-12 col-lg-12',
			                    'style' => '',
			                    'label'     => array(
				                    'hidden'    => false,
				                    'value'     => 'Re-sync GAU and Campaign',
				                    'position'  => 'top',
			                    ),
			                    'content' => '<i id="wfc-don-fetch-gaucamp-icon" class="fa fa-refresh" style="margin-right: 8px;"></i>Re-sync',
			                    'description' => 'By clicking the re-sync button, all GAU and Campaign are fetch from the Salesforce organization connected.'
		                    )
	                    ),
	                    'wfc_donation_saveinssettings_btn' => array(
		                    'type' => 'button',
		                    'settings' => array(
			                    'id'    => 'wfc_donation_saveinssettings_btn',
			                    'class' => 'wfc_donation_saveinssettings_btn',
			                    'name'  => 'wfc_donation_saveinssettings_btn',
			                    'type'  => 'button',
			                    'style' => 'float:left;margin-top:20px;',
			                    'gridclass'=> 'col-12',
			                    'label'     => array(
				                    'hidden'    => true,
				                    'value'     => '',
				                    'position'  => 'top',
			                    ),
			                    'content' => 'SAVE'
		                    )
	                    ),
                    );
					Saiyan::tist()->render_module_mapping(array(
						'wfc_donation_'.$salesforceinstance_slug.'_settings' => array(
							'type' => 'settingsform',
							'settings' => array(
								'id'        => 'wfc_donation_'.$salesforceinstance_slug.'_settings',
								'class'     => '',
								'action'    => '',
								'name'      => 'wfc_donation_'.$salesforceinstance_slug.'_settings',
								'style'     => 'width: 100%;',
								'title'     => '',
								'action'    => '',
								'method'    => 'post',
								'wp_option' => array(
									'enable'     => true,
									'slug_name'  => 'wfc_donation_'.$salesforceinstance_slug.'_settings',
								),
								'submit_id' => 'wfc_donation_saveinssettings_btn',
							),
							'children' => $children,
						),
					));
					?>
                </div>
			</div>
		</div>

		<div class="wfc-don-sfinstance-gateway-cont">
			<div class="wfc-don-sfinstance-gateway-head"><span>Payment Gateway</span></div>
			<div class="wfc-don-sfinstance-gateway-body container-fluid">
				<?php require_once'webforce-connect-donation-payment-gateway.php'; ?>
			</div>
		</div>
        <div class="wfc-don-sfinstance-btn-cont">
	        <?php
	        Saiyan::tist()->render_module_mapping(array(
		        'wfc_donation_back_btn' => array(
			        'type' => 'button',
			        'settings' => array(
				        'id'    => 'wfc_donation_back_btn',
				        'class' => '',
				        'name'  => 'wfc_donation_back_btn',
				        'style' => 'margin:0px 20px 10px 0px;',
				        'label'     => array(
					        'hidden'    => true,
					        'value'     => '',
					        'position'  => 'top',
				        ),
				        'content' => 'Back'
			        )
		        ),
	        ));
	        ?>
        </div>



	</div>
</div>
<input type="hidden" id="wfc-don-back-url" value="<?php echo esc_url(admin_url( 'admin.php?page=wfc-donation&sfinstancelist=1')); ?>">
<script>
    /**
     * @author: Junjie Canonio
     */
    jQuery(document).ready(function($){
        // go back to payment gateway lists
        $('#wfc_donation_back_btn').on('click', function() {
            var backbuttonurl = $('#wfc-don-back-url').val();
            window.location.replace(backbuttonurl);
        });
    });
</script>