<?php 
/**
* @author: Junjie Canonio <junjie@alphasys.com.au>
*/
if(isset($_GET['sfinstancelist']) && $_GET['sfinstancelist'] == 1):
$sfinstancelist_path = plugin_dir_path( __FILE__ ).'webforce-connect-donation-sfinstance-list.php';
require_once $sfinstancelist_path;

elseif(isset($_GET['sfinstanceID']) && !empty($_GET['sfinstanceID'])):
$sfinstance_path = plugin_dir_path( __FILE__ ).'webforce-connect-donation-sfinstance.php';
require_once $sfinstance_path;

elseif(isset($_GET['gatewaylist']) && $_GET['gatewaylist'] == 1):
$gatewaylist_path = plugin_dir_path( __FILE__ ).'webforce-connect-donation-payment-gateway.php';
require_once $gatewaylist_path;

elseif((isset($_GET['gatewayID']) && !empty($_GET['gatewayID'])) && (isset($_GET['gateway']) && !empty($_GET['gateway']))):
$gateway = isset($_GET['gateway']) ? strtolower($_GET['gateway']) : '';
$gateway_path = plugin_dir_path( __FILE__ ).'../../payment-gateway/'.$gateway.'/settings.php'; 
require_once $gateway_path;

elseif(isset($_GET['settings']) && $_GET['settings'] == 1): 
$settings_path = plugin_dir_path( __FILE__ ).'webforce-connect-donation-settings-2.php';
require_once $settings_path;

elseif(isset($_GET['donationlogs']) && $_GET['donationlogs'] == 1): 
$donationlogs_path = plugin_dir_path( __FILE__ ).'webforce-connect-donation-donation-logs.php';
require_once $donationlogs_path;
?>
<?php else: ?>
<div class="wfc_don_settings_main_cont">
	<h2><?php echo __('Webforce Connect - Donation', 'webforce-connect-donation'); ?></h2>
	<div class="wfc-menu-container">
		<div class="container-fluid">
			<div class="row">
				<div class="col-6">
                <?php require_once'webforce-connect-donation-settings-2.php'; ?>
                <?php require_once'webforce-connect-donation-sfinstance-list.php'; ?>
                <?php require_once'webforce-connect-donation-donation-form.php'; ?>
                <?php require_once'webforce-connect-donation-donation-logs.php'; ?>
				</div>
			</div>
		</div>
	</div>	
</div>
<style type="text/css">

</style>
<?php endif; ?>



<!-- <script
    src="https://www.paypal.com/sdk/js?client-id=AW_ebsryLF05Zp1eU5fUdrGkHeSzPkv3XB0RmphR3pLR5BjSqadmT0SIlrdcMctdv1tJPM8mDQjVK9d7&commit=true&intent=capture">
</script>
<div id="paypal-button-container"></div>
<script>
    console.log(paypal);
    paypal.Buttons({
        client: {
            sandbox:    'AVzXHq-nUYBUx9W1GBOntg8PM8IcCk2L_SSupq616oBUE5WupUjkoNJDIWOytowOmISP8g6S1AuQL4Qf',
            production: 'AVzXHq-nUYBUx9W1GBOntg8PM8IcCk2L_SSupq616oBUE5WupUjkoNJDIWOytowOmISP8g6S1AuQL4Qf'
        },

        style: {
            layout:  'horizontal',
            label: 'pay',  // checkout | credit | pay | buynow | generic
            size:  'responsive', // small | medium | large | responsive
            shape: 'rect',   // pill | rect
            color: 'blue',   // gold | blue | silver | black
            tagline: false
        },

        commit: true,

        createOrder: function(data, actions) {
            console.log('createOrder');

            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: '0.01'
                    }
                }]
            })
        },

        onApprove: function(data, actions) {
            return actions.order.capture().then(function(details) {
                // alert('Transaction completed by ' + details.payer.name.given_name);
                console.log(details);
                // Call your server to save the transaction
                
                // return fetch('/paypal-transaction-complete', {
                //     method: 'post',
                //     body: JSON.stringify({
                //         orderID: data.orderID
                //     })
                // });
                
            });
        }
    }).render('#paypal-button-container');
</script> -->