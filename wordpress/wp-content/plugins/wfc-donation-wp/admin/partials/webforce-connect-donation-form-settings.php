<link rel="stylesheet" id="wp-mediaelement-css" href="http://localhost/donation/wp-includes/js/mediaelement/wp-mediaelement.min.css?ver=4.9.7" type="text/css" media="all">
<style>
    #donation_v5_form_user_fields-container .saiyan-module-wrapper {
        overflow: hidden;
    }

    #donation_v5_form_user_fields-container .force-hide {
        display: none !important;
    }
</style>
<?php
/*
wp_deregister_script('wp-mediaelement');
wp_deregister_style('wp-mediaelement');
wp_enqueue_style('wp-mediaelement');
wp_enqueue_script('wp-mediaelement');
*/

wp_enqueue_media();

wp_enqueue_script('wp-mediaelement');
wp_enqueue_script('webforce-connect-donation-form-settings-js');
wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
wp_enqueue_script('webforce-connect-geo-js');

$prefix = 'donation';

/*
 * get Salesforce instances
 */
$instances = WFC_DonationFormSettings::get_sfinstances();
$sfinstances = WFC_DonationFormSettings::get_sfinstances_selectoption();


/*
 * get frequencies on settings config
 */
$setting = get_option( 'pronto_donation_settings', 0 );
$freq = !empty( $setting ) ? $setting->frequencies : null;

global $post;
$post_meta_form_values = get_post_meta($post->ID);

// TODO do reconstruct on donation types + checking
$checkbox_donation_types = array();
$checkbox_donation_types_checked = array();
if($freq !== null){
	$freq_arr = json_decode($freq, true);
	foreach ($freq_arr as $key => $value) {
		$item = $value[key($value)];
		if ($item['values']['frequency-switch'] === 'true') {
			$checkboxes['title'] = $item['values']['frequency-name'];
			$checkboxes['value'] = $item['values']['frequency-value'];

			array_push($checkbox_donation_types, $checkboxes);
		}
	}
}


$select_template_mode_normal = array();
foreach (WFC_DonationFormSettings::_get_pdwp_templates() as $key => $value) {
	$options['title'] = $value;
	$options['value'] = $key;

	array_push($select_template_mode_normal, $options);
}

$select_template_mode_wizard = array();
foreach (WFC_DonationFormSettings::_get_pdwp_templates2() as $key => $value) {
	$options['title'] = $value;
	$options['value'] = $key;

	array_push($select_template_mode_wizard, $options);
}

if (isset($post_meta_form_values[$prefix . '_form_donation_types'][0])) {
	foreach (unserialize($post_meta_form_values[$prefix . '_form_donation_types'][0]) as $key => $value) {
		if ($value === 'true') array_push($checkbox_donation_types_checked, $key);
	}
}
	
/**
 * @param $sortable_list_gateways
 * @param $gateway_key
 *
 * @return bool
 */
function _wfc_is_published_gateway_exist($sortable_list_gateways, $gateway_key) {
	foreach ($sortable_list_gateways as $key => $value) {
		if ($value['settings']['checkboxes'][0]['value'] == $gateway_key) {
			return true;
		}
	}

	return false;
}


$pages = get_pages();
$list_page = array();
foreach ( $pages as $page ) {
	$list['title'] = $page->post_title;
	$list['value'] = $page->ID;

	array_push($list_page, $list);
}

$thankyou_email_test_prereq = (isset($post_meta_form_values['donation_form_from_email'][0]) && $post_meta_form_values['donation_form_from_email'][0] !== '') &&
                              (isset($post_meta_form_values['donation_form_from_name'][0]) && $post_meta_form_values['donation_form_from_name'][0] !== '');

$admin_notification_test_prereq = $thankyou_email_test_prereq &&
                                  (isset($post_meta_form_values['donation_form_recipient_email'][0]) && $post_meta_form_values['donation_form_recipient_email'][0]);

$country = WFC_DonationFormSettings::getCountryList();

foreach ($country as &$ctry) {
    $ctry['value'] = $ctry['title'];
}



/*
 * Set up general tab ================================================================================================================================
 */
$general_content = array();
$general_content[$prefix . '_form_version'] = array(
	'type' => 'input',
	'settings' => array(
		'id' => $prefix . '_form_version',
		'name' => $prefix . '_form_version',
		'type' => 'hidden',
		'value' => 'version_5',
		'label' => array(
			'hidden' => true
		)
	)
);

$general_content[$prefix . '_form_sfinstance'] = array(
	'type' => 'select',
	'settings' => array(
		'id' => $prefix . '_form_sfinstance',
		'name' => $prefix . '_form_sfinstance',
		'gridclass' => 'col-md-12',
		'label'         => array(
			'hidden'    => false,
			'value'     => 'Salesforce Instance',
			'position'  => 'top',
		),
		'description' => 'Set the Saleforce Organization for this donation form.',
		'options' => $sfinstances,
		'selected' => isset($post_meta_form_values[$prefix . '_form_sfinstance'][0]) ? $post_meta_form_values[$prefix . '_form_sfinstance'][0] : ''
	)
);
foreach ($instances as $key => $value) {
	$instance_name = isset($value['name']) ? $value['name'] : '';
	$instance_slug = isset($value['slug']) ? $value['slug'] : '';

	/*
     * Fetch GAU and Campaign GAU
     */
	$pdwp_ASSFAPI_campaign = get_option( 'pdwp_ASSFAPI/campaign_'.$instance_slug, array());
	$pdwp_ASSFAPI_campaign = ( is_array($pdwp_ASSFAPI_campaign) ) ? $pdwp_ASSFAPI_campaign : array();
	$pdwp_ASSFAPI_gau = get_option( 'pdwp_ASSFAPI/gau_'.$instance_slug, array() );
	$pdwp_ASSFAPI_gau = ( is_array($pdwp_ASSFAPI_gau) ) ? $pdwp_ASSFAPI_gau : array();

	$select_sf_campaign = array();
	array_push($select_sf_campaign, array('title' => '--Select--', 'value' => ''));
	foreach ($pdwp_ASSFAPI_campaign as $key => $value) {
		$options['title'] = $value;
		$options['value'] = $key;

		array_push($select_sf_campaign, $options);
	}

	$select_sf_gau = array();
	array_push($select_sf_gau, array('title' => '--Select--', 'value' => ''));
	foreach ($pdwp_ASSFAPI_gau as $key => $value) {
		$options['title'] = $value;
		$options['value'] = $key;

		array_push($select_sf_gau, $options);
	}

	$general_content[$prefix . '_form_sf_campaign_'.$instance_slug] = array(
		'type' => 'select',
		'settings' => array(
			'id' => $prefix . '_form_sf_campaign_'.$instance_slug,
			'name' => $prefix . '_form_sf_campaign_'.$instance_slug,
			'class' => $prefix . '_form_sf_campaign',
			'gridclass' => 'col-md-6',
			'label' => array(
				'hidden' => false,
				'value' => 'Salesforce Campaign',
				'position' => 'top',
			),
			'description' => 'Set a campaign intended for this form.',
			'options' => $select_sf_campaign,
			'selected' => isset($post_meta_form_values[$prefix . '_form_sf_campaign_'.$instance_slug][0]) ? $post_meta_form_values[$prefix . '_form_sf_campaign_'.$instance_slug][0] : ''
		)
	);
	$general_content[$prefix . '_form_sf_gau_'.$instance_slug] = array(
		'type' => 'select',
		'settings' => array(
			'id' => $prefix . '_form_sf_gau_'.$instance_slug,
			'name' => $prefix . '_form_sf_gau_'.$instance_slug,
			'class' => $prefix . '_form_sf_gau',
			'gridclass' => 'col-md-6',
			'label' => array(
				'hidden' => false,
				'value' => 'Salesforce GAU',
				'position' => 'top',
			),
			'description' => 'Set a gau intended for this form.',
			'options' => $select_sf_gau,
			'selected' => isset($post_meta_form_values[$prefix . '_form_sf_gau_'.$instance_slug][0]) ? $post_meta_form_values[$prefix . '_form_sf_gau_'.$instance_slug][0] : ''
		)
	);
}
$general_content[$prefix . '_form_gift'] = array(
	'type' => 'switch',
	'settings' => array(
		'id' => $prefix . '_form_gift',
		'name' => $prefix . '_form_gift'    ,
		'gridclass' => 'col-md-6',
		'label'         => array(
			'hidden'    => false,
			'value'     => 'Show Tribute Field ?',
			'position'  => 'top',
		),
		'description' => 'If enable, the tribute field will be available to form.',
		'switch' => isset($post_meta_form_values[$prefix . '_form_gift'][0]) && $post_meta_form_values[$prefix . '_form_gift'][0] === 'true' ? 'on' : 'off'
	)
);
$general_content[$prefix . '_form_gift_caption'] = array(
	'type' => 'input',
	'settings' => array(
		'id' => $prefix . '_form_gift_caption',
		'name' => $prefix . '_form_gift_caption',
		'gridclass' => 'col-md-6',
		'type' => 'text',
		'label'         => array(
			'hidden'    => false,
			'value'     => 'Tribute Field Caption',
			'position'  => 'top',
		),
		'description' => 'Allow admin to change tribute field caption.',
		'value' => isset($post_meta_form_values[$prefix . '_form_gift_caption'][0]) ? $post_meta_form_values[$prefix . '_form_gift_caption'][0] : 'This gift is in memory',
	)
);
$general_content['separator_1'] = array(
	'type' => 'separator',
	'settings' => array(
		'visibility' => 'hidden'
	)
);

$general_content[$prefix . '_form_donation_types'] = array(
	'type' => 'checkbox',
	'settings' => array(
		'id' => $prefix . '_form_donation_types',
		'name' => $prefix . '_form_donation_types',
		'gridclass' => 'col-md-6',
		'label'         => array(
			'hidden'    => false,
			'value'     => 'Donation Types',
			'position'  => 'top',
		),
		'description' => ( empty( $checkbox_donation_types ) ) ?
			"<p style='color: #f78484;font-size: 11px;font-style: italic;'>Please set frequencies on settings page.<p>" :
			esc_html__( 'Select donation type you wish to be on donation form.', 'pronto-donation' ),
		'checkboxes' => $checkbox_donation_types,
		'checked' => $checkbox_donation_types_checked
	)
);
$general_content[$prefix . '_form_filter'] = array(
	'type' => 'input',
	'settings' => array(
		'id' => $prefix . '_form_filter',
		'name' => $prefix . '_form_filter',
		'gridclass' => 'col-md-6',
		'label' => array(
			'value' => 'Donation form filter'
		),
		'description' => 'Use this field to broaden search filter this donation form.',
		'value' => isset($post_meta_form_values[$prefix . '_form_filter'][0]) ? $post_meta_form_values[$prefix . '_form_filter'][0] : ''
	)
);
/*
 * Set up general tab ================================================================================================================================
 */


/*
 * Set up payment fields tab ================================================================================================================================
 */
$paymentfields =  array();
foreach ($instances as $key => $value){
	$instance_name = isset($value['name']) ? $value['name'] : '';
	$instance_slug = isset($value['slug']) ? $value['slug'] : '';


	$sortable_list_gateways = array();
	$index = 0;
	foreach (WFC_DonationFormSettings::pd_get_donation_gateway_list($instance_slug) as $key => $value) {
		$item = array(
			'index' => $index++,
			'settings' => array(
				'checkboxes' => array(
					array(
						'title' => $value['payment_gateway_name'],
						'value' => $value['payment_gateway_key']
					)
				)
			)
		);

		array_push($sortable_list_gateways, $item);
	}

	$hold_sortable_list_gateways = $sortable_list_gateways;

	if (isset($post_meta_form_values[$prefix . '_v5' . '_form_payment_gateways_'.$instance_slug][0])) {
		$form_payment_gateways = json_decode($post_meta_form_values[$prefix . '_v5' . '_form_payment_gateways_'.$instance_slug][0], true);
		$sortable_list_gateways = array();
		if (isset($form_payment_gateways)) {
			$index = 0;
			$keys = array();
			foreach ($form_payment_gateways as $gateway) {
				$checked = array();
				foreach ($gateway as $key => $value) {
					$checked = array();
					// echo $key;
					$item = array(
						'index' => $index++,
						'settings' => array(
							'checkboxes' => array(
								array(
									'title' => $value['title'],
									'value' => $key
								)
							)
						)
					);

					if ($value['value'] == 'true') array_push($checked, $key);

					$item['settings']['checked'] = $checked;

					array_push($keys, $key);
				}

				if (_wfc_is_published_gateway_exist($hold_sortable_list_gateways, $key)){
					array_push($sortable_list_gateways, $item);
                }

			}

			if (count($sortable_list_gateways) < count($hold_sortable_list_gateways)) {
				foreach ($hold_sortable_list_gateways as $key => $value) {
					if (! in_array($value['settings']['checkboxes'][0]['value'], $keys)) {
						array_push($sortable_list_gateways, $value);
					}
				}
			}
		}
	}

	$paymentfields[$prefix . '_v5' . '_form_payment_gateways_'.$instance_slug] = array(
		'type' => 'sortable-item',
		'settings' => array(
			'id' => $prefix . '_v5' . '_form_payment_gateways_'.$instance_slug,
			'name' => $prefix . '_v5' . '_form_payment_gateways_'.$instance_slug,
			'mode' => 'basic',
			'base_title_id' => $prefix . '_v5' . '_form_payment_gateways_'.$instance_slug,
			'label' => array(
				'value' => $instance_name.' - Payment Gateways'
			),
			'description' => '',
			'dependency' => array(
				'id' => $prefix . '_form_sfinstance',
				'trigger_value' => $instance_slug,
				'trigger_action' => 'show_hide',
			),
		),
		'item_struct' => array(
			$prefix . '_v5' . '_form_payment_gateways_'.$instance_slug => array(
				'type' => 'checkbox',
				'settings' => array(
					'id' => $prefix . '_v5' . '_form_payment_gateways_'.$instance_slug,
					'name' => $prefix . '_v5' . '_form_payment_gateways_'.$instance_slug,
					'label' => array(
						'hidden' => true
					),
					'checkboxes' => array(
						array(
							'title' => '',
							'value' => 'qwe'
						)
					)
				)
			)
		),
		'item_values' => $sortable_list_gateways
	);
}


$paymentfields[$prefix . '_form_separator_payment_gateways'] = array(
    'type' => 'separator',
    'settings' => array(
        'id' => $prefix . '_form_separator_payment_gateways',
        'name' => $prefix . '_form_separator_payment_gateways',
        'visibility' => 'visible',
        'size' => 10,
        'align' => 'center',
        'width' => '100%',
        'style' => 'border-bottom: 1px #f5f5f5 solid; margin: 25px 0;'
    )
);
$paymentfields[$prefix . '_form_credit'] = array(
    'type' => 'sortable-item',
    'settings' => array(
        'id' => $prefix . '_form_credit',
        'name' => $prefix . '_form_credit',
        'gridclass' => 'col-md-6',
        'mode' => 'advance',
        'add_item' => false,
        'base_title_id' => 'label',
        'width' => '100%',
        'label' => array(
            'value' => 'Credit'
        ),
        'description' => '<a class="donation-form-credit-reset" id="donation-reset-credit">Reset Data<div class="cherry-spinner-campaign"><div class="double-bounce-2"></div></div></a>'
    ),
    'item_struct' => array(
        'label' => array(
            'type' => 'input',
            'settings' => array(
                'id' => 'label',
                'name' => 'label',
                'label' => array(
                    'value' => 'Label'
                )
            )
        ),
        'placeholder' => array(
            'type' => 'input',
            'settings' => array(
                'id' => 'placeholder',
                'name' => 'placeholder',
                'label' => array(
                    'value' => 'Placeholder'
                )
            )
        ),
        'original_label' => array(
            'type' => 'input',
            'settings' => array(
                'id' => 'original_label',
                'name' => 'original_label',
                'type' => 'hidden',
                'label' => array(
                    'hidden' => true
                )
            )
        )
    ),
    'item_values' => Saiyan_Module_SortableItem::get_value(
        isset($post_meta_form_values[$prefix . '_form_credit'][0]) ? $post_meta_form_values[$prefix . '_form_credit'][0] : null,
        array(
            array(
                'index' => 0,
                'values' => array(
                    'label' => 'Card Number',
                    'placeholder' => '',
                    'original_label' => 'card_number',
                ),
                'non_deletable' => true
            ),
            array(
                'index' => 1,
                'values' => array(
                    'label' => 'Exp. Month',
                    'placeholder' => '',
                    'original_label' => 'expiry_month'
                ),
                'non_deletable' => true
            ),
            array(
                'index' => 2,
                'values' => array(
                    'label' => 'Exp. Year',
                    'placeholder' => '',
                    'original_label' => 'expiry_year'
                ),
                'non_deletable' => true
            ),
            array(
                'index' => 3,
                'values' => array(
                    'label' => 'Name of Card',
                    'placeholder' => '',
                    'original_label' => 'name_on_card'
                ),
                'non_deletable' => true
            ),
            array(
                'index' => 4,
                'values' => array(
                    'label' => 'CCV',
                    'placeholder' => '',
                    'original_label' => 'ccv'
                ),
                'non_deletable' => true
            )
        )
    )
);
$paymentfields[$prefix . '_form_debit'] = array(
    'type' => 'sortable-item',
    'settings' => array(
        'id' => $prefix . '_form_debit',
        'name' => $prefix . '_form_debit',
        'gridclass' => 'col-md-6',
        'mode' => 'advance',
        'add_item' => false,
        'base_title_id' => 'label',
        'width' => '100%',
        'label' => array(
            'value' => 'Debit'
        ),
        'description' => '<a class="donation-form-debit-reset" id="donation-reset-debit">Reset Data<div class="cherry-spinner-campaign"><div class="double-bounce-2"></div></div></a>'
    ),
    'item_struct' => array(
        'label' => array(
            'type' => 'input',
            'settings' => array(
                'id' => 'label',
                'name' => 'label'
            )
        ),
        'placeholder' => array(
            'type' => 'input',
            'settings' => array(
                'id' => 'placeholder',
                'name' => 'placeholder',
                'label' => array(
                    'value' => 'Placeholder'
                )
            )
        ),
        'original_label' => array(
            'type' => 'input',
            'settings' => array(
                'id' => 'original_label',
                'name' => 'original_label',
                'type' => 'hidden',
                'label' => array(
                    'hidden' => true
                )
            )
        )
    ),
    'item_values' => Saiyan_Module_SortableItem::get_value(
        isset($post_meta_form_values[$prefix . '_form_debit'][0]) ? $post_meta_form_values[$prefix . '_form_debit'][0] : null,
        array(
            array(
                'index' => 0,
                'values' => array(
                    'label' => 'Account BSB',
                    'placeholder' => '',
                    'original_label' => 'pdwp_bank_code'
                ),
                'non_deletable' => true
            ),
            array(
                'index' => 1,
                'values' => array(
                    'label' => 'Account Number',
                    'placeholder' => '',
                    'original_label' => 'pdwp_account_number'
                ),
                'non_deletable' => true
            ),
            array(
                'index' => 2,
                'values' => array(
                    'label' => 'Account Name',
                    'placeholder' => '',
                    'original_label' => 'pdwp_account_holder_name'
                ),
                'non_deletable' => true
            )
        )
    )
);
/*
 * Set up payment fields tab ================================================================================================================================
 */


Saiyan::tist()->render_module_mapping(array(
    'donation_form' => array(
        'type' => 'accordion',
        'settings' => array(
            'id' => 'donation_form'
        ),
        'sections' => array(
            array(
                'title' => 'General',
                'description' => 'General donation form settings',
                'content' => $general_content
            ),
	        array(
		        'title' => 'Amount',
		        'description' => 'Amount and currency configurations',
		        'content' => array(
			        $prefix . '_form_accept_cents' => array(
				        'type' => 'switch',
				        'settings' => array(
					        'id' => $prefix . '_form_accept_cents',
					        'name' => $prefix . '_form_accept_cents',
					        'gridclass' => 'col-md-4',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Accept cents on donation amount',
						        'position'  => 'top',
					        ),
					        'description' => 'If enable, the donation amount will now accept cents.',
					        'switch' => isset($post_meta_form_values[$prefix . '_form_accept_cents'][0]) && $post_meta_form_values[$prefix . '_form_accept_cents'][0] === 'true' ? 'on' : 'off'
				        )
			        ),
			        $prefix . '_target' => array(
				        'type' => 'input',
				        'settings' => array(
					        'id' => $prefix . '_target',
					        'name' => $prefix . '_target',
					        'gridclass' => 'col-md-4',
					        'type' => 'number',
					        'min' => 0,
					        'max' => 999999999,
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Donation Target',
						        'position'  => 'top',
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_target'][0]) ? $post_meta_form_values[$prefix . '_target'][0] : 0
				        )
			        ),
			        $prefix . '_min_amount' => array(
				        'type' => 'input',
				        'settings' => array(
					        'id' => $prefix . '_min_amount',
					        'name' => $prefix . '_min_amount',
					        'gridclass' => 'col-md-4',
					        'type' => 'number',
					        'min' => 1,
					        'max' => 10000,
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Minimum Amount',
						        'position'  => 'top',
					        ),
					        'description' => 'Pronto donation minimum amount',
					        'value' => isset($post_meta_form_values[$prefix . '_min_amount'][0]) ? $post_meta_form_values[$prefix . '_min_amount'][0] : 2
				        )
			        ),
			        $prefix . '_form_custom_amount' => array(
				        'type' => 'switch',
				        'settings' => array(
					        'id' => $prefix . '_form_custom_amount',
					        'name' => $prefix . '_form_custom_amount',
					        'gridclass' => 'col-md-4',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Hide custom amount on form ?',
						        'position'  => 'top',
					        ),
					        'description' => 'If enable, the custom amount on form will be hidden.',
					        'switch' => isset($post_meta_form_values[$prefix . '_form_custom_amount'][0]) && $post_meta_form_values[$prefix . '_form_custom_amount'][0] === 'true' ? 'on' : 'off'
				        )
			        ),
			        $prefix . '_form_custom_amount_default' => array(
				        'type' => 'switch',
				        'settings' => array(
					        'id' => $prefix . '_form_custom_amount_default',
					        'name' => $prefix . '_form_custom_amount_default',
					        'gridclass' => 'col-md-4',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Make custom amount default',
						        'position'  => 'top',
					        ),
					        'description' => 'If enable, the custom amount (Other Button) on form will be selected by default.',
					        'dependency' => array(
						        'id' => $prefix . '_form_custom_amount',
						        'trigger_value' => 'off',
						        'trigger_action' => 'access'
					        ),
					        'switch' => isset($post_meta_form_values[$prefix . '_form_custom_amount_default'][0]) && $post_meta_form_values[$prefix . '_form_custom_amount_default'][0] === 'true' ? 'on' : 'off'
				        )
			        ),
			        $prefix . '_form_separate_amtfield_from_amtbtns' => array(
				        'type' => 'switch',
				        'settings' => array(
					        'id' => $prefix . '_form_separate_amtfield_from_amtbtns',
					        'name' => $prefix . '_form_separate_amtfield_from_amtbtns',
					        'gridclass' => 'col-md-4',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Separate amount field from amount buttons ?',
						        'position'  => 'top',
					        ),
					        'description' => 'If enable, the amount buttons will be rendered first and then the amount field at the buttom.',
					        'dependency' => array(
						        'id' => $prefix . '_form_custom_amount',
						        'trigger_value' => 'off',
						        'trigger_action' => 'access'
					        ),
					        'switch' => isset($post_meta_form_values[$prefix . '_form_separate_amtfield_from_amtbtns'][0]) && $post_meta_form_values[$prefix . '_form_separate_amtfield_from_amtbtns'][0] === 'true' ? 'on' : 'off'
				        )
			        ),
			        $prefix . '_cc' => array(
				        'type' => 'currency-picker',
				        'settings' => array(
					        'id' => $prefix . '_cc',
					        'name' => $prefix . '_cc',
					        'gridclass' => 'col-md-6',
					        'label' => array(
						        'value' => 'Set currency'
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_cc_currency'][0]) ? $post_meta_form_values[$prefix . '_cc_currency'][0] : 'AUD'
				        )
			        ),

                    $prefix . '_form_amount_levels_separator' => array(
                        'type' => 'separator',
                        'settings' => array(
                            'id' => $prefix . '_form_amount_levels_separator',
                            'name' => $prefix . '_form_amount_levels_separator',
                            'gridclass' => 'col-md-12',
                            'visibility' => 'visible',
                            'size' => 10,
                            'align' => 'center',
                            'width' => '100%',
                            'style' => 'border-bottom: 1px #f5f5f5 solid; margin: 25px 0;'
                        )
                    ),

                    // TODO Insert Amount Levels Sortable Item
                    $prefix . '_v5' . '_form_amount_levels' => array(
                        'type' => 'sortable-item',
                        'settings' => array(
                            'id' => $prefix . '_v5' . '_form_amount_levels',
                            'name' => $prefix . '_v5' . '_form_amount_levels',
                            'gridclass' => 'col-md-6',
                            'width'=>'100%',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Amount Levels',
                                'position'  => 'top',
                            ),
                            'description' => 'This will be the default amounts on form. <br><a class="donation-form-amount-levels-reset" id="donation-reset-amount-levels">Reset Data<div class="cherry-spinner-campaign"><div class="double-bounce-2"></div></div></a>',
                            'add_item_title' => 'Add Amount Level',
                            'mode' => 'advance',
                            'base_title_id' => 'amount_level',
                            'item_deletable' => false,
                        ),
                        'item_struct' => array(
                            'amount_level' => array(
                                'type' => 'input',
                                'settings' => array(
                                    'id' => 'amount_level',
                                    'name' => 'amount_level',
                                    'type' => 'number',
                                    'label'         => array(
                                        'hidden'    => false,
                                        'value'     => 'Amount',
                                        'position'  => 'top',
                                    ),
                                    'min' => 2,
                                    'max' => 10000,
                                    'value' => 2
                                )
                            ),
                            'is_default' => array(
                                'type' => 'switch',
                                'settings' => array(
                                    'id' => 'is_default',
                                    'name' => 'is_default',
                                    'label'         => array(
                                        'hidden'    => false,
                                        'value'     => 'Default Selected',
                                        'position'  => 'top',
                                    ),
                                )
                            ),
                            'title' => array(
                                'type' => 'input',
                                'settings' => array(
                                    'id' => 'title',
                                    'name' => 'title',
                                    'type' => 'text',
                                    'label'         => array(
                                        'hidden'    => false,
                                        'value'     => 'Title',
                                        'position'  => 'top',
                                    ),
                                    'value' => ''
                                )
                            ),
                            'amount_image' => array(
                                'type' => 'media',
                                'settings' => array(
                                    'id' => 'amount_image',
                                    'class' => 'amount_image',
                                    'name' => 'amount_image',
                                    'label'         => array(
                                        'hidden'    => false,
                                        'value'     => 'Image',
                                        'position'  => 'top',
                                    ),
                                    'multiple' => false,
                                )
                            ),
                            'description' => array(
                                'type' => 'textarea',
                                'settings' => array(
                                    'id' => 'description',
                                    'class' => 'description',
                                    'name' => 'description',
                                    'style' => 'width: 100%;',
                                    'label'         => array(
                                        'hidden'    => false,
                                        'value'     => 'Description',
                                        'position'  => 'top',
                                    ),
                                    'value' => ''
                                )
                            ),
                        ),
                        'item_values' => Saiyan_Module_SortableItem::get_value(
                        // pass null or else undefined index will be thrown on passing variable
                            isset($post_meta_form_values[$prefix . '_v5' . '_form_amount_levels'][0]) ? $post_meta_form_values[$prefix . '_v5' . '_form_amount_levels'][0] : null,
                            array(
                                array(
                                    'index' => 0,
                                    'values' => array(
                                        'amount_level' => 10,
                                        'is_default' => 'on',
                                        'title' => '',
                                        'amount_image' => 0,
                                        'description' => '',
                                    ),
                                    'non_deletable' => true
                                )
                            )
                        )
                    ),

                    $prefix . '_v5' . '_form_amount_display_type' => array(
                        'type' => 'select',
                        'settings' => array(
                            'id' => $prefix . '_v5' . '_form_amount_display_type',
                            'name' => $prefix . '_v5' . '_form_amount_display_type',
                            'gridclass' => 'col-md-6',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Amount Display Type',
                                'position'  => 'top',
                            ),
                            'description' => 'Set whether the amount display type is default or card.',
                            'options' => array(
                                array(
                                    'title' => 'Default',
                                    'value' => 'default'
                                ),
                                array(
                                    'title' => 'Card',
                                    'value' => 'card'
                                )
                            ),
                            'selected' => isset($post_meta_form_values[$prefix . '_v5' . '_form_amount_display_type'][0]) ? $post_meta_form_values[$prefix . '_v5' . '_form_amount_display_type'][0] : 'default'
                        )
                    ),
                    $prefix . '_v5' . '_form_amount_card_image_position' => array(
                        'type' => 'select',
                        'settings' => array(
                            'id' => $prefix . '_v5' . '_form_amount_card_image_position',
                            'name' => $prefix . '_v5' . '_form_amount_card_image_position',
                            'gridclass' => 'col-md-6',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Card Image Position',
                                'position'  => 'top',
                            ),
                            'description' => 'Set the image position of the card amount.',
                            'options' => array(
                                array(
                                    'title' => 'Top',
                                    'value' => 'top'
                                ),
                                array(
                                    'title' => 'Bottom',
                                    'value' => 'bottom'
                                ),
                                array(
                                    'title' => 'Left',
                                    'value' => 'left'
                                ),
                                array(
                                    'title' => 'Right',
                                    'value' => 'right'
                                )
                            ),
                            'selected' => isset($post_meta_form_values[$prefix . '_v5' . '_form_amount_card_image_position'][0]) ? $post_meta_form_values[$prefix . '_v5' . '_form_amount_card_image_position'][0] : 'top'
                        )
                    ),
		        )
	        ),
	        array(
		        'title' => 'Form Fields',
		        'description' => 'This fields will be the user information displayed on form.',
		        'content' => array(
		        	$prefix . '_v5' . '_form_user_fields' => array(
		        		'type' => 'sortable-item',
				        'settings' => array(
				        	'id' => $prefix . '_v5' . '_form_user_fields',
					        'name' => $prefix . '_v5' . '_form_user_fields',
					        'mode' => 'advance',
					        'base_title_id' => 'field_label',
					        'description' => '<a class="donation-form-form-fields-reset" id="donation-reset-form-fields">Reset Data<div class="cherry-spinner-campaign"><div class="double-bounce-2"></div></div></a>'
				        ),
				        'item_struct' => array(
				        	'field_label' => array(
				        		'type' => 'input',
						        'settings' => array(
						        	'id' => 'field_label',
							        'name' => 'field_label',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Label',
								        'position'  => 'top',
							        ),
						        )
					        ),
					        'field_key' => array(
						        'type' => 'input',
						        'settings' => array(
							        'id' => 'field_key',
							        'name' => 'field_key',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Key (no white spaces)',
								        'position'  => 'top',
							        ),
							        'description' => 'Value must not have white spaces.',
							        // 'readonly' => true
						        )
					        ),
					        'field_option' => array(
						        'type' => 'select',
						        'settings' => array(
							        'id' => 'field_option',
							        'name' => 'field_option',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Field Option on Form.',
								        'position'  => 'top',
							        ),
							        'options' => array(
							        	array(
							        		'title' => 'Hide',
									        'value' => 'hide'
								        ),
								        array(
									        'title' => 'Show',
									        'value' => 'show'
								        ),
								        array(
									        'title' => 'Required',
									        'value' => 'required'
								        )
							        )
						        )
					        ),
					        'field_type' => array(
					        	'type' => 'radio',
						        'settings' => array(
						        	'id' => 'field_type',
							        'name' => 'field_type',
							        'label'         => array(
								        'hidden'    => false,
								        'value'     => 'Type',
								        'position'  => 'top',
							        ),
							        // 'readonly' => true,
							        'radios' => array(
							        	array(
							        		'title' => 'Text',
									        'value' => 'text'
								        ),
								        array(
									        'title' => 'Email',
									        'value' => 'email'
								        ),
								        array(
									        'title' => 'Number',
									        'value' => 'number'
								        ),
								        array(
									        'title' => 'Select',
									        'value' => 'select'
								        ),
								        array(
									        'title' => 'Textarea',
									        'value' => 'textarea'
								        ),
								        array(
									        'title' => 'Checkbox',
									        'value' => 'checkbox'
								        ),
                                        array(
                                            'title' => 'Radio',
                                            'value' => 'radio'
                                        )
							        )
						        )
					        ),
					        'field_select_option' => array(
					        	'type' => 'textarea',
						        'settings' => array(
						        	'id' => 'field_select_option',
							        'name' => 'field_select_option',
							        'label' => array(
							        	'value' => 'Options'
							        ),
							        'placeholder' => 'Option1, Option2, Option3',
							        // 'readonly' => true,
							        'dependency' => array(
							        	'id' => 'field_type',
								        'trigger_value' => 'select',
							        )
						        )
					        ),
					        'field_checkbox_option' => array(
						        'type' => 'textarea',
						        'settings' => array(
							        'id' => 'field_checkbox_option',
							        'name' => 'field_checkbox_option',
							        'label' => array(
								        'value' => 'Message'
							        ),
							        'placeholder' => 'Message here',
							        // 'readonly' => true,
							        'dependency' => array(
								        'id' => 'field_type',
								        'trigger_value' => 'checkbox',
							        )
						        )
					        ),
                            'field_radio_title' => array(
                                'type' => 'textarea',
                                'settings' => array(
                                    'id' => 'field_radio_title',
                                    'name' => 'field_radio_title',
                                    'label' => array(
                                        'value' => 'Title'
                                    ),
                                    'placeholder' => 'Title1, Title2, Title3',
                                    'dependency' => array(
                                        'id' => 'field_type',
                                        'trigger_value' => 'radio'
                                    )
                                )
                            ),
                            'field_radio_value' => array(
                                'type' => 'textarea',
                                'settings' => array(
                                    'id' => 'field_radio_value',
                                    'name' => 'field_radio_value',
                                    'label' => array(
                                        'value' => 'Value'
                                    ),
                                    'placeholder' => 'Value1, Value2, Value3',
                                    'dependency' => array(
                                        'id' => 'field_type',
                                        'trigger_value' => 'radio'
                                    )
                                )
                            ),
                            'field_default_value' => array(
                                'type' => 'select',
                                'settings' => array(
                                    'id' => 'field_default_value',
                                    'name' => 'field_default_value',
                                    'label' => array(
                                        'value' => 'Default Value'
                                    ),
                                    'options' => $country,
                                    'dependency' => array(
                                        'id' => 'field_type',
                                        'trigger_value' => 'select'
                                    )
                                )
                            )
				        ),
				        'item_values' => Saiyan_Module_SortableItem::get_value(
				        	isset($post_meta_form_values[$prefix . '_v5' . '_form_user_fields'][0]) ? $post_meta_form_values[$prefix . '_v5' . '_form_user_fields'][0] : null,
				        	array(
					            array(
					                'index' => 0,
							        'values' => array(
							            'field_label' => 'Donor Type',
								        'field_key' => 'donor_type',
								        'field_option' => 'required',
								        'field_type' => 'select',
								        'field_select_option' => 'Individual, Business',
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 1,
							        'values' => array(
								        'field_label' => 'Company',
								        'field_key' => 'donor_company',
								        'field_option' => 'show',
								        'field_type' => 'text',
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 2,
							        'values' => array(
								        'field_label' => 'Title',
								        'field_key' => 'donor_title',
								        'field_option' => 'hide',
								        'field_type' => 'select',
								        'field_select_option' => ',Mrs., Mr., Ms.'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 3,
							        'values' => array(
								        'field_label' => 'First Name',
								        'field_key' => 'donor_first_name',
								        'field_option' => 'required',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 4,
							        'values' => array(
								        'field_label' => 'Last Name',
								        'field_key' => 'donor_last_name',
								        'field_option' => 'required',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 6,
							        'values' => array(
								        'field_label' => 'Keep this donation anonymous',
								        'field_key' => 'donor_anonymous',
								        'field_option' => 'hide',
								        'field_type' => 'checkbox',
								        'field_checkbox_option' => 'Check this field to make this donation anonymous and keep your name hidden.'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 7,
							        'values' => array(
								        'field_label' => 'Email',
								        'field_key' => 'donor_email',
								        'field_option' => 'required',
								        'field_type' => 'email'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 8,
							        'values' => array(
								        'field_label' => 'Phone',
								        'field_key' => 'donor_phone',
								        'field_option' => 'show',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 9,
							        'values' => array(
								        'field_label' => 'Address',
								        'field_key' => 'donor_address',
								        'field_option' => 'required',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 10,
							        'values' => array(
								        'field_label' => 'Country',
								        'field_key' => 'donor_country',
								        'field_option' => 'required',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 11,
							        'values' => array(
								        'field_label' => 'State',
								        'field_key' => 'donor_state',
								        'field_option' => 'show',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 12,
							        'values' => array(
								        'field_label' => 'Postcode',
								        'field_key' => 'donor_postcode',
								        'field_option' => 'show',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 13,
							        'values' => array(
								        'field_label' => 'Suburb',
								        'field_key' => 'donor_suburb',
								        'field_option' => 'show',
								        'field_type' => 'text'
							        ),
							        'non_deletable' => true
						        ),
						        array(
							        'index' => 14,
							        'values' => array(
								        'field_label' => 'Comment',
								        'field_key' => 'donor_comment',
								        'field_option' => 'show',
								        'field_type' => 'textarea'
							        ),
							        'non_deletable' => true
						        ),
					        )
				        )
			        )
		        )
	        ),
	        array(
		        'title' => 'Payment Fields',
		        'description' => 'Select payment gateways available on form.',
		        'content' => $paymentfields
	        ),
	        array(
		        'title' => 'Thank You Email',
		        'description' => 'Configure email send to donor.',
		        'content' => array(
		        	$prefix . '_form_thankyou_email_enable' => array(
		        		'type' => 'switch',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_email_enable',
					        'name' => $prefix . '_form_thankyou_email_enable',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Enable sending thankyou email ?',
						        'position'  => 'top',
					        ),
					        'switch' => isset($post_meta_form_values[$prefix . '_form_thankyou_email_enable'][0]) && $post_meta_form_values[$prefix . '_form_thankyou_email_enable'][0] === 'true' ? 'on' : 'off'
				        )
			        ),
			        $prefix . '_form_thankyou_email_test' => array(
			        	'type' => 'button',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_email_test',
				        	'name' => $prefix . '_form_thankyou_email_test',
					        'gridclass' => 'col-md-6',
					        'disabled' => ! $thankyou_email_test_prereq,
					        'content' => 'Send Email',
					        'label' => array(
					        	'value' => 'Test Email'
					        ),
					        'description' => $thankyou_email_test_prereq ?
						        'Test if provided email receives a Thank You email' :
						        '<span style="color: red;">Fill Email Address and Name inside <a id="donation_form_thankyou_email_focus_prereq">Admin Notification</a> to do tests</span>'
				        )
			        ),
			        $prefix . '_form_thankyou_email_subject' => array(
				        'type' => 'input',
				        'settings' => array(
					        'id' => $prefix . '_form_thankyou_email_subject',
					        'name' => $prefix . '_form_thankyou_email_subject',
					        'gridclass' => 'col-md-12',
					        'label' => array(
						        'value' => 'Thankyou Email Subject'
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_thankyou_email_subject'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_email_subject'][0] : 'Thank you for your donation.'
				        )
			        ),
			        $prefix . '_form_thankyou_email_separator' => array(
			        	'type' => 'separator',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_email_separator',
					        'visibility' => 'hidden'
				        )
			        ),
			        // TODO revert back original ID after testing
			        $prefix . '_form_thankyou_email_msg' => array(
			        	'type' => 'wpeditor',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_email_msg',
					        'name' => $prefix . '_form_thankyou_email_msg',
					        'content' => isset($post_meta_form_values[$prefix . '_form_thankyou_email_msg'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_email_msg'][0] :
						                 '<p>Hi [first-name] [last-name],</p>'.
					                     '<p></p>'.
					                     '<p>Thank you for your donation. We really appreciate it.</p>'.
					                     '<p></p>'.
					                     '<p>Sincerely,</p>'.
					                     '<p>[from-name]</p>',
					        'wpeditor_settings' => array(
					        	'media_buttons' => false
					        ),
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Thankyou Email Message',
						        'position'  => 'top',
					        )
				        )
			        )
		        )
	        ),
	        array(
		        'title' => 'Thank You Page',
		        'description' => 'Thank You Page configurations.',
		        'content' => array(
                    $prefix . '_form_thankyou_url_one_off' => array(
                        'type' => 'input',
                        'settings' => array(
                            'id' => $prefix . '_form_thankyou_url_one_off',
                            'name' => $prefix . '_form_thankyou_url_one_off',
                            'gridclass' => 'col-md-6',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Thank You Redirect URL (One-off)',
                                'position'  => 'top',
                            ),
                            'value' => isset($post_meta_form_values[$prefix . '_form_thankyou_url_one_off'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_url_one_off'][0] : ''
                        )
                    ),
                    $prefix . '_form_thankyou_url_recurring' => array(
                        'type' => 'input',
                        'settings' => array(
                            'id' => $prefix . '_form_thankyou_url_recurring',
                            'name' => $prefix . '_form_thankyou_url_recurring',
                            'gridclass' => 'col-md-6',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Thank You Redirect URL (Recurring)',
                                'position'  => 'top',
                            ),
                            'value' => isset($post_meta_form_values[$prefix . '_form_thankyou_url_recurring'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_url_recurring'][0] : ''
                        )
                    ),
                    $prefix . '_thankyou_url_separator' => array(
                        'type' => 'separator',
                        'settings' => array(
                            'id' => $prefix . '_thankyou_url_separator',
                            'gridclass' => 'col-md-12',
                            'style' => 'margin: 0 0 30px;',
                            'visibility' => 'visible'
                        )
                    ),
		        	$prefix . '_form_thankyou_page_title' => array(
		        		'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_page_title',
				        	'name' => $prefix . '_form_thankyou_page_title',
					        'gridclass' => 'col-md-6',
					        'label' => array(
					        	'value' => 'Thank You Page Title'
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_thankyou_page_title'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_page_title'][0] : 'Thank You'
				        )
			        ),
		        	$prefix . '_form_thankyou_url' => array(
		        		'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_url',
					        'name' => $prefix . '_form_thankyou_url',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Thank You Redirect URL',
						        'position'  => 'top',
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_thankyou_url'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_url'][0] : ''
				        )
			        ),
			        $prefix . '_form_thankyou_page_content' => array(
			        	'type' => 'wpeditor',
				        'settings' => array(
				        	'id' => $prefix . '_form_thankyou_page_content',
					        'name' => $prefix . '_form_thankyou_page_content',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Thankyou Page Content',
						        'position'  => 'top',
					        ),
					        'wpeditor_settings' => array(
					        	'wpautop' => false
					        ),
					        'description' => 'To add social buttons on thank you page add this on the content on the editor. Use "[facebook]" for Facebook, use "[twitter]" for Twitter, use "[google-plus]" for Google Plus, use "[linkedin]" for LinkedIn',
					        'content' => isset($post_meta_form_values[$prefix . '_form_thankyou_page_content'][0]) ? $post_meta_form_values[$prefix . '_form_thankyou_page_content'][0] : 'Thank you for your donation.'
				        )
			        )
		        )
	        ),
	        array(
                'title' => 'Failure Message',
                'description' => '',
                'content' => array(
                    $prefix . '_form_error_message_behavior' => array(
                        'type' => 'select',
                        'settings' => array(
                            'id' => $prefix . '_form_error_message_behavior',
                            'name' => $prefix . '_form_error_message_behavior',
                            'gridclass' => 'col-md-7',
                            'label' => array(
                                'value' => 'Error Message Behavior'
                            ),
                            'options' => array(
                                array(
                                    'title' => 'Standard',
                                    'value' => 'standard'
                                ),
                                array(
                                    'title' => 'Pop-up',
                                    'value' => 'popup'
                                )
                            ),
                            'selected' => isset($post_meta_form_values[$prefix . '_form_error_message_behavior'][0]) ? $post_meta_form_values[$prefix . '_form_error_message_behavior'][0] : 'standard'
                        )
                    ),
	                $prefix . '_form_override_failure_message' => array(
		                'type' => 'switch',
		                'settings' => array(
			                'id' => $prefix . '_form_override_failure_message',
			                'name' => $prefix . '_form_override_failure_message',
			                'gridclass' => 'col-md-6',
			                'label' => array(
				                'value' => 'Override Failure Message'
			                ),
			                'switch' => isset($post_meta_form_values[$prefix . '_form_override_failure_message'][0]) && $post_meta_form_values[$prefix . '_form_override_failure_message'][0] === 'true' ? 'on' : 'off'
		                )
	                ),
	                $prefix . '_form_override_message_type' => array(
		                'type' => 'select',
		                'settings' => array(
			                'id' => $prefix . '_form_override_message_type',
			                'name' => $prefix . '_form_override_message_type',
			                'gridclass' => 'col-md-6',
			                'label' => array(
				                'value' => 'Failure Message Type'
			                ),
			                'options' => array(
				                array(
					                'title' => 'Plain Text',
					                'value' => 'plain_text'
				                ),
				                array(
					                'title' => 'HTML',
					                'value' => 'html'
				                )
			                ),
			                'selected' => isset($post_meta_form_values[$prefix . '_form_override_message_type'][0]) ? $post_meta_form_values[$prefix . '_form_override_message_type'][0] : 'plain_text'
		                )
	                ),
                    $prefix . '_form_override_message' => array(
                        'type' => 'wpeditor',
                        'settings' => array(
                            'id' => $prefix . '_form_override_message',
                            'name' => $prefix . '_form_override_message',
                            'gridclass' => 'col-md-12',
                            'label' => array(
                                'value' => 'Failure Message'
                            ),
                            'content' => isset($post_meta_form_values[$prefix . '_form_override_message'][0]) ? $post_meta_form_values[$prefix . '_form_override_message'][0] : 'There has been an issue with processing your donation. Please contact our administrator.'
                        )
                    )
                )
	        ),
	        array(
		        'title' => 'Admin Notification',
		        'description' => 'Admin notification during donation.',
		        'content' => array(
		        	$prefix . '_form_notification_option' => array(
		        		'type' => 'select',
				        'settings' => array(
				        	'id' => $prefix . '_form_notification_option',
					        'name' => $prefix . '_form_notification_option',
					        'label' => array(
					        	'value' => 'Notification Option'
					        ),
					        'description' => 'Send notification to administrator if donation synchronization fail or success.', 
					        'gridclass' => 'col-md-6',
					        'options' => array(
					        	array(
					        		'title' => 'Both',
							        'value' => 'both'
						        ),
						        array(
							        'title' => 'Success',
							        'value' => 'success'
						        ),
						        array(
							        'title' => 'Failed',
							        'value' => 'failed'
						        )
					        ),
					        'selected' => isset($post_meta_form_values[$prefix . '_form_notification_option'][0]) ? $post_meta_form_values[$prefix . '_form_notification_option'][0] : ''
				        )
			        ),
			        $prefix . '_form_recipient_email' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_recipient_email',
					        'name' => $prefix . '_form_recipient_email',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Recipient Email Address',
						        'position'  => 'top',
					        ),
					        'description' => 'For multiple emails use comma separeted.',
					        'placeholder' => 'johndoe@sample.com',
					        'value' => isset($post_meta_form_values[$prefix . '_form_recipient_email'][0]) ? $post_meta_form_values[$prefix . '_form_recipient_email'][0] : ''
				        )
			        ),
			        $prefix . '_form_separator' => array(
				        'type' => 'separator',
				        'settings' => array(
					        'id' => $prefix . '_form_separator',
					        'gridclass' => 'col-md-12',
					        'visibility' => 'hidden'
				        )
			        ),
			        $prefix . '_form_from_email' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_from_email',
					        'name' => $prefix . '_form_from_email',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'From Email Address',
						        'position'  => 'top',
					        ),
					        'placeholder' => 'johndoe@sample.com',
					        'value' => isset($post_meta_form_values[$prefix . '_form_from_email'][0]) ? $post_meta_form_values[$prefix . '_form_from_email'][0] : ''
				        )
			        ),
			        $prefix . '_form_from_name' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_from_name',
					        'name' => $prefix . '_form_from_name',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'From Name',
						        'position'  => 'top',
					        ),
					        'placeholder' => 'John Doe',
					        'value' => isset($post_meta_form_values[$prefix . '_form_from_name'][0]) ? $post_meta_form_values[$prefix . '_form_from_name'][0] : ''
				        )
			        ),
			        $prefix . '_form_admin_notification_test_success' => array(
			        	'type' => 'button',
				        'settings' => array(
				        	'id' => $prefix . '_form_admin_notification_test_success',
					        'gridclass' => 'col-md-6',
					        'label' => array(
					        	'value' => 'Test Success Sync Notification'
					        ),
					        'description' => $thankyou_email_test_prereq ?
						        'Test if provided email receives a Thank You email' :
						        '<span style="color: red;">Fill Recipients Email Address, Email Address and Name inside to do tests</span>',
					        'disabled' => ! $admin_notification_test_prereq,
					        'content' => 'Test Success'
				        )
			        ),
			         $prefix . '_form_admin_notification_test_failed' => array(
				        'type' => 'button',
				        'settings' => array(
					        'id' => $prefix . '_form_admin_notification_test_failed',
					        'gridclass' => 'col-md-6',
					        'label' => array(
						        'value' => 'Test Failed Sync Notification'
					        ),
					        'description' => $thankyou_email_test_prereq ?
						        'Test if provided email receives a Thank You email' :
						        '<span style="color: red;">Fill Recipients Email Address, Email Address and Name inside to do tests</span>',
					        'disabled' => ! $admin_notification_test_prereq,
					        'content' => 'Test Failed'
				        )
			        )
		        )
	        ),
	        array(
                'title' => 'Social Configurations',
                'description' => '',
                'content' => array(
                        $prefix . '_form_social_link' => array(
                        'type' => 'input',
                        'settings' => array(
                            'id' => $prefix . '_form_social_link',
                            'name' => $prefix . '_form_social_link',
                            'gridclass' => 'col-md-12',
                            'label' => array(
                                'hidden' => false,
                                'value' => 'Social Link',
                                'position' => 'top'
                            ),
                            'description' => 'The link to be shared on social sites',
                            'value' => isset($post_meta_form_values[$prefix . '_form_social_link'][0]) ? $post_meta_form_values[$prefix . '_form_social_link'][0] : ''
                        )
                    ),
                    $prefix . '_form_social_message' => array(
                        'type' => 'textarea',
                        'settings' => array(
                            'id' => $prefix . '_form_social_message',
                            'name' => $prefix . '_form_social_message',
                            'gridclass' => 'col-md-12',
                            'style' => 'width: 100%',
                            'label' => array(
                                'hidden' => false,
                                'value' => 'Social Message',
                                'position' => 'top'
                            ),
                            'description' => 'Additional pre-text message for sharing on social sites',
                            'value' => isset($post_meta_form_values[$prefix . '_form_social_message'][0]) ? $post_meta_form_values[$prefix . '_form_social_message'][0] : ''
                        )
                    )
                )
	        ),
	        array(
		        'title' => 'Form Style',
		        'description' => '',
		        'content' => array(
		        	$prefix . '_form_template_mode' => array(
		        		'type' => 'radio',
				        'settings' => array(
				        	'id' => $prefix . '_form_template_mode',
					        'name' => $prefix . '_form_template_mode',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Template Mode',
						        'position'  => 'top',
					        ),
					        'radios' => array(
					        	array(
					        		'title' => 'Normal Mode',
							        'value' => 'normal'
						        ),
						        array(
							        'title' => 'Wizard Mode',
							        'value' => 'wizard'
						        )
					        ),
					        'checked' => isset($post_meta_form_values[$prefix . '_form_template_mode'][0]) ? $post_meta_form_values[$prefix . '_form_template_mode'][0] : 'normal'
				        )
			        ),
			        $prefix . '_form_template_style' => array(
			        	'type' => 'select',
				        'settings' => array(
				        	'id' => $prefix . '_form_template_style',
					        'name' => $prefix . '_form_template_style',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Template Name',
						        'position'  => 'top',
					        ),
					        'options' => $select_template_mode_normal,
					        'dependency' => array(
					        	'id' => $prefix . '_form_template_mode',
						        'trigger_value' => 'normal',
						        'trigger_action' => 'show_hide'
					        ),
					        'selected' => isset($post_meta_form_values[$prefix . '_form_template_style'][0]) ? $post_meta_form_values[$prefix . '_form_template_style'][0] : ''
				        )
			        ),
			        $prefix . '_form_template_style2' => array(
			        	'type' => 'select',
				        'settings' => array(
				        	'id' => $prefix . '_form_template_style2',
					        'name' => $prefix . '_form_template_style2',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Wizard Template Name',
						        'position'  => 'top',
					        ),
					        'options' => $select_template_mode_wizard,
					        'dependency' => array(
					        	'id' => $prefix . '_form_template_mode',
						        'trigger_value' => 'wizard',
						        'trigger_action' => 'show_hide'
					        ),
					        'selected' => isset($post_meta_form_values[$prefix . '_form_template_style2'][0]) ? $post_meta_form_values[$prefix . '_form_template_style2'][0] : ''
				        )
			        ),
			        $prefix . '_form_separator' => array(
			        	'type' => 'separator',
				        'settings' => array(
				        	'id' => $prefix . '_form_separator',
					        'gridclass' => 'col-md-12',
					        'visibility' => 'hidden'
				        )
			        ),
			        $prefix . '_form_class' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_class',
					        'name' => $prefix . '_form_class',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Class',
						        'position'  => 'top',
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_class'][0]) ? $post_meta_form_values[$prefix . '_form_class'][0] : 'pronto-donation-input'
				        )
			        ),
			        $prefix . '_form_input_class' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_input_class',
					        'name' => $prefix . '_form_input_class',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Input Field Class',
						        'position'  => 'top',
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_input_class'][0]) ? $post_meta_form_values[$prefix . '_form_input_class'][0] : 'pronto-donation-input'
				        )
			        ),
			        $prefix . '_form_btn_class' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_class',
					        'name' => $prefix . '_form_btn_class',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Button Class',
						        'position'  => 'top',
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_btn_class'][0]) ? $post_meta_form_values[$prefix . '_form_btn_class'][0] : 'pronto-donation-btn'
				        )
			        ),
			        $prefix . '_form_btn_caption' => array(
			        	'type' => 'input',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_caption',
					        'name' => $prefix . '_form_btn_caption',
					        'gridclass' => 'col-md-6',
					        'type' => 'text',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Button Caption',
						        'position'  => 'top',
					        ),
					        'value' => isset($post_meta_form_values[$prefix . '_form_btn_caption'][0]) ? $post_meta_form_values[$prefix . '_form_btn_caption'][0] : 'Donate'
				        )
			        ),
			        $prefix . '_form_btn_padding' => array(
			        	'type' => 'slider',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_padding',
					        'name' => $prefix . '_form_btn_padding',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Button Padding',
						        'position'  => 'top',
					        ),
					        'description' => 'Render button width in px',
					        'min' => 0,
					        'max' => 100,
					        'value' => isset($post_meta_form_values[$prefix . '_form_btn_padding'][0]) ? $post_meta_form_values[$prefix . '_form_btn_padding'][0] : 18
				        ),
			        ),
			        $prefix . '_form_btn_font_size' => array(
			        	'type' => 'slider',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_font_size',
					        'name' => $prefix . '_form_btn_font_size',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Button Font Size',
						        'position'  => 'top',
					        ),
					        'description' => 'Render button font size in px',
					        'min' => 1,
					        'max' => 100,
					        'value' => isset($post_meta_form_values[$prefix . '_form_btn_font_size'][0]) ? $post_meta_form_values[$prefix . '_form_btn_font_size'][0] : 18
				        )
			        ),
			        $prefix . '_form_btn_width' => array(
			        	'type' => 'slider',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_width',
					        'name' => $prefix . '_form_btn_width',
					        'gridclass' => 'col-md-6',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Button Width',
						        'position'  => 'top',
					        ),
					        'description' => 'Render button width in pxs',
					        'min' => 100,
					        'max' => 1000,
					        'value' => isset($post_meta_form_values[$prefix . '_form_btn_width'][0]) ? $post_meta_form_values[$prefix . '_form_btn_width'][0] : 150
				        )
			        ),
			        $prefix . '_form_btn_color' => array(
			        	'type' => 'color-picker',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_color',
					        'name' => $prefix . '_form_btn_color',
					        'gridclass' => 'col-md-3',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Template Theme Color',
						        'position'  => 'top',
					        ),
					        'color' => isset($post_meta_form_values[$prefix . '_form_btn_color'][0]) ? $post_meta_form_values[$prefix . '_form_btn_color'][0] : '#1497c1'
				        )
			        ),
			        $prefix . '_form_btn_font_color' => array(
			        	'type' => 'color-picker',
				        'settings' => array(
				        	'id' => $prefix . '_form_btn_font_color',
					        'name' => $prefix . '_form_btn_font_color',
					        'gridclass' => 'col-md-3',
					        'label'         => array(
						        'hidden'    => false,
						        'value'     => 'Form Button Font Color',
						        'position'  => 'top',
					        ),
					        'color' => isset($post_meta_form_values[$prefix . '_form_btn_font_color'][0]) ? $post_meta_form_values[$prefix . '_form_btn_font_color'][0] : '#ffffff'
				        )
			        ),
			        'separator_2' => array(
                        'type' => 'separator',
                        'settings' => array(
                            'visibility' => 'hidden'
                        )
			        ),
			        $prefix . '_donation_page_btn_preview' => array(
			        	'type' => 'button',
				        'settings' => array(
				        	'id' => $prefix . '_donation_page_btn_preview',
					        'name' => $prefix . '_donation_page_btn_preview',
					        'gridclass' => 'col-md-6',
					        'label' => array(
					        	'value' => 'Donate Button Preview'
					        ),
					        'description' => 'This button is the preview of Donation Page Button.',
					        'content' => '<span class="text">' . esc_html__( 'Donate', 'donation' ) . '</span>'
				        )
			        )
		        )
	        )
        )
    ),
    'navigator' => array(
        'type' => 'navigator',
        'settings' => array(
            'id' => 'navigator',
            'class' => 'navigator',
            'link_to_accordion' => 'donation_form'
        ),
        'way_points' => array(
            'post-body-content' => 'Donation Content',
            'donation_form' => 'Form Settings'
        )
    )
));

wp_localize_script(
	$this->plugin_name,
	'wfc_js_object',
	array(
		'post_status' => $post->post_status
	)
);

global $pagenow;

if (in_array($pagenow, array('post.php'))) {
	$item_vals = array();
	for ($i=0, $count=count($sortable_list_gateways); $i<$count; $i++) {
		$item = null;
		// print_r($this->item_values[$i]);
		$item[$sortable_list_gateways[$i]['settings']['checkboxes'][0]['value']] = array(
			'title' => $sortable_list_gateways[$i]['settings']['checkboxes'][0]['title'],
			'value' => in_array(
				$sortable_list_gateways[$i]['settings']['checkboxes'][0]['value'],
				(isset($sortable_list_gateways[$i]['settings']['checked']) ? $sortable_list_gateways[$i]['settings']['checked'] : array())
			) ? 'true' : 'false'
		);
		$item_vals[$i] = $item;
	}

	update_post_meta(
		get_the_ID(),
		$prefix . '_v5' . '_form_payment_gateways',
		json_encode($item_vals)
	);
}