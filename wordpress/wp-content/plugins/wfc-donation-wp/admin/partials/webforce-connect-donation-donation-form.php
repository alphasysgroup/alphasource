<?php 
/**
* @author: Junjie Canonio
*/
?>
<a class="wfc-don-menu-btn-link" href="<?php echo admin_url( 'edit.php?post_type=pdwp_donation' );?>">
  <div id="wfc-don-menu-btn" class="wfc-don-menu-btn">
    <div class="container-fluid">
      <div class="row">
        <div class="col-2 wfc-don-menu-btn-icon"><i class="fa fa-drivers-license"></i></div>
        <div class="col-10 wfc-don-menu-btn-desc">
          <h3><?php echo __('Donation Form', 'webforce-connect-donation'); ?></h3>
          <em><?php echo __('Handle and configure donation forms.', 'webforce-connect-donation'); ?></em>
        </div>
      </div>
    </div>
  </div>
</a>
