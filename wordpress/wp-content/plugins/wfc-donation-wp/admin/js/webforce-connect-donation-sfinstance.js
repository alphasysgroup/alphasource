/**
 * @author: Junjie Canonio
 */
jQuery(document).ready(function($){
    console.log('Salesforce Instance JS');


    /**
     * Fetch GAU and Campaign from Salesforce
     */
    var fetchgaucamp_icn = $('#wfc-don-fetch-gaucamp-icon');
    $('#wfc_resync_campaign_gau').on('click', function(){
        $(this).attr('disabled','disabled');
        fetchgaucamp_icn.removeClass('fa-spin');
        fetchgaucamp_icn.addClass('fa-spin');
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Fethcing GAU and Campaign.</div>',null,'info');
        $.ajax({
            url: wfc_don_fetchgaucamp_param.url,
            type:'POST',
            dataType:'json',
            data:{
                'action': 'wfc_don_fetchgaucamp_action',
                nonce: wfc_don_fetchgaucamp_param.nonce,
                'sfinstanceid': $('#wfc_don_salesforceinstance').val()
            },
            success:function(response){
                console.log(response);
                $('#wfc_resync_campaign_gau').removeAttr('disabled');
                fetchgaucamp_icn.removeClass('fa-spin');
                SaiyantistCore.snackbar.hide();
                if(response.status == 'Success'){
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU and Campaign Fetch Successful</div>',3000,'success');
                }else if(response.status == 'Warning-camp'){
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU Fetch Successful</div>',3000,'success');
                    setTimeout(function(){
                        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Campaign Fetch Failed</div>',3000,'warning');
                    }, 3000);
                }else if(response.status == 'Warning-gau'){
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Campaign Fetch Successful</div>',3000,'success');
                    setTimeout(function(){
                        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU Fetch Failed</div>',3000,'warning');
                    }, 3000);
                }else{
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU and Campaign Fetch Failed</div>',3000,'error');
                }
            },
            error: function(response) {
                //console.log(response);
                $('#wfc_resync_campaign_gau').removeAttr('disabled');
                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU and Campaign Fetch Error</div>',3000,'error');
            }
        });
    });


    /**
     * Render Payment Gateway of the Saleforce Instance
     */
    fetchSalesforceInstanceItems();
    function fetchSalesforceInstanceItems() {
        if($('#wfc-don-easyPaginate-sfinstance').length){
            $.ajax({
                url: wfc_don_admin_sfinstance_param.url,
                type: 'POST',
                dataType: 'json',
                data: {
                    'action': 'wfc_don_getsfinstance_action',
                    nonce: wfc_don_admin_sfinstance_param.nonce,
                },
                success: function (response) {
                    console.log(response);
                    if (response.status == 'success'){
                        var instances = response.instances
                        renderSalesforceInstanceItems(instances);
                    }else if(response.status == 'empty'){
                        $('#wfc-don-easyPaginate-sfinstance').empty();
                        $('#wfc-don-easyPaginate-sfinstance').append('<li class="list-group-item"><div>' + response.msg + '</div></li>');
                    }
                },
                error: function (response) {
                    reject(response);
                }
            });
        }
    }


    function renderSalesforceInstanceItems(param){
        var gateway_item = '';
        $.each( param, function( key, value ) {
            var instance_url = '';
            if(typeof(value.tokens) != 'undefined'){
                if(typeof(value.tokens.instance_url) != 'undefined'){
                    instance_url = value.tokens.instance_url;
                }
            }

            var wfc_base_instancetype = value.wfc_base_instancetype;
            wfc_base_instancetype = wfc_base_instancetype.toUpperCase();

            gateway_item += '<li class="list-group-item ' + value.wfc_base_instancetype + '">';
            gateway_item += '<div class="row">';

            gateway_item += '<div class="wfc-don-list-item col-3">';
            gateway_item += '<span><a href="' + value.instances_settings_url + '" target="_blank">' + value.wfc_base_salesforceinstance_name + '</a></span>';
            gateway_item += '</div>';

            gateway_item += '<div class="wfc-don-list-item col-4">';
            gateway_item += '<span>' + value.wfc_base_salesforceinstance_slug + '</span>';
            gateway_item += '</div>';

            gateway_item += '<div class="wfc-don-list-item col-3">';
            gateway_item += '<span>' + instance_url + '</span>';
            gateway_item += '</div>';

            gateway_item += '<div class="col-1">';
            if(value.instances_connection_status.status == 'success'){
                gateway_item += '<span>' + '<i class="fa fa-plug" style="font-size: 36px; color: green;"></i>' + '</span>';
            }else{
                gateway_item += '<span>' + '<i class="fa fa-plug" style="font-size: 36px; color: red;"></i>' + '</span>';
            }

            gateway_item += '</div>';

            gateway_item += '<div class="col-1">';
            gateway_item += '<a href="' + value.instances_url + '" target="_blank">';
            gateway_item += '<span><i id="' + value.wfc_base_salesforceinstance_slug + '" class="fa fa-gear wfc-don-gateway-item-gear" style="font-size:36px"></i></span>';
            gateway_item += '</a>';
            gateway_item += '</div>';

            gateway_item += '</div>';
            gateway_item += '</li>';
        });

        var easyPaginateNav = $('.easyPaginateNav');
        easyPaginateNav.remove();

        var easyPaginate = $('#wfc-don-easyPaginate-sfinstance');
        easyPaginate.empty();
        easyPaginate.append(gateway_item);
        easyPaginate.easyPaginate({
            paginateElement: 'li',
            elementsPerPage: 10,
            effect: 'climb'
        });
    }
});