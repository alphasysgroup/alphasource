/**
 * @author: Junjie Canonio
 */
jQuery(document).ready(function($){
    console.log('Set Payment Transaction Form');
    console.log(wfc_don_paymenttransaction_param);

    $('#wfc-donation-list-payment-transaction-form-save-btn').on('click', function() {
        var txnstatus_val = $('#wfc_don_paymenttransactionform_txnstatus option:selected').val();
        var txnid_val = $('#wfc_don_paymenttransactionform_txnid').val();
        var txndate_val = $('#wfc_don_paymenttransactionform_txndate').val();
        var txnpaymentref_val = $('#wfc_don_paymenttransactionform_txnpaymentref').val();

        if (txnstatus_val.replace(/^\s+|\s+$/g, "").length == 0){
            SaiyantistCore.snackbar.hide();
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Please set transaction status.</div>',3000,'warning');
            $('#wfc_don_paymenttransactionform_txnstatus').focus();
        }else if (txnid_val.replace(/^\s+|\s+$/g, "").length == 0){
            SaiyantistCore.snackbar.hide();
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Please set transaction ID.</div>',3000,'warning');
            $('#wfc_don_paymenttransactionform_txnid').focus();
        }else if (txndate_val.replace(/^\s+|\s+$/g, "").length == 0){
            SaiyantistCore.snackbar.hide();
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Please set transaction date.</div>',3000,'warning');
            $('#wfc_don_paymenttransactionform_txndate').focus();
        }else if (txnpaymentref_val.replace(/^\s+|\s+$/g, "").length == 0){
            SaiyantistCore.snackbar.hide();
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Please set transaction reference.</div>',3000,'warning');
            $('#wfc_don_paymenttransactionform_txnpaymentref').focus();
        }else{

            if(txndate_val.match(/^[0-9]{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])/) && txndate_val.replace(/^\s+|\s+$/g, "").length < 11){

                var txnpaymentref_val_arr = txnpaymentref_val.split('-');

                if (typeof txnpaymentref_val_arr[2] !== 'undefined' && txnpaymentref_val_arr[2] == wfc_don_paymenttransaction_param.donation_id) {

                    var confirm_setpaymenttransaction = confirm("Are you sure that all the payment transaction details are correct and do you confirm to set transaction details to this donation record ?");
                    if (confirm_setpaymenttransaction == true) {
                        console.log('Set payment transaction confirmed!');
                        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Updating Donation Record.</div>',null,'info');
                        $.ajax({
                            url: wfc_don_paymenttransaction_param.url,
                            type:'POST',
                            dataType:'json',
                            data: {
                                'action': 'wfc_don_setpaymenttransaction_action',
                                'donation_id' : wfc_don_paymenttransaction_param.donation_id,
                                'txnstatus' : txnstatus_val,
                                'txnid' : txnid_val,
                                'txndate' : txndate_val,
                                'txnpaymentref' : txnpaymentref_val,
                            },
                            success: function(response) {
                                console.log(response);

                                SaiyantistCore.snackbar.hide();
                                if(response.status == 'Success'){
                                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+response.msg+'</div>',5000,'success');
                                    setTimeout(function(){
                                        location.reload();
                                    }, 5000);
                                }else if(response.status == 'Error'){
                                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+response.msg+'</div>',3000,'warning');
                                }else{
                                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
                                }
                            },
                            error: function(resp) {
                                reject( resp );
                                location.reload();
                            }
                        });
                    } else {
                        console.log('Set payment transaction cancelled!');
                    }

                }else{
                    SaiyantistCore.snackbar.hide();
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">The transaction reference set is not valid for this donation record please check it properly.</div>',3000,'warning');
                    $('#wfc_don_paymenttransactionform_txnpaymentref').focus();
                }

            }else{
                SaiyantistCore.snackbar.hide();
                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Invalid transaction date format.</div>',3000,'warning');
                $('#wfc_don_paymenttransactionform_txndate').focus();
            }


        }
    });

});