jQuery(document).ready(function($) {

    toggleSFinstancefields()
    function toggleSFinstancefields(){
        var sfinstance = $('#donation_form_sfinstance').val();
        toggleSFinstance_campaign_fields(sfinstance);
        toggleSFinstance_gateway_fields(sfinstance);
        toggleSFinstance_gau_fields(sfinstance);
    }
    $('#donation_form_sfinstance').on('change', function () {
        toggleSFinstancefields();
    });

    function toggleSFinstance_campaign_fields(sfinstance){
        $('.donation_form_sf_campaign').css('display','none');
        if(sfinstance != ''){
            $('#donation_form_sf_campaign_'+sfinstance+'-container').css('display','block');
            $('#donation_form_sf_campaign_'+sfinstance).css('display','block');
        }
    }

    function toggleSFinstance_gau_fields(sfinstance){
        $('.donation_form_sf_gau').css('display','none');
        if(sfinstance != ''){
            $('#donation_form_sf_gau_'+sfinstance+'-container').css('display','block');
            $('#donation_form_sf_gau_'+sfinstance).css('display','block');
        }
    }
    function toggleSFinstance_gateway_fields(sfinstance){
        $('div[data-saiyan-dependency-id]').css('display','none');
        if(sfinstance != ''){
            $('#donation_v5_form_payment_gateways_'+sfinstance+'-container').css('display','block');
        }
    }




    $('input[type=\'hidden\'][value=\'expiry_month\']').closest('.content').find('div[id^=\'placeholder\']').hide();
    $('input[type=\'hidden\'][value=\'expiry_year\']').closest('.content').find('div[id^=\'placeholder\']').hide();

    // donation button live render functionality
    var donationFormButtonCaption = $('#donation_form_btn_caption');
    var donationFormButtonPadding = $('#donation_form_btn_padding + .slider-ui');
    var donationFormButtonFontSize =$('#donation_form_btn_font_size + .slider-ui');
    var donationFormButtonWidth = $('#donation_form_btn_width + .slider-ui');
    var donationFromButtonColor = $('#donation_form_btn_color');
    var donationFormButtonFontColor = $('#donation_form_btn_font_color');

    var donationFormButton = $('#donation_donation_page_btn_preview');

    donationFormButton
        .css({
            'padding': donationFormButtonPadding.slider('values', 0) + 'px',
            'font-size': donationFormButtonFontSize.slider('values', 0) + 'px',
            'width': donationFormButtonWidth.slider('values', 0) + 'px',
            'background-color': donationFromButtonColor.val(),
            'color': donationFormButtonFontColor.val()
        })
        .find('span').text(donationFormButtonCaption.val());

    donationFormButtonCaption.on('keyup', function() {
        donationFormButton.find('span').text($(this).val());
    });

    donationFormButtonPadding.on('slide', function(event, ui) {
        donationFormButton.css('padding', ui.value + 'px');
    });

    donationFormButtonFontSize.on('slide', function(event, ui) {
        donationFormButton.css('font-size', ui.value + 'px');
    });

    donationFormButtonWidth.on('slide', function(event, ui) {
        donationFormButton.css('width', ui.value + 'px');
    });

    donationFromButtonColor.wpColorPicker({
        change: function(event, ui) {
            var color = ui.color._color;
            var r = Math.floor(color / (256 * 256));
            var g = Math.floor(color / 256) % 256;
            var b = color % 256;

            donationFormButton.css(
                'background-color',
                'rgb(' + r + ', ' + g + ', ' + b + ')'
            );
        }
    });

    donationFormButtonFontColor.wpColorPicker({
        change: function(event, ui) {
            var color = ui.color._color;
            var r = Math.floor(color / (256 * 256));
            var g = Math.floor(color / 256) % 256;
            var b = color % 256;

            donationFormButton.css(
                'color',
                'rgb(' + r + ', ' + g + ', ' + b + ')'
            );
        }
    });

    // required default form fields based on its index
    var requiredFields = [
        'donor_type',
        'donor_company',
        'donor_first_name',
        'donor_last_name',
        'donor_email'
    ];
    $('#donation_v5_form_user_fields-container').find('.collapsible').each(function() {
        var formOption = $(this).find('select');

        // console.log($(this).find('input[id^=\'field_key\']').val());

        $(this).find('input[id^=\'field_key\']').attr('readonly', true);
        $(this).find('.saiyan-radio .saiyan-grouped-module').attr('readonly', true);
        if ($(this).find('input[id^=\'field_key\']').val() === 'donor_type')
            $(this).find('textarea').attr('readonly', true);

        if ($(this).find('input[id^=\'field_key\']').val() === 'donor_country') {
            $(this).find('textarea').attr('readonly', true);
            $(this).find('.saiyan-radio .saiyan-grouped-module').attr('readonly', false);
            $(this).find('.saiyan-radio .saiyan-grouped-module').find('.saiyan-radio-item').attr('readonly', true);
            $(this).find('.saiyan-radio .saiyan-grouped-module').find('.saiyan-radio-item input[value=\'text\']').closest('.saiyan-radio-item').attr('readonly', false);
            $(this).find('.saiyan-radio .saiyan-grouped-module').find('.saiyan-radio-item input[value=\'select\']').closest('.saiyan-radio-item').attr('readonly', false);
        }

        if ($(this).find('input[id^=\'field_key\']').val() !== 'donor_country') {
            $(this).find('div[id^=\'field_default_value\']').addClass('force-hide');
        }

        if (jQuery.inArray($(this).find('input[id^=\'field_key\']').val(), requiredFields) !== -1)
            formOption.attr('readonly', true);

        $(this).find('input:not([id^=\'field_label-\'])').attr('readonly', true);
    });

    $('#field_default_value-container').addClass('force-hide');
});