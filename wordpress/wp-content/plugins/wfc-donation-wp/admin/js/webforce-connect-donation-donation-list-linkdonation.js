/**
* jQuery
*/
var j = jQuery;

var LinkDonation = function() {

};

LinkDonation.prototype.checkOpportunityExist = function() {
	return new Promise(function (resolve, reject) {
		$.ajax({
	        url: ajaxurl,
	        type:'POST',
		    dataType:'json',
	        data: {
	            'action': 'wfc_don_linkdonation_action',
	            'step': '1',
	            'OpportunityId' : $('#wfc_OpportunityId').val(),
	        },
	        success: function(response) {
	        	resolve( {
					status : response.status,
					result_code : response.result_code,
					result_message : response.result_message,
				} );
	        },
	        error: function(response) {
	            reject( response );
	        }
	    });
	});
};

LinkDonation.prototype.checkRecurringExist = function() {
	return new Promise(function (resolve, reject) {
		$.ajax({
	        url: ajaxurl,
	        type:'POST',
		    dataType:'json',
	        data: {
	            'action': 'wfc_don_linkdonation_action',
	            'step': '2',
	            'RecurringId' : $('#wfc_RecurringId').val(),
	        },
	        success: function(response) {
	        	resolve( {
					status : response.status,
					result_code : response.result_code,
					result_message : response.result_message,
				} );
	        },
	        error: function(response) {
	            reject( response );
	        }
	    });
	});
};

LinkDonation.prototype.updateDonationAsLinked= function() {
	return new Promise(function (resolve, reject) {
		$.ajax({
	        url: ajaxurl,
	        type:'POST',
		    dataType:'json',
	        data: {
	            'action': 'wfc_don_linkdonation_action',
	            'step': '3',
	            'OpportunityId' : $('#wfc_OpportunityId').val(),
	            'RecurringId' : $('#wfc_RecurringId').val(),
	            'meta_id' : $('#wfc-donation-edit-cont').attr('wfc-donation-id'),
	        },
	        success: function(response) {
	        	resolve( {
					status : response.status,
					result_code : response.result_code,
					result_message : response.result_message,
				} );
	        },
	        error: function(response) {
	            reject( response );
	        }
	    });
	});
};