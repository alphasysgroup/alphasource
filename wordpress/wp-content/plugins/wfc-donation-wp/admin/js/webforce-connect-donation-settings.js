/**
* @author: Junjie Canonio
*/
jQuery(document).ready(function($){

	/**
	* Save donation settings
	*/
	$('#wfc_donation_savesettings_btn').on('click', function(){
		$(this).attr('disabled','disabled');
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving</div>',null,'info');
		$.ajax({
		    url: wfc_don_savesettings_param.url,
		    type:'POST',
		    dataType:'json',
		    data:{
		        'action': 'wfc_don_savesettings_action', 
		        nonce: wfc_don_savesettings_param.nonce,
		        data : $('#wfc_donation_settings').serializeArray(),
		        frequencies: $('#frequencies').val(),
		    },
		    success:function(response){
		    	//console.log(response);
		    	$('#wfc_donation_savesettings_btn').removeAttr('disabled');

		    	SaiyantistCore.snackbar.hide();
		    	if(response.status == 'Success'){
                	SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Successful</div>',3000,'success');
		    	}else{
		    		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Failed</div>',3000,'error');
		    	}
		    },
		    error: function(response) {
		        console.log(response);
		        $('#wfc_donation_savesettings_btn').removeAttr('disabled');
		        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU and Campaign Fetch Error</div>',3000,'error');
		    }
		});
	});

	$('#wfc_donation_resetsettings_btn').on('click', function() {
        $(this).attr('disabled','disabled');
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Resetting</div>',null,'info');
        $.ajax({
            url: wfc_don_resetsettings_param.url,
            type:'POST',
            dataType:'json',
            data:{
                'action': 'wfc_don_resetsettings_action',
                nonce: wfc_don_resetsettings_param.nonce,
            },
            success:function(response){
                console.log(response);
                $('#wfc_donation_resetsettings_btn').removeAttr('disabled');

                SaiyantistCore.snackbar.hide();
                if(response.status == 'Success'){
                	setTimeout(function() {
                		window.location.reload();
					}, 3000);
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Reset Successful</div>',3000,'success');
                }else{
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Reset Failed</div>',3000,'error');
                }
            },
            error: function(response) {
                console.log(response);
                $('#wfc_donation_resetsettings_btn').removeAttr('disabled');
                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">GAU and Campaign Fetch Error</div>',3000,'error');
            }
        });
	})
});
