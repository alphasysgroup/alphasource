/**
* @author: Junjie Canonio
*/
jQuery(document).ready(function($){
	console.log('DONATION LIST JS');
	var statusCode = $('#statusCode');
	var btn_resync_direct = $('#btn_resync_direct');
	var btn_forcesyncdonation_direct = $('#wfc_forcesyncdonation');
	var donation_cartItems = $('#btn_resync_direct-container');


	/**
	* resync donation
	*/
	var resyncdonation_icn = $('#wfc-don-resync-donation-icon');
	$('#btn_resync_direct').on('click', function() {
		console.log('Resyncing Donation');
		$(this).attr('disabled','disabled');
		resyncdonation_icn.removeClass('fa-spin');  
        resyncdonation_icn.addClass('fa-spin'); 
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Syncing Donation to Salesforce.</div>',null,'info');
		$.ajax({
            url: ajaxurl,
            type:'POST',
		    dataType:'json',
            data: {
                'action': 'wfc_don_resyncdonation_action',
                'donation_id' : $('#wfc-donation-edit-cont').attr('wfc-donation-id'),
                'RecurringId' : $('#RecurringId').val(),
                'sync_status' : $('#sync_statuses').val(),
                'pdwp_donation_type' : $('#pdwp_donation_type').val(),
            },
            success: function(response) {
            	console.log(response);
            	resyncdonation_icn.removeClass('fa-spin'); 
            	SaiyantistCore.snackbar.hide();
            	if(response.status == 'Success'){
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Successful</div>',3000,'success');
					setTimeout(function(){
						location.reload();
					}, 3000);
            	}else if(response.status == 'Warning'){
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Failed</div>',3000,'warning');
					setTimeout(function(){
						location.reload();
					}, 3000);
            	}else{
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Error</div>',3000,'error');
            	}
            },
            error: function(resp) {
                reject( resp );
                location.reload();
            }
        })
	});

	/**
	* force resync donation
	*/
	btn_forcesyncdonation_direct.on('click', function() {
		$(this).attr('disabled','disabled');
		$('#wfc-don-forcesyncdonation-icon').removeClass('fa-spin');  
        $('#wfc-don-forcesyncdonation-icon').addClass('fa-spin'); 
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Syncing Donation to Salesforce.</div>',null,'info');
		$.ajax({
            url: ajaxurl,
            type:'POST',
		    dataType:'json',
            data: {
                'action': 'wfc_don_resyncdonation_action',
                'donation_id' : $('#wfc-donation-edit-cont').attr('wfc-donation-id'),
                'RecurringId' : $('#RecurringId').val(),
                'sync_status' : $('#sync_statuses').val(),
                'pdwp_donation_type' : $('#pdwp_donation_type').val(),
            },
            success: function(response) {
            	$('#wfc-don-forcesyncdonation-icon').removeClass('fa-spin'); 
            	SaiyantistCore.snackbar.hide();
            	console.log(response);
            	if(response.status == 'Success'){
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Successful</div>',3000,'success');
            	}else if(response.status == 'Warning'){
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Failed</div>',3000,'warning');
            	}else{
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Error</div>',3000,'error');
            	}

            	setTimeout(function(){
               		location.reload();
                }, 3000);
            },
            error: function(resp) {
                reject( resp );
                location.reload();
            }
        })
	});


	/**
	* resync advance cart donation
	*/
	var advance_cartItems = $('#donation_cartItems').val();
	var advance_cartItems = jQuery.parseJSON( advance_cartItems );
	//console.log(donation_cartItems[0]);

	var btn_resync_direct_advancecart = $('.btn_resync_direct_advancecart').find('button');
	btn_resync_direct_advancecart.on('click', function(){
		var forcesyncdonation_icn = $(this).find('i');
        var suffix = $(this).attr('name');
        suffix = suffix.replace(/btn_resync_direct_advancecart/g,'');

		$('.btn_resync_direct_advancecart').attr('disabled','disabled');
		forcesyncdonation_icn.removeClass('fa-spin');  
        forcesyncdonation_icn.addClass('fa-spin'); 
      	
      	SyncAdvanceCart(suffix,forcesyncdonation_icn);
		console.log($('#wfc-donation-edit-cont').attr('wfc-donation-id'));
	});


	function SyncAdvanceCart(suffix,forcesyncdonation_icn){
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Syncing Donation to Salesforce.</div>',null,'info');
		$.ajax({
            url: ajaxurl,
            type:'POST',
		    dataType:'json',
            data: {
                'action': 'wfc_don_resyncadvancecart_action',
                'donation_id' : $('#wfc-donation-edit-cont').attr('wfc-donation-id'),
                'cart_suffix' : suffix
            },
            success: function(response) {
            	console.log(response);
            	forcesyncdonation_icn.removeClass('fa-spin'); 
            	SaiyantistCore.snackbar.hide();
            	if(response.status == 'Success'){
            		if(response.sync_result.STATUS == 'Success') {
            			SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Successful</div>',3000,'success');		
            		}else{
            			SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+response.sync_result.result+'</div>',3000,'warning');
            		}	
            	}else{
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Error</div>',3000,'error');
            	}
            	setTimeout(function(){
               		location.reload();
                }, 3000);
            },
            error: function(resp) {
                reject( resp );
                location.reload();
            }
        })  
	}

	$('input[name=exclude_on_cronsyncing]').on('change', function() {
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Updating Record.</div>',null,'info');
		$.ajax({
		    url: ajaxurl,
            type:'POST',
		    dataType:'json',
            data: {
                'action': 'wfc_don_excludefromcronsync_action',
                'donation_id' : $('#wfc-donation-edit-cont').attr('wfc-donation-id'),
                'exludecronsync' : $('input[name=exclude_on_cronsyncing]').val()
            },
            success: function(response) {
            	console.log(response);
            	SaiyantistCore.snackbar.hide();
            	if(response.status == 'Success'){
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Update Successful</div>',3000,'success');	
            	}else{
            		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Error</div>',3000,'error');
            	}
            },
            error: function(resp) {
                reject( resp );
                location.reload();
            }
        }) 
		console.log($('input[name=exclude_on_cronsyncing]').val());
	});	

	/**
	* link donation
	*/
	// var linkdonation_btn = $('#wfc_linkdonation');
	// linkdonation_btn.on('click', function() {
	// 	var linkdonation = new LinkDonation();
	// 	SaiyantistCore.snackbar.hide();
	// 	SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Checking Opportunity ID exist.</div>',null,'info');
	// 	linkdonation.checkOpportunityExist()
	// 	.then(function(result) {
	// 		console.log('Checking Opportunity ID exist');
	// 		console.log(result);
	// 		if(result.status == 'Success'){
	// 			SaiyantistCore.snackbar.hide();
	// 			if(result.result_code == 'id_good') {
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+result.result_message+'</div>',3000,'success');
	// 				SaiyantistCore.snackbar.hide();
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Checking Recurring ID exist.</div>',null,'info');
	// 				return linkdonation.checkRecurringExist();
	// 			}else{
	// 				return result;
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+result.result_message+'</div>',5000,'warning');
	// 			}
	// 		}
	// 	})
	// 	.then(function(result) {
	// 		console.log('Checking Recurring ID exist');
	// 		console.log(result);
	// 		SaiyantistCore.snackbar.hide();
	// 		if(result.status == 'Success'){
	// 			if(result.result_code == 'id_good') {
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+result.result_message+'</div>',null,'info');
	// 				SaiyantistCore.snackbar.hide();	
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Updating Donation.</div>',null,'info');
	// 				return linkdonation.updateDonationAsLinked();
	// 			}else{
	// 				return result;
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+result.result_message+'</div>',5000,'warning');
	// 			}
	// 		}
	// 	})	
	// 	.then(function(result) {
	// 		console.log('Updating donation');
	// 		console.log(result);
			
	// 		if(result.status == 'Success'){
	// 			if(result.result_code == 'update_done') {
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+result.result_message+'</div>',5000,'success');
	// 			}else{
	// 				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+result.result_message+'</div>',5000,'warning');
	// 			}
	// 		}
	// 	})		
	// 	.catch(function(error) {
	// 		console.error(error)
	// 	});	
	// });	

});