/**
* @author: Junjie Canonio
*/
jQuery(document).ready(function($){

    // interaction logic for pins and notes on the Donor List and Donor Details
    var pinsContainer = $(document).find('#wfc-donation-list-info');
    var pins = pinsContainer.find('.pins');
    var notes = pinsContainer.find('.notes');

    // increment top to align note to the designated pin
    var counter = 0;
    notes.find('.info-list-container').each(function() {
        var $this = $(this);

        var top = parseInt($this.css('top').replace('px', ''));

        $this.css('top', ((70 * counter++) + top) + 'px');
    });

    pins.find('.info-loading-icon').on('click', function(e) {
        e.stopPropagation();
        var $this = $(this);
        var targetNote = $(notes.find('.info-list-container')[$this.index()]);

        //console.log(pins.find('.info-loading-icon.active').length);

        if ($this.hasClass('active')) {
            if (pins.find('.info-loading-icon.active').length > 0) {
                $this.removeClass('active');
                targetNote.removeClass('active');
            }
        }
        else {
            pins.find('.info-loading-icon.active').removeClass('active');
            notes.find('.info-list-container.active').removeClass('active');

            $this.addClass('active');
            targetNote.addClass('active');
        }
    });
    notes.find('.info-list-container').on('click', function(e) {
        e.stopPropagation();
    });

    $(document).on('click', function() {
        pins.find('.info-loading-icon').removeClass('active');
        notes.find('.info-list-container').removeClass('active');
    });


    /* Campaign resync ajax functionality */
    $('#campaign').on('click', function() {
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Syncing campaign...</div>');
        $.ajax({
            url: ajaxurl,
            data: {
                'action': 'wfc_resync_campaign'
            },
            success: function(response) {
                var json = JSON.parse(response);
                var status = json.status;
                var message = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + json.message + '</div>';

                console.log(json);

                if (status === 'Success') {
                    SaiyantistCore.snackbar.show(message, 3000, 'success');

                    var selectElemCampaign = $('#donation_form_sf_campaign');
                    selectElemCampaign.empty();
                    selectElemCampaign.append(
                        $('<option></option>')
                            .attr('value', '')
                            .text('--Select--')
                    );
                    for (var i=0, count=Object.keys(json.campaign).length; i<count; i++) {
                        var key = Object.keys(json.campaign)[i];

                        selectElemCampaign.append(
                            $('<option></option>')
                                .attr('value', key)
                                .text(json.campaign[key])
                        );
                    }
                }
                else if (status === 'Failed') {
                    SaiyantistCore.snackbar.show(message, 3000, 'error');
                }
            }
        })
    });

    /* GAU resync ajax functionality */
    $('#gau').on('click', function() {
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Syncing GAU...</div>');
        $.ajax({
            url: ajaxurl,
            data: {
                'action': 'wfc_resync_gau'
            },
            success: function(response) {
                var json = JSON.parse(response);
                var status = json.status;
                var message = '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + json.message + '</div>';

                console.log(json);

                if (status === 'Success') {
                    SaiyantistCore.snackbar.show(message, 3000, 'success');

                    var selectElemGAU = $('#donation_form_sf_gau');
                    selectElemGAU.empty();
                    selectElemGAU.append(
                        $('<option></option>')
                            .attr('value', '')
                            .text('--Select--')
                    );
                    for (var i=0, count=Object.keys(json.gau).length; i<count; i++) {
                        var key = Object.keys(json.gau)[i];

                        selectElemGAU.append(
                            $('<option></option>')
                                .attr('value', key)
                                .text(json.gau[key])
                        );
                    }
                }
                else if (status === 'Failed') {
                    SaiyantistCore.snackbar.show(message, 3000, 'error');
                }
            }
        })
    });


    /* toggle logic for amount levels frequency */
    var amountLevelsContainer = $('#donation_v5_form_amount_levels-container');
    var itemStructure = amountLevelsContainer.find('.add-item-container .item-structure');
    amountLevelsContainer.find('.collapsible').each(function() {
        var $this = $(this);
        var switchElem = $this.find('[id^=\'is_default\'] input[type=\'checkbox\']');
        switchElem.on('change', function() {
            if ($(this).not(':checked')) {
                amountLevelsContainer.find('.collapsible [id^=\'is_default\'] input[type=\'checkbox\']').prop('checked', false);
                $(this).prop('checked', true);
            }
        });
        $this.find('button[data-item-action=\'delete\']').on('click', function() {
            if ($this.find('[id^=\'is_default\'] input[type=\'checkbox\']').is(':checked')) {
                amountLevelsContainer.find('.collapsible [id^=\'is_default\'] input[type=\'checkbox\']').prop('checked', false);
                amountLevelsContainer.find('.collapsible [id=\'is_default-0\']')
            }
        })
    });
    itemStructure.find('button').on('click', function() {
        if (itemStructure.find('.item-structure-wrapper input[type=\'checkbox\']').is(':checked')) {
            amountLevelsContainer.find('.collapsible [id^=\'is_default\'] input[type=\'checkbox\']').prop('checked', false);
        }
        // delay execution to 0.1s to be able to bind event after the item is added
        setTimeout(function() {
            var switchElem = amountLevelsContainer.find('.collapsible input[type=\'checkbox\']');
            switchElem.on('change', function() {
                if ($(this).not(':checked')) {
                    amountLevelsContainer.find('.collapsible [id^=\'is_default\'] input[type=\'checkbox\']').prop('checked', false);
                    $(this).prop('checked', true);
                }
            });
            amountLevelsContainer.find('.collapsible button[data-item-action=\'delete\']').on('click', function() {
                if ($(this).parent().parent().find('.content input[type=\'checkbox\']').is(':checked')) {
                    amountLevelsContainer.find('.collapsible [id^=\'is_default\'] input[type=\'checkbox\']').prop('checked', false);
                    amountLevelsContainer.find('.collapsible [id=\'is_default-0\'] input[type=\'checkbox\']').prop('checked', true);
                }
            });
        }, 100);
    });


    // form validation logic
    if (typeof wfc_js_object === 'undefined') return false;
    var publishButton = $('form#post #publish');
    var accordion = $('#donation_form-container').find('.accordion');

    var value = wfc_js_object.post_status;
    switch (value) {
        case 'auto-draft':
            value = 'Publish';
            action = 'publish';
            break;
        case 'publish':
            value = 'Update';
            action = 'update';
            break;
    }

    publishButton
        .hide()
        .after(
            '<input id="wfc-publish" type="button" class="button button-primary button-large" value="' + value + '"/>'
        );

    $('form#post #wfc-publish').on('click', function() {
        var donationTypes = $('#donation_form_donation_types-container');
        var paymentGateways = $('#donation_v5_form_payment_gateways-container');
        if (donationTypes.find('.saiyan-checkbox-item input[type=\'checkbox\']:checked').length === 0) {
            SaiyantistCore.snackbar.show('Check at least one donation type', 3000, 'warning');
            accordion.accordion('option', 'active', 0);
            $('html, body').animate({
                scrollTop: donationTypes.offset().top
            }, 500, null, function() {
                donationTypes.addClass('focus');
                donationTypes.bind('oanimationend animationend webkitAnimationEnd', function() {
                    $(this).removeClass('focus');
                });
            });
            return false;
        }
        // if (paymentGateways.find('.saiyan-checkbox-item input[type=\'checkbox\']:checked').length === 0) {
        //     SaiyantistCore.snackbar.show('Check at least on Payment Gateways', 3000, 'warning');
        //     accordion.accordion('option', 'active', 3);
        //     $('html, body').animate({
        //         scrollTop: paymentGateways.offset().top
        //     }, 500, null, function() {
        //         paymentGateways.addClass('focus');
        //         paymentGateways.bind('oanimationend animationend webkitAnimationEnd', function() {
        //             $(this).removeClass('focus');
        //         });
        //     });
        //     return false;
        // }

        publishButton.click();
    });


    // logic for resetting values on modules using sortable-item
    $('[id^=\'donation-reset\']').on('click', function() {
        if (confirm('Do you want to reset these data?\n(Note: Current values will be reset to default and cannot be undone.)')) {
            var sortable = $(this).attr('id').substr('donation-reset-'.length);

            $.ajax({
                url: ajaxurl,
                data: {
                    'action': 'donation_form_fields_reset',
                    'post_id': $('#post').find('#post_ID').val(),
                    'sortable_field': sortable
                },
                success: function(response) {
                    var json = JSON.parse(response);
                    if (json.success) window.location.reload();
                    else alert('Failed to reset data or data is already been reset.');
                }
            })
        }
    });


    // controller for testing thanyou email
    $('#donation_form_thankyou_email_test').on('click', function() {
        var email = prompt('Enter your email to test and check it if it sends.');

        if (typeof email !== 'undefined' && email !== null) {
            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sending email...</div>');
            $.ajax({
                url: ajaxurl,
                data: {
                    'action': 'wfc_donation_donor_thankyou_email',
                    'post_id': $('#post').find('#post_ID').val(),
                    'email': email
                },
                success: function(response) {
                    var json = JSON.parse(response);

                    SaiyantistCore.snackbar.show(
                        '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + (json.success ? 'Email successfully sent' : 'Failed to send email') + '</div>',
                        3000,
                        json.success ? 'success' : 'error'
                    );
                }
            })
        }
    });


    // controller for testing admin notification
    $('button[id^=\'donation_form_admin_notification_test_\']').on('click', function() {
        var test = $(this).attr('id').replace('donation_form_admin_notification_test_', '');

        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sending email...</div>');
        $.ajax({
            url: ajaxurl,
            data: {
                'action': 'wfc_donation_admin_notification_test',
                'post_id': $('#post').find('#post_ID').val(),
                'test': test
            },
            success: function(response) {
                var json = JSON.parse(response);

                SaiyantistCore.snackbar.show(
                    '<div style="text-align: center;font-size: 14px;font-weight: 500;">' + (json.success ? 'Email successfully sent' : 'Failed to send email') + '</div>',
                    3000,
                    json.success ? 'success' : 'error'
                );
            }
        })
    });


    $('#donation_form_thankyou_email_focus_prereq').on('click', function() {
        accordion.accordion('option', 'active', 6);
        $('html, body').animate({
            scrollTop: $('#ui-id-13.saiyan-accordion-title').offset().top
        }, 500);
    });
});