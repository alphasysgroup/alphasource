/**
* @author: Junjie Canonio
*/
jQuery(document).ready(function($){
    console.log('PAYMENT GATEWAY JS');

    var wfc_don_admin_paymntgate = function() {};
    var gatewaylist = new wfc_don_admin_paymntgate();

    /**
    * Get payment gateway list --------------------------------------------------------------
    */
    wfc_don_admin_paymntgate.prototype.wfc_don_getpaymntgate = function() {
        return new Promise(function (resolve, reject) {

            $.ajax({
                url: wfc_don_admin_paymntgate_param.url,
                type:'POST',
                dataType:'json',
                data:{
                    'action': 'wfc_don_getpaymntgate_action',
                    'sfinstanceid' : $('#wfc-don-gateway-list-container').attr('sfinstanceID'),
                    nonce: wfc_don_admin_paymntgate_param.nonce,
                },
                success:function(response){
                    console.log(response);
                    resolve( {
                        status: 'success',
                        gateways: response.gateway_arr
                    } );
                },
                error: function(response) {
                    reject( response );
                }
            });

        });
    };

    wfc_don_admin_paymntgate.prototype.build_gateway_item = function( param ) {
        return new Promise(function (resolve, reject) {
            
            var gateway_item = '';
            $.each( param, function( key, value ) {
                gateway_item += '<li class="list-group-item ' + value.sandbox_mode_state + '">';
                gateway_item += '<div class="row">';
                gateway_item += '<div class="wfc-don-list-item col-6">';
                gateway_item += '<span>' + value.sf_name + '</span>';
                gateway_item += '</div>';
                gateway_item += '<div class="wfc-don-list-item col-1">';
                gateway_item += '<span><i class="' + value.sandbox_mode_image + '" style="font-size:18px"></i></span>';
                gateway_item += '</div>';
                gateway_item += '<div class="wfc-don-list-item col-2">';
                gateway_item += '<span><i class="' + value.tokenization_image + '" style="font-size:18px"></i></span>';
                gateway_item += '</div>';
                gateway_item += '<div class="col-2"><span>';
                gateway_item += value.gateway_logo;
                gateway_item += '</span></div>';
                gateway_item += '<div class="col-1">';
                gateway_item += '<a href="' + value.settings_url + '">';
                gateway_item += '<span><i id="' + value.sf_id + '" class="fa fa-gear wfc-don-gateway-item-gear" style="font-size:36px"></i></span>';
                gateway_item += '</a>';
                gateway_item += '</div>';
                gateway_item += '</div>';
                gateway_item += '</li>';
            });  

            var easyPaginateNav = $('.easyPaginateNav');
            easyPaginateNav.remove();

            var easyPaginate = $('#wfc-don-gateways-easyPaginate');
            easyPaginate.empty();
            easyPaginate.append(gateway_item);
            easyPaginate.easyPaginate({
                paginateElement: 'li',
                elementsPerPage: 10,
                effect: 'climb'
            });

            wfc_don_toggle_gateway_settings();
        });
    };

    gatewaylist.wfc_don_getpaymntgate()
    .then(function( result ) {
        var result_gateways = result.gateways;
        if(result_gateways.length === 0){
            $('#wfc-don-gateways-easyPaginate').append('<li class="list-group-item"><div>There no available payment gateway.</div></li>');
        }else{
            gatewaylist.build_gateway_item( result.gateways );
        }

    })
    .catch(function( error ) {
        console.error( error )
    });
    /**
    * Get payment gateway list --------------------------------------------------------------
    */    


    /**
    * Resync payment gateway --------------------------------------------------------------
    */   
    var resyncgateway_btn = $('#wfc-don-resync-gateway-btn');
    var resyncgateway_icn = $('#wfc-don-resync-gateway-icon');
    resyncgateway_btn.on('click', function(){
        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Fethcing Payment Gateway</div>',null,'info');
        resyncgateway_icn.removeClass('fa-spin');  
        resyncgateway_icn.addClass('fa-spin'); 
        resyncgateway_btn.attr('disabled', 'disabled');
        //resyncgateway_btn.removeClass('btn-primary');   

        var easyPaginateNav = $('.easyPaginateNav');
        easyPaginateNav.remove();

        var easyPaginate = $('#wfc-don-gateways-easyPaginate');
        easyPaginate.empty();

        gatewaylist.wfc_don_syncpaymntgate()
        .then(function( result ) {
            return result;
        })
        .then(function( result ) {
            if(result.status == 'Success'){
                gatewaylist.wfc_don_getpaymntgate()
                .then(function( result ) {
                    gatewaylist.build_gateway_item( result.gateways ); 
                    resyncgateway_icn.removeClass('fa-spin'); 
                    resyncgateway_btn.removeAttr('disabled');
                    //resyncgateway_btn.addClass('btn-primary'); 
                    SaiyantistCore.snackbar.hide();
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Successful</div>',3000,'success');   
                })  
                .catch(function( error ) {
                    console.error( error )
                });
            }else if(result.status == 'Failed'){
                resyncgateway_icn.removeClass('fa-spin');
                resyncgateway_btn.removeAttr('disabled');
                SaiyantistCore.snackbar.hide();
                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Sync Failed</div>',3000,'error');
            }
        })    
        .catch(function( error ) {
            console.error( error );

            resyncgateway_icn.removeClass('fa-spin'); 
            resyncgateway_btn.removeAttr('disabled');
            //resyncgateway_btn.addClass('btn-primary'); 
            SaiyantistCore.snackbar.hide();   
        });


    });

    wfc_don_admin_paymntgate.prototype.wfc_don_syncpaymntgate = function() {
        return new Promise(function (resolve, reject) {

            $.ajax({
                url: wfc_don_admin_paymntgate_param.url,
                type:'POST',
                dataType:'json',
                data:{
                    'action': 'wfc_don_syncpaymntgate_action',
                    'sfinstanceid' : $('#wfc-don-gateway-list-container').attr('sfinstanceID'),
                    nonce: wfc_don_admin_paymntgate_param.nonce,
                },
                success:function(response){
                    console.log(response);
                    resolve( {
                        status: response.status,
                        response: response.response, 
                    } );
           
                },
                error: function(response) {
                    reject( response );
                }
            });

        });
    };
    /**
    * Resync payment gateway --------------------------------------------------------------
    */  


    /**
    * Open payment gateway settings --------------------------------------------------------------
    */  
    var wfc_don_toggle_gateway_settings =  function(){
        var gatewayitem_gear = $('.wfc-don-gateway-item-gear');

        gatewayitem_gear.each(function(i) {
            $(this).on('click', function(){
                //console.log($(this).attr('id'));
            });
        });    
     
    }

    // Promise.all([paymntgate.wfc_don_getpaymntgate(1), paymntgate.wfc_don_getpaymntgate(2)]
    // .then(function( result ) {
    //     console.log( result );
    // })
    // .catch(function( error ) {
    //     console.error( error )
    // });
    
    // UI/UX controller for payment gateways regarding the tokenize option
    var modalContainer = $('.wfc-how-to-modal-container');
    modalContainer.css('transition', '400ms ease-out');

    $('.wfc-warn-info-button').on('click', function() {
        modalContainer.toggleClass('slide-in');
    });

    $('.wfc-how-to-modal-container i.fa').on('click', function() {
        modalContainer.removeClass('slide-in');
    });

    $('.wfc-hot-to-modal img').on('click', function(e) {
        e.stopPropagation();
    });

    modalContainer.on('click', function() {
        modalContainer.removeClass('slide-in');
    });
});