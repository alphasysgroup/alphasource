/**
* @author: Junjie Canonio
*/
jQuery(document).ready(function($){
	/**
	* Check Google Api Key
	*/
	if($('#google_api_key').val() == '' || $('#google_api_key').val() == null) {
		$('#enable_validation-true').removeAttr('checked');
		$('#enable_validation-false').attr('checked','checked');
		$('#google_api_key').css('border-left','4px solid #dc3232');
	} else {
		$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+'ozamiz'+'&key='+$('#google_api_key').val(), function (data) {
			google_geocode_request_status = data['status'];
			if(google_geocode_request_status=='REQUEST_DENIED'){
				$('#enable_validation-true').removeAttr('checked');
				$('#enable_validation-false').attr('checked','checked');	
				$('#google_api_key').css('border-left','4px solid #dc3232');		
			}else{
				$('#google_api_key').css('border-left','4px solid #46b450');
			}
		});
	}


	/**
	* Check Google Api Key On Input Focusout
	*/
	$('#google_api_key').on('focusout', function(){
		if($('#google_api_key').val() == '' || $('#google_api_key').val() == null) {
			$('#enable_validation-true').removeAttr('checked');
			$('#enable_validation-false').attr('checked','checked');
			$('#google_api_key').css('border-left','4px solid #dc3232');
		} else {
			$.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+'ozamiz'+'&key='+$('#google_api_key').val(), function (data) {
				google_geocode_request_status = data['status'];
				if(google_geocode_request_status=='REQUEST_DENIED'){
					$('#enable_validation-true').removeAttr('checked');
					$('#enable_validation-false').attr('checked','checked');
					$('#google_api_key').css('border-left','4px solid #dc3232');			
				}else{
					$('#google_api_key').css('border-left','4px solid #46b450');
				}
			});
		}
	});
});