<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @since      1.0.0
 *
 * @package    Webforce_Connect_Donation
 * @author     AlphaSys Pty. Ltd. <https://alphasys.com.au>
 */
class Webforce_Connect_Donation_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */

	/**
	 * The main class of the plugin
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $class    The main class of this plugin
	 */
	public $class;

	public function __construct( $plugin_name, $version, $parent ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->class = $parent;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles($hook) {

		/*
		* register_style 'wfc-donation-admin-css'
		*/
		wp_register_style( 
			'wfc-donation-admin-css', 
			plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-admin.css', 
			array(), 
			$this->version,
			 'all' 
		);

        /*
		* register_style 'wfc-donation-sfinstance-css'
		*/
		wp_register_style(
			'wfc-donation-sfinstance-css',
			plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-sfinstance.css',
			array(),
			$this->version,
			'all'
		);


		/*
		* register_style 'wfc-donation-payment-gateway-css'
		*/
		wp_register_style( 
			'wfc-donation-payment-gateway-css', 
			plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-payment-gateway.css', 
			array(), 
			$this->version, 
			'all' 
		);

		/*
		* register_style 'webforce-connect-donation-donation-list-css'
		*/
		wp_register_style(
			'webforce-connect-donation-donation-list-css',
			plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-donation-list.css',
			array(),
			$this->version,
			'all'
		);

		/*
		* register_style 'webforce-connect-donation-settings-css'
		*/
		wp_register_style(
			'webforce-connect-donation-settings-css',
			plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-settings.css',
			array(),
			$this->version,
			'all'
		);

		/*
		* enqueue donation logs css
		*/
		wp_register_style( 
			'webforce-connect-donation-donation-logs', 
			plugin_dir_url( __FILE__ ) . 'css/webforce-connect-donation-donation-logs.css', 
			array( 'dashicons' ), 
			$this->version, 
			'all' 
		); 
		global $post;
		if ( $hook == 'post-new.php' || $hook == 'post.php' ) {
	        if ( 'wfc_donation_logs' === $post->post_type ) {  
				wp_enqueue_style('webforce-connect-donation-donation-logs');     	
	        }
	    }

	    if (isset($_GET['post_type']) && $_GET['post_type'] == 'wfc_donation_logs') {
	    	wp_enqueue_style('webforce-connect-donation-donation-logs');      
	    }

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-admin.js', array( 'jquery' ), $this->version, true );

		/*
		* register_script 'wfc-donation-payment-gateway-js'
		*/
		wp_register_script(
			'wfc-donation-sfinstance-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-sfinstance.js',
			array( 'jquery' ),
			$this->version,
			true
		);

		/*
        * Localize "wfc_don_admin_sfinstance_param" on 'wfc-donation-sfinstance-js' script
        */
		wp_localize_script(
			'wfc-donation-sfinstance-js',
			'wfc_don_admin_sfinstance_param',
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('ajax-nonce'),
			)
		);

		/*
		* Localize "wfc_don_fetchgaucamp_param" on 'wfc-donation-sfinstance-js' script
		*/
		wp_localize_script(
			'wfc-donation-sfinstance-js',
			'wfc_don_fetchgaucamp_param',
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('ajax-nonce'),
			)
		);


		/*
		* register_script 'wfc-donation-payment-gateway-js'
		*/
		wp_register_script( 
			'wfc-donation-payment-gateway-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-payment-gateway.js',
			array( 'jquery' ),
			$this->version, 
			true
		);

        /*
        * Localize "wfc_don_getpaymntgate_param" on 'wfc-donation-payment-gateway-js' script
        */
		wp_localize_script(
			'wfc-donation-payment-gateway-js',
			'wfc_don_admin_paymntgate_param',
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('ajax-nonce'),
			)
		);

		/*
		* register_script 'webforce-connect-donation-settings-js'
		*/
		wp_register_script(
			'webforce-connect-donation-settings-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-settings.js',
			array( 'jquery' ),
			$this->version,
			false
		);

		/*
		* Localize "wfc_don_savesettings_param" on 'webforce-connect-donation-settings-js' script 
		*/	
		wp_localize_script( 
			'webforce-connect-donation-settings-js', 
			'wfc_don_savesettings_param',
			array( 
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('ajax-nonce'), 
			) 
		);

		/*
		 * Localize "wfc_don_resetsettings_param" on 'webforce-connect-donation-settings-js' script
		 */
		wp_localize_script(
			'webforce-connect-donation-settings-js',
			'wfc_don_resetsettings_param',
			array(
				'url' => admin_url('admin-ajax.php'),
				'nonce' => wp_create_nonce('ajax-nonce'),
			)
		);

		/*
		* register_script 'webforce-connect-donation-address-validation-js'
		*/
		wp_register_script(
			'webforce-connect-donation-address-validation-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-address-validation.js',
			array( 'jquery' ),
			$this->version,
			false
		);

		/*
		* register_script 'jquery-ui-js'
		*/
		wp_register_script(
			'jquery-ui-js',
			'https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js',
			array( 'jquery' ),
			$this->version,
			false
		);


		/*
		* register_script 'wfc-donation-easyPaginate-js'
		*/
		wp_register_script( 
			'wfc-donation-easyPaginate-js', 
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-jquery.easyPaginate.js', 
			array( 'jquery' ),
			$this->version, 
			false 
		);


		/*
		* register_script 'webforce-connect-donation-donation-list-js'
		*/
		wp_register_script( 
			'webforce-connect-donation-donation-list-js', 
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-donation-list.js', 
			array( 'jquery' ),
			$this->version, 
			false 
		);

		wp_register_script( 
			'webforce-connect-donation-donation-list-linkdonation-js', 
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-donation-list-linkdonation.js', 
			array( 'jquery' ),
			$this->version, 
			false 
		);

		wp_register_script(
			'webforce-connect-donation-donation-list-paymenttransaction-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-donation-list-paymenttransaction.js',
			array( 'jquery' ),
			$this->version,
			false
		);

		/*
		 * register script for form settings
		 */
		wp_register_script(
			'webforce-connect-donation-form-settings-js',
			plugin_dir_url( __FILE__ ) . 'js/webforce-connect-donation-form-settings.js',
			array( 'jquery' ),
			$this->version,
			false
		);
	}
	
	/**
	 * This Function displays admin pages for WFC Donation.
	 *
	 * @author Junjie Canonio
	 *
	 * @param       $template
	 * @param array $params
	 *
	 *
	 * @return false|string
     *
     * @LastUpdated  March 27, 2018
	 */
	public function wfc_view($template, $params = array()) {
		ob_start();
		extract($params);
		require(plugin_dir_path(dirname( __FILE__ )) . '/admin/partials/' . $template . '.php');
		$var = ob_get_contents();
		ob_end_clean();
		return $var;
	}
	
	/**
	 * Registers the Admin Menu Item for the Donation Form on the admin area.
	 *
	 * @author Junjie Canonio
	 *
     * @LastUpdated  March 27, 2018
	 */
	public function register(){
		$labels = array(
			'name'               => __( 'Donation Forms', 'webforce-connect-donation' ),
			'singular_name'      => __( 'Donation Forms list', 'webforce-connect-donation' ),
			'add_new'            => __( 'Add Donation Form', 'webforce-connect-donation' ),
			'add_new_item'       => __( 'Add Donation Form', 'webforce-connect-donation' ),
			'edit_item'          => __( 'Edit Donation Form', 'webforce-connect-donation' ),
			'new_item'           => __( 'New Donation Form', 'webforce-connect-donation' ),
			'view_item'          => __( 'View Donation Form', 'webforce-connect-donation' ),
			'search_items'       => __( 'Search Donation Forms', 'webforce-connect-donation' ),
			'not_found'          => __( 'No Donation Forms found', 'webforce-connect-donation' ),
			'not_found_in_trash' => __( 'No Donation Forms found in trash', 'webforce-connect-donation' ),
		);

		$supports = array(
			'title',
			'editor',
			'thumbnail',
		);

		$args = array(
			'labels'          => $labels,
			'supports'        => $supports,
			'public'          => true,
			'capability_type' => 'post',
			'exclude_from_search' => true,
			'publicly_queryable' => false,
			'rewrite'         => array( 'slug' => 'donation-form', ),
			'menu_position'   => null,
			'menu_icon'       => 'dashicons-money',
			'can_export'      => true,
			'has_archive'     => true,
			'show_in_menu'	  => false,
			'taxonomies'      => array( 'post_format' )
		);

		register_post_type( 'pdwp_donation', $args );

		add_theme_support( 'post-thumbnails', array('post', 'page') );
	}

	/**
	 * Modifies donation custom posttype pdwp_donation table headers
	 *
	 * @author Junjie Canonio
	 * @param array $columns
	 * @return array
     *
     * @LastUpdated  April 30, 2018
	 */
	public function wfc_donation_forms_headers( $columns ) {
		?>
		<style type="text/css">
			thead th,thead th a, thead td{
	            color: #ffffff !important;
	            background-color: #1497c1 !important;
	            font-weight: 600 !important;
	        }

	        th .sorting-indicator:before{
	            color: #ffffff !important;
	        }

	        tfoot{
	            display: none !important;
	        }
		</style>
		<?php
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Donation Name', 'webforce-connect-donation' ),
			'amount_level' => __( 'Amounts', 'webforce-connect-donation' ),
			'donations' => __( 'Donations', 'webforce-connect-donation' ),
			'income' => __( 'Income', 'webforce-connect-donation' ),
			'donation_target' => __( 'Target', 'webforce-connect-donation' ),
			'donation_type' => __( 'Donation Types', 'webforce-connect-donation' ),
			'campaign_shortcode' => __( 'Shortcode', 'webforce-connect-donation' ),
			'date' => __('Date', 'webforce-connect-donation')
		);
		return $columns;
	}
	
	/**
	 * This function will display post type pdwp_donation column data
	 *
	 *
	 * @author Junjie Canonio
	 *
	 * @param $column
	 * @param $post_id
     *
     * @LastUpdated  April 30, 2018
	 */
	public function wfc_donation_forms_data( $column, $post_id ) {
		$WFC_DonationForms = new WFC_DonationForms();

		$prefix = 'donation';

		switch( $column ) {
			
			case 'title' :
				 
			break;

			case 'amount_level' :

				$amount_level = get_post_meta( $post_id, 'donation_v5_form_amount_levels', true );
				
				if(!empty($amount_level)){
					$amount_level = json_decode($amount_level,true);
					$am = array();
					foreach ( $amount_level as $key => $opt ) {
						foreach ($opt as $key => $value) {
							array_push( $am , $value['values']['amount_level'] );						
						}
					}
					echo '[' . implode(', ', $am) . ']';					
				}else{
					$amount_level = get_post_meta( $post_id, 'donation_form_amount_levels', true );
					$am = array();
					if (! empty($amount_level)) {
						foreach ( $amount_level as $opt ) {
							array_push( $am , $opt['amount_level'] );
						}
					}
					echo '[' . implode(', ', $am) . ']';
				}

			break;

			case 'donations' :
				echo $WFC_DonationForms->wfc_count_donations_by_post_id( $post_id );
			break;

			case 'income' :

				/*
				* currency
				*/
				$form_currency = get_post_meta( $post_id, 'donation_cc_currency', true );
				$currency = !empty( $form_currency ) ? $WFC_DonationForms->wfc_get_currencySymbol( $form_currency ) : '';

				/*
				* total
				*/
				$total = $WFC_DonationForms->wfc_total_donations( $post_id );
				echo ( $total )  ? $currency . esc_html( number_format( (int) $total, 2, '.', ',') ) : $currency . '0.00';

			break;
			
			case 'campaign_shortcode' :
 				echo '<input style="width: 100%;" onclick="this.setSelectionRange(0, this.value.length)" type="text" class="shortcode-input" readonly="" value="'.esc_html( '[pronto-donation-form id=' . $post_id .']' ).'">';
			break;
			
			case 'donation_type' :

				$donation_types = get_post_meta( $post_id, 'donation_v5_form_donation_types', true );
				if(!empty($donation_types)){
					$donation_types = unserialize($donation_types);

	 				$types = array();
	 				if( $donation_types ) {
	 					foreach ($donation_types as $k => $v) {
	 						if( $v === 'true' ) {
	 							array_push( $types, ucfirst( $k ) );	
	 						}
	 					}
	 				}
	 				echo implode(', ', $types );
				}else{						
	 				$donation_types = get_post_meta( $post_id, 'donation_form_donation_types', true );
	 				$types = array();
	 				if( $donation_types ) {
	 					foreach ($donation_types as $k => $v) {
	 						if( $v === 'true' ) {
	 							array_push( $types, ucfirst( $k ) );	
	 						}
	 					}
	 				}
	 				echo implode(', ', $types );
				}

			break;

			case 'donation_target' :
				/*
				* settings
				*/
				$setting = get_option( 'pronto_donation_settings', 0 );


				/*
				* currency
				*/
				$form_currency = get_post_meta( $post_id, 'donation_cc_currency', true );
				$currency = !empty( $form_currency ) ? $WFC_DonationForms->wfc_get_currencySymbol( $form_currency ) : '';

 				$donate_target = get_post_meta( $post_id, $prefix . '_target', true );
 				echo ( $donate_target )  ? $currency . esc_html( number_format( (int) $donate_target, 2, '.', ',') ) : 'Not Set';
			break;
		}
	}
	
	/**
	 * Add Donation List  submenu on Webforce Connect menu
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  March 27, 2018
	 */
	public function admin_menu() {
		if( class_exists( 'Pronto_Base' ) ) {

			add_menu_page(
				esc_html__( 'Donation List Page', 'webforce-connect-donation' ),
				esc_html__( 'Donations', 'webforce-connect-donation' ),
			    'manage_options',
				'donation-list-page',
				array( $this, 'wfc_donation_list' ),
			    'dashicons-index-card',
			    101
			);

			add_submenu_page(
				'pronto-ultimate',
				__('Donation', 'webforce-connect-donation'),
				__('Donation', 'webforce-connect-donation'),
				'manage_options',
				'wfc-donation',
				array( $this, 'wfc_donation_submenu' )
			);

		}
	}
	
	/**
	 * Displays the list of donations.
	 *
	 * @author Junjie Canonio
	 * @LastUpdated  March 27, 2018
	 */
	public function wfc_donation_list(){

		/*
		* enqueue_style
		*/

		wp_enqueue_style( 'wfc-donation-admin-css' );
		wp_enqueue_style( 'wfc-BootstrapCDN-font-awesome-css' );
		wp_enqueue_style( 'webforce-connect-donation-donation-list-css' );

		/*
		* enqueue_script
		*/
		if (isset($_GET['action']) && $_GET['action'] === 'view' && isset($_GET['ID'])) {
			wp_enqueue_script( 'webforce-connect-donation-donation-list-js' );
			wp_enqueue_script( 'webforce-connect-donation-donation-list-linkdonation-js' );
			wp_enqueue_script( 'webforce-connect-donation-donation-list-paymenttransaction-js' );

			/*
            * Localize "wfc_don_paymenttransaction_param" on 'webforce-connect-donation-donation-list-paymenttransaction-js' script
            */
			wp_localize_script(
				'webforce-connect-donation-donation-list-paymenttransaction-js',
				'wfc_don_paymenttransaction_param',
				array(
					'url' => admin_url('admin-ajax.php'),
					'donation_id' => $_GET['ID'],
					'nonce' => wp_create_nonce('ajax-nonce'),
				)
			);
		}

		$WFC_DonationList = new WFC_DonationList();

		$search = isset( $_GET['s'] ) ? $_GET['s'] :'';
		
		$paged = isset( $_REQUEST['paged'] ) ? $_REQUEST['paged'] : 1;
		$exta_con = array(
			'limit'  => 10,
			'paged'  => $paged,
			'offset' => 10,
			);

		$donations = $WFC_DonationList->wfc_get_donations_list( array(
			array(
				'key' 		=> 'meta_value',
				'operator' 	=> 'LIKE',
				'value' 	=> "'%{$search}%'",
				),
		), $exta_con);

		$data_donation_COUNT = $WFC_DonationList->wfc_get_donations_list_count( array(
			array(
				'key' 		=> 'meta_value',
				'operator' 	=> 'LIKE',
				'value' 	=> "'%{$search}%'",
			)
		));

		$data_donation = $WFC_DonationList->wfc_donation_donorList( $donations );

		$data['admin'] = $this;
		$data['donation_data'] = $data_donation;

		$columns = array(
			// 'ID' => 'ID',
			'data_version' => __( '' ) ,
			'title' => __( 'Donation', 'webforce-connect-donation' ),
			'donation_form' => __( 'Donation Form', 'webforce-connect-donation' ),
			'donation_type' => __( 'Donation Type', 'webforce-connect-donation' ),
			'transaction_status' => __( 'Transaction Status', 'webforce-connect-donation' ),
			'sync_status' => __( 'Sync Status', 'webforce-connect-donation' ),
			'amount' => __( 'Amount', 'webforce-connect-donation' ),
			'date' => __( 'Date', 'webforce-connect-donation' ),
		);

		if(isset($_GET['did']))
		{

		}
		else
		{
			$data['tableList'] = new WFC_List_Table($data_donation, $columns, 10, true, array( 'view'), array(), $data_donation_COUNT);

			echo $this->wfc_view('webforce-connect-donation-donation-list', $data);
		}
	}
	
	/**
	 * Register metabox form settings for donation form
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  April 27, 2018
	 */
	public function wfc_register_formsettings() {
	    add_meta_box( 
	    	'wfc-donation-form-settings', 
	    	__( 'Form Settings', 'webforce-connect-donation' ),
	    	array($this, 'wfc_register_formsettings_callback'), 
	    	'pdwp_donation',
	    	'normal',
	        'default' 
	    );
	}
	
	/**
	 * Form settings meta box callback.
     *
	 * @author Junjie Canonio
     *
     * @LastUpdated  April 27, 2018
	 */
	public function wfc_register_formsettings_callback(){
		include 'partials/webforce-connect-donation-form-settings.php';
	}
	
	/**
	 * Add donation submenu on webforce connect menu.
  
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  March 27, 2018
	 */
	public function wfc_donation_submenu(){

		/*
		* enqueue_style
		*/
		wp_enqueue_style( 'wfc-donation-admin-css' );
		wp_enqueue_style( 'wfc-BootstrapCDN-css' );

		wp_enqueue_style( 'wfc-BootstrapCDN-font-awesome-css' );
		wp_enqueue_style( 'wfc-animate-css' );
		wp_enqueue_style( 'webforce-connect-donation-settings-css' );

		/*
		* enqueue_script
		*/
		wp_enqueue_script( 'wfc-BootstrapCDN-js' );
		wp_enqueue_script( 'wfc-BootstrapCDN-min-js' );
		wp_enqueue_script( 'jquery-ui-js' );
		wp_enqueue_script( 'wfc-donation-easyPaginate-js' );
		wp_enqueue_script( 'webforce-connect-donation-settings-js' );
		wp_enqueue_script( 'webforce-connect-donation-address-validation-js' );
		

		$template = plugin_dir_path( dirname( __FILE__ ) ). 'admin/partials/webforce-connect-donation-menu.php';
		$form_body = array(
			'gateway_list' => get_option('pronto_donation_gateway_list'),
		);
		echo $this->class->pdwp_view_tmpl( $template, $form_body );

	}
	
	/**
	 * Registers a wfc_donation_logs post type.
	 *
	 * @author Junjie Canonio
	 */
	public function wfc_don_register_donationlogs() {
		$labels = array(
			'name'               => _x( 'Donation Logs', 'post type general name', 'webforce-connect-donation' ),
			'singular_name'      => _x( 'Donation Log', 'post type singular name', 'webforce-connect-donation' ),
			'menu_name'          => _x( 'Donation Logs', 'admin menu', 'webforce-connect-donation' ),
			'name_admin_bar'     => _x( 'Donation Log', 'add new on admin bar', 'webforce-connect-donation' ),
			'add_new'            => _x( 'Add New', 'donation log', 'webforce-connect-donation' ),
			'add_new_item'       => __( 'Add New Donation Log', 'webforce-connect-donation' ),
			'new_item'           => __( 'New Donation Log', 'webforce-connect-donation' ),
			'edit_item'          => __( 'Donation Log', 'webforce-connect-donation' ),
			'view_item'          => __( 'View Donation Log', 'webforce-connect-donation' ),
			'all_items'          => __( 'All Donation Logs', 'webforce-connect-donation' ),
			'search_items'       => __( 'Search Donation Logs', 'webforce-connect-donation' ),
			'parent_item_colon'  => __( 'Parent Donation Logs:', 'webforce-connect-donation' ),
			'not_found'          => __( 'No Donation Logs found.', 'webforce-connect-donation' ),
			'not_found_in_trash' => __( 'No Donation Logs found in Trash.', 'webforce-connect-donation' )
		);

		$args = array(
			'labels'             => $labels,
	        'description'        => __( 'Description.', 'webforce-connect-donation' ),
			'public'             => true,
			'publicly_queryable' => false,
			'exclude_from_search' => true, 
			'show_ui'            => true,
			'show_in_menu'       => false,
			'show_in_admin_bar'  => false,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'wfc-donation-log' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => 102,
			'supports'           => array( 'title' )
		);

		register_post_type( 'wfc_donation_logs', $args );
	}
	
	/**
	 * Register a meta box for donation logs.
	 *
	 * @author Junjie Canonio
	 */
	public function wfc_don_register_donationlogs_meta_boxes() {
	    add_meta_box( 
	    	'wfc-donation-validateDetails-logs', 
	    	__( 'Donation - validateDetails logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_validateDetails_meta_box'),
	    	'wfc_donation_logs' 
	    );

	    add_meta_box( 
	    	'wfc-donation-resetForm-logs', 
	    	__( 'Donation - resetForm logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_resetForm_meta_box'),
	    	'wfc_donation_logs' 
	    );

	   	add_meta_box( 
	    	'wfc-donation-donationData-logs', 
	    	__( 'Donation - donationData logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_donationData_meta_box'),
	    	'wfc_donation_logs' 
	    );

	    add_meta_box( 
	    	'wfc-donation-getSFAccessToken-logs', 
	    	__( 'Donation - getSFAccessToken logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_getSFAccessToken_meta_box'),
	    	'wfc_donation_logs' 
	    );
	   	
	   	add_meta_box( 
	    	'wfc-donation-completeDonation-logs', 
	    	__( 'Donation - completeDonation logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_completeDonation_meta_box'),
	    	'wfc_donation_logs' 
	    );

	    add_meta_box( 
	    	'wfc-donation-purgeDonation-logs', 
	    	__( 'Donation - purgeDonation logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_purgeDonation_meta_box'),
	    	'wfc_donation_logs' 
	    );

	    add_meta_box( 
	    	'wfc-donation-deleteDonation-logs', 
	    	__( 'Donation - deleteDonation logs', 'webforce-connect-donation' ),
	    	array($this, 'wfc_donationlogs_deleteDonation_meta_box'),
	    	'wfc_donation_logs' 
	    );
	}
	
	/**
	 * Callback function for donation logs meta box.
	 *
	 * @author Junjie Canonio
	 */
	public function wfc_donationlogs_validateDetails_meta_box(){
		global $post;
		$post_content = maybe_unserialize($post->post_content);
		$validateDetails = str_replace( '\\','', json_encode( $post_content['validateDetails'] ) );
		?>
		<script type="text/javascript">
			jQuery(document).ready(function($){
				beautifyJSONTextarea(['validateDetails']);
				beautifyJSONTextarea(['resetForm']);
				beautifyJSONTextarea(['donationData1']);
				beautifyJSONTextarea(['donationData2']);

				beautifyJSONTextarea(['getSFAccessToken']);
				beautifyJSONTextarea(['completeDonation']);
				beautifyJSONTextarea(['purgeDonation']);
				beautifyJSONTextarea(['deleteDonation']);


				function beautifyJSONTextarea( elems ) {
					$.each(elems, function(v, k){
						var obj = $( '#' + k );
						var str = syntaxHighlight( $.parseJSON( obj.val() ) );
						$( '#' + k ).after("<pre>"+str+"</pre>");
						$( '#' + k ).hide();
					});
				}

				function syntaxHighlight(json) {
				    if (typeof json != 'string') {
				         json = JSON.stringify(json, undefined, 2);
				    }
				    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
				    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
				        var cls = 'number';
				        if (/^"/.test(match)) {
				            if (/:$/.test(match)) {
				                cls = 'key';
				            } else {
				                cls = 'string';
				            }
				        } else if (/true|false/.test(match)) {
				            cls = 'boolean';
				        } else if (/null/.test(match)) {
				            cls = 'null';
				        }
				        return '<span class="' + cls + '">' + match + '</span>';
				    });
				}
			});	
		</script>	
		<textarea id="validateDetails" name="validateDetails"><?php echo $validateDetails; ?></textarea>
		<?php
	}

	/**
	 * Callback function for donation logs reset form.
     *
	 * @author Junjie Canonio
	 */
	public function wfc_donationlogs_resetForm_meta_box(){
		global $post;
		$resetForm = get_post_meta( $post->ID, 'resetForm', true );
		$resetForm = maybe_unserialize($resetForm);
		$resetForm = str_replace( '\\','', json_encode( $resetForm ) );
		?>
		<textarea id="resetForm" name="resetForm"><?php echo $resetForm; ?></textarea>
		<?php
	}

	/**
	* Callback function for donation logs Donation Data.
	* @author Junjie Canonio
	*/
	public function wfc_donationlogs_donationData_meta_box(){
		global $post;
		$donationData1 = get_post_meta( $post->ID, 'donationData1', true );
		$donationData1 = maybe_unserialize($donationData1);
		$donationData1 = str_replace( '\\','', json_encode( $donationData1 ) );
		$donationData2 = get_post_meta( $post->ID, 'donationData2', true );
		$donationData2 = maybe_unserialize($donationData2);
		$donationData2 = str_replace( '\\','', json_encode( $donationData2 ) );
		?>
		<textarea id="donationData1" name="donationData1"><?php echo $donationData1; ?></textarea>
		<textarea id="donationData2" name="donationData2"><?php echo $donationData2; ?></textarea>
		<?php
	}

	/**
	* Callback function for donation logs for fetching SF Access Token.
	* @author Junjie Canonio
	*
	*/
	public function wfc_donationlogs_getSFAccessToken_meta_box(){
		global $post;
		$getSFAccessToken = get_post_meta( $post->ID, 'getSFAccessToken', true );
		$getSFAccessToken = maybe_unserialize($getSFAccessToken);
		$getSFAccessToken = str_replace( '\\','', json_encode( $getSFAccessToken ) );
		?>
		<textarea id="getSFAccessToken" name="getSFAccessToken"><?php echo $getSFAccessToken; ?></textarea>
		<?php
	}

	/**
	* Callback function for donation logs Complete Donation.
	* @author Junjie Canonio
	*/
	public function wfc_donationlogs_completeDonation_meta_box(){
		global $post;
		$completeDonation = get_post_meta( $post->ID, 'completeDonation', true );
		$completeDonation = maybe_unserialize($completeDonation);
		$completeDonation = str_replace( '\\','', json_encode( $completeDonation ) );
		?>
		<textarea id="completeDonation" name="completeDonation"><?php echo $completeDonation; ?></textarea>
		<?php
	}	

	/**
	* Callback function for donation logs purge Donation data.
	* @author Junjie Canonio
	*/
	public function wfc_donationlogs_purgeDonation_meta_box(){
		global $post;
		$purgeDonation = get_post_meta( $post->ID, 'purgeDonation', true );
		$purgeDonation = maybe_unserialize($purgeDonation);
		$purgeDonation = str_replace( '\\','', json_encode( $purgeDonation ) );
		?>
		<textarea id="purgeDonation" name="purgeDonation"><?php echo $purgeDonation; ?></textarea>
		<?php
	}	

	/**
	* Callback function for donation logs delete donation data.
	* @author Junjie Canonio
	*/
	public function wfc_donationlogs_deleteDonation_meta_box(){
		global $post;
		$deleteDonation = get_post_meta( $post->ID, 'deleteDonation', true );
		$deleteDonation = maybe_unserialize($deleteDonation);
		$deleteDonation = str_replace( '\\','', json_encode( $deleteDonation ) );
		?>
		<textarea id="deleteDonation" name="deleteDonation"><?php echo $deleteDonation; ?></textarea>
		<?php
	}
	
	/**
	 * Registers the _forms for the admin area.
	 *
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  March 28, 2018
	 */
	public function _get_gateway(){

		$data = array();
		if( isset( $_GET['gatewayID'] ) && !empty( $_GET['gatewayID'] ) ) {
			$gateId = $_GET['gatewayID'];
			$gateway_list = get_option( 'pronto_donation_gateway_list', 0 );
			if( $gateway_list != 0 && sizeof( $gateway_list ) > 0 ) {
				foreach ( $gateway_list as $gates ) {
					if( isset( $gates->Id ) && $gates->Id == $gateId ) {
						$gateType = ( isset( $gates->RecordType->DeveloperName ) ? 
									( strtolower( $gates->RecordType->DeveloperName ) == 'ech' ) ? 
									'paydock' : strtolower( $gates->RecordType->DeveloperName ) : '' );

						$tmpl = array(
							'gateType' => $gateType,
							'class' => $this->class,
							'settings' => '',
							'gateway' => $gates

						);

						echo $this->_pdwpgetModuleTemplate( $tmpl );
					}
				}
			}
		} else {
			$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );
			$gateway_list = $oauth->api_request( 'apexrest/ASSFAPI/gateway' , '', 'get' );	

			if( $gateway_list['status_code'] == 200 && $gateway_list['sf_response']['status'] == 'Success'){
				$gateway_result = 
				isset( $gateway_list['sf_response']['result'] ) ? $gateway_list['sf_response']['result'] : array();

				if(!empty($gateway_result)){
					update_option( 'pronto_donation_gateway_list' , json_decode(json_encode($gateway_result)));			
				}				
			}
		}
	}
	
	/**
	 * Ajax callback for syncing payment gateway from Salesforce to web.
	 *
	 *
	 * @author Junjie Canonio
	 *
     * @LastUpdated  September 11, 2019
	 */
	public function wfc_don_syncpaymntgate_callback() {
		if(isset($_POST['action']) && $_POST['action'] == 'wfc_don_syncpaymntgate_action') {
			$response_arr = array();

			if (isset($_POST['sfinstanceid']) && !empty($_POST['sfinstanceid'])){

                $oauth = new Pronto_Ultimate_SF_Oauth(plugin_basename(__FILE__), $_POST['sfinstanceid']);
                $gateway_list = $oauth->api_request('apexrest/ASSFAPI/gateway', '', 'get');

                if ($gateway_list['status_code'] == 200 && $gateway_list['sf_response']['status'] == 'Success') {
                    $gateway_result =
                        isset($gateway_list['sf_response']['result']) ? $gateway_list['sf_response']['result'] : array();

                    if (!empty($gateway_result)) {
                        update_option('WFCDON_gateway_list_'.$_POST['sfinstanceid'], json_decode(json_encode($gateway_result)));
                    }
                    $response_arr['status'] = 'Success';
                    $response_arr['response'] = $gateway_list;
                } else {
                    $response_arr['status'] = 'Failed';
                    $response_arr['response'] = $gateway_list;
                }

            }else{
				$response_arr['status'] = 'Failed';
				$response_arr['response'] = '';
				$response_arr['msg'] = 'Please provide Salesforce Instance ID.';
            }

			$response_arr['POST'] = $_POST;
			echo json_encode($response_arr);
			wp_die();
		}
	}

	/**
	 * Ajax callback for fetching salesforce instance.
	 *
	 * @author Junjie Canonio
	 * @return void
	 *
	 * @LastUpdated  September 9, 2019
	 */
	public function wfc_don_getsfinstance_callback() {
	    if(isset($_POST['action']) && $_POST['action'] == 'wfc_don_getsfinstance_action'){
		    $response_arr = array();


		    $WFCBASE_MultiInstanceSettings = get_option('WFCBASE_MultiInstanceSettings');

		    if (isset($WFCBASE_MultiInstanceSettings['instances']) && !empty($WFCBASE_MultiInstanceSettings['instances'])){
			    $current_instances = $WFCBASE_MultiInstanceSettings['instances'];
			    $instances_arr = array();

			    if(isset($WFCBASE_MultiInstanceSettings['default_instance']) && !empty($WFCBASE_MultiInstanceSettings['default_instance'])){
				    $default_instance = $WFCBASE_MultiInstanceSettings['default_instance'];
				    array_unshift($current_instances, $default_instance);
				    $current_instances = array_unique($current_instances);
			    }

			    foreach ($current_instances as $key => $value){
				    $instances_option = get_option($value);
				    $salesforceinstance_slug = isset($instances_option['wfc_base_salesforceinstance_slug']) ? $instances_option['wfc_base_salesforceinstance_slug'] : '';
				    $instances_option['instances_url'] = admin_url( 'admin.php?page=wfc-donation&sfinstanceID='.$salesforceinstance_slug );
				    $instances_option['instances_settings_url'] = admin_url( 'admin.php?page=wfc-base-multi-instance-settings&salesforceinstance='.$salesforceinstance_slug );
				    $instances_option['instances_connection_status'] = WFC_Base_MultiSFInstanceOauth::test_connection($salesforceinstance_slug);
				    array_push($instances_arr,$instances_option);
                }

			    $response_arr['current_instances'] = $current_instances;
			    $response_arr['instances'] = $instances_arr;
			    $response_arr['status'] = 'success';
			    $response_arr['msg'] = 'Salesforce Instance Successfully fetched.';

            }else{
			    $response_arr['status'] = 'empty';
			    $response_arr['msg'] = 'There are no Salesforce Instances available.';
            }


		    echo json_encode($response_arr);
		    wp_die();
        }
	}
	
	/**
	 * Ajax callback for fetching payment gateway.
	 *
	 * @author Junjie Canonio
	 * @return void
     *
     * @LastUpdated  September 11, 2019
	 */
	public function wfc_don_getpaymntgate_callback() {
		if(isset($_POST['action']) && $_POST['action'] == 'wfc_don_getpaymntgate_action') {
			$response_arr = array();
			$gateway_arr = array();

			if (isset($_POST['sfinstanceid']) && !empty($_POST['sfinstanceid'])){

                $pronto_donation_gateway_list = get_option('WFCDON_gateway_list_'.$_POST['sfinstanceid']);

                if (is_ssl()) {
                    $wfc_donation_homeurl = home_url('/', 'https');
                } else {
                    $wfc_donation_homeurl = home_url('/', 'http');
                }

                if (is_array($pronto_donation_gateway_list) && !empty($pronto_donation_gateway_list)) {
                    foreach ($pronto_donation_gateway_list as $key => $value) {

                        $sf_id =
                            isset($value->Id) ? $value->Id : '';

                        $sf_name =
                            isset($value->Name) ? $value->Name : '';

                        $isSandbox =
                            isset($value->ASPHPP__isSandbox__c) ? $value->ASPHPP__isSandbox__c : 1;

                        $tokenization =
                            isset($value->ASPHPP__Tokenization_Payment__c) ? $value->ASPHPP__Tokenization_Payment__c : 1;


                        $developer_name =
                            isset($value->RecordType->DeveloperName) ? $value->RecordType->DeveloperName : 1;
                        $developer_name = str_replace('_', '', $developer_name);
                        $developer_name = strtolower($developer_name);

                        if ($tokenization == 1) {
                            $tokenization_image = 'fa fa-check-square';
                        } else {
                            $tokenization_image = 'fa fa-square-o';
                        }

                        if ($isSandbox == 1) {
                            $sandbox_mode_state = 'list-group-item-warning';
                            $sandbox_mode_image = 'fa fa-thumbs-o-down';
                        } else {
                            $sandbox_mode_state = 'list-group-item-success';
                            $sandbox_mode_image = 'fa fa-thumbs-o-up';
                        }

                        $gateway_logo = '<img src="' . plugin_dir_url(__FILE__) . '../images/' . strtolower($developer_name) . '.png" style="width:50px;"/>';

                        $gateway_arr[] = array(
                            'sf_id' => $sf_id,
                            'sf_name' => $sf_name,
                            'sandbox_mode_state' => $sandbox_mode_state,
                            'sandbox_mode_image' => $sandbox_mode_image,
                            'tokenization_image' => $tokenization_image,
                            'gateway_logo' => $gateway_logo,
                            'payment_gateway' => $developer_name,
                            'settings_url' => admin_url('admin.php?page=wfc-donation&gatewaysfinstanceid='.$_POST['sfinstanceid'].'&gatewayID=' . $sf_id . '&gateway=' . $developer_name),
                        );
                    }

                    $response_arr['status'] = 'Success';
                    $response_arr['count'] = count($gateway_arr);
                    $response_arr['gateway_arr'] = $gateway_arr;
                    $response_arr['pronto_donation_gateway_list'] = $pronto_donation_gateway_list;

                } else {
                    $response_arr['status'] = 'Empty';
                    $response_arr['count'] = 0;
                    $response_arr['gateway_arr'] = array();
                }
			}else{
				$response_arr['status'] = 'Failed';
				$response_arr['count'] = 0;
				$response_arr['gateway_arr'] = array();
				$response_arr['msg'] = 'Please provide Salesforce Instance ID.';
			}

            $response_arr['POST'] = $_POST;
			echo json_encode($response_arr);
			wp_die();
		}
	}
	
	/**
	 * Ajax callback for fetching GAU and Campaign form Salesforce.
	 *
	 * @author Junjie Canonio
     * @LastUpdated  May 15, 2018
	 */
	public function wfc_don_fetchgaucamp_callback() {
		if(isset($_POST['action']) && $_POST['action'] == 'wfc_don_fetchgaucamp_action') {
			if (isset($_POST['sfinstanceid']) && !empty($_POST['sfinstanceid'])) {

				$result_fetchgau = WFC_SalesforcePPAPI::wfc_donation_fetchgau($_POST['sfinstanceid']);
				$result_fetchcamp = WFC_SalesforcePPAPI::wfc_donation_fetchcamp($_POST['sfinstanceid']);

				if ((isset($result_fetchgau['status']) && $result_fetchgau['status'] == 'Success') &&
					isset($result_fetchcamp['status']) && $result_fetchcamp['status'] == 'Success') {
					$response_arr['status'] = 'Success';
					$response_arr['message'] = array(
						'message_gau' => isset($result_fetchgau['message']) ? $result_fetchgau['message'] : '',
						'message_camp' => isset($result_fetchcamp['message']) ? $result_fetchcamp['message'] : '',
					);
				} elseif ((isset($result_fetchgau['status']) && $result_fetchgau['status'] == 'Success') &&
					isset($result_fetchcamp['status']) && $result_fetchcamp['status'] != 'Success') {
					$response_arr['status'] = 'Warning-camp';
					$response_arr['message'] = array(
						'message_gau' => isset($result_fetchgau['message']) ? $result_fetchgau['message'] : '',
						'message_camp' => isset($result_fetchcamp['message']) ? $result_fetchcamp['message'] : '',
					);
				} elseif ((isset($result_fetchgau['status']) && $result_fetchgau['status'] != 'Success') &&
					isset($result_fetchcamp['status']) && $result_fetchcamp['status'] == 'Success') {
					$response_arr['status'] = 'Warning-gau';
					$response_arr['message'] = array(
						'message_gau' => isset($result_fetchgau['message']) ? $result_fetchgau['message'] : '',
						'message_camp' => isset($result_fetchcamp['message']) ? $result_fetchcamp['message'] : '',
					);
				} elseif ((isset($result_fetchgau['status']) && $result_fetchgau['status'] != 'Success') &&
					isset($result_fetchcamp['status']) && $result_fetchcamp['status'] != 'Success') {
					$response_arr['status'] = 'Failed';
					$response_arr['message'] = array(
						'message_gau' => isset($result_fetchgau['message']) ? $result_fetchgau['message'] : '',
						'message_camp' => isset($result_fetchcamp['message']) ? $result_fetchcamp['message'] : '',
					);
				} else {
					$response_arr['status'] = 'Failed';
					$response_arr['message'] = array(
						'message_gau' => 'Error',
						'message_camp' => 'Error',
					);
				}
				echo json_encode($response_arr);
				wp_die();
			}
		}
	}
	
	/**
	 * Cron sync action function for GAU and Campaign.
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  May 28, 2018
	 */
	public function wfc_don_cronresyncgaucamp() {	
		$result_fetchgau = WFC_SalesforcePPAPI::wfc_donation_fetchgau();
		$result_fetchcamp = WFC_SalesforcePPAPI::wfc_donation_fetchcamp();		
	}
	
	/**
	 * Ajax callback for saving donation settings.
	 *
	 * @author Junjie Canonio
	 * @LastUpdated  May 15, 2018
	 */
	public function wfc_don_savesettings_callback() {
		if(isset($_POST['action']) && $_POST['action'] =='wfc_don_savesettings_action'){

			$data_arr = isset($_POST['data']) ? $_POST['data'] : array();

			$optiondon_arr = array();
			if(is_array($data_arr)){
				foreach ($data_arr as $key => $value) {
					$optiondon_arr[$value['name']] = $value['value'];
				}
			}

			$donation_debug_mode = isset($optiondon_arr['donation_debug_mode']) ? $optiondon_arr['donation_debug_mode'] : 'false';
			$cron_schedule = isset($optiondon_arr['cron_schedule']) ? $optiondon_arr['cron_schedule'] : 'hourly';
			$donation_dnsr_single = isset($optiondon_arr['donation_dnsr_single']) ? $optiondon_arr['donation_dnsr_single'] : 'true';
			$donation_dnsr_recurring = isset($optiondon_arr['donation_dnsr_recurring']) ? $optiondon_arr['donation_dnsr_recurring'] : 'true';
			
			$purge_donation = isset($optiondon_arr['purge_donation']) ? $optiondon_arr['purge_donation'] : 'false';

			$purge_donation_datefrom = 
			isset($optiondon_arr['purge_donation_datefrom']) ? 
			$optiondon_arr['purge_donation_datefrom'] : '';


			$purge_donation_datefrom_timestamp = 
			isset($optiondon_arr['purge_donation_datefrom-timestamp']) ? 
			$optiondon_arr['purge_donation_datefrom-timestamp'] : '';


			$purge_donation_dateto = 
			isset($optiondon_arr['purge_donation_dateto']) ? 
			$optiondon_arr['purge_donation_dateto'] : '';


			$purge_donation_dateto_timestamp = 
			isset($optiondon_arr['purge_donation_dateto-timestamp']) ? 
			$optiondon_arr['purge_donation_dateto-timestamp'] : '';
			
			
			$frequencies_def = '[{"item-0":{"values":{"frequency-name":"One-off","frequency-switch":"true","frequency-value":"one-off"},"non_deletable":"true"}},{"item-1":{"values":{"frequency-name":"Daily","frequency-switch":"true","frequency-value":"daily"},"non_deletable":"true"}},{"item-2":{"values":{"frequency-name":"Weekly","frequency-switch":"true","frequency-value":"weekly"},"non_deletable":"true"}},{"item-3":{"values":{"frequency-name":"Four-weekly","frequency-switch":"true","frequency-value":"fourweekly"},"non_deletable":"true"}},{"item-4":{"values":{"frequency-name":"Fortnightly","frequency-switch":"true","frequency-value":"fortnightly"},"non_deletable":"true"}},{"item-5":{"values":{"frequency-name":"Monthly","frequency-switch":"true","frequency-value":"monthly"},"non_deletable":"true"}},{"item-6":{"values":{"frequency-name":"Quarterly","frequency-switch":"true","frequency-value":"quarterly"},"non_deletable":"true"}},{"item-7":{"values":{"frequency-name":"Yearly","frequency-switch":"true","frequency-value":"yearly"},"non_deletable":"true"}}]';
			$frequencies = isset($_POST['frequencies']) ? $_POST['frequencies'] : $frequencies_def;

			$enable_validation = isset($optiondon_arr['enable_validation']) ? $optiondon_arr['enable_validation'] : 'false';
			$google_api_key = isset($optiondon_arr['google_api_key']) ? $optiondon_arr['google_api_key'] : '';
			$google_prioritized_country = isset($optiondon_arr['google_prioritized_country']) ? $optiondon_arr['google_prioritized_country'] : '';
			$register_custom_set_address = isset($optiondon_arr['register_custom_set_address']) ? $optiondon_arr['register_custom_set_address'] : '[]';
			$harmony_rapid_displaymode = isset($optiondon_arr['harmony_rapid_displaymode']) ? $optiondon_arr['harmony_rapid_displaymode'] : 'fieldmode';
			$harmony_overridablefields = isset($optiondon_arr['harmony_overridablefields']) ? $optiondon_arr['harmony_overridablefields'] : 'false';
			$harmony_rapid_sourceoftruth = isset($optiondon_arr['harmony_rapid_sourceoftruth']) ? $optiondon_arr['harmony_rapid_sourceoftruth'] : 'AUSOTS';
			$harmony_rapid_username = isset($optiondon_arr['harmony_rapid_username']) ? $optiondon_arr['harmony_rapid_username'] : '';
			$harmony_rapid_password = isset($optiondon_arr['harmony_rapid_password']) ? $optiondon_arr['harmony_rapid_password'] : '';

			$enable_recaptcha = isset($optiondon_arr['enable_recaptcha']) ? $optiondon_arr['enable_recaptcha'] : 'false';
			$google_site_key = isset($optiondon_arr['google_site_key']) ? $optiondon_arr['google_site_key'] : '';
			$google_secret_key = isset($optiondon_arr['google_secret_key']) ? $optiondon_arr['google_secret_key'] : '';

			$social_facebook = isset($optiondon_arr['social_facebook']) ? $optiondon_arr['social_facebook'] : 'false';
			$social_twitter = isset($optiondon_arr['social_twitter']) ? $optiondon_arr['social_twitter'] : 'false';
			$social_google_plus = isset($optiondon_arr['social_google_plus']) ? $optiondon_arr['social_google_plus'] : 'false';
			$social_linkedin = isset($optiondon_arr['social_linkedin']) ? $optiondon_arr['social_linkedin'] : 'false';

			$loading_enable = isset($optiondon_arr['loading_enable']) ? $optiondon_arr['loading_enable'] : 'true';
			$loading_spinner = isset($optiondon_arr['loading_spinner']) ? $optiondon_arr['loading_spinner'] : 'tail';
			$loading_tinter_color = isset($optiondon_arr['loading_tinter_color']) ? $optiondon_arr['loading_tinter_color'] : '#000000';

			$pd_settings = get_option('pronto_donation_settings', 0);
			$pd_thank_you_page_id = isset($pd_settings->pd_thank_you_page_id) ? $pd_settings->pd_thank_you_page_id : '';

			$pd_homeurl = home_url( '/', 'http' );
			if(is_ssl()){ $pd_homeurl = home_url( '/', 'https' ); }

			$wfc_don_settings_args = array(
	            'donation_debug_mode' 	 => $donation_debug_mode,
	            'donation_dnsr_single' 	  => $donation_dnsr_single,
	            'donation_dnsr_recurring' => $donation_dnsr_recurring,
	            'cron_schedule' 		 => $cron_schedule,

	            'purge_donation'		 => $purge_donation,
	            'purge_donation_datefrom' => $purge_donation_datefrom,
	            'purge_donation_datefrom_timestamp' => $purge_donation_datefrom_timestamp,
	            'purge_donation_dateto' => $purge_donation_dateto,
	            'purge_donation_dateto_timestamp' => $purge_donation_dateto_timestamp,

	            'frequencies' 			 => stripslashes($frequencies),

	            'enable_validation' 	 => $enable_validation,
	            'google_api_key' 		 => $google_api_key,
				'google_prioritized_country' 		 => $google_prioritized_country,
				'register_custom_set_address'  => stripslashes($register_custom_set_address),

	            'harmony_rapid_displaymode'   => $harmony_rapid_displaymode,
	            'harmony_overridablefields'   => $harmony_overridablefields,
	            'harmony_rapid_sourceoftruth' => $harmony_rapid_sourceoftruth,
	            'harmony_rapid_username' 	  => $harmony_rapid_username,
	            'harmony_rapid_password' 	  => $harmony_rapid_password,

	            'enable_recaptcha' 		 => $enable_recaptcha,
	            'google_site_key' 		 => $google_site_key,
	            'google_secret_key' 	 => $google_secret_key,

	            'social_facebook' 		 => $social_facebook,
	            'social_twitter' 		 => $social_twitter,
	            'social_google_plus' 	 => $social_google_plus,
	            'social_linkedin' 		 => $social_linkedin,

	            'loading_enable'		 => $loading_enable,	
	            'loading_spinner' 		 => $loading_spinner,
				'loading_tinter_color' 	 => $loading_tinter_color,

	            'pd_thank_you_page_id'   => $pd_thank_you_page_id,
	            'pd_homeurl'			 => $pd_homeurl,
			);
			update_option('pronto_donation_settings' , (object)$wfc_don_settings_args);

			/** 
			* activate cron sync donation
			*/    	
			wp_clear_scheduled_hook( 'wfc_don_cron_resyncdonation' );
			wp_schedule_event( time(), trim($cron_schedule), 'wfc_don_cron_resyncdonation' );
			//wp_schedule_event( time(), 'minutes_10', 'wfc_don_cron_resyncdonation' );
			wp_clear_scheduled_hook( 'wfc_don_cron_resyncgaucamp' );
			wp_schedule_event( time(), 'hourly', 'wfc_don_cron_resyncgaucamp' );
			
			$response_arr['status'] = 'Success' ;
			$response_arr['data'] = $wfc_don_settings_args;
			echo json_encode($response_arr);
	    	wp_die();
		}
	}

	// public function wfc_don_10min_cron($interval) {
	//     $interval['minutes_10'] = array('interval' => 10*60, 'display' => 'Once 10 minutes');
	//     return $interval;
	// }


	/**
	 * Call back for Reset Settings
	 *
	 * @author Von Sienard Vibar <von@alphasys.com.au>
     * @since 1.0.0
	 */
	public function wfc_don_resetsettings_callback() {
	
		// Data to be saved
		$donation_debug_mode = 'false';
		$cron_schedule = 'hourly';
		$donation_dnsr_single = 'true';
		$donation_dnsr_recurring = 'true';

		$purge_donation = 'false';
		$purge_donation_datefrom = '';
		$purge_donation_datefrom_timestamp 	= '';
		$purge_donation_dateto 	= '';
		$purge_donation_dateto_timestamp = '';


		$frequencies_def 		 = '[{"item-0":{"values":{"frequency-name":"One-off","frequency-switch":"true","frequency-value":"one-off"},"non_deletable":"true"}},{"item-1":{"values":{"frequency-name":"Daily","frequency-switch":"true","frequency-value":"daily"},"non_deletable":"true"}},{"item-2":{"values":{"frequency-name":"Weekly","frequency-switch":"true","frequency-value":"weekly"},"non_deletable":"true"}},{"item-3":{"values":{"frequency-name":"Four-weekly","frequency-switch":"true","frequency-value":"fourweekly"},"non_deletable":"true"}},{"item-4":{"values":{"frequency-name":"Fortnightly","frequency-switch":"true","frequency-value":"fortnightly"},"non_deletable":"true"}},{"item-5":{"values":{"frequency-name":"Monthly","frequency-switch":"true","frequency-value":"monthly"},"non_deletable":"true"}},{"item-6":{"values":{"frequency-name":"Quarterly","frequency-switch":"true","frequency-value":"quarterly"},"non_deletable":"true"}},{"item-7":{"values":{"frequency-name":"Yearly","frequency-switch":"true","frequency-value":"yearly"},"non_deletable":"true"}}]';
		$frequencies 			 = $frequencies_def ;

		$enable_validation 		 = 'false';
		$google_api_key 		 = '';
		$harmony_rapid_displaymode  = 'fieldmode';
		$harmony_overridablefields = 'false';
		$harmony_rapid_sourceoftruth  = 'AUSOTS';
		$harmony_rapid_username 	  = '';
		$harmony_rapid_password 	  = '';

		$enable_recaptcha 		 = 'false';
		$google_site_key 		 = '';
		$google_secret_key 		 = '';

		$social_facebook 		 = 'false';
		$social_twitter 		 = 'false';
		$social_google_plus 	 = 'false';
		$social_linkedin 		 = 'false';

		$loading_enable 		 = 'true';
		$loading_spinner 		 = 'tail';
		$loading_tinter_color 	 = '#000000';

		$pd_settings = get_option('pronto_donation_settings', 0);
		$pd_thank_you_page_id 	 = isset($pd_settings->pd_thank_you_page_id) ? $pd_settings->pd_thank_you_page_id : '';

		$pd_homeurl = home_url( '/', 'http' );
		if(is_ssl()){ $pd_homeurl = home_url( '/', 'https' ); }

		$pd_settings_args = array(
			'donation_debug_mode' 	 => $donation_debug_mode,
			'cron_schedule' 		 => $cron_schedule,
			'donation_dnsr_single' 	  => $donation_dnsr_single,
			'donation_dnsr_recurring' => $donation_dnsr_recurring,

			'purge_donation' => $purge_donation,
			'purge_donation_datefrom' => $purge_donation_datefrom,
	        'purge_donation_datefrom_timestamp' => $purge_donation_datefrom_timestamp,
	        'purge_donation_dateto' => $purge_donation_dateto,
	        'purge_donation_dateto_timestamp' => $purge_donation_dateto_timestamp,

			'frequencies' 			 => $frequencies,

			'enable_validation' 	 => $enable_validation,
			'google_api_key' 		 => $google_api_key,
			'harmony_rapid_displaymode'   => $harmony_rapid_displaymode,
			'harmony_overridablefields' => $harmony_overridablefields,
			'harmony_rapid_sourceoftruth' => $harmony_rapid_sourceoftruth,
			'harmony_rapid_username' 	  => $harmony_rapid_username,
			'harmony_rapid_password' 	  => $harmony_rapid_password,

			'enable_recaptcha' 		 => $enable_recaptcha,
			'google_site_key' 		 => $google_site_key,
			'google_secret_key' 	 => $google_secret_key,

			'social_facebook' 		 => $social_facebook,
			'social_twitter' 		 => $social_twitter,
			'social_google_plus' 	 => $social_google_plus,
			'social_linkedin' 		 => $social_linkedin,

			'loading_enable' 		 => $loading_enable,
			'loading_spinner' 		 => $loading_spinner,
			'loading_tinter_color' 	 => $loading_tinter_color,

			'pd_thank_you_page_id'   => $pd_thank_you_page_id,
			'pd_homeurl'			 => $pd_homeurl,

		);

		if (update_option('pronto_donation_settings' , (object)$pd_settings_args))
			$response_arr['status'] = 'Success';
		else
			$response_arr['status'] = 'Failed';

		echo json_encode($response_arr);

		wp_die();
	}

	/**
	 * Resync failed donation.
     *
	 * @LastUpdated May 23, 2018
	 * @return void
	 * @author Junjie Canonio
	 */
	public function wfc_don_resyncdonation_callback() {
		if(isset($_POST['action']) && $_POST['action'] =='wfc_don_resyncdonation_action'){
			$donation_id = isset($_POST['donation_id']) ? $_POST['donation_id'] : '';
			$RecurringId = isset($_POST['RecurringId']) ? $_POST['RecurringId'] : '';
			$sync_status = isset($_POST['sync_status']) ? $_POST['sync_status'] : '';
			$pdwp_donation_type = isset($_POST['pdwp_donation_type']) ? $_POST['pdwp_donation_type'] : '';

			$WFC_DonationForms = new WFC_DonationForms();
			$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $donation_id );
			$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
			$donation = maybe_unserialize( $donations );
			$donation = stripslashes_deep( $donation );

			$WFC_DonationDataHandler = new WFC_DonationDataHandler();

			if($sync_status == 'recurring_failed_sync' && $pdwp_donation_type != 'one-off') {
				$result = $WFC_DonationDataHandler->wfc_sync_recurringdonation_tosf($donation,$this->class,'');
				if((isset($result['SalesforceResponse']['RecurringId']) && !empty($result['SalesforceResponse']['RecurringId'])) &&
        			(isset($result['SalesforceResponse']['status']) && $result['SalesforceResponse']['status'] == 200 )){

					$RecurringId = isset($result['SalesforceResponse']['RecurringId']) ? $result['SalesforceResponse']['RecurringId'] : '';
					$donation['post_meta_id'] = $donation_id;
					$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($donation,$this->class,$RecurringId);
				}
			}elseif($sync_status == 'not_sync' && $pdwp_donation_type != 'one-off'){
				$result = $WFC_DonationDataHandler->wfc_sync_recurringdonation_tosf($donation,$this->class,'');
				if((isset($result['SalesforceResponse']['RecurringId']) && !empty($result['SalesforceResponse']['RecurringId'])) &&
        			(isset($result['SalesforceResponse']['status']) && $result['SalesforceResponse']['status'] == 200 )){

					$RecurringId = isset($result['SalesforceResponse']['RecurringId']) ? $result['SalesforceResponse']['RecurringId'] : '';
					$donation['post_meta_id'] = $donation_id;
					$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($donation,$this->class,$RecurringId);
				}
			}elseif($sync_status == 'partial_sync' && !empty($RecurringId)){
				$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($donation,$this->class,$RecurringId);
			}elseif(($sync_status == 'failed_sync' || $sync_status == 'not_sync') && $pdwp_donation_type == 'one-off'){
				$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($donation,$this->class);
			}elseif($sync_status == 'attemp_reach' && $pdwp_donation_type == 'one-off'){
				$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($donation,$this->class);
			}else{

			}
			
			if(!empty($result)) {
				if(isset($result['SalesforceLogs']['result']['single']['status']) &&
					$result['SalesforceLogs']['result']['single']['status'] == 'Success') {
					$response_arr['status'] = 'Success';
					$response_arr['data'] = $result;
				}elseif(isset($result['SalesforceLogs']['result']['recurring']['status']) &&
					$result['SalesforceLogs']['result']['recurring']['status'] == 'Success'){
					$response_arr['status'] = 'Success';
					$response_arr['data'] = $result;
				}else{
					$response_arr['status'] = 'Warning';
					$response_arr['data'] = $result;
				}
			}else{
				$response_arr['status'] = 'Error';
				$response_arr['data'] = isset($result) ? $result : array();
			}

			echo json_encode($response_arr);
    		wp_die();
		}
	}


	/**
	 * Set payment transaction and update donation record.
	 *
	 * @author Junjie Canonio
	 * @LastUpdated  July 7, 2019
	 */
	public function wfc_don_setpaymenttransaction_callback() {
		if(isset($_POST['action']) && $_POST['action'] =='wfc_don_setpaymenttransaction_action'){

		    if(!isset($_POST['donation_id']) || empty($_POST['donation_id']) && $_POST['donation_id'] == null &&
			    !isset($_POST['txnstatus']) || empty($_POST['txnstatus']) && $_POST['txnstatus'] == null &&
			    !isset($_POST['txnid']) || empty($_POST['txnid']) && $_POST['txnid'] == null &&
			    !isset($_POST['txndate']) || empty($_POST['txndate']) && $_POST['txndate'] == null &&
			    !isset($_POST['txnpaymentref']) || empty($_POST['txnpaymentref']) && $_POST['txnpaymentref'] == null
            ){

			    $response_arr['status'] = 'Error';
			    $response_arr['msg'] = 'Required fields are not provide properly.';
			    $response_arr['POST'] = $_POST;

            }else{
			    $WFC_DonationForms = new WFC_DonationForms();

			    $donations = $WFC_DonationForms->wfc_get_donation_by_meta_id($_POST['donation_id']);
			    $donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
			    $donation = maybe_unserialize( $donations );
			    $donation = stripslashes_deep( $donation );

			    if ($_POST['txnstatus'] == 'Success'){
				    $donation[statusCode] = '1';
				    $donation[statusText] = 'APPROVED';

			    }elseif ($_POST['txnstatus'] == 'Failed'){
				    $donation[statusCode] = '0';
				    $donation[statusText] = 'FAILED';
			    }

			    $donation[PaymentTransaction]['TxnStatus'] = isset($_POST['txnstatus']) ? $_POST['txnstatus'] : '';
			    $donation[PaymentTransaction]['TxnId'] = isset($_POST['txnid']) ? $_POST['txnid'] : '';
			    $donation[PaymentTransaction]['TxnDate'] = isset($_POST['txndate']) ? $_POST['txndate'] : '';
			    $donation[PaymentTransaction]['TxnMessage'] = isset($_POST['txnpaymentref']) ? $_POST['txnstatus'].'-'.$_POST['txnpaymentref'] : '';
			    $donation[PaymentTransaction]['TxnPaymentRef'] = isset($_POST['txnpaymentref']) ? $_POST['txnpaymentref'] : '';

			    $donation['paymenttransactionsetmanually'] = 'true';

			    $post = stripslashes_deep($donation);
			    $this->class->wfc_donation_data(
				    array(
					    serialize( $post ),
					    $_POST['donation_id'],
				    ),
				    'update'
			    );

			    $response_arr['status'] = 'Success';
			    $response_arr['msg'] = 'This donation record has been successfully updated.';
			    $response_arr['donation'] = $post;
            }
		    
			echo json_encode($response_arr);
			wp_die();
		}
	}
	
	/**
	 * Link failed donation callback function.
	 *
	 * @author Junjie Canonio
     * @LastUpdated  June 13, 2018
	 */
	public function wfc_don_linkdonation_callback() {	
		if(isset($_POST['action']) && $_POST['action'] == 'wfc_don_linkdonation_action'){
			$WFC_DonationList = new WFC_DonationList();

			$response_arr['result_code'] = 'step_error';
			$response_arr['result_message'] = 'An error occurred upon linking the donation to a Salesforce record.';

			/**
			* Step one check the OpportunityId if existing
			*/
			if(isset($_POST['step']) &&  $_POST['step'] == '1') {
				if(isset($_POST['OpportunityId']) && !empty($_POST['OpportunityId'])) {
					$oauth = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );
					$result = $oauth->api_request( 'data/v39.0/sobjects/Opportunity/'.$_POST['OpportunityId'], '', 'get' );

					$result_code = 'not_existing';
					$result_message = 'ID does not exist.';
					if(is_array($result)) {
						if((isset($result['status_code']) && $result['status_code'] == 200) && 
							(isset($result['sf_response']['Id']) && !empty($result['sf_response']['Id']))) {
							$search = isset($_POST['OpportunityId']) ? $_POST['OpportunityId'] : '';	
							if(!empty($search)) {
								$check_opportunity = $WFC_DonationList->wfc_get_donations( 
									array(
										array(
											'key' 		=> 'meta_value',
											'operator' 	=> 'LIKE',
											'value' 	=> "'%{$search}%'",
										),
									)
								);	
								if(!empty($check_opportunity)) {
									$result_code = 'id_taken';
									$result_message = 'Opportunity Id is already linked to an existing donation. Please provide an Opportunity Id not linked to another donation.';
								}else{
									$result_code = 'id_good';
									$result_message = 'Opportunity Id is good to use.';
								}
								
							}else{
								$result_code = 'no_opportunity_prov';
								$result_message = 'Please provide Opportunity Id.';
							}
						}else{
							if(isset($result['sf_error']) && !empty($result['sf_error']) && isset($result['sf_error'][0]['errorCode'])) {
								$result_errorCode = isset($result['sf_error'][0]['errorCode']) ? $result['sf_error'][0]['errorCode'] : 'unknown_errorCode';
								$result_message = isset($result['sf_error'][0]['message']) ? $result['sf_error'][0]['message'] : 'Unknown Error';
								$result_code = $result_errorCode;
								$result_message = $result_message;
							}else{
								$result_error = isset($result['sf_error']['error']) ? $result['sf_error']['error'] : 'unknown_error';
								$result_errordec = isset($result['sf_error']['error_description']) ? $result['sf_error']['error_description'] : 'Unknown Error';
								$result_code = $result_error;
								$result_message = $result_errordec;
							}
						}
						$response_arr['result_code'] = $result_code;
						$response_arr['result_message'] = $result_message;
					}
				}else{
					$response_arr['result_code'] = 'no_opportunity_prov';
					$response_arr['result_message'] = 'Please provide Opportunity Id.';
				}				
			}


			/**
			* Step two check the RecurringId if existing
			*/
			if(isset($_POST['step']) &&  $_POST['step'] == '2') {
				if(isset($_POST['RecurringId']) && !empty($_POST['RecurringId'])) {
					$oauth = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );
					$result = $oauth->api_request( 'data/v39.0/sobjects/npe03__Recurring_Donation__c/'.$_POST['RecurringId'], '', 'get' );

					$result_code = 'not_existing';
					$result_message = 'ID does not exist.';
					if(is_array($result)) {
						if((isset($result['status_code']) && $result['status_code'] == 200) && 
							(isset($result['sf_response']['Id']) && !empty($result['sf_response']['Id']))) {
							$search = isset($_POST['RecurringId']) ? $_POST['RecurringId'] : '';	
							if(!empty($search)) {
								$check_recurring = $WFC_DonationList->wfc_get_donations( 
									array(
										array(
											'key' 		=> 'meta_value',
											'operator' 	=> 'LIKE',
											'value' 	=> "'%{$search}%'",
										),
									)
								);	
								if(!empty($check_recurring)) {
									$result_code = 'id_taken';
									$result_message = 'Recurring Id is already linked to an existing donation. Please provide an Recurring Id not linked to another donation.';
								}else{
									$result_code = 'id_good';
									$result_message = 'Recurring Id is good to use.';
								}
								
							}else{
								$result_code = 'no_recurring_prov';
								$result_message = 'Please provide Recurring Id.';
							}
						}else{
							if(isset($result['sf_error']) && !empty($result['sf_error']) && isset($result['sf_error'][0]['errorCode'])) {
								$result_errorCode = isset($result['sf_error'][0]['errorCode']) ? $result['sf_error'][0]['errorCode'] : 'unknown_errorCode';
								$result_message = isset($result['sf_error'][0]['message']) ? $result['sf_error'][0]['message'] : 'Unknown Error';
								$result_code = $result_errorCode;
								$result_message = $result_message;
							}else{
								$result_error = isset($result['sf_error']['error']) ? $result['sf_error']['error'] : 'unknown_error';
								$result_errordec = isset($result['sf_error']['error_description']) ? $result['sf_error']['error_description'] : 'Unknown Error';
								$result_code = $result_error;
								$result_message = $result_errordec;
							}
						}
						$response_arr['result_code'] = $result_code;
						$response_arr['result_message'] = $result_message;
					}
				}else{
					$response_arr['result_code'] = 'no_recurring_prov';
					$response_arr['result_message'] = 'Please provide Recurring Id.';
				}				
			}

			/**
			* Step three update donation
			*/
			if(isset($_POST['step']) &&  $_POST['step'] == '3') {
				if((isset($_POST['OpportunityId']) && !empty($_POST['OpportunityId'])) &&
					(isset($_POST['RecurringId']) && !empty($_POST['RecurringId'])) &&
					(isset($_POST['meta_id']) && !empty($_POST['meta_id']))) {
					$WFC_DonationForms = new WFC_DonationForms();

					$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $_POST['meta_id'] );
					$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
					$donation = maybe_unserialize( $donations );
					$donation = stripslashes_deep( $donation );
					$donation['SalesforceResponse']['OriginalSalesforceResponse'] = isset($donation['SalesforceResponse']) ? $donation['SalesforceResponse'] : 'None';
					$donation['SalesforceResponse']['OpportunityId'] = isset($_POST['OpportunityId']) ? $_POST['OpportunityId'] : 'none';
					$donation['SalesforceResponse']['RecurringId'] = isset($_POST['RecurringId']) ? $_POST['RecurringId'] : 'none';
					$donation['SalesforceResponse']['status'] = '200';
					$donation['SalesforceResponse']['type'] = 'link';
					$donation['SalesforceResponse']['OpportunityId'] = 'This Recurring donation has been linked successfully';

					/** 
			        * update donation record
			        */      
			        $this->class->wfc_donation_data( array( serialize( $donation ), $_POST['meta_id'] ), 'update' ); 
			        $response_arr['result_code'] = 'update_done';
					$response_arr['result_message'] = 'Donation successfully updated.';
				}else{
					$response_arr['result_code'] = 'lack_data_prov';
					$response_arr['result_message'] = 'Please provide Opportunity Id and Recurring Id.';
				}
			}

			$response_arr['status'] = 'Success';
			echo json_encode($response_arr);
    		wp_die();
		}
	}
	
	/**
	 * Function that manages cron sync donation.
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  May 25, 2018
	 */
	public function wfc_don_cronresyncdonation() {
		$WFC_DonationForms = new WFC_DonationForms();
		$wfc_get_donations = $WFC_DonationForms->wfc_get_donations();
		/** 
		* EOF checking query
		*/    		    	
		if( false !== $wfc_get_donations ) {
			$wfc_don_settings = get_option( 'pronto_donation_settings', 0 );
			$purge_donation = isset($wfc_don_settings->purge_donation) ? $wfc_don_settings->purge_donation : 'false';

			/** 
			* EOF foreach
			*/   	
			$successfully_sync_donations = array();

			foreach ( $wfc_get_donations as $result ) {
				$meta_value = isset($result['meta_value']) ? $result['meta_value'] : '';
				$meta_id = isset($result['meta_id']) ? $result['meta_id'] : '';
				if(!empty($meta_value)){
					$post = maybe_unserialize($meta_value);
					$post = stripslashes_deep( $post );
					$post['post_meta_id'] = $meta_id;

					/** 
					* check timestamp of donation if cleared to sync
					*/
					$clearedtosync = $this->wfc_don_checktimestampforsync($post);

					/** 
					* make sure donation record is processed by the gateway
					*/

					if( isset( $post['PaymentTransaction'] ) &&
						$clearedtosync == true &&
						!isset( $post['exclude_on_cronsyncing'])) {

						if(isset($post['wfc_carttype']) && $post['wfc_carttype'] == 'advance_cart') {
							$syncadvancecartdonation_arr = array('donation_id' => $meta_id);

							$WFC_DonationList = new WFC_DonationList();
							$sync_overallstatus = $WFC_DonationList->wfc_get_sync_overallstatus_advancecart($post);
							if ($sync_overallstatus == 'complete' && $purge_donation =='true') {
								wp_clear_scheduled_hook( 'wfc_don_event_purgedonation', array(array('post_meta_id' => $meta_id)));
								wp_schedule_single_event( time() + 5, 'wfc_don_event_purgedonation', array(array('post_meta_id' => $meta_id)));
							}elseif($sync_overallstatus == 'incomplete'){
								wp_clear_scheduled_hook( 'wfc_don_event_syncadvancecartdonation', array($syncadvancecartdonation_arr));
				 				wp_schedule_single_event( time() + 5, 'wfc_don_event_syncadvancecartdonation', array($syncadvancecartdonation_arr));
							}else{
								// Do nothing
							}
						}else{
							$WFC_DonationList = new WFC_DonationList();
							$wfc_get_sync_status = $WFC_DonationList->wfc_get_sync_status( $post );
							$pdwp_donation_type = isset($post['pdwp_donation_type']) ? $post['pdwp_donation_type'] : '';
							$RecurringId = isset($post['SalesforceResponse']['RecurringId']) ? $post['SalesforceResponse']['RecurringId'] : '';

							wp_clear_scheduled_hook( 'wfc_don_event_resyncdonation', array($post));
				        	
							if($wfc_get_sync_status['status'] == 'recurring_failed_sync' && $pdwp_donation_type != 'one-off') {
								wp_schedule_single_event( time() + 1, 'wfc_don_event_resyncdonation', array($post) );
							}elseif($wfc_get_sync_status['status'] == 'not_sync' && $pdwp_donation_type != 'one-off') {
								wp_schedule_single_event( time() + 1, 'wfc_don_event_resyncdonation', array($post) );
							}elseif($wfc_get_sync_status['status'] == 'partial_sync' && !empty($RecurringId)){	
								wp_schedule_single_event( time() + 1, 'wfc_don_event_resyncdonation', array($post) );
							}elseif($wfc_get_sync_status['status'] == 'failed_sync' && $pdwp_donation_type == 'one-off'){	
								wp_schedule_single_event( time() + 1, 'wfc_don_event_resyncdonation', array($post) );
							}elseif($wfc_get_sync_status['status'] == 'not_sync' && $pdwp_donation_type == 'one-off'){	
								wp_schedule_single_event( time() + 1, 'wfc_don_event_resyncdonation', array($post) );
							}elseif($wfc_get_sync_status['status'] == 'attemp_reach' || $wfc_get_sync_status['status'] == 'success_sync' || $wfc_get_sync_status['status'] == 'cannot_sync'){

								if ($wfc_get_sync_status['status'] == 'success_sync' && $purge_donation =='true') {
									wp_clear_scheduled_hook( 'wfc_don_event_purgedonation', array(array('post_meta_id' => $meta_id)));
									wp_schedule_single_event( time() + 5, 'wfc_don_event_purgedonation', array(array('post_meta_id' => $meta_id)));
								}

							}else{
								// Do nothing yet	
							}	
						}

					}
				}
			}

			

		}
	}

	/**
	 * Check the timestamp if donation is still valid for syncing.
	 *
	 * @author Junjie Canonio
	 * @param array
	 * @return boolean
     *
     * @LastUpdated  September 28, 2018
	 */
	public function wfc_don_checktimestampforsync($donation){
		$donation_timestamp = isset($donation['timestamp']) ? $donation['timestamp'] : ''; 
		$donation_timestamp = (int)$donation_timestamp + 60*60;

		if (time() > $donation_timestamp) {
			return true;
		}else{
			return false;
		}
	}
	
	
	/**
	 * Checks the timestamp of the successfully synced donation and verify it with the purge from date and to date.
	 *
	 * @note If the donation record has been verified to be purge, the record will be deleted.
	 *
	 * @author Junjie Canonio
	 *
	 * @param array
	 *
	 * @throws Exception
     *
     * @LastUpdated  April 30, 2019
	 */
	public function wfc_don_purgedonationrecord($data){

		$meta_id = isset($data['post_meta_id']) ? $data['post_meta_id'] : '';

		$WFC_DonationForms = new WFC_DonationForms();
		$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $meta_id );
		$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
		$donation = maybe_unserialize($donations);
		$donation = stripslashes_deep($donation);

		$WFC_DonationList = new WFC_DonationList();
		if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart') {
			$sync_overallstatus = $WFC_DonationList->wfc_get_sync_overallstatus_advancecart($donation);
			if ($sync_overallstatus == 'complete') {
				$execute_purge = true;
			}else{
				$execute_purge = false;
			}
		}else{
			$wfc_get_sync_status = $WFC_DonationList->wfc_get_sync_status( $donation );
			if (isset($wfc_get_sync_status['status']) && $wfc_get_sync_status['status'] == 'success_sync') {
				$execute_purge = true;
			}else{
				$execute_purge = false;
			}
		}

		if ($execute_purge == true) {
			
			$donation_timestamp = isset($donation['timestamp']) ? $donation['timestamp'] : '';
			$donation_timestamp = strtotime(date("Y-m-d", $donation_timestamp));

			$current_datetime = new DateTime();
			$current_datetime->setTimestamp(time());
			$current_datetime_result = $current_datetime->format('Y-m-d H:i:s');
			$current_datetime_result = DateTime::createFromFormat('Y-m-d H:i:s',$current_datetime_result);
			
			$donation_datetime = new DateTime();
			$donation_datetime->setTimestamp($donation_timestamp);
			$donation_datetime_result = $donation_datetime->format('Y-m-d H:i:s');
			$donation_datetime_result = DateTime::createFromFormat('Y-m-d H:i:s',$donation_datetime_result);


			$wfc_don_settings = get_option( 'pronto_donation_settings', 0 );

			$args = array(
				'date_of_donation' => $donation_datetime_result,
				'purge_current_date' => $current_datetime_result
			);

			$purge_donation_datefrom_timestamp = isset($wfc_don_settings->purge_donation_datefrom_timestamp) ?  $wfc_don_settings->purge_donation_datefrom_timestamp : '';
			$purge_donation_datefrom_timestamp_result = DateTime::createFromFormat('Y-m-d H:i:s',$purge_donation_datefrom_timestamp);
			
			$purge_donation_datefrom = new DateTime();
			if (is_integer($purge_donation_datefrom_timestamp)){
				$purge_donation_datefrom->setTimestamp($purge_donation_datefrom_timestamp);
				$purge_donation_datefrom_result = $purge_donation_datefrom->format('Y-m-d H:i:s');
				$purge_donation_datefrom_result = DateTime::createFromFormat('Y-m-d H:i:s',$purge_donation_datefrom_result);
				$args['purge_from_date'] = $purge_donation_datefrom_result;
			}


			$purge_donation_dateto_timestamp = isset($wfc_don_settings->purge_donation_dateto_timestamp) ? $wfc_don_settings->purge_donation_dateto_timestamp : '';

			$purge_donation_dateto = new DateTime();
			if (is_integer($purge_donation_dateto_timestamp)) {
				$purge_donation_dateto->setTimestamp($purge_donation_dateto_timestamp);
				$purge_donation_dateto_result = $purge_donation_dateto->format('Y-m-d H:i:s');
				$purge_donation_dateto_result = DateTime::createFromFormat('Y-m-d H:i:s', $purge_donation_dateto_result);
				$args['purge_to_date'] = $purge_donation_dateto_result;
			}



			if (!empty($donation_timestamp) && !empty($purge_donation_datefrom_timestamp) && !empty($purge_donation_dateto_timestamp)) {

				// purge record if donation date is in between the purge from date and to date

				if (($donation_timestamp >= $purge_donation_datefrom_timestamp) && ($donation_timestamp <= $purge_donation_dateto_timestamp)){

					$args['action'] = 'include';
					$this->wfc_don_purgeanddeletedonation($args ,$donation);
				 
				}else{

					$args['action'] = 'exclude';
					$this->wfc_don_purgeanddeletedonation($args ,$donation);

				}

			}elseif(!empty($donation_timestamp) && !empty($purge_donation_datefrom_timestamp) && empty($purge_donation_dateto_timestamp)){

				// purge record if donation date is on the purge from date and beyond

				if ($donation_timestamp >= $purge_donation_datefrom_timestamp){

			        $args['action'] = 'include';
					$this->wfc_don_purgeanddeletedonation($args ,$donation);
				 
				}else{

					$args['action'] = 'exclude';
					$this->wfc_don_purgeanddeletedonation($args ,$donation);

				}

			}elseif(!empty($donation_timestamp) && empty($purge_donation_datefrom_timestamp) && !empty($purge_donation_dateto_timestamp)){

				// purge record if donation date is on the purge from date and beyond

				if ($donation_timestamp <= $purge_donation_dateto_timestamp){
			     
			        $args['action'] = 'include';
					$this->wfc_don_purgeanddeletedonation($args ,$donation);
				 
				}else{

					$args['action'] = 'exclude';
					$this->wfc_don_purgeanddeletedonation($args ,$donation);

				}

			}else{

				$args['action'] = 'exclude';
				$this->wfc_don_purgeanddeletedonation($args ,$donation);

			}
		}

	}
	
	
	/**
	 * Updates and sets the donation record to either be included or excluded from the donation purge.
	 *
	 * @author Junjie Canonio
	 *
	 * @param $arg
	 * @param $donation
	 *
	 * @return void
     *
     * @LastUpdated  April 30, 2019
	 */
	public function wfc_don_purgeanddeletedonation($arg,$donation){
		$purge_action = isset($arg['action']) ? $arg['action'] : 'exclude';
		$date_of_donation = isset($arg['date_of_donation']) ? $arg['date_of_donation'] : '';
		$purge_current_date = isset($arg['purge_current_date']) ? $arg['purge_current_date'] : '';
		$purge_from_date = isset($arg['purge_from_date']) ? $arg['purge_from_date'] : '';
		$purge_to_date = isset($arg['purge_to_date']) ? $arg['purge_to_date'] : '';


		$donation_purge = isset($donation['purge_donation']['purge']) ? $donation['purge_donation']['purge'] : '';
		$meta_id = isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '';
		$donation_logs_id = isset($donation['donation_logs_id']) ? $donation['donation_logs_id'] : 0;

		if ($donation_purge == 'include') {
			
			if($donation_logs_id != 0 && !empty($meta_id)){

	            $logs['delete_donation']['form'] = $donation;
	            $logs['delete_donation']['info'] = array(
	            	'donation_id' => isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '',
	            	'date_of_donation' => $date_of_donation,
	            	'date_deleted' => $purge_current_date,
	            );
				$logs['delete_donation']['status'] = 'The donation has been scheduled for purge and has now been deleted.';

	            $logs_status = update_post_meta( $donation_logs_id, 'deleteDonation', serialize($logs) );

	            if ($logs_status == true) {
	            	$data = $this->class->wfc_delete_donation_data( $meta_id );	
	            }
			}

		}else{
			
	        if($donation_logs_id != 0 && !empty($meta_id)){

	        	if ($purge_action == 'include') {
		        	$donation['purge_donation'] = array(
		        		'status' => 'This donation is scheduled for deletion.',
		        		'purge' => 'include' 
		        	);
	        	}else{
	        		$donation['purge_donation'] = array(
		        		'status' => 'This donation has been excluded from deletion.',
		        		'purge' => 'exclude' 
		        	);
	        	}

	        	$data = $this->class->wfc_donation_data(
	                array(
	                    serialize( $donation ),
	                    $meta_id,
	                ),
	                'update'
	            );	

	            $logs['purge_donation']['form'] = $donation;
	            $logs['purge_donation']['info'] = array(
	            	'donation_id' => isset($donation['post_meta_id']) ? $donation['post_meta_id'] : '',
	            	'date_of_donation' => $date_of_donation,
	            	'purge_current_date' => $purge_current_date,
	            	'purge_from_date' => $purge_from_date,
	            	'purge_to_date' => $purge_to_date,
	            );

				$logs['purge_donation']['status'] = $donation['purge_donation']['status'];
				$logs['purge_donation']['purge'] = $donation['purge_donation']['purge'];
	        
	            update_post_meta( $donation_logs_id, 'purgeDonation', serialize($logs) );
	        }
			
		}


	}
	
	
	/**
	 * Cron sync donation single event function.
	 * LastUpdated : May 25, 2018
	 *
	 * @author Junjie Canonio
	 * @param $post
     *
     * @LastUpdated  May 25, 2018
	 */
	public function wfc_don_eventresyncdonation($post){
		/** 
		* make sure donation record is processed by the gateway
		*/	
		if( isset( $post['PaymentTransaction'] ) ) {
			$WFC_DonationList = new WFC_DonationList();
			$WFC_DonationDataHandler = new WFC_DonationDataHandler();

			$wfc_get_sync_status = $WFC_DonationList->wfc_get_sync_status( $post );

			$pdwp_donation_type = isset($post['pdwp_donation_type']) ? $post['pdwp_donation_type'] : '';
			$RecurringId = isset($post['SalesforceResponse']['RecurringId']) ? $post['SalesforceResponse']['RecurringId'] : '';

			if($wfc_get_sync_status['status'] == 'recurring_failed_sync' && $pdwp_donation_type != 'one-off') {

				$result = $WFC_DonationDataHandler->wfc_sync_recurringdonation_tosf($post,$this->class,'');
				if((isset($result['SalesforceResponse']['RecurringId']) && !empty($result['SalesforceResponse']['RecurringId'])) &&
        			(isset($result['SalesforceResponse']['status']) && $result['SalesforceResponse']['status'] == 200 )){

					$RecurringId = isset($result['SalesforceResponse']['RecurringId']) ? $result['SalesforceResponse']['RecurringId'] : '';
					$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($post,$this->class,$RecurringId);
				
				}

			}elseif($wfc_get_sync_status['status'] == 'not_sync' && $pdwp_donation_type != 'one-off') {

				$result = $WFC_DonationDataHandler->wfc_sync_recurringdonation_tosf($post,$this->class,'');
				if((isset($result['SalesforceResponse']['RecurringId']) && !empty($result['SalesforceResponse']['RecurringId'])) &&
        			(isset($result['SalesforceResponse']['status']) && $result['SalesforceResponse']['status'] == 200 )){

					$RecurringId = isset($result['SalesforceResponse']['RecurringId']) ? $result['SalesforceResponse']['RecurringId'] : '';
					$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($post,$this->class,$RecurringId);
				
				}
				
			}elseif($wfc_get_sync_status['status'] == 'partial_sync' && !empty($RecurringId)){

				$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($post,$this->class,$RecurringId);

			}elseif($wfc_get_sync_status['status'] == 'failed_sync' && $pdwp_donation_type == 'one-off'){	

				$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($post,$this->class);

			}elseif($wfc_get_sync_status['status'] == 'not_sync' && $pdwp_donation_type == 'one-off'){	

				$result = $WFC_DonationDataHandler->wfc_sync_singledonation_tosf($post,$this->class);

			}elseif($wfc_get_sync_status['status'] == 'attemp_reach' || $wfc_get_sync_status['status'] == 'success_sync' || $wfc_get_sync_status['status'] == 'cannot_sync'){
					
			}else{

			}						
		}	
	}
	
	
	/**
	 * Cron sync donation single event function for advance cart.
	 *
	 * @author Junjie Canonio
	 * @param $datials
     *
     * @LastUpdated  July 26, 2018
	 */
	public function wfc_don_syncadvancecartdonation($datials){
		$donation_id = isset($datials['donation_id']) ? $datials['donation_id'] : '';
		$WFC_DonationList = new WFC_DonationList();
		$donation = $this->wfc_don_fetchunserializedDonation($donation_id);

		if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart' && isset($donation['cartItems'])) {

			$cartItems = isset($donation['cartItems']) ? $donation['cartItems'] : array(); 
			$singlePaymentsItem = array();
			$recurringPaymentsItem = array();
			if(!empty($cartItems)) {
				foreach ($cartItems as $key => $value) {
					if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
						array_push($singlePaymentsItem,$value);
					}else{
						array_push($recurringPaymentsItem,$value);
					}
				}
			}

			$cartItems = $recurringPaymentsItem;
			array_push($cartItems,$singlePaymentsItem);
			

			if(!empty($cartItems)) {
				foreach ($cartItems as $key => $value) {
					$pdwp_donation_type = isset($value['wfc_donation_type']) ? $value['wfc_donation_type'] : '';
					if (empty($pdwp_donation_type)) {
						$pdwp_donation_type = isset($value[0]['wfc_donation_type']) ? $value[0]['wfc_donation_type'] : '';
					}

					$fresh_donation = $this->wfc_don_fetchunserializedDonation($donation_id);
					$wfc_get_sync_status = $WFC_DonationList->wfc_get_sync_status_advancecart($fresh_donation,NULL,$key);
					$RecurringId = isset($wfc_get_sync_status['RecurringId']) ? $wfc_get_sync_status['RecurringId'] : '';

					if($wfc_get_sync_status['status'] == 'recurring_failed_sync' && $pdwp_donation_type != 'one-off') {
						if (!empty($RecurringId)) {
							$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);
						}else {
							$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);
							if (isset($resync_result['STATUS']) && $resync_result['STATUS'] == 'Success') {
								$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);
							}
						}	
					}elseif($wfc_get_sync_status['status'] == 'not_sync' && $pdwp_donation_type != 'one-off') {

						$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);
						if (isset($resync_result['STATUS']) && $resync_result['STATUS'] == 'Success') {
							$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);
						}

					}elseif($wfc_get_sync_status['status'] == 'partial_sync' && !empty($RecurringId)){	

						$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);

					}elseif($wfc_get_sync_status['status'] == 'failed_sync' && $pdwp_donation_type == 'one-off'){	

						$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);

					}elseif($wfc_get_sync_status['status'] == 'not_sync' && $pdwp_donation_type == 'one-off'){

						$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $key);

					}elseif($wfc_get_sync_status['status'] == 'attemp_reach' || $wfc_get_sync_status['status'] == 'success_sync' || $wfc_get_sync_status['status'] == 'cannot_sync'){
						// Do nothing yet
					}else{
						// Do nothing yet
					}	

				}
			}
		}
		
	}
	
	/**
	 * Fetches and unserializes donation.
	 *
	 * @author Junjie Canonio
	 *
	 * @param $donation_id
	 *
	 * @return array
     *
     * @LastUpdated  July 25, 2018
	 */
	public function wfc_don_fetchunserializedDonation($donation_id){
		$WFC_DonationForms = new WFC_DonationForms();
		$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $donation_id );
		$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
		$donation = maybe_unserialize($donations);
		$donation = stripslashes_deep($donation);	
		return $donation;
	}
	
	/**
	 * Resync advance cart donation.
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  July 25, 2018
	 */
	public function wfc_don_resyncadvancecartdonation_callback() {	
		if(isset($_POST['action']) && $_POST['action'] =='wfc_don_resyncadvancecart_action'){
			$donation_id = isset($_POST['donation_id']) ? $_POST['donation_id'] : '';
			$cart_suffix = isset($_POST['cart_suffix']) ? $_POST['cart_suffix'] : '';
			$resync_result = $this->wfc_don_eventresync_advancecartdonation($donation_id, $cart_suffix);
			$response_arr['status'] = 'Success';
			$response_arr['POST'] = $_POST;
			$response_arr['sync_result'] = $resync_result;
			echo json_encode($response_arr);
			wp_die();
		}
	}
	
	
	/**
	 * Cron sync advance cart donation single event function.
	 *
	 * @author Junjie Canonio
	 *
	 * @param $meta_id
	 * @param $suffix
	 *
	 * @return array
     *
     * @LastUpdated  July 23, 2018
	 */
	public function wfc_don_eventresync_advancecartdonation($meta_id, $suffix){
		$WFC_DonationList = new WFC_DonationList();
		$WFC_DonationForms = new WFC_DonationForms();

		$donations = $WFC_DonationForms->wfc_get_donation_by_meta_id( $meta_id );
		$donations = isset($donations[0]['meta_value']) ? $donations[0]['meta_value'] : array();
		$donation = maybe_unserialize($donations);
		$donation = stripslashes_deep($donation);
		
		if(isset($donation['wfc_carttype']) && $donation['wfc_carttype'] == 'advance_cart') {
			$donation['advance_cart_ver'] = '2.0';	
			$cartItems = isset($donation['cartItems']) ? $donation['cartItems'] : array(); 
			$singlePaymentsItem = array();
			$recurringPaymentsItem = array();
			if(!empty($cartItems)) {
				foreach ($cartItems as $key => $value) {
					if (isset($value['wfc_donation_type']) && $value['wfc_donation_type'] == 'one-off') {
						array_push($singlePaymentsItem,$value);
					}else{
						array_push($recurringPaymentsItem,$value);
					}
				}
			}

			if (sizeof($singlePaymentsItem) > 1) {
				$cartItems = $recurringPaymentsItem;
				array_push($cartItems,$singlePaymentsItem);	
			}
			foreach ($cartItems as $key => $value) {
				if (empty($value)) {
					unset($cartItems[$key]);
				}
			}

			// return array(
			// 	'STATUS' => 'Success',
			// 	'single' => $singlePaymentsItem,
			// 	'recurring' => $recurringPaymentsItem,
			// 	'cartItems' => $cartItems,
			// 	'oneoff' => $cartItems[3],
			// 	'suffix' =>  $suffix
			// );

						
			if(!empty($cartItems)) {
				$WFC_DonationDataHandler = new WFC_DonationDataHandler();

				$donationToSync = array(
					'Suffix' => '',
					'SyncAction' => false,
					'DonationType' => NULL,
					'DonationAmount' => '',
					'RecurringId' => '',
					'SyncStatus' => '',
				);
				foreach ($cartItems as $key => $value) {
					if ($key == $suffix) {
						if (sizeof($singlePaymentsItem) > 1 && !isset($value['wfc_donation_type'])) {
							$wfc_get_sync_status = $WFC_DonationList->wfc_get_sync_status_advancecart($donation,NULL,$key);
							if($donationToSync['SyncAction'] == false) {
								$isAllSingle = 'no';
								$amountAlllSingle = 0; 
								foreach ($value as $singlekey => $singlevalue) {
									$wfc_donation_type = 
									isset($singlevalue['wfc_donation_type']) ? $singlevalue['wfc_donation_type'] : '';
									$amount = 
									isset($singlevalue['amount']) ? $singlevalue['amount'] : '';
									if ($wfc_donation_type == 'one-off') {
										$isAllSingle = 'yes';
										$amountAlllSingle = $amountAlllSingle + $amount;
									}else{
										$isAllSingle = 'no';
									}
								}

								if ($isAllSingle == 'yes') {
									if($wfc_get_sync_status['status'] == 'failed_sync'){	
										$donationToSync['Suffix'] = $key;
										$donationToSync['SyncAction'] = true;
										$donationToSync['DonationType'] = 'one-off';
										$donationToSync['DonationAmount'] = $amountAlllSingle;
										$donationToSync['RecurringId'] = '';
										$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
									}elseif($wfc_get_sync_status['status'] == 'not_sync'){	
										$donationToSync['Suffix'] = $key;
										$donationToSync['SyncAction'] = true;
										$donationToSync['DonationType'] = 'one-off';
										$donationToSync['DonationAmount'] = $amountAlllSingle;
										$donationToSync['RecurringId'] = '';
										$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
									}elseif($wfc_get_sync_status['status'] == 'attemp_reach' || $wfc_get_sync_status['status'] == 'success_sync' || $wfc_get_sync_status['status'] == 'cannot_sync'){
										$donationToSync['Suffix'] = '';
										$donationToSync['SyncAction'] = false;
										$donationToSync['DonationType'] = NULL;
										$donationToSync['DonationAmount'] = $amountAlllSingle;
										$donationToSync['RecurringId'] = '';
										$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
									}else{
										$donationToSync['Suffix'] = '';
										$donationToSync['SyncAction'] = false;
										$donationToSync['DonationType'] = NULL;
										$donationToSync['DonationAmount'] = $amountAlllSingle;
										$donationToSync['RecurringId'] = '';
										$donationToSync['SyncStatus'] = 'not_sync';
									}
								}

							}	
							// return array(
							// 	'STATUS' => 'Success',
							// 	'single' => $singlePaymentsItem,
							// 	'recurring' => $recurringPaymentsItem,
							// 	'cartItems' => $cartItems,
							// 	'suffix' => $suffix,
							// 	'wfc_get_sync_status' => $wfc_get_sync_status,
							// 	'singlePaymentsItem'=>sizeof($singlePaymentsItem),
							// 	'value' => $value,
							// 	'isAllSingle' => $isAllSingle,
							// 	'amountAlllSingle' => $amountAlllSingle
							// );

						}else{
							$wfc_get_sync_status = $WFC_DonationList->wfc_get_sync_status_advancecart($donation,NULL,$key);
							if($donationToSync['SyncAction'] == false) {
								$wfc_donation_type = isset($value['wfc_donation_type']) ? $value['wfc_donation_type'] : NULL;
								if($wfc_get_sync_status['status'] == 'recurring_failed_sync' && $wfc_donation_type != 'one-off') {
									$donationToSync['Suffix'] = $key;
									$donationToSync['SyncAction'] = true;
									$donationToSync['DonationType'] = $wfc_donation_type;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];

									$donationToSync['RecurringId'] = 
									isset($wfc_get_sync_status['RecurringId']) ? 
									$wfc_get_sync_status['RecurringId'] : '';
								}elseif($wfc_get_sync_status['status'] == 'not_sync' && $wfc_donation_type != 'one-off') {
									$donationToSync['Suffix'] = $key;
									$donationToSync['SyncAction'] = true;
									$donationToSync['DonationType'] = $wfc_donation_type;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
								}elseif($wfc_get_sync_status['status'] == 'partial_sync' && !empty($wfc_get_sync_status['RecurringId'])){
									$donationToSync['Suffix'] = $key;
									$donationToSync['SyncAction'] = true;
									$donationToSync['DonationType'] = $wfc_donation_type;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['RecurringId'] = $wfc_get_sync_status['RecurringId'];
									$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
								}elseif($wfc_get_sync_status['status'] == 'failed_sync' && $wfc_donation_type == 'one-off'){	
									$donationToSync['Suffix'] = $key;
									$donationToSync['SyncAction'] = true;
									$donationToSync['DonationType'] = $wfc_donation_type;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['RecurringId'] = '';
									$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
								}elseif($wfc_get_sync_status['status'] == 'not_sync' && $wfc_donation_type == 'one-off'){	
									$donationToSync['Suffix'] = $key;
									$donationToSync['SyncAction'] = true;
									$donationToSync['DonationType'] = $wfc_donation_type;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['RecurringId'] = '';
									$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
								}elseif($wfc_get_sync_status['status'] == 'attemp_reach' || $wfc_get_sync_status['status'] == 'success_sync' || $wfc_get_sync_status['status'] == 'cannot_sync'){
									$donationToSync['Suffix'] = '';
									$donationToSync['SyncAction'] = false;
									$donationToSync['DonationType'] = NULL;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['RecurringId'] = '';
									$donationToSync['SyncStatus'] = $wfc_get_sync_status['status'];
								}else{
									$donationToSync['Suffix'] = '';
									$donationToSync['SyncAction'] = false;
									$donationToSync['DonationType'] = NULL;
									$donationToSync['DonationAmount'] = $value['amount'];
									$donationToSync['RecurringId'] = '';
									$donationToSync['SyncStatus'] = 'not_sync';
								}	
							}
						}
					}
				}
				
				if($donationToSync['SyncAction'] == true && empty($donationToSync['RecurringId'])) {
					$donation['wfc_advancecart_suffix'] = $donationToSync['Suffix']; 
					$donation['wfc_advancecart_amount'] = $donationToSync['DonationAmount'];
					$donation['pdwp_donation_type'] = $donationToSync['DonationType']; 
					$result = $WFC_DonationDataHandler->wfc_sync_advancecartdonation_tosf($donation,$this->class);

					if($donationToSync['DonationType'] == 'one-off') {
						$result_status = isset($result['SalesforceLogs'][$donationToSync['Suffix']]['result']['single']['status']) ? $result['SalesforceLogs'][$donationToSync['Suffix']]['result']['single']['status'] : 0;
					}else{
						$result_status = isset($result['SalesforceLogs'][$donationToSync['Suffix']]['result']['recurring']['status']) ? $result['SalesforceLogs'][$donationToSync['Suffix']]['result']['recurring']['status'] : 0;
					}
					
					return array(
						'STATUS' => 'Success',
						'sfsync_result' => $result,
						'result' => $result_status,
						'donationToSync' => $donationToSync,
					);
				}elseif($donationToSync['SyncAction'] == true && !empty($donationToSync['RecurringId'])) {
					$donation['wfc_advancecart_suffix'] = $donationToSync['Suffix']; 
					$donation['wfc_advancecart_amount'] = $donationToSync['DonationAmount'];
					$donation['pdwp_donation_type'] = $donationToSync['DonationType']; 
					$result = $WFC_DonationDataHandler->wfc_sync_advancecartdonation_tosf($donation,$this->class,$donationToSync['RecurringId']);
					$result_status = isset($result['SalesforceLogs'][$donationToSync['Suffix']]['result']['single']['status']) ? $result['SalesforceLogs'][$donationToSync['Suffix']]['result']['single']['status'] : 0;

					return array(
						'STATUS' => 'Success',
						'sfsync_result' => $result,
						'result' => $result_status,
						'donationToSync' => $donationToSync,
						'RecurringId' => $donationToSync['RecurringId'],
						
					);
				}else{
					return array(
						'STATUS' => 'Success',
						'result' => 'No items to sync.',
					);
				}


			}else{
				return array(
					'STATUS' => 'Failed',
					'result' => 'No items on cart',
				);				
			}
		}else{
			return array(
				'STATUS' => 'Failed',
				'result' => 'Donation is not an advance cart.',
			);
		}
			
	}


	/**
	 * Callback for saving form settings.
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	public function wfc_save_form_settings() {
		if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

		global $post;

		if (isset($post) && $post->post_type === 'pdwp_donation') {
			$post_id = $post->ID;
			foreach ($_POST as $key => $value) {
				if (strpos($key, 'donation') !== false) {
					update_post_meta($post_id, $key, $value);
				}
			}
		}
	}

	/**
	 * AJAX Callback for campaign resync functionality
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	public function wfc_resync_campaign() {
		$result = WFC_SalesforcePPAPI::wfc_donation_fetchcamp();

		$response['status'] = $result['status'];
		$response['message'] = $result['message'];

		if ($result['status'] === 'Success') {
			$response['campaign'] = get_option('pdwp_ASSFAPI/campaign');
		}

		echo json_encode($response);

		wp_die();
	}

	/**
	 * AJAX Callback for GAU resync functionality
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	public function wfc_resync_gau() {
		$result = WFC_SalesforcePPAPI::wfc_donation_fetchgau();

		$response['status'] = $result['status'];
		$response['message'] = $result['message'];

		if ($result['status'] === 'Success') {
			$response['gau'] = get_option('pdwp_ASSFAPI/gau');
		}

		echo json_encode($response);

		wp_die();
	}

	/**
	 * AJAX Callback for resetting sortable-item values
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	public function donation_form_fields_reset() {
		$post_id = $_REQUEST['post_id'];
		$success = false;
		switch ($_REQUEST['sortable_field']) {
			case 'amount-levels':
				$success = delete_post_meta($post_id, 'donation_v5_form_amount_levels');
				break;
			case 'form-fields':
				$success = delete_post_meta($post_id, 'donation_v5_form_user_fields');
				break;
			case 'payment-gateways':
				$success = delete_post_meta($post_id, 'donation_v5_form_payment_gateways');
				break;
			case 'credit':
				$success = delete_post_meta($post_id, 'donation_form_credit');
				break;
			case 'debit':
				$success = delete_post_meta($post_id, 'donation_form_debit');
				break;
		}

		$response['success'] = $success;

		echo json_encode($response);

		wp_die();
	}

	/**
	 * AJAX Callback for Thank you Email testing
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	public function wfc_donation_donor_thankyou_email() {
		$donation = new WFC_DonationEmailHandler();

		$response = $donation->wfc_donation_donor_thankyou_email(array(
			'donation_campaign' => $_REQUEST['post_id'],
			'donor_email' => $_REQUEST['email'],
			'donor_first_name' => 'First Name',
			'donor_last_name' => 'Last Name',
			'donor_address' => 'Address St.',
			'donor_country' => 'Australia',
			'donor_state' => 'State',
			'donor_postcode' => '1234',
			'donor_suburb' => 'Suburb',
			'currency' => 'AUD',
			'pdwp_custom_amount' => '1000',
			'pdwp_amount' => '1000'
		), 'test');

		echo json_encode($response);

		wp_die();
	}

	/**
	 * AJAX Callback for Admin notification testing
	 *
	 * @since 1.0.0
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 */
	public function wfc_donation_admin_notification_test() {
		$WFC_DonationEmailHandler = new WFC_DonationEmailHandler();

		switch ($_REQUEST['test']) {
			case 'success':
				$response['success'] = $WFC_DonationEmailHandler->wfc_donation_admin_success_notification(array(
					'donation_campaign' => $_REQUEST['post_id'],
					'donor_company' => 'Test Company',
					'donor_email' => 'test@email.com',
					'donor_first_name' => 'First',
					'donor_last_name' => 'Last',
					'donor_address' => '33 George Street',
					'donor_country' => 'Australia',
					'donor_state' => 'NSW',
					'donor_postcode' => '2000',
					'donor_suburb' => 'The Rocks',
					'currency' => '$,AUD',
					'payment_type' => 'credit_card',
					'pdwp_amount' => '10',
					'PaymentTransaction' => array(
						'TxnStatus' => 'Success',
						'TxnId' => '6796999',
						'TxnDate' => '2018-05-24',
						'TxnMessage' => 'null-1527151325-1622750464-2733',
						'TxnPaymentReference' => '1527151325-1622750464-2733'
					),
					'SalesforceResponse' => array(
						'OpportunityId' => 'a0a7F000001o74FQAQ',
						'RecurringId' => 'a0a7F000001o74FQAQ',
						'status' => '200',
						'Message' => 'The Recurring donation has been created successfully.'
					)
				));
				break;
			case 'failed':
				$response['success'] = $WFC_DonationEmailHandler->wfc_donation_admin_fail_notification(array(
					'donation_campaign' => $_REQUEST['post_id'],
					'donor_company' => 'Test Company',
					'donor_email' => 'test@email.com',
					'donor_first_name' => 'First',
					'donor_last_name' => 'Last',
					'donor_address' => '33 George Street',
					'donor_country' => 'Australia',
					'donor_state' => 'NSW',
					'donor_postcode' => '2000',
					'donor_suburb' => 'The Rocks',
					'currency' => '$,AUD',
					'payment_type' => 'credit_card',
					'pdwp_amount' => '20',
					'PaymentTransaction' => array(
						'TxnStatus' => 'Success',
						'TxnId' => '6795628',
						'TxnDate' => '2018-05-24',
						'TxnMessage' => 'null-1527147827-2107009953-2730',
						'TxnPaymentReference' => '1527147827-2107009953-2730'
					),
					'SalesforceResponse' => array(
						'status' => '',
						'Message' => ''
					)
				));
				break;
			default:
				$response['success'] = false;
				break;
		}

		echo json_encode($response);

		wp_die();
	}
	
	/**
	 * Excludes selected donations from corn sync.
	 *
	 * @author Junjie Canonio
     *
     * @LastUpdated  October 10, 2018
	 */
	public function wfc_don_excludefromcronsync_callback() {	
		if(isset($_POST['action']) && $_POST['action'] =='wfc_don_excludefromcronsync_action'){
			$donation = $this->wfc_don_fetchunserializedDonation($_POST['donation_id']);
			if(isset($_POST['exludecronsync']) && $_POST['exludecronsync']  == 'true') {
				$donation['exclude_on_cronsyncing'] = 'true';
			}else{
				unset($donation['exclude_on_cronsyncing']);				
			}
			
			$update_result = $this->class->wfc_donation_data( array( serialize( $donation ), $_POST['donation_id'] ), 'update' ); 
			if ($update_result == 1) {
				$response_arr['status'] = 'Success';
				$response_arr['POST'] = $_POST;
				$response_arr['update_result'] = $update_result;
				$response_arr['donation'] = $donation;
			}else{
				$response_arr['status'] = 'Failed';
				$response_arr['POST'] = $_POST;
				$response_arr['update_result'] = $update_result;
				$response_arr['donation'] = $donation;
			}

			echo json_encode($response_arr);
			wp_die();
		}
	}
}
