(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function(){

      /*  // interaction logic for pins and notes on the Portal email settings.
        var pinsContainer = $(document).find('.wfc-portal-list-info');
        var pins = pinsContainer.find('.pins');
        var notes = pinsContainer.find('.notes');

        // increment top to align note to the designated pin
        var counter = 0;
        notes.find('.info-list-container').each(function() {
            var $this = $(this);

            var top = parseInt($this.css('top').replace('px', ''));

            $this.css('top', ((60 * counter++) + top) + 'px');
        });

        pins.find('.info-loading-icon').on('click', function(e) {
            e.stopPropagation();
            var $this = $(this);
            var targetNote = $(notes.find('.info-list-container')[$this.index()]);

            console.log(pins.find('.info-loading-icon.active').length);

            if ($this.hasClass('active')) {
                if (pins.find('.info-loading-icon.active').length > 0) {
                    $this.removeClass('active');
                    targetNote.removeClass('active');
                }
            }
            else {
                pins.find('.info-loading-icon.active').removeClass('active');
                notes.find('.info-list-container.active').removeClass('active');

                $this.addClass('active');
                targetNote.addClass('active');
            }
        });
        notes.find('.info-list-container').on('click', function(e) {
            e.stopPropagation();
        });

        $(document).on('click', function() {
            pins.find('.info-loading-icon').removeClass('active');
            notes.find('.info-list-container').removeClass('active');
        });*/

        $('body').on('click', '.pup-switcher-wrap', function() {
        	var $this = $(this);
            var pupSwitchTrue = $this.find('.pup-input-switcher-true');
            var pupSwitchFalse = $this.find('.pup-input-switcher-false');
            var flag = pupSwitchTrue[0].checked;

            pupSwitchTrue.attr('checked', !flag);
            pupSwitchFalse.attr('checked', flag).trigger('change');
        })
	});
})( jQuery );
