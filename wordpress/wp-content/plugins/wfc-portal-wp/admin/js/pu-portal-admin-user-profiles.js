(function( $ ) {
	'use strict';
	$(document).ready(function(){
		var table	= $('table.pup_profile_fields');
		var form	= table.parents('form')[0];

		if(typeof form !== 'undefined') {

			var fileInput 	= table.find('.img-upload-ctrl');
			var uploadBtn 	= table.find('.btn-upload-avatar');
			var img 		= table.find('.avatar-ctrl');

			form.encoding 	= "multipart/form-data";
			form.setAttribute('enctype', 'multipart/form-data');

			uploadBtn.on('click', function() { fileInput.trigger('click'); });

			fileInput.on('change', function(){
				if (this.files && this.files[0]) {
					var reader = new FileReader();
					
					reader.onload = function (e) {
						img.attr('src', e.target.result);
					}
					
					reader.readAsDataURL(this.files[0]);
				}
			});
		}
	});
})( jQuery );
