(function ($) {
    $(document).ready(function () {
        var reg;
        var notehtml;
        var notext;
        var sel;

        if ($('input[name=\'wfc_portal_reg_settings\']').val() === 'true') {
            $('#registration-form-2').addClass('hide');
        } else {
            $('#registration-form-2').removeClass('hide');
        }

        $('input[name=\'wfc_portal_reg_settings\']').on('change', function () {
            if ($('input[name=\'wfc_portal_reg_settings\']').val() === 'true') {
                $('#registration-form-2').addClass('hide');
            } else {
                $('#registration-form-2').removeClass('hide');
            }
        });

        $('#wfc_portal_save_reg_settings').on('click', function () {
            notehtml = get_tinymce_content('wfc_portal_reg_wp_editor');
            notext = $('textarea[name=\'wfc_portal_reg_textarea\']').val();
            sel = $('input[name=\'wfc_portal_reg_radio\']:checked').val();
            reg = $('input[name=\'wfc_portal_reg_settings\']').val();
            $.ajax({
                url: wfc_portal_reg_param.url,
                dataType: 'json',
                type: 'post',
                data: {
                    action: 'register-settings-js',
                    nonce: wfc_portal_reg_param.nonce,
                    selects: sel,
                    reg: reg,
                    notehtml: notehtml,
                    notext: notext
                },
                success: function (resp) {
                    var STATUS = resp['status'];

                    if (STATUS === 'Success') {
                        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Successful</div>', 3000, 'success');
                    } else {
                        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Failed</div>', 3000, 'error');
                    }
                },
                error: function (response) {
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Failed</div>', 3000, 'error');
                }
            })
        });

        function get_tinymce_content(id) {
            var content;
            var inputid = id;
            var editor = tinyMCE.get(inputid);
            var textArea = $('textarea#' + inputid);
            if (textArea.length > 0 && textArea.is(':visible')) {
                content = textArea.val();
            }
            return content;
        }


    })
})(jQuery);