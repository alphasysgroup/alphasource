/**
* Name : pronto-wp-portal-admin-general-settings.js
* Description: This script is use for PWP-Portal Sync Settings Save handler
* LastUpdated : July 10, 2017
* @author Junjie Canonio
* @param  
* @return   
*/ 
jQuery(document).ready(function($){
	console.log('WFC - Portal General Settings');	
	$('a').css('outline', 'none');
	$('#WPPortal-DeleteUser-api-endpoint-container').css('display', 'none');


	/*
	* Limit the schedule dropdown --------------------------------------
	*/
	if($('#WPPortalSync-time-schedule').length == 1){
		if($('#Pronto_WP_Portal_getContactCount').length == 1){
			var ContactCountStatus = $('#Pronto_WP_Portal_getContactCount').attr('Status');
			var ContactCount 	   = $('#Pronto_WP_Portal_getContactCount').attr('Count');
			var ContactCountMSG    = $('#Pronto_WP_Portal_getContactCount').attr('MSG');
			if(ContactCountStatus == 'Success'){
				
				if( ContactCount > 1000 ){
					$('select[id="WPPortalSync-time-schedule"] option').each(function() {
					    var $thisOption = $(this);
					    if($thisOption.val() == 'min_10') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Ten Minutes - Your Salesforce organization exceeds the capacity to sync the data in 10 minutes.");
					    }
					});
				}
				if( ContactCount > 5000 ){
					$('select[id="WPPortalSync-time-schedule"] option').each(function() {
					    var $thisOption = $(this);
					    if($thisOption.val() == 'min_10') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Ten Minutes - Your Salesforce organization exceeds the capacity to sync the data in 10 minutes.");
					    }
					    if($thisOption.val() == 'min_30') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Thirty Minutes - Your Salesforce organization exceeds the capacity to sync the data in 30 minutes.");
					    }
					});
				}
				if( ContactCount > 10000 ){
					$('select[id="WPPortalSync-time-schedule"] option').each(function() {
					    var $thisOption = $(this);
					    if($thisOption.val() == 'min_10') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Ten Minutes - Your Salesforce organization exceeds the capacity to sync the data in 10 minutes.");
					    }
					    if($thisOption.val() == 'min_30') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Thirty Minutes - Your Salesforce organization exceeds the capacity to sync the data in 30 minutes.");
					    }
					    if($thisOption.val() == 'hourly') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Hourly - Your Salesforce organization exceeds the capacity to sync the data in 1 hour.");
					    }			    
					});						
				}
				if( ContactCount > 20000 ){
					$('select[id="WPPortalSync-time-schedule"] option').each(function() {
					    var $thisOption = $(this);
					    if($thisOption.val() == 'min_10') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Ten Minutes - Your Salesforce organization exceeds the capacity to sync the data in 10 minutes.");
					    }
					    if($thisOption.val() == 'min_30') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Thirty Minutes - Your Salesforce organization exceeds the capacity to sync the data in 30 minutes.");
					    }
					    if($thisOption.val() == 'hourly') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Hourly - Your Salesforce organization exceeds the capacity to sync the data in 1 hour.");
					    }
					    if($thisOption.val() == '3hours') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Three Hours - Your Salesforce organization exceeds the capacity to sync the data in 3 hours.");
					    }			    
					});						
				}
				if( ContactCount > 50000 ){
					$('select[id="WPPortalSync-time-schedule"] option').each(function() {
					    var $thisOption = $(this);
					    if($thisOption.val() == 'min_10') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Ten Minutes - Your Salesforce organization exceeds the capacity to sync the data in 10 minutes.");
					    }
					    if($thisOption.val() == 'min_30') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Thirty Minutes - Your Salesforce organization exceeds the capacity to sync the data in 30 minutes.");
					    }
					    if($thisOption.val() == 'hourly') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Hourly - Your Salesforce organization exceeds the capacity to sync the data in 1 hour.");
					    }
					    if($thisOption.val() == '3hours') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Three Hours - Your Salesforce organization exceeds the capacity to sync the data in 3 hours.");
					    }
					    if($thisOption.val() == '6hours') {
							$thisOption.attr("disabled","disabled");
							$thisOption.removeAttr("selected");
							$thisOption.text("Every Six Hours - Your Salesforce organization exceeds the capacity to sync the data in 6 hours.");
					    }				    
					});	
				}
			}else{
				$('select[id="WPPortalSync-time-schedule"] option').each(function() {
				    var $thisOption = $(this);
				    if($thisOption.val() == 'min_10') {
						$thisOption.attr("disabled","disabled");
						$thisOption.removeAttr("selected");
						$thisOption.text("Every Ten Minutes - Your Salesforce organization exceeds the capacity to sync the data in 10 minutes.");
				    }
				    if($thisOption.val() == 'min_30') {
						$thisOption.attr("disabled","disabled");
						$thisOption.removeAttr("selected");
						$thisOption.text("Every Thirty Minutes - Your Salesforce organization exceeds the capacity to sync the data in 30 minutes.");
				    }
				    if($thisOption.val() == 'hourly') {
						$thisOption.attr("disabled","disabled");
						$thisOption.removeAttr("selected");
						$thisOption.text("Hourly - Your Salesforce organization exceeds the capacity to sync the data in 1 hour.");
				    }
				    if($thisOption.val() == '3hours') {
						$thisOption.attr("disabled","disabled");
						$thisOption.removeAttr("selected");
						$thisOption.text("Every Three Hours - Your Salesforce organization exceeds the capacity to sync the data in 3 hours.");
				    }
				    if($thisOption.val() == '6hours') {
						$thisOption.attr("disabled","disabled");
						$thisOption.removeAttr("selected");
						$thisOption.text("Every Six Hours - Your Salesforce organization exceeds the capacity to sync the data in 6 hours.");
				    }				    
				});				
			}
		}
	}



	/*
	* Sync Settings --------------------------------------
	*/
	$("#WPPortalSync_save_schedule").click(function(){

		var connection = $(this);
		connection.after( '<span class="spinner"></span>' );
		$('.spinner').css({
			'margin-top': '28px',
			'visibility' : 'visible',
			'position': 'absolute',
			'background-color': '#ffffff'
		});

		$("#WPPortalSync_save_schedule").attr('disabled','disabled');
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving Schedule.</div>',null,'info');
		$.ajax({
			url:  wfc_portal_generalsettings_param.url,
	        type:'POST',
		    dataType:'json',
	        data: {
	            'action': 'wfc_portal_generalsettings_Save',
	            nonce: wfc_portal_generalsettings_param.nonce,
		        dataform : $('#WPPortalSync-syn-settings-form').serializeArray(),
	        },
		    success:function(resp){
				console.log(resp);
				var STATUS        = resp['STATUS'];
				var sf_response   = resp['sf_response'];
				if( STATUS == 'Fail' ){
					if( sf_response == 0 ){
						SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Network Error</div>',3000,'error');
					}else{
						var message   = resp['SF_RESPONCE']['sf_response']['message'];
						SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+message+'</div>',3000,'error');
					}
				}else{
					if( STATUS == 'Warn' ){			
						SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Get existing settings SF ID.</div>',3000,'warning');
					}else{
						SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Successful</div>',3000,'success');
					}
				}

				$(".spinner").remove();
				$('#WPPortalSync_save_schedule').removeAttr('disabled');
		    },
		    error: function(response) {
		    	console.log(response);
		    	$('#WPPortalSync_save_schedule').removeAttr('disabled');
		    	SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
			}
		});
	});
});