/**
* Name : pronto-wp-portal-admin-pages.js
* Description: This script is use for PWP-Portal Save Pages handler
* LastUpdated : July 11, 2017
* @author Junjie Canonio
* @param  
* @return   
*/ 
jQuery(document).ready(function($){
	console.log('WFC - Portal Pages');
 	$('#WPPortalSync_save_pages').on('click',function(){
		var connection = $("#WPPortalSync_save_pages");
		connection.after( '<span class="spinner"></span>' );
		$('.spinner').css({
			'margin-top': '28px',
			'visibility' : 'visible',
			'position': 'absolute',
			'background-color': '#ffffff'
		});

		$("#WPPortalSync_save_pages").attr('disabled','disabled');
		SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving Pages.</div>',null,'info');
		$.ajax({
			url:  wfc_portal_pages_param.url,
	        type:'POST',
		    dataType:'json',
	        data: {
	            'action': 'pages_settings',
	            nonce: wfc_portal_pages_param.nonce,
		        dataform : $('#pagesform').serializeArray(),
	        },
		    success:function(resp){
				console.log(resp);
				var STATUS        = resp['STATUS'];
				if( STATUS == 'success' ){
					SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Successful</div>',3000,'success');
				}else{
					SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
				}

				$(".spinner").remove();
				$('#WPPortalSync_save_pages').removeAttr('disabled');
		    },
		    error: function(response) {
		    	console.log(response);
		    	$('#WPPortalSync_save_schedule').removeAttr('disabled');
		    	SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error');
			}
		});

	});	
});