<?php
$pages = get_pages();
$pagesArray = array();
foreach ($pages as $key => $page_data) {
    $pagesArray[$key]['title'] = $page_data->post_title;
    $pagesArray[$key]['value'] = $page_data->ID;
}


$pagesArray_login = array(
    array(
        'title' => 'WordPress Login Default Page',
        'value' => 'wordpress-login-default-page'
    )
);

$pagesArray_register = array(
    array(
        'title' => 'WordPress Register User Default Page',
        'value' => 'wordpress-register-user-default-page'
    )
);

$pagesArray_forgot_pass = array(
    array(
        'title' => 'WordPress Lost Password Default Page',
        'value' => 'wordpress-lost-password-default-page'
    )
);

$pagesArray_reset_pass = array(
    array(
        'title' => 'WordPress Reset Password Default Page',
        'value' => 'wordpress-reset-password-default-page'
    )
);

$pageSettings = get_option('pup_pages_settings');
$wfc_syncer_pages = array(
    'pagesform' => array(
        'type' => 'settingsform',
        'settings' => array(
            'id'        => 'pagesform',
            'class'     => 'pagesform',
            'action'    => '',
            'name'      => 'pagesform',
            'style'     => '',
            'title'     => '',
            'method'    => 'post',
            'wp_option' => array(
                'enable'     => false,
                'slug_name'  => '',
            ),
            'submit_id' => '',
        ),
        'children' => array(
            'login_id' => array(
                'type' => 'select',
                'settings' => array(
                    'id' => 'login_id',
                    'name' => 'login_id',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Login'
                    ),
                    'options' => array_merge($pagesArray_login,$pagesArray),
                    'selected' => $pageSettings[0]['value'],
                )
            ),
            'register_id' => array(
                'type' => 'select',
                'settings' => array(
                    'id' => 'register_id',
                    'name' => 'register_id',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Registration'
                    ),
                    'options' => array_merge($pagesArray_register,$pagesArray),
                    'selected' => $pageSettings[1]['value'],
                )
            ),
            'forgot_id' => array(
                'type' => 'select',
                'settings' => array(
                    'id' => 'forgot_id',
                    'name' => 'forgot_id',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Forgot Password'
                    ),
                    'options' => array_merge($pagesArray_forgot_pass,$pagesArray),
                    'selected' => $pageSettings[2]['value'],
                )
            ),
            'reset_id' => array(
                'type' => 'select',
                'settings' => array(
                    'id' => 'reset_id',
                    'name' => 'reset_id',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Reset Password'
                    ),
                    'options' => array_merge($pagesArray_reset_pass,$pagesArray),
                    'selected' => $pageSettings[3]['value'],
                )
            ),
            'details_id' => array(
                'type' => 'select',
                'settings' => array(
                    'id' => 'details_id',
                    'name' => 'details_id',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Profile Details'
                    ),
                    'options' => $pagesArray,
                    'selected' => $pageSettings[4]['value'],
                )
            ),
            'edit_id' => array(
                'type' => 'select',
                'settings' => array(
                    'id' => 'edit_id',
                    'name' => 'edit_id',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Profile Edit'
                    ),
                    'options' => $pagesArray,
                    'selected' => $pageSettings[5]['value'],
                )
            ),
            'WPPortal-back_button' => array(
                'type' => 'button', 
                'settings' => array(
                    'id' => 'WPPortal-back_button',
                    'style' => 'position:relative;float:left;margin-top:20px;margin-left:20px;',
                    'label' => array(
                        'hidden' => true
                    ),
                    'content' => 'BACK'
                )
            ),
            'WPPortalSync_save_pages' =>  array(
                'type' => 'button',
                'settings' => array(
                    'id' => 'WPPortalSync_save_pages',
                    'name' => 'WPPortalSync_save_pages',
                    'gridclass'=> 'col-1',
                    'style' => 'float:left;margin-top:20px;',
                    'content' => 'Save',
                    'label' => array(
                        'hidden' => true
                    )
                )
            ), 
        )
    )
);
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');

$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
    $wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
    $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
    $wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
    $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
    $wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
    $wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff';

$wfc_portal_colorschemeform = array(
    'wfc_portal-colorscheme-form' => array(
        'type' => 'settingsform',
        'settings' => array(
            'id'        => 'wfc_portal-colorscheme-form',
            'class'     => 'wfc_portal-colorscheme-form',
            'action'    => '',
            'name'      => 'wfc_portal-colorscheme-form',
            'style'     => '',
            'title'     => '',
            'method'    => 'post',
            'wp_option' => array(
                'enable'     => true,
                'slug_name'  => 'wfc_portal_colorscheme',
            ),
            'submit_id' => 'wfc_portal_savecolorscheme',
        ),
        'children' => array(
            'wfc_portal-theme-color' => array(
                'type' => 'color-picker',
                'settings' => array(
                    'id' => 'wfc_portal-theme-color',
                    'name' => 'wfc_portal-theme-color',
                    'gridclass' => 'col-md-3',
                    'label' => array(
                        'value' => 'Theme Color'
                    ),
                    'description' => 'This will be the main theme color',
                    'color' => $wfc_portalThemeColor
                )
            ),
            'wfc_portal-theme-font-color' => array(
                'type' => 'color-picker',
                'settings' => array(
                    'id' => 'wfc_portal-theme-font-color',
                    'name' => 'wfc_portal-theme-font-color',
                    'gridclass' => 'col-md-3',
                    'label' => array(
                        'value' => 'Theme Font Color'
                    ),
                    'description' => 'This will be the main font color',
                    'color' => $wfc_portalThemeFontColor
                )
            ),
            'wfc_portal-sub-theme-color' => array(
                'type' => 'color-picker',
                'settings' => array(
                    'id' => 'wfc_portal-sub-theme-color',
                    'name' => 'wfc_portal-sub-theme-color',
                    'gridclass' => 'col-md-3',
                    'label' => array(
                        'value' => 'Sub-theme Color'
                    ),
                    'description' => 'This will be the sub theme color',
                    'color' => $wfc_portalSubThemeColor
                )
            ),
            'wfc_portal-sub-theme-font-color' => array(
                'type' => 'color-picker',
                'settings' => array(
                    'id' => 'wfc_portal-sub-theme-font-color',
                    'name' => 'wfc_portal-sub-theme-font-color',
                    'gridclass' => 'col-md-3',
                    'label' => array(
                        'value' => 'Sub-theme  Font Color'
                    ),
                    'description' => 'This will be the sub theme font color',
                    'color' => $wfc_portalSubThemeFontColor
                )
            ),
            'wfc_portal-button-color' => array(
                'type' => 'color-picker',
                'settings' => array(
                    'id' => 'wfc_portal-button-color',
                    'name' => 'wfc_portal-button-color',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Button Color'
                    ),
                    'description' => 'This will be applied for buttons, as well as checkboxes and radio buttons',
                    'color' => $wfc_portalButtonColor
                )
            ),
            'wfc_portal-button-font-color' => array(
                'type' => 'color-picker',
                'settings' => array(
                    'id' => 'wfc_portal-button-font-color',
                    'name' => 'wfc_portal-button-font-color',
                    'gridclass' => 'col-md-6',
                    'label' => array(
                        'value' => 'Button Font Color'
                    ),
                    'description' => 'This will be the font color for buttons, checkboxes, and radio buttons',
                    'color' => $wfc_portalButtonFontColor
                )
            ),            
            'WPPortal-back_button2' => array(
                'type' => 'button', 
                'settings' => array(
                    'id' => 'WPPortal-back_button2',
                    'style' => 'position:relative;float:left;margin-top:20px;margin-left:20px;',
                    'label' => array(
                        'hidden' => true
                    ),
                    'content' => 'BACK'
                )
            ),
            'wfc_portal_savecolorscheme' =>  array(
                'type' => 'button',
                'settings' => array(
                    'id' => 'wfc_portal_savecolorscheme',
                    'name' => 'wfc_portal_savecolorscheme',
                    'gridclass'=> 'col-1',
                    'style' => 'float:left;margin-top:20px;',
                    'content' => 'Save',
                    'label' => array(
                        'hidden' => true
                    )
                )
            ), 
        )
    )
);

$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');

$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'on';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';

$toolkit_instance = Saiyan_Toolkit::get_instance();

$toolkits = scandir($toolkit_instance->get_spinner_svg_dir());
$select_toolkits = array();
foreach ($toolkits as $toolkit) {
	if ($toolkit !== '.' && $toolkit !== '..') {
		$svg_type = preg_replace('/^(loading_)/', '', $toolkit);
		$svg_type = preg_replace('/(\.svg)?/', '', $svg_type);

		array_push($select_toolkits, array(
			'title' => ucwords(preg_replace('/_/', ' ', $svg_type)),
			'value' => $svg_type
		));
	}
}

$wfc_portal_loadingoptions_form = array(
	'wfc_portal-templateoptions-form' => array(
        'type' => 'settingsform',
        'settings' => array(
	        'id'        => 'wfc_portal-templateoptions-form',
	        'class'     => 'wfc_portal-templateoptions-form',
	        'action'    => '',
	        'name'      => 'wfc_portal-templateoptions-form',
	        'style'     => '',
	        'title'     => '',
	        'method'    => 'post',
	        'wp_option' => array(
		        'enable'     => true,
		        'slug_name'  => 'wfc_portal_loadingoptions',
	        ),
	        'submit_id' => 'wfc_portal-templateoptions',
        ),
        'children' => array(
	        'wfc_portal-loading-enable' => array(
		        'type' => 'switch',
		        'settings' => array(
			        'id' => 'wfc_portal-loading-enable',
			        'name' => 'wfc_portal-loading-enable',
			        'class' => 'wfc_portal-loading-enable',
			        'gridclass' => 'col-md-6',
			        'label' => array(
				        'value' => 'Enable Loader'
			        ),
			        'description' => 'Enable loader during page redirection',
                    'switch' => $wfc_portalLoadingEnable === 'true' ? 'on' : 'off'
		        )
	        ),

	        'separator_1' => array(
		        'type' => 'separator',
		        'settings' => array(
			        'gridclass' => 'col-md-12',
			        'visibility' => 'hidden'
		        )
	        ),

	        'wfc_portal-loading-spinner' => array(
		        'type' => 'select',
		        'settings' => array(
			        'id' => 'wfc_portal-loading-spinner',
			        'name' => 'wfc_portal-loading-spinner',
			        'gridclass' => 'col-md-6',
			        'label' => array(
				        'value' => 'Loading Spinner'
			        ),
			        'description' => 'This will be displayed on any page that has a loading feature',
			        'options' => $select_toolkits,
			        'selected' => $wfc_portalLoadingSpinner
		        )
	        ),
	        'wfc_portal-loading-spinner-preview' => array(
		        'type' => 'html',
		        'settings' => array(
			        'id' => 'wfc_portal-loading-spinner-preview',
			        'gridclass' => 'col-md-6',
			        'label' => array(
				        'value' => 'Spinner Preview'
			        )
		        ),
		        'content' =>
			        '<img id="wfc_portal-spinner-preview" height="70px" style="background-color: rgba(0, 0, 0, 0.15);" src="' . $toolkit_instance->get_spinner_svg_url($wfc_portalLoadingSpinner) . '" />'
	        ),

	        'wfc_portal-loading-tinter-color' => array(
		        'type' => 'color-picker',
		        'settings' => array(
			        'id' => 'wfc_portal-loading-tinter-color',
			        'name' => 'wfc_portal-loading-tinter-color',
			        'class' => 'wfc_portal-loading-tinter-color',
			        'gridclass' => 'col-md-6',
			        'label' => array(
				        'value' => 'Tinter Color'
			        ),
			        'description' => 'This will be applied for the loading page with tinter. Opacity will be added automatically',
			        'color' => $wfc_portalLoadingTinterColor
		        )
	        ),

	        'separator_2' => array(
                'type' => 'separator',
                'settings' => array(
                    'gridclass' => 'col-md-12',
                    'visibility' => 'hidden'
                )
            ),

	        'WPPortal-back_button3' => array(
		        'type' => 'button',
		        'settings' => array(
			        'id' => 'WPPortal-back_button3',
			        'style' => 'position:relative;float:left;margin-top:20px;margin-left:20px;',
			        'label' => array(
				        'hidden' => true
			        ),
			        'content' => 'BACK'
		        )
	        ),
	        'wfc_portal-templateoptions' =>  array(
		        'type' => 'button',
		        'settings' => array(
			        'id' => 'wfc_portal-templateoptions',
			        'name' => 'wfc_portal-templateoptions',
			        'gridclass'=> 'col-1',
			        'style' => 'float:left;margin-top:20px;',
			        'content' => 'Save',
			        'label' => array(
				        'hidden' => true
			        )
		        )
	        ),
        )
    )
);

$accordion = array(
    'wfc_base' => array(
        'type' => 'accordion',
        'settings' => array(
            'id' => 'wfc_base',
            // 'gridclass' => 'col-md-12 col-lg-12',
            'gridclass' => '',
        ),
        'sections' => array(
            array(
                'title' => 'Page Redirections',
                'description' => 'Set up the pages for the redirection functionality of the WFC- Portal.',
                'content' => $wfc_syncer_pages,
            ),
            array(
                'title' => 'Color Scheme',
                'description' => 'Configure color scheme to match your theme.',
                'content' => $wfc_portal_colorschemeform,
            ),
            array(
                'title' => 'Loading Options',
                'description' => 'Configure the view of your loading page.',
                'content' => $wfc_portal_loadingoptions_form
            )
        )
    ),
);
?>
<div class="wfc_portal_pages_main_container">
    <h2><?php echo __('Page redirection and Template Options', 'pu-portal'); ?></h2>
    <?php Saiyan::tist()->render_module_mapping($accordion);?>
    <script>
        jQuery('#WPPortal-back_button, #WPPortal-back_button2, #WPPortal-back_button3').on('click', function() {
            window.location.replace("<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal'; ?>");
        });

        jQuery('#wfc_portal-loading-spinner').on('change', function() {
            let svgPath = '<?php echo $toolkit_instance->get_spinner_svg_url(); ?>';

            jQuery('#wfc_portal-spinner-preview').attr('src', svgPath + 'loading_' + jQuery(this).val() + '.svg');
        });
    </script>
</div>
<?php
