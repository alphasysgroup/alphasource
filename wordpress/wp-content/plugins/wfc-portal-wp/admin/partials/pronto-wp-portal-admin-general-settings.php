<?php

$WPPortalSyncSettings          = get_option( 'WPPortalSyncSettings', true );
$WPPortalSyncEnableDisable     = isset( $WPPortalSyncSettings['WPPortalSync-enable-sf-sync'] ) ? $WPPortalSyncSettings['WPPortalSync-enable-sf-sync'] : 'false';
$WPPortalSyncEnableDisable 	   = ($WPPortalSyncEnableDisable == 'true') ? 'on' : 'off';
$WPPortalSyncTimeSchedule      = isset( $WPPortalSyncSettings['WPPortalSync-time-schedule'] ) ? $WPPortalSyncSettings['WPPortalSync-time-schedule'] : 'min_10';   
$WPPortalSyncAPIEndpoint       = get_site_url().'/wp-json/prontoultimate/v1/portalcontact';
$WPPortalDeleteUserAPIEndpoint = get_site_url().'/wp-json/prontoultimate/v1/portalcontactdelete';

$ContactCount = $this->Pronto_WP_Portal_getContactCount();
$ContactCountStatus = isset( $ContactCount['Status'] ) ? $ContactCount['Status'] : 'Failed';
$ContactCountCount  = isset( $ContactCount['Count'] ) ? $ContactCount['Count'] : 0 ;
$ContactCountMSG    = isset( $ContactCount['MSG'] ) ? $ContactCount['MSG'] : 'Fail';

$WPPortalSyncEnableDeltaSyncing = isset($WPPortalSyncSettings['WPPortalSync-enable-delta-syncing']) ? $WPPortalSyncSettings['WPPortalSync-enable-delta-syncing'] : 'false';
$WPPortalSyncEnableDeltaSyncing = ($WPPortalSyncEnableDeltaSyncing == 'true') ? 'on' : 'off';
$WPPortal_sync_record_modified_datefrom = isset($WPPortalSyncSettings['WPPortalSync-sync-record-modified-datefrom']) ? $WPPortalSyncSettings['WPPortalSync-sync-record-modified-datefrom'] : '';
$WPPortal_sync_record_modified_datefrom_timestamp = (isset($WPPortalSyncSettings['WPPortalSync-sync-record-modified-datefrom-timestamp']) && $WPPortalSyncSettings['WPPortalSync-sync-record-modified-datefrom-timestamp'] != 0) ? (Int)$WPPortalSyncSettings['WPPortalSync-sync-record-modified-datefrom-timestamp'] : '';
$WPPortal_sync_record_modified_dateto = isset($WPPortalSyncSettings['WPPortalSync-sync-record-modified-dateto']) ? $WPPortalSyncSettings['WPPortalSync-sync-record-modified-dateto'] : '';
$WPPortal_sync_record_modified_dateto_timestamp = (isset($WPPortalSyncSettings['WPPortalSync-sync-record-modified-dateto-timestamp']) && $WPPortalSyncSettings['WPPortalSync-sync-record-modified-dateto-timestamp'] != 0) ? (Int)$WPPortalSyncSettings['WPPortalSync-sync-record-modified-dateto-timestamp'] : '';

?>
<input id="Pronto_WP_Portal_getContactCount"
       Status="<?php echo $ContactCountStatus;?>"
       Count="<?php echo $ContactCountCount;?>"
       MSG="<?php echo $ContactCountMSG;?>" style="display:none;"/>
<?php
$wfc_syncer_generalsettings = array(
    'WPPortalSync-syn-settings-form' => array(
    	'type' => 'settingsform',
		'settings' => array(
			'id'        => 'WPPortalSync-syn-settings-form',
			'class'     => 'WPPortalSync-syn-settings-form',
			'action'    => '',
			'name'      => 'WPPortalSync-syn-settings-form',
			'style'     => '',
			'title'     => '',
			'method'    => 'post',
			'wp_option' => array(
				'enable'     => false,
				'slug_name'  => '',
			),
			'submit_id' => '',
		),
        'children' => array(
	        'WPPortalSync-enable-sf-sync' => array(
				'type' => 'switch',
				'settings' => array(
					'id'       => 'WPPortalSync-enable-sf-sync',
					'class'    => 'WPPortalSync-enable-sf-sync',
					'name'     => 'WPPortalSync-enable-sf-sync',
		            'switch'   => $WPPortalSyncEnableDisable,
		            'gridclass' => 'col-md-6 col-lg-6',
		            'required' => false,
		            'readonly' => false,
		            'label'         => array(
						'hidden'    => false,
						'value'     => 'Enable/Disable Sync',
						'position'  => 'top',
					),
					'description' => ''
				)
			),
	        'WPPortalSync-time-schedule' => array(
		        'type' => 'select',
		        'settings' => array(
			        'id' => 'WPPortalSync-time-schedule',
			        'name' => 'WPPortalSync-time-schedule',
			        'gridclass' => 'col-md-6',
			        'label' => array(
				        'value' => 'Select Time Schedule'
			        ),
			        'options' => array(
				        array(
					        'title' => 'Every Ten Minutes',
					        'value' => 'min_10'
				        ),
				        array(
					        'title' => 'Every Thirty Minutes',
					        'value' => 'min_30'
				        ),
				        array(
					        'title' => 'Hourly',
					        'value' => 'hourly'
				        ),
				        array(
					        'title' => 'Every Three Hours',
					        'value' => '3hours'
				        ),
				        array(
					        'title' => 'Every Six Hours',
					        'value' => '6hours'
				        ),
				        array(
					        'title' => 'Daily',
					        'value' => '1day'
				        )
			        ),
			        'selected' => $WPPortalSyncTimeSchedule
		        )
	        ),
	        'WPPortalSync-resync-api-endpoint' => array(
		        'type' => 'input',
		        'settings' => array(
			        'id' => 'WPPortalSync-resync-api-endpoint',
			        'name' => 'WPPortalSync-resync-api-endpoint',
			        'class' => 'WPPortalSync-resync-api-endpoint',
			        'gridclass' => 'col-md-12 col-lg-12',
			        'type' => 'text',
			        'readonly' => true,
			        'label' => array(
				        'value' => 'WP Portal API Endpoint'
			        ),
			        'value' => $WPPortalSyncAPIEndpoint
		        )
	        ),
	        'WPPortal-DeleteUser-api-endpoint' => array(
		        'type' => 'input',
		        'settings' => array(
			        'id' => 'WPPortal-DeleteUser-api-endpoint',
			        'name' => 'WPPortal-DeleteUser-api-endpoint',
			        'class' => 'WPPortal-DeleteUser-api-endpoint',
			        'gridclass' => 'col-md-12 col-lg-12',
			        'type' => 'text',
			        'readonly' => true,
			        'label' => array(
				        'value' => 'WP Portal Delete User API Endpoint'
			        ),
			        'value' => $WPPortalDeleteUserAPIEndpoint
		        )
	        ),
	        'WPPortalSync-deltasyncheadlabel' => array(
		        'type' => 'html',
		        'settings' => array(
			        'id' => 'html1',
			        'name' => '',
			        'gridclass' => 'col-md-12 col-lg-12',
			        'label'         => array(
				        'hidden'    => true,
				        'value'     => '',
				        'position'  => 'top',
			        ),

			        'description' => ''
		        ),
		        'content' => '<div style="background-color: #1497c1;padding: 5px;"><span style="font-weight: 700; color: #fff;width: 100%; display: block; text-align: center;">Delta Syncing Options</span></div>
                            <div style=" padding: 10px;"><span style="color: #999;"><b>Note : </b> The Delta syncing functionality is focused more on the last modified date of a record and we want to use the vanilla/standard syncing functionality of WFC Portal, all we have to do is to set disable the <span style="color: #ff000075;">Enable/Disable Sync Records Modified Within 24 Hours</span> and leave both <span style="color: #ff000075;">Sync records modified on date set and beyond</span> and <span style="color: #ff000075;">Sync records modified on date set and before</span> option as blank.</span></div>',
	        ),
	        'WPPortalSync-enable-delta-syncing' => array(
		        'type' => 'switch',
		        'settings' => array(
			        'id'       => 'WPPortalSync-enable-delta-syncing',
			        'class'    => 'WPPortalSync-enable-delta-syncing',
			        'name'     => 'WPPortalSync-enable-delta-syncing',
			        'switch'   => $WPPortalSyncEnableDeltaSyncing,
			        'gridclass' => 'col-md-12 col-lg-12',
			        'required' => false,
			        'readonly' => false,
			        'label'         => array(
				        'hidden'    => false,
				        'value'     => 'Enable/Disable Sync Records Modified Within 24 Hours',
				        'position'  => 'top',
			        ),
			        'description' => 'Note: This option will force WFC Portal to only sync records that have been modified within the past 24 hours.'
		        )
	        ),
	        'WPPortalSync-sync-record-modified-datefrom' => array(
		        'type' => 'date-picker',
		        'settings' => array(
			        'id' => 'WPPortalSync-sync-record-modified-datefrom',
			        'class' => 'WPPortalSync-sync-record-modified-datefrom',
			        'name' => 'WPPortalSync-sync-record-modified-datefrom',
			        'gridclass' => 'col-md-6 col-lg-6',
			        'label' => array(
				        'hidden' => false,
				        'value' => 'Sync records modified on date set and beyond',
				        'position' => 'top'
			        ),
			        'value' => is_integer($WPPortal_sync_record_modified_datefrom_timestamp) ? gmdate("Y-m-d", $WPPortal_sync_record_modified_datefrom_timestamp) : '',
			        'description' => '<b>Note : </b> Set the a <b>(DATE FROM)</b> date range to sync the records that have been modified on the date set and beyond.',
			        'dependency' => array(
				        'id' => 'WPPortalSync-enable-delta-syncing',
				        'trigger_value' => 'on',
				        'trigger_action' => 'show_hide',
			        )
		        )
	        ),
	        'WPPortalSync-sync-record-modified-dateto' => array(
		        'type' => 'date-picker',
		        'settings' => array(
			        'id' => 'WPPortalSync-sync-record-modified-dateto',
			        'class' => 'WPPortalSync-sync-record-modified-dateto',
			        'name' => 'WPPortalSync-sync-record-modified-dateto',
			        'gridclass' => 'col-md-6 col-lg-6',
			        'label' => array(
				        'hidden' => false,
				        'value' => 'Sync records modified on date set and before',
				        'position' => 'top'
			        ),
			        'value' => is_integer($WPPortal_sync_record_modified_dateto_timestamp) ? gmdate("Y-m-d", $WPPortal_sync_record_modified_dateto_timestamp) : '',
			        'description' => '<b>Note : </b> Set the a <b>(DATE TO)</b> date range to sync the records that have been modified on the date set and before.',
			        'dependency' => array(
				        'id' => 'WPPortalSync-enable-delta-syncing',
				        'trigger_value' => 'on',
				        'trigger_action' => 'show_hide',
			        )
		        )
	        ),
	        'WPPortal-back_button' => array(
		        'type' => 'button',
		        'settings' => array(
			        'id' => 'WPPortal-back_button',
			        'gridclass'=> 'col-1',
			        'style' => 'float:left;margin-top:20px;',
			        'label' => array(
				        'hidden' => true
			        ),
			        'content' => 'BACK'
		        )
	        ),
	        'WPPortalSync_save_schedule' =>  array(
		        'type' => 'button',
		        'settings' => array(
			        'id' => 'WPPortalSync_save_schedule',
			        'name' => 'WPPortalSync_save_schedule',
			        'style' => 'position:relative;float:left;margin-top:20px;margin-left:20px;',
			        'content' => 'Save',
			        'label' => array(
				        'hidden' => true
			        )
		        )
	        ),
        )
    )
);
?>
<div class="wfc_portal_general_settings_main_container">
	<h2><?php echo __('General Settings', 'pu-portal'); ?></h2>
	<?php Saiyan::tist()->render_module_mapping($wfc_syncer_generalsettings);?>
    <script>
        jQuery('#WPPortal-back_button').on('click', function() {
            window.location.href = '<?php echo get_admin_url() . 'admin.php?page=pronto-portal'; ?>';
        })
    </script>
</div>