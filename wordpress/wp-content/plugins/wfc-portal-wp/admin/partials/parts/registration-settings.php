<?php
$showgeneralsettings = (isset($_GET['dev']) && $_GET['dev'] == 1) ? 'display:block;' : 'display:none;';

$WPPortalSyncSettings          = get_option( 'WPPortalSyncSettings', true );
$WPPortalSyncEnableDisable     = isset( $WPPortalSyncSettings['WPPortalSync-enable-sf-sync'] ) ? $WPPortalSyncSettings['WPPortalSync-enable-sf-sync'] : 'false';
$WPPortalSyncTimeSchedule      = isset( $WPPortalSyncSettings['WPPortalSync-time-schedule'] ) ? $WPPortalSyncSettings['WPPortalSync-time-schedule'] : 'min_10';   
$WPPortalSyncAPIEndpoint       = get_site_url().'/wp-json/prontoultimate/v1/portalcontact';
$WPPortalDeleteUserAPIEndpoint = get_site_url().'/wp-json/prontoultimate/v1/portalcontactdelete';
$WPPortalSync_SFID = isset( $WPPortalSyncSettings['WPPortalSync-SFID'] ) ? $WPPortalSyncSettings['WPPortalSync-SFID'] : ''; 



$reg_settings = get_option('reg_settings');
$sf_fields =  get_option('sf_fields');

$field_opt = array();
if( ! empty( $meta_keys ) ) {
    foreach ( $meta_keys as $key ) {
		$field_opt[$key['meta_key']] = $key['meta_key'];
    }
}
ob_start();
?>
<div style="<?php echo $showgeneralsettings;?>">
	<input type="text" id="WPPortalSync-enable-sf-syn" value="<?php echo $WPPortalSyncEnableDisable; ?>">
	<input type="text" id="WPPortalSync-time-schedule" value="<?php echo $WPPortalSyncTimeSchedule; ?>">
	<input type="text" id="WPPortalSync-SFID" value="<?php echo $WPPortalSync_SFID; ?>">
	<input type="text" id="WPPortalSync-resync-api-endpoint" value="<?php echo $WPPortalSyncAPIEndpoint; ?>">
	<input type="text" id="WPPortal-DeleteUser-api-endpoint" value="<?php echo $WPPortalDeleteUserAPIEndpoint; ?>">
</div>
<div class="pup-modal-container" >
	<div class="pup-modal">
		<div class="pup-modal-head">
			<h3><?php echo __('Field Settings', 'pu-portal' ); ?></h3>
			<a class="pup-modal-close" ><span class="fa fa-close"></span></a>
		</div>
		<div class="pup-modal-body">
			<input type="hidden" class="target-element" value=''>

			<div class="form-group col s6" >
				<label for="field_label"><?php echo __('Field Label', 'pu-portal' ); ?></label>
				<input id="field_label" type="text" class="form-data" name="field_label" value="" />
			</div>
			
			<div class="form-group col s6">
				<label for="field_position"><?php echo __('Label Position', 'pu-portal' ); ?></label>
				<select id="field_position" name="label_position" class="form-data">
					<option value='top'><?php echo __('Top', 'pu-portal' ); ?></option>
					<option value='right'><?php echo __('Right', 'pu-portal' ); ?></option>
					<option value='bottom'><?php echo __('Bottom', 'pu-portal' ); ?></option>
					<option value='left'><?php echo __('Left', 'pu-portal' ); ?></option>
				</select>
			</div>
			
			<div class="form-group col s6" >
				<label for="placeholder"><?php echo __('Placeholder', 'pu-portal' ); ?></label>
				<input id="placeholder" type="text" class="form-data" name="placeholder" value="" />
			</div>
			
			<div class="form-group col s6" >
				<label for="default_value"><?php echo __('Default Value', 'pu-portal' ); ?></label>
				<input id="default_value" type="text" class="form-data" name="default_value" value="" />
			</div>
			
			<div class="form-group col s6" >
				<label for="css_classes"><?php echo __('CSS Class', 'pu-portal' ); ?></label>
				<input type="text" id="css_classes" name="css_classes" class="form-data" value=""/>
			</div>
			
			<div class="form-group col s6" >
				<label for="layout_size"><?php echo __('Grid Layout', 'pu-portal' ); ?></label>
				<select id="layout_size" name="layout_size" class="form-data">
					<option value='pu-one'><?php echo __('Full width', 'pu-portal' ); ?></option>
					<option value='pu-one-half'><?php echo __('One-half', 'pu-portal' ); ?></option>
					<option value='pu-one-third'><?php echo __('One-third', 'pu-portal' ); ?></option>
					<option value='pu-one-fourth'><?php echo __('One-fourth', 'pu-portal' ); ?></option>
					<option value='pu-one-sixth'><?php echo __('One-sixth', 'pu-portal' ); ?></option>
					<option value='pu-one-twelfth'><?php echo __('One-twelfth', 'pu-portal' ); ?></option>
				</select>
			</div>

			<div class="form-group col s12 hidden" >
				<label for="pup_duplicate_email"><?php echo __('Allow Duplicate Email', 'pu-portal' ); ?></label>
				<?php $pup_duplicate_email = get_option('pup_duplicate_email'); ?>
				<div class="pup-switcher-wrap pup-switcher-wrap-ade size-normal">
					<input type="radio" <?php echo ($pup_duplicate_email == '1') ? 'checked="checked"' : ''; ?> id="pup_duplicate_email-true" class="pup-input-switcher pup-input-switcher-true pup_duplicate_email" name="pup_duplicate_email" value="1" />
					<input type="radio" <?php echo ($pup_duplicate_email == '0' || $pup_duplicate_email != '1' ) ? 'checked="checked"' : ''; ?> id="pup_duplicate_email-false" class="radio-false pup-input-switcher pup-input-switcher-false pup_duplicate_email" name="pup_duplicate_email" value="0" />
					<label class="sw-enable">
						<span><?php echo __('On', 'pu-portal' ); ?></span>
					</label>
					<label class="sw-disable">
						<span><?php echo __('Off', 'pu-portal' ); ?></span>
					</label>
					<span class="state-marker"></span>
				</div>
			</div>

			<div class="form-group col s12" >
				<label for="required_field"><?php echo __('Required Field', 'pu-portal' ); ?></label>
				<div class="pup-switcher-wrap size-normal">
					<input type="radio" id="required_field-true" class="pup-input-switcher pup-input-switcher-true form-data" name="required_field" value="1" />
					<input type="radio" checked="checked" id="required_field-false" class="radio-false pup-input-switcher pup-input-switcher-false form-data" name="required_field" value="0" />
					<label class="sw-enable">
						<span><?php echo __('On', 'pu-portal' ); ?></span>
					</label>
					<label class="sw-disable">
						<span><?php echo __('Off', 'pu-portal' ); ?></span>
					</label>
					<span class="state-marker"></span>
				</div>
			</div>
			
			<div class="form-group col s12" >
				<label for="show_on_reg"><?php echo __('Show on Registration', 'pu-portal' ); ?></label>
				<div class="pup-switcher-wrap size-normal">
					<input type="radio" id="show_on_reg-true" class="pup-input-switcher pup-input-switcher-true form-data" name="show_on_reg" value="1" />
					<input type="radio" checked="checked" id="show_on_reg-false" class="radio-false pup-input-switcher pup-input-switcher-false form-data" name="show_on_reg" value="0" />
					<label class="sw-enable">
						<span><?php echo __('On', 'pu-portal' ); ?></span>
					</label>
					<label class="sw-disable">
						<span><?php echo __('Off', 'pu-portal' ); ?></span>
					</label>
					<span class="state-marker"></span>
				</div>
			</div>
			<div class="pup-modal-save col s12">
				<a class="btn pup-modal-save-ctrl"><?php echo __('SAVE', 'pu-portal' ); ?></a>
			</div>
		</div>
	</div>
</div>

<div class="registration-form-container">
	<div class="header-wrapper">
		<!-- <h2><?php // _e('Registration Settings', 'pu-portal'); ?></h2> -->
	</div>
    <ul id="registration-list" >
        <?php
	    if( ! empty( $reg_settings ) ) :
		$selected_sf = array();
		
		foreach( $reg_settings as $key => $val ) {
			$selected_sf[] = $val['salesforce_field']['label'];
		}
		?>
		<?php foreach( $reg_settings as $key => $val ) : ?>
            <?php $sel = ''; ?>
            <li class="ui-state-default">
                <input
                    type="hidden"
                    class="form-data form_settings"
                    name="form_settings"
                    value='<?php echo str_replace("'", "`", json_encode($val['form_settings'])); ?>'
                />
                <div class="form-group">
                    <label for="salesforce_field"><?php echo __('Salesforce Field', 'pu-portal' ); ?></label>
                    <select
                        id="salesforce_field"
                        name="salesforce_field"
                        class="form-data salesforce_field"
                        <?php echo ($val['salesforce_field']['name'] == "Email" || $val['salesforce_field']['name'] == "ASPU__WPUsername__c" || $val['salesforce_field']['name'] == "FirstName" || $val['salesforce_field']['name'] == "LastName" ) ? 'disabled' : ''; ?>>
                        <option value="">--<?php echo __('Select', 'pu-portal' ); ?>--</option>
                        <?php if ( ! empty( $sf_fields ) ) : foreach( $sf_fields as $sf_field_key => $sf_field_val ) : ?>
                            <option
                                <?php echo ( ( in_array( $sf_field_val['label'], $selected_sf ) ) && $sf_field_val['label'] != $val['salesforce_field']['label'] ) ? "disabled='disabled'" : ''; ?>
                                <?php
                                if( $sf_field_val['name'] == $val['salesforce_field']['name'] ) {
                                    echo "selected='selected' class='act'";
                                    $sel = $val['salesforce_field']['name'];
                                }
                                ?>
                                value='<?php echo str_replace("'", "`", json_encode($sf_fields[ $sf_field_key ])); ?>'
                            ><?php echo str_replace("'", "`", $sf_field_val['label']); ?></option>
                        <?php endforeach; endif;?>
                    </select>
                </div>

                <div class="sf-pointer form-group">
                    <span class="fa fa-arrow-right"></span>
                </div>

                <div class="form-group">
                    <label for="wp_user_meta"><?php echo __('Meta Field', 'pu-portal' ); ?></label>
                    <select
                        id="wp_user_meta"
                        name="wp_user_meta"
                        class="form-data"
                        <?php echo ($val['salesforce_field']['name'] == "Email" || $val['salesforce_field']['name'] == "ASPU__WPUsername__c" || $val['salesforce_field']['name'] == "FirstName" || $val['salesforce_field']['name'] == "LastName" ) ? 'disabled' : ''; ?>
                    >
                        <option value="">--<?php echo __('Select', 'pu-portal' ); ?>--</option>
                        <option style=" font-weight: 700; " class="new-user-meta" value="new_user_meta"><?php echo __('New', 'pu-portal' ); ?></option>
                        <?php foreach( $field_opt as $field_opt_key => $field_opt_val ) : ?>
                            <option <?php selected( ( $field_opt_key == $val['wp_user_meta'] ) ) ?> value="<?php echo $field_opt_key; ?>" ><?php echo $field_opt_val; ?></option>
                        <?php endforeach; ?>
                        <option
                            <?php echo ( $sel == 'Email' ||  $sel == 'ASPU__WPUsername__c' || $sel == 'FirstName' || $sel == 'LastName' ) ? 'selected="selected"' : '' ; ?>
                            value="<?php echo ( $sel == 'Email' ) ? "Email" : ( ( $sel == 'ASPU__WPUsername__c' ) ? "ASPU__WPUsername__c" : (($sel == 'FirstName') ? 'FirstName' : (($sel == 'LastName') ? 'LastName' : '' ))); ?>"
                        ><?php echo ( $sel == 'Email' ) ? "Email" : ( ( $sel == 'ASPU__WPUsername__c' ) ? "ASPU__WPUsername__c" : (($sel == 'FirstName') ? 'FirstName' : (($sel == 'LastName') ? 'LastName' : '' )) ); ?></option>
                    </select>
                </div>
                <div class="reg-set-ctrl form-group">
                    <a class="conf-reg-fld"><span class="fa fa-cog fa-2x"></span></a>
                    <a class="fa fa-close remove-reg-fld <?php echo ( $val['salesforce_field']['name'] == "Email" || $val['salesforce_field']['name'] == "ASPU__WPUsername__c" || $val['salesforce_field']['name'] == "FirstName" || $val['salesforce_field']['name'] == "LastName" ) ? 'hidden': ''; ?>"></a>
                </div>
            </li>
        <?php endforeach; ?>
	    <?php else: for( $i = 0; $i <= 1; $i++ ) : ?>
            <?php $has_username = 0; ?>
            <li class="ui-state-default">
                <input
                    type="hidden"
                    class="form-data form_settings"
                    name="form_settings"
                    value='[{"name":"field_label","value":"<?php echo ($i == 0) ? "Email" : (($i == 1) ? "WP Username" : (($i == 2) ? "FirstName" : "LastName")); ?>"},{"name":"label_position","value":"top"},{"name":"placeholder","value":""},{"name":"default_value","value":""},{"name":"css_classes","value":""},{"name":"layout_size","value":"pu-one"},{"name":"required_field","value":"1"},{"name":"show_on_reg","value":"1"}]'
                >
                <div class="form-group col s4">
                    <label for="salesforce_field"><?php echo __('Salesforce Field', 'pu-portal' ); ?></label>
                    <select id="salesforce_field" name="salesforce_field" class="form-data salesforce_field" disabled >
                        <option value="">--<?php echo __('Select', 'pu-portal' ); ?>--</option>
                        <?php if( ! empty( $sf_fields ) ) : foreach( $sf_fields as $sf_field_key => $sf_field_val ): ?>
                            <option
                                <?php echo ( ( $i == 1 && $sf_field_val['name'] == 'Email' ) || ( $i == 0 && $sf_field_val['name'] == 'ASPU__WPUsername__c' ) || ( $i == 3 && $sf_field_val['name'] == 'FirstName' ) || ( $i == 2 && $sf_field_val['name'] == 'FirstName' )) ? 'disabled="disabled"' : ''; ?>
                                <?php echo ( ( $i == 0 && $sf_field_val['name'] == 'Email' ) || ( $i == 1 && $sf_field_val['name'] == 'ASPU__WPUsername__c' ) || ( $i == 2 && $sf_field_val['name'] == 'FirstName' ) || ( $i == 3 && $sf_field_val['name'] == 'FirstName' ) ) ? 'selected="selected"' : ''; ?>
                                value='<?php echo str_replace("'", "`", json_encode($sf_fields[ $sf_field_key ])); ?>'
                            ><?php echo $sf_field_val['label']; ?></option>
                        <?php endforeach; endif; ?>
                    </select>
                </div>

                <div class="sf-pointer form-group col s2">
                    <span class="fa fa-arrow-right"></span>
                </div>

                <div class="form-group col s4">
                    <label for="wp_user_meta"><?php echo __('Meta Field', 'pu-portal' ); ?></label>
                    <select id="wp_user_meta" name="wp_user_meta" class="form-data"  disabled >
                        <option value="">--<?php echo __('Select', 'pu-portal' ); ?>--</option>
                        <option style=" font-weight: 700; " class="new-user-meta" value="new_user_meta"><?php echo __('New', 'pu-portal' ); ?></option>
                        <?php foreach( $field_opt as $field_opt_key => $field_opt_val ) : ?>
                            <option value="<?php echo $field_opt_key; ?>" ><?php echo $field_opt_val; ?></option>
                        <?php endforeach; ?>
                        <option selected="selected" value="<?php echo ( $i == 0 ) ? "Email" : "ASPU__WPUsername__c"; ?>"><?php echo ( $i == 0 ) ? "Email" : "WP Username"; ?></option>
                    </select>
                </div>
                <div class="reg-set-ctrl form-group col s2">
                    <a class="conf-reg-fld"><span class="fa fa-cog fa-2x"></span></a>
                    <a class="fa fa-close remove-reg-fld hidden"></a>
                </div>
            </li>
        <?php endfor; endif; ?>
    </ul>
	<div>
		<a class="btn add-reg-fld"><?php echo __('ADD', 'pu-portal' ); ?></a>
	</div>
	<div class="form-ctrl">
		<a class="btn save-reg-fld"><?php echo __('SAVE', 'pu-portal' ); ?></a>
        <a class="btn save-resync-reg-fld"><?php echo __('SAVE & RESYNC', 'pu-portal' ); ?></a>
        <a id="WPPortal-back_button" href="<?php echo get_admin_url() . 'admin.php?page=pronto-portal'; ?>" class="btn back"><?php echo __('BACK', 'pu-portal' ); ?></a>
	</div>
</div>
<script>
(function ($) {
    $(document).ready(function(){

    	var registerSettings = {
			init: function(obj) {
				this.var = obj;
				this.run();
			},
    		run : function() {
				this.var.listParent.sortable({items: "> li"});
			
				this.var.body.on('click', '.remove-reg-fld', this.removeList);
				this.var.body.on('click', '.conf-reg-fld', this.showFormSetting);
				this.var.body.on('change', '#salesforce_field', this.setFormSettingFldLbl);

				$('.add-reg-fld').click(this.addList);
				$('.save-reg-fld').click(this.saveList);
				$('.save-resync-reg-fld').click(this.syncListData);
				
				this.displayMessage();
    		}, 
    		displayMessage: function() {
				if ( (typeof(localStorage.regSaved) !== "undefined") ) {
					var pRegSaved = this.parseValidJSON(localStorage.regSaved);
					if(pRegSaved.status === 1){
						// CherryJsCore.cherryHandlerUtils.noticeCreate( pRegSaved.type, pRegSaved.message );
					}
					$('.pup_profile_wrapper .btn.save-prof-fld').trigger('click');
					/* localStorage.removeItem('regSaved'); */
				}
				if ( (typeof(localStorage.regSynced) !== "undefined") ) {
					var pRegSaved = this.parseValidJSON(localStorage.regSynced);
					if(pRegSaved.status === 1){
						// CherryJsCore.cherryHandlerUtils.noticeCreate( pRegSaved.type, pRegSaved.message );
					}
					$('.pup_profile_wrapper .btn.save-prof-fld').trigger('click');
					/* localStorage.removeItem('regSynced'); */
				}
			},
    		addList: function() {
				var self =  registerSettings,
					list = $('li.ui-state-default');

				fieldGroup = list.first().clone();
				fieldGroup
					.find('#salesforce_field')
						.removeAttr('disabled')
						.children('option:selected')
							.prop('disabled', 'disabled')
							.removeClass('act')
							.end()
						.end()
					.find('.form-data')
						.val('')
						.end()
					.find('.remove-reg-fld')
						.removeClass('hidden')
						.end()
					.find('.form_settings')
						.val(self.resetHiddenVal())
						.end()
					.find('#wp_user_meta')
						.removeAttr('disabled');
				
				self.var.listParent.append( fieldGroup.fadeIn() );
			},
			removeList: function() {
				var self = registerSettings,
					list = $('li.ui-state-default'),
					thisList = $(this).parents('li.ui-state-default'),
					loginMeta = $('.main-ul').find('#meta_field.form-data'),
					isActive = 0;
					
				var thisVal = thisList.find('#salesforce_field').children(':selected').val();
				var SFName = self.parseValidJSON(thisVal);
				
				if ( SFName ) {
					SFName = SFName.name;
					loginMeta.each(function(index, element){
						if( $(element).children('option:selected').val() == SFName) {
							isActive = 1;
						}
					});
				}
				
				if ( isActive === 0 ) {
				    ( list.length == 2 ) ? thisList.find('.form-data').val('').end().find('.form-data:hidden').val(self.resetHiddenVal()) : thisList.fadeOut(300, function() {thisList.remove()});
				} else {
                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">' + PUP.REGSET_MSG_DUPLICATE + '</div>',3000,'info');
				}
			},
			saveList: function(sync) {
				var self =  registerSettings,
					list = $('li.ui-state-default'),
					regSettings = [],
					btnParent = $(this).parent();
				
				if (! btnParent.hasClass('ajax-called')) {
					btnParent.addClass('ajax-called').append('<span class="spinner"></span>').find('.spinner').css({
						'visibility' : 'visible',
						'float'	: 'none',
						'margin' : '10px 0 12px 10px'
					});

                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving Registration Settings</div>',null,'info');
				
					$.each(list, function(index, element){					
						regSettings[index] = {
							form_settings: self.parseValidJSON($(element).find('.form_settings').val()),
							salesforce_field : self.parseValidJSON($(element).find('#salesforce_field').val()),
							wp_user_meta: $(element).find('#wp_user_meta').val()
						};
					});
					
					regSetData = {
						'pup_duplicate_email' : self.var.modalContainer.find('.pup_duplicate_email').serializeArray(),
						'regSettings' : regSettings
					};
					
					$.post(
						PUP.AJAX_URL,
						{
							action	: 'save_sort',
							data	: regSetData,
							nonce	: PUP.REG_NONCE
						},
						function(data) {
							btnParent.removeClass('ajax-called').find('.spinner').remove();
							if (sync !== 'resync') {
								var dataArr = {
									'status':1,
									'type':data.type,
									'message':data.message
								};
								localStorage.regSaved = JSON.stringify(dataArr);
								
								// window.location.reload();
                                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Registration Settings Saved</div>',3000,'success', function() {

                                	var WPPortalSync_SFID = $('#WPPortalSync-SFID').val();  
                            		if (WPPortalSync_SFID != '') {
                            			registerSettings.saveGeneralSettings();
                            		}else{
                                   		window.location.href = "<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal&menu=registration'; ?>";
                            		}
                                	
                                    // window.location.reload();
                                });
							}
						},
						'json'
					);
				}
			},
			saveGeneralSettings: function() {
				console.log('asdasda');
				var WPPortalSyncEnableDisable = $('#WPPortalSync-enable-sf-syn').val();   
				var WPPortalSyncTimeSchedule = $('#WPPortalSync-time-schedule').val();       
				var WPPortalSyncAPIEndpoint = $('#WPPortalSync-resync-api-endpoint').val();       
				var WPPortalDeleteUserAPIEndpoint = $('WPPortal-DeleteUser-api-endpoint').val();    
				var WPPortalSync_SFID = $('#WPPortalSync-SFID').val();   


				var general_settings = new Array();
				general_settings[0] = {name: "WPPortalSync-enable-sf-sync", value: WPPortalSyncEnableDisable};
				general_settings[1] = {name: "WPPortalSync-time-schedule", value: WPPortalSyncTimeSchedule};
				general_settings[2] = {name: "WPPortalSync-resync-api-endpoint", value: WPPortalSyncAPIEndpoint};
				general_settings[3] = {name: "WPPortal-DeleteUser-api-endpoint", value: WPPortalDeleteUserAPIEndpoint};
				console.log(general_settings);


				// var connection = $(this);
				// connection.after( '<span class="spinner"></span>' );
				// $('.spinner').css({
				// 	'margin-top': '28px',
				// 	'visibility' : 'visible',
				// 	'position': 'absolute',
				// 	'background-color': '#ffffff'
				// });

				//$("#WPPortalSync_save_schedule").attr('disabled','disabled');
				SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving Schedule.</div>',null,'info');
				$.ajax({
					url:  PUP.AJAX_URL,
			        type:'POST',
				    dataType:'json',
			        data: {
			            'action': 'wfc_portal_generalsettings_Save',
			           // nonce: wfc_portal_generalsettings_param.nonce,
				        dataform : general_settings,
			        },
				    success:function(resp){
						console.log(resp);
						var STATUS        = resp['STATUS'];
						var sf_response   = resp['sf_response'];
						if( STATUS == 'Fail' ){
							if( sf_response == 0 ){
								SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Network Error</div>',3000,'error', function() {

                                   	window.location.href = "<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal&menu=registration'; ?>";
                                });
							}else{
								var message   = resp['SF_RESPONCE']['sf_response']['message'];
								SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+message+'</div>',3000,'error', function() {

                                   	window.location.href = "<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal&menu=registration'; ?>";
                                });
							}
						}else{
							if( STATUS == 'Warn' ){			
								SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Get existing settings SF ID.</div>',3000,'warning', function() {

                                   	window.location.href = "<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal&menu=registration'; ?>";
                                });
							}else{
								SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Successful</div>',3000,'success', function() {

                                   	window.location.href = "<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal&menu=registration'; ?>";
                                });
							}
						}

						// $(".spinner").remove();
						// $('#WPPortalSync_save_schedule').removeAttr('disabled');
				    },
				    error: function(response) {
				    	console.log(response);
				    	$('#WPPortalSync_save_schedule').removeAttr('disabled');
				    	SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Save Error</div>',3000,'error', function() {

                                   	window.location.href = "<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal&menu=registration'; ?>";
                                });


					}
				});
			},
			showFormSetting: function() {
				
				var self = registerSettings,
					userMetaVal = $(this).parents('li.ui-state-default').find('#wp_user_meta').children('option:selected').val(),
					formSettings = $(this).parents('li.ui-state-default').find('.form_settings'),
					modalBtnSave = self.var.modalContainer.find('.pup-modal-save-ctrl'),
					modalBtnClose = self.var.modalContainer.find('.pup-modal-close'),
					modalInp =  self.var.modalContainer.find('.form-data'),
					modal = {
						mSave: function() {
							formSettings.val(JSON.stringify(modalInp.serializeArray()));
							this.mHide();
						},
						mHide: function() {
							self.var.modalContainer.css('display', 'none');
							
							$.each(modalInp, function(index, element) {
								if ( $(element).is(':radio') ) {
									if( $(element).hasClass('radio-true') ) { 
										$(element).prop('checked', 'checked'); 
									}
								} else if ( $(element).is('select') ){
									if( $(element).is('select[name="label_position"]') ) { 
										$(element).val('top'); 
									} else if ( $(element).is('select[name="layout_size"]') ) {
										$(element).val('col pu-one'); 
									} else {
										$(element).val(''); 
									}
								} else { 
									$(element).val(''); 
								}
							});

							modalBtnSave.unbind('click');
							modalBtnClose.unbind('click');
						},
						mPopulate: function() {	
							if (userMetaVal == 'ASPU__WPUsername__c') {
								self.var.modalContainer.find('.pup-switcher-wrap').parents('.col').addClass('hide');
							} else if (userMetaVal == 'Email') {
								self.var.modalContainer.find('.pup-switcher-wrap-ade').parents('.col').removeClass('hide');
								self.var.modalContainer.find('.pup-switcher-wrap').not('.pup-switcher-wrap-ade').parents('.col').addClass('hide');
							} else {
								self.var.modalContainer.find('.pup-switcher-wrap-ade').parents('.col').addClass('hide');
								self.var.modalContainer.find('.pup-switcher-wrap').not('.pup-switcher-wrap-ade').parents('.col').removeClass('hide');
							}
							
							$.each(self.parseValidJSON(formSettings.val()), function(key, data) {
								if( data.name == 'required_field' || data.name == 'show_on_reg' ) {
									( data.value == '1' ) ? self.var.modalContainer.find('#'+data.name+'-true').prop('checked', 'checked') : self.var.modalContainer.find('#'+data.name+'-false').prop('checked', 'checked');
								} else {
									self.var.modalContainer.find('[name="'+ data.name +'"]').val(data.value);
								}
							});
						}
					};
				modal.mPopulate();

				modalBtnSave.click(function(){ modal.mSave() });
				modalBtnClose.click(function(){ modal.mHide() });
				self.var.modalContainer.css('display', 'block');
			},
			resetHiddenVal: function() {
				return '[{"name":"field_label","value":""},{"name":"label_position","value":"top"},{"name":"placeholder","value":""},{"name":"default_value","value":""},{"name":"css_classes","value":""},{"name":"layout_size","value":"pu-one"},{"name":"required_field","value":"1"},{"name":"show_on_reg","value":"1"}]';
			},
			setFormSettingFldLbl: function() {
				var self = registerSettings,
					formSettings = $(this).parents('li.ui-state-default').find('.form_settings'),
					options = $(this).children('option'),
					optionVal = $(this).children('option:selected').html();
				
				var act = $(this).children('option.act').html();

				options.removeClass('act');
				
				$.each(self.var.listParent.find('option'), function(index, element) {
					if ( $(element).html() == act ) {
						$(element).removeProp('disabled');
					}
				});
				
				$(this).children('option:selected').addClass('act');
				$.each( self.var.listParent.find('option'), function(index, element) {
					if ( $(element).html() == optionVal ) {
						$(element).not('.act').attr('disabled','disabled');
					}
				});
				
				var formData = self.parseValidJSON(formSettings.val());
				
				if (formData[0].value != '') {
					$.each(options, function(index, element){
						
						if ($(element).html() != '') {
							if ( formData[0].value == $(element).html()){
								formData[0].value = optionVal;
							}	
						}
					});
				} else {
					formData[0].value = optionVal;
				}

				formSettings.val(JSON.stringify(formData));
			},
			syncListData: function() {
				var self = registerSettings,
					options = '',
					selected = [],
					btnParent = $(this).parent();
				
				if (! btnParent.hasClass('ajax-called')) {
					btnParent.addClass('ajax-called').append('<span class="spinner"></span>').find('.spinner').css({
						'visibility' : 'visible',
						'float'	: 'none',
						'margin' : '10px 0 12px 10px'
					});
					
					self.saveList('resync');

                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving and Resyncing Registration Settings</div>',null,'info');
					
					$.post(
						PUP.AJAX_URL,
						{
							action: 'sync_list_data',
							nonce:	PUP.NONCE
						},
						function(data) {
							btnParent.removeClass('ajax-called').find('.spinner').remove();

							var dataArr = {
								'status'	: 1,
								'type'		: data.type,
								'message'	: data.message
							};
							localStorage.regSaved = JSON.stringify(dataArr);

                            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Settings saved and resynced</div>',3000,'success', function() {
                                window.location.reload();
                            });
						},
						'json'
					);
				}
			},
			parseValidJSON: function(obj) {
				try {
					obj = JSON.parse(obj);
				} catch(e) {
					return '';
				}
				return obj;
			}
    	};

		registerSettings.init({
			body: $('body'),
			listParent: $('#registration-list'),
			modalContainer: $('.pup-modal-container')
		});
    });
})( jQuery );
</script>
<?php
return ob_get_clean();