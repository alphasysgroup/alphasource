<?php
$field_opt = array();
if( ! empty( $meta_keys ) ) {
    foreach ( $meta_keys as $key ) {
        $field_opt[$key['meta_key']] = $key['meta_key'];
    }

}
$emailDataSettings = get_option('user_data_settings');

$new_user 	= $emailDataSettings['new_user'];
$new_admin 	= $emailDataSettings['new_admin'];
$retrieve 	= $emailDataSettings['retrieve'];

ob_start();
?>

    <div class="pup_container_email">
        <div class="token_modal_wrapper modal_close">
            <div class="token_header">
                <span><?php echo __('Token','pu-portal'); ?></span>
                <span><?php echo __('Value','pu-portal'); ?></span>
                <a class="token_close fa fa-close"></a>
            </div>
            <div class="token_content">
                <table>
                    <thead>
                    </thead>
                    <tbody>
                    <?php foreach($field_opt as $key => $val) : ?>
                        <tr>
                            <td><span class="token_span"><code>[<?php echo $key ?>]</code></span></td>
                            <td><?php echo $val ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div id="wfc-portal-list-info" class="wfc-portal-list-info">
            <div class="pins">
                <a class="info-loading-icon token_modal" title="Click Here to browse for available tokens.">
                    <?php require WFC_PORTAL_BASEDIR . 'svg/webforce-connect-portal-list-info.php';?>
                </a>
            </div>
        </div>
       <!-- <a class="token_modal"><?php /*_e('Browse available Tokens','pu-portal'); */?></a>-->
        <div class="new_user_wrapper">
            <h4><?php echo __('New User','pu-portal'); ?></h4>
            <div class="form_container">
                <div class="form_group col s6">
                    <label for="nwsr_name"><?php echo __('From Name','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="nwsr_field_name" id="nwsr_name" value="<?php if (! empty($new_user['nwsr_field_name'])) echo $new_user['nwsr_field_name'];  ?>">
                </div>
                <div class="form_group col s6">
                    <label for="nwsr_email"><?php echo __('From E-mail','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="nwsr_field_email" id="nwsr_email" value="<?php if ( !empty($new_user['nwsr_field_email'])) echo $new_user['nwsr_field_email'];  ?>">
                </div>
                <div class="form_group col s6">
                    <label for="nwsr_e_format"><?php echo __('E-mail Format','pu-portal'); ?></label>
                    <select class="form_data" name="nwsr_field_format" id="nwsr_e_format">
                        <option <?php selected( $new_user['nwsr_field_format'],'Plain Text' ); ?> value="Plain Text"><?php echo __('Plain Text','pu-portal'); ?></option>
                        <option <?php selected( $new_user['nwsr_field_format'],'HTML' ); ?> value="HTML"><?php echo __('HTML','pu-portal'); ?></option>
                    </select>
                </div>
                <div class="form_group col s6">
                    <label for="nwsr_subject"><?php echo __('Subject','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="nwsr_field_subject" id="nwsr_subject" value="<?php if(!empty($new_user['nwsr_field_subject'])) echo $new_user['nwsr_field_subject'];  ?>">
                </div>
                <div class="form_group col s12 <?php echo ($new_user['nwsr_field_format'] != 'HTML') ? 'toggle-plain': 'toggle-editor'; ?>">
                    <label for="nwsr_message"><?php echo __('Message','pu-portal'); ?></label>
                    <small class="wfc_portal_email_notice wfc_hidden"><?php echo __('NOTE: Please add <code>[pu_password_reset_link]</code> for the password configuration link to appear on your mail.', 'pu-portal'); ?></small>
                    <textarea class="form_data" name="nwsr_field_message" id="nwsr_message"><?php echo stripslashes($new_user['nwsr_field_message']); ?></textarea>
                    <div class="pup_editor_wrapper">
                        <?php
                        $settings = array(
                            'textarea_name'	=> 'nwsr_field_messagex',
                            'editor_class' 	=> 'form_data',
                            'media_buttons' => false,
                            'tinymce'		=> array(
                                'init_instance_callback' => 'function(editor) {
									editor.on("focus",function(){
										theCondition = true;
										tokenInput =  editor.id;	
									})
									editor.on("keyup",function(){
										
										document.getElementById("nwsr_messagex").value = editor.getContent();
										
									})

									nwsrEditor = editor;
									
								}'
                            )
                        );
                        wp_editor(stripslashes($new_user['nwsr_field_messagex']),'nwsr_messagex',$settings);
                        ?>
                    </div>
                </div>
            </div>
            <span class="group-toggle"></span>
        </div>
        <div class="new_user_admin_wrapper">
            <h4><?php echo __('New User - Admin Notification','pu-portal'); ?></h4>
            <div class="form_container">
                <div class="form_group col s12">
                    <div class="row col s6">
                        <label for="nwsradmin_to"><?php echo __('To','pu-portal'); ?></label>
                        <input type="text" class="form_data" name="nwsradmin_field_to" id="nwsradmin_to" value="<?php if (! empty($new_admin['nwsradmin_field_to'])) echo $new_admin['nwsradmin_field_to'];  ?>">
                    </div>
                </div>
                <div class="form_group col s6">
                    <label for="nwsradmin_name"><?php echo __('From Name','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="nwsradmin_field_name" id="nwsradmin_name" value="<?php if (! empty($new_admin['nwsradmin_field_name'])) echo $new_admin['nwsradmin_field_name'];  ?>">
                </div>
                <div class="form_group col s6">
                    <label for="nwsradmin_email"><?php echo __('From E-mail','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="nwsradmin_field_email" id="nwsradmin_email" value="<?php if(! empty($new_admin['nwsradmin_field_email'])) echo $new_admin['nwsradmin_field_email'];  ?>">
                </div>
                <div class="form_group col s6">
                    <label for="nwsradmin_e_format"><?php echo __('E-mail Format','pu-portal'); ?></label>
                    <select class="form_data" name="nwsradmin_field_format" id="nwsradmin_e_format">
                        <option <?php selected( $new_admin['nwsradmin_field_format'],'Plain Text' ); ?> value="Plain Text"><?php echo __('Plain Text','pu-portal'); ?></option>
                        <option <?php selected( $new_admin['nwsradmin_field_format'],'HTML' ); ?> value="HTML"><?php echo __('HTML','pu-portal'); ?></option>
                    </select>
                </div>
                <div class="form_group col s6">
                    <label for="nwsradmin_subject"><?php echo __('Subject','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="nwsradmin_field_subject" id="nwsradmin_subject" value="<?php if(!empty($new_admin['nwsradmin_field_subject'])) echo $new_admin['nwsradmin_field_subject'];  ?>">
                </div>
                <div class="form_group col s12 <?php echo ($new_admin['nwsradmin_field_format'] != 'HTML') ? 'toggle-plain': 'toggle-editor'; ?>">
                    <label for="nwsradmin_message"><?php echo __('Message','pu-portal'); ?></label>
                    <textarea class="form_data" name="nwsradmin_field_message" id="nwsradmin_message"><?php echo stripslashes($new_admin['nwsradmin_field_message']); ?></textarea>
                    <div class="pup_editor_wrapper">
                        <?php
                        $settings = array(
                            'textarea_name'	=> 'nwsradmin_field_messagex',
                            'editor_class' 	=> 'form_data',
                            'media_buttons' => false,
                            'tinymce'		=> array(
                                'init_instance_callback' => 'function(editor) {
									editor.on("focus",function(){
										tokenInput =  editor.id;
										theCondition = true;
									})
									editor.on("keyup",function(){
										
										document.getElementById("nwsradmin_messagex").value = editor.getContent();
										
									})

									nwsradminEditor = editor;
									
								}'
                            )

                        );
                        wp_editor(stripslashes($new_admin['nwsradmin_field_messagex']),'nwsradmin_messagex',$settings);
                        ?>
                    </div>
                </div>
            </div>
            <span class="group-toggle"></span>
        </div>
        <div class="retrieve_wrapper">
            <h4><?php echo __('Retrieve Password','pu-portal'); ?></h4>
            <div class="form_container">
                <div class="form_group col s6">
                    <label for="retrieve_name"><?php echo __('From Name','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="retrieve_field_name" id="retrieve_name" value="<?php if (! empty($retrieve['retrieve_field_name'])) echo $retrieve['retrieve_field_name'];  ?>">
                </div>
                <div class="form_group col s6">
                    <label for="retrieve_email"><?php echo __('From E-mail','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="retrieve_field_email" id="retrieve_email" value="<?php if (! empty($retrieve['retrieve_field_email'])) echo $retrieve['retrieve_field_email']; ?>">
                </div>
                <div class="form_group col s6">
                    <label for="retrieve_e_format"><?php echo __('E-mail Format','pu-portal'); ?></label>
                    <select class="form_data" name="retrieve_field_format" id="retrieve_e_format">
                        <option <?php selected( $retrieve['retrieve_field_format'],'Plain Text' ); ?> value="Plain Text"><?php echo __('Plain Text','pu-portal'); ?></option>
                        <option <?php selected( $retrieve['retrieve_field_format'],'HTML' ); ?> value="HTML"><?php echo __('HTML','pu-portal'); ?></option>
                    </select>
                </div>
                <div class="form_group col s6">
                    <label for="retrieve_subject"><?php echo __('Subject','pu-portal'); ?></label>
                    <input type="text" class="form_data" name="retrieve_field_subject" id="retrieve_subject" value="<?php if (! empty($retrieve['retrieve_field_subject'])) echo $retrieve['retrieve_field_subject']; ?>">
                </div>
                <div class="form_group col s12 <?php echo ($retrieve['retrieve_field_format'] != 'HTML') ? 'toggle-plain': 'toggle-editor'; ?>">
                    <label for="retrieve_message"><?php echo __('Message','pu-portal'); ?></label>
                    <span class="wfc_portal_email_notice wfc_hidden"><?php echo __('NOTE: Please add <code>[pu_password_reset_link]</code> for the password configuration link to appear on your mail.', 'pu-portal'); ?></span>
                    <textarea class="form_data" name="retrieve_field_message" id="retrieve_message"><?php echo stripslashes($retrieve['retrieve_field_message']); ?></textarea>
                    <div class="pup_editor_wrapper">
                        <?php
                        $settings = array(
                            'textarea_name'	=> 'retrieve_field_messagex',
                            'editor_class' 	=> 'form_data',
                            'media_buttons' => false,
                            'tinymce'		=> array(
                                'init_instance_callback' => 'function(editor) {
									editor.on("focus",function(){
										tokenInput =  editor.id;
										theCondition = true;
									})
									editor.on("keyup",function(){
										
										document.getElementById("retrieve_messagex").value = editor.getContent();
										
									})

									retrieveEditor = editor;
									
									
								}'
                            )
                        );
                        wp_editor(stripslashes($retrieve['retrieve_field_messagex']),'retrieve_messagex',$settings);
                        ?>
                    </div>
                </div>
            </div>
            <span class="group-toggle"></span>
        </div>
        <button type="button" name="emailSubmit" id="emailSubmit"><?php echo __('Save','pu-portal'); ?></button>
        <button type="button" class="WPPortal-back_button" id="goBack" onclick="window.location.href = '<?php echo get_admin_url() . 'admin.php?page=pronto-portal'; ?>';"><?php echo __('Back','pu-portal'); ?></button>

    </div>
    <script language="javascript" type="text/javascript">
        var tokenInput,nwsrEditor,nwsradminEditor,retrieveEditor,theCondition;
        var pinsContainer = jQuery(document).find('.wfc-portal-list-info');
        var pins = pinsContainer.find('.pins');

        (function($){
            $(document).ready(function(){

                $('.token_modal_wrapper').draggable();

                pins.on('click', function () {
                    pins.addClass('closed');
                });

                $('.token_close').on('click',function(){
                    var $parent = $(this).parents('.token_modal_wrapper');

                    if(! $parent.hasClass('modal_close')) {
                        $parent.hide();
                        $parent.addClass('modal_close');
                        pins.removeClass('closed');
                    } else {
                        $parent.show();
                        $parent.removeClass('modal_close');
                        pins.addClass('closed');

                    }
                });
                $('.token_modal').on('click',function(){
                    if($('.token_modal_wrapper').hasClass('modal_close')) {
                        $('.token_close').trigger('click');
                    }
                });

                $('.group-toggle').on('click',function() {
                    var $parent = $(this).parent();
                    if($parent.hasClass('toggle-closed')) {
                        $parent.addClass('toggle-closed');
                    } else {
                        $parent.removeClass('toggle-closed');
                    }
                });


                $("body").on('click', '.token_span', function() {

                    var tokenAdd = $(this).text();

                    if(theCondition) {
                        if (tokenInput == 'nwsr_messagex') {
                            nwsrEditor.selection.setContent(tokenAdd);
                            $('#'+ tokenInput).val(nwsrEditor.getContent());
                        } else if (tokenInput == 'nwsradmin_messagex') {
                            nwsradminEditor.selection.setContent(tokenAdd);
                            $('#'+ tokenInput).val(nwsradminEditor.getContent());
                        } else if (tokenInput == 'retrieve_messagex') {
                            retrieveEditor.selection.setContent(tokenAdd);
                            $('#'+ tokenInput).val(retrieveEditor.getContent());
                        }
                    } else {
                        if (document.getElementById(tokenInput) == null) return false;

                        var thistoken = document.getElementById(tokenInput).selectionStart,
                            valuefield = $('#'+ tokenInput).val();

                        $('#'+ tokenInput).val(valuefield.substring(0, thistoken) + tokenAdd + valuefield.substring(thistoken) );
                    }
                });

                $( ".pup_container_email" ).find('input[type="text"], textarea').focus(function() {
                    tokenInput = $(this).attr('id');
                    theCondition = false;
                });

                $('body').on('change','select.form_data',function(){
                    var containerWrap = $(this).parents('.form_container'),
                        lastgroup = containerWrap.find('.form_group:last-child'),
                        iframevar = lastgroup.find('iframe');

                    if($(this).val() == 'HTML') {
                        lastgroup.toggleClass('toggle-plain toggle-editor');
                        containerWrap.find('.wfc_portal_email_notice').removeClass('wfc_hidden');
                    } else {
                        lastgroup.toggleClass('toggle-editor toggle-plain');
                        if(containerWrap.find('.wfc_portal_email_notice').hasClass('wfc_hidden') === false) {
                            containerWrap.find('.wfc_portal_email_notice').addClass('wfc_hidden');
                        }
                    }

                    lastgroup.find('textarea').val('');
                    $(iframevar).contents().find("body p").remove();
                });

                $('#retrieve_e_format, #nwsr_e_format').on('change', function () {
                    if ($('#nwsr_e_format').val() === 'HTML') {
                        $('#nwsr_message').append('<span class="wfc_portal_email_notice" style="color: red">Please add <code>[pu_password_reset_link]</code> for the password configuration link to appear on your mail.</span>');
                    }
                    else if ($('#retrieve_e_format').val() === 'HTML') {
                        $('#retrieve_message').preppend('<span class="wfc_portal_email_notice" style="color: red">Please add <code>[pu_password_reset_link]</code> for the reset link to appear on your mail.</span>');
                    }
                });



                $('body').on('click','#emailSubmit',function(){
                    var emailData = $('.pup_container_email').find('.form_data').serializeArray();
                    var actionData = {
                            action: 'save_email',
                            data: emailData,
                            nonce: PUP.EMAIL_NONCE
                        },
                        btnParent = $(this).parent();

                    // console.log(actionData);

                    if(! btnParent.hasClass('ajax-called')) {
                        btnParent.addClass('ajax-called').append('<span class="spinner"></span>').find('.spinner').css({
                            'visibility' : 'visible',
                            'float'	: 'none',
                            'margin' : '10px 0 12px 10px'
                        });

                        $.post(ajaxurl, actionData, function(response) {
                                btnParent.removeClass('ajax-called').find('.spinner').remove();
                                // console.log(response);
                                SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">'+ response.message + '</div>',3000,response.type);
                            }
                        );
                    }
                });
            });
        })( jQuery );

    </script>
    <style type="text/css">
        .wfc_portal_email_settings_form{
            padding: 0 !important;
        }

        .pup_container_email{
            padding: 0 !important;
        }
    </style>
<?php return ob_get_clean(); ?>