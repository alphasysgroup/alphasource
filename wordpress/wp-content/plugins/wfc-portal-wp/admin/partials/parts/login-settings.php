<?php
/* <form id="loginform" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">*/
$field_opt = array();

foreach( get_user_meta( get_current_user_id(), '', true ) as $key => $val ){
	$field_opt[ $key ] = $val[0];
}

ob_start();
?>
<div class="list-condition-entry">
    <!--
	<div class="header-wrapper">
		<h2><?php echo __('Filter Settings', 'pu-portal'); ?></h2>
	</div>
	-->
    <ul class="main-ul">
	    <?php $count = count(get_option('login_filter')); ?>
        <?php if(! empty(get_option('login_filter'))) : foreach( get_option('login_filter') as $key => $val ) : ?>
        <?php $opentag = 0; ?>
        <?php echo ( ! empty( $val['op'] ) ) ? '<li class="nested" data-emop="' . $val['op'] . '">' . $val['op'] . '</li>' : ''; ?>
			<li class="child-li">
				<ul class="parentUl">
					<li>
		                <?php foreach( $val['fldgroup'] as $key2 => $val2 ) :
                        $flag = 0;
                        $last = ( $key2+1 == count( $val['fldgroup'] ) ) ? true : false;

                        echo ( $key2 != 0 ) ? '<em data-emop="' . $val2['op'] . '">' . $val2['op'] . '</em>' : '';

                        if( $val2['op'] == 'or' ) {
                            echo '<ul class="parentUl"><li>';
                            $opentag++;
                        }
                        ?>
                        <div class="form-content">
                            <div class="col s8">
                                <div class="col s4">
                                    <label for="meta_field"><?php echo __('Meta Field', 'pu-portal'); ?></label>
                                    <select id="meta_field" name="metafield" class="form-data" >
                                        <option value="">--<?php echo __('Select', 'pu-portal'); ?>--</option>
                                        <?php foreach($field_opt as $field_opt_key => $field_opt_val) : ?>
                                            <?php $choices = ( is_serialized($field_opt_val) ) ? json_encode( maybe_unserialize($field_opt_val) ) : 0; ?>
                                            <?php if( $choices && ( $val2['data']['metafield'] == $field_opt_key ) ) $flag = 1; ?>
                                            <option data-choices='<?php echo $choices; ?>' <?php selected( $val2['data']['metafield'] , $field_opt_key ); ?> value="<?php echo $field_opt_key; ?>" ><?php echo $field_opt_key; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col s4">
                                    <label for="meta_key"><?php echo __('Meta Key', 'pu-portal'); ?></label>
                                    <select id="meta_key" name="metakey" class="form-data">
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '' ); ?> value=''>--<?php echo __('Select', 'pu-portal'); ?>--</option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '=' ); ?> value='='><?php echo __('is equal to', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '!=' ); ?> value='!='><?php echo __('not equal to', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '<' ); ?> value='<'><?php echo __('less than', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '<=' ); ?> value='<='><?php echo __('less than or equal', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '>' ); ?> value='>'><?php echo __('greater than', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], '>=' ); ?> value='>='><?php echo __('greater than or equal', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], 'starts' ); ?> value='starts'><?php echo __('starts with', 'pu-portal'); ?></option>
                                        <option class="<?php if($flag) echo 'hidden';?> hid-on-arr" <?php selected( $val2['data']['metakey'], 'ends' ); ?> value='ends'><?php echo __('ends with', 'pu-portal'); ?></option>
                                        <option class="<?php if(!$flag) echo 'hidden';?> show-on-arr" <?php selected( $val2['data']['metakey'], 'IN' ); ?> value='IN'><?php echo __('in', 'pu-portal'); ?></option>
                                        <option class="<?php if(!$flag) echo 'hidden';?> show-on-arr" <?php selected( $val2['data']['metakey'], 'NOT IN' ); ?> value='NOT IN'><?php echo __('not in', 'pu-portal'); ?></option>
                                    </select>
                                </div>
                                <div class="col s4" >
                                    <!--<label for="meta_value">Meta Value</label>
                                    <input type="text" class="form-data" id="meta_value" name="metavalue" value="<?php //echo $val2['data']['metavalue']; ?>"/>-->
                                    <label for="meta_value"><?php echo __('Meta Value', 'pu-portal'); ?></label>
                                    <input <?php echo ( empty( $val2['data']['meta_tags'] ) && ! empty( $val2['data']['metavalue'] ) ) ? 'style="display:block"' : 'style="display:none"' ; ?> id="meta_value" type="text" class="form-data" name="metavalue" value="<?php echo $val2['data']['metavalue']; ?>" required />
                                    <span class="wfc_portal_login_notice"><?php echo __('If value is <code>boolean</code>, use either <code>1</code>/<code>0</code><br/> or <code>true</code>/<code>false</code>.', 'pu-portal'); ?></span>
                                    <input <?php echo ( empty( $val2['data']['metavalue'] ) && ! empty( $val2['data']['meta_tags'] )) ? 'data-has="yes"' : 'data-has="no"'; ?> name="meta_tags" class="form-data meta_tags" value="<?php echo $val2['data']['meta_tags']; ?>"/>
                                </div>
                                <div class="col s12">
                                    <label for="error_msg"><?php echo __('Error Message', 'pu-portal'); ?></label>
                                    <textarea class="form-data" rows="10" cols="30" name="errmsg" id="error_msg" required><?php echo $val2['data']['errmsg']; ?></textarea>
                                </div>
                            </div>
                            <div class="col s3" >
                                <a class="btn parent-btn" data-par="and" <?php echo ( $last ) ? 'style="display:inline-block"' : 'style="display:none"';?>><?php echo __('and', 'pu-portal'); ?></a>
                                <a class="btn parent-btn" data-par="or" <?php echo ( $last ) ? 'style="display:inline-block"' : 'style="display:none"';?>><?php echo __('or', 'pu-portal'); ?></a>
                                <a class="fa fa-close remove-btn"  <?php echo ( $last && $key2 != 0 ) ? 'style="display:inline-block"' : 'style="display:none"';?>></a>
                            </div>
                        </div>
			        <?php
                    if( $last ) {
                        for( $i = 0; $i < $opentag; $i++ )
                            echo '</li></ul>';
                    }
					?>
					<?php endforeach; ?>
					</li>
				</ul>
				<div class="remove-group">
					<a class="btn remove-all"><?php echo __($count === 1 ? 'Reset Group' : 'Remove Group', 'pu-portal'); ?></a>
				</div>
			</li>
        <?php endforeach; ?>
	    <?php else : ?>
            <li class="child-li">
                <ul class="parentUl">
                    <li>
                        <div class="form-content">
                            <div class="col s8">
                                <div class="col s4">
                                    <label for="meta_field"><?php echo __('Meta Field', 'pu-portal'); ?></label>
                                    <select id="meta_field" name="metafield" class="form-data" >
                                        <option value="">--<?php echo __('Select', 'pu-portal'); ?>--</option>
                                        <?php foreach($field_opt as $field_opt_key => $field_opt_val) : ?>
                                            <?php $choices = ( is_serialized($field_opt_val) ) ? json_encode( maybe_unserialize($field_opt_val) ) : 0;  ?>
                                            <option data-choices='<?php echo $choices; ?>' value="<?php echo $field_opt_key; ?>" ><?php echo $field_opt_key; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col s4">
                                    <label for="meta_key"><?php echo __('Meta Key', 'pu-portal'); ?></label>
                                    <select id="meta_key" name="metakey" class="form-data">
                                        <option class="hid-on-arr" value=''>--<?php echo __('Select', 'pu-portal'); ?>--</option>
                                        <option class="hid-on-arr" value='='><?php echo __('is equal to', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='!='><?php echo __('not equal to', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='<'><?php echo __('less than', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='<='><?php echo __('less than or equal', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='>'><?php echo __('greater than', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='>='><?php echo __('greater than or equal', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='starts'><?php echo __('starts with', 'pu-portal'); ?></option>
                                        <option class="hid-on-arr" value='ends'><?php echo __('ends with', 'pu-portal'); ?></option>
                                        <option class="show-on-arr" value='IN'><?php echo __('in', 'pu-portal'); ?></option>
                                        <option class="show-on-arr" value='NOT IN'><?php echo __('not in', 'pu-portal'); ?></option>
                                    </select>
                                </div>
                                <div class="col s4" >
                                    <label for="meta_value"><?php echo __('Meta Value', 'pu-portal'); ?></label>
                                    <input type="text" class="form-data" id="meta_value" name="metavalue" required />
                                    <span class="wfc_portal_login_notice"><?php echo __('If value is <code>boolean</code>, use either <code>1</code>/<code>0</code><br/> or <code>true</code>/<code>false</code>.', 'pu-portal'); ?></span>
                                    <input data-has="no" name="meta_tags" class="form-data meta_tags meta_tags_new" />
                                </div>

                                <div class="col s12">
                                    <label for="error_msg"><?php echo __('Error Message', 'pu-portal'); ?></label>
                                    <textarea class="form-data" rows="10" cols="30" name="errmsg" id="error_msg" required></textarea>
                                </div>
                            </div>
                            <div class="col s3">
                                <a class="btn parent-btn" data-par="and"><?php echo __('and', 'pu-portal'); ?></a>
                                <a class="btn parent-btn" data-par="or"><?php echo __('or', 'pu-portal'); ?></a>
                                <a style="display:none;" class="fa fa-close remove-btn"></a>
                            </div>
                        </div>
                    </li>
                </ul>
                <div class="remove-group">
                    <a class="btn remove-all"><?php echo __('Reset Group', 'pu-portal'); ?></a>
                </div>
            </li>
        <?php endif; ?>
    </ul>
    <div class="condition-content">
        <a class="btn con_main" data-con="and"><?php echo __('and', 'pu-portal'); ?></a>
        <a class="btn con_main" data-con="or"><?php echo __('or', 'pu-portal'); ?></a>
    </div>
    <div style="border-top: 1px solid #f1f1f1;margin: 20px 0;padding-top:20px;">
        <button type="button" name="submitbtn" id="submitbtn" class="btn"><?php echo __('Save', 'pu-portal'); ?></button>
        <a id="WPPortal-back_button" href="<?php echo get_admin_url() . 'admin.php?page=pronto-portal'; ?>" type="button" class="btn" style="-webkit-appearance: none;"><?php echo __('BACK', 'pu-portal'); ?></a>
    </div>
</div>
<script type="text/javascript">
(function( $ ) {
    $(document).ready(function(){
        
		/* function regTag(e, x) {
			var a = (e != null) ? e.find('.meta_tags') : $('.meta_tags');
			var z = (x != null ) ? x : '' ;
			a.tagsInput({
				'height':'40px',
				'width':'100%',
				'onChange' : this.tagsCallback
			});
			
		} regTag(); */
		
		var actionPack = {
			init: function( generate ) {
				this.generate = generate;
				this.bindActionPack();  
				this.triggerTagInputs();
			},
			bindActionPack: function() {
				this.generate.staticBody.on('click','.parent-btn', this.parentCallback );
				this.generate.staticBody.on('click','.remove-btn', this.removeCallback );
				this.generate.staticBody.on('click','.con_main', this.ormainCallback );
				this.generate.staticBody.on('click','.remove-all', this.removeAllCallback );
				this.generate.staticBody.on('change','#meta_field', this.metaFldChangeCallback );
				this.generate.submitBtn.on('click',this.submitForm );
			},
			triggerTagInputs: function(selector, hideElem) {

				$('.form-content').each(function() {
					var z = $(this).find('#meta_field').children('option:selected').val();
					var y = $(this).find('#meta_field').children('option:selected').data('choices');
					var a = $(this).find('.meta_tags');
					a.tagsInput({
						'autocomplete_url': PUP.AJAX_URL + '?action=get_tags&nonce=' + PUP.NONCE + '&metafield=' + z,
						'height':'60px',
						'width':'100%',
						'onChange' : function (e,i) {
							/* console.log(jQuery.inArray( i, y )); */
							if($.inArray( i, y ) == -1) {
								$.each($(this).next('.tagsinput').find('span span:contains("'+ i +'")'), function(index, val){
									if($.trim($(val).text()).length == i.length){
										$(this).parent().remove();
									}
								});
								$(this).val($.grep($(this).val().split(','), function(value) {
									return value != i;
								}));
							}
						}
					});					
				});

				if(hideElem != true) {
					if(selector != undefined){
						if(selector.find('.meta_tags').hasClass('show_tags') !== true) {
							selector.find('.tagsinput').hide();
						} else {
							selector.find('.meta_tags').removeClass('show_tags');
						}
					}
				} else {
					selector.find('#meta_value').hide();
				}
			},
			resetCallback: function(appendParent,li,btnShow) {
					
				var self = actionPack, resetVar = $('#reset-data');

                resetVar.find('option').removeAttr('selected');
                resetVar.find('input[type="text"]').val('');
                resetVar.find('textarea').text('');
                resetVar.find('a.parent-btn').show();
                resetVar.find('.meta_tags').removeAttr('id');
                resetVar.find('.meta_tags').attr('data-has','no');
                resetVar.find('.meta_tags').removeAttr('data-tagsinput-init');
                resetVar.find('.meta_tags').removeAttr('value');
                resetVar.find('.tagsinput').remove();
                resetVar.removeAttr('id');
                resetVar.find('#meta_value').removeAttr('value');
                resetVar.find('#meta_value').css('display', 'block');
					
				if (btnShow != null) {
				    resetVar.find('a.remove-btn').hide();
				} else {
					resetVar.find('a.remove-btn').show();
				}
				resetVar.find('li').removeClass('has-child');
	
				if (appendParent != null) {
					if(appendParent) {
						resetVar.find('.form-content').unwrap('#reset-data');
						resetVar.find('a.remove-btn').show();
						resetVar.find('#meta_field').show();
					} else {
						resetVar.removeAttr('id');
					}
				}
				if (li != null) {
					if (li.children('.form-content').length > 1) {
						li.find('.form-content:last-child a.parent-btn').show();
						li.find('.form-content:last-child a.remove-btn').show();
					} else {
						li.find('.parent-btn').show();
						if(li.parents('ul.parentUl').length > 1) {
							li.find('.remove-btn').show();
						}	
					}   
				}		
			},
			parentCallback: function() {
				var self = actionPack,
					$this = $(this),
					parentMainLi = $this.parents('ul.main-ul li'),
					parentLi = $(this).parents('ul.parentUl li'),
					formfirst = parentLi.find('.form-content'),
					parentUl = $this.parents('ul.parentUl'),
					childUl = parentUl.html();
			
				var realparentLiMainUl = self.generate.staticBody.find('ul.main-ul').children();
				
				parentLi.find('.parent-btn').hide();
				parentLi.find('.remove-btn').hide();

				realparentLiMainUl.each(function() {
					$(this).find('.parentUl').children().each(function(index, item) {
						if(parentLi.get(0)  === item) {
							var innerParent = $this.parents('ul.parentUl').eq(0),
								innerParentLi = innerParent.find('li');

							if(innerParent.hasClass('has-child') != true) {
								var appendParent;

								switch ($this.data('par')) {
									case 'and':
                                        $('<em data-emop="and">and</em><div id="reset-data"><div class="form-content">' + formfirst.html() + '</div></div>').appendTo(innerParentLi).hide().fadeIn();
                                        appendParent = true;
                                        break;
									case 'or':
                                        $('<em data-emop="or">or</em><ul class="parentUl" id="reset-data"><li><div class="form-content">' + formfirst.html() + '</div></li></ul>').appendTo(innerParentLi).hide().fadeIn();
                                        appendParent = false;
                                        break;
								} 
								self.resetCallback(appendParent);
							}
						}
					})
				})
			},
			removeCallback: function() {
				var self = actionPack,
					$this = $(this),
					parentMainUl = $this.parents('ul.main-ul'),
					parentMainLi = $this.parents('ul.main-ul li'),
					parentUl = $this.parents('ul.parentUl'),
					parentUlfirst = parentUl.eq(0),
					parentLi = parentUl.find('li');
				var appendParent = null;
			   
				// console.log(parentUl);

				if (parentUlfirst.hasClass('has-child') != true) {
					if (parentUlfirst.find('li').children('.form-content').length > 1) {
                        parentUlfirst.find('.form-content:last-child').fadeOut(300, function() {
                            parentUlfirst.find('.form-content:last-child').prev('em').remove();
                            parentUlfirst.find('.form-content:last-child').remove();
                            self.resetCallback(appendParent,parentUlfirst.find('li'));
                        });
					} else {
                        parentUlfirst.prev('em').fadeOut(300, function() {
                            if(parentUl.eq(1).find('.form-content').length > 1){
                                parentUlfirst.prev('em').remove();
                                parentUlfirst.remove();
                            }

                            parentUl.eq(1).removeClass('has-child');
                            self.resetCallback(appendParent,parentUl.eq(1).find('li'));
                        });
					} 
				}
			},
			ormainCallback: function() {
				var self = actionPack,
					$this = $(this),
					parentMainUl = $('ul.main-ul'),
					parentMainLi = parentMainUl.find('li:last-child'),
					parentMainLifirst = parentMainUl.find('li').eq(0),
					parentUlChild = parentMainLifirst.find('.form-content:first-child');
                var appendParent = false,agi = null,or = true;

                $('<li class="nested" data-emop="'+ $this.data('con') +'">'+ $this.data('con') +'</li><li class="child-li"><ul class="parentUl"><li id="reset-data"><div class="form-content">' + parentUlChild.html() + '</div><div class="remove-group"><a class="btn remove-all">Remove Group</a></div></li></ul></li>').appendTo(parentMainUl).hide().fadeIn();
				self.resetCallback(appendParent,agi,or);

                $(document).find('.remove-all').text('REMOVE GROUP');
			},
			removeAllCallback: function() {
				var self = actionPack,
                    $this = $(this),
                    parentMainLi = $this.parents('ul.main-ul li');
                    parentMainChild = $this.parents('ul.main-ul').children('.child-li');
					
				if(parentMainChild.length > 1) {
				    if (parentMainLi.prev('li.nested').html() != null) {
                        parentMainLi.prev('li.nested').fadeOut(300, function() {
                            parentMainLi.prev('li.nested').remove();
                            if ($this.parents('ul.main-ul').children('.child-li').length - 1 === 1) {
                                $(document).find('.remove-all').text('RESET GROUP');
                            }
                        });
					} else {
                        parentMainLi.next('li.nested').fadeOut(300, function() {
                            parentMainLi.next('li.nested').remove();
                            if ($this.parents('ul.main-ul').children('.child-li').length - 1 === 1) {
                                $(document).find('.remove-all').text('RESET GROUP');
                            }
                        });
					}
                    parentMainLi.fadeOut(300, function() {
                        parentMainLi.remove();
                    });
				} else {
				    parentMainLi.find('#meta_field').val('');
				    parentMainLi.find('#meta_key').val('');
				    parentMainLi.find('#meta_value').val('').prop('required', false);
				    parentMainLi.find('#error_msg').val('').prop('required', false);

				    var childElems = parentMainLi.find('.form-content ~ *');
				    var i = setInterval(function() {
				        if (childElems.length === 0) clearInterval(i);

                        childElems.find('.remove-btn').click();
				        childElems = parentMainLi.find('.form-content ~ *');
                    }, 50);
                }
			},
			submitForm: function() {
			    if (! actionPack.validateEmptyField()) return false;

				var parentMainLi = $('ul.main-ul').children(),
					data = [],
					fieldGroup = [],
					counter = 0,
					operator = '',
					btnParent = $(this).parent();
			
				if(! btnParent.hasClass('ajax-called')) {
					btnParent.addClass('ajax-called').append('<span class="spinner"></span>').find('.spinner').css({
						'visibility' : 'visible',
						'float'	: 'none',
						'margin' : '10px 0 12px 10px'
					});
				
				
					$.each( parentMainLi, function(i) {					
						if( i % 2 != 0 ){
							operator = $(this).data('emop');
							console.log($(this));
						} else {
							$.each($(this).find('.form-content'), function(h) {
								fieldGroup[h] = {
									'data': $(this).find('.form-data').serializeArray(),
									'op': ( h == 0 ) ? '' : ( typeof $(this).prev('em').data('emop') === 'undefined' ) ? 'or' : 'and'
								}
							});
							
							data[counter] = {
								'fldgroup':	fieldGroup,
								'op': operator
							};
							// console.log(data);
							fieldGroup = [];
							counter++;
						}
					});

                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving Login Settings</div>',null,'info');

					$.post(
						PUP.AJAX_URL, 
						{
							action	: 	'filter_login',
							data  	: 	data,
							nonce	:	PUP.LOGIN_NONCE
						},
						function(data) {
							btnParent.removeClass('ajax-called').find('.spinner').remove();
                            SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Login Settings Saved</div>',3000,'success');
						},
						'json'
					);
				}
			},
			metaFldChangeCallback: function() {
				
				var self = actionPack, 
					parentUl = $(this).parents('ul.parentUl'),
					parentUlfirst = parentUl.eq(0),
					parentLi = $(this).parents('.col.s8'),
					self = actionPack,
					hideElem,
					choices = $(this).children('option:selected').data('choices');

				parentLi.find('.meta_tags').removeClass('meta_tags_new');
				parentLi.find('#meta_key').children().eq(0).attr('selected', 'selected');

				if ( ( $.type(choices) === 'array' || $.type(choices) === 'object' ) && choices != 'number') {
					parentLi.find('.hid-on-arr').hide();
					parentLi.find('.show-on-arr').show();

					parentLi.find('#meta_value').val('').hide();
					parentLi.find('.tagsinput').show();
					parentLi.find('.meta_tags').removeAttr('id');
					parentLi.find('.meta_tags').attr('data-has','no');
					parentLi.find('.meta_tags').removeAttr('data-tagsinput-init');
					parentLi.find('.meta_tags').removeAttr('value');
					parentLi.find('.tagsinput').remove();
					parentLi.find('.meta_tags').addClass('show_tags');
					hideElem = true;
				} else {
					parentLi.find('.hid-on-arr').show();
					parentLi.find('.show-on-arr').hide();
					
					parentLi.find('#meta_value').show();
					hideElem = false;
				}
				
				self.triggerTagInputs(parentLi, hideElem);
			},

            validateEmptyField: function() {
			    var isValid = true;

                $('input[name=\'metavalue\']').each(function() {
                    var $this = $(this);

                    if (! $this[0].reportValidity()) {
                        isValid = false;

                        return false;
                    } else {
                        var textarea = $this.parent().parent().find('textarea[name=\'errmsg\']');

                        if (! textarea[0].reportValidity()) {
                            isValid = false;

                            return false;
                        }
                    }
                });

			    return isValid;
            }
		};
		
		actionPack.init({
			staticBody: $('body'),
			submitBtn: $('button#submitbtn'),
			containerdiv : $('.list-condition-entry'),
			metaTags: $('.meta_tags')
		});
    });
})( jQuery );
</script>
<?php
return ob_get_clean();