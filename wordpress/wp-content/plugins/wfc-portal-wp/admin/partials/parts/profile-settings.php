<?php
/*
 * <form id="loginform" method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
 **/

ob_start();
?>
<div class="pup_profile_wrapper">

	<div class="profile_content">
	
		<div class="list_of_field">
			<div class="header-wrapper">
				<!-- <h2><?php // _e('Profile Settings', 'pu-portal'); ?></h2> -->
			</div>
			<ul class="list_unstyled">
                <?php
                $reg_settings = get_option('reg_settings');
                $pup_profile_settings = get_option('pup_profile_settings');

                if( ! empty($reg_settings) && ! empty($pup_profile_settings) ) : foreach($reg_settings as $key => $value) :
                    $show = '1';
                    $readonly = '0';
                    $layout_size = 'pu-one';
                    $class = $id = '';

                    foreach( $pup_profile_settings as $pps_key => $pps_value ) {
                        if ( $value['wp_user_meta'] == $pps_value['meta_key'] ) {
                            $show        = ( isset( $pps_value[ $pps_value['meta_key'] . '_show' ] ) ) ? $pps_value[ $pps_value['meta_key'] . '_show' ] : 1;
                            $readonly    = ( isset( $pps_value[ $pps_value['meta_key'] . '_readonly' ] ) ) ? $pps_value[ $pps_value['meta_key'] . '_readonly' ] : 0;
                            $layout_size = ( isset( $pps_value['layout_size'] ) ) ? $pps_value['layout_size'] : 'pu-one';
                            $class       = ( isset( $pps_value['class'] ) ) ? $pps_value['class'] : '';
                            $id          = ( isset( $pps_value['id'] ) ) ? $pps_value['id'] : '';
                        }
                    }
                ?>
                    <li>
                        <div class="form_container">
                            <div class="form_wrapper s5 m2">
                                <input type="hidden" name="meta_key" class="form-data" value="<?php echo $value['wp_user_meta'] ?>" />

                                <div class="col s12">
                                    <h3><?php printf( __('Field Name: %s', 'pu-portal'), $value['field_label'] ); ?></h3>
                                </div>

                                <div>
                                    <div class="wrap col s4 m3">

                                        <label for="showorhide_<?php  echo $value['wp_user_meta'];  ?>"><?php echo __('Show/Hide','pu-portal'); ?></label><br/>
                                        <div class="pup-switcher-wrap size-normal">
                                            <input
                                                    type="radio"
                                                <?php echo ( $show == '1' ) ? 'checked="checked"' : '' ; ?>
                                                    id="<?php echo $value['wp_user_meta'] ?>_show-true"
                                                    class="pup-input-switcher pup-input-switcher-true form-data"
                                                    name="<?php echo $value['wp_user_meta'] ?>_show"
                                                    value="1"
                                            />
                                            <input
                                                    type="radio"
                                                <?php echo ( $show == '0' ) ? 'checked="checked"' : '' ; ?>
                                                    id="<?php echo $value['wp_user_meta'] ?>_show-false"
                                                    class="radio-false pup-input-switcher pup-input-switcher-false form-data"
                                                    name="<?php echo $value['wp_user_meta'] ?>_show"
                                                    value="0"
                                            />
                                            <label class="sw-enable">
                                                <span><?php echo __('On','pu-portal'); ?></span>
                                            </label>
                                            <label class="sw-disable">
                                                <span><?php echo __('Off','pu-portal'); ?></span>
                                            </label>
                                            <span class="state-marker"></span>
                                        </div>
                                    </div>

                                    <div <?php echo ( $value['wp_user_meta'] == 'ASPU__WPUsername__c' ) ? 'class="wrap col s4 isUname" style="opacity: 0.5;cursor: default;"': 'class="wrap col s4"'; ?>>
                                        <label for="read_<?php echo $value['wp_user_meta']; ?>"><?php echo __('Read Only','pu-portal'); ?></label><br/>
                                        <div class="pup-switcher-wrap size-normal">
                                            <input
                                                    type="radio"
                                                <?php echo ( $readonly == '1' || $value['wp_user_meta'] == 'ASPU__WPUsername__c' ) ? 'checked="checked"' : ''; ?>
                                                    id="<?php echo $value['wp_user_meta'] ?>_readonly-true"
                                                    class="pup-input-switcher pup-input-switcher-true form-data"
                                                    name="<?php echo $value['wp_user_meta'] ?>_readonly"
                                                    value="1"
                                            />
                                            <input
                                                    type="radio"
                                                <?php echo ( $readonly == '0' && $value['wp_user_meta'] != 'ASPU__WPUsername__c' ) ? 'checked="checked"' : ''; ?>
                                                    id="<?php echo $value['wp_user_meta'] ?>_readonly-false"
                                                    class="radio-false pup-input-switcher pup-input-switcher-false form-data"
                                                    name="<?php echo $value['wp_user_meta'] ?>_readonly"
                                                    value="0"
                                            />
                                            <label class="sw-enable">
                                                <span><?php echo __('On','pu-portal'); ?></span>
                                            </label>
                                            <label class="sw-disable">
                                                <span><?php echo __('Off','pu-portal'); ?></span>
                                            </label>
                                            <span class="state-marker"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form_wrapper s5 m2">

                                <div class="form_header">
                                    <h3 ><b><?php echo __('Advanced Settings','pu-portal'); ?></b></h3>
                                    <span class="fa fa-cog fa-2x"></span>
                                </div>

                                <div class="form_body set_hide">
                                    <div class="form_group" >
                                        <label><?php echo __('Grid Layout', 'pu-portal' ); ?></label>
                                        <select id="layout_size" name="layout_size" class="form-data">
                                            <option <?php echo ( $layout_size == 'pu-one' ) ? 'selected="selected"' : ''; ?> value='pu-one'><?php echo __('Full width', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $layout_size == 'pu-one-half' ) ? 'selected="selected"' : ''; ?> value='pu-one-half'><?php echo __('One-half', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $layout_size == 'pu-one-third' ) ? 'selected="selected"' : ''; ?> value='pu-one-third'><?php echo __('One-third', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $layout_size == 'pu-one-fourth' ) ? 'selected="selected"' : ''; ?> value='pu-one-fourth'><?php echo __('One-fourth', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $layout_size == 'pu-one-sixth' ) ? 'selected="selected"' : ''; ?> value='pu-one-sixth'><?php echo __('One-sixth', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $layout_size == 'pu-one-twelfth' ) ? 'selected="selected"' : ''; ?> value='pu-one-twelfth'><?php echo __('One-twelfth', 'pu-portal' ); ?></option>
                                        </select>
                                    </div>
                                    <div class="form_group">
                                        <label><?php echo __('Class','pu-portal'); ?></label>
                                        <input type="text" class="form-data" name="class" value="<?php echo $class; ?>">
                                    </div>
                                    <div class="form_group">
                                        <label><?php echo __('ID','pu-portal'); ?></label>
                                        <input type="text" class="form-data" name="id" value="<?php echo $id; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endforeach; ?>
                <?php foreach( $pup_profile_settings as $pps_key => $pps_value ) : if( $pps_value['meta_key'] == 'Password' || $pps_value['meta_key'] == 'ConPassword' ) :  ?>
                    <li>
                        <div class="form_container">
                            <div class="form_wrapper s5 m2">
                                <input type="hidden" name="meta_key" class="form-data" value="<?php echo $pps_value['meta_key'];  ?>" />

                                <div class="col s12">
                                    <h3><?php echo ( $pps_value['meta_key'] == 'Password' ) ? __('Field Name: ', 'pu-portal').'Password' : __('Field Name: ', 'pu-portal').'Confirm Password' ; ?></h3>
                                </div>

                                <div>
                                    <div class="wrap col s4 m3">

                                        <label for="showorhide_<?php  echo $pps_value['meta_key'];  ?>"><?php echo __('Show/Hide','pu-portal'); ?></label><br/>
                                        <div class="pup-switcher-wrap size-normal">
                                            <input
                                                    type="radio"
                                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_show'] == '1' ) ? 'checked="checked"' : '' ; ?>
                                                    id="<?php echo $pps_value['meta_key']; ?>_show-true"
                                                    class="pup-input-switcher pup-input-switcher-true form-data"
                                                    name="<?php echo $pps_value['meta_key']; ?>_show"
                                                    value="1"
                                            />
                                            <input
                                                    type="radio"
                                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_show'] == '0' ) ? 'checked="checked"' : '' ; ?>
                                                    id="<?php echo $pps_value['meta_key'] ?>_show-false"
                                                    class="radio-false pup-input-switcher pup-input-switcher-false form-data"
                                                    name="<?php echo $pps_value['meta_key'] ?>_show"
                                                    value="0"
                                            />
                                            <label class="sw-enable">
                                                <span><?php echo __('On','pu-portal'); ?></span>
                                            </label>
                                            <label class="sw-disable">
                                                <span><?php echo __('Off','pu-portal'); ?></span>
                                            </label>
                                            <span class="state-marker"></span>
                                        </div>
                                    </div>

                                    <div class="wrap col s4">
                                        <label for="read_<?php echo $pps_value['meta_key']; ?>"><?php echo __('Read Only','pu-portal'); ?></label><br/>
                                        <div class="pup-switcher-wrap size-normal">
                                            <input
                                                    type="radio"
                                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly'] == '1' ) ? 'checked="checked"' : '' ; ?>
                                                    id="<?php echo $pps_value['meta_key'] ?>_readonly-true"
                                                    class="pup-input-switcher pup-input-switcher-true form-data"
                                                    name="<?php echo $pps_value['meta_key'] ?>_readonly"
                                                    value="1"
                                            />
                                            <input
                                                    type="radio"
                                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly'] == '0' ) ? 'checked="checked"' : '' ; ?>
                                                    id="<?php echo $pps_value['meta_key'] ?>_readonly-false"
                                                    class="radio-false pup-input-switcher pup-input-switcher-false form-data"
                                                    name="<?php echo $pps_value['meta_key'] ?>_readonly"
                                                    value="0"
                                            />
                                            <label class="sw-enable">
                                                <span><?php echo __('On','pu-portal'); ?></span>
                                            </label>
                                            <label class="sw-disable">
                                                <span><?php echo __('Off','pu-portal'); ?></span>
                                            </label>
                                            <span class="state-marker"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form_wrapper s5 m2">

                                <div class="form_header">
                                    <h3 ><b><?php echo __('Advanced Settings','pu-portal'); ?></b></h3>
                                    <span class="fa fa-cog fa-2x"></span>
                                </div>

                                <div class="form_body set_hide">
                                    <div class="form_group" >
                                        <label><?php echo __('Grid Layout', 'pu-portal' ); ?></label>
                                        <select id="layout_size" name="layout_size" class="form-data">
                                            <option <?php echo ( $pps_value['layout_size'] == 'pu-one' ) ? 'selected="selected"' : ''; ?> value='pu-one'><?php echo __('Full width', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $pps_value['layout_size'] == 'pu-one-half' ) ? 'selected="selected"' : ''; ?> value='pu-one-half'><?php echo __('One-half', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $pps_value['layout_size'] == 'pu-one-third' ) ? 'selected="selected"' : ''; ?> value='pu-one-third'><?php echo __('One-third', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $pps_value['layout_size'] == 'pu-one-fourth' ) ? 'selected="selected"' : ''; ?> value='pu-one-fourth'><?php echo __('One-fourth', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $pps_value['layout_size'] == 'pu-one-sixth' ) ? 'selected="selected"' : ''; ?> value='pu-one-sixth'><?php echo __('One-sixth', 'pu-portal' ); ?></option>
                                            <option <?php echo ( $pps_value['layout_size'] == 'pu-one-twelfth' ) ? 'selected="selected"' : ''; ?> value='pu-one-twelfth'><?php echo __('One-twelfth', 'pu-portal' ); ?></option>
                                        </select>
                                    </div>
                                    <div class="form_group">
                                        <label><?php echo __('Class','pu-portal'); ?></label>
                                        <input type="text" class="form-data" name="class" value="<?php echo $pps_value['class']; ?>">
                                    </div>
                                    <div class="form_group">
                                        <label><?php echo __('ID','pu-portal'); ?></label>
                                        <input type="text" class="form-data" name="id" value="<?php echo $pps_value['id']; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php endif; endforeach; endif; ?>
            </ul>
			<div class="form-ctrl">
				<a class="btn save-prof-fld"><?php echo __('SAVE', 'pu-portal' ); ?></a>
				<a id="WPPortal-back_button" href="<?php echo get_admin_url() . 'admin.php?page=pronto-portal'; ?>" class="btn back"><?php echo __('BACK', 'pu-portal' ); ?></a>
			</div>
		</div>
	</div>
</div>

<script>
	(function($) {
		$(document).ready(function() {
			var profileControl = {
				init:function( arg ) {
					this.arg = arg;
					this.defaultCallback();
				},
				defaultCallback: function() {
					this.arg.profileContent.on('click', '.fa-cog', this.settingsCallback);
					this.arg.profileContent.on('click', '.save-prof-fld', this.saveList);
					this.disabledFlds('.isUname');
				},
				disabledFlds: function(selector) {
					$(selector).on( 'change click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    });
				},
				settingsCallback: function() {
					var thisfunc = $(this),
						formBody = thisfunc.parent().next();
					
					var d = (formBody.hasClass('set_hide')) ? -80 : 80;
					
					$({deg: 0}).animate({deg: d}, {
						duration: 500,
						step: function(now){
							thisfunc.css({
								 transform: "rotate(" + now + "deg)"
							});
						},
						done: function() {
							if (formBody.hasClass('set_hide')) {
								formBody.slideDown('fast');
								formBody.removeClass('set_hide');
							} else {
								formBody.slideUp('fast');
								formBody.addClass('set_hide');	
							}
						}
					});

				},
				saveList: function(e) {
					e.preventDefault();
					
					var self = profileControl,
						list = self.arg.profileContent.find('.form_container'),
						pupObj = {},
						btnParent = $(this).parent();
				
					if (! btnParent.hasClass('ajax-called')) {
						btnParent.addClass('ajax-called').append('<span class="spinner"></span>').find('.spinner').css({
							'visibility' : 'visible',
							'float'	: 'none',
							'margin' : '10px 0 12px 10px'
						});

                        SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Saving Profile Settings</div>',null,'info');
						
						$.each(list, function(index, element) {
							pupObj[index] = $(element).find('.form-data').serializeArray();
						});
						
						// console.log(pupObj);
						
						$.post(
							PUP.AJAX_URL,
							{
								action: 'save_profile',
								data: pupObj,
								nonce: PUP.PROF_NONCE
							},
							function (data) {
								btnParent.removeClass('ajax-called').find('.spinner').remove();
								if( (typeof(localStorage.regSaved) === "undefined") ) {
                                    SaiyantistCore.snackbar.show('<div style="text-align: center;font-size: 14px;font-weight: 500;">Profile Settings Saved</div>',3000,'success');
								} else {
									localStorage.removeItem('regSaved');
									localStorage.removeItem('regSynced');
								}
							},
							'json'
						);
					}					
				}
			};
			
			profileControl.init({
				profileContent : $('.pup_profile_wrapper'),
				staticBody: $('body')
			});
		});
	})( jQuery );
</script>




<?php
return ob_get_clean();