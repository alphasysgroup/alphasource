<div class="wfc_portal_settings_container">
    <h2><?php echo __('Registration Settings', 'pu-portal'); ?></h2>
    <div class="content">
        <?php

        if (isset($_GET['reset_field_mapping']) && $_GET['reset_field_mapping'] == 1) {
            // require_once plugin_dir_path(dirname(__FILE__)) . 'class-pronto-wp-portal-admin.php';
            delete_option('reg_settings');
            delete_option('created_user_meta');
            Pronto_Wp_Portal_Admin::set_default_opt_val();
            ?>
            <div class="notice notice-success is-dismissible">
                <p><?php echo __('Field Mapping reset.', 'pu-portal'); ?></p>
            </div>
            <?php
        }

        $reg_html = include 'parts/registration-settings.php';
        $defaultxt = 'ERROR: Registration not Allowed. '.'Please contact your Administrator.';
        $defaulthtml = '<b>ERROR:</b> Registration not Allowed.<br/> Please contact your Administrator.';

        $reg_is_allowed = get_option('users_can_register');
        $reg_is_allowed = $reg_is_allowed == '1' ? 'on' :'off';
        $reg_note = get_option('wfc_portal_reg_note');
        $reg_note = isset($reg_note) && !empty($reg_note) ? $reg_note : $defaulthtml;
        $reg_note_text = get_option('wfc_portal_reg_note_text');
        $reg_note_text = isset($reg_note_text) && !empty($reg_note_text) ? $reg_note_text : $defaultxt;
        $reg_text_format = get_option('wfc_portal_reg_text_format');
        $reg_text_format = isset($reg_text_format) && ! empty($reg_text_format) ? $reg_text_format : 'text';


        $reg_setting = array(
            'wfc_portal_reg_settings'  => array(
                'type'      => 'switch',
                'settings'  => array(
                    'id'    => 'wfc_portal_reg_settings',
                    'gridclass' => 'col6 col-md-6',
                    'name'  => 'wfc_portal_reg_settings',
                    'label' => array(
                        'hidden' => false,
                        'value'  => 'Allow registration.'
                    ),
                    'switch' => $reg_is_allowed,
                )
            ),
            'wfc_portal_reg_radio' => array(
                'type'  => 'radio',
                'settings' => array(
                    'id'            => 'wfc_portal_reg_radio',
                    'class'         => 'wfc_portal_reg_radio',
                    'gridclass'     => 'col-md-6',
                    'name'          => 'wfc_portal_reg_radio',
                    'style'         => null,
                    'label'         => array(
                        'hidden'    => false,
                        'value'     => 'Format',
                        'position'  => 'top',
                    ),
                    'description'   => '&nbsp;',
                    'radios'       => array(
                        array(
                            'title' => 'Text',
                            'value' => 'text',
                            'disabled' => false
                        ),
                        array(
                            'title' => 'HTML',
                            'value' => 'html',
                            'disabled' => false
                        )
                    ),
                     'dependency' => array(
                        'id' => 'wfc_portal_reg_settings',
                        'trigger_value' => 'off',
                        'trigger_action' => 'access'
                    ),
                    'checked'      => $reg_text_format,
                )
            ),
            'wfc_portal_reg_separator'  => array(
                'type'  => 'separator',
                'settings'  =>  array(
                    'id'    =>  'wfc_portal_reg_separator',
                    'visibility'    =>  'hidden',
                    'dependency' => array(
                        'id' => 'wfc_portal_reg_settings',
                        'trigger_value' => 'off',
                        'trigger_action' => 'show_hide'
                    ),
                )
            ),
            'registration-form-2' => array(
                'type'      => 'card',
                'settings'  => array(
                    'id'        => 'registration-form-2',
                    'style'     => 'width: 100%; background-color: #f2f2f2; box-shadow: 0 0 0 0 #f2f2f2; padding: 0;',
                    'dependency' => array(
                        'id' => 'wfc_portal_reg_settings',
                        'trigger_value' => 'off',
                        'trigger_action' => 'show_hide'
                    )
                ),
                'children' => array(
                    'wfc_portal_reg_wp_editor' => array(
                        'type'  => 'wpeditor',
                        'settings' => array(
                            'id'            => 'wfc_portal_reg_wp_editor',
                            'class'         => 'wfc_portal_reg_wp_editor',
                            'gridclass'     => 'col-12 col-md-12',
                            'name'          => 'wfc_portal_reg_wp_editor',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Registration Error Message',
                                'position'  => 'top',
                            ),
                            'dependency' => array(
                                'id' => 'wfc_portal_reg_radio',
                                'trigger_value' => 'html',
                                'trigger_action' => 'show_hide'
                            ),
                            'content' => $reg_note
                        )
                    ),
                    'wfc_portal_reg_textarea' => array(
                        'type'  => 'textarea',
                        'settings' => array(
                            'id'            => 'wfc_portal_reg_textarea',
                            'class'         => 'wfc_portal_reg_textarea',
                            'gridclass'     => 'col-12 col-md-12',
                            'style'         => 'width: 100%;',
                            'name'          => 'wfc_portal_reg_textarea',
                            'label'         => array(
                                'hidden'    => false,
                                'value'     => 'Registration Error Message',
                                'position'  => 'top',
                            ),
                            'dependency' => array(
                                'id' => 'wfc_portal_reg_radio',
                                'trigger_value' => 'text',
                                'trigger_action' => 'show_hide'
                            ),
                            'value' => $reg_note_text
                        )
                    )
                )
            ),
            'wfc_portal_save_reg_settings' =>  array(
                'type' => 'button',
                'settings' => array(
                    'id' => 'wfc_portal_save_reg_settings',
                    'name' => 'wfc_portal_save_reg_settings',
                    'gridclass'=> 'col-1',
                    'style' => 'float:left;margin-top:20px;color: #FFFFFF;font-size: 14px;font-weight: 500;padding: 10px 20px 10px 20px;',
                    'content' => 'SAVE',
                    'label' => array(
                        'hidden' => true
                    )
                )
            ),
            'WPPortal-back_button' =>  array(
                'type' => 'button',
                'settings' => array(
                    'id' => 'WPPortal-back_button',
                    'name' => 'WPPortal-back_button',
                    'gridclass'=> 'col-1',
                    'style' => 'float:left;margin-top:20px;color: #FFFFFF;font-size: 14px;font-weight: 500;padding: 10px 20px 10px 20px;',
                    'content' => 'BACK',
                    'label' => array(
                        'hidden' => true
                    )
                )
            )
        );
        $reg_settings_form = array(
            'wfc_portal_reg_settings' => array(
                'type'     => 'settingsform',
                'settings' => array(
                    'id'   => 'wfc_portal_reg_settings_form',
                    'name' => 'wfc_portal_reg_settings_form',
                    'style'     => '',
                    'title'     => '',
                    'submit_id' =>  '',
                ),
                'children' => $reg_setting
            )

        );

        $reg_mapping = array(
            'form_registration' => array(
                'type'      => 'settingsform',
                'settings'  => array(
                    'id'     => 'form_registration',
                    'name'   => 'form_registration',
                    'action' => $_SERVER['REQUEST_URI']
                ),
                'children' => array(
                    'registration-form' => array(
                        'type'      => 'html',
                        'settings'  => array(
                            'id'     => 'registration-form',
                            'label'  => array(
                                'hidden' => true
                            )
                        ),
                        'content' => $reg_html
                    )
                )
            )
        );


        Saiyan::tist()->render_module_mapping(array(
            'wfc_portal_reg_config' => array(
                'type'      => 'accordion',
                'settings'  => array(
                    'id'    =>  'wfc_portal_reg_config',
                ),
                'sections'  => array(
                    array(
                        'title'     => 'Registration Settings',
                        'content'   => $reg_settings_form
                    ),
                    array(
                        'title'     => 'Registration Mapping',
                        'content'   => $reg_mapping
                    )
                )
            )
        ));



      /*  if(! empty($notext)){
            update_option('reg_note', $notext);
        }

        if(! empty($select)){
            update_option('reg_text_format', $select);
        }*/

        ?>
    </div>
</div>
<style>
    input#wfc_hidden_input{
        display: none !important;
    }

    .saiyan-card{
        padding: 0 !important;
        margin-bottom: 0 !important;
    }

    #registration-form-2-container > .saiyan-module{
        display: none;
    }

</style>
<script>
    jQuery(document).ready(function ($) {
        $('#WPPortal-back_button').on('click', function () {
            window.location.href ='<?php echo get_site_url().'/wp-admin/admin.php?page=pronto-portal'; ?>'
        })
    })
</script>
