<div class="wfc_portal_settings_container">
	<h2><?php echo __('Profile Page Settings', 'pu-portal'); ?></h2>
	<div class="content">
		<?php
		$profile_html = include 'parts/profile-settings.php';

		Saiyan::tist()->render_module_mapping(array(
			'form_profile' => array(
				'type' => 'settingsform',
				'settings' => array(
					'id' => 'form_profile',
					'name' => 'form_profile',
				),
				'children' => array(
					'profile-form' => array(
						'type' => 'html',
						'settings' => array(
							'id' => 'profile-form',
							'name' => 'profile-form',
							'label' => array(
								'hidden' => true
							)
						),
						'content' => $profile_html
					)
				)
			)
		));
		?>
	</div>
</div>
