<div class="wfc_portal_settings_container">
	<h2><?php _e('Login Restriction Settings', 'pu-portal'); ?></h2>
	<div class="content">
		<?php
		$Syncer_lis = include 'parts/login-settings.php';

		Saiyan::tist()->render_module_mapping(array(

			'form_login' => array(
				'type' => 'settingsform',
				'settings' => array(
					'id' => 'form_login',
					'name' => 'form_login',
				),
				'children' => array(
					'container-form' => array(
						'type' => 'html',
						'settings' => array(
							'id' => 'container-form',
							'name' => 'container-form',
							'class' => 'container-form-login',
							'label' => array(
								'hidden' => true
							)
						),
						'content' => $Syncer_lis
					)
				)
			),
			/*
			'login_settings_info' => array(
				'type' => 'pinned-notes',
				'settings' => array(
					'id' => 'login_settings_info'
				),
				'pins_notes' => array(
					array(
						'pin' => 'list-info',
						'note' => array(
							'header' => 'Data Types',
							'content' => array(

							)
						)
					)
				)
			)
			*/
		));
		?>
	</div>
</div>
