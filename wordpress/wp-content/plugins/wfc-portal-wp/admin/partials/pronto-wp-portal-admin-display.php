<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 */
?>
<div class="wrap">
    <div class="form-container">
        <?php
        if (isset($_GET['menu'])) {
            switch ($_GET['menu']) {
                case 'general_settings':
                    include 'pronto-wp-portal-admin-general-settings.php';
                    break;
	            case 'registration':
		            include 'pronto-wp-portal-admin-registration.php';
		            break;
	            case 'profile':
		            include 'pronto-wp-portal-admin-profile.php';
		            break;
                case 'login':
                    include 'pronto-wp-portal-admin-login.php';
                    break;
                case 'email':
                    include 'pronto-wp-portal-admin-email.php';
                    break;
                case 'pages':
                    include 'pronto-wp-portal-admin-pages.php';
                    break;
            }
        }
        else {
            ?>
            <div class="wfc_portal_settings_container">
                <h2><?php echo __('Webforce Connect - Portal', 'pu-portal'); ?></h2>
                <div class="wfc-menu-container">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-12">
                                <a class="wfc-portal-menu-btn-link" href="<?php echo esc_url(home_url() . '/wp-admin/admin.php?page=pronto-portal&menu=general_settings') ?>">
                                    <div id="wfc-portal-menu-btn" class="wfc-portal-menu-btn">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="wfc-portal-menu-btn-icon" style="width: 90px;"><i class="fa fa-cogs"></i></div>
                                                <div class="col-10 wfc-portal-menu-btn-desc">
                                                    <h3><?php echo __('General Settings', 'pu-portal'); ?></h3>
                                                    <em><?php echo __('Handle and configure settings on scheduled syncing on WFC - Portal.'. 'pu-portal'); ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a class="wfc-portal-menu-btn-link" href="<?php echo esc_url(home_url() . '/wp-admin/admin.php?page=pronto-portal&menu=registration') ?>">
                                    <div id="wfc-portal-menu-btn" class="wfc-portal-menu-btn">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="wfc-portal-menu-btn-icon" style="width: 90px;"><i class="fa fa-id-card-o"></i></div>
                                                <div class="col-10 wfc-portal-menu-btn-desc">
                                                    <h3><?php echo __('Registration', 'pu-portal'); ?></h3>
                                                    <em><?php echo __('Map registered post type to SalesForce object.', 'pu-portal'); ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a class="wfc-portal-menu-btn-link" href="<?php echo esc_url(home_url() . '/wp-admin/admin.php?page=pronto-portal&menu=profile') ?>">
                                    <div id="wfc-portal-menu-btn" class="wfc-portal-menu-btn">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="wfc-portal-menu-btn-icon" style="width: 90px;"><i class="fa fa-address-card-o"></i></div>
                                                <div class="col-10 wfc-portal-menu-btn-desc">
                                                    <h3><?php echo __('Profile', 'pu-portal'); ?></h3>
                                                    <em><?php echo __('Manages additional attributes on the registered post types that will be displayed on the Profile pages.', 'pu-portal'); ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a class="wfc-portal-menu-btn-link" href="<?php echo esc_url(home_url() . '/wp-admin/admin.php?page=pronto-portal&menu=login') ?>">
                                    <div id="wfc-portal-menu-btn" class="wfc-portal-menu-btn">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="wfc-portal-menu-btn-icon" style="width: 90px;"><i class="fa fa-sign-in"></i></div>
                                                <div class="col-10 wfc-portal-menu-btn-desc">
                                                    <h3><?php echo __('Login', 'pu-portal'); ?></h3>
                                                    <em><?php echo __('Manages User Logins.', 'pu-portal'); ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a class="wfc-portal-menu-btn-link" href="<?php echo esc_url(home_url() . '/wp-admin/admin.php?page=pronto-portal&menu=email') ?>">
                                    <div id="wfc-portal-menu-btn" class="wfc-portal-menu-btn">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="wfc-portal-menu-btn-icon" style="width: 90px;"><i class="fa fa-at"></i></div>
                                                <div class="col-10 wfc-portal-menu-btn-desc">
                                                    <h3><?php echo __('Email', 'pu-portal'); ?></h3>
                                                    <em><?php echo __('Register E-mail response settings.', 'pu-portal'); ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a class="wfc-portal-menu-btn-link" href="<?php echo esc_url(home_url() . '/wp-admin/admin.php?page=pronto-portal&menu=pages') ?>">
                                    <div id="wfc-portal-menu-btn" class="wfc-portal-menu-btn">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="wfc-portal-menu-btn-icon" style="width: 90px;"><i class="fa fa-files-o"></i></div>
                                                <div class="col-10 wfc-portal-menu-btn-desc">
                                                    <h3><?php echo __('Page redirection and Template Options', 'pu-portal'); ?></h3>
                                                    <em><?php echo __('Configure page redirection used on WFC - Portal.', 'pu-portal'); ?></em>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</div>