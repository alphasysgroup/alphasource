<?php
$email_html = include 'parts/email-settings.php';
?>
<div class="wfc_portal_emailtab_container">
    <h2><?php echo __('Email', 'pu-portal'); ?></h2>
    <div class="content">
        <?php
        Saiyan::tist()->render_module_mapping(array(
            'wfc_portal_email_settings' => array(
                'type'     => 'settingsform',
                'settings' => array(
                    'id'        =>  'wfc_portal_email_settings_form',
                    'class'     =>  ' wfc_portal_email_settings_form',
                    'wp_option' => array(
                        'enable'    =>  true,
                        'slug'      => 'user_data_settings'
                    ),
                    'submit_id' => 'emailSubmit'
                ),
                'children' => array(
                    'wfc_email_form' => array(
                        'type'      => 'html',
                        'settings'  => array(
                            'id'    => 'wfc_portal_email_form',
                            'class'    => 'wfc_portal_email_form',
                            'label'         => array(
                                'hidden'    => true,
                                'value'     => 'email',
                                'position'  => 'top',
                            ),
                        ),
                        'content'   => $email_html
                    )
                )
            )
        ));
        ?>
    </div>
</div>