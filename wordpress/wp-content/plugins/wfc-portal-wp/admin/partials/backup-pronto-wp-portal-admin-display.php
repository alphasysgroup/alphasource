<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @since      1.0.0
 *
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
  
  $field_opt = array();
  if( !empty( $meta_keys ) ) {
    foreach ( $meta_keys as $key ) {
      $field_opt[$key['meta_key']] = $key['meta_key'];
    }
  }

  // login form filter settings
  $login_settings = array();
  $login_form = get_option( 'pup_login_form_settings', 0 );
  if( !empty( $login_form ) ) {
    foreach ( $login_form as $value ) {
      $login_settings = $value;
    }
  }

  /**
   * Initialize interface builder
   */
 /* $base->get_core()->init_module( 'cherry-interface-builder', array() );*/

  /**
   * get interface builder obj
   */
  /*$builder = $base->get_core()->modules['cherry-interface-builder'];*/

 /* $builder->register_component( array(
        'pup-component' => array(
            'type' => 'component-tab-horizontal',
            'parent' => '',
            'title' => esc_html__( 'WebForce Connect - Portal', 'pu-portal' ),
            'description' => esc_html__( 'Configure Pronto Ultimate Portal Settings', 'pu-portal' ),
        )
  ) );

  $builder->register_settings( array(
    'pup-login' => array(
      'parent' => 'pup-component',
      'title'  => esc_html__( 'Login', 'pu-portal' ),
    ),
    'pup-profile' => array(
      'parent' => 'pup-component',
      'title'  => esc_html__( 'Profile', 'pu-portal' ),
    ),
    'pup-registration' => array(
      'parent' => 'pup-component',
      'title'  => esc_html__( 'Registration', 'pu-portal' ),
    ),
    'pup-lost-password' => array(
      'parent' => 'pup-component',
      'title'  => esc_html__( 'Lost Password', 'pu-portal' ),
    ),
    'pup-reset-password' => array(
      'parent' => 'pup-component',
      'title'  => esc_html__( 'Reset Password', 'pu-portal' ),
    ),
    'pup-email' => array(
      'parent' => 'pup-component',
      'title'  => esc_html__( 'Email', 'pu-portal' ),
    )
  ) );

  $builder->register_form( array(
    'pup-login-form' => array(
      'type' => 'form',
      'parent' => 'pup-login'
    )
  ) );

  $builder->register_control( array(
    'login_filter' => array(
      'type'        => 'repeater',
      'parent'      => 'pup-login-form',
      'title'       => esc_html__( 'Filters', 'pu-portal' ),
      'description' => esc_html__( 'Add filter for login form.', 'pu-portal' ),
      'add_label'   => esc_html__( 'Add Filter', 'pu-portal' ),
      // 'title_field' => 'meta_field',
      'fields'      => array(
        'meta_field' => array(
          'type'        => 'select',
          'id'          => 'meta_field',
          'name'        => 'meta_field',
          'placeholder' => esc_html__( '', 'pu-portal' ),
          'label'       => esc_html__( 'Meta Field', 'pu-portal' ),
          'options'     => $field_opt
        ),
        'meta_operator' => array(
          'type'        => 'select',
          'id'          => 'meta_operator',
          'name'        => 'meta_operator',
          'placeholder' => esc_html__( '', 'pu-portal' ),
          'label'       => esc_html__( 'Operator', 'pu-portal' ),
          'options'     => array(
            '='         => '=',
            '!='        => '≠',
            '<'         => '&lt;',
            '<='        => '≤',
            '>'         => '&gt;',
            '>='        => '≥',
            'starts'    => 'starts with',
            'ends'      => 'ends with',
            'contains'  => 'contains',
            'IN'        => 'in',
            'NOT IN'    => 'not in',
            'INCLUDES'  => 'includes',
            'EXCLUDES'  => 'excludes',
          ),
        ),
        'meta_value' => array(
          'type'        => 'text',
          'id'          => 'meta_value',
          'name'        => 'meta_value',
          'placeholder' => esc_html__( 'Enter Your Value', 'pu-portal' ),
          'label'       => esc_html__( 'Value', 'pu-portal' ),
        ),
        'filter_error_message' => array(
          'type'        => 'textarea',
          'id'          => 'meta_valueArea',
          'name'        => 'meta_valueArea',
          'placeholder' => esc_html__( '', 'pu-portal' ),
          'label'       => esc_html__( 'Error Message', 'pu-portal' ),
        ),
      ),
      'value' => $login_settings
    ),
    'login_filter_btn' => array(
      'title' => '',
      'type' => 'button',
      'style' => 'primary',
      'content' => '<span class="text">' . esc_html__( 'Save', 'pu-portal' ) . '</span>',
      'parent' => 'pup-login-form',
    )
  ));
*/?><!--

<div class="wrap">
  <div class="pronto-portal-wrap">
    <?php /*echo $builder->render(); */?>
  </div>
</div>-->