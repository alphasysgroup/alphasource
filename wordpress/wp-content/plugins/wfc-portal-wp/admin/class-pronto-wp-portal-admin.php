<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 *
 * @author     AlphaSys Pty Ltd
 */

class Pronto_Wp_Portal_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The main class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $includes    The main class of this plugin.
	 */
	private $includes;

	/**
	 * The oath class of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	public static $oauth = null;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version, $includes ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		$this->includes = $includes;
		
		self::set_default_opt_val();
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles($hook) {

		if($hook == "webforce-connect_page_pronto-portal"){

			wp_enqueue_style( 
				$this->plugin_name . '-font-awesome-css', 
				plugin_dir_url( __FILE__ ) . 'css/font-awesome.min.css', 
				array(), 
				$this->version, 
				'all' 	
			);
			wp_enqueue_style( 
				$this->plugin_name . '-taginputcss',
				plugin_dir_url( __FILE__ ) . 'css/jquery.tagsinput.css',
				array(), 
				$this->version, 
				'all' 
			);
			wp_enqueue_style( 
				$this->plugin_name . '-bootstrap-tags-input', 
				plugin_dir_url( __FILE__ ) . 'css/bootstrap-tagsinput.css', 
				array(), 
				$this->version, 
				'all'
			);
			wp_enqueue_style( 
				$this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pu-portal-admin.css', 
				array(), 
				$this->version, 
				'all'
			); 

			wp_register_style( 
				'pronto-wp-portal-admin-general-settings-css', 
				plugin_dir_url( __FILE__ ) . 'css/pronto-wp-portal-admin-general-settings.css', 
				array(), 
				$this->version, 
				'all'
			);

			wp_register_style(
				'pronto-wp-portal-admin-login-css',
				plugin_dir_url( __FILE__ ) . 'css/pronto-wp-portal-admin-login.css',
				array(),
				$this->version,
				'all'
			);

			wp_register_style(
				'pronto-wp-portal-admin-profile-css',
				plugin_dir_url( __FILE__ ) . 'css/pronto-wp-portal-admin-profile.css',
				array(),
				$this->version,
				'all'
			);

			wp_register_style(
				'pronto-wp-portal-admin-registration-css',
				plugin_dir_url( __FILE__ ) . 'css/pronto-wp-portal-admin-registration.css',
				array(),
				$this->version,
				'all'
			);

			wp_register_style( 
				'pronto-wp-portal-admin-pages-css', 
				plugin_dir_url( __FILE__ ) . 'css/pronto-wp-portal-admin-pages.css', 
				array(), 
				$this->version, 
				'all'
			);

		} elseif ( 'user-edit.php' == $hook || 'user-new.php' == $hook ) {
			wp_enqueue_style( 
				$this->plugin_name . '-portal-admin-user-profiles', 
				plugin_dir_url( __FILE__ ) . 'css/pu-portal-admin-user-profiles.css',
				array(), 
				$this->version, 
				'all'
			); 
		}

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts( $hook ) {

		if($hook == "webforce-connect_page_pronto-portal") {

			wp_enqueue_script( 
				$this->plugin_name . 'taginsputjs', 
				plugin_dir_url( __FILE__ ) . 'js/jquery.tagsinput.js', 
				array( 'jquery' ), 
				$this->version, 
				false 
			);
			wp_enqueue_script( 
				$this->plugin_name . 'jqueryui', 
				plugin_dir_url( __FILE__ ) . 'js/jquery-ui.min.js', 
				array( 'jquery' ), 
				$this->version, 
				false 
			);
			wp_enqueue_script( 
				$this->plugin_name . '-bootstrap-tags-input', 
				plugin_dir_url( __FILE__ ) . 'js/bootstrap-tagsinput.js', 
				array( 'jquery' ), 
				$this->version, 
				false 
			);
			wp_enqueue_script( 
				$this->plugin_name, 
				plugin_dir_url( __FILE__ ) . 'js/pu-portal-admin.js', 
				array( 'jquery' ), 
				$this->version, 
				false 
			);


			/*
			 * register script general-settings js
			 */
			wp_register_script( 
				'pronto-wp-portal-admin-general-settings-js', 
				plugin_dir_url( __FILE__ ) . 'js/pronto-wp-portal-admin-general-settings.js', 
				array( 'jquery' ), 
				$this->version, 
				false 
			);

			/*
			* Localize "wfc_syncer_generalsettings_param" on 'pwp_syncer-general-settings' script 
			*/	
			wp_localize_script( 
				'pronto-wp-portal-admin-general-settings-js', 
				'wfc_portal_generalsettings_param',
				array( 
					'url' => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('ajax-nonce'), 
				) 
			);


			/*
			 * register script pages js
			 */
			wp_register_script( 
				'pronto-wp-portal-admin-pages-js', 
				plugin_dir_url( __FILE__ ) . 'js/pronto-wp-portal-admin-pages.js', 
				array( 'jquery' ), 
				$this->version, 
				false 
			);

			/*
			* Localize "wfc_portal_pages_param" on 'pwp_syncer-pages' script 
			*/	
			wp_localize_script( 
				'pronto-wp-portal-admin-pages-js', 
				'wfc_portal_pages_param',
				array( 
					'url' => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('ajax-nonce'), 
				) 
			);

			/*
			 * register script pages js
			 */
			wp_register_script(
				'register-settings-js',
				plugin_dir_url( __FILE__ ) . 'js/registration-settings.js',
				array( 'jquery' ),
				$this->version,
				false
			);

			/*
			* Localize "wfc_portal_pages_param" on 'pwp_syncer-pages' script
			*/
			wp_localize_script(
				'register-settings-js',
				'wfc_portal_reg_param',
				array(
					'url' => admin_url('admin-ajax.php'),
					'nonce' => wp_create_nonce('ajax-nonce'),
				)
			);


			/*
			 * register script login save settings js
			 *
			 * @since    1.0.0
			 */
			wp_register_script(
				$this->plugin_name . 'admin-login',
				plugin_dir_url( __FILE__ ) . 'js/pu-portal-admin-login.js',
				array( 'jquery' ),
				$this->version,
				false
			);
			
			/*
			 * Script localization
			 *
			 * @since    1.0.0
			 */
			wp_localize_script( 
				$this->plugin_name, 
				'PUP', 
				array( 
					'AJAX_URL'	=> admin_url( 'admin-ajax.php'), 
					'NONCE' 	=> wp_create_nonce( 'pup-save-settings' ),
					'LOGIN_NONCE' 	=> wp_create_nonce( 'pup-save-login-settings' ),
					'REG_NONCE' 	=> wp_create_nonce( 'pup-save-registration-settings' ),
					'PROF_NONCE' 	=> wp_create_nonce( 'pup-save-profile-settings' ),
					'EMAIL_NONCE' 	=> wp_create_nonce( 'pup-save-email-settings' ),
					'REGSET_MSG_DUPLICATE' 	=> __('Cannot remove group. Meta Field is currently used in other settings.', 'pu-portal')
				)
			); 
		}

		if ( 'user-edit.php' == $hook || 'user-new.php' == $hook ) {
			wp_enqueue_script( 
				$this->plugin_name . 'user-profiles',
				plugin_dir_url( __FILE__ ) . 'js/pu-portal-admin-user-profiles.js',
				array( 'jquery' ),
				$this->version,
				false
			);
		}
	}

	/**
	 * Register the portal sub menu for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function admin_menu() {

		add_submenu_page(
			'pronto-ultimate',
			__('Portal', 'pu-portal'),
			__('Portal', 'pu-portal'),
			'manage_options',
			'pronto-portal',
			array( $this, 'admin_page' )
		);

	}

	/** 
	 * Register the sub menu page callback.
	 *
	 * @since    1.0.0
	 */
	public function admin_page() {

		if( class_exists( 'Pronto_Base' ) ) {

			$data['base'] = $this->includes->get_base();
			$data['meta_keys'] = $this->get_all_user_meta();

			echo $this->tmpl_view( 'pronto-wp-portal-admin-display', $data );

			// enqueue styles and scripts to their respective menu
			if (isset($_GET['menu'])) {
			    switch ($_GET['menu']) {
                    case 'general_settings':
	                    wp_enqueue_script('pronto-wp-portal-admin-general-settings-js');
	                    wp_enqueue_style('pronto-wp-portal-admin-general-settings-css');
                        break;
                    case 'login':
	                    wp_enqueue_style('pronto-wp-portal-admin-login-css');
                        break;
                    case 'profile':
	                    wp_enqueue_style('pronto-wp-portal-admin-profile-css');
                        break;
                    case 'registration':
	                    wp_enqueue_style('pronto-wp-portal-admin-registration-css');
	                    wp_enqueue_script('register-settings-js');
                        break;
                    case 'pages':
	                    wp_enqueue_script('pronto-wp-portal-admin-pages-js');
	                    wp_enqueue_style('pronto-wp-portal-admin-pages-css');
                        break;
                }
            }
			/*
			*	enqueue Genral settings js and css
			*/
			/*
			if(isset($_GET['menu']) && $_GET['menu'] == 'general_settings'){
				wp_enqueue_script('pronto-wp-portal-admin-general-settings-js');
				wp_enqueue_style('pronto-wp-portal-admin-general-settings-css');
			}
			*/

			/*
			*	enqueue pages js and css
			*/
			/*
			if(isset($_GET['menu']) && $_GET['menu'] == 'pages'){
				wp_enqueue_script('pronto-wp-portal-admin-pages-js');
				wp_enqueue_style('pronto-wp-portal-admin-pages-css');
			}
			*/


			wp_enqueue_style( 'wfc-BootstrapCDN-css' );

		}
		
	}
	
	
	/**
	 * This functions is the callback for save general settings ajax.
	 *
	 * @author Junjie Canonio
     * @LastUpdated July 10, 2017
	 */
	public function wfc_portal_savegeneralsettings_callback(){

		if ( isset( $_POST['action'] ) && $_POST['action'] === 'wfc_portal_generalsettings_Save' ) {

			/*
			* parse data for saving
			*/
			$data_post = array();
			if (!empty($_POST['dataform']) && is_array($_POST['dataform'])) {
				foreach ($_POST['dataform'] as $key => $value) {
					$data_post[$value['name']] = filter_var ( $value['value'], FILTER_SANITIZE_STRING);
				}
			}

			$WPPSSreg_settings_arr	   = array();
			$WPPSSreg_settings = get_option('reg_settings',1) ;
			if( is_array($WPPSSreg_settings) && !empty($WPPSSreg_settings ) ){
				foreach ($WPPSSreg_settings as $key => $value) {
					$salesforce_field 	   = isset($value['salesforce_field']) ? $value['salesforce_field'] : array();
					$salesforce_field_name = isset($salesforce_field['name']) ? $salesforce_field['name'] : '';
					array_push($WPPSSreg_settings_arr, $salesforce_field_name);
				}
			}
			$WPPSSreg_settings  	   = json_encode($WPPSSreg_settings_arr);


			$WPPortalSyncSettings 	   		 = get_option( 'WPPortalSyncSettings', true );
    	    $WPPortalSyncScheduleSFID  		 = isset( $WPPortalSyncSettings['WPPortalSync-SFID'] ) ? $WPPortalSyncSettings['WPPortalSync-SFID'] : '';
			$WPPortalSyncEnableDisable 		 = isset( $data_post['WPPortalSync-enable-sf-sync'] ) ? $data_post['WPPortalSync-enable-sf-sync'] : 'false';
			$WPPortalSyncTimeSchedule  		 = isset( $data_post['WPPortalSync-time-schedule'] ) ? $data_post['WPPortalSync-time-schedule'] : '1day';	
			$WPPortalSyncAPIEndpoint   		 = isset( $data_post['WPPortalSync-resync-api-endpoint'] ) ? $data_post['WPPortalSync-resync-api-endpoint'] : '';
			$WPPortalDeleteUserAPIEndpoint   = isset( $data_post['WPPortal-DeleteUser-api-endpoint'] ) ? $data_post['WPPortal-DeleteUser-api-endpoint'] : '';

			$WPPortalSyncEnableDeltaSyncing  = isset( $data_post['WPPortalSync-enable-delta-syncing'] ) ? $data_post['WPPortalSync-enable-delta-syncing'] : 'false';
			$WPPortalSyncRecordModifiedDateFrom  = isset( $data_post['WPPortalSync-sync-record-modified-datefrom'] ) ? $data_post['WPPortalSync-sync-record-modified-datefrom'] : '';
			$WPPortalSyncRecordModifiedDateFromTimestamp  = isset( $data_post['WPPortalSync-sync-record-modified-datefrom-timestamp'] ) ? $data_post['WPPortalSync-sync-record-modified-datefrom-timestamp'] : '';
			$WPPortalSyncRecordModifiedDateTo = isset( $data_post['WPPortalSync-sync-record-modified-dateto'] ) ? $data_post['WPPortalSync-sync-record-modified-dateto'] : '';
			$WPPortalSyncRecordModifiedDateToTimestamp  = isset( $data_post['WPPortalSync-sync-record-modified-dateto-timestamp'] ) ? $data_post['WPPortalSync-sync-record-modified-dateto-timestamp'] : '';

			$OtherSettings = array(
				'WPPortalAPIEndpoint' => $WPPortalSyncAPIEndpoint,
				'WPPortalDeleteUserAPIEndpoint' => $WPPortalDeleteUserAPIEndpoint,
				'WPPortalSyncEnableDeltaSyncing' => $WPPortalSyncEnableDeltaSyncing,
				'WPPortalSyncRecordModifiedDateFrom' => $WPPortalSyncRecordModifiedDateFrom,
				'WPPortalSyncRecordModifiedDateFromTimestamp' => $WPPortalSyncRecordModifiedDateFromTimestamp,
				'WPPortalSyncRecordModifiedDateTo' => $WPPortalSyncRecordModifiedDateTo,
				'WPPortalSyncRecordModifiedDateToTimestamp' => $WPPortalSyncRecordModifiedDateToTimestamp
			);

			if( !empty($WPPortalSyncScheduleSFID) && $WPPortalSyncScheduleSFID != '' ){
		    	$APIBody = array(
			    	'EnableDisable' 	     		=> $WPPortalSyncEnableDisable, 
		     		'TimeSchedule'	  	     		=> $WPPortalSyncTimeSchedule,
		     		'ProntoSFPortalFieldMap' 		=> $WPPSSreg_settings,
				    'OtherSettings' => json_encode($OtherSettings, JSON_UNESCAPED_SLASHES),
				    'ExtraArgs' => ''
			    );
				$data_Arr = array(
				   	'SFID'    => $WPPortalSyncScheduleSFID,
					'APIBody' => $APIBody
				);

				$result = $this->Pronto_WP_Portal_update_sfportalsyncsettings( $data_Arr );
				$status_code = isset( $result['status_code'] ) ? $result['status_code'] : '';
				if( $status_code == 200 ){
					$UpdateSFSyncerSettingsStatus = isset( $result['sf_response']['status'] ) ? $result['sf_response']['status'] : '';
	    	    	$UpdateSFSyncerSettingsSFID   = isset( $result['sf_response']['SFID'] ) ? $result['sf_response']['SFID'] : '';
    	    		if( $UpdateSFSyncerSettingsStatus == 'Success' ){
			    	    $time_schedule = array(
			    	    	'WPPortalSync-enable-sf-sync'      => $WPPortalSyncEnableDisable,
			    	    	'WPPortalSync-time-schedule'       => $WPPortalSyncTimeSchedule ,
			    	    	'WPPortalSync-SFID'			       => $UpdateSFSyncerSettingsSFID,
			    	    	'WPPortalSync-resync-api-endpoint' => $WPPortalSyncAPIEndpoint,
			    	    	'WPPortal-DeleteUser-api-endpoint' => $WPPortalDeleteUserAPIEndpoint,
					        'WPPortalSync-enable-delta-syncing'  => $WPPortalSyncEnableDeltaSyncing,
					        'WPPortalSync-sync-record-modified-datefrom'  => $WPPortalSyncRecordModifiedDateFrom,
					        'WPPortalSync-sync-record-modified-datefrom-timestamp'  => $WPPortalSyncRecordModifiedDateFromTimestamp,
					        'WPPortalSync-sync-record-modified-dateto'  => $WPPortalSyncRecordModifiedDateTo,
					        'WPPortalSync-sync-record-modified-dateto-timestamp'  => $WPPortalSyncRecordModifiedDateToTimestamp
			    	    );
			    	    update_option( 'WPPortalSyncSettings', $time_schedule ); 
					    wp_send_json( array(
							'ACTION'  	  => __('Save Settings', 'pu-portal'),
							'STATUS' 	  => 'Successfull',
							'SF_RESPONCE' => $result,
							'WP_POST' => $_POST,
							'data_post' => $data_post,
						) );	  	    			
    	    		}else{
			    	    $time_schedule = array(
			    	    	'WPPortalSync-enable-sf-sync'      => $WPPortalSyncEnableDisable,
			    	    	'WPPortalSync-time-schedule'       => $WPPortalSyncTimeSchedule ,
			    	    	'WPPortalSync-SFID'			       => '',
			    	    	'WPPortalSync-resync-api-endpoint' => $WPPortalSyncAPIEndpoint,
			    	    	'WPPortal-DeleteUser-api-endpoint' => $WPPortalDeleteUserAPIEndpoint,
					        'WPPortalSync-enable-delta-syncing'  => $WPPortalSyncEnableDeltaSyncing,
					        'WPPortalSync-sync-record-modified-datefrom'  => $WPPortalSyncRecordModifiedDateFrom,
					        'WPPortalSync-sync-record-modified-datefrom-timestamp'  => $WPPortalSyncRecordModifiedDateFromTimestamp,
					        'WPPortalSync-sync-record-modified-dateto'  => $WPPortalSyncRecordModifiedDateTo,
					        'WPPortalSync-sync-record-modified-dateto-timestamp'  => $WPPortalSyncRecordModifiedDateToTimestamp
			    	    );
    	    			update_option( 'WPPortalSyncSettings', $time_schedule ); 
					    wp_send_json( array(
							'ACTION'  	  => __('Save Settings', 'pu-portal'),
							'STATUS' 	  => 'Fail',
							'SF_RESPONCE' => $result,
							'WP_POST' => $_POST,
							'data_post' => $data_post,
						) );						
					}
				}else{
				    wp_send_json( array(
						'ACTION'  	  	 =>__('Save Settings', 'pu-portal'),
						'STATUS' 	  	 => 'Fail',
						'SF_RESPONCE' 	 => 0,
						'DEBUG_RESPONSE' => $result,
						'WP_POST' => $_POST,
							'data_post' => $data_post,
					) );		    		
		    	}
			}else{
		    	$APIBody = array(
			    	'EnableDisable' 	     		=> $WPPortalSyncEnableDisable, 
		     		'TimeSchedule'	  	     		=> $WPPortalSyncTimeSchedule,
		     		'ProntoSFPortalFieldMap' 	    => $WPPSSreg_settings,
				    'OtherSettings' => json_encode($OtherSettings, JSON_UNESCAPED_SLASHES),
                    'ExtraArgs' => ''
			    );
				$data_Arr = array(
			    	'APIBody' => $APIBody
			    );
				$result = $this->Pronto_WP_Portal_create_sfportalsyncsettings( $data_Arr );
		    	$status_code = isset( $result['status_code'] ) ? $result['status_code'] : '';
		    	if( $status_code == 200 ){
	    	    	$CreateSFSyncerSettingsStatus = isset( $result['sf_response']['status'] ) ? $result['sf_response']['status'] : '';
	    	    	$CreateSFSyncerSettingsSFID   = isset( $result['sf_response']['SFID'] ) ? $result['sf_response']['SFID'] : '';
    	    		if( $CreateSFSyncerSettingsStatus == 'Success' ){
			    	    $time_schedule = array(
			    	    	'WPPortalSync-enable-sf-sync' 	   => $WPPortalSyncEnableDisable,
			    	    	'WPPortalSync-time-schedule'  	   => $WPPortalSyncTimeSchedule ,
			    	    	'WPPortalSync-SFID'				   => $CreateSFSyncerSettingsSFID,
					    	'WPPortalSync-resync-api-endpoint' => $WPPortalSyncAPIEndpoint,
			    	    	'WPPortal-DeleteUser-api-endpoint' => $WPPortalDeleteUserAPIEndpoint,
					        'WPPortalSync-enable-delta-syncing'  => $WPPortalSyncEnableDeltaSyncing,
					        'WPPortalSync-sync-record-modified-datefrom'  => $WPPortalSyncRecordModifiedDateFrom,
					        'WPPortalSync-sync-record-modified-datefrom-timestamp'  => $WPPortalSyncRecordModifiedDateFromTimestamp,
					        'WPPortalSync-sync-record-modified-dateto'  => $WPPortalSyncRecordModifiedDateTo,
					        'WPPortalSync-sync-record-modified-dateto-timestamp'  => $WPPortalSyncRecordModifiedDateToTimestamp
			    	    );
			    	    update_option( 'WPPortalSyncSettings', $time_schedule ); 
					    wp_send_json( array(
							'ACTION'  	  => __('Save Settings', 'pu-portal'),
							'STATUS' 	  => 'Successfull',
							'SF_RESPONCE' => $result,
							'WP_POST' => $_POST
						) );
					}else{
			    	    $time_schedule = array(
			    	    	'WPPortalSync-enable-sf-sync' 	   => $WPPortalSyncEnableDisable,
			    	    	'WPPortalSync-time-schedule'  	   => $WPPortalSyncTimeSchedule ,
			    	    	'WPPortalSync-SFID'				   => $CreateSFSyncerSettingsSFID,
					    	'WPPortalSync-resync-api-endpoint' => $WPPortalSyncAPIEndpoint,
					    	'WPPortal-DeleteUser-api-endpoint' => $WPPortalDeleteUserAPIEndpoint,
					        'WPPortalSync-enable-delta-syncing'  => $WPPortalSyncEnableDeltaSyncing,
					        'WPPortalSync-sync-record-modified-datefrom'  => $WPPortalSyncRecordModifiedDateFrom,
					        'WPPortalSync-sync-record-modified-datefrom-timestamp'  => $WPPortalSyncRecordModifiedDateFromTimestamp,
					        'WPPortalSync-sync-record-modified-dateto'  => $WPPortalSyncRecordModifiedDateTo,
					        'WPPortalSync-sync-record-modified-dateto-timestamp'  => $WPPortalSyncRecordModifiedDateToTimestamp
			    	    );
			    	    update_option( 'WPPortalSyncSettings', $time_schedule ); 
					    wp_send_json( array(
							'ACTION'  	  => __('Save Settings'),
							'STATUS' 	  => 'Warn',
							'SF_RESPONCE' => $result,
							'WP_POST' => $_POST
						) );						
					}
					// print_r($result);
		    	}else{
				    wp_send_json( array(
						'ACTION'  	  	 => __('Save Settings', 'pu-portal'),
						'STATUS' 	  	 => 'Fail',
						'SF_RESPONCE' 	 => 0,
						'DEBUG_RESPONSE' => $result,
						'WP_POST' => $_POST
					) );		    		
		    	}
		    }	
		}
	}
	
	/**
	 * This function calls a REST API request to salesforce to update sfportalsyncsettings schedule.
	 *
	 * @author Junjie Canonio
	 *
	 * @param $data
	 *
	 * @return array|object
     *
	 */
   	public function Pronto_WP_Portal_create_sfportalsyncsettings( $data ) {
   		$return_val = array();
   		$api_body	= isset($data['APIBody']) ? $data['APIBody'] : array();
   		$oauth 	    = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );
   		if( !empty($api_body) ){
   			$return_val = $oauth->api_request( 'apexrest/ASPU/ProntoSFPortalSyncSettingsAPI/', $api_body, 'create', true, false ) ;
   		}
   		return $return_val;
   	}

	/**
	 * This function calls a REST API request to salesforce to update sfportalsyncsettings schedule.
     *
	 * @author Junjie Canonio
     * @param array
	 * @return array
	 */
   	public function Pronto_WP_Portal_update_sfportalsyncsettings( $data ) {
   		$return_val = array();
   		$pwp_syncer_sf_settings_id = isset($data['SFID']) ? $data['SFID'] : '';
   		$api_body = isset($data['APIBody']) ? $data['APIBody'] : array();
   		$oauth 	= new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );
   		if( !empty($pwp_syncer_sf_settings_id) && !empty($api_body) ){
   			$return_val = $oauth->api_request( 'apexrest/ASPU/ProntoSFPortalSyncSettingsAPI/'.$pwp_syncer_sf_settings_id, $api_body , 'update', true, false ) ;
   		}
   		return $return_val;
   	}
	
	/**
	 * This function calls fetch the contact count from Salesforce.
	 *
	 * @author Junjie Canonio
	 *
	 * @return mixed
	 */
   	public function Pronto_WP_Portal_getContactCount(){
   		$queryResult['Status'] = 'Failed';
   		$queryResult['Count']  = 0;
   		$queryResult['MSG']    = 'Fail';
   		try{
	   		$result = self::$oauth->api_request( "data/v39.0/query/?q=SELECT+count()+from+Contact+where+ASPU__Action__c='include'", '' , 'get' );
	   		$status_code = isset( $result['status_code'] ) ? $result['status_code'] : '';
			if( $status_code == 200 ){
				$queryResult['Status'] = 'Success';
				$queryResult['Count']  = isset( $result['sf_response']['totalSize'] ) ? $result['sf_response']['totalSize'] : 0;
				$queryResult['MSG']    = 'Fetch Success';
			}
   		}catch (Exception $e) {
   			$queryResult['Status'] = 'Error';
   			$queryResult['MSG']    = $e;
   		}
   		return $queryResult ;
   	}
	
	/**
	 * Register the template render for the admin area.
	 *
	 * @author   Danryl Carpio <danryl@alphasys.com.au>
	 *
	 * @param string $template will be the name of admin partials files
	 * @param array  $params   variables available for template
	 *
	 * @return false|string
	 * @since    1.0.0
     *
     * @LastUpdated  July 4, 2017
	 */
	public function tmpl_view( $template, $params = array() ) {
		ob_start();
		extract( $params );
		require( plugin_dir_path( __FILE__ ) . 'partials/' . $template . '.php' );
		$var = ob_get_contents();
		ob_end_clean();
		return $var;
	}
	
	/**
	 * Get all user meta keys.
	 *
	 * @author   Danryl Carpio <danryl@alphasys.com.au>
	 * @return array|null|object
	 * @since    1.0.0
     *
     * @LastUpdated  July 11, 2017
	 */
	public function get_all_user_meta() {
		global $wpdb;
		return $wpdb->get_results( "SELECT meta_key FROM $wpdb->usermeta GROUP BY meta_key", ARRAY_A );
	}
	
	/**
	 * Enqueues scripts for portal admin pages.
     *
	 * @author   Danryl Carpio <danryl@alphasys.com.au>
	 *
	 * @param $hook
	 * @since    1.0.0
     *
     * @LastUpdated  July 11, 2017
	 */
	public function portal_enqueue_scripts( $hook ) {
		$prefix = 'webforce-connect_page_';
		switch ( $hook ) {
			case $prefix . 'pronto-portal':
				
				// login settings
				wp_enqueue_script( $this->plugin_name . 'admin-login' );

			break;
		}
	}
	
	/**
	 * Ajax callback function to create/update login settings.
	 *
	 * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
	 * @since         1.0.0
     *
     * @LastUpdated    August 8, 2017
	 */
	public function pup_save_login_settings() {
		
		if ( ! check_ajax_referer( 'pup-save-login-settings', 'nonce' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Invalid Nonce.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
		
		if ( ! current_user_can( 'manage_options' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('You are not allow to do this.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
	
		$outer = $fldgroup = $data = array();
		$counter = 0;
		
		if( isset( $_POST['data'] ) && ! empty( $_POST['data'] ) ) {
		 	foreach( $_POST['data'] as $key => $val ) {
				$counter2 = 0;
				foreach( $val['fldgroup'] as $key2 => $val2 ) {
					
					foreach( $val2['data'] as $key3 => $val3 ) {
						$data[ $val3['name'] ] = ( $val3['name'] == 'metavalue' || $val3['name'] == 'errmsg' ) ? sanitize_text_field( $val3['value'] ) : $val3['value'];
					}
					
					$metavalue = isset($data['metavalue']) ? trim( $data['metavalue'] ) :'';
					if( ! ( empty( $data['metakey'] )  && empty( $data['meta_tags'] ) ) 
					&& ! empty( $data['metafield'] ) 
					&& ! empty( $metavalue ) ) {
						$fldgroup[$counter2] = array(
							'data' 	=> $data,
							'op'	=>	( $counter2 == 0 ) ? '' : $val2['op']
						);
						$counter2++;
					}
					
					$data = array();
				}

				if( ! empty( $fldgroup ) ){
					$outer[ $counter ] = array(
						'fldgroup' => $fldgroup,
						'op'	=> $val['op']
					);
					$counter++;
					$outer[0]['op'] = "";
				}

				$fldgroup = array();
			}

			

			$changes = update_option( 'login_filter', $outer );
			$success = true;
			
			/* echo wp_send_json( array( */
			echo json_encode( array(
				'message'	=>	__('Settings is saved successfully.', 'pu-portal'),
				'type'	=> 	'success-notice',
				'outer'	=>	$outer,
			), JSON_PRETTY_PRINT );
			
		} else {
			
			echo wp_send_json( array(
				'message'	=>	__('Settings not saved. No data received from the server.', 'pu-portal'),
				'type'	=> 	'error-notice'
			) );
		}
		
		wp_die();
	}



    /**
     * Ajax callback function to create/update users_can_register settings.
     *
     * @author		Excel Pulmano <excel@alphasys.com.au>
     * @since		1.0.0
     * @LastUpdated	July 25, 2018
     */
    public function wfc_reg_settings_callback() {
        if(isset($_POST['action']) && $_POST['action'] == 'register-settings-js'){

            $reg = isset($_POST['reg']) ? $_POST['reg']: '';
            $notehtml = isset($_POST['notehtml']) ? $_POST['notehtml']: '';
            $notext = isset($_POST['notext']) ? $_POST['notext']: '';
            $select = isset($_POST['selects']) ? $_POST['selects']: '';

            if($reg == 'false') {
                update_option('users_can_register', 0);
            }else{
                update_option('users_can_register', 1);
            }

            if($select == 'html'){
                if(! empty($notehtml)){
                    $notehtml = stripslashes($notehtml);
                    update_option('wfc_portal_reg_note', $notehtml);
                }
            }elseif ($select == 'text'){

                update_option('wfc_portal_reg_note_text',$notext);
            }
            update_option('wfc_portal_reg_text_format', $select);

            $reg_data = array(
                    $reg,
                $select,
                $notext,
                $notehtml);


            wp_send_json(
                array(
                    'status' => 'Success',
                    'data'   => $reg_data
                )
            );
            wp_die();

        }
    }

	
	/**
     * Ajax callback function to create/update registration settings.
     *
	 * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
	 * @since		1.0.0
     * @LastUpdated	August 8, 2017
     */
	public function pup_save_registration_settings() {

	    // some bug found -> 403
	    /*
		if ( ! check_ajax_referer( 'pup-save-registration-settings', 'nonce' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Invalid Nonce.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
	    */
		
		if ( ! current_user_can( 'manage_options' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('You are not allow to do this.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
		
		if( ! isset( $_POST['data'] ) && ! empty( $_POST['data'] ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Settings not saved. No data received from the server.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}

		$result = array();
		$counter = 0;
		$reg_settings_before_changes = get_option( 'reg_settings' );

		
		foreach ($_POST['data']['pup_duplicate_email'] as $key => $value) {
			// update_option( 'pup_duplicate_email', $value['value']);
		}
		$_POST['data'] = $_POST['data']['regSettings'];

		foreach( $_POST['data'] as $key => $val ) {

			$wp_user_meta = isset( $val['wp_user_meta'] ) ? trim( $val['wp_user_meta'] ) : '';

			if( ! empty( $val['salesforce_field'] ) && ! empty( $wp_user_meta ) ) {
				foreach( $val as $key2 => $val2 ) {
					if( $key2 === 'form_settings' ) {

						$form_settings = array();
						
						foreach( $val2 as $key3 => $val3 ) {
							$result[ $counter ][ $val3['name'] ] = ( 
								( $val['wp_user_meta'] == 'Email' 
								|| $val['wp_user_meta'] == 'ASPU__WPUsername__c')
								&& ( $val3['name'] == 'show_on_reg' 
								|| $val3['name'] == 'required_field' ) ) ? '1' : sanitize_text_field( $val3['value'] );
							
							$form_settings[] = array(
								'name' => $val3['name'],
								'value'=> $result[ $counter ][ $val3['name'] ]
							);
						}

						$result[ $counter ]['form_settings'] = $form_settings;
					} elseif( /* $key2 === 'wp_user_meta' && */ $val2 === 'new_user_meta' ) {
						$created_user_meta[ $result[ $counter ]['salesforce_field']['name'] ] = sanitize_text_field( $result[ $counter ]['default_value'] );
						$result[ $counter ][ $key2 ] = $result[ $counter ]['salesforce_field']['name'];
					} else {
						$result[ $counter ][ $key2 ] = $val2;
					}
				}
				$counter++;
			}
		}

		update_option( 'reg_settings', $result );

		/* Create user meta */
		if( ! empty( $created_user_meta ) ) {
			
			$all_users = get_users( array( 'fields' => array( 'ID' ) ) );
			$created_user_meta_op = get_option('created_user_meta');
			
			if( ! empty( $created_user_meta_op ) ) {
				$new_created_user_meta = array();
				
				foreach( $created_user_meta as $key => $val ) {
					if( ! array_key_exists( $key, $created_user_meta_op ) ) {
						$new_created_user_meta[ $key ] = $val;
					}
				}					
				
				$created_user_meta_op = array_merge( $created_user_meta_op, $new_created_user_meta );
				
				if( ! empty( $new_created_user_meta ) ) {
					foreach( $new_created_user_meta as $key => $val ){
						foreach( $all_users as $user ){
							add_user_meta( $user->ID, $key, $val );
						}
					}
				}
				
				$changes2 = update_option( 'created_user_meta', $created_user_meta_op );
			} else {
			
				foreach( $created_user_meta as $key => $val ){
					foreach( $all_users as $user ){
						add_user_meta( $user->ID, $key, $val );
					}
				}
				
				$changes2 = update_option( 'created_user_meta', $created_user_meta );
			}

		}
		
		$reg_settings = get_option( 'reg_settings' );
		
		/* get current registration settings for later use. */
		foreach( $reg_settings as $key => $val ) {
			$current_settings[ $val['wp_user_meta'] ] = $val['default_value'];
		}
		
		/* set array for unused wordpress usermeta in pronto-portal */
		foreach( $reg_settings_before_changes as $key => $val ) {
			if( ! array_key_exists( $val['wp_user_meta'], $current_settings ) ) {
				$reset_defalt_um[ $val['wp_user_meta'] ] = '';
			}
		}

		global $wpdb;
		
		/* preform update on default WP usermeta */
		if( ! empty( $reset_defalt_um ) ) {
			$all_non_admin_users = get_users( array( 'role__not_in' => array( 'administrator' ) ) );
			foreach( $reset_defalt_um as $key => $val ) {
				foreach( $all_non_admin_users as $user ){
					$wpdb->update( "$wpdb->usermeta", array( 'meta_value' => $val ), array( 'meta_key' => $key, 'user_id' => $user->ID ), array( '%s' ), array( '%s', '%d' ) );
				}
			}
		}

		$created_user_meta = get_option( 'created_user_meta' );	

		/* record updated list of created user meta, record unused user meta */
		if( ! empty( $created_user_meta ) ) {
			foreach( $created_user_meta as $key => $val ) {
				if( ! array_key_exists( $key , $current_settings) )
					$prev_created_um[] = $key;
				else
					$curr_created_um[ $key ] = $val;
			}

			/* save recorded list of newly created usermeta */
			update_option( 'created_user_meta', $curr_created_um );
			
			/* preform deletion of unused custom/created usermeta */
			if( ! empty( $prev_created_um ) ) {
				foreach( $prev_created_um as $key => $val ) {
					$wpdb->delete( "$wpdb->usermeta", array( 'meta_key' => $val ), array( '%s' ) );		
				}
			}
		}
		

		echo json_encode( array(
			'message'	=>	__( 'Settings is saved successfully.', 'pu-portal' ),
			'type'		=> 	'success-notice',
			'result'	=>	$result
		), 	JSON_PRETTY_PRINT );
		wp_die();
	}
	
	
	/**
	 * Ajax callback function to create/update pages settings.
	 *
	 * @author        Junjie Canonio
	 * @since         1.0.0
     *
     *
     * @LastUpdated    July 11, 2017
	 */
	public function pup_save_pages_settings() {
        
        if ( isset( $_POST['action'] ) && $_POST['action'] === 'pages_settings' ) {
			$result = update_option( 'pup_pages_settings', $_POST['dataform'] );
			wp_send_json( array(
				'ACTION'  => $_POST['action'],
				'STATUS'  => 'success',
				'WP_POST' => $_POST['dataform']
			) );			
        }
    }
	
	/**
     * Save a copy of SalesForce Contacts fields to an option `sf_fields`.
     *
	 * @author        John Rendhon Gerona <rendhon@alphasys.com.au>\n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param 		boolean		$sync		to force syncronize fields	(optional)
     *
     * @return		mixed		returns an array of SalesForce Contacts fields when `sf_fields` option is empty
	 *							or force syncing is enabled. | FALSE if wordpress `sf_fields` are already populated.
     * @since		1.0.0
     *
     * @LastUpdated	August 11, 2017
     */
	public static function get_salesforce_fields( $sync = FALSE ) {
		
        /* $result = self::$oauth->api_request( 'data/v39.0/sobjects/', $data, 'create' ); */
		$sf_fields = get_option('sf_fields');
		if( empty( $sf_fields ) || $sync === TRUE ) {
			$result = self::$oauth->api_request( 'data/v39.0/sobjects/Contact/describe', '', 'retrieve', true, false );
			$newArray = array();
			$sf_fields = isset( $result[ 'sf_response' ][ 'fields' ] ) ? $result[ 'sf_response' ][ 'fields' ] : '';
			if( '200' == $result[ 'status_code' ] && !empty( $sf_fields ) ) {
				foreach ( $sf_fields as $key => $sf_field ) {
					if( is_array( $sf_field ) && $sf_field[ 'name' ] != 'ASPU__Action__c' ) {
						$sf_fieldvar['name'] = $sf_field[ 'name' ];
						$sf_fieldvar['label'] = $sf_field[ 'label' ];
						$sf_fieldvar['type'] = $sf_field[ 'type' ];
						$sf_fieldvar['picklistValues'] = $sf_field[ 'picklistValues' ];
						$sf_fieldvar['referenceTo'] = $sf_field[ 'referenceTo' ];
						array_push( $newArray, $sf_fieldvar );
					}
				}
				
				update_option('sf_fields', $newArray);
				return get_option('sf_fields');
			}
		} else {
			return false;
		}	
    }
	
	/**
	 * Add a contact record in SalesForce Contacts.
	 *
	 * @author        John Rendhon Gerona <rendhon@alphasys.com.au>\n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param array $object_curl the array containing the unique SalesForce Contact field key and the desired value    (required)
	 *
	 * @return mixed
	 * @since         1.0.0
     *
     * @LastUpdated    August 11, 2017
	 */
	public static function add_salesforce_fields( $object_curl ) {
		
		if( $object_curl != null ) {
			$result = self::$oauth->api_request( 'data/v39.0/sobjects/Contact', $object_curl, 'create' );
		}
		return $result;
	} 
	
	/**
     * Callback function used to suggest a list of data to the `tags input` based on user's query,
	 *				suggestions are retrieved from the user meta.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au>\n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
     * @since		1.0.0
     *
     * @LastUpdated	August 24, 2017
     */
	public function pup_login_suggest_user_meta() {
		
		if ( ! check_ajax_referer( 'pup-save-settings', 'nonce' ) || ! current_user_can( 'manage_options' ) ) {
			echo wp_send_json( __('Invalid Nonce.', 'pu-portal') );
			wp_die();
		}
		
		$new_user_meta = array();
		
		if( isset( $_REQUEST['metafield'] ) && isset( $_REQUEST['term'] )  && ! empty( $_REQUEST['metafield'] ) ) {
			$metafield = $_REQUEST['metafield'];
			$term = $_REQUEST['term'];
			
			$user_meta = get_user_meta( get_current_user_id(), $metafield, true );
			
			foreach( $user_meta as $key => $val ) {
				if( strpos($val, $term) !== false ) {
					$new_user_meta[] = $val;
				}
			}
		}
		echo json_encode( $new_user_meta );
		wp_die();
	}
	
	/**
	 * Sets the directory to fetch API.
	 *
	 * @param $dirs
	 *
	 * @return array
	 * @since        1.0.0
	 */
	public function set_api_directory( $dirs ) {
		if( !is_array( $dirs ) ) {
			$dirs = array();
		}
		
		$dirs[] = plugin_dir_path( dirname( __FILE__ ) ) . 'includes';
		
		return $dirs;
	}
	
	/**
     * Callback function to syncronize SalesForce contacts fields using `Pronto_Wp_Portal_Admin::get_salesforce_fields`.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au>\n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
     * @since		1.0.0
     *
     * @LastUpdated	August 24, 2017
     */
	public function sync_sf_fields_callback() {
		
		if ( ! check_ajax_referer( 'pup-save-settings', 'nonce' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Invalid Nonce.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
		
		if ( ! current_user_can( 'manage_options' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('You are not allow to do this.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
		
		$result = self::get_salesforce_fields( TRUE );
		
		echo wp_send_json( array(
			'changes'	=>	$result,
			'message'	=>	__('SalesForce data successfully synced.', 'pu-portal'),
			'type'		=> 	'success-notice'
		) );
		wp_die();
	}
	
	/**
     * Function to display plugin-created user meta.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au>\n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
     * @param 		object		$user		the user object containing current/viewed user info (required)
     * @since		1.0.0
     *
     * @LastUpdated	August 30, 2017
     */
	public function view_created_pup_profile_fields( $user ) {
		if( gettype($user) == 'object' && ! user_can( $user, 'manage_options' ) ) {

			$created_user_meta = get_option('created_user_meta');
			$sf_fields = get_option('sf_fields');
			$avatar_url = get_user_meta( $user->ID, 'pup_avatar', 1 );
			$pup_sf_id = get_user_meta( $user->ID, 'pup_sf_id', 1 );
			/*$avatar = ( is_object( $user ) ) ? get_user_meta( $user->ID, 'pup_avatar', 1 ) : ' '; */
			
			if( ! empty( $sf_fields ) && ! empty( $created_user_meta )  ) : ?>

			<h3><?php _e('Pronto Ultimate Portal Fields', 'pu-portal'); ?></h3>

			<table class="form-table pup_profile_fields">
				<tr>
					<th><label for="avatar"><?php _e( 'Avatar', 'pu-portal'); ?></label></th>
					<td>
						<div class="img-container">
							<img 
								class="avatar-ctrl" 
								alt="pup-user-avatar" 
								src="<?php echo ( ! empty( $avatar_url ) ) ? $avatar_url : get_avatar_url( $user->ID ); ?>"
							/>
						</div>
						<div class="btn-container">
							<button type="button" class="button btn-upload-avatar"><?php _e( 'Upload', 'pu-portal'); ?></button>
							<p><i><?php _e('Maximum image size: 1MB.', 'pu-portal' ); ?></i></p>
						</div>
						<input type="file" style="display:none;" accept="image/jpeg, image/png" name="pup_avatar" class="img-upload-ctrl" />
					</td>
				</tr>
				<?php if( get_current_screen()->action != 'add' ) : ?>
				<tr>
					<th><label for="pup_sf_id"><?php _e( 'Salesforce ID', 'pu-portal'); ?></label></th>
					<td>
						<input 
							type="text" 
							name="pup_sf_id"
							id="pup_sf_id" 
							value="<?php echo $pup_sf_id; ?>" 
							class="regular-text" 
						/>
					</td>
				</tr>
				<?php endif; ?>
	<?php  	foreach( $created_user_meta as $key => $val ) : 
				foreach ( $sf_fields as $sf_field_key => $sf_field_val ) :
					if( $key == $sf_field_val['name'] ) : ?>
				<tr <?php echo ( $sf_field_val['name'] == 'Email') ? 'class="hidden"' : '';?>>
					<th><label for="<?php echo $key; ?>"><?php echo $key; ?></label></th>
					<td> 
				<?php	switch( $sf_field_val['type']) :

							case 'multipicklist' :	
								$type = 'picklistValues';
								
								if( ! empty( $sf_field_val[ $type ]  ) ) :
									$options = '';
									
									foreach( $sf_field_val[ $type ] as $opt ) :
										if( $type == 'picklistValues' ) {

											$selectArr = ( is_object( $user ) ) ? get_the_author_meta( $key, $user->ID ) : array();
											$postSelectArr = ( isset( $_POST[ $key ] ) ) ? $_POST[ $key ] : array();

											if ( ! is_array( $selectArr ) ) {
												$selectArr = explode( ';', $selectArr );
											}

											if ( ! is_array( $postSelectArr ) ) {
												$postSelectArr = explode( ';', $postSelectArr );
											}

											$selected = ( in_array( $opt['value'], $selectArr ) ) 
												? 'selected="selected"' 
												: ( ( in_array( $opt['value'], $postSelectArr ) ) 
													? 'selected="selected"' 
													: '');
										
											$options .= '<option ' . $selected . ' value="' . $opt['value'] . '">' . $opt['label'] . '</option>';
										}
									endforeach;
								endif; ?>

								<input 
									type='hidden' 
									value='' 
									name="<?php echo $key; ?>" 
								/>

								<select 
									style="min-width: 38.2%;height: auto;" 
									class="regular-text" 
									multiple 
									size="5" 
									name="<?php echo $key; ?>[]" 
									id="<?php echo $key; ?>"
								>
									<?php echo $options; ?>
								</select>
					<?php	break;	

							case 'picklist' :
							case 'reference' :	
								$type = ( $sf_field_val['type'] == 'picklist' ) ? 'picklistValues' : 'referenceTo';

								if( ! empty( $sf_field_val[ $type ]  ) ) :
									$options = '';
									
									foreach( $sf_field_val[ $type ] as $opt ) :
										if( $type == 'picklistValues' ) {
											$selected = ( is_object( $user )  ) ? ( ( esc_attr( get_the_author_meta( $key, $user->ID ) ) == $opt['value'] ) ? 'selected="selected"' : '' ) : ( ( isset( $_POST[ $key ] ) && $_POST[ $key ] == $opt['value'] ) ? 'selected="selected"' : '' ) ;
											$options .= '<option ' . $selected . ' value="' . $opt['value'] . '">' . $opt['label'] . '</option>';
										} else {
											$selected = ( is_object( $user )  ) ? ( ( esc_attr( get_the_author_meta( $key, $user->ID ) ) == $opt ) ? 'selected="selected"' : '' ) : ( ( isset( $_POST[ $key ] ) && $_POST[ $key ] == $opt ) ? 'selected="selected"' : '' ) ;
											$options .= '<option ' . $selected . ' value="' . $opt . '">' . $opt . '</option>';
										}
									endforeach;
									
								endif; ?>
								
								<select 
									name="<?php echo $key; ?>" 
									id="<?php echo $key; ?>"
								>
									<?php echo $options; ?>
								</select>
						<?php	break;				
							
							case 'textarea' : ?>
								<textarea 
									class="regular-text" 
									name="<?php echo $key; ?>" id="<?php echo $key; ?>" 
								/><?php echo ( is_object( $user ) ) ? esc_attr( get_the_author_meta( $key, $user->ID ) ) : ( ( isset( $_POST[ $key ] ) ) ? $_POST[ $key ] : '' ); ?></textarea>
						<?php	break;
								
							case 'boolean' : ?>
								<fieldset><legend class="screen-reader-text"><span><?php echo $key; ?></span></legend>
									<label for="<?php echo $key; ?>">
										<input 
											id="<?php echo $key; ?>" 
											type='hidden' 
											value='0' 
											name="<?php echo $key; ?>" />
										<input 
											type="checkbox" 
											name="<?php echo $key; ?>" 
											id="<?php echo $key; ?>" 
											value='1' <?php echo ( is_object( $user ) && esc_attr( get_the_author_meta( $key, $user->ID ) ) == '1' ) ? 'checked="checked"' : ( ( isset( $_POST[ $key ] ) ) ? $_POST[ $key ] : '' ); ?>/>
									</label>
								</fieldset>
						<?php	break;
					
							default : ?>
								<input 
									type="<?php
										if ( $sf_field_val['type'] == 'email' || $sf_field_val['type'] == 'url' || $sf_field_val['type'] == 'date' ) 
											echo $sf_field_val['type'];
										elseif( $sf_field_val['type'] == 'datetime' )
											echo 'datetime-local';
										elseif( $sf_field_val['type'] == 'phone' )
											echo 'tel';
										else echo 'text';
									?>" 
									name="<?php echo $key; ?>"
									id="<?php echo $key; ?>" 
									value="<?php 
										if( is_object($user) ) {
											if( $sf_field_val['type'] == 'date' )
												echo date( "Y-m-d", strtotime( esc_attr( get_the_author_meta( $key, $user->ID ) ) ) );
											elseif( $sf_field_val['type'] == 'datetime' )
												echo date( "Y-m-d\TH:i:s", strtotime( esc_attr( get_the_author_meta( $key, $user->ID ) ) ) );
											else 
												echo esc_attr( get_the_author_meta( $key, $user->ID ) );
										}
										elseif( isset( $_POST[ $key ] ) )
											echo $_POST[ $key ];
										else 
											echo '';
							
									?>" 
									class="regular-text" 
								/>
						<?php	break;
								
						endswitch; ?>
					</td>
				</tr>
			<?php	endif;
				endforeach;
			endforeach; ?>

			</table>		
	<?php 	endif;
		}
	}
	
	/**
     * Function to filter plugin-created user meta errors upon saving/updating.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au>\n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
     * @param 		object		$error		the WP_Error Object.
     * @param 		bool	$update		the current operation.
     * @param 		mixed	$user		Wordpress User Object if update is true.
     * @since		1.0.0
     *
     * @LastUpdated	September 30, 2017
     */
	public function filter_created_pup_profile_fields( $error, $update, $user ) {

		if( isset( $_FILES['pup_avatar'] ) 
		&& ! empty( $_FILES['pup_avatar']['name'] ) 
		&& $_FILES['pup_avatar']['size'] != '0' 
		&& $_FILES['pup_avatar']['error'] != '4' ) {
			$allowedImageType = array("image/jpeg", "image/png");
			
			if( ! in_array( $_FILES['pup_avatar']['type'], $allowedImageType ) ) {
				
				$error->add( 'file_not_supported', __( '<strong>ERROR</strong>: File type not supported.', 'pu-portal' ) );
			}
			
			if( $_FILES["pup_avatar"]["size"] > 1000000 ) {
				$error->add( 'file_size_exceeded', __( '<strong>ERROR</strong>: Image exceeds the maximum size of 1MB.', 'pu-portal' ) );
			}
			
			$_POST['files']['has_avatar'] = 1;
			$_POST['files']['avatar'] = $_FILES['pup_avatar'];
		}

		if( get_option( 'pup_duplicate_email' ) == "1") {
			if( isset($error->errors['email_exists'])) {
		       $error->remove('email_exists');
		    }
		}

		if( ! $error->get_error_codes() ) {
			
			$sf_fields_arr = $meta_data = array();
			$reg_settings = get_option( 'reg_settings' );
 
			$sf_fields_arr['Email'] = sanitize_text_field( $_POST['email'] );

			foreach( $reg_settings as $key => $val ) {
				if ( array_key_exists( $val['salesforce_field']['name'], $_POST ) 
				/* || ( $val['salesforce_field']['name'] != $val['wp_user_meta'] ) */ ) {

                    // var_dump($val['wp_user_meta']);

                    $val['wp_user_meta'] = apply_filters('get_sf_field_from_user_meta', $val['wp_user_meta']);

                    // var_dump($val['wp_user_meta']);
					
					$sf_umeta[ $val['wp_user_meta'] ] = ( ! is_array( $_POST[ $val['wp_user_meta'] ] ) ) ? sanitize_text_field( $_POST[ $val['wp_user_meta'] ] ) : $_POST[ $val['wp_user_meta'] ];
					
					if( $val['salesforce_field']['type'] == 'date' || $val['salesforce_field']['type'] == 'datetime	' ) {
					    // add date to sf data to save
                        $sf_fields_arr[ $val['salesforce_field']['name'] ] = $sf_umeta[ $val['wp_user_meta'] ] = date( "Y-m-d\TH:i:s", strtotime( sanitize_text_field( $_POST[ $val['wp_user_meta'] ] ) ) );
					
					} else if( $val['salesforce_field']['type'] == 'boolean' ) {
						$bol_val = sanitize_text_field( $_POST[ $val['wp_user_meta'] ] );
						$sf_fields_arr[ $val['salesforce_field']['name'] ] = ( $bol_val == '1' ) ? true : false;
						
					} elseif( $val['salesforce_field']['type'] == 'multipicklist' ) {

						$multipicklistv = '';
						if( ! empty( $_POST[ $val['wp_user_meta'] ] ) ) {
							foreach( $_POST[ $val['wp_user_meta'] ] as $multi_picklist_key => $multi_picklist_val){
								$multipicklistv = ( $multipicklistv == '' ) ? $multipicklistv . $multi_picklist_val : $multipicklistv . ';' . $multi_picklist_val;
							}
						}
						$sf_fields_arr[ $val['salesforce_field']['name'] ] = $multipicklistv;

					} else {
						$sf_fields_arr[ $val['salesforce_field']['name'] ] = sanitize_text_field( $_POST[ $val['wp_user_meta'] ] );
					}
				}
			}
			
			if ( ! isset( $_POST['first_name'] ) || sanitize_text_field( $_POST['first_name'] ) == '' ) {
				$_POST['first_name'] = 'Contact';
			}
			if ( ! isset( $_POST['last_name'] ) || sanitize_text_field( $_POST['last_name'] ) == '' ) {
				$_POST['last_name'] = 'Anonymous';
			}

			$sf_fields_arr['FirstName'] = $_POST['first_name'];
			$sf_fields_arr['LastName'] = $_POST['last_name'];

			if( $update && ! user_can( $_POST['user_id'], 'manage_options' ) ) {
				$pup_sf_id = isset($_POST['pup_sf_id']) ? sanitize_text_field( $_POST['pup_sf_id'] ) : '';
				if( ! isset( $_POST['pup_sf_id'] ) || ( isset( $_POST['pup_sf_id'] ) && empty( $pup_sf_id ) ) ) {	
					if ( current_user_can('editor') || current_user_can('administrator')) {
						// Don nothing
					} else {
						$error->add( 'empty_sf_id', __('<strong>ERROR: </strong>Cannot update. Record does not exists in SalesForce.', 'pu-portal') );
					}			
				} elseif( ! user_can( $_POST['user_id'], 'manage_options') ) {
					$sf_umeta['pup_sf_id'] = $sf_id = $_POST['pup_sf_id'];
					
					$sf_update_res = self::update_salesforce_fields( $sf_id, $sf_fields_arr );

                    // var_dump($sf_fields_arr);
                    // var_dump($sf_update_res);

					// die();
					
					if( $sf_update_res['status_code'] !== 204 ) {
						foreach($sf_update_res['sf_error'] as $sf_err_key => $sf_err_data )
							$error->add( $sf_err_data['errorCode'], __('<strong>SALESFORCE ERROR: </strong>', 'pu-portal') . $sf_err_data['message'] );
					} else {
						if( isset( $_POST['files'] ) && ! empty( $_POST['files'] ) && $_POST['files']['has_avatar'] == 1 ) {
							Pronto_Wp_Portal_Common::pup_upload_image( $_POST['user_id'], 'pup_avatar', 'pup_avatar' );
						}
						
						foreach( $sf_umeta as $key => $val ) {
							update_user_meta( $_POST['user_id'], $key, $val );
						}
					}
				} 
			} elseif( isset( $_POST['role'] ) && $_POST['role'] != 'administrator' ) {
				$sf_fields_arr['ASPU__WPUsername__c'] = $_POST['user_login'];
				$sf_fields_arr['ASPU__Action__c'] = 'include';
				
				$sf_fields = self::add_salesforce_fields( $sf_fields_arr );
			
				if( $sf_fields['status_code'] !== 201 ) {			
					foreach($sf_fields['sf_error'] as $sf_err_key => $sf_err_data )
						$error->add( $sf_err_data['errorCode'], __('<strong>SALESFORCE ERROR: </strong>', 'pu-portal') . $sf_err_data['message'] );
				} else {
					$_POST['add_usr_sf_data'] = $sf_umeta;
					$_POST['add_usr_sf_data']['pup_sf_id'] = $sf_fields['sf_response']['id'];
					$_POST['add_usr_sf_data']['first_name'] = $_POST['first_name'];
					$_POST['add_usr_sf_data']['last_name'] = $_POST['last_name'];
				} 
			}
		}
	}
	
	/**
     * Function to update plugin-created user metas.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
	 * @param 		int		$user_id		the user id in which the changes will be apllied (required)
     * @since		1.0.0
     *
     * @LastUpdated	August 30, 2017
     */
	public function save_created_pup_profile_fields( $user_id ) {
		
		if( isset( $_POST['add_usr_sf_data'] ) && ! empty( $_POST['add_usr_sf_data'] ) ) {
			foreach( $_POST['add_usr_sf_data'] as $key => $val ) {
				update_user_meta( $user_id, $key, $val );
			}
			unset( $_POST['add_usr_sf_data'] );
			
			if( isset( $_POST['files'] ) && ! empty( $_POST['files'] ) && $_POST['files']['has_avatar'] == 1  ) {
				Pronto_Wp_Portal_Common::pup_upload_image( $user_id, 'pup_avatar', 'pup_avatar' );
			}
		}
	}

    /**
     * Callback function to update `user_data_settings` record in $wpdb->option.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
     *                Juniel Fajardo <juniel@alphasys.com.au>\n
     *                Edil Vincent Liong <edil@alphasys.com.au>
     *
     * @since        1.0.0
     *
     * @LastUpdated    August 24, 2017
     */
	public function pup_save_email_settings() {
		
		if ( ! check_ajax_referer( 'pup-save-email-settings', 'nonce' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Invalid Nonce.', 'pu-portal'),
				'type'		=> 	'error'
			) );
			wp_die();
		}
		
		if ( ! current_user_can( 'manage_options' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('You are not allow to do this.', 'pu-portal'),
				'type'		=> 	'error'
			) );
			wp_die();
		}
		
		if ( ! isset( $_REQUEST['data'] ) || empty( $_REQUEST['data'] ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Settings not saved. No data received from the server.', 'pu-portal'),
				'type'		=> 	'error'
			) );
		}
		
		$allDataArray = array();
		
		
		foreach($_REQUEST['data'] as $key => $val) {
			
			if($key <= 5) {
				$allDataArray['new_user'][$val['name']] = $val['value'];

			}else if($key <= 12){

				$allDataArray['new_admin'][$val['name']] = $val['value'];
			}else if($key <= 18) {

				$allDataArray['retrieve'][$val['name']] = $val['value'];
			}
			
		}
	
		update_option('user_data_settings',$allDataArray);
		
		echo wp_send_json( array(
			'message'	=>	__('Settings are saved successfully.', 'pu-portal'),
			'type'	=> 	'success',
			'data'	=> 	$allDataArray,
		) );
	
	}
	
	/**
	 * Callback function to update `pup_profile_settings` record in $wpdb->option.
	 *
	 * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
	 * @since         1.0.0
     *
     * @LastUpdated    August 24, 2017
	 */
	public function pup_save_profile_settings(){
	
		if ( ! check_ajax_referer( 'pup-save-profile-settings', 'nonce' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('Invalid Nonce.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}
		
		if ( ! current_user_can( 'manage_options' ) ) {
			echo wp_send_json( array(
				'message'	=>	__('You are not allow to do this.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
			wp_die();
		}

		if(isset($_POST['data']) && ! empty( $_POST['data'] )) {
			
			$tmp_res = $result = array();
			
			/* echo json_encode($_POST['data'], JSON_PRETTY_PRINT); */
			
			foreach( $_POST['data'] as $key => $val ) {
				foreach( $val as $inner_key => $outer_data ) {
					$tmp_res[ $outer_data['name'] ] = $outer_data['value']; 
				}
				$result[ $key ] = $tmp_res;
				$tmp_res = array();
			}
			
			update_option('pup_profile_settings', $result);
			
			echo wp_send_json( array(
				'message'	=>	__('Settings is saved successfuly.', 'pu-portal'),
				'type'		=> 	'success-notice'
			) );
		} else {			
			echo wp_send_json( array(
				'message'	=>	__('Settings not saved. No data received from the server.', 'pu-portal'),
				'type'		=> 	'error-notice'
			) );
		}
		wp_die();
	}
	
	/**
     * Update a contact record in SalesForce Contacts.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
     * @param 		array		$id		the array containing the unique Contact id 	(required)
     * @param 		array		$data	the array containing the unique SalesForce Contact field key and the desired value 	(required)
     * @return		object		The SalesForce query result object. 	
     * @since		1.0.0
     *
     * @LastUpdated	August 24, 2017
     */
	public static function update_salesforce_fields( $id, $data ) {
		if( $data != null ) {
			$result = self::$oauth->api_request( 'data/v39.0/sobjects/Contact/' . $id, $data, 'update' );
		}
		return $result;
	} 
	
	/**
     * Inserts `pup_profile_settings` and `reg_settings` record in $wpdb->option, if it does not exist.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
     *
	 * @since		1.0.0
     *
     * @LastUpdated	August 29, 2017
     */
	public static function set_default_opt_val() {
		if( null === self::$oauth && class_exists( 'Pronto_Ultimate_SF_Oauth' ) ) {
			self::$oauth = new Pronto_Ultimate_SF_Oauth( plugin_basename( __FILE__ ) );

			self::get_salesforce_fields();
		}
		$reg_settings = get_option('reg_settings');
		if( empty( $reg_settings ) ){
			update_option('reg_settings', array(
				array(
					"field_label"	=> "Email",
					"label_position"=> "",
					"placeholder"	=> "",
					"default_value"	=> "",
					"css_classes"	=> "",
					"required_field"=> "1",
					"show_on_reg"	=> "1",
					"form_settings"	=> array(
						array( "name"	=> "field_label", "value"	=> "Email" ),
						array( "name"	=> "label_position", "value"=> "top" ),
						array( "name"	=> "placeholder","value"	=> "" ),
						array( "name"	=> "default_value","value"	=> "" ),
						array( "name"	=> "css_classes","value"	=> "" ),
						array( "name"	=> "layout_size", "value"	=> "pu-one" ),
						array( "name"	=> "required_field","value"	=> "1" ),
						array( "name"	=> "show_on_reg","value"	=> "1" )
					),
					"salesforce_field"=> array(
						"name"	=> "Email",
						"label"	=> "Email",
						"type"	=> "email"
					),
					"wp_user_meta"	=> "Email"
				),
				array(
					"field_label"	=> "WP Username",
					"label_position"=> "",
					"placeholder"	=> "",
					"default_value"	=> "",
					"css_classes"	=> "",
					"required_field"=> "1",
					"show_on_reg"	=> "1",
					"form_settings"	=> array(
						array( "name"	=> "field_label", "value"	=> "WP Username" ),
						array( "name"	=> "label_position", "value"=> "top" ),
						array( "name"	=> "placeholder", "value"	=> "" ),
						array( "name"	=> "default_value", "value"	=> "" ),
						array( "name"	=> "css_classes", "value"	=> "" ),
						array( "name"	=> "layout_size", "value"	=> "pu-one" ),
						array( "name"	=> "required_field", "value"=> "1" ),
						array( "name"	=> "show_on_reg", "value"	=> "1" )
					),
					"salesforce_field"=> array(
						"name"	=> "ASPU__WPUsername__c",
						"label"	=> "WP Username",
						"type"	=> "string"
					),
					"wp_user_meta"	=> "ASPU__WPUsername__c"
				),
                array(
                    "field_label"	=> "First Name",
                    "label_position"=> "",
                    "placeholder"	=> "",
                    "default_value"	=> "",
                    "css_classes"	=> "",
                    "required_field"=> "1",
                    "show_on_reg"	=> "1",
                    "form_settings"	=> array(
                        array( "name"	=> "field_label", "value"	=> "First Name" ),
                        array( "name"	=> "label_position", "value"=> "top" ),
                        array( "name"	=> "placeholder", "value"	=> "" ),
                        array( "name"	=> "default_value", "value"	=> "" ),
                        array( "name"	=> "css_classes", "value"	=> "" ),
                        array( "name"	=> "layout_size", "value"	=> "pu-one" ),
                        array( "name"	=> "required_field", "value"=> "1" ),
                        array( "name"	=> "show_on_reg", "value"	=> "1" )
                    ),
                    "salesforce_field"=> array(
                        "name"	=> "FirstName",
                        "label"	=> "First Name",
                        "type"	=> "string"
                    ),
                    "wp_user_meta"	=> "first_name"
                ),
                array(
                    "field_label"	=> "Last Name",
                    "label_position"=> "",
                    "placeholder"	=> "",
                    "default_value"	=> "",
                    "css_classes"	=> "",
                    "required_field"=> "1",
                    "show_on_reg"	=> "1",
                    "form_settings"	=> array(
                        array( "name"	=> "field_label", "value"	=> "Last Name" ),
                        array( "name"	=> "label_position", "value"=> "top" ),
                        array( "name"	=> "placeholder", "value"	=> "" ),
                        array( "name"	=> "default_value", "value"	=> "" ),
                        array( "name"	=> "css_classes", "value"	=> "" ),
                        array( "name"	=> "layout_size", "value"	=> "pu-one" ),
                        array( "name"	=> "required_field", "value"=> "1" ),
                        array( "name"	=> "show_on_reg", "value"	=> "1" )
                    ),
                    "salesforce_field"=> array(
                        "name"	=> "LastName",
                        "label"	=> "Last Name",
                        "type"	=> "string"
                    ),
                    "wp_user_meta"	=> "last_name"
                ),
                /*
                array(
                    "field_label"	=> "Contact ID",
                    "label_position"=> "",
                    "placeholder"	=> "",
                    "default_value"	=> "",
                    "css_classes"	=> "",
                    "required_field"=> "1",
                    "show_on_reg"	=> "1",
                    "form_settings"	=> array(
                        array( "name"	=> "field_label", "value"	=> "Contact ID" ),
                        array( "name"	=> "label_position", "value"=> "top" ),
                        array( "name"	=> "placeholder","value"	=> "" ),
                        array( "name"	=> "default_value","value"	=> "" ),
                        array( "name"	=> "css_classes","value"	=> "" ),
                        array( "name"	=> "layout_size", "value"	=> "pu-one" ),
                        array( "name"	=> "required_field","value"	=> "1" ),
                        array( "name"	=> "show_on_reg","value"	=> "0" )
                    ),
                    "salesforce_field"=> array(
                        "name"	=> "Id",
                        "label"	=> "Contact ID",
                        "type"	=> "string"
                    ),
                    "wp_user_meta"	=> "Id"
                )
                */
			));
		}
		$pup_profile_settings = get_option('pup_profile_settings');
		if( empty( $pup_profile_settings ) ){
			update_option('pup_profile_settings', array(
				array(
					"meta_key"		=> "Email",
					"Email_show"	=> "1",
					"Email_readonly"=> "0",
					"layout_size"	=> "pu-one",
					"class"			=> "",
					"id"			=> ""
				),
				array(
					"meta_key"				=> "ASPU__WPUsername__c",
					"ASPU__WPUsername__c_show"	=> "1",
					"ASPU__WPUsername__c_readonly"=> "1",
					"layout_size"			=> "pu-one",
					"class"					=> "",
					"id"					=> ""
				),
                array(
                    "meta_key"			=> "FirstName",
                    "FirstName_show"	=> "1",
                    "FirstName_readonly"=> "1",
                    "layout_size"		=> "pu-one",
                    "class"				=> "",
                    "id"				=> ""
                ),
                array(
                    "meta_key"			=> "LastName",
                    "LastName_show"	    => "1",
                    "LastName_readonly" => "1",
                    "layout_size"		=> "pu-one",
                    "class"				=> "",
                    "id"				=> ""
                ),
                /*
                array(
                    "meta_key"				=> "Id",
                    "Id_show"	            => "1",
                    "Id_readonly"           => "1",
                    "layout_size"			=> "pu-one",
                    "class"					=> "",
                    "id"					=> ""
                ),
                */
				array(
					"meta_key"				=> "Password",
					"Password_show"			=> "1",
					"Password_readonly"		=> "0",
					"layout_size"			=> "pu-one",
					"class"					=> "",
					"id"					=> ""
				),
				array(
					"meta_key"				=> "ConPassword",
					"ConPassword_show"		=> "1",
					"ConPassword_readonly"	=> "0",
					"layout_size"			=> "pu-one",
					"class"					=> "",
					"id"					=> ""
				)
			));
		}
		
		if( get_option('pup_duplicate_email') == "" ) {
			update_option('pup_duplicate_email', '0');
		}
	}
	
	/**
     * Override the wordpress default avatar (Gravatar) with the custom avatar uploaded.
     *
     * @author        John Rendhon Gerona <rendhon@alphasys.com.au> \n
	 *                Juniel Fajardo <juniel@alphasys.com.au>\n
	 *                Edil Vincent Liong <edil@alphasys.com.au>
	 * @since		1.0.0
     *
     * @LastUpdated	September 14, 2017
     */
	public static function pup_override_gravatar( $avatar, $id_or_email, $size, $default, $alt ) {
		$user = false;

		if ( is_numeric( $id_or_email ) ) {

			$id = (int) $id_or_email;
			$user = get_user_by( 'id' , $id );

		} elseif ( is_object( $id_or_email ) ) {

			if ( ! empty( $id_or_email->user_id ) ) {
				$id = (int) $id_or_email->user_id;
				$user = get_user_by( 'id' , $id );
			}

		} else {
			$user = get_user_by( 'email', $id_or_email );	
		}

		if ( $user && is_object( $user ) &&  ! empty( $user->data->ID ) && $avatar_tmp = get_user_meta( $user->data->ID, 'pup_avatar', 1 ) ) {
			$avatar = isset( $avatar_tmp['url'] ) ? $avatar_tmp['url'] : '';
			$avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
		}

		return $avatar;
	}
}