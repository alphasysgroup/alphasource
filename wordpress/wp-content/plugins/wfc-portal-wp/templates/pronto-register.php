<?php

/**
 * Provide a client area view for the profile page
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 */


/*
* Fetch color scheme form buttons , checkbox and radio
*/
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';    

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff';  

/*
* call Saiyan_Toolkit
*/
Saiyan_Toolkit::get_instance()->load_toolkits(array(
    'tinter', 'spinner'
));

/*
* Fetch template
*/
$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');
$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'true';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';  
 
$template_opt = $template->get_template_options( 'register' );
?>
<div class="wppu wppu-register wfc-portal-register-main-container container">

	<?php $template->the_template_message( 'register' ); ?>
	<?php $template->the_errors(); ?>
	
	<form method="post" class="pup-register-form" action="<?php echo wp_registration_url(); ?>" enctype="multipart/form-data">
		
		<div class="col avatar-fld-group">
			<div class="img-container">
				<img class="avatar-ctrl" alt="pup-user-avatar" src="<?php echo get_avatar_url(' '); ?>"/>
			</div>
			<div class="btn-container">
				<button type="button" class="btn-upload-avatar fa fa-image"><?php // _e( 'Upload', 'pu-portal'); ?></button>
			</div>
			<input type="file" accept="image/jpeg, image/png" name="avatar" class="img-upload-ctrl" />
			<p><i><?php echo __('Maximum image size: 1MB.', 'pu-portal'); ?></i></p>
		</div>
		
        <?php if( ! empty( $template_opt['reg_settings'] ) ) :
            foreach( $template_opt['reg_settings'] as $key => $val ) :
                if($val['show_on_reg'] == 1 ) : ?>
                    <div class="col <?php echo esc_attr( $val['css_classes'] ) . ' ' . $val['label_position']. ' ' . $val['layout_size']; ?>">
                        <div class="form-group">
                            <label class="wfc-portal-field-label" for="<?php echo esc_attr( $val['field_label'] ); ?>" ><?php echo ( $val['required_field'] == 1 ) ? esc_attr( $val['field_label'] ).'<span style="color:red;"> *</span>' : esc_attr( $val['field_label'] ); ?></label>

                            <input type="hidden"  name="data[<?php echo $val['wp_user_meta']; ?>][type]" value="<?php echo $val['salesforce_field']['type']; ?>" />
                            <input type="hidden" name="data[<?php echo $val['wp_user_meta']; ?>][sf_key]" value="<?php echo $val['salesforce_field']['name']; ?>" />

                            <?php
                            switch( $val['salesforce_field']['type']) {
	                            case 'picklist' :
	                            case 'reference' :
		                            $type = ( $val['salesforce_field']['type'] == 'picklist' ) ? 'picklistValues' : 'referenceTo';

		                            if ( ! empty( $val['salesforce_field'][ $type ] ) ) {
			                            $options = ( ! empty( trim( $val['placeholder'] ) ) ) ? '<option disabled selected>' . esc_attr( $val['placeholder'] ) . '</option>' : '';

			                            foreach ( $val['salesforce_field'][ $type ] as $sf_opt ) {
				                            if ( $type == 'picklistValues' ) {
					                            $selected = ( $val['default_value'] == $sf_opt['value'] )
						                            ? 'selected="selected"'
						                            : ( ( isset( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) && $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] == $sf_opt['value'] )
							                            ? 'selected="selected"'
							                            : '' );
					                            $options  .= '<option ' . $selected . ' value="' . $sf_opt['value'] . '">' . $sf_opt['label'] . '</option>';
				                            } else {
					                            $selected = ( $val['default_value'] == $sf_opt )
						                            ? 'selected="selected"'
						                            : ( ( isset( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) && $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] == $sf_opt )
							                            ? 'selected="selected"'
							                            : '' );
					                            $options  .= '<option ' . $selected . ' value="' . $sf_opt . '">' . $sf_opt . '</option>';
				                            }
			                            }
		                            }
		                            ?>

                                    <select
                                        class="form-data"
                                        name="data[<?php echo $val['wp_user_meta']; ?>][value]"
			                            <?php echo ( $val['required_field'] == 1 ) ? 'required' : ''; ?>
                                    >
			                            <?php echo $options; ?>
                                    </select>
		                            <?php
                                    break;

	                            case 'textarea' :
	                                ?>
                                    <textarea
                                        class="form-data"
                                        name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                        <?php echo ( $val['required_field'] == 1 ) ? 'required' : ''; ?>
                                    ><?php echo ( isset( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) ) ? esc_attr( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) : esc_attr( $val['default_value'] ); ?></textarea>
		                            <?php
                                    break;

	                            case 'boolean' :
	                                ?>
                                    <label for="<?php echo $key; ?>">
                                        <input
                                            class="pup-bool pup-hid pup-not form-data"
                                            type="hidden"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                            value='0'
                                        />
                                        <input
                                            class="pup-bool pup-cb pup-not form-data"
                                            type="checkbox"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
				                            <?php echo ( isset( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) && $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] == 1 ) ? 'checked' : ''; ?>
                                            value='1'
                                        />
                                    </label>
		                            <?php
                                    break;

	                            default:
	                                ?>
                                    <input
                                        type="<?php
                                        if ( $val['salesforce_field']['type'] == 'email' || $val['salesforce_field']['type'] == 'url' || $val['salesforce_field']['type'] == 'date' ) {
                                            echo $val['salesforce_field']['type'];
                                        } elseif ( $val['salesforce_field']['type'] == 'datetime' ) {
                                            echo 'datetime-local';
                                        } elseif ( $val['salesforce_field']['type'] == 'phone' ) {
                                            echo 'tel';
                                        } else {
                                            echo 'text';
                                        } ?>"
                                        name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                        class="form-data" <?php echo ( $val['required_field'] == 1 ) ? 'required' : ''; ?>
                                        value="<?php echo ( isset( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) ) ? esc_attr( $_SESSION['reg_form_data'][ $val['wp_user_meta'] ]['value'] ) : esc_attr( $val['default_value'] ); ?>"
                                        placeholder="<?php echo esc_attr( $val['placeholder'] ); ?>"
                                    />
		                            <?php break;
                            }
                            ?>
                        </div>
                    </div>
                <?php endif;
            endforeach;

            unset($_SESSION['reg_form_data']);
        endif;
        ?>
		<div class="wfc-portal-register-button-container">
			<input type="submit" name="submit_btn" class="submit_btn" value="<?php echo __('Register', 'pu-portal' ); ?>"/>
		</div>
	</form>
</div>
<style type="text/css">
    .wfc-portal-field-label,
    .wfc-portal-register-main-container label,
    .wfc-portal-register-main-container p{
        color: <?php echo $wfc_portalSubThemeFontColor;?>; 
    }

	.wfc-portal-register-main-container{
        color: <?php echo $wfc_portalSubThemeFontColor;?>;  
        background-color: <?php echo $wfc_portalSubThemeColor;?>;   
	}

    .wfc-portal-register-main-container button:not(.fa-image),
    .wfc-portal-register-main-container input[type="submit"] {
        background-color: <?php echo $wfc_portalButtonColor;?>;
        color: <?php echo $wfc_portalButtonFontColor;?>;
        text-shadow: none;
        transition: 200ms ease-out;
    }
</style>
<script>
(function ($) {
    $(document).ready(function(){
		$('.btn-upload-avatar').click(function() {
			$('.img-upload-ctrl').click();
		});
		
		$('.img-upload-ctrl').change(function(){

			if (this.files && this.files[0]) {
				var reader = new FileReader();
				var avatarCtrl = $('.avatar-ctrl');

                avatarCtrl.css('opacity', '0.25');
				
				reader.onload = function (e) {
				    setTimeout(function() {
                        avatarCtrl
                            .css('opacity', '1.0')
                            .attr('src', e.target.result);
                    }, 100);
				};
				
				reader.readAsDataURL(this.files[0]);
			}
		});
	});
})( jQuery );
</script>
<?php if ($wfc_portalLoadingEnable === 'true') : ?>
<script>
    jQuery(document).ready(function($) {
        SaiyanToolkit.spinner.show(null, '<?php echo $wfc_portalLoadingSpinner; ?>');
        $('body .saiyan-spinner').css('pointer-events', 'none');
        $('body .saiyan-spinner img').css('opacity', '0.0');
        $('.pup-register-form').submit(function() {
            $(window).bind('beforeunload', function() {
                $('body .saiyan-spinner img').animate({
                    'opacity': '1.0'
                }, 300, 'linear', function() {
                    $('body .saiyan-spinner').css('pointer-events', 'all');
                });
                SaiyanToolkit.tinter.show('<?php echo $wfc_portalLoadingTinterColor; ?>');
            });
        });
    });
</script>
<?php endif; ?>