<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 */

$template_opt = $template->get_template_options( 'profile' ); 
$avatar_url = get_user_meta( $template_opt['curr_uid'], 'pup_avatar', 1 );


/*
* Fetch color scheme form buttons , checkbox and radio
*/
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';    

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff';   

/*
* call Saiyan_Toolkit
*/
Saiyan_Toolkit::get_instance()->load_toolkits(array(
    'tinter', 'spinner'
));

/*
* Fetch template
*/
$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');
$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'true';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';

?>
<div class="wppu wppu-profile wfc-portal-profile-main-container">

    <?php $template->the_template_message(); ?>
    <?php $template->the_errors(); ?>

    <div class="col avatar-fld-group">
        <div class="img-container">
            <img class="avatar-ctrl" alt="pup-user-avatar" src="<?php echo ( ! empty( $avatar_url ) ) ? $avatar_url : get_avatar_url( $template_opt['curr_uid'] ); ?>"/>
        </div>
    </div>

    <?php
    if( ! empty( $template_opt['reg_settings'] ) && ! empty( $template_opt['pup_profile_settings'] ) ) :
        foreach( $template_opt['pup_profile_settings'] as $pps_key => $pps_value ) :
            foreach( $template_opt['reg_settings'] as $key => $val) :
                if( ( $val['wp_user_meta'] == $pps_value['meta_key'] ) && ( $pps_value[ $pps_value['meta_key'] . '_show'] == '1' ) ) :	?>

                    <div class="col list-group">
                        <div class="lg-left" >
                            <label class="wfc-portal-field-label" for="<?php echo esc_attr( $val['field_label'] ); ?>" ><?php echo esc_attr( $val['field_label'] ); ?>:</label>
                        </div>
                        <div class="lg-right">
                            <?php
                            switch( $val['salesforce_field']['type']) {
	                            case 'picklist' :
	                            case 'reference' :
		                            $type = ( $val['salesforce_field']['type'] == 'picklist' ) ? 'picklistValues' : 'referenceTo';

		                            foreach ( $val['salesforce_field'][ $type ] as $opt_key => $opt ) {
			                            if ( $type == 'picklistValues' ) {
				                            if ( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) == $opt['value'] ) {
					                            echo $opt['label'];
				                            } else if ( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) == $opt ) {
					                            echo $opt;
				                            }
			                            }
		                            }
		                            break;

	                            case 'textarea' :
	                            case 'boolean' :
		                            echo esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) );
		                            break;

	                            default:
		                            if ( $val['salesforce_field']['name'] == 'Email' ) {
			                            echo esc_attr( get_userdata( $template_opt['curr_uid'] )->user_email );
		                            } elseif ( $val['salesforce_field']['name'] == 'ASPU__WPUsername__c' ) {
			                            echo esc_attr( get_userdata( $template_opt['curr_uid'] )->user_login );
		                            } elseif ( $val['salesforce_field']['type'] == 'date' ) {
			                            echo date( get_option( 'date_format' ), strtotime( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) ) );
		                            } elseif ( $val['salesforce_field']['type'] == 'datetime' ) {
			                            echo date( get_option( 'date_format' ) . " " . get_option( 'time_format' ), strtotime( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) ) );
		                            } elseif ( $val['salesforce_field']['type'] == 'multipicklist' ) {
			                            $multi_list_val = get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 );

			                            if ( ! is_array( $multi_list_val ) ) {
				                            $multi_list_val = explode( ';', $multi_list_val );
			                            }

			                            if ( ! empty( $multi_list_val ) ) {
				                            foreach ( $multi_list_val as $MLV_value ) {
					                            echo esc_attr( $MLV_value ) . '<br />';
				                            }
			                            }

		                            } else {
			                            echo esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) );
		                            }
		                            break;
                            }
                            ?>
                        </div>
		            </div>
                <?php endif;
			endforeach;
		endforeach; 
	endif; ?>
    <div class="link-edit-profile wfc-portal-ediptprof-cont" style="padding:0px 15px;"><a href="<?php echo home_url( 'profile-edit' ); ?>"><?php echo __('Edit Profile', 'pu-portal' ); ?></a></div>
</div>
<style type="text/css">
	.wfc-portal-field-label,
	.wfc-portal-profile-main-container label,
	.wfc-portal-profile-main-container p{
        color: <?php echo $wfc_portalSubThemeFontColor;?>; 
    }

	.wfc-portal-profile-main-container{
		background-color: <?php echo $wfc_portalSubThemeColor;?>;
		color: <?php echo $wfc_portalSubThemeFontColor;?>;  
		text-shadow: none;
	    transition: 200ms ease-out;	
	}
	
	.wfc-portal-ediptprof-cont a{
		color: <?php echo $wfc_portalButtonColor;?>;
	}

	.wfc-portal-ediptprof-cont a:hover{
		color: <?php echo $wfc_portalButtonColor;?>;
		text-shadow: 4px 4px 5px rgba(150, 150, 150, 0.5);
	}	
</style>
<?php if ($wfc_portalLoadingEnable === 'true') : ?>
<script>
    jQuery(document).ready(function($) {
        SaiyanToolkit.spinner.show(null, '<?php echo $wfc_portalLoadingSpinner; ?>');
        $('body .saiyan-spinner').css('pointer-events', 'none');
        $('body .saiyan-spinner img').css('opacity', '0.0');
        $(document).on('click', '.wfc-portal-ediptprof-cont a', function() {
            $(window).bind('beforeunload', function() {
                $('body .saiyan-spinner img').animate({
                    'opacity': '1.0'
                }, 300, 'linear', function() {
                    $('body .saiyan-spinner').css('pointer-events', 'all');
                });
                SaiyanToolkit.tinter.show('<?php echo $wfc_portalLoadingTinterColor; ?>');
            });
        });
    });
</script>
<?php endif; ?>
