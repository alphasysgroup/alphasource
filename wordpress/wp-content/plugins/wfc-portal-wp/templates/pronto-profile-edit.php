<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 */


/*
* Fetch color scheme form buttons , checkbox and radio
*/
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';    

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff'; 

/*
* call Saiyan_Toolkit
*/
Saiyan_Toolkit::get_instance()->load_toolkits(array(
    'tinter', 'spinner'
));

/*
* Fetch template
*/
$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');
$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'true';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';  


$template_opt = $template->get_template_options( 'profile' );
$avatar_url = get_user_meta( $template_opt['curr_uid'], 'pup_avatar', 1 ); ?>
<div class="wppu wppu-profile-edit wfc-portal-profile-edit-main-container">

	<?php $template->the_template_message(); ?>
	<?php $template->the_errors(); ?>
	
	<form method="post" class="pu-profile-form" action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" enctype="multipart/form-data">
	
		<?php wp_nonce_field( 'pup-profile', 'PROFILE_NONCE' ); ?>
		<input type="hidden" name="action" value="update_profile" />
		<input type="hidden" name="data[pup_sf_id]" value="<?php echo get_user_meta( $template_opt['curr_uid'], 'pup_sf_id', 1 ) ?>" />
		
		<div class="col avatar-fld-group">
			<div class="img-container">
				<img class="avatar-ctrl" alt="pup-user-avatar" src="<?php echo ( ! empty( $avatar_url ) ) ? $avatar_url : get_avatar_url( $template_opt['curr_uid'] ); ?>"/>
			</div>
			<div class="btn-container">
				<button type="button" class="btn-upload-avatar fa fa-image"><?php // _e( 'Upload', 'pu-portal'); ?></button>
			</div>
			<input type="file" accept="image/jpeg, image/png" name="avatar" class="img-upload-ctrl" />
			<p><i><?php echo __('Maximum image size: 1MB.', 'pu-portal' ); ?></i></p>
		</div>
        <?php if ( ! empty( $template_opt['reg_settings'] ) && ! empty( $template_opt['pup_profile_settings'] ) ) :
            foreach( $template_opt['pup_profile_settings'] as $pps_key => $pps_value ) :
                if ( ( $pps_value['meta_key'] == 'Password' ) && ( $pps_value[ $pps_value['meta_key'] . '_show'] == '1' ) ) : ?>
                    <div id="<?php echo esc_attr( $pps_value['id'] );?>" class="col <?php echo esc_attr( $pps_value['class'] ) . " " . esc_attr( $pps_value['layout_size'] ); ?>">
                        <div class="form-group">
                            <label class="wfc-portal-field-label" for="Password" >Password</label>
                            <input
                                class="form-data"
                                type="password"
                                name="data[Password]"
                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly'] == '1' ) ? 'readonly' : ''; ?>
                            />
                        </div>
                    </div>
                <?php elseif ( ( $pps_value['meta_key'] == 'ConPassword' ) && ( $pps_value[ $pps_value['meta_key'] . '_show'] == '1' ) ) : ?>
                    <div id="<?php echo esc_attr( $pps_value['id'] );?>" class="col <?php echo esc_attr( $pps_value['class'] ) . " " . esc_attr( $pps_value['layout_size'] ); ?>">
                        <div class="form-group">
                            <label class="wfc-portal-field-label" for="ConPassword" >Confirm Password</label>
                            <input
                                class="form-data"
                                type="password"
                                name="data[ConPassword]"
                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly'] == '1' ) ? 'readonly' : ''; ?>
                            />
                        </div>
                    </div>
                <?php endif;
		        foreach( $template_opt['reg_settings'] as $key => $val) :
			        if( ( $val['wp_user_meta'] == $pps_value['meta_key'] ) && ( $pps_value[ $pps_value['meta_key'] . '_show'] == '1' ) ) :	?>
                        <div id="<?php echo esc_attr( $pps_value['id'] );?>" class="col <?php echo esc_attr( $pps_value['class'] ) . " " . esc_attr( $pps_value['layout_size'] ); ?>">
                            <div class="form-group">
                                <label class="wfc-portal-field-label" for="<?php echo esc_attr( $val['field_label'] ); ?>" ><?php echo ( $val['required_field'] == '1' ) ? esc_attr( $val['field_label'] ).'<span style="color:red;"> *</span>' : esc_attr( $val['field_label'] ); ?></label>

                                <input type="hidden"  name="data[<?php echo $val['wp_user_meta']; ?>][type]" value="<?php echo $val['salesforce_field']['type']; ?>" />
                                <input type="hidden" name="data[<?php echo $val['wp_user_meta']; ?>][sf_key]" value="<?php echo $val['salesforce_field']['name']; ?>" />

                                <?php
                                switch( $val['salesforce_field']['type']) {
	                                case 'multipicklist' :
		                                $type = 'picklistValues';

		                                if ( ! empty( $val['salesforce_field'][ $type ] ) ) {
			                                $options = '';

			                                foreach ( $val['salesforce_field'][ $type ] as $opt_key => $opt ) {
				                                if ( $type == 'picklistValues' ) {
					                                $selectArr = get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 );

					                                if ( ! is_array( $selectArr ) ) {
						                                $selectArr = explode( ';', $selectArr );
					                                }

					                                $selected = ( in_array( $opt['value'], $selectArr ) )
						                                ? 'selected="selected"'
						                                : '';
					                                $options  .= '<option ' . $selected . ' value="' . $opt['value'] . '">' . $opt['label'] . '</option>';
				                                }
			                                }
		                                }
		                                ?>
                                        <input
                                            type="hidden"
                                            class="form-data"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                            value=""
                                        />
                                        <select
                                            class="form-data"
                                            multiple
                                            size="5"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value][]"
                                        >
			                                <?php echo $options; ?>
                                        </select>
		                                <?php
                                        break;

	                                case 'picklist' :
	                                case 'reference' :
		                                $default = '';
		                                $type = ( $val['salesforce_field']['type'] == 'picklist' ) ? 'picklistValues' : 'referenceTo';

		                                if ( ! empty( $val['salesforce_field'][ $type ] ) ) {

			                                $options = ( ! empty( trim( $val['placeholder'] ) ) ) ? '<option disabled selected>' . esc_attr( $val['placeholder'] ) . '</option>' : '';

			                                foreach ( $val['salesforce_field'][ $type ] as $opt_key => $opt ) {
				                                if ( $type == 'picklistValues' ) {
					                                $selected = ( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) == $opt['value'] ) ? 'selected="selected"' : '';
					                                $options  .= '<option ' . $selected . ' value="' . $opt['value'] . '">' . $opt['label'] . '</option>';
				                                } else {
					                                $selected = ( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) == $opt ) ? 'selected="selected"' : '';
					                                $options  .= '<option ' . $selected . ' value="' . $opt . '">' . $opt . '</option>';
				                                }
			                                }
		                                }
		                                ?>
                                        <input
                                            type="hidden"
                                            class="form-data"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                            value="<?php echo ( empty( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) ) )
                                                ? esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) )
                                                : $default; ?>"
                                        />
                                        <select
                                            class="form-data"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                            <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly' ] == '1' )
                                                ? 'disabled'
                                                : ( ( $val['required_field'] == '1' )
                                                    ? 'required'
                                                    : '' ); ?>
                                        >
			                                <?php echo $options; ?>
                                        </select>
		                                <?php
                                        break;

	                                case 'textarea' :
	                                    ?>
                                        <textarea
                                            class="form-data"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                            <?php echo ( $val['required_field'] == '1' ) ? 'required' : ''; ?>
                                            <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly' ] == '1' ) ? 'readonly' : ''; ?>
                                        ><?php echo esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ); ?></textarea>
		                                <?php
                                        break;

	                                case 'boolean' :
		                                $bool_val = esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) );
		                                ?>
                                        <label class="wfc-portal-field-label" for="<?php echo $key; ?>">
                                            <input
                                                class="pup-bool pup-hid pup-not form-data"
                                                type="hidden"
                                                name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                                value='<?php echo ( $bool_val ) ? $bool_val : '0'; ?>'
                                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly' ] == 1 ) ? 'disabled' : ''; ?>
                                            />
                                            <input
                                                class="pup-bool pup-cb pup-not form-data"
                                                type="checkbox"
                                                name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                                value='1'
                                                <?php echo ( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) == '1' ) ? 'checked="checked"' : ''; ?>
                                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly' ] == 1 ) ? 'disabled' : ''; ?>
                                            />
                                        </label>
		                                <?php
                                        break;

	                                default:
	                                    ?>
                                        <input
                                            class="form-data"
                                            type="<?php
                                            if ( $val['salesforce_field']['type'] == 'email' || $val['salesforce_field']['type'] == 'url' || $val['salesforce_field']['type'] == 'date' ) {
                                                echo $val['salesforce_field']['type'];
                                            } elseif ( $val['salesforce_field']['type'] == 'datetime' ) {
                                                echo 'datetime-local';
                                            } elseif ( $val['salesforce_field']['type'] == 'phone' ) {
                                                echo 'tel';
                                            } else {
                                                echo 'text';
                                            } ?>"
                                            name="data[<?php echo $val['wp_user_meta']; ?>][value]"
                                            value="<?php
                                            if ( $val['salesforce_field']['name'] == 'Email' ) {
                                                echo esc_attr( get_userdata( $template_opt['curr_uid'] )->user_email );
                                            } elseif ( $val['salesforce_field']['name'] == 'ASPU__WPUsername__c' ) {
                                                echo esc_attr( get_userdata( $template_opt['curr_uid'] )->user_login );
                                            } elseif ( $val['salesforce_field']['type'] == 'date' ) {
                                                echo date( "Y-m-d", strtotime( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) ) );
                                            } elseif ( $val['salesforce_field']['type'] == 'datetime' ) {
                                                echo date( "Y-m-d\TH:i:s", strtotime( esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) ) ) );
                                            } else {
                                                echo esc_attr( get_user_meta( $template_opt['curr_uid'], $val['wp_user_meta'], 1 ) );
                                            }
                                            ?>"
                                            placeholder="<?php echo esc_attr( $val['placeholder'] ); ?>"
			                                <?php echo ( $pps_value[ $pps_value['meta_key'] . '_readonly' ] == '1' ) ? 'readonly' : ''; ?>
			                                <?php echo ( $val['required_field'] == '1' ) ? 'required' : ''; ?>
                                        />
		                                <?php break;
                                }
                                ?>
                            </div>
                        </div>
                    <?php
                    endif;
		        endforeach;
	        endforeach;
        endif; ?>
		<div class="wppu_profile_link" style="display: inline-block;width:100%;padding-left: 15px;padding-right: 15px;">
			<input type="submit" class="update_btn" value="<?php echo __('Update', 'pu-portal' ); ?>" />
		</div>
	</form>
</div>
<style type="text/css">
    .wfc-portal-field-label,
    .wfc-portal-profile-edit-main-container label,
    .wfc-portal-profile-edit-main-container p{
        color: <?php echo $wfc_portalSubThemeFontColor;?>; 
    }

    .wfc-portal-profile-edit-main-container{
        background-color: <?php echo $wfc_portalSubThemeColor;?>;  
    }

    .wfc-portal-profile-edit-main-container button:not(.fa-image),
    .wfc-portal-profile-edit-main-container input[type="submit"] {
        background-color: <?php echo $wfc_portalButtonColor;?> !important;
        color: <?php echo $wfc_portalButtonFontColor;?> !important;
        text-shadow: none;
        transition: 200ms ease-out;
    }
</style>
<script>
(function ($) {
    $(document).ready(function(){
		$('.btn-upload-avatar').click(function() {
			$('.img-upload-ctrl').click();
		});

		$('.img-upload-ctrl').change(function(){

			if (this.files && this.files[0]) {
				var reader = new FileReader();
				var avatarCtrl = $('.avatar-ctrl');

				avatarCtrl.css('opacity', '0.25');
				
				reader.onload = function (e) {
				    setTimeout(function() {
                        avatarCtrl
                            .css('opacity', '1.0')
                            .attr('src', e.target.result);
                    }, 100);
				};
				
				reader.readAsDataURL(this.files[0]);
			}
		});
	});
})( jQuery );
</script>
<?php if ($wfc_portalLoadingEnable === 'true') : ?>
<script>
    jQuery(document).ready(function($) {
        SaiyanToolkit.spinner.show(null, '<?php echo $wfc_portalLoadingSpinner; ?>');
        $('body .saiyan-spinner').css('pointer-events', 'none');
        $('body .saiyan-spinner img').css('opacity', '0.0');
        $('.pu-profile-form').submit(function() {
            $(window).bind('beforeunload', function() {
                $('body .saiyan-spinner img').animate({
                    'opacity': '1.0'
                }, 300, 'linear', function() {
                    $('body .saiyan-spinner').css('pointer-events', 'all');
                });
                SaiyanToolkit.tinter.show('<?php echo $wfc_portalLoadingTinterColor; ?>');
            });
        });
    });
</script>
<?php endif; ?>
