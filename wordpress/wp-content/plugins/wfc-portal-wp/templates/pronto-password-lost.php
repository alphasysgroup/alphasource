<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 */


/*
* Fetch color scheme form buttons , checkbox and radio
*/
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';    

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff'; 

/*
* call Saiyan_Toolkit
*/
Saiyan_Toolkit::get_instance()->load_toolkits(array(
    'tinter', 'spinner'
));

/*
* Fetch template
*/
$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');
$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'true';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';      
?>
<div class="wppu wppu-lostpassword wfc-portal-password-lost-main-container container">
	
	<?php $template->the_template_message( 'lostpassword' ); ?>
	<?php $template->the_errors(); ?>
	<?php $template_opt = $template->get_template_options( 'lostpassword' ); ?>
	
	<form name="lostpasswordform" id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
		<div class="wppu-user-login-wrap">
			<label class="wfc-portal-field-label" for="user_login"><?php echo $template_opt['user_login_label']; ?></label>
			<input type="text" name="user_login" id="user_login" class="input" required>
		</div>
		<?php do_action( 'lostpassword_form' ); ?>

		<div class="wppu-submit-wrap">
			<input type="submit" name="wp-submit" id="wp-submit" value="<?php echo __('Get New Password', 'pu-portal'); ?>" />
			<input type="hidden" name="redirect_to" value="<?php echo home_url() .'/login/?checkemail=confirm'; ?>" />
			<input type="hidden" name="action" value="lostpassword" />
		</div>
	</form>
	<ul class="wppu-action-links wfc-portal-login-regist-cont">
		<li><a href="<?php echo wp_login_url(); ?>" rel="nofollow"><?php echo __( 'Log In', 'pu-portal' ); ?></a></li>
		<?php if( '1' == get_option( 'users_can_register' ) ) : ?>
		    <li><a href="<?php echo wp_registration_url(); ?>" rel="nofollow"><?php echo __( 'Register', 'pu-portal' ); ?></a></li>
		<?php endif; ?>
	</ul>
</div>
<style type="text/css">
	.wfc-portal-field-label,
	.wfc-portal-password-lost-main-container label,
	.wfc-portal-password-lost-main-container p{
        color: <?php echo $wfc_portalSubThemeFontColor;?>; 
    }

	.wfc-portal-password-lost-main-container{
    	background-color: <?php echo $wfc_portalSubThemeColor;?>;
    	color: <?php echo $wfc_portalSubThemeFontColor;?>;  
		text-shadow: none;
	    transition: 200ms ease-out;		
	}

	.wfc-portal-password-lost-main-container button, 
	.wfc-portal-password-lost-main-container input[type="submit"] {
	    background-color: <?php echo $wfc_portalButtonColor;?>;
	    color: <?php echo $wfc_portalButtonFontColor;?>;
	    text-shadow: none;
	    transition: 200ms ease-out;		
	}

	.wfc-portal-login-regist-cont a{
		color: <?php echo $wfc_portalButtonColor;?>;
	}
	
	.wfc-portal-login-regist-cont a:hover{
		color: <?php echo $wfc_portalButtonColor;?>;
		text-shadow: 4px 4px 5px rgba(150, 150, 150, 0.5);
	}
</style>
<?php if ($wfc_portalLoadingEnable === 'true') : ?>
<script>
    jQuery(document).ready(function($) {
        SaiyanToolkit.spinner.show(null, '<?php echo $wfc_portalLoadingSpinner; ?>');
        $('body .saiyan-spinner').css('pointer-events', 'none');
        $('body .saiyan-spinner img').css('opacity', '0.0');
        $('#lostpasswordform').submit(function() {
            load();
        });
        $(document).on('click', '.wfc-portal-login-regist-cont a', function() {
            load();
        });
        function load() {
            $(window).bind('beforeunload', function() {
                $('body .saiyan-spinner img').animate({
                    'opacity': '1.0'
                }, 300, 'linear', function() {
                    $('body .saiyan-spinner').css('pointer-events', 'all');
                });
                SaiyanToolkit.tinter.show('<?php echo $wfc_portalLoadingTinterColor; ?>');
            });
        }
    });
</script>
<?php endif; ?>