<?php

/**
 * Provide a client area view for the profile page
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 */

$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
	$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#1497c1';
$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
	$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#ffffff';


$wfc_message_option = get_option('wfc_portal_reg_text_format');
$wfc_message_option = isset($wfc_message_option) ? $wfc_message_option : 'text';



$template_opt = $template->get_template_options( 'register' );
?>
<div class="wppu wppu-register wfc_portal_reg_not_allowed_notif_container container">
    <?php
    $default = '<b>ERROR:</b> Registration not Allowed.<br/> Please contact your Administrator.';

    if ($wfc_message_option == 'text'){
        $wfc_message = get_option('wfc_portal_reg_note_text');
        $wfc_message = htmlspecialchars($wfc_message);
        $wfc_message = str_replace("\n", '<br />', $wfc_message);
        $wfc_message = isset($wfc_message) ? $wfc_message :  $default;
        ?>
        <div class="wfc_portal_notallowed_notif wppu ">
            <p class="error"><?php echo $wfc_message ?>
                <br/>
                <a href="<?php echo esc_url( wp_login_url() ); ?>" rel="nofollow"><?php echo __('Login', 'pu-portal'); ?></a>
            </p>

        </div>
        <?php
    }elseif ($wfc_message_option == 'html'){
        $wfc_message = get_option('wfc_portal_reg_note');
        $wfc_message = html_entity_decode($wfc_message);
        $wfc_message = isset($wfc_message) ? $wfc_message :  $default;
        ?>
        <div class="wfc_notallowed_notif wppu error">
            <?php echo $wfc_message ?>
            <br/>
            <a href="<?php echo esc_url( wp_login_url() ); ?>" rel="nofollow"><?php echo __('Login', 'pu-portal'); ?></a>
        </div>
        <?php
    }
    ?>
</div>
<style>

    .wfc_portal_reg_not_allowed_notif_container{
        padding: 25px;
        margin: auto;
    }

</style>