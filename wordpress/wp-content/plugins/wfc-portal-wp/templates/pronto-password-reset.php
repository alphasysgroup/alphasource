<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 */


/*
* Fetch color scheme form buttons , checkbox and radio
*/
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';    

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff';   

/*
* call Saiyan_Toolkit
*/
Saiyan_Toolkit::get_instance()->load_toolkits(array(
    'tinter', 'spinner'
));

/*
* Fetch template
*/
$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');
$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'true';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';       
?>
<div class="wppu wppu-resetpass wfc-portal-password-reset-main-container container">
	<?php $template->the_template_message( 'resetpass' ); ?>
	<?php $template->the_errors(); ?>
	<form name="resetpassform" id="resetpassform" action="<?php echo site_url( 'wp-login.php?action=resetpass' ); ?>" method="post" autocomplete="off">
        <input type="hidden" id="user_login" name="rp_login" value="<?php echo esc_attr( $_GET['login'] ); ?>" autocomplete="off" />
        <input type="hidden" name="rp_key" value="<?php echo esc_attr( $_GET['key'] ); ?>" />

        <div>
            <label class="wfc-portal-field-label" for="pass1"><?php echo __( 'New password', 'pu-portal' ) ?></label>
            <input type="password" name="pass1" id="pass1" class="input" size="20" value="" autocomplete="off" />
        </div>
        <div>
            <label class="wfc-portal-field-label" for="pass2"><?php echo __( 'Repeat new password', 'pu-portal' ) ?></label>
            <input type="password" name="pass2" id="pass2" class="input" size="20" value="" autocomplete="off" />
        </div>
        <div class="wfc-portal-password-reset-description description indicator-hint"><?php echo wp_get_password_hint(); ?></div>
        <div class="wppu-submit-wrap wfc-portal-password-reset-submit-wrap">
            <input type="submit" name="submit" id="resetpass-button" value="<?php echo __( 'Reset Password', 'pu-portal' ); ?>" />
        </div>
    </form>
</div>
<style type="text/css">
    .wfc-portal-field-label,
    .wfc-portal-password-reset-main-container label,
    .wfc-portal-password-reset-main-container p{
        color: <?php echo $wfc_portalSubThemeFontColor;?>; 
    }

    .wfc-portal-password-reset-main-container{
        background-color: <?php echo $wfc_portalSubThemeColor;?>; 
        color: <?php echo $wfc_portalSubThemeFontColor;?>;    
    }
    
    .wfc-portal-password-reset-main-container button, 
    .wfc-portal-password-reset-main-container input[type="submit"] {
        background-color: <?php echo $wfc_portalButtonColor;?>;
        color: <?php echo $wfc_portalButtonFontColor;?>;
        text-shadow: none;
        transition: 200ms ease-out;     
    }
</style>
<?php if ($wfc_portalLoadingEnable === 'true') : ?>
<script>
    jQuery(document).ready(function($) {
        SaiyanToolkit.spinner.show(null, '<?php echo $wfc_portalLoadingSpinner; ?>');
        $('body .saiyan-spinner').css('pointer-events', 'none');
        $('body .saiyan-spinner img').css('opacity', '0.0');
        $('#resetpassform').submit(function() {
            $(window).bind('beforeunload', function() {
                $('body .saiyan-spinner img').animate({
                    'opacity': '1.0'
                }, 300, 'linear', function() {
                    $('body .saiyan-spinner').css('pointer-events', 'all');
                });
                SaiyanToolkit.tinter.show('<?php echo $wfc_portalLoadingTinterColor; ?>');
            });
        });
    });
</script>
<?php endif; ?>