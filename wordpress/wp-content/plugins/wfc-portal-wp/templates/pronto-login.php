<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 */


/*
* Fetch color scheme form buttons , checkbox and radio
*/
$wfc_portal_colorscheme = get_option('wfc_portal_colorscheme');
$wfc_portalThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-color'] : '#d9d9d9';

$wfc_portalSubThemeColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-color'] : '#ebebeb';

$wfc_portalThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-theme-font-color'] : '#999999';

$wfc_portalSubThemeFontColor = isset( $wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-sub-theme-font-color'] : '#999999';    

$wfc_portalButtonColor = isset( $wfc_portal_colorscheme['wfc_portal-button-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-color'] : '#1497c1';

$wfc_portalButtonFontColor = isset( $wfc_portal_colorscheme['wfc_portal-button-font-color'] ) ?
$wfc_portal_colorscheme['wfc_portal-button-font-color'] : '#ffffff';   

/*
* call Saiyan_Toolkit
*/
Saiyan_Toolkit::get_instance()->load_toolkits(array(
    'tinter', 'spinner'
));

/*
* Fetch template
*/
$wfc_portal_loadingoptions = get_option('wfc_portal_loadingoptions');
$wfc_portalLoadingEnable = isset( $wfc_portal_loadingoptions['wfc_portal-loading-enable'] ) ?
	$wfc_portal_loadingoptions['wfc_portal-loading-enable'] : 'true';

$wfc_portalLoadingSpinner = isset( $wfc_portal_loadingoptions['wfc_portal-loading-spinner'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-spinner'] : 'tail';

$wfc_portalLoadingTinterColor = isset( $wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] ) ?
$wfc_portal_loadingoptions['wfc_portal-loading-tinter-color'] : '#000000';     
?>
<div class="wppu wppu-login wfc-portal-login-main-container container">
	<?php $template->the_template_message( 'login' ); ?>
	<?php $template->the_errors(); ?>
	<?php $template_opt = $template->get_template_options( 'login' ); ?>
	<form class="wfc-portal-login-form" name="loginform" id="loginform" action="<?php echo wp_login_url(); ?>" method="post">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span class="wppu-user-login-wrap">
					<label class="wfc-portal-field-label" for="user_login"><?php echo $template_opt['user_login_label']; ?></label>
					<input type="text" name="log" id="user_login" class="input" value="">
				</span>			
			</div>
			<div class="col-12 col-sm-12 col-md-12 col-lg-12">
				<span class="wppu-user-pass-wrap">
					<label class="wfc-portal-field-label" for="user_pass"><?php echo __('Password', 'pu-portal'); ?></label>
					<input type="password" name="pwd" id="user_pass" class="input" value="" autocomplete="off">
				</span>
			</div>
		</div>	
		<?php do_action( 'login_form' ); ?>
		<div class="wppu-rememberme-submit-wrap wfc-portal-login-rem-btn-cont row">
			<div class="wppu-rememberme-wrap col-12 col-sm-12 col-md-7 col-lg-7">
				<label class="wfc-portal-rememberme-container"><?php echo __('Remember Me', 'pu-portal'); ?>
					<input name="rememberme" type="checkbox" id="rememberme" value="forever">
					<span class="wfc-portal-rememberme-checkmark"></span>
				</label>
			</div>
			<div class="wppu-submit-wrap col-12 col-sm-12 col-md-5 col-lg-5">
				<input type="submit" name="wp-submit" id="wp-submit" value="<?php echo __('Log In', 'pu-portal'); ?>">
				<input type="hidden" name="redirect_to" value="http://localhost/devlogin/wp-admin/">
				<input type="hidden" name="action" value="pronto-login">
			</div>
		</div>
	</form>
	<ul class="wppu-action-links wfc-portal-regist-lostpass-cont">
		<?php if( '1' == get_option( 'users_can_register' ) ) : ?>
		    <li><a href="<?php echo wp_registration_url(); ?>" rel="nofollow"><?php echo __('Register', 'pu-portal'); ?></a></li>
		<?php endif; ?>
		<li><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" rel="nofollow"><?php echo __('Lost Password', 'pu-portal'); ?></a></li>
	</ul>
</div>
<style type="text/css">
	.wfc-portal-field-label,
	.wfc-portal-login-main-container label,
	.wfc-portal-login-main-container p{
        color: <?php echo $wfc_portalSubThemeFontColor;?>; 
    }

	.wfc-portal-login-main-container{
		background-color: <?php echo $wfc_portalSubThemeColor;?>;
		color: <?php echo $wfc_portalSubThemeFontColor;?>;  
		text-shadow: none;
	    transition: 200ms ease-out;	
	}

	.wfc-portal-login-main-container button, 
	.wfc-portal-login-main-container input[type="submit"] {
	    background-color: <?php echo $wfc_portalButtonColor;?>;
	    color: <?php echo $wfc_portalButtonFontColor;?>;
	    text-shadow: none;
	    transition: 200ms ease-out;		
	}
	
	.wfc-portal-rememberme-container input:checked ~ .wfc-portal-rememberme-checkmark {
	    background-color: <?php echo $wfc_portalButtonColor;?>;
	    color: <?php echo $wfc_portalButtonFontColor;?>;
	    text-shadow: none;
	    transition: 200ms ease-out;
	}

	.wfc-portal-regist-lostpass-cont a{
		color: <?php echo $wfc_portalButtonColor;?>;
	}

	.wfc-portal-regist-lostpass-cont a:hover{
		color: <?php echo $wfc_portalButtonColor;?>;
		text-shadow: 4px 4px 5px rgba(150, 150, 150, 0.5);
	}
</style>
<?php if ($wfc_portalLoadingEnable === 'true') : ?>
<script>
    jQuery(document).ready(function($) {
        SaiyanToolkit.spinner.show(null, '<?php echo $wfc_portalLoadingSpinner; ?>');
        $('body .saiyan-spinner').css('pointer-events', 'none');
        $('body .saiyan-spinner img').css('opacity', '0.0');
        $(document).on('click', '#wp-submit, .wfc-portal-regist-lostpass-cont a', function() {
            $(window).bind('beforeunload', function() {
                $('body .saiyan-spinner img').animate({
                    'opacity': '1.0'
                }, 300, 'linear', function() {
                    $('body .saiyan-spinner').css('pointer-events', 'all');
                });
                SaiyanToolkit.tinter.show('<?php echo $wfc_portalLoadingTinterColor; ?>');
            });
        });
    });
</script>
<?php endif; ?>