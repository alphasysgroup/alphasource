<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 *
 * Plugin Name:       Webforce Connect - Portal
 *
 * Plugin URI:        https://alphasys.com.au/
 *
 * Description:       Webforce Connect - Portal handles WordPress users and links user-data to a Salesforce Contact. This Plugin also handles User Registration, User Login, Password Reset and Password Recovery Functionality.
 *
 * Version: 1.1.2.1
 *
 * Author:            AlphaSys Pty. Ltd.
 *
 * Author URI:        https://alphasys.com.au/
 *
 * License:           GPL-2.0+
 *
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * Text Domain:       pu-portal
 *
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


define('WFC_PORTAL_BASEDIR', plugin_dir_path(__FILE__));

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-pronto-wp-portal-activator.php
 */
function activate_pronto_wp_portal() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pronto-wp-portal-activator.php';
	Pronto_Wp_Portal_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-pronto-wp-portal-deactivator.php
 */
function deactivate_pronto_wp_portal() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-pronto-wp-portal-deactivator.php';
	Pronto_Wp_Portal_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_pronto_wp_portal' );
register_deactivation_hook( __FILE__, 'deactivate_pronto_wp_portal' );

/*
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-pronto-wp-portal.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pronto_wp_portal() {

	$plugin = new Pronto_Wp_Portal();
	$plugin->run();

}
run_pronto_wp_portal();