<?php

/**
 * Class Pronto_Wp_Portal_Activator
 *
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since             1.0.0
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Activator {

	/**
	 * Create default portal pages when the plugin is activated
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

		// Information needed for creating the portal pages
	    $pronto_pages = array(
	        'login' => array(
	            'title' => __( 'Sign In', 'pu-portal' ),
	            'content' => '[pronto-portal]'
	        ),
	        'register' => array(
	            'title' => __( 'Register', 'pu-portal' ),
	            'content' => '[pronto-portal action="register"]'
	        ),
	        'profile' => array(
	            'title' => __( 'Profile', 'pu-portal' ),
	            'content' => '[pronto-portal action="profile"]'
	        ),
	        'profile-edit' => array(
	            'title' => __( 'Edit Profile', 'pu-portal' ),
	            'content' => '[pronto-portal action="profile-edit"]'
	        ),
	        'password-lost' => array(
	            'title' => __( 'Forgot Your Password?', 'pu-portal' ),
	            'content' => '[pronto-portal action="lostpass"]'
	        ),
	        'password-reset' => array(
	            'title' => __( 'Pick a New Password', 'pu-portal' ),
	            'content' => '[pronto-portal action="resetpass"]'
	        ),
	    );

	    $pup_page_settings = get_option('pup_pages_settings', array(
	    	array('name' => 'login_id',    'value' => ''),
	    	array('name' => 'register_id', 'value' => ''),
	    	array('name' => 'forgot_id',   'value' => ''),
	    	array('name' => 'reset_id',    'value' => ''),
	    	array('name' => 'details_id',  'value' => ''),
	    	array('name' => 'edit_id',     'value' => ''),
	    ));
	 
	    foreach ( $pronto_pages as $slug => $page ) {
	        // Check that the page doesn't exist already
	        $query = new WP_Query( 'pagename=' . $slug );

	        if ( ! $query->have_posts() ) {
	            // Add the page using the data from the array above
	            $id = wp_insert_post(
	                array(
	                    'post_name'      => $slug,
	                    'post_title'     => $page['title'],
	                    'post_content'   => $page['content'],
	                    'post_status'    => 'publish',
	                    'post_type'      => 'page',
	                    'ping_status'    => 'closed',
	                    'comment_status' => 'closed',
	                )
	            );

	            if(!is_wp_error($id)){
	            	switch ( $slug ) {
				    	case 'login':
							$pup_page_settings[0]['value'] = $id;
							break;
						case 'register':
							$pup_page_settings[1]['value'] = $id;
							break;
						case 'password-lost':
							$pup_page_settings[2]['value'] = $id;
							break;
						case 'password-reset':
							$pup_page_settings[3]['value'] = $id;
							break;
						case 'profile':
							$pup_page_settings[4]['value'] = $id;
							break;
						case 'profile-edit':
							$pup_page_settings[5]['value'] = $id;
							break;
						default:
					}
	            }
	        }
	    }

	    update_option('pup_pages_settings', $pup_page_settings);
	}

}
