<?php

/**
 * Class Pronto_Wp_Portal_Deactivator
 *
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since             1.0.0
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		
	}

}
