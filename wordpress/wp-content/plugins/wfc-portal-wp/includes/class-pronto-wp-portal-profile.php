<?php

/**
 * Class Pronto_Wp_Portal_Profile
 *
 * Register all actions and filters related to profile
 *
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Profile extends Pronto_Wp_Portal_Common {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		
		parent::__construct();
	}
	
	/**
	 * Redirect to login if user is not yet logged in.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since 	1.0.0
	 *
	 * @LastUpdated August 30, 2017
	 */
	public function redirect_to_login() {
		$wp_err = new WP_Error();
		if( is_page('profile') || is_page('profile-edit') ) {
			if( get_current_user_id() == 0 ) {
				$slug = ( is_page('profile') ) ? 'profile' : 'profile-edit';
				
				$wp_err->add( 'login_required', __( '<strong>ERROR</strong>: You must login first.', 'pu-portal' ) );
				$_SESSION['pronto_portal_errors'] = $wp_err;
				
				wp_redirect( add_query_arg( 'redirect_to', home_url( $this->pup_page( $slug ) ), home_url( $this->pup_page('login')) ) );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			}
		}
	}
	
	/**
	 * Callback function to allow duplicate email on registration.
	 *
	 * @author    John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param $user_email
	 *
	 * @return object
	 * @since          1.0.0
	 *
	 * @LastUpdated    December 10, 2017
	 */
	function skip_duplicate_email( $user_email ){
		if( get_option( 'pup_duplicate_email' ) == "1" ) {
			define( 'WP_IMPORTING', 'SKIP_EMAIL_EXIST' );
		}
		return $user_email;
	}
	
	/**
	 * Callback function to update user.
	 *
	 * @author    John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since         1.0.0
	 *
	 * @LastUpdated    September 11, 2017
	 */
	public function update_user_profile() {

		$wp_err = new WP_Error();

		$meta_fields_arr = $sf_fields_arr = array();
		$redirect_url = home_url( $this->pup_page('profile-edit') );

		$current_user = wp_get_current_user();
		$cur_uid = $current_user->ID;
		$old_email = $current_user->user_email;
		$hasAvatar = false;

		if( ! isset( $_POST['PROFILE_NONCE'] )  || ! wp_verify_nonce( $_POST['PROFILE_NONCE'], 'pup-profile' ) ) {
			$wp_err->add( 'nonce_err', __( '<strong>ERROR</strong>: Invalid nonce.', 'pu-portal' ) );
			$_SESSION['pronto_portal_errors'] = $wp_err;

			wp_redirect( $redirect_url );

			if (defined('PHPUNIT_RUNNING')) return;
			else exit;
		} 

		if( empty( $cur_uid ) ) {
			$wp_err->add( 'login_required', __( '<strong>ERROR</strong>: You must login first.', 'pu-portal' ) );
			$redirect_url = add_query_arg( 'redirect_to', $redirect_url, home_url( $this->pup_page('login') ) );
			$_SESSION['pronto_portal_errors'] = $wp_err;

			wp_redirect( $redirect_url );

			if (defined('PHPUNIT_RUNNING')) return;
			else exit;
		}

		if( isset( $_FILES['avatar'] )
			&& ! empty( $_FILES['avatar']['name'] )
			&& $_FILES['avatar']['size'] != '0'
			&& $_FILES['avatar']['error'] != '4' ) {

			$allowedImageType = array("image/jpeg", "image/png");

			if( ! in_array( $_FILES['avatar']['type'], $allowedImageType ) ) {

				$wp_err->add( 'file_not_supported', __( '<strong>ERROR</strong>: File type not supported.', 'pu-portal' ) );
				$_SESSION['pronto_portal_errors'] = $wp_err;

				wp_redirect( $redirect_url );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			}

			if( $_FILES["avatar"]["size"] > 1000000 ) {
				$wp_err->add( 'file_size_exceeded', __( '<strong>ERROR</strong>: Image exceeds the maximum size of 1MB.', 'pu-portal' ) );
				$_SESSION['pronto_portal_errors'] = $wp_err;

				wp_redirect( $redirect_url );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			}

			$hasAvatar = 1;
		}

		if( ! isset( $_POST['data'] ) && empty($_POST['data']) ) {
			$wp_err->add( 'empty_post', __( '<strong>ERROR</strong>: No data received from the server.', 'pu-portal' ) );
			$_SESSION['pronto_portal_errors'] = $wp_err;

			wp_redirect( $redirect_url );

			if (defined('PHPUNIT_RUNNING')) return;
			else exit;
		}

		foreach( $_POST['data'] as $key => $val ) {
			if( $key == 'pup_sf_id' ) {
				$id = $val;
			} elseif( $key == 'Password' ) {
				$Password = $val;
			} elseif( $key == 'ConPassword' ) {
				$ConPassword = $val;
			} else {
				if( $key != 'ASPU__WPUsername__c' ) {
					$meta_fields_arr[ $key ] = $sf_fields_arr[ $val['sf_key'] ] = sanitize_text_field( $val['value'] );

					if( $val['type'] == 'date' || $val['type'] == 'datetime	' ) {
						$meta_fields_arr[ $key ] = date( "Y-m-d\TH:i:s", strtotime( $meta_fields_arr[ $key ] ) );
					} elseif( $val['type'] == 'multipicklist' ) {

						$multipicklistv = '';
						if( ! empty( $val['value'] ) ) {
							foreach( $val['value']as $multi_picklist_key => $multi_picklist_val){
								$multipicklistv = ( $multipicklistv == '' ) ? $multipicklistv . $multi_picklist_val : $multipicklistv . ';' . $multi_picklist_val;
							}
						}

						$meta_fields_arr[ $key ] = $val['value'];
						$sf_fields_arr[ $val['sf_key'] ] = $multipicklistv;
					}
				}
			}
		}

		$email = sanitize_email( $meta_fields_arr['Email'] );

		if( empty( $id ) ) {
			$wp_err->add( 'empty_sf_id', __( '<strong>ERROR</strong>: Cannot update. SalesForce record does not exist.' ) );
		}

		if( empty( $email ) ) {
			$wp_err->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your email address.' ) );
		} elseif( ! is_email( $email ) ) {
			$wp_err->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
		} elseif( $old_email != $email ) {
			$user_exist_obj = get_user_by( 'email', $email )->ID;

			if( get_option( 'pup_duplicate_email' ) != "1" && !empty( $user_exist_obj ) && $cur_uid != $user_exist_obj ) {

				$wp_err->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
			}
		}

		if ( ( ( ! empty( $Password ) && empty( $ConPassword ) )
		|| ( empty( $Password ) && ! empty( $ConPassword ) ) )
		|| ( $Password != $ConPassword  ) ) {
			$wp_err->add( 'email_exists', __( '<strong>ERROR</strong>: Password mismatch.' ) );
		}

		if( $wp_err->get_error_code() ) {
			$_SESSION['pronto_portal_errors'] = $wp_err;

			wp_redirect( $redirect_url );

			if (defined('PHPUNIT_RUNNING')) return;
			else exit;
		}

		if( sanitize_text_field( $meta_fields_arr['last_name'] ) == '' )
			$sf_fields_arr['LastName'] = $meta_fields_arr['last_name'] = 'Anonymous';
		if( sanitize_text_field( $meta_fields_arr['first_name'] ) == '' )
			$sf_fields_arr['FirstName'] = $meta_fields_arr['first_name'] = 'Contact';

		$sf_update_res = Pronto_Wp_Portal_Admin::update_salesforce_fields( $id, $sf_fields_arr );

		if( $sf_update_res['status_code'] === 204 ) {

			if( ! empty( $Password ) && ! empty( $ConPassword ) ){
				wp_set_password( $Password, $cur_uid );
				$redirect_url = home_url( $this->pup_page('login') );
			} else {
				$redirect_url = home_url( $this->pup_page('profile') );
			}

			foreach( $meta_fields_arr as $key => $val ) {
				if( $key != 'ASPU__WPUsername__c' && $key != 'Email' ) {
					update_user_meta( $cur_uid, $key, (( is_array( $val ) ) ? $val : sanitize_text_field( $val )) );
				} elseif( $key == 'Email' ) {
					if( $old_email != sanitize_text_field( $val ) ) {
						$err = wp_update_user( array(
							'ID' => $cur_uid,
							'user_email' => sanitize_text_field( $val ),
						) );
					}
				}

				if( $hasAvatar ) {
					Pronto_Wp_Portal_Common::pup_upload_image( $cur_uid, 'pup_avatar', 'avatar' );
				}
			}

			$redirect_url = add_query_arg( 'profile', 'updated', $redirect_url );
		} else {
			if (isset($sf_update_res['sf_error'])) {
				foreach($sf_update_res['sf_error'] as $sf_err_key => $sf_err_data )
					$wp_err->add( $sf_err_data['errorCode'], __('<strong>SALESFORCE ERROR: </strong>', 'pu-portal') . $sf_err_data['message'] );

				$_SESSION['pronto_portal_errors'] = $wp_err;
			}
		}

		wp_redirect( $redirect_url );

		if (defined('PHPUNIT_RUNNING')) return;
		else exit;
	}
}