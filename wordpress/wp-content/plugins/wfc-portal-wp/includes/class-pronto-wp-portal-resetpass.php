<?php

/**
 * Class Pronto_Wp_Portal_Resetpass
 *
 * Register all actions and filters related to login
 *
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Resetpass extends Pronto_Wp_Portal_Common {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	
		parent::__construct();
	}

	/**
	 * This function is responsible for Redirecting the user  
	 * to the portal lost password page if the reset password key 
	 * is missing, invalid or expired
	 *
	 * @see Pronto_Wp_Portal::define_resetpass_hooks()
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 28, 2017
	 */
	public function check_reset_password_params() {
	    if ( is_page( 'password-reset' ) ) {
	        $user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
	        if ( ! $user || is_wp_error( $user ) ) {
	            
				if ( $user && $user->get_error_code() === 'expired_key' ) {
					
	                wp_redirect( home_url( $this->pup_page('password-lost').'?error=expiredkey' ) );
	            } else {
					
	                wp_redirect( home_url( $this->pup_page('password-lost').'?error=invalidkey' ) );
	            }

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
	        }
	    }
	}
	
	/**
	 * This function is responsible for Redirecting the user  
	 * to the portal reset password page instead of wp-login.php?action=resetpass.
	 *
	 * @see Pronto_Wp_Portal::define_resetpass_hooks()
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 28, 2017
	 */
	public function redirect_to_portal_resetpass() {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[3]['value']) ? $pup_pages[3]['value'] : '';

	    if ( $_SERVER['REQUEST_METHOD'] == 'GET' && 
	    	!empty($page_value) && 
	    	$page_value != 'wordpress-reset-password-default-page') {	
 
	        $user = check_password_reset_key( $_REQUEST['key'], $_REQUEST['login'] );
	        if ( ! $user || is_wp_error( $user ) ) {
	            if ( $user && $user->get_error_code() === 'expired_key' ) {
	                wp_redirect( home_url( $this->pup_page('password-lost').'?error=expiredkey' ) );
	            } else {
	                wp_redirect( home_url( $this->pup_page('password-lost').'?error=invalidkey' ) );
	            }

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
	        }
	 
	        $redirect_url = home_url( $this->pup_page('password-reset') );
	        $redirect_url = add_query_arg( 'login', esc_attr( $_REQUEST['login'] ), $redirect_url );
	        $redirect_url = add_query_arg( 'key', esc_attr( $_REQUEST['key'] ), $redirect_url );
	 
	        wp_redirect( $redirect_url );

		    if (defined('PHPUNIT_RUNNING')) return;
		    else exit;
	    }
	}

	/**
	 * This function is responsible for updating user password  
	 *
	 * @see Pronto_Wp_Portal::define_resetpass_hooks()
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated  July 28, 2017
	 */
	public function process_reset_password() {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[3]['value']) ? $pup_pages[3]['value'] : '';

	    if ( 'POST' == $_SERVER['REQUEST_METHOD'] && 
	    	!empty($page_value) && 
	    	$page_value != 'wordpress-reset-password-default-page') {	

	        $rp_key = $_REQUEST['rp_key'];
	        $rp_login = $_REQUEST['rp_login'];
	 
	        $user = check_password_reset_key( $rp_key, $rp_login );
	 
	        if ( ! $user || is_wp_error( $user ) ) {
	            if ( $user && $user->get_error_code() === 'expired_key' ) 
	                wp_redirect( home_url( $this->pup_page('login').'?error=expiredkey' ) );
	            else 
	                wp_redirect( home_url( $this->pup_page('login').'?error=invalidkey' ) );

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
	        }

	        $redirect_url = home_url( $this->pup_page('password-reset') );
	 
	        if ( isset( $_POST['pass1'] ) ) {
	            if ( $_POST['pass1'] != $_POST['pass2'] ) {
	                /*Passwords don't match*/ 
	                $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
	                $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
	                $redirect_url = add_query_arg( 'error', 'password_reset_mismatch', $redirect_url );
	 
	                wp_redirect( $redirect_url );

		            if (defined('PHPUNIT_RUNNING')) return;
		            else exit;
	            }
	 
	            if ( empty( $_POST['pass1'] ) ) {
	                /*Password is empty*/
	                $redirect_url = add_query_arg( 'key', $rp_key, $redirect_url );
	                $redirect_url = add_query_arg( 'login', $rp_login, $redirect_url );
	                $redirect_url = add_query_arg( 'error', 'password_reset_empty', $redirect_url );
	 
	                wp_redirect( $redirect_url );

		            if (defined('PHPUNIT_RUNNING')) return;
		            else exit;
	            }
	
	            /*Parameter checks OK, reset password*/
	            reset_password( $user, $_POST['pass1'] );
	            wp_redirect( home_url( $this->pup_page('login').'?password=changed' ) );

	        } else {
	            _e("Invalid request.", "pu-portal");
	        }

		    if (defined('PHPUNIT_RUNNING')) return;
		    else exit;
	    }
	}

}