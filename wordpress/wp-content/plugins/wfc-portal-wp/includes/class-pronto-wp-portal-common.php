<?php

/**
 * Class Pronto_Wp_Portal_Common
 * Register all common function
 *
 * @since             1.0.0
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Common {

	/**
	 * The page slug of portal login.
	 *
	 * @since    1.0.0
	 * @access   public
	 * @var      string    $pup_pages    The page slug of portal login.
	 */
	public $pup_pages;

	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 */
	public function __construct( ) {

		$this->pup_pages = $this->get_portal_pages();
		
	}

	/**
	 *
	 * This function is responsible for retrieving portal pages from save setting.
	 *
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since    1.0.0
	 * @access   public
	 * @return   array Portal Pages
	 *
	 * @LastUpdated August 11, 2017
	 */
	public function get_portal_pages( ) {
	    
	    $pup_pages = get_option('pup_pages_settings');

	    return $pup_pages;
	}
	
	
	/**
	 * Responsible for displaying the required page based on the parameter value given.
	 *
	 * @param $page
	 *
	 * @return string
	 */
	public function pup_page( $page ) {
	    switch ( $page ) {
	    	case 'login':
				$page_id = $this->pup_pages[0]['value'];
				break;
			case 'register':
				$page_id = $this->pup_pages[1]['value'];
				break;
			case 'password-lost':
				$page_id = $this->pup_pages[2]['value'];
				break;
			case 'password-reset':
				$page_id = $this->pup_pages[3]['value'];
				break;
			case 'profile':
				$page_id = $this->pup_pages[4]['value'];
				break;
			case 'profile-edit':
				$page_id = $this->pup_pages[5]['value'];
				break;
			default:
				$page_id = $this->pup_pages[0]['value'];
		}

		return get_post_field( 'post_name', $page_id );

	}
	
	/**
	 * Returns error messages based on configured filters in the admin settings.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param       $user
	 * @param array $errors The WP_Error object (optional)
	 *
	 * @return    mixed        Returns 'WP_Error object' if filter found errors, 'FALSE' if filter passed.
	 * @since     1.0.0
	 *
	 * @LastUpdated: August 14,2017
	 */
	public static function pup_get_login_filter_errors( $user, $errors = array() ) {		
		
		global $wpdb;
		$id = $user->data->ID;		
		
		if ( ! user_can( $id, 'manage_options' ) ) {
			
			$i_res = $o_res = array();
			$i_counter = $o_counter = 0;
			$o_flag = 1;
			
			foreach( get_option( 'login_filter' ) as $o_key => $o_val ) {	
			
				$i_flag = 1;
				
				foreach( $o_val['fldgroup'] as $i_key => $i_val ) {

                    $data_type = Pronto_Wp_Portal_Common::get_data_type($i_val['data']['metafield']);

                    if ($data_type === 'boolean'){
                        if($i_val['data']['metavalue'] === 1 || $i_val['data']['metavalue'] === 'true'){
                            $i_val['data']['metavalue'] = 1;
                        }
                        if($i_val['data']['metavalue'] === 0 || $i_val['data']['metavalue'] === 'false'){
                            $i_val['data']['metavalue'] = 0;
                        }
                    }

					switch ( $i_val['data']['metakey'] ) {
						case '=':
						case '!=':
						case '<':
						case '<=':
						case '>':
						case '>=':
							$query = 'SELECT
								umeta_id
							FROM 
								' . $wpdb->prefix . 'usermeta
							WHERE
								user_id		= ' . $id . '
							AND meta_key	= "' . $i_val['data']['metafield'] . '"
							AND meta_value	' . $i_val['data']['metakey'] . ' "' . esc_attr( $i_val['data']['metavalue'] ) . '"';
						break;

						case 'IN':
						case 'NOT IN':
							
							$query = '';
							$user_meta_arr = get_user_meta($id, $i_val['data']['metafield'], true);
							$meta_arr = array_map( 'strval', explode(',', $i_val['data']['meta_tags'] ) ); /* convert meta_tags string into array */
							
							if( ! empty( $user_meta_arr ) ) {
								
								$user_meta =  implode( '","', $user_meta_arr); /* stringify array and insert string (", ") between items */
								
								foreach( $meta_arr as $opt_key => $opt_val ) {
									
									$meta_opt = 'SELECT
										umeta_id
									FROM 
										' . $wpdb->prefix . 'usermeta
									WHERE
										user_id    	= ' . $id . '
									AND meta_key    = "' . $i_val['data']['metafield'] . '"
									AND "' . $opt_val . '" ' . $i_val['data']['metakey'] . ' ("' . $user_meta . '") ';
									
									if( ! $wpdb->get_results( $meta_opt, ARRAY_A ) ) {                                
										$i_flag = 0;
									}
								} 
								
								if( $i_flag == 0 ) {
									$err[] = array(
										'code'         => $i_val['data']['metafield'],
										'message'    => esc_attr( $i_val['data']['errmsg'] )
									);
								}
							}
						break;

						case 'starts':
							$query = 'SELECT
								umeta_id
							FROM 
								' . $wpdb->prefix . 'usermeta
							WHERE
								user_id		= ' . $id . '
							AND meta_key	= "' . $i_val['data']['metafield'] . '"
							AND meta_value	like "' . esc_attr( $i_val['data']['metavalue'] ) . '%"';
							break;

						case 'ends':
							$query = 'SELECT
								umeta_id
							FROM 
								' . $wpdb->prefix . 'usermeta
							WHERE
								user_id		= ' . $id . '
							AND meta_key	= "' . $i_val['data']['metafield'] . '"
							AND meta_value	like "%' . esc_attr( $i_val['data']['metavalue'] ) . '"';
						break;

						case 'ends':
							$query = 'SELECT
								umeta_id
							FROM 
								' . $wpdb->prefix . 'usermeta
							WHERE
								user_id		= ' . $id . '
							AND meta_key	= "' . $i_val['data']['metafield'] . '"
							AND meta_value	like "%' . esc_attr( $i_val['data']['metavalue'] ) . '%"';
						break;
					}
					
					if( $i_val['op'] == 'or')
						$i_flag = 1;
					
					if( ! empty( $query ) && ! $wpdb->get_results( $query, ARRAY_A ) ) {
					
						$err[] = array(
							'code' 		=> $i_val['data']['metafield'],
							'message'	=> esc_attr( $i_val['data']['errmsg'] )
						);
						
						$i_flag = 0;
					}
					
					if( $i_val['op'] == 'or')
						$i_counter++;

					$i_res[ $i_counter ] = $i_flag;						
				}

				if( $o_val['op'] == 'or' )
					$o_flag = 1;
				
				$flag = ( in_array( 1, $i_res ) ) ? 1 : 0;
				
				if( $flag == 0 )
					$o_flag = 0;
				
				if( $o_val['op'] == 'or' )
					$o_counter++;
				
				$o_res[ $o_counter ] = $o_flag;
				
				$i_res = array();
			}
			
			if( ! in_array( 1, $o_res ) ) {	
			
				foreach( $err as $key => $val ) {
					
					if( ! is_wp_error( $errors ) && $key == 0 )
						$errors = new WP_Error( $val['code'], sprintf( __('<strong>ERROR: </strong>%s'), $val['message'] ));
					else
						$errors->add( $val['code'], sprintf( __('<strong>ERROR: </strong>%s', 'pu-portal'), $val['message'] ));	
				}
				
				$_SESSION['pronto_portal_errors'] = $errors;
				
				return $errors;
			}
		}
		return false;
	}

	/**
	 * Returns the data type of the meta_key
	 *
	 * @param $meta_key
	 *
	 * @return string Data type of the meta_key
	 *
	 * @author Von Sienard Vibar <von@alphasys.com.au>
	 * @since 1.0.0
	 */
	public static function get_data_type($meta_key) {
		$sf_fields = get_option('sf_fields');
		$reg_settings = get_option('reg_settings');

		foreach ($reg_settings as $setting) {
			$wp_user_meta = $setting['wp_user_meta'];

			if ($meta_key === $wp_user_meta) {
				$sf_field = $setting['salesforce_field']['name'];
				foreach ($sf_fields as $field) {
					if ($field['name'] === $sf_field) return $field['type'];
				}
			}
		}
	}
	
	/**
	 * Name: redirect_logged_in_user
	 * Description: redirect to a admin page if user is already logged in and is admin.
	 * Last Updated: 9/12/2017
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param        $curr_page
	 * @param string $non_admin_URL required        the page slug to redirect if user is not an admin..
	 *
	 * @since     1.0.0
	 */
	public static function redirect_logged_in_user( $curr_page, $non_admin_URL ) {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[0]['value']) ? $pup_pages[0]['value'] : '';
		if (empty($page_value) || $page_value == 'wordpress-login-default-page') {
			$non_admin_URL = '';
		}

		global $post;
		$indicator = false;
		$settings = get_option('pup_pages_settings');

		if( isset( $post->ID ) ){
			foreach( $settings as $key => $pages ) {
				if( $pages['value'] == $post->ID && !in_array( $pages['name'], array( 'details_id', 'edit_id' ) ) ) {
					$indicator = true;
					break;
				}
			}			
		}

		if( $indicator ) {
			if( is_page( $post->ID ) && is_user_logged_in() ) {
				$user = wp_get_current_user();
				if ( user_can( $user, 'manage_options' ) ) 
					wp_redirect( admin_url() );       
				else 
					wp_redirect( home_url( $non_admin_URL ) );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			}
		}
	}
	
	/**
	 * Name: pup_upload_image
	 * Description: Crop and upload image. Serialized image data array is stored in $user_meta_key.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param int    $user_id       required		the user id to save the user meta
	 * @param string $user_meta_key required		the meta key to store the image data.
	 * @param array  $img           required		the image data array.
	 *
	 * @since 	1.0.0
	 *
	 * @LastUpdated: September 12,2017
	 */
	public static function pup_upload_image( $user_id, $user_meta_key, $img ) {
		
		/*if ( ! function_exists( 'wp_handle_upload' ) ) {*/
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
		/*}*/
		
		$upload_overrides = array( 'test_form' => false );
		/*$movefile = wp_handle_upload( $img, $upload_overrides );*/
		$movefile = media_handle_upload( $img, 0, array(), $upload_overrides );

		if ( $movefile && ! is_wp_error( $movefile ) ) {
			/*update_user_meta( $user_id, $user_meta_key, $movefile );*/
			update_user_meta( $user_id, $user_meta_key, wp_get_attachment_thumb_url( $movefile ) );
		
			/*$image = wp_get_image_editor( $movefile['file'] );
			
			if ( ! is_wp_error( $image ) ) {
				$image->resize( 300, 300, true );
				$image->save( $movefile['file'] );
			}*/
		}
	}
}