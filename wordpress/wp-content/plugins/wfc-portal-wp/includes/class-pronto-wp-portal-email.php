<?php

/**
 * Register all common function
 *
 * @since             1.0.0
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Email {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}
	
	/**
	 * Overrides the "From Email Address" of the email.
	 *
	 * @param $email
	 *
	 * @return string
	 */
	public function override_mail_from_email($email) {
	
		if( isset( $_POST['override_mail_from_email'] ) && ! empty( $_POST['override_mail_from_email'] ) ) {
			$tmp = $_POST['override_mail_from_email'];
			unset( $_POST['override_mail_from_email'] );
			return sanitize_email( $tmp );
		}
		return $email;	
	}
	
	/**
	 * Overrides the "From Name" of the email.
	 *
	 * @param $name
	 *
	 * @return mixed
	 */
	public function override_mail_from_name($name) {
		
		if( isset( $_POST['override_mail_from_name'] ) && ! empty( $_POST['override_mail_from_name'] ) ) {
			$tmp = $_POST['override_mail_from_name'];
			unset( $_POST['override_mail_from_name'] );
			return $tmp;
		}
		return $name;	
	}
}