<?php

/**
 * Handles syncing based on mapping data.
 *
 * @LastUpdated 08/22/2017
 *
 *
 * @version 	1.0
 * @author 		Junjie Canonio <junjie@alphasys.com.au>
 */
class Pronto_Wp_Portal_Contact_ResetPass_Api extends Pronto_Wp_Ultimate_Api {
	/**
	 * Function callback for `prontosyncer` API route.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	WP_REST_Request 	$request		Standard API request format.
	 */
	public function api_callback( WP_REST_Request $request ) {
		$wfc_portal_request_resetpass = array(
			'status' => 'bad',
			'message' => 'error', 
		);

		$response_data = json_decode( $request->get_body() );
		if(!isset($response_data->sf_id) || !isset($response_data->user_name)) {
			$wfc_portal_request_resetpass = array(
				'status' => 'bad',
				'message' => 'Make sure to provide both Salesforce Id and Username.', 
			);
		}else {
			$wfc_portal_request_resetpass = WFC_Portal_UserHandler::wfc_portal_request_resetpass($response_data->sf_id,$response_data->user_name);
			if(!is_array($wfc_portal_request_resetpass) || empty($wfc_portal_request_resetpass)) {
				$wfc_portal_request_resetpass = array(
					'status' => 'bad',
					'message' => 'No reset email has been sent.', 
				);
			}	
		}
		echo json_encode( $wfc_portal_request_resetpass );
		die();

		// echo"<pre>";var_dump($users);die();
	}	
}

// Returns a new instance of this API.
return new Pronto_Wp_Portal_Contact_ResetPass_Api();	