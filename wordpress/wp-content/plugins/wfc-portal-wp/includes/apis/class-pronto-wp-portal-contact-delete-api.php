<?php

/**
 * Handles syncing based on mapping data.
 *
 * @LastUpdated 08/22/2017
 *
 * @version 	1.0
 * @author 		Carl Ortiz <carl@alphasys.com.au>
 */
class Pronto_Wp_Portal_Contact_Delete_Api extends Pronto_Wp_Ultimate_Api {

	/**
	 * Function callback for `prontosyncer` API route.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	WP_REST_Request 	$request		Standard API request format.
	 */
	public function api_callback( WP_REST_Request $request ) {
		$response_data = json_decode( $request->get_body() );

		if( !isset( $response_data->sf_id ) ) {
			echo json_encode( array( 'status' => 'failed', 'msg' => "SalesForce Contact ID is required." ) );
			die();
		}

		$sf_id = $response_data->sf_id;

		$users = get_users( array(
				'role__in'		=> 'subscriber',
				'meta_key' 		=> 'pup_sf_id',
				'meta_value'	=> $sf_id
			) );

		if( empty( $users ) ) {
			echo json_encode( array( 'status' => 'failed', 'msg' => "Could not find user with Contact ID $sf_id." ) );
			die();
		}

		require_once( ABSPATH.'wp-admin/includes/user.php' );
		
		foreach ( $users as $user ) {
			wp_delete_user( $user->ID );
		}

		echo json_encode( array( 'status' => 'success', 'msg' => "User with SalesForce Contact ID $sf_id is successfully deleted." ) );
		die();

		// echo"<pre>";var_dump($users);die();
	}
}

// Returns a new instance of this API.
return new Pronto_Wp_Portal_Contact_Delete_Api();