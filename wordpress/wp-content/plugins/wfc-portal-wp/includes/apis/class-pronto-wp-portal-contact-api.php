<?php

/**
 * Class Pronto_Wp_Portal_Contact_Api
 *
 * SF Contacts - WP Users syncer API.
 *
 *
 * @version 	1.0
 * @author 		Carl Ortiz <carl@alphasys.com.au>
 * @LastUpdated August 18, 2017
 */
class Pronto_Wp_Portal_Contact_Api extends Pronto_Wp_Ultimate_Api {

	/**
	 * Function callback for `portalcontact` API route.
	 *
	 * @since 	1.0
	 * @access 	public
	 * @param 	WP_REST_Request 	$request		Standard API request format.
	 */
	public function api_callback( WP_REST_Request $request ) {
		$request = json_decode( $request->get_body() );

		//update_option( 'portal_test_option' , $request);		
		if( isset( $request->data ) ) {
			$contacts = $request->data;
			
			if( !empty( $contacts ) ) {
				$updated_contacts = array();

				foreach ( $contacts as $contact ) {
					if ( !empty( $contact->Id ) ) {
						$status = 'error';

						if ( !empty( $contact->ASPU__WPUsername__c ) && !empty( $contact->Email ) ) {
							$email = $contact->Email;
							$username = $contact->ASPU__WPUsername__c;
							$user_id = false;

							if ( false != ( $user_id = $this->is_user_exists_with_sf_id( $contact->Id ) ) )
								$this->update_user_info( $user_id, $contact );
							else {
								/* 30-11-2017 Changes Start */
								
								/* $user_id = register_new_user( $username, $email ); */
								
								$sanitized_user_login = sanitize_user( $username );

							 	$errors = new WP_Error();
								
							    // Check the username
							    if ( $sanitized_user_login == '' ) {
							        $errors->add( 'empty_username', __( '<strong>ERROR</strong>: Please enter a username.' ) );
							    } elseif ( ! validate_username( $username ) ) {
							        $errors->add( 'invalid_username', __( '<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.' ) );
							        $sanitized_user_login = '';
							    } elseif ( username_exists( $sanitized_user_login ) ) {
							        $errors->add( 'username_exists', __( '<strong>ERROR</strong>: This username is already registered. Please choose another one.' ) );
							    } else {
							        /** This filter is documented in wp-includes/user.php */
							        $illegal_user_logins = array_map( 'strtolower', (array) apply_filters( 'illegal_user_logins', array() ) );
							        if ( in_array( strtolower( $sanitized_user_login ), $illegal_user_logins ) ) {
							            $errors->add( 'invalid_username', __( '<strong>ERROR</strong>: Sorry, that username is not allowed.' ) );
							        }
							    }
							 	
							    // Check the email address
							    if ( $email == '' ) {
							        $errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your email address.' ) );
							    } elseif ( ! is_email( $email ) ) {
							        $errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
							        $email = '';
							    } elseif ( email_exists( $email ) ) {
							        $errors->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
							    }

							    if ( $errors->get_error_code() ){
							    	$user_id = $errors;
							    }

								$user_pass = wp_generate_password( 12, false );
								$user_id = wp_create_user( $sanitized_user_login, $user_pass, $email );
								if ( ! $user_id || is_wp_error( $user_id ) ) {
									$errors->add( 'registerfail', sprintf( __( '<strong>ERROR</strong>: Couldn&#8217;t register you&hellip; please contact the <a href="mailto:%s">webmaster</a> !' ), get_option( 'admin_email' ) ) );
									$user_id = $errors;
								}
								
								if ( ! $errors->get_error_code() ){
									update_user_option( $user_id, 'default_password_nag', true, true ); //Set up the Password change nag.
							    }
								/* 30-11-2017 Changes End ***/
								
								$this->update_user_info( $user_id, $contact );
								
								/* 30-11-2017 Changes Start ***/
								if ( ! $errors->get_error_code() ){
									do_action( 'register_new_user', $user_id );
								}
								/* 30-11-2017 Changes End ***/
							}

							if ( false !== $user_id && !is_wp_error( $user_id ) )
								$status = 'success';

						}

						$updated_contacts[ $contact->Id ] = $status;

						if ( $status == 'error' && isset( $errors ) && is_a( $errors, 'WP_Error' ) ) {
							$err_code = $errors->get_error_code();

							$updated_contacts[ $contact->Id ] .= ' --- ' . json_encode( $errors->get_error_message( $err_code ) );
						}
						
					}
				}
				echo json_encode( $updated_contacts );
				die();
			}

			echo json_encode(array());
		}

		die();
	}

	/**
	 * Checks if the user exists based on SalesForce ID.
	 *
	 * @since 	1.0
	 * @access 	private
	 * @param 	string 		$sf_id		Salesforce contact ID.
	 * @return 	int/boolean
	 */
	private function is_user_exists_with_sf_id( $sf_id ) {
		$users = get_users( array(
				'role__in'		=> 'subscriber',
				'meta_key' 		=> 'pup_sf_id',
				'meta_value'	=> $sf_id
			) );

		if ( !empty( $users ) && isset( $users[ 0 ]->ID ) )
			return $users[ 0 ]->ID;

		return false;
	}

	/**
	 * Update user meta.
	 *
	 * @since 	1.0
	 * @access 	private
	 * @param 	int 		$user_id		WP user ID.
	 * @param 	object 		$contact		SalesForce contact.
	 */
	private function update_user_info( $user_id, $contact ) {
		$mapping_details = get_option( 'reg_settings' );

		if ( empty( $mapping_details ) && !is_array( $mapping_details ) ) {
			json_encode( array(
					'status'	=> 'error',
					'msg' 		=> __('No mapping data.', 'pu-portal')
				) );
			die();
		}

		foreach ( $mapping_details as $mapping_detail ) {
			if ( isset( $mapping_detail[ 'salesforce_field' ][ 'name' ] ) ) {
				$sf_field_name = $mapping_detail[ 'salesforce_field' ][ 'name' ];

				if ( isset( $contact->{ $sf_field_name } ) && isset( $mapping_detail[ 'wp_user_meta' ] ) ) {
					$sf_field_val = $contact->{ $sf_field_name };

					if ( "ASPU__WPUsername__c" == $sf_field_name ) {
						$userdata = get_userdata( $user_id );

						if ( isset( $userdata->user_login ) && $userdata->user_login != $sf_field_val && !username_exists( $sf_field_val ) ) {
							global $wpdb;

							$wpdb->update(
									$wpdb->users,
									array( 'user_login' => $sf_field_val ),
									array( 'ID' => $user_id )
								);
						}

						continue;
					} else if ( "Email" == $sf_field_name ) {
						$userdata = get_userdata( $user_id );

						if ( isset( $userdata->user_login ) && $userdata->user_login != $sf_field_val && is_email( $sf_field_val ) ) {
							wp_update_user( array(
									'ID' 			=> $user_id,
									'user_email'	=> $sf_field_val
								) );
						}

						continue;
					}
					if (gettype($sf_field_val) == 'boolean' && class_exists('Webforce_Connect_Connexions_Addon') == true) {
						if ($sf_field_val == true) {
							$updated = update_user_meta( $user_id, $mapping_detail[ 'wp_user_meta' ], 'true');
						}else{
							$updated = update_user_meta( $user_id, $mapping_detail[ 'wp_user_meta' ],  '');
						}
						
					} else if (strpos($sf_field_val, 'taxonomy_checkbox') !== false && class_exists('Webforce_Connect_Connexions_Addon') == true) {
                        $json = json_decode($sf_field_val, true);
                        if (is_array($json) && isset($json['taxonomy_checkbox'])) {
                            $taxonomy = array();
                            foreach ($json['taxonomy_checkbox'] as $key => $value) {
                                array_push($taxonomy, $key);
                            }

                            $updated = update_user_meta( $user_id, $mapping_detail[ 'wp_user_meta' ], $taxonomy );
                        }
                    } else{
						$updated = update_user_meta( $user_id, $mapping_detail[ 'wp_user_meta' ], $sf_field_val );
					}
					
				}
			}
		}

		update_user_meta( $user_id, 'first_name', $contact->FirstName );
		update_user_meta( $user_id, 'last_name', $contact->LastName );
		update_user_meta( $user_id, 'Email', $contact->Email );
		update_user_meta( $user_id, 'ASPU__WPUsername__c', $contact->ASPU__WPUsername__c );
		update_user_meta( $user_id, 'pup_sf_id', $contact->Id );
	}
}

// Returns a new instance of this API.
return new Pronto_Wp_Portal_Contact_Api();