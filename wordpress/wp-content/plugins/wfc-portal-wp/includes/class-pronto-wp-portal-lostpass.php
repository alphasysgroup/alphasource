<?php

/**
 * Class Pronto_Wp_Portal_Lostpass
 *
 * Register all actions and filters related to login
 *
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */


/* require plugin_dir_path(__FILE__) .'/notifications/retrieve-notification.php'; */

class Pronto_Wp_Portal_Lostpass extends Pronto_Wp_Portal_Common {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		parent::__construct();
	}

	/**
	 * A function callback to `login_form_lostpassword` action hook.
	 *
	 * This function is responsible for Redirecting the user  
	 * to the portal forgot password page instead of wp-login.php?action=lostpassword.
	 *
	 * @see Pronto_Wp_Portal::define_lostpass_hooks()
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 27, 2017
	 */
	public function redirect_to_portal_lostpass() {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[2]['value']) ? $pup_pages[2]['value'] : '';
	
	    if ( $_SERVER['REQUEST_METHOD'] == 'GET' && 
	    	!empty($page_value) && 
	    	$page_value != 'wordpress-lost-password-default-page') {	

	        if ( is_user_logged_in() ) {

	            $user = wp_get_current_user();

			    if ( user_can( $user, 'manage_options' ) ) 
			        wp_redirect( admin_url() );       
			    else
			        wp_redirect( home_url( $this->pup_page('profile') ) );

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
	        }
	 
	        wp_redirect( home_url( $this->pup_page('password-lost') ) );

		    if (defined('PHPUNIT_RUNNING')) return;
		    else exit;
	    }
	}
	
	/**
	 * Redirect to profile if user is already logged in.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since    1.0.0
	 * @LastUpdated September 12, 2017
	 */
	public function check_user_logged_in() {
		Pronto_Wp_Portal_Common::redirect_logged_in_user( 'password-lost' , $this->pup_page('profile') );
	}
	
	/**
	 * A function callback to `login_form_lostpassword` action hook.
	 *
	 *
	 * This function is responsible for processing and  
	 * sending email to reset the user password
	 * @see Pronto_Wp_Portal::define_lostpass_hooks()
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 27, 2017
	 */
	public function process_portal_lostpass() {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[2]['value']) ? $pup_pages[2]['value'] : '';
	
	    if ( $_SERVER['REQUEST_METHOD'] == 'POST' && 
	    	!empty($page_value) && 
	    	$page_value != 'wordpress-lost-password-default-page') {	
			
			$user_login = $_REQUEST['user_login'];

			$errors = new WP_Error();

			$pup_duplicate_email = get_option( 'pup_duplicate_email' );

			if( $pup_duplicate_email == '1' && ! validate_username( $user_login ) ) {
				$errors->add( 'invalid_user_login', __('<strong>ERROR: </strong>&nbsp;Invalid username.', 'pu-portal'));
				$_SESSION['pronto_portal_errors'] = $errors;
				wp_redirect( home_url( $this->pup_page('password-lost') ) );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			} elseif( $pup_duplicate_email != '1' && ! is_email( $user_login ) && ! validate_username( $user_login ) ) {
				$errors->add( 'invalid_user_login', __('<strong>ERROR: </strong>&nbsp;Invalid username or email.', 'pu-portal'));
				$_SESSION['pronto_portal_errors'] = $errors;
				wp_redirect( home_url( $this->pup_page('password-lost') ) );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			} else {

				$user_by_login = get_user_by( 'login', $user_login );
				
				if( $pup_duplicate_email == '1' ){
					$user = $user_by_login;
				} else {
					$user_by_email = get_user_by( 'email', $user_login );
					$user = ( $user_by_email ) ? $user_by_email : ( ( $user_by_login ) ? $user_by_login : '' );
				}

				if( ! $user ) {
					$errors->add( 'usernotfound', __('<strong>ERROR: </strong>&nbsp;User not found.', 'pu-portal') );
					$redirect_url = home_url( $this->pup_page('password-lost') );
				} else {
					
					do_action( 'lostpassword_post', $errors );
					
					if ( $errors->get_error_code() ) {
						$redirect_url = home_url( $this->pup_page('password-lost') );
					} else {
					
						//pup_lost_noti_message( $user );
						
						$redirect_url = home_url( $this->pup_page('login') );
						$redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
					}
				}
			}

			$_SESSION['pronto_portal_errors'] = $errors;
			wp_redirect( $redirect_url );

			if (defined('PHPUNIT_RUNNING')) return;
			else exit;
		}else{
			$errors = new WP_Error();
			$pup_duplicate_email = get_option( 'pup_duplicate_email' );
			$user_by_login = get_user_by( 'login', $user_login );
				
			if( $pup_duplicate_email == '1' ){
				$user = $user_by_login;
			} else {
				$user_by_email = get_user_by( 'email', $user_login );
				$user = ( $user_by_email ) ? $user_by_email : ( ( $user_by_login ) ? $user_by_login : '' );
			}

			if( ! $user ) {
				$errors->add( 'usernotfound', __('<strong>ERROR: </strong>&nbsp;User not found.', 'pu-portal') );
				$redirect_url = home_url(wp_lostpassword_url());
			} else {
				
				do_action( 'lostpassword_post', $errors );
				
				if ( $errors->get_error_code() ) {
					$redirect_url = home_url(wp_lostpassword_url());
				} else {
					
					$redirect_url = home_url(wp_login_url());
					$redirect_url = add_query_arg( 'checkemail', 'confirm', $redirect_url );
				}
			}
		}
	}

	/**
	 * A function callback to `lostpassword_post` action hook.
	 *
	 * This is used to perform additional validation/authentication 
	 * when a user request for a reset password
	 *
	 * @see Pronto_Wp_Portal::define_lostpass_hooks()
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 27, 2017
	 */
	public function additional_validation( $errors ) {
		if ( ! empty( $_POST['user_login'] ) ) {
			
			$user_login = trim( $_POST['user_login'] );
			
			$pup_duplicate_email = get_option( 'pup_duplicate_email' );

			if( $pup_duplicate_email == '1' ) {
				$user = get_user_by( 'login', $user_login );
			} else {
				$user_by_email = get_user_by( 'email', $user_login );
				$user_by_login = get_user_by( 'login', $user_login );

				$user = ( $user_by_email ) ? $user_by_email : ( ( $user_by_login ) ? $user_by_login : '' );
			}

			if( ! empty( $user ) && ! empty( get_option( 'login_filter' ) ) ) {
				self::pup_get_login_filter_errors( $user, $errors );
			}
		}
	}
}