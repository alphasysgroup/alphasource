<?php

/**
 * Class Pronto_Wp_Portal
 *
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since             1.0.0
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Pronto_Wp_Portal_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;
	
	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'pronto-wp-portal';
		$this->version = '1.0.0';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();
		$this->define_template_hooks();
		$this->define_login_hooks();
		$this->define_lostpass_hooks();
		$this->define_resetpass_hooks();
		$this->define_register_hooks();
		$this->define_profile_hooks();
		$this->define_email_hooks();
	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Pronto_Wp_Portal_Loader. Orchestrates the hooks of the plugin.
	 * - Pronto_Wp_Portal_i18n. Defines internationalization functionality.
	 * - Pronto_Wp_Portal_Admin. Defines all hooks for the admin area.
	 * - Pronto_Wp_Portal_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/*
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-loader.php';

		/*
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-i18n.php';

		/*
		 * The class responsible for defining common functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-common.php';

		/*
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-pronto-wp-portal-admin.php';

		/*
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-pronto-wp-portal-public.php';

		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * template.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-template.php';

		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * login page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-login.php';

		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * forgot password page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-lostpass.php';

		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * reset password page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-resetpass.php';

		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * registration page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-register.php';

		require_once plugin_dir_path( dirname( __FILE__ ) ) .'includes/notifications/register-notification.php';
		
		require_once plugin_dir_path( dirname( __FILE__ ) ) .'includes/notifications/retrieve-notification.php';
		
		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * profile and profile edit page.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-profile.php';

		/*
		 * The class responsible for defining all actions that occur in the pronto portal 
		 * email settings.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-email.php';

		/*
		 * The class containing the functions that handles the user 
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-pronto-wp-portal-user-handler.php';

		$this->loader = new Pronto_Wp_Portal_Loader();
	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Pronto_Wp_Portal_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Pronto_Wp_Portal_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Pronto_Wp_Portal_Admin( $this->get_plugin_name(), $this->get_version(), $this );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'admin_menu' );



        /*
		 * enqueue script admin
		 */
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'portal_enqueue_scripts' );

        /*
		 *  cherry handlers
		 */
        /*	$this->loader->add_action( 'after_setup_theme', $plugin_admin, 'pup_cherry_handlers' );*/

        /*
		 *  modified settings
		 */
        $this->loader->add_action( 'wp_ajax_save_sort', $plugin_admin, 'pup_save_registration_settings' );
        $this->loader->add_action( 'wp_ajax_nopriv_save_sort', $plugin_admin, 'pup_save_registration_settings' );

        $this->loader->add_action( 'wp_ajax_filter_login', $plugin_admin, 'pup_save_login_settings' );
        $this->loader->add_action( 'wp_ajax_nopriv_filter_login', $plugin_admin, 'pup_save_login_settings' );

        $this->loader->add_action( 'wp_ajax_register-settings-js', $plugin_admin, 'wfc_reg_settings_callback');
        $this->loader->add_action( 'wp_ajax_nopriv_register-settings-js', $plugin_admin, 'wfc_reg_settings_callback');

        $this->loader->add_action( 'wp_ajax_pages_settings', $plugin_admin, 'pup_save_pages_settings' );
		$this->loader->add_action( 'wp_ajax_nopriv_pages_settings', $plugin_admin, 'pup_save_pages_settings' );

		$this->loader->add_action( 'wp_ajax_get_tags', $plugin_admin, 'pup_login_suggest_user_meta' );
		$this->loader->add_action( 'wp_ajax_nopriv_get_tags', $plugin_admin, 'pup_login_suggest_user_meta' );

		$this->loader->add_action( 'wp_ajax_sync_list_data', $plugin_admin, 'sync_sf_fields_callback' );
		$this->loader->add_action( 'wp_ajax_nopriv_sync_list_data', $plugin_admin, 'sync_sf_fields_callback' );
		
		$this->loader->add_action( 'wp_ajax_save_email', $plugin_admin, 'pup_save_email_settings' );
		$this->loader->add_action( 'wp_ajax_nopriv_save_email', $plugin_admin, 'pup_save_email_settings' );
		
		$this->loader->add_action( 'wp_ajax_save_profile', $plugin_admin, 'pup_save_profile_settings' );
		$this->loader->add_action( 'wp_ajax_nopriv_save_profile', $plugin_admin, 'pup_save_profile_settings' );

		$this->loader->add_filter( 'pronto_wp_ultimate_api_dirs', $plugin_admin, 'set_api_directory' );
	
		/* Display Pronto Portal Fields in `View User` page. */
		$this->loader->add_action( 'show_user_profile', $plugin_admin, 'view_created_pup_profile_fields' );
		$this->loader->add_action( 'edit_user_profile', $plugin_admin, 'view_created_pup_profile_fields' );
		
		/* Display Pronto Portal Fields in `Add new User` page. */
		$this->loader->add_action( 'user_new_form', $plugin_admin, 'view_created_pup_profile_fields' );
		
		/* Filter Pronto Portal Fields errors. */
		$this->loader->add_filter( 'user_profile_update_errors', $plugin_admin, 'filter_created_pup_profile_fields', 10, 3 );
		
		/* Save Pronto Portal Fields. */
		$this->loader->add_action( 'user_register', $plugin_admin, 'save_created_pup_profile_fields', 10, 1 );
		
		$this->loader->add_filter( 'get_avatar', $plugin_admin, 'pup_override_gravatar' , 1 , 5 );

		/*
		* Save General Settings
		*/	
		$this->loader->add_action( 'wp_ajax_wfc_portal_generalsettings_Save',$plugin_admin, 'wfc_portal_savegeneralsettings_callback');
		$this->loader->add_action( 'wp_ajax_nopriv_wfc_portal_generalsettings_Save',$plugin_admin, 'wfc_portal_savegeneralsettings_callback');
	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Pronto_Wp_Portal_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
	}

	/**
	 * Register all of the hooks related to the pronto portal template functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_template_hooks() {

		$portal_template = new Pronto_Wp_Portal_Template( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'init', $portal_template, 'start_session' );
		$this->loader->add_action( 'wp_enqueue_scripts', $portal_template, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $portal_template, 'enqueue_scripts' );
		
		add_shortcode('pronto-portal', array( $portal_template, 'load_template' ));
	}


	/**
	 * Register all of the hooks related to the pronto portal login functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_login_hooks() {

		$portal_login = new Pronto_Wp_Portal_Login( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'template_redirect', $portal_login, 'check_user_logged_in' );
		$this->loader->add_action( 'login_form_login', $portal_login, 'redirect_to_portal_login' );
		$this->loader->add_action( 'wp_logout', $portal_login, 'redirect_after_logout' );
		
		$this->loader->add_filter( 'login_redirect', $portal_login, 'redirect_after_login', 10, 3 );
		$this->loader->add_filter( 'authenticate', $portal_login, 'authenticate', 111, 3 );
		$this->loader->add_filter( 'wp_authenticate_user', $portal_login, 'additional_authentication', 10, 3 );
		
		/* $this->loader->add_filter( 'login_errors', $portal_login, 'login_err', 10, 1 ); */

		$this->loader->add_filter('wp_nav_menu_items', $portal_login,'wfc_portal_add_loginout_navitem', 10, 2);
	}


	/**
	 * Register all of the hooks related to the pronto portal lost password functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_lostpass_hooks() {

		$portal_lostpass = new Pronto_Wp_Portal_Lostpass( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'template_redirect', $portal_lostpass, 'check_user_logged_in' );
		$this->loader->add_action( 'login_form_lostpassword', $portal_lostpass, 'redirect_to_portal_lostpass' );
		$this->loader->add_action( 'login_form_lostpassword', $portal_lostpass, 'process_portal_lostpass' );
		
		$this->loader->add_action( 'lostpassword_post', $portal_lostpass, 'additional_validation' );
	}


	/**
	 * Register all of the hooks related to the pronto portal lost password functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_resetpass_hooks() {

		$portal_resetpass = new Pronto_Wp_Portal_Resetpass( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'login_form_rp', $portal_resetpass, 'redirect_to_portal_resetpass' );
		$this->loader->add_action( 'login_form_resetpass', $portal_resetpass, 'redirect_to_portal_resetpass' );
		
		$this->loader->add_action( 'login_form_rp', $portal_resetpass, 'process_reset_password' );
		$this->loader->add_action( 'login_form_resetpass', $portal_resetpass, 'process_reset_password' );
		$this->loader->add_action( 'template_redirect', $portal_resetpass, 'check_reset_password_params' );
	}


	/**
	 * Register all of the hooks related to the pronto portal lost password functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_register_hooks() {

		$portal_register = new Pronto_Wp_Portal_Register( $this->get_plugin_name(), $this->get_version() );
		
		$this->loader->add_action( 'template_redirect', $portal_register, 'check_user_logged_in' );
		$this->loader->add_action( 'login_form_register', $portal_register, 'redirect_to_portal_registration' );
		$this->loader->add_action( 'login_form_register', $portal_register, 'pup_user_register' );
		$this->loader->add_filter( 'pre_user_email', $portal_register, 'skip_duplicate_email' );
	}
	
	
	/**
	 * Register all of the hooks related to the pronto portal profile functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_profile_hooks() {
		
		$portal_profile = new Pronto_Wp_Portal_Profile( $this->get_plugin_name(), $this->get_version() );
		 
		$this->loader->add_action( 'template_redirect', $portal_profile, 'redirect_to_login' );
		
		$this->loader->add_action( 'admin_post_update_profile', $portal_profile, 'update_user_profile' );
		$this->loader->add_action( 'admin_post_nopriv_update_profile', $portal_profile, 'update_user_profile' );
		$this->loader->add_filter( 'pre_user_email', $portal_profile, 'skip_duplicate_email' );
	}

	/**
	 * Register all of the hooks related to the pronto portal email functionality
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_email_hooks() {
		
		$portal_email = new Pronto_Wp_Portal_Email( $this->get_plugin_name(), $this->get_version() );
	
		$this->loader->add_filter( 'wp_mail_from', $portal_email, 'override_mail_from_email', 20, 1 );
		$this->loader->add_filter( 'wp_mail_from_name', $portal_email, 'override_mail_from_name', 20, 1 );
	}

	
	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Pronto_Wp_Portal_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}
	
	/**
	 * Name : get_base
	 * Description: Initialize pronto ultimate base plugin
	 * LastUpdated : July 10, 2017
	 *
	 * @author   Danryl Carpio <danryl@alphasys.com.au>
	 * @return bool|Pronto_Base
	 * @since    1.0.0
	 */
	public function get_base() {
		if( class_exists( 'Pronto_Base' ) ) {
			return new Pronto_Base();
		}
		return false;
	}
}