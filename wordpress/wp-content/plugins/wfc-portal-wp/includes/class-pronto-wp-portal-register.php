<?php

/**
 * Class Pronto_Wp_Portal_Register
 *
 * Register all actions and filters related to login
 *
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Register extends Pronto_Wp_Portal_Common {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		
		parent::__construct();
	}

	/**
	 * A function callback to `login_form_login` action hook.
	 *
	 * This function is responsible for Redirecting the user  
	 * to the portal login page instead of wp-login.php.
	 *
	 * @see Pronto_Wp_Portal::define_login_hooks()
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 25, 2017
	 */
	public function redirect_to_portal_registration() {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[1]['value']) ? $pup_pages[1]['value'] : '';

	    if ( $_SERVER['REQUEST_METHOD'] == 'GET' && 
	    	!empty($page_value) && 
	    	$page_value != 'wordpress-register-user-default-page') {
	    		
	        $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
	     
	        if ( is_user_logged_in() ) {

	            $user = wp_get_current_user();

			    if ( user_can( $user, 'manage_options' ) ) 
			        wp_redirect( admin_url() );       
			    else 
			        wp_redirect( home_url( $this->pup_page('profile') ) );

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
	        } elseif( '0' == get_option( 'users_can_register' ) ) {
				wp_redirect( home_url( $this->pup_page('login') ) );

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
			}
	 
	        wp_redirect( home_url( $this->pup_page('register') ) );

		    if (defined('PHPUNIT_RUNNING')) return;
		    else exit;
	    }
	}

	/**
     * Callback function to allow duplicate email on registration.
     *
	 * @author    John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
     * @return		object		
     * @since		1.0.0
	 *
	 * @LastUpdated	December 10, 2017
     */
	function skip_duplicate_email( $user_email ){
		if( get_option( 'pup_duplicate_email' ) == "1" ) {
			define( 'WP_IMPORTING', 'SKIP_EMAIL_EXIST' );
		}
		return $user_email;
	}
	
	/**
	 * Redirect to login if user will try to access the registration page and registration is
	 *				  not allowed. Redirect to profile if user is already logged in.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since 	1.0.0
	 *
	 * @LastUpdated September 12, 2017
	 */
	public function check_user_logged_in() {


		/*if( is_page( 'register' ) && '0' == get_option( 'users_can_register' ) ) {
			wp_redirect( home_url( $this->pup_page('login') ) );
			exit;
		} 
		else*/
			Pronto_Wp_Portal_Common::redirect_logged_in_user( 'register' , $this->pup_page('profile') );
	}
	
	/**
	 * Name: pup_user_register
	 * Description: registers a user in wordpress.
	 * Last Updated: 9/5/2017
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @return void true if user is registered, false if user credentials contains errors.
	 * @since     1.0.0
	 */
	public function pup_user_register() {		
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] ) {

			$wp_err = new WP_Error();
			$redirect_url = home_url( $this->pup_page('register') );
			$result = $admin_emails = array();
			
			if( ! isset( $_POST['data'] ) || empty( $_POST['data'] ) ) {
				$wp_err->add( 'nonce_err', __( '<strong>ERROR</strong>: No data received from the server.', 'pu-portal' ) );
				$_SESSION['pronto_portal_errors'] = $wp_err;
				
				wp_redirect( $redirect_url );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			}

			foreach( $_POST['data'] as $key => $val ) {
				if( $key == $val['sf_key'] && $key != 'ASPU__WPUsername__c' && $key != 'Email' ) {
					
					$sf_umeta[ $key ] = sanitize_text_field( $val['value'] );
					
					if( $val['type'] == 'date' || $val['type'] == 'datetime	' ) {
						$sf_umeta[ $key ] = date( "Y-m-d\TH:i:s", strtotime( $val[ 'value' ] ) );
					}
				}	
				
				$sf_data[ $val['sf_key'] ] = $result[ $key ] = sanitize_text_field( $val['value'] );
				
				if( $val['type'] == 'date' || $val['type'] == 'datetime	' ) {
					$result[ $key ] = date( "Y-m-d\TH:i:s", strtotime( $val[ 'value' ] ) );
				}
			}	
			
			$_SESSION['reg_form_data'] = $_POST['data']; 
			
			$sanitized_user_login = sanitize_user( $result['ASPU__WPUsername__c'] );
			$user_login = $result['ASPU__WPUsername__c'];
			$user_email = apply_filters( 'user_registration_email', $result['Email'] );
			
			if ( $sanitized_user_login == '' ) {
				$wp_err->add( 'empty_username', __( '<strong>ERROR</strong>: Please enter a username.' ) );
			} elseif ( ! validate_username( $user_login ) ) {
				$wp_err->add( 'invalid_username', __( '<strong>ERROR</strong>: This username is invalid because it uses illegal characters. Please enter a valid username.' ) );
				$sanitized_user_login = '';
			} elseif ( username_exists( $sanitized_user_login ) ) {
				$wp_err->add( 'username_exists', __( '<strong>ERROR</strong>: This username is already registered. Please choose another one.' ) );
			} else {
				$illegal_user_logins = array_map( 'strtolower', (array) apply_filters( 'illegal_user_logins', array() ) );
				if ( in_array( strtolower( $sanitized_user_login ), $illegal_user_logins ) ) {
					$wp_err->add( 'invalid_username', __( '<strong>ERROR</strong>: Sorry, that username is not allowed.' ) );
				}
			}
			
			if ( $user_email == '' ) {
				$wp_err->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your email address.' ) );
			} elseif ( ! is_email( $user_email ) ) {
				$wp_err->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
				$user_email = '';
			} 
			elseif ( get_option( 'pup_duplicate_email' ) != "1" && email_exists( $user_email ) ) {
				$wp_err->add( 'email_exists', __( '<strong>ERROR</strong>: This email is already registered, please choose another one.' ) );
			}
			
			if( $wp_err->get_error_code() ) {
				$_SESSION['pronto_portal_errors'] = $wp_err;
				
				wp_redirect( $redirect_url );

				if (defined('PHPUNIT_RUNNING')) return;
				else exit;
			}

			$hasAvatar = 0;
			
			if( isset( $_FILES['avatar'] ) 
			&& ! empty( $_FILES['avatar']['name'] ) 
			&& $_FILES['avatar']['size'] != '0' 
			&& $_FILES['avatar']['error'] != '4' ) {
				$allowedImageType = array("image/jpeg", "image/png");
				
				if( ! in_array( $_FILES['avatar']['type'], $allowedImageType ) ) {
					
					$wp_err->add( 'file_not_supported', __( '<strong>ERROR</strong>: File type not supported.', 'pu-portal' ) );
					$_SESSION['pronto_portal_errors'] = $wp_err;
				
					wp_redirect( $redirect_url );

					if (defined('PHPUNIT_RUNNING')) return;
					else exit;
				}
				
				if( $_FILES["avatar"]["size"] > 1000000 ) {
					$wp_err->add( 'file_size_exceeded', __( '<strong>ERROR</strong>: Image exceeds the maximum size of 1MB.', 'pu-portal' ) );
					$_SESSION['pronto_portal_errors'] = $wp_err;
				
					wp_redirect( $redirect_url );

					if (defined('PHPUNIT_RUNNING')) return;
					else exit;
				}
				
				$hasAvatar = 1;
			}
			
			/* register contact into salesforce */
			if( ! array_key_exists('LastName', $sf_data) || ( isset( $sf_data['LastName'] ) && empty( $sf_data['LastName'] ) ) ) {
				$sf_data['LastName'] = 'Anonymous';
			}
			
			if( ! array_key_exists('FirstName', $sf_data ) || ( isset( $sf_data['FirstName'] ) && empty( $sf_data['FirstName'] ) ) ) {
				$sf_data['FirstName'] = 'Contact';
			}
			
			/* Defaults */
			$sf_data['ASPU__Action__c'] = 'include';
			$sf_data['ASPU__Step1__c'] = 'true';
			
			$sf_fields = Pronto_Wp_Portal_Admin::add_salesforce_fields( $sf_data );
			if( $sf_fields['status_code'] !== 201 ) {
				if (isset($sf_fields['sf_error'])) {
					foreach($sf_fields['sf_error'] as $sf_err_key => $sf_err_data )
						$wp_err->add( $sf_err_data['errorCode'], __('<strong>SALESFORCE ERROR: </strong>', 'pu-portal') . $sf_err_data['message'] );
					$_SESSION['pronto_portal_errors'] = $wp_err;

					wp_redirect( $redirect_url );

					if (defined('PHPUNIT_RUNNING')) return;
					else exit;
				}
			}
			
			unset( $_SESSION['reg_form_data'] );

			$result['user_login']	= trim( $result['ASPU__WPUsername__c'] );
			$result['user_email']	= trim( $result['Email'] );
			$result['user_pass']	= wp_generate_password( 12, false );
			$result['last_name']	= ( ! isset( $result['last_name'] ) || empty( $result['last_name'] ) ) ? 'Anonymous' : $result['last_name'];
			$result['first_name']	= ( ! isset( $result['first_name'] ) || empty( $result['first_name'] ) ) ? 'Contact' : $result['first_name'];
			
			$user_id = wp_insert_user( $result );

			/* update user meta */
			update_user_option( $user_id, 'default_password_nag', true, true ); //Set up the Password change nag.
			foreach( $sf_umeta as $key => $val ) {
				update_user_meta( $user_id, $key, $val );
			}
			if (isset($sf_fields['sf_response']))
				update_user_meta( $user_id, 'pup_sf_id', $sf_fields['sf_response']['id'] );
			
			if( $hasAvatar ) {
				Pronto_Wp_Portal_Common::pup_upload_image( $user_id, 'pup_avatar', 'avatar' );
			}
			
			do_action( 'register_new_user', $user_id );
			
			$redirect_url = add_query_arg( 'checkemail', 'confirm', home_url( $this->pup_page('login') ) );
			
			wp_redirect( $redirect_url );

			if (defined('PHPUNIT_RUNNING')) return;
			else exit;
		}
	}
}