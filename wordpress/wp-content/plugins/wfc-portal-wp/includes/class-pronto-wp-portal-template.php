<?php

/**
 * Class Pronto_Wp_Portal_Template
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Template {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Holds errors object
	 *
	 * @since 6.0
	 * @access public
	 * @var object
	 */
	public $errors;
	
	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Start Session.
	 *
	 * @since    1.0.0
	 */
	public function start_session() {
	    if(!session_id()) {
	        session_start();
	    }
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/pu-portal-admin.css', array(), $this->version, 'all' );

		/*
		 * Register Profile form css
		 */
		wp_register_style( 'wfc-portal-pronto-profile-css', plugin_dir_url( __FILE__ ) . '../templates/css/pronto-profile.css', array(), $this->version, 'all' );

		/*
		 * Register Profile form css
		 */
		wp_register_style( 'wfc-portal-pronto-profile-edit-css', plugin_dir_url( __FILE__ ) . '../templates/css/pronto-profile-edit.css', array(), $this->version, 'all' );

		/*
		* Register Login form css
		*/
		wp_register_style( 'wfc-portal-pronto-login-css', plugin_dir_url( __FILE__ ) . '../templates/css/pronto-login.css', array(), $this->version, 'all' );

		/*
		* Register Password Lost form css
		*/
		wp_register_style( 'wfc-portal-pronto-password-lost-css', plugin_dir_url( __FILE__ ) . '../templates/css/pronto-password-lost.css', array(), $this->version, 'all' );

		/*
		* Register Password Reset form css
		*/
		wp_register_style( 'wfc-portal-pronto-password-reset-css', plugin_dir_url( __FILE__ ) . '../templates/css/pronto-password-reset.css', array(), $this->version, 'all' );

		/*
		 * Register Register form css
		 */
		wp_register_style( 'wfc-portal-pronto-register-css', plugin_dir_url( __FILE__ ) . '../templates/css/pronto-register.css', array(), $this->version, 'all' );
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/pu-portal-admin.js', array( 'jquery' ), $this->version, false );

	}
	
	/**
	 * A function callback to `pronto-portal` shortcode.
	 * Returns the portal template.
	 *
	 * @see        Pronto_Wp_Portal::define_template_hooks()
	 *
	 * @author     Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since      1.0.0
	 * @access     public
	 *
	 * @param object $attrs shortcode attributes
	 *
	 * @return false|string
	 *
	 * @LastUpdated August 08, 2017
	 */
	public function load_template($attrs){

		$template = $this;

		extract( shortcode_atts( 
				array( 'action' => 'login')
			, $attrs )
	    );

	    switch ( $action ) {
	    	case 'profile':
				$temp_file = 'pronto-profile.php';
				break;
			case 'profile-edit':
				$temp_file = 'pronto-profile-edit.php';
				break;
			case 'register':
                if( is_page( 'register' ) && '0' == get_option( 'users_can_register' ) ){
                    $temp_file = 'pronto-register-not-allowed.php';
                } else{
                    $temp_file = 'pronto-register.php';
                }
				break;
			case 'lostpass':
				$temp_file = 'pronto-password-lost.php';
				break;
			case 'resetpass':
				$temp_file = 'pronto-password-reset.php';
				break;
			case 'login':
			default:
				$temp_file = 'pronto-login.php';
		        // $temp_file = 'pronto-register.php';
		}


		if ( '' === locate_template( 'pu-templates/'.$temp_file, false, false ) ) {
		    switch ( $action ) {
		    	case 'profile':
				    wp_enqueue_style('wfc-BootstrapCDN-css');
				    wp_enqueue_style('wfc-portal-pronto-profile-css');
					break;
				case 'profile-edit':
					wp_enqueue_style('wfc-BootstrapCDN-css');
					wp_enqueue_style('wfc-BootstrapCDN-font-awesome-css');
					wp_enqueue_style('wfc-portal-pronto-profile-edit-css');
					break;
				case 'register':
					wp_enqueue_style('wfc-BootstrapCDN-css');
					wp_enqueue_style('wfc-BootstrapCDN-font-awesome-css');
					wp_enqueue_style('wfc-portal-pronto-register-css');
					break;
				case 'lostpass':
					wp_enqueue_style('wfc-BootstrapCDN-css');
					wp_enqueue_style('wfc-portal-pronto-password-lost-css');
					break;
				case 'resetpass':	
					wp_enqueue_style('wfc-BootstrapCDN-css');
					wp_enqueue_style('wfc-portal-pronto-password-reset-css');				
					break;
				case 'login':
				default:
					wp_enqueue_style('wfc-BootstrapCDN-css');
			        // wp_enqueue_style('wfc-BootstrapCDN-font-awesome-css');
					wp_enqueue_style('wfc-portal-pronto-login-css');
			        // wp_enqueue_style('wfc-portal-pronto-register-css');
			}			
		}


		ob_start();

		if ( '' === locate_template( 'pu-templates/'.$temp_file, false, false ) )
  			include plugin_dir_path( dirname(__FILE__) ) . 'templates/' . $temp_file;
  		else
  			include(locate_template( 'pu-templates/'.$temp_file ));

  		return ob_get_clean();
	}
	
	/**
	 * Returns the portal error messages.
	 *
	 * @author     Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since      1.0.0
	 * @access     public
	 *
	 * @param object $wp_error error object
	 *
	 * @return string
	 *
	 * @LastUpdated July 26, 2017
	 */
	public function get_errors($wp_error) {
		$output = '';

		if(is_wp_error($wp_error)){
			if ( $wp_error->get_error_code() ) {
				$errors = '';
				$messages = '';
				foreach ( $wp_error->get_error_codes() as $code ) {
					$severity = $wp_error->get_error_data( $code );
					foreach ( $wp_error->get_error_messages( $code ) as $error ) {
						if ( 'message' == $severity )
							$messages .= '    ' . $error . "<br />\n";
						else
							$errors .= '    ' . $error . "<br />\n";
					}
				}
				if ( ! empty( $errors ) )
					$output .= '<p class="error">' . apply_filters( 'login_errors', $errors ) . "</p>\n";
				if ( ! empty( $messages ) )
					$output .= '<p class="message">' . apply_filters( 'login_messages', $messages ) . "</p>\n";
			}
		}
		
		return $output;
	}

	/**
	 * Prints the portal errors
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated  July 26, 2017
	 */
	public function the_errors() {

		if( isset($_SESSION['pronto_portal_errors']) ){

			$errors = $_SESSION['pronto_portal_errors'];

			unset( $_SESSION['pronto_portal_errors'] );
		}else
			$errors = new WP_Error();

		$secure_cookie = '';
		$reauth = empty($_REQUEST['reauth']) ? false : true;
		$interim_login = isset($_REQUEST['interim-login']);

		if( isset( $_REQUEST['redirect_to'] ) ) {
			$redirect_to = $_REQUEST['redirect_to'];
			/*Redirect to https if user wants ssl*/
			if ( $secure_cookie && false !== strpos($redirect_to, 'wp-admin') )
				$redirect_to = preg_replace('|^http://|', 'https://', $redirect_to);
		}else 
			$redirect_to = admin_url();
		
		/*Clear errors if loggedout is set.*/
		if( !empty($_GET['loggedout']) || $reauth )
			$errors = new WP_Error();

		if( $interim_login ) {
			if( ! $errors->get_error_code() )
				$errors->add( 'expired', __( 'Your session has expired. Please log in to continue where you left off.', 'pu-portal' ), 'message' );
		}else {
			/*Some parts of this script use the main login form to display a message*/
			if( isset($_GET['loggedout']) && true == $_GET['loggedout'] )
				$errors->add('loggedout', __('You are now logged out.', 'pu-portal'), 'message');
			elseif( isset($_GET['registration']) && 'disabled' == $_GET['registration'] )
				$errors->add('registerdisabled', __('User registration is currently not allowed.', 'pu-portal'));
			elseif( isset($_GET['checkemail']) && 'confirm' == $_GET['checkemail'] )
				$errors->add('confirm', __('Check your email for the confirmation link.', 'pu-portal'), 'message');
			elseif( isset($_GET['checkemail']) && 'newpass' == $_GET['checkemail'] )
				$errors->add('newpass', __('Check your email for your new password.', 'pu-portal'), 'message');
			elseif( isset($_GET['checkemail']) && 'registered' == $_GET['checkemail'] )
				$errors->add('registered', __('Registration complete. Please check your email.', 'pu-portal'), 'message');
			elseif( strpos( $redirect_to, 'about.php?updated' ) )
				$errors->add('updated', __( '<strong>You have successfully updated WordPress!</strong> Please log back in to see what&#8217;s new.', 'pu-portal'), 'message' );
			
			elseif( isset($_GET['password']) && 'changed' == $_GET['password'] )
				$errors->add('changed', __('Your password has been reset.', 'pu-portal'), 'message');
			elseif( isset($_GET['profile']) && 'updated' == $_GET['profile'] )
				$errors->add('updated', __('Your profile has been updated.', 'pu-portal'), 'message');
		}

		/*Reset password error messages.*/
		if( isset( $_REQUEST['error'] ) ) {
			if( 'invalidkey' == $_REQUEST['error'] ) 
				$errors->add( 'invalidkey', __( 'Your password reset link appears to be invalid. Please request a new link below.', 'pu-portal' ) );
			elseif( 'expiredkey' == $_REQUEST['error'] ) 
				$errors->add( 'expiredkey', __( 'Your password reset link has expired. Please request a new link below.', 'pu-portal' ) );
			elseif( 'password_reset_mismatch' == $_REQUEST['error'] ) 
				$errors->add( 'password_reset_mismatch', __( 'The two passwords you entered don\'t match.', 'pu-portal' ) );
			elseif( 'password_reset_empty' == $_REQUEST['error'] ) 
				$errors->add( 'password_reset_empty', __( 'Sorry, we don\'t accept empty passwords.', 'pu-portal' ) );
		}

		echo $this->get_errors( $errors );
	}
	
	/**
	 * Returns template message for requested action.
	 *
	 * @since  1.0
	 * @access public
	 *
	 * @param string $action Action to retrieve
	 *
	 * @return mixed|void The requested template message
	 */
	public static function get_template_message( $action = '' ) {
		switch ( $action ) {
			case 'register':
				$message = __( 'Register For This Site', 'pu-portal' );
				break;
			case 'lostpassword':
				$message = __( 'Please enter your username. You will receive a link to create a new password via email.', 'pu-portal' );
				break;
			case 'resetpass':
				$message = __( 'Enter your new password below.', 'pu-portal' );
				break;
			default:
				$message = '';
		}
		$message = apply_filters( 'login_message', $message );

		return apply_filters( 'prontowp_template_message', $message, $action );
	}

	/**
	 * Outputs template message for requested action
	 *
	 * @since 1.0
	 * @access public
	 *
	 * @param string $action Action to retrieve
	 * @param string $before_message Text/HTML to add before the message
	 * @param string $after_message Text/HTML to add after the message
	 */
	public function the_template_message( $action = 'login', $before_message = '<p class="message">', $after_message = '</p>' ) {
		if ( $message = self::get_template_message( $action ) )
			echo $before_message . $message . $after_message;
	}
	
	/**
	 * Renders data for template.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @param 	bool 		$tpl	the template in which the data will be used.
	 *
	 * @return 	array 	 	merged data array.
	 * @LastUpdated September 8, 2017
	 */
	public function get_template_options( $tpl ) {
		
		if( $tpl === 'register' ) {
			return array(
				'reg_settings' => get_option('reg_settings')
			);
		} elseif( $tpl === 'profile' ) {
			return array(
				'reg_settings' => get_option('reg_settings'),
				'pup_profile_settings' => get_option('pup_profile_settings'),
				'curr_uid' => get_current_user_id()
			);			
		} elseif( $tpl === 'profile-edit' ) {
			return array(
				'reg_settings' => get_option('reg_settings'),
				'curr_uid' => get_current_user_id()
			);			
		} elseif( $tpl === 'login' || $tpl === 'lostpassword' ) {
			return array(
				'user_login_label' => ( get_option( 'pup_duplicate_email' ) == "1" ) ? __('Username', 'pu-portal') : __('Username or Email Address', 'pu-portal')
			);			
		}
	}
}