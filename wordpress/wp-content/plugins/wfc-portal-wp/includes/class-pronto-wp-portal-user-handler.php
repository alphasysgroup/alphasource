<?php
/**
 * Class WFC_Portal_UserHandler
 *
 * This class handles the users extra functionalities like reset password.
 *
 *
 * @package           Pronto_Wp_Portal
 * @author            Junjie Canonio <junjie@alphasys.com.au>
 * @since             1.0.0
 *
 */
class WFC_Portal_UserHandler {
	/**
	 * Sends the user a reset password email.
	 *
	 * @author       Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $sfid
	 * @param $username
	 *
	 * @return array
	 *
	 * @LastUpdated  August 7, 2018
	 */
	public static function wfc_portal_request_resetpass($sfid, $username) {
		$response = array(
			'status' => 'bad',
			'message' => 'error', 
		);

	    $args = array (
	        'order' => 'ASC',
	        'meta_query' => array(
	            'relation' => 'AND',
	            array(
	                'key'     => 'pup_sf_id',
	                'value'   => $sfid,
	                'compare' => 'LIKE'
	            ),
	        )
	    );
	    $wp_user_query = new WP_User_Query($args);
	    $user_query = $wp_user_query->get_results();		

	    if (!is_array($user_query) || empty($user_query)) {
			$response = array(
				'status' => 'bad',
				'message' => 'No user has this '.$sfid.' Salesforce Id.', 
			);	    	
	    }else{
	    	if (empty($username)) {
	    		$response = array(
					'status' => 'bad',
					'message' => 'Please provide Username.', 
				);	
	    	}else{
		    	$pup_duplicate_email = get_option( 'pup_duplicate_email' );
			    $user_by_login = get_user_by( 'login', $username );		
				if(!$user_by_login) {
					$response = array(
						'status' => 'bad',
						'message' => 'The user with this Salesforce Id : '.$sfid.' does not match this Username : '.$username, 
					);					
				}else{
					pup_lost_noti_message( $user_by_login );
					$response = array(
						'status' => 'good',
						'message' => 'A reset password email has been sent to this person.', 
					);
				}	    		
	    	}
	    }
		return $response;
	}
}