<?php


if ( !function_exists('wp_new_user_notification') ) {
	
	function pup_toString( $theArray,$value ) {

		$theValue = '';
		$arrayToReplace = array("[","]");
		$strReplace = str_replace($arrayToReplace, "###", $value);
		$newArrayExplode = explode('###',$strReplace);
		
		foreach($newArrayExplode as $newkey => $newvalue) {
		
			foreach($theArray as $keyz) {
				if(array_key_exists($newvalue,$keyz)) {
					
					$keyexist = $keyz[$newvalue];
					$keyx = array_search($keyexist, $keyz);
					$searchpos = array_search($keyx, $newArrayExplode);						
					
					array_splice($newArrayExplode, $searchpos, 1, $keyexist);
					
					$theValue = $newArrayExplode;
				} else {
					$theValue = $newArrayExplode;
				}
			}
		}
		return implode('',$theValue );
	}

	function pup_new_user_mail( $user, $fieldValue, $adminFieldValue ) {
		
		global $wpdb, $wp_hasher ;

		$key = wp_generate_password( 20, false );
		
		if ( empty( $wp_hasher ) ) {
			require_once ABSPATH . WPINC . '/class-phpass.php';
			$wp_hasher = new PasswordHash( 8, true );
		}
		
		$hashed = time() . ':' . $wp_hasher->HashPassword( $key );

		$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

		$subject =	( ! empty( $fieldValue['fieldSubject'] ) ) ? $fieldValue['fieldSubject'] : sprintf(__('[%s] Your username and password info'), $blogname);
		
		$_POST['override_mail_from_name'] = ( ! empty( trim( $fieldValue['fieldName'] ) ) ) ? $fieldValue['fieldName'] : '';
		
		$_POST['override_mail_from_email'] = ( ! empty( trim( $fieldValue['fieldEmail'] ) ) ) ? $fieldValue['fieldEmail'] : '';
		
		$headers 	= array('Content-Type: text/plain; charset=utf-8');
		
		if( empty( trim( $fieldValue['fieldMessagex'] ) ) && empty( trim( $fieldValue['fieldMessage'] ) ) ) {
			$message = sprintf(__( 'Username: %s', 'pu-portal' ), $user->user_login) . "\r\n\r\n";
			$message .= __( 'To set your password, visit the following address:', 'pu-portal' ) . "\r\n\r\n";
			$message .= '<' . network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login') . ">\r\n\r\n";
			$message .= wp_login_url() . "\r\n";
		} else {
			if($fieldValue['fieldFormat'] == 'HTML') {
				$headers 	= array('Content-Type: text/html; charset=UTF-8');
				
				$password_reset_link = network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login');

				$message = str_replace('pu_password_reset_link', $password_reset_link, $fieldValue['fieldMessagex']);

			} else {
				$message 	=	$fieldValue['fieldMessage'] . "\r\n\r\n";
				$message 	.= sprintf(__( 'Username: %s', 'pu-portal' ), $user->user_login) . "\r\n\r\n";
				$message 	.=  network_site_url("wp-login.php?action=rp&key=$key&login=" . rawurlencode($user->user_login), 'login');
			}
		}

		if (has_filter('pup_new_user_mail_message_flt') == true) {
			$message = apply_filters( 'pup_new_user_mail_message_flt', $message );
		}
		
		wp_mail( $user->user_email, $subject, $message, $headers );
		
		if( version_compare(get_bloginfo('version'),'4.7', '>=') ) {

			$switched_locale = switch_to_locale( get_user_locale( $user ) );

			if ( $switched_locale ) {
				restore_previous_locale();
			}

			/* send to admin */
			$switched_locale = switch_to_locale( get_user_locale() );
		} 

		$subject =	( ! empty( $adminFieldValue['fieldSubject'] ) ) ? $adminFieldValue['fieldSubject'] : sprintf( __( '[%s] New User Registration' ), $blogname );
		
		$to = ( empty( trim( $adminFieldValue['fieldTo'] ) ) ) ? get_option( 'admin_email' ) : $adminFieldValue['fieldTo'];
		
		$_POST['override_mail_from_name'] = ( ! empty( trim( $adminFieldValue['fieldName'] ) ) ) ? $adminFieldValue['fieldName'] : '';
		
		$_POST['override_mail_from_email'] = ( ! empty( trim( $adminFieldValue['fieldEmail'] ) ) ) ? $adminFieldValue['fieldEmail'] : '';
		
		$headers = array('Content-Type: text/plain; charset=utf-8');

		if( empty( trim( $adminFieldValue['fieldMessagex'] ) ) && empty( trim( $adminFieldValue['fieldMessage'] ) ) ) {
			$message  = sprintf( __( 'New user registration on your site %s:', 'pu-portal' ), $blogname ) . "\r\n\r\n";
			$message .= sprintf( __( 'Username: %s', 'pu-portal' ), $user->user_login ) . "\r\n\r\n";
			$message .= sprintf( __( 'Email: %s', 'pu-portal' ), $user->user_email ) . "\r\n";
		} else {
			if( $adminFieldValue['fieldFormat'] == 'HTML' ) {
				$headers 	= array('Content-Type: text/html; charset=UTF-8');

				$message = str_replace('pu_user_email', $user->user_email, $adminFieldValue['fieldMessagex']);
			} else {
				$message 	=	$adminFieldValue['fieldMessage'] . "\r\n\r\n";
				$message 	.= sprintf( __( 'Username: %s', 'pu-portal' ), $user->user_login ) . "\r\n\r\n";
			}
		}
		wp_mail( $to, $subject, $message, $headers );
		
		if( version_compare( get_bloginfo('version'),'4.7', '>=' ) ){

			if ( $switched_locale ) {
				restore_previous_locale();
			}
		}
	}
	
    function wp_new_user_notification($user_id, $plaintext_pass) {
		global $wpdb;
		
		$user = new WP_User($user_id);
		
		$metaKey = $wpdb->get_results( "SELECT meta_key FROM $wpdb->usermeta GROUP BY meta_key", ARRAY_A );
		
		$newArray = array();
		
		foreach($metaKey as $key => $val) {
			
			if($val['meta_key'] != 'Email') {
				$userData = get_user_meta($user_id, $val['meta_key'], true);
				$the_value =  (is_array($userData)) ? implode(',',$userData ) : $userData;
			} else {
				$the_value = stripslashes($user->user_email);
			}
			
			$next = array(
				$val['meta_key'] => $the_value,
			);
			
			array_push($newArray,$next);
		}
		
		$valueSettings =  get_option('user_data_settings');

		$dataBaseOnRole = $valueSettings['new_user'];
		$input_1 = 'nwsr_field_name';
		$input_2 = 'nwsr_field_email';
		$input_3 = 'nwsr_field_subject';
		$input_4 = 'nwsr_field_message';
		$input_5 = 'nwsr_field_format';
		$input_6 = 'nwsr_field_messagex';

		$adminDataBaseOnRole = $valueSettings['new_admin'];
		$admin_input_1 = 'nwsradmin_field_name';
		$admin_input_2 = 'nwsradmin_field_email';
		$admin_input_3 = 'nwsradmin_field_subject';
		$admin_input_4 = 'nwsradmin_field_message';
		$admin_input_5 = 'nwsradmin_field_format';
		$admin_input_6 = 'nwsradmin_field_messagex';
		$admin_input_7 = 'nwsradmin_field_to';

		$fieldEmail = '';
		$fieldName = '';
		$fieldSubject = '';
		$fieldMessage = '';
		$fieldFormat = '';
		$fieldMessagex = '';
		$admin_fieldName = '';
		$admin_fieldEmail = '';
		$admin_fieldSubject = '';
		$admin_fieldMessage = '';
		$admin_fieldFormat = '';
		$admin_fieldMessagex = '';
		$admin_fieldTo = '';

		if( is_array( $dataBaseOnRole ) ){
			foreach( $dataBaseOnRole as $key => $value ) {

				$varfunc = pup_toString( $newArray, stripslashes($value) );

				switch($key) {
					case $input_1:
						$fieldName = $varfunc;
						break;
					case $input_2:
						$fieldEmail = $varfunc;
						break;
					case $input_3:
						$fieldSubject = $varfunc;
						break;
					case $input_4:
						$fieldMessage = $varfunc;
						break;
					case $input_5:
						$fieldFormat = $value;
						break;
					case $input_6:
						$fieldMessagex = $varfunc;
						break;
				}
			}
		}
		

		if( is_array( $adminDataBaseOnRole ) ){
			foreach( $adminDataBaseOnRole as $key => $value ) {

				$varfunc = pup_toString( $newArray, stripslashes($value) );

				switch($key) {
					case $admin_input_1:
						$admin_fieldName = $varfunc;
						break;
					case $admin_input_2:
						$admin_fieldEmail = $varfunc;
						break;
					case $admin_input_3:
						$admin_fieldSubject = $varfunc;
						break;
					case $admin_input_4:
						$admin_fieldMessage = $varfunc;
						break;
					case $admin_input_5:
						$admin_fieldFormat = $value;
						break;
					case $admin_input_6:
						$admin_fieldMessagex = $varfunc;
						break;
					case $admin_input_7:
						$admin_fieldTo = $varfunc;
						break;
				}
			}
		}

		$fieldValue = array(
			'fieldSubject'	=> $fieldSubject,
			'fieldName'		=> $fieldName,
			'fieldEmail'	=> $fieldEmail,
			'fieldMessage'	=> $fieldMessage,
			'fieldMessagex'	=> $fieldMessagex,
			'fieldFormat'	=> $fieldFormat,
		);
		
		$adminFieldValue = array(
			'fieldSubject'	=> $admin_fieldSubject,
			'fieldName'		=> $admin_fieldName,
			'fieldEmail'	=> $admin_fieldEmail,
			'fieldMessage'	=> $admin_fieldMessage,
			'fieldMessagex'	=> $admin_fieldMessagex,
			'fieldTo'		=> $admin_fieldTo,
			'fieldFormat'	=> $admin_fieldFormat,
		);
		pup_new_user_mail( $user, $fieldValue, $adminFieldValue );
    }
} ?>