<?php
	function retrieve_toString( $theArray,$value ) {

		$theValue = '';
		$arrayToReplace = array("[","]");
		$strReplace = str_replace($arrayToReplace, ",", $value);
		$newArrayExplode = explode(',',$strReplace);
		
		foreach($newArrayExplode as $newkey => $newvalue) {
		
			foreach($theArray as $keyz) {
				if(array_key_exists($newvalue,$keyz)) {

					$keyexist = $keyz[$newvalue];
					$keyx = array_search($keyexist, $keyz);
					$searchpos = array_search($keyx, $newArrayExplode);
						
					array_splice($newArrayExplode, $searchpos, 1, $keyexist);
					
					$theValue = $newArrayExplode;
					
				
				}else {
					$theValue = $newArrayExplode;
				}	
			}
		}
		return implode('',$theValue );
	}

	function pup_retrieve_mail($user,$fieldValue,$fieldFormat) {
		global $wpdb, $wp_hasher ;

		$key = wp_generate_password( 20, false );
		if ( empty( $wp_hasher ) ) {
			require_once ABSPATH . WPINC . '/class-phpass.php';
			$wp_hasher = new PasswordHash( 8, true );
		}
		$hashed = time() . ':' . $wp_hasher->HashPassword( $key );

		$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user->user_login ) );

		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

		$subject 	 =	( ! empty( trim( $fieldValue['fieldSubject'] ) ) ) ? $fieldValue['fieldSubject'] : sprintf( __( '%s Password Reset', 'pu-portal' ), $blogname );
		
		$_POST['override_mail_from_name'] = ( ! empty( trim( $fieldValue['fieldName'] ) ) ) ? $fieldValue['fieldName'] : '';
		
		$_POST['override_mail_from_email'] = ( ! empty( trim( $fieldValue['fieldEmail'] ) ) ) ? $fieldValue['fieldEmail'] : '';

		$headers 	= array('Content-Type: text/plain; charset=utf-8');
		
		if( empty( trim( $fieldValue['fieldMessagex'] ) ) && empty( trim( $fieldValue['fieldMessage'] ) ) ) {
			$message = __('Someone has requested a password reset for the following account:') . "\r\n\r\n";
			$message .= network_home_url( '/' ) . "\r\n\r\n";
			$message .= sprintf(__('Username: %s'), $user->user_login) . "\r\n\r\n";
			$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
			$message 	.= wp_login_url("wp-login") . '?action=rp&key=' . $key .'&login=' . $user->user_login . "\r\n";
		} else {
			if($fieldFormat == 'HTML') {
				$headers 	= array('Content-Type: text/html; charset=UTF-8');

				$message = str_replace('pu_user_email', $user->user_email, $fieldValue['fieldMessagex']);
				$password_reset_link = wp_login_url("wp-login") . '?action=rp&key=' . $key .'&login=' . $user->user_login;

				$message = str_replace('pu_password_reset_link', $password_reset_link, $message);

			} else {
				$message 	=	$fieldValue['fieldMessage'];
				$message 	.= "\r\n\r\n".wp_login_url("wp-login") . '?action=rp&key=' . $key .'&login=' . $user->user_login;
			}
		}

		wp_mail( $user->user_email, $subject, $message, $headers );
	}

	function pup_lost_noti_message( $user ) {
		
		global $wpdb;
		
		$metaKey = $wpdb->get_results( "SELECT meta_key FROM $wpdb->usermeta GROUP BY meta_key", ARRAY_A );
		
		$newArray = array();
		
		foreach($metaKey as $key => $val) {
			
			if($val['meta_key'] != 'Email') {
				$userData = get_user_meta($user->ID, $val['meta_key'], true);
				
				$the_value =  (is_array($userData)) ? implode(',',$userData ) : $userData;
			}else {
				$the_value = stripslashes($user->user_email);
			}

			$next = array(
				$val['meta_key'] => $the_value,
			);
			
			array_push( $newArray, $next );
		}

		$valueSettings =  get_option('user_data_settings');


		foreach($valueSettings['retrieve'] as $key => $value) {

			$varfunc = retrieve_toString( $newArray, stripslashes($value) );


			switch($key) {
				case 'retrieve_field_name':
					$fieldName = $varfunc;
				break;
				case 'retrieve_field_email':
					$fieldEmail = $varfunc;
				break;
				case 'retrieve_field_subject':
					$fieldSubject = $varfunc;	
				break;
				case 'retrieve_field_message':
					$fieldMessage = $varfunc;
				break;
				case 'retrieve_field_format':
					$fieldFormat = $value;
				break;
				case 'retrieve_field_messagex':
					$fieldMessagex = $varfunc;
				break;
			}
		}

		$fieldValue = array(
			'fieldSubject'	=> $fieldSubject,
			'fieldName'		=> $fieldName,
			'fieldEmail'	=> $fieldEmail,
			'fieldMessage'	=> $fieldMessage,
			'fieldMessagex'	=> $fieldMessagex,
		);

		pup_retrieve_mail($user,$fieldValue,$fieldFormat);
	}

	if ( !function_exists( 'wp_password_change_notification' ) ) {
	    function wp_password_change_notification() {}
	}
?>