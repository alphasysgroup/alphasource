<?php

/**
 * Class Pronto_Wp_Portal_Login
 *
 * Register all actions and filters related to login
 *
 *
 * @package           Pronto_Wp_Portal
 * @author            AlphaSys Pty Ltd
 */
class Pronto_Wp_Portal_Login extends Pronto_Wp_Portal_Common {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		parent::__construct();
	}
	
	/**
	 * A function callback to `authenticate` action hook.
	 *
	 *
	 * This function is responsible for Redirecting the user
	 * to the portal login page with the authentication error messages.
	 *
	 * @author     Jhobet Baroga
	 * @since      1.0.0
	 * @access     public
	 *
	 * @see        Pronto_Wp_Portal::define_login_hooks()
	 *
	 * @param $user
	 * @param $username
	 * @param $password
	 *
	 * @return   object User
	 *
	 * @LastUpdated September 08,2017
	 */
	public function authenticate( $user, $username, $password ) {
	    if ( $_SERVER['REQUEST_METHOD'] === 'POST' ) {

	    	$pup_duplicate_email = get_option( 'pup_duplicate_email' );
	    	$has_user_by_username = get_user_by( 'login', $username );

	        if ( is_wp_error( $user ) ) {
	        	if( $pup_duplicate_email == "1" ){
					if( $has_user_by_username && in_array( 'incorrect_password', $user->get_error_codes() ) ) {
						$user->remove( 'incorrect_password' );
						$user->add( 'invalid_username', sprintf( __( '<strong>ERROR</strong>: The password you entered for the username <strong>%s</strong> is incorrect. <a href="%s">Lost your password?</a>', 'pu-portal' ) , $username, wp_lostpassword_url() ) );
					} elseif( ! $has_user_by_username && in_array( 'incorrect_password', $user->get_error_codes() ) ) {
						$user->remove( 'incorrect_password' );
						$user->add( 'invalid_username', sprintf( __( '<strong>ERROR</strong>: Invalid username. <a href="%s">Lost your password?</a>', 'pu-portal' ) , wp_lostpassword_url() ) );
					} elseif( in_array( 'invalid_email', $user->get_error_codes() ) ) {	
						$user->remove( 'invalid_email' );
						$user->add( 'invalid_username', sprintf( __( '<strong>ERROR</strong>: Invalid username. <a href="%s">Lost your password?</a>', 'pu-portal' ) , wp_lostpassword_url() ) );
					}
	        	}
				
	        	$_SESSION['pronto_portal_errors'] = $user;
	 
	            wp_redirect( home_url( $this->pup_page('login') ) );
	            exit;
	        } elseif( $pup_duplicate_email == "1" && ! get_user_by( 'login', $username ) ) {

				$err = new WP_Error();

				$err->add( 'invalid_username', sprintf( __( '<strong>ERROR</strong>: Invalid username. <a href="%s">Lost your password?</a>', 'pu-portal' ) , wp_lostpassword_url() ) );
				
				$_SESSION['pronto_portal_errors'] = $err;
				
				wp_redirect( home_url( $this->pup_page('login') ) );
				exit;
			}
	    }
	    return $user;
	}

	/**
	 * A function callback to `login_form_login` action hook.
	 * @see Pronto_Wp_Portal::define_login_hooks()
	 *
	 * This function is responsible for Redirecting the user  
	 * to the portal login page instead of wp-login.php.
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 25,2017
	 */
	public function redirect_to_portal_login() {
		$pup_pages = get_option('pup_pages_settings');
		$page_value = isset($pup_pages[0]['value']) ? $pup_pages[0]['value'] : '';
	    
	    if ( $_SERVER['REQUEST_METHOD'] == 'GET' && 
	    	!empty($page_value) && 
	    	$page_value != 'wordpress-login-default-page') {

	        $redirect_to = isset( $_REQUEST['redirect_to'] ) ? $_REQUEST['redirect_to'] : null;
	     
	        if ( is_user_logged_in() ) {

	            $user = wp_get_current_user();

			    if ( user_can( $user, 'manage_options' ) ) 
			        wp_redirect( admin_url() );       
			    else 
			        wp_redirect( home_url( $this->pup_page('profile') ) );

		        if (defined('PHPUNIT_RUNNING')) return;
		        else exit;
	        }
	 
	        /*The rest are redirected to the portal login page*/
	        $login_url = home_url( $this->pup_page('login') );
	        if ( ! empty( $redirect_to ) ) {
	            $login_url = add_query_arg( 'redirect_to', $redirect_to, $login_url );
	        }
	 
	        wp_redirect( $login_url );

		    if (defined('PHPUNIT_RUNNING')) return;
		    else exit;
	    }
	}
	
	/**
	 * Redirect to profile if user is already logged in.
	 *
	 * @author    Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *            John Rhendon Gerona <rendhon@alphasys.com.au> \n
	 *            Juniel Fajardo <juniel@alphasys.com.au> \n
	 *            Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since 	1.0.0
	 * @LastUpdated September 12,2017
	 */
	public function check_user_logged_in() {
		Pronto_Wp_Portal_Common::redirect_logged_in_user( 'login' , $this->pup_page('profile') );
	}
	
	/**
	 * A function callback to `wp_logout` action hook.
	 * @see Pronto_Wp_Portal::define_login_hooks()
	 *
	 * This function is responsible for Redirecting the user  
	 * to the portal login page after the user has been logged out.
	 *
	 * @author 	 Jhobet Baroga <jhobet@alphasys.com.au>
	 * @since    1.0.0
	 * @access   public
	 *
	 * @LastUpdated July 26,2017
	 */
	public function redirect_after_logout() {
	    $redirect_url = home_url( $this->pup_page('login') . '?loggedout=true' );
	    wp_safe_redirect( $redirect_url );

		if (defined('PHPUNIT_RUNNING')) return;
		else exit;
	}
	
	/**
	 * A function callback to `login_redirect` action hook.
	 *
	 * Returns the URL to which the user should be
	 * redirected after the (successful) login
	 *
	 * @author     Jhobet Baroga <jhobet@alphasys.com.au>
	 *
	 * @see        Pronto_Wp_Portal::define_login_hooks()
	 *
	 * @since      1.0.0
	 * @access     public
	 *
	 * @param $redirect_to
	 * @param $requested_redirect_to
	 * @param $user
	 *
	 * @return   string Redirect URL
	 *
	 * @LastUpdated July 26, 2017
	 */
	public function redirect_after_login( $redirect_to, $requested_redirect_to, $user ) {

		$this->link_user_to_salesforce( $user );

		if ( isset( $user->roles ) && is_array( $user->roles ) ) {
	        if ( in_array( 'administrator', $user->roles ) ) 
	            return admin_url();
	        else 
	            return home_url( $this->pup_page('profile') );
	    } else 
	        return $redirect_to;
	}
	
	/**
	 * A function callback to `wp_authenticate_user` action hook.
	 *
	 * This is used to perform additional validation/authentication
	 * any time a user logs in to website
	 *
	 * @see        Pronto_Wp_Portal::define_login_hooks()
	 *
	 * @author     Jhobet Baroga <jhobet@alphasys.com.au> \n
	 *             Juniel Fajardo <juniel@alphasys.com.au> \n
	 *             Edil Vincent Liong <edil@alphasys.com.au>
	 *
	 * @since      1.0.0
	 * @access     public
	 *
	 * @param $user
	 * @param $password
	 *
	 * @return   object User
	 *
	 * @LastUpdated July 27, 2017
	 */
	public function additional_authentication( $user, $password ) {
		
		$result = $user;
		
		if( $user && wp_check_password( $password, $user->data->user_pass, $user->ID ) && ! empty( get_option( 'login_filter' ) ) ) {
			
			$err = Pronto_Wp_Portal_Common::pup_get_login_filter_errors( $user );
			$result = ( $err ) ? $err : $user;	
		}
		
		return $result;
	}
	
	
	/**
	 * Check user if it's link to a salesforce contact. If not link, this function creates a contact on Salesforce
	 *               and link to the current user login.
	 *
	 * @author      Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $user
	 *
	 * @LastUpdated April 5, 2018
	 */
	public function link_user_to_salesforce( $user ) {
		$pup_sf_id = get_user_meta( $user->ID, 'pup_sf_id', 1 ) ;

		if(empty($pup_sf_id)){

			$user_email = isset($user->data->user_email) ? $user->data->user_email : 'anonymous@alphasys.com.au';
			$user_login = isset($user->data->user_login) ? $user->data->user_login : $user_email;

			/* Defaults */
			$sf_data['LastName'] = get_user_meta( $user->ID, 'last_name', 1 ) ;
			$sf_data['FirstName'] = get_user_meta( $user->ID, 'first_name', 1 ) ;
			$sf_data['Email'] = $user_email;
			$sf_data['ASPU__Action__c'] = 'include';
			$sf_data['ASPU__WPUsername__c'] = $user_login;
			$sf_data['ASPU__Step1__c'] = 'true';
			
			
			$sf_fields = Pronto_Wp_Portal_Admin::add_salesforce_fields( $sf_data );
			if( $sf_fields['status_code'] !== 201 ) {	

			}else{
				$sf_id = isset($sf_fields['sf_response']['id']) ? $sf_fields['sf_response']['id'] : '';
				update_user_meta( $user->ID, 'pup_sf_id', $sf_id );
			}

		}

	}
	
	
	/**
	 * Toggle login and logout menu.
	 *
	 * @author Junjie Canonio <junjie@alphasys.com.au>
	 *
	 * @param $items
	 * @param $args
	 *
	 * @return string
	 *
	 * @LastUpdated  July 18, 2018
	 */
	public function wfc_portal_add_loginout_navitem($items, $args) {
	    if ($args->theme_location == 'primary') {
	         // Replace "primary" with your theme's menu name
	        if (!(is_user_logged_in())) {
	            $login_item = '<li class="menu-item login-menu-item"><a itemprop="url" href="/wp-login.php" rel="nofollow">Log in</a></li>';
	        } 
	        else {
	            $login_item = '<li class="menu-item logout-menu-item">' . wp_loginout($_SERVER['REQUEST_URI'], false) . '</li>';
	        }
	        $items.= $login_item;
	    }
	    return $items;
	}

}